## Old certificates
sha1: a9:b6:64:46:93:31:72:99:30:57:5e:75:fa:17:fe:fb:fc:2d:6a:96
sha1: b0:90:99:69:d4:9c:bb:74:3f:3a:a6:8a:6b:ab:02:10:81:f2:e5:f1
sha256: 74:2f:1c:ee:5d:6d:13:aa:7f:d5:cc:3e:7d:3e:a2:e1:ac:a3:1c:52:61:18:f4:33:5f:e0:34:84:d9:a6:a8:41

## From current key.jks
md5: 15:FB:60:24:3A:CC:17:15:03:A8:4A:35:41:70:51:83
sha1: A7:64:27:59:68:45:E2:88:53:FF:7D:1B:20:DA:6B:FC:04:3A:2C:76
sha256: 28:3D:A7:1B:BE:72:DA:68:A2:A1:2A:EA:1C:03:3E:38:F6:E3:3E:36:A6:06:89:36:A4:09:32:72:4C:34:5F:FD

## For android debug
sha1: 28:de:79:0d:21:66:d7:08:a5:7b:50:f3:35:59:c7:e6:00:38:97:a0