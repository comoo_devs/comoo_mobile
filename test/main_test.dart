import 'package:comoo/src/pages/signup/signup.dart';
import 'package:comoo/src/pages/signup/signup_access/signup_access_form.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  print('Testing');
  testWidgets('SignUp has something', (WidgetTester tester) async {
    await tester.pumpWidget(SignUpPage());
    await tester.pumpWidget(SignupAccessForm());

    final emailFinder = find.text('email');
    expect(emailFinder, findsOneWidget);
  });
}
