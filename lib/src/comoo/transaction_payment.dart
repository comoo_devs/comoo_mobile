import 'package:cloud_firestore/cloud_firestore.dart';

class TransactionPayment {
  String cardNumber;
  String cardholderName;
  String expiryMonth;
  String expiryYear;
  String cvv;
  DateTime birthDate;
  String cpf;
  String phone;
  String address;
  String complement;
  String city;
  String state;
  String zip;
  String seller;
  String buyer;
  String total;
  DocumentReference negotiation;
  String status;

  Map<String, dynamic> toJson() => {
        "cardNumber": cardNumber,
        "cardholderName": cardholderName,
        "expiryMonth": expiryMonth,
        "expiryYear": expiryYear,
        "cvv": cvv,
        "birthDate": birthDate,
        "cpf": cpf,
        "phone": phone,
        "address": address,
        "complement": complement,
        "city": city,
        "state": state,
        "zip": zip,
        "seller": seller,
        "buyer": buyer,
        "total": total,
        "status": status,
        "negotiation": negotiation
      };
}
