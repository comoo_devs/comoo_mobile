class CategoryItem {
  CategoryItem({this.id, this.name, this.selected: false});
  String name;
  String id;
  bool selected;
}
