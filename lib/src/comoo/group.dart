import 'package:cloud_firestore/cloud_firestore.dart';

enum GroupMenu {
  update,
  leave,
  link,
  invite,
  suggestion,
}

class Group {
  Group(
      {this.id,
      this.name,
      this.avatar,
      this.admin,
      this.userAdmin,
      this.askToJoin,
      this.description,
      this.location,
      this.lastMsg});
  Group.fromJson(Map<dynamic, dynamic> json)
      : id = json['id'],
        name = json['name'],
        avatar = json['avatar'],
        userAdmin = json['admin'] ?? false,
        askToJoin = json['askToJoin'] ?? true,
        description = json['description'] ?? '',
        location = json['location'] ?? '',
        lastMsg = json['lastMsg'] ?? '',
        address = json['address'] == null
            ? GroupAddress()
            : GroupAddress.fromJson(json['address']);

  Group.fromSnapshot(DocumentSnapshot snap)
      : id = snap.documentID,
        name = snap.data['name'],
        avatar = snap.data['avatar'],
        userAdmin = snap.data['admin'],
        askToJoin = snap.data['askToJoin'] ?? false,
        description = snap.data['description'] ?? '',
        location = snap.data['location'] ?? '',
        lastMsg = snap.data['lastMsg'] ?? '',
        address = GroupAddress.fromJson(snap.data['address']);

  Group.fromUserSnapshot(DocumentSnapshot snap)
      : id = snap.documentID,
        name = snap['name'],
        avatar = snap['avatar'],
        admin = snap['admin'],
        askToJoin = snap['askToJoin'] ?? false,
        location = snap['location'] ?? '',
        description = snap['description'] ?? '',
        lastMsg = snap['lastMsg'] ?? '';

  Group.fromGroupSearch(GroupSearch gs)
      : id = gs.id,
        name = gs.name,
        avatar = gs.photoURL,
        admin = gs.isAdmin,
        askToJoin = true,
        location = '',
        description = '',
        lastMsg = '';

  String name;
  String avatar;
  String description;
  String id;
  String lastMsg;
  String location;
  bool admin = false;
  String userAdmin;
  bool askToJoin;
  bool isMember = false;
  List<GroupUser> users = List();
  GroupAddress address;

  addUserFromSnapshot(DocumentSnapshot snap) {
    users.add(GroupUser.fromSnapshot(snap));
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{'id': this.id, 'name': this.name};
  }

  String get subtitle => location.isEmpty || (address?.city ?? '').isEmpty
      ? ''
      : '$location - ${address?.city}';
}

class GroupUser {
  GroupUser({this.id, this.name, this.admin});

  GroupUser.fromSnapshot(DocumentSnapshot snap)
      : id = snap.documentID,
        name = snap.data['name'],
        admin = snap.data['admin'],
        categories = snap.data['categories'],
        avatar = snap.data['avatar'],
        description = snap.data['description'];

  String id;
  String name;
  bool admin = false;
  String avatar;
  String description;
  List categories;
}

class GroupSearch {
  GroupSearch({
    this.id,
    this.name,
    this.photoURL,
    this.inGroup,
    this.isAdmin,
    this.membersQty,
  });

  GroupSearch.fromSnapshot(snap)
      : id = snap['id'],
        name = snap['name'],
        photoURL = snap['avatar'],
        inGroup = snap['inGroup'],
        isAdmin = snap['admin'],
        membersQty = snap['members'];

  GroupSearch.fromJson(Map<dynamic, dynamic> json)
      : id = json['id'],
        name = json['name'],
        photoURL = json['avatar'],
        inGroup = json['inGroup'],
        isAdmin = json['admin'],
        membersQty = json['members'];

  String id;
  String name;
  String photoURL;
  bool inGroup;
  bool isAdmin;
  int membersQty;
}

class GroupAddress {
  GroupAddress({this.city = '', this.state = ''});

  GroupAddress.fromSnapshot(DocumentSnapshot snap)
      : city = snap.data['city'] ?? '',
        state = snap.data['state'] ?? '';

  GroupAddress.fromJson(Map<dynamic, dynamic> json)
      : city = json['city'] ?? '',
        state = json['state'] ?? '';

  String city;
  String state;
}
