class Interest {
  final String interest;
  final int installments;
  final int interestsCents;
  final int totalWithInterestsCents;

  Interest(this.installments, this.interest, this.interestsCents,
      this.totalWithInterestsCents);

  Interest.fromJson(Map<String, dynamic> json)
      : interest = json["interest"],
        installments = json["installments"],
        interestsCents = json["interest_cents"],
        totalWithInterestsCents = json["total_with_interest_cents"];
}
