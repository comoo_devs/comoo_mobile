import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';

class CategoriesModel {
  String id;
  String title;
  String url;
  IconData iconData;
  bool isSelected;

  CategoriesModel(
      {this.id, this.title, this.url, this.isSelected, this.iconData});
}

List<CategoriesModel> categoriesModel = [
  new CategoriesModel(
    id: 'moda_feminina',
    title: 'Moda Feminina',
    url: 'images/feminino.png',
    iconData: COMOO.icons.dress.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'moda_masculina',
    title: 'Moda Masculina',
    url: 'images/masculino.png',
    iconData: COMOO.icons.tie.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'moda_unissex',
    title: 'Moda Unissex',
    url: 'images/unisex.png',
    iconData: COMOO.icons.shirt.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'bebe',
    title: 'Bebê',
    url: 'images/bebe.png',
    iconData: COMOO.icons.baby.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'crianca',
    title: 'Criança',
    url: 'images/crianca.png',
    iconData: COMOO.icons.robot.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'saude_e_beleza',
    title: 'Saúde e Beleza',
    url: 'images/saudebeleza.png',
    iconData: COMOO.icons.beauty.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'casa_e_Decoracao',
    title: 'Casa e Decoração',
    url: 'images/casa.png',
    iconData: COMOO.icons.decoration.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'eletrodomesticos',
    title: 'Eletrodomésticos',
    url: 'images/eletrodomesticos.png',
    iconData: COMOO.icons.fridge.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'eletronicos',
    title: 'Eletrônicos',
    url: 'images/eletronicos.png',
    iconData: COMOO.icons.computer.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'festas_e_casamentos',
    title: 'Festas e Casamentos',
    url: 'images/festascasamentos.png',
    iconData: COMOO.icons.cake.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'fitness_e_esportes',
    title: 'Fitness e Esportes',
    url: 'images/fitness.png',
    iconData: COMOO.icons.fitness.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'jogos_e_games',
    title: 'Jogos e Games',
    url: 'images/games.png',
    iconData: COMOO.icons.games.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'livros_e_revistas',
    title: 'Livros e Revistas',
    url: 'images/livros.png',
    iconData: COMOO.icons.books.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'musica',
    title: 'Música',
    url: 'images/musica.png',
    iconData: COMOO.icons.music.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'hobbies_e_lazer',
    title: 'Hobbies e Lazer',
    url: 'images/hobbies.png',
    iconData: COMOO.icons.nature.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'pets',
    title: 'Pets',
    url: 'images/pets.png',
    iconData: COMOO.icons.dog.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'religiao_e_espiritualidade',
    title: 'Religião e Espiritualidade',
    url: 'images/espiritualidade.png',
    iconData: COMOO.icons.religion.iconData,
    isSelected: false,
  ),
  new CategoriesModel(
    id: 'outros',
    title: 'Outros',
    url: 'images/outros.png',
    iconData: COMOO.icons.dots.iconData,
    isSelected: false,
  ),
];
