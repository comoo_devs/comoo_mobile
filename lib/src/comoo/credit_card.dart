class CreditCard {
  CreditCard({
    this.uid,
    this.name,
    this.number,
    this.year,
    this.month,
    this.isDefault,
    this.brand,
  });

  CreditCard.fromSnapshot(snap)
      : uid = snap.documentID,
        name = snap['name'],
        apiID = snap['creditCard_id'],
        number = snap['number'] == null
            ? ''
            : snap['number']
                .substring(snap['number'].length - 4, snap['number'].length),
        isDefault = snap['default'],
        month = snap['expirationMonth'],
        year = snap['expirationYear'],
        brand = snap['brand'];

  CreditCard.fromMap(snap)
      : uid = snap['uid'],
        name = snap['name'],
        apiID = snap['creditCard_id'],
        number = snap['number'] == null
            ? ''
            : snap['number']
                .substring(snap['number'].length - 4, snap['number'].length),
        isDefault = snap['default'],
        month = snap['expirationMonth'],
        year = snap['expirationYear'],
        brand = snap['brand'];

  String uid;
  String apiID;
  String name;
  String number;
  String year;
  String month;
  bool isDefault;
  String brand;
  bool isSelected = false;
}
