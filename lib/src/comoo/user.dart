import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:flutter/material.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'group.dart';
// import 'product.dart';
import 'package:cloud_functions/cloud_functions.dart';

class User {
  String uid;
  String name;
  String fullName;
  String nickname;
  String email;
  String description;
  String photoURL;
  List likes;
  List<String> interests;
  String status;
  String cpf;
  Phone phone;
  String phonePrefix;
  String birthDate;
  String agencyNumber;
  String bankNumber;
  String accountNumber;
  String sellerStatus;
  String accountType;
  String customerId;
  Map<dynamic, dynamic> defaultCard;
  Address address;
  BankData bank;
  bool hasNotification = false;

  List<User> friendRecommendations = new List<User>();
  List<Group> groupRecommendations = new List<Group>();
  List<String> friends = new List<String>();
  List<String> groups = new List<String>();

  User(this.uid, this.name, this.photoURL);

  User.fromSnapshot(DocumentSnapshot snapshot)
      : uid = snapshot.documentID,
        name = snapshot.data['name'] ?? '',
        photoURL = snapshot.data['photoURL'] ??
            'https://firebasestorage.googleapis.com/v0/b/comoo-app.appspot.com/o/defaultUser.png?alt=media&token=15452a28-dadb-4fc5-a20b-36812e908064',
        status = snapshot.data['status'] ?? 'incomplete',
        sellerStatus = snapshot.data['sellerStatus'] ?? 'incomplete',
        likes =
            List<String>.from((snapshot.data['likes'] as List) ?? <String>[]),
        interests =
            ((snapshot.data['categories'] ?? <dynamic>[]) as List<dynamic>)
                .map<String>((dynamic cat) => cat.toString())
                .toList(),
        fullName = snapshot.data['fullName'] ?? '',
        nickname = snapshot.data['nickname'] ?? '',
        email = snapshot.data['email'] ?? '',
        description = snapshot.data['description'] ?? '',
        cpf = snapshot.data['cpf'] ?? '',
        phone = snapshot.data['phone'] != null
            ? Phone.fromSnapshot(snapshot.data['phone'])
            : Phone(),
        phonePrefix = snapshot.data['phone_prefix'] ?? '',
        address = snapshot.data['address'] != null
            ? Address.fromSnapshot(snapshot.data['address'])
            : Address(),
        birthDate = snapshot.data['birthDate'] ?? '',
        bank = snapshot.data['bank'] != null
            ? BankData.fromSnapshot(snapshot.data['bank'])
            : null,
        agencyNumber = snapshot.data['agencyNumber'] ?? '',
        bankNumber = snapshot.data['bankNumber'] ?? null,
        accountType = snapshot.data['accountType'] ?? null,
        defaultCard = snapshot.data['default_card'] ?? null,
        accountNumber = snapshot.data['accountNumber'] ?? '',
        customerId = snapshot.data['customer_id'] ?? '',
        hasNotification = snapshot.data['littleBall'] ?? false;

  Future save() async {
    return Firestore.instance
        .collection('/users')
        .document(this.uid)
        .updateData(<String, dynamic>{
      'fullName': this.fullName,
      'nickname': this.nickname,
      'email': this.email,
      'description': this.description,
      'cpf': this.cpf,
      'phone': this.phone,
      'phone_prefix': this.phonePrefix,
      'birthDate': this.birthDate,
      'address': this.address,
      'agencyNumber': this.agencyNumber,
      'bankNumber': this.bankNumber,
      'accountNumber': this.accountNumber,
      'sellerStatus': this.sellerStatus,
      'accountType': this.accountType
    });
  }

  Future fetchGroupRecommendation() async {
    // if (this.groupRecommendations.isNotEmpty) return this.groupRecommendations;
    final HttpsCallableResult httpResult = await CloudFunctions.instance
        .getHttpsCallable(functionName: 'fetchGroupRecommendation')
        .call()
        .catchError((error) {
      print('>> fetchGroupRecommendation Error!\n'
          '$error');
      return <dynamic>[];
    });
    List groups = httpResult.data;
    this.groupRecommendations = groups
        .map((g) => Group(
            id: g['id'],
            name: g['name'],
            avatar: g['avatar'],
            userAdmin: g['admin'],
            location: g['location'],
            askToJoin: true,
            admin: false,
            description: g['description']))
        .toList();
    return this.groupRecommendations;
    // var querySnapshot = await _db
    //     .collection('groups')
    //     // .where('askToJoin', isEqualTo: false)
    //     .getDocuments();

    // var userSnapshot = await _db
    //     .collection('users')
    //     .document(this.uid)
    //     .collection('groups')
    //     .getDocuments();

    // List<Group> groups = new List<Group>();
    // List<String> userGroups = userSnapshot.documents
    //     .map((DocumentSnapshot gSnap) => gSnap.documentID)
    //     .toList();

    // querySnapshot.documents.forEach((DocumentSnapshot groupSnap) {
    //   if (userGroups.indexOf(groupSnap.documentID) == -1) {
    //     groups.add(new Group.fromSnapshot(groupSnap));
    //   }
    // });

    // this.groupRecommendations = groups;

    // return this.groupRecommendations;
  }

  Stream<List<Future<DocumentSnapshot>>> fetchUserRecommendation(
      {int page = 0, int size = 15}) {
    StreamTransformer<QuerySnapshot, List<Future<DocumentSnapshot>>>
        transformer =
        StreamTransformer.fromBind((Stream<QuerySnapshot> stream) {
      return stream.asyncMap((QuerySnapshot query) {
        return query.documentChanges.map((DocumentChange change) async {
          return await Firestore.instance
              .collection('users')
              .document(change.document.documentID)
              .get();
        }).toList();
      });
    });
    return Firestore.instance
        .collection('recommendation')
        .document(currentUser.uid)
        .collection('users')
        .orderBy('similarity', descending: true)
        // .orderBy('show', descending: false)
        .limit(size + page * size)
        .snapshots()
        .transform<List<Future<DocumentSnapshot>>>(transformer);
  }

  @deprecated
  Future<List<User>> fetchFriendRecommendation() async {
    // Deprecated on 18/03/2019
    final HttpsCallableResult result = await CloudFunctions.instance
        .getHttpsCallable(functionName: 'fetchUserRecommendation')
        .call()
        .catchError((error) {
      print('>> fetchUserRecommendation Error!\n'
          '$error');
      return <dynamic>[];
    });
    List users = result.data;
    this.friendRecommendations = users
        .map((u) => User(
            u['uid'],
            u['name'],
            u['photoURL'] ??
                'https://firebasestorage.googleapis.com/v0/b/comoo-app.appspot.com/o/defaultUser.png?alt=media&token=15452a28-dadb-4fc5-a20b-36812e908064'))
        .toList();
    return this.friendRecommendations;

    //Deprecated on 01/11/2018
//    if (this.friendRecommendations.isNotEmpty) {
//      return this.friendRecommendations;
//    }
//
//    var userFriends = await _db
//        .collection('users')
//        .document(this.uid)
//        .collection('friends')
//        .getDocuments();
//
//    var usersSnapshot = await _db.collection('users').getDocuments();
//    List<User> users = new List<User>();
//
//    List<String> friendsId = userFriends.documents
//        .map((DocumentSnapshot snap) => snap.documentID)
//        .toList();
//
//    usersSnapshot.documents.forEach((DocumentSnapshot snap) {
//      if (snap.documentID != this.uid) {
//        if (friendsId.indexOf(snap.documentID) == -1) {
//          users.add(User.fromSnapshot(snap));
//        }
//      }
//    });
//    this.friendRecommendations = users;
//    return this.friendRecommendations;
  }

  Future fetchFriendReference() async {
    var _friends = await Firestore.instance
        .collection('users')
        .document(this.uid)
        .collection('friends')
        .getDocuments();
    this.friends = _friends.documents
        .map((DocumentSnapshot snap) =>
            (snap['id'] as DocumentReference).documentID)
        .toList();

    return this.friends;
  }

  Future fetchGroupReference() async {
    var _groups = await Firestore.instance
        .collection('users')
        .document(this.uid)
        .collection('groups')
        .getDocuments();
    this.groups = _groups.documents
        .map((DocumentSnapshot snap) => snap.documentID)
        .toList();

    return this.groups;
  }

  Future addToLiked(String product) async {
    if (this.likes == null)
      this.likes = new List<String>();
    else {
      this.likes = this.likes.map((p) => p.toString()).toList();
    }
    this.likes.add(product);
    return Firestore.instance
        .collection('users')
        .document(this.uid)
        .updateData(<String, dynamic>{'likes': this.likes});
  }

  Future removeToLiked(String product) async {
    if (this.likes == null)
      return null;
    else {
      this.likes = this.likes.map((p) => p.toString()).toList();
    }
    this.likes.remove(product);
    return Firestore.instance
        .collection('users')
        .document(this.uid)
        .updateData(<String, dynamic>{'likes': this.likes});
  }

  Future updateFCMToken(String token) async {
    return Firestore.instance
        .collection('users')
        .document(this.uid)
        .updateData({'fcmToken': token});
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'uid': uid,
      'photoURL': photoURL,
    };
  }
}

class UserSearch {
  String id;
  String name;
  String photoURL;
  String nickname;
  bool isFriend;

  UserSearch({
    this.id,
    this.name,
    this.photoURL,
    this.nickname,
    this.isFriend,
  });

  UserSearch.fromSnapshot(snap)
      : id = snap['id'],
        name = snap['name'],
        photoURL = snap['photoURL'],
        nickname = snap['nickname'],
        isFriend = snap['isFriend'];

  UserSearch.fromJson(dynamic json)
      : id = json['id'],
        name = json['name'],
        photoURL = json['photoURL'],
        nickname = json['nickname'],
        isFriend = json['isFriend'];
}

class Phone {
  String areaCode = '';
  String countryCode = '';
  String number = '';

  Phone({
    this.areaCode,
    this.countryCode,
    this.number,
  });

  Phone.fromSnapshot(snap)
      : areaCode = snap['areaCode'],
        countryCode = snap['countryCode'],
        number = snap['number'];
}

class Address {
  String zipCode = '';
  String street = '';
  String district = '';
  String number = '';
  String complement = '';
  String city = '';
  String state = '';
  String country = '';

  Address({
    this.zipCode,
    this.street,
    this.district,
    this.number,
    this.complement,
    this.city,
    this.state,
    this.country,
  });

  Address.fromSnapshot(snap)
      : zipCode = snap['zipCode'],
        street = snap['street'],
        district = snap['district'],
        number = snap['number'],
        complement = snap['complement'],
        city = snap['city'],
        state = snap['state'],
        country = snap['country'];
}

class BankData {
  String account;
  String agency;
  String number;
  String type;

  BankData({
    this.account,
    this.agency,
    this.number,
    this.type,
  });

  BankData.fromSnapshot(snap)
      : account = snap['account'],
        agency = snap['agency'],
        number = snap['number'],
        type = snap['type'];
}

class Friend {
  Friend(
      {@required this.uid,
      @required this.name,
      @required this.nickname,
      @required this.photoURL});

  String photoURL;
  String uid;
  String name;
  String nickname;
}
