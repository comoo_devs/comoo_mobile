class Validations {
  String validateName(String value) {
    if (value.isEmpty) return 'Nome é obrigatório';
    final RegExp nameExp = RegExp(r'^[A-za-z ]{2,300}$');
    if (!nameExp.hasMatch(value)) {
      return 'Por favor, apenas caractéres alfanuméricos';
    }
    return null;
  }

  String validateEmail(String value) {
    if (value.isEmpty) return 'E-mail é obrigatório.';
    final RegExp nameExp =
        RegExp(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");
    if (!nameExp.hasMatch(value)) return 'Endereço de e-mail inválido';
    return null;
  }

  String validatePassword(String value) {
    if (value.isEmpty) {
      return 'Escolha uma senha.';
    }
    if (value.length < 6) {
      return 'Digite no mínimo 6 caracteres.';
    }
    return null;
  }

  String validateInviteCode(String value) {
    if (value.isEmpty) return 'Informe o código do seu convite';

    return null;
  }

  String validateCPF(String value) {
    String invalidString = 'Insira um número de CPF válido.';
    if (value.isEmpty) return 'Informe o número do CPF';
    final RegExp cpfExp = RegExp(
        '([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})');
    if (!cpfExp.hasMatch(value)) {
      return invalidString;
    } else {
      List<String> values = <String>[];
      //Gets cpf digits from string
      if (value.contains('-')) {
        values = value.split('-');
      } else {
        values = <String>[
          value.substring(0, value.length - 2),
          value.substring(value.length - 2, value.length)
        ];
      }
      String cpf;
      if (values[0].contains('.')) {
        cpf = values[0].replaceAll('.', '');
      } else {
        cpf = values[0];
      }
      String verifyDigits = values[1];
      //Verify first digit
      int total = 0;
      for (var i = 0, multiplier = 10; i < cpf.length; ++i, --multiplier) {
        total += int.parse(cpf[i]) * multiplier;
      }
      int verify = (total * 10) % 11;
      verify = verify == 10 ? 0 : verify;
      if (verifyDigits[0] != verify.toString()) {
        return invalidString;
      }
      //Verify second digit
      cpf += verifyDigits[0];
      total = 0;
      for (var i = 0, multiplier = 11; i < cpf.length; ++i, --multiplier) {
        total += int.parse(cpf[i]) * multiplier;
      }
      verify = (total * 10) % 11;
      verify = verify == 10 ? 0 : verify;
      if (verifyDigits[1] != verify.toString()) {
        return invalidString;
      }
    }
    return null;
  }

  String validatePhone(String value) {
    if (value.isEmpty) return 'Informe o número do telefone';
    final RegExp phoneExp = RegExp(
        r'^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$');
    if (!phoneExp.hasMatch(value)) {
      return 'Insira um número de telefone válido';
    }
    return null;
  }

  String validateEmpty(String value) {
    if (value.isEmpty) return 'Preenchimento obrigatório';
    return null;
  }

  String validateNickname(String value) {
    if (value.isEmpty) return 'Preenchimento obrigatório.';
    final RegExp nicknameExp = RegExp(r'^[a-z0-9\._-]{3,20}$');
    if (!nicknameExp.hasMatch(value)) {
      if (value.length < 3)
        return 'Nome de usuário inválido.\nTamanho mínimo 3 caracteres.';
      if (value.length > 20)
        return 'Nome de usuário inválido.\nTamanho máximo 20 caracteres.';
      return 'Nome de usuário inválido.\nApenas letras minúsculas, números e os símbolos _ - .';
    }
    return null;
  }

  String validateBithday(String value) {
    if (value.isEmpty) return 'Preenchimento obrigatório';
    // final RegExp birthDateExp = RegExp(
    //     r'^(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\\d\\d$');
    // print(birthDateExp.hasMatch(value));
    // if (!birthDateExp.hasMatch(value)) {
    //   return 'Data de nascimento inválida.';
    // }
    return null;
  }

  String validateCard(String value) {
    if (value.isEmpty) return 'Informe o número do cartão de crédito';
    RegExp _creditCard = RegExp(
        r'^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$');
    if (!_creditCard.hasMatch(value))
      return 'Insira um número de cartão de crédito válido';

    return null;
  }

  String validateMonthYear(String value) {
    if (value.isEmpty) return 'Informe o mês e ano de validade do cartão';
    RegExp _monthYear = RegExp(r'([0-9]{2}\/[0-9]{2})');
    if (!_monthYear.hasMatch(value))
      return 'Insira um número de cartão de crédito válido';
    int month = int.parse(value.split('/').first);
    if (month > 12) return 'Informe um mês válido';
    int year = int.parse(value.split('/').last);
    DateTime now = DateTime.now();
    if ((2000 + year) < now.year) return 'Informe um ano válido';
    if ((2000 + year) == now.year && month <= now.month)
      return 'Informe uma data válida';
    return null;
  }

  String validateBirthDate(String value) {
    if (value.isEmpty) return 'Insira uma data no formato dd/mm/aaaa';
    RegExp _dateRegExp = RegExp(r'([0-9]{2})\/([0-9]{2})\/([0-9]{4})');
    if (!_dateRegExp.hasMatch(value))
      return 'Insira uma data no formato dd/mm/aaaa';
    List<String> values = value.split('/');
    int day = int.parse(values[0]);
    int month = int.parse(values[1]);
    int year = int.parse(values[2]);
    DateTime now = DateTime.now();
    DateTime date = DateTime(year, month, day);
    int minYear = 18;
    if (date.isAfter(now)) return 'Informe uma data válida';
    DateTime minDate = DateTime(now.year - minYear, now.month, now.day);
    if (date.isAfter(minDate)) return 'É necessário ter $minYear anos';
    return null;
  }
}
