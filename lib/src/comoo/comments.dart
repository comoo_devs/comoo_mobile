import 'package:cloud_firestore/cloud_firestore.dart';

import 'user.dart';

class Comment {
  Comment(this.text, this.user, this.date);

  Comment.fromSnapshot(DocumentSnapshot snap, User user,
      {Map<String, DocumentReference> refs})
      : text = snap['text'],
        user = user,
        refs = refs ?? <String, DocumentReference>{},
        date = (snap['date'] is Timestamp)
            ? DateTime.fromMillisecondsSinceEpoch(
                (snap['date'] as Timestamp).millisecondsSinceEpoch)
            : (snap['date'] as DateTime);

  DateTime date;
  String text;
  User user;
  Map<String, DocumentReference> refs = <String, DocumentReference>{};

  void addRefs(Map<String, DocumentReference> refs) {
    this.refs = refs;
  }
}
