import 'package:cloud_firestore/cloud_firestore.dart';

class Negotiation {
  String id;
  DocumentReference buyer;
  String buyerName;
  DocumentReference seller;
  String sellerName;
  DocumentReference product;
  String title;
  String thumb;
  DateTime date;
  DateTime lastUpdate;
  DateTime expirationDate;
  String lastMessage;
  String status;
  double price;
  String side;
  String invoiceId;
  Map<dynamic, dynamic> payment;
  String location;
  String orderId;
  String paymentId;
  Payment paymentDescriptor;
  NegotiationMessage message;
  bool paid;
  int unreadMessages;

  Negotiation(
      {this.id,
      this.title,
      this.thumb,
      this.status,
      this.price,
      this.seller,
      this.buyer,
      this.product});

  Negotiation.fromSnapshot(DocumentSnapshot snap)
      : id = snap.documentID,
        title = snap['title'],
        buyer = snap['client'],
        seller = snap['seller'],
        status = snap['status'],
        product = snap['product'],
        thumb = snap['thumb'],
        date = DateTime.fromMicrosecondsSinceEpoch(
            snap['date'].microsecondsSinceEpoch),
        lastUpdate = DateTime.fromMicrosecondsSinceEpoch(
            snap['lastUpdate'].microsecondsSinceEpoch),
        side = snap['side'],
        price = snap['price'] * 1.0,
        expirationDate = snap['expirationDate'],
        unreadMessages = snap['chats'] ?? 0,
        payment = snap['payment'] ?? null,
        invoiceId = snap['invoice_id'] ?? '',
        lastMessage = snap['lastMessage'] ?? '',
        location = snap['location'] ?? '',
        paid = snap['paid'] ?? false,
        orderId = snap['order_id'],
        paymentId = snap['payment_id'],
        paymentDescriptor = snap['payment_descriptor'] != null
            ? (snap['payment_descriptor']['paymentType'] == 'CREDIT_CARD'
                ? CreditCardPayment.fromSnapshot(snap['payment_descriptor'])
                : BankSlipPayment.fromSnapshot(snap['payment_descriptor']))
            : null;

  Negotiation.fromJson(Map<dynamic, dynamic> json, id,
      {DocumentReference buyer,
      DocumentReference seller,
      DocumentReference product})
      : id = id,
        title = json['title'],
        this.buyer = buyer,
        this.seller = seller,
        status = json['status'],
        this.product = product,
        thumb = json['thumb'],
        unreadMessages = json['chats'] ?? 0,
        date = DateTime.fromMillisecondsSinceEpoch(json['date']),
        lastUpdate = DateTime.fromMillisecondsSinceEpoch(json['lastUpdate']),
        // side = json['side'],
        price = (json['price'] as num).toDouble(),
        // expirationDate = json['expirationDate'],
        lastMessage = json['lastMessage'] ?? '',
        paid = json['paid'] ?? false,
        location = json['location'] ?? '';

  String get statusName => _statusName();
  String _statusName() {
    String statusName;
    switch (status) {
      case 'cancelled':
        statusName = 'Cancelado';
        break;
      case 'under negotiation':
        statusName = 'Aguardando Pagamento';
        break;
      case 'pending':
        statusName = 'Processando Pagamento';
        break;
      case 'sent':
        statusName = 'Enviado';
        break;
      case 'waiting_send':
        statusName = 'Aguardando Entrega';
        break;
        break;
      case 'paid':
        statusName = 'Pago';
        break;
      case 'finished':
        statusName = 'Concluído';
        break;
      case 'expired':
        statusName = 'VENCIDO';
        break;
      default:
        statusName = '';
        break;
    }
    return statusName.toUpperCase();
  }

  int valueOfStatus(String status) {
    if (status == 'under negotiation') {
      return 0;
    } else {
      return 10;
    }
  }

  int compareTo(Negotiation negotiation) {
    return valueOfStatus(status).compareTo(valueOfStatus(negotiation.status));
  }

  String displayUnreadMessages() {
    if (unreadMessages > 99) {
      return '99+';
    } else {
      return '$unreadMessages';
    }
  }
}

class Payment {
  String paymentType;
  double total;
  double sellerTotal;
  String releaseDate;

  Payment({this.paymentType, this.total, this.sellerTotal});

  Payment.fromSnapshot(snap)
      : paymentType = snap['paymentType'],
        total = snap['total'] + 0.0;
}

class BankSlipPayment implements Payment {
  String paymentType;
  double total;
  double sellerTotal;
  double tax;
  String bankSlipCode;
  String bankSlipUrl;
  String date;
  String releaseDate;

  BankSlipPayment();

  BankSlipPayment.fromSnapshot(snap)
      : paymentType = snap['paymentType'],
        total = snap['total'] == null ? 0.0 : snap['total'] + 0.0,
        sellerTotal =
            snap['sellerTotal'] == null ? 0.0 : snap['sellerTotal'] + 0.0,
        tax = snap['tax'] == null ? 0.0 : snap['tax'] + 0.0,
        bankSlipCode = snap['boleto_code'],
        date = snap['date'],
        releaseDate = snap['releaseDate'],
        bankSlipUrl = snap['boleto_link'];
}

class CreditCardPayment extends Payment {
  String paymentType;
  double total;
  double sellerTotal;
  String date;
  String releaseDate;
  String number;
  int installmentAmount;
  double portion;
  String name;
  String brand;

  CreditCardPayment();

  CreditCardPayment.fromSnapshot(snap)
      : paymentType = snap['paymentType'],
        total = snap['total'] == null ? 0.0 : snap['total'] + 0.0,
        sellerTotal =
            snap['sellerTotal'] == null ? 0.0 : snap['sellerTotal'] + 0.0,
        date = snap['date'],
        releaseDate = snap['releaseDate'],
        number = snap['number'],
        installmentAmount = snap['installmentCount'],
        portion = snap['portion'] == null ? 0.0 : snap['portion'] + 0.0,
        name = snap['name'],
        brand = snap['brand'];
}

class NegotiationMessage {
  String type;
  String text;
  DateTime date;
  DocumentReference from;

  NegotiationMessage({
    this.text,
    this.type,
    this.date,
    this.from,
  });

  NegotiationMessage.fromSnapshot(snap)
      : text = snap['text'],
        type = snap['type'],
        date = DateTime.fromMicrosecondsSinceEpoch(
            snap['date'].microsecondsSinceEpoch),
        this.from = snap['from'];
}
