import 'package:comoo/src/comoo/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'authentication.dart';
import 'dart:async';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/utility/site.dart';
import 'package:comoo/src/comoo/authentication.dart' as authentication;

Comoo comoo = new Comoo();

class Comoo {
  final ComooApi api = new ComooApi();
  final ComooAuth auth = new ComooAuth();
  final Site site = new Site(SiteSetting.STANDARD);
  static final Comoo _comoo = new Comoo._internal();

  factory Comoo() {
    return _comoo;
  }

  Comoo._internal();

  Future<User> currentUser() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    String uid = _prefs.getString('uid');
    if (uid == null) {
      var _user = await auth.currentUser();
      if (_user == null) {
        return null;
      }
      uid = _user.uid;
    }
    _prefs.setString('uid', uid);
    final User user = await api.fetchUser(uid);
    authentication.currentUser = user;
    return user;
  }
}
