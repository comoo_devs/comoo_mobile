import 'package:cloud_firestore/cloud_firestore.dart';

class GroupMessage {
  GroupMessage.fromJson(json)
      : message = json['message'],
        date = json['date'],
        id = json['id'],
        user = json['user'],
        type = json['type'];

  GroupMessage.fromSnapshot(DocumentSnapshot snap)
      : id = snap.documentID,
        date = DateTime.fromMicrosecondsSinceEpoch(
            snap.data['date'].microsecondsSinceEpoch),
        type = snap.data['type'],
        message = snap.data['message'],
        groups = snap.data['groups'],
        likes = snap.data['likes'] ?? 0,
        commentsQty = snap.data['commentsQty'] ?? 0,
        user = (snap.data['user'] as Map<dynamic, dynamic>)
            .cast<String, dynamic>();

  String message;
  DateTime date;
  String type;
  String id;
  List groups;
  int likes;
  int commentsQty;
  Map<String, dynamic> user;
  Firestore _db = Firestore.instance;

  addLike() {
    this.likes++;
    var ref = _db.collection('loose_comments').document(this.id);
    return ref.updateData(<String, int>{'likes': this.likes});
  }

  removeLike() {
    this.likes--;
    var ref = _db.collection('loose_comments').document(this.id);
    return ref.updateData(<String, int>{'likes': this.likes});
  }

  Future delete() async {
    var batch = _db.batch();
    batch.delete(_db.collection('loose_comments').document(this.id));
    if (groups != null) {
      groups.forEach((g) {
        var feedRef = _db
            .collection('groups')
            .document(g['id'])
            .collection('feed')
            .document(this.id);
        batch.delete(feedRef);
      });
    }
    return batch.commit();
  }
}
