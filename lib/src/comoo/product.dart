import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/message.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/utility/converter.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_functions/cloud_functions.dart';

class Product {
  DocumentReference owner;
  DateTime date;
  String title;
  String description;
  double price;
  String location;
  String condition;
  String id;
  String status;
  String thumb;
  String category;
  String brand;
  String size;
  String paymentType;
  Map user;
  List groups;
  List<String> photos;
  List thumbs;
  int likes;
  int commentsQty;
  bool underNegotiation;
  double height;
  double width;
  // DocumentReference _ref;
  bool sold = false;

  Product(
      {this.id,
      this.title,
      this.description,
      this.photos,
      this.price,
      this.location,
      this.condition,
      this.thumb,
      this.owner,
      this.date,
      this.likes,
      this.commentsQty,
      this.status,
      this.category,
      this.brand});

  Product.fromSnapshot(DocumentSnapshot snap)
      : id = snap.documentID,
        // _ref =
        //     Firestore.instance.collection('products').document(snap.documentID),
        title = snap.data['title'] ?? '',
        description = snap.data['description'] ?? '',
        price = DataConverter.convertToDouble(snap.data['price']),
        location = snap.data['location'] ?? '',
        condition = snap.data['condition'] ?? '',
//        thumb = snap.data['thumb'],
        photos = List<String>.from(snap.data['photos']),
        thumbs = snap.data['thumbs'] ?? [null],
        user = snap.data['user'],
        date = DateTime.fromMicrosecondsSinceEpoch(
            snap.data['date'].microsecondsSinceEpoch),
        size = snap.data['size'],
        groups = snap.data['groups'],
        likes = snap.data['likes'] ?? 0,
        commentsQty = snap.data['commentsQty'] ?? snap.data['comments'] ?? 0,
        category = snap.data['category'],
        brand = snap.data['brand'],
        paymentType = snap.data['payMethod'],
        underNegotiation = snap.data['underNegotiation'],
        status = snap.data['status'],
        height = (snap.data['height'] as num)?.toDouble(),
        width = (snap.data['width'] as num)?.toDouble(),
        sold = snap.data['sold'] ?? false;

//  FirebaseAuth _auth = FirebaseAuth.instance;
  Firestore _db = Firestore.instance;
  FirebaseStorage _storage = FirebaseStorage.instance;
  CloudFunctions _functions = CloudFunctions.instance;

  Future delete() async {
    return _functions
        .getHttpsCallable(functionName: 'deleteProduct')
        .call(<String, dynamic>{
          'productId': this.id,
        })
        .then((HttpsCallableResult result) => result.data)
        .catchError((onError) => <String, dynamic>{});
//    var batch = _db.batch();
//    batch.updateData(
//        _db.collection('products').document(this.id), {'status': 'deleted'});
//    batch.delete(this.owner.collection('feed').document(this.id));
    // batch.delete(_db.collection('products').document(this.id));
//    return batch.commit();
  }

  Future saveDraft(List<File> newPhotos) async {
    this.status = 'draft';
    return _save(newPhotos);
  }

  Future<List<String>> _saveImages(String productId, List<File> images) async {
    List<Future<String>> saveTasks = new List<Future<String>>();
    images.forEach((file) {
      ProductPhoto productPhoto = new ProductPhoto();
      saveTasks.add(productPhoto.create(productId, file));
    });
    return Future.wait(saveTasks);
  }

  Future<void> addLike() async {
    final DocumentReference productDocumentRef =
        _db.collection('products').document(this.id);
    final DocumentSnapshot snap = await productDocumentRef.get();
    if (!snap.exists) {
      return null;
    }
    likes = snap.data['likes'] ?? 0;
    // print(likes);
    likes++;

    return productDocumentRef.updateData(<String, int>{'likes': likes});
  }

  Future removeLike() async {
    // this.likes--;
    var productDocumentRef = _db.collection('products').document(this.id);
    final DocumentSnapshot snap = await productDocumentRef.get();
    if (!snap.exists) {
      return null;
    }
    likes = snap.data['likes'] ?? 1;
    // print(likes);
    likes--;

    return productDocumentRef.updateData(<String, int>{'likes': likes});
  }

//
//  Future _uploadImages(String productId, List<File> images) async {
//    List<Future> uploadTasks = new List<Future>();
//    images.forEach((file) {
//      if (file is File) {
//        File _file = file;
//        final String rand = '${new Random().nextInt(10000)}';
//        StorageReference ref =
//            _storage.ref().child('/products/$productId/$rand.jpg');
//
//        uploadTasks.add(ref.put(_file).future);
//      }
//    });
//
//    return Future.wait(uploadTasks).then((List uploads) {
//      List<String> images = uploads
//          .cast<UploadTaskSnapshot>()
//          .map((uploads) => uploads.downloadUrl.toString())
//          .toList();
//      return images;
//    });
//  }

  Future _save(List<File> newPhotos) async {
//    FirebaseUser user = await _auth.currentUser();
    User user = currentUser;
    WriteBatch batch = _db.batch();

    var productDocumentRef = _db.collection('products').document(this.id);
    var userDocumentRef = _db.collection('users').document(user.uid);
    //Save all images and add to the product photo collection
    var images = await _saveImages(productDocumentRef.documentID, newPhotos);

    if (images.length > 0) this.thumb = images[0];

    batch.setData(productDocumentRef, <String, dynamic>{
      'condition': this.condition,
      'location': this.location,
      'description': this.description ?? '',
      'title': this.title ?? '',
      'price': this.price ?? '',
      'groups': this.groups,
      'date': new DateTime.now(),
      'status': this.status,
      'category': this.category,
      'brand': this.brand,
      'thumb': this.thumb,
      'owner': userDocumentRef
    });

    if (this.status == 'public') {
      await _broadCastMessage(this.groups, productDocumentRef);
      await _addToFeed(productDocumentRef);
    }

    return batch.commit();
  }

  Future publish(List<String> groups, List<File> newImages) async {
    this.status = 'public';
    this.groups = groups;

    return _save(newImages);
  }

  Future update(List<File> newPhotos) async {
    var productDocumentRef = _db.collection('products').document(this.id);
    var images = await _saveImages(productDocumentRef.documentID, newPhotos);
    if (images.length > 0) this.thumb = images[0];

    return productDocumentRef.updateData({
      'condition': this.condition,
      'location': this.location,
      'description': this.description ?? '',
      'title': this.title ?? '',
      'price': this.price ?? '',
      'updated': new DateTime.now(),
      'status': this.status,
      'category': this.category,
      'brand': this.brand,
      'thumb': this.thumb,
    });
  }

  Future _broadCastMessage(
      List<String> groups, DocumentReference product) async {
    var batch = _db.batch();

//    FirebaseUser user = await _auth.currentUser();
    User user = currentUser;
    DocumentReference userRef = _db.collection('users').document(user.uid);

    groups.forEach((String id) {
      var message = _db
          .collection('groups')
          .document(id)
          .collection('messages')
          .document();
      Message newMessage = new Message('Novo produto adicionado', userRef,
          new DateTime.now(), MessageType.product,
          product: product);
      batch.setData(message, newMessage.toJson());
    });

    return batch.commit();
  }

  Future _addToFeed(DocumentReference product) async {
    DocumentReference userProductRef = _db
        .collection('users')
        .document(currentUser.uid)
        .collection('feed')
        .document(product.documentID);
    return userProductRef
        .setData({'date': new DateTime.now(), 'product': product});
  }
}

class ProductPhoto {
  String photoId;
  String filename;
  String downloadURL;

  final FirebaseStorage _storage = FirebaseStorage.instance;
  final Firestore _db = Firestore.instance;

  Future<String> create(String productId, File file) async {
    DocumentReference fileDoc = _db
        .collection('products')
        .document(productId)
        .collection('photos')
        .document();
    this.photoId = fileDoc.documentID;
    this.filename = 'photo_$photoId.jpg';

    //UPLOAD IMAGE
    this.downloadURL = await upload(productId, file);

    //SAVE AS COLLECTION DOCUMENT OF PRODUCT
    await fileDoc.setData(<String, dynamic>{
      'filename': this.filename,
      'downloadURL': this.downloadURL
    });
    return this.downloadURL;
  }

  //TESTA ESSA BOMBA AI DEPOIS
  Future<String> upload(String productId, File file) async {
    StorageReference ref =
        _storage.ref().child('/products/$productId/$filename.jpg');
    final StorageTaskSnapshot uploadTask = await ref.putFile(file).onComplete;
    final String result = await uploadTask.ref.getDownloadURL().then((onValue) {
      return onValue.toString();
    }).catchError((onError) {
      print(onError);
      return '';
    });
    return result;
  }
}

class ProductSearch {
  ProductSearch({
    this.id,
    this.title,
    this.photoURL,
  });

  ProductSearch.fromSnapshot(snap)
      : id = snap['id'],
        title = snap['name'],
        photoURL = snap['photo'];

  ProductSearch.fromJson(dynamic json)
      : id = json['id'],
        title = json['name'],
        photoURL = json['photo'];

  String id;
  String title;
  String photoURL;
}
