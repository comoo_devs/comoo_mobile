class Invoice {
  String id;
  String createdAt;
  DateTime dueDate;
  DateTime paidAt;
  DateTime receivableDate;
  String paidValue;
  String taxesPaid;

  Invoice.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        createdAt = json["created_at"],
        dueDate = DateTime.parse(json["due_date"]),
        receivableDate = DateTime.parse(json["receivable_date"]?? json["paid_at"]),
        paidAt = DateTime.parse(json["paid_at"]),
        paidValue = json["paid_value"],
        taxesPaid = json["taxes_paid"];
}
