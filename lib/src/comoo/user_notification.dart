import 'package:cloud_firestore/cloud_firestore.dart';

class UserNotification {
  String id;
  String thumb;
  String type;
  String name;
  String title;
  String from;
  String text;
  String group;
  String ref;
  String negotiation;
  DateTime date;
  bool approved;

  UserNotification.fromSnapshot(DocumentSnapshot snapshot)
      : id = snapshot.documentID,
        thumb = snapshot['thumb'],
        type = snapshot['type'],
        name = snapshot['name'],
        title = snapshot['title'],
        from = snapshot['from'],
        text = snapshot['text'],
        group = snapshot['group'],
        ref = snapshot['ref'],
        negotiation = snapshot['negotiation'],
        approved = snapshot['approved'] ?? false,
        date = DateTime.fromMicrosecondsSinceEpoch(
            snapshot['date'].microsecondsSinceEpoch);
}
