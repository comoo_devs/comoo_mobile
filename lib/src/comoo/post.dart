import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

class Post {
  Post.fromJson(json)
      : message = json['message'],
        date = json['date'],
        id = json['id'],
        user = json['user'],
        type = json['type'];

  Post.fromSnapshot(DocumentSnapshot snap)
      : id = snap.documentID,
        date = DateTime.fromMicrosecondsSinceEpoch(
            snap.data['date'].microsecondsSinceEpoch),
        type = snap.data['type'],
        message = snap.data['message'],
        groups = snap.data['groups'],
        likes = snap.data['likes'] ?? 0,
        commentsQty = snap.data['commentsQty'] ?? 0,
        user = snap.data['user'];

  String message;
  DateTime date;
  String type;
  String id;
  List groups;
  int likes;
  int commentsQty;
  Map user;

  Firestore _db = Firestore.instance;

  Future delete() async {
    var batch = _db.batch();
    batch.delete(_db.collection('posts').document(this.id));
    if (groups != null) {
      groups.forEach((g) {
        var feedRef = _db
            .collection('groups')
            .document(g['id'])
            .collection('feed')
            .document(this.id);
        batch.delete(feedRef);
      });
    }
    return batch.commit();
  }

  addLike() {
    this.likes++;
    var postRef = _db.collection('posts').document(this.id);
    return postRef.updateData(<String, int>{'likes': this.likes});
  }

  removeLike() {
    this.likes--;
    var postRef = _db.collection('posts').document(this.id);
    return postRef.updateData(<String, int>{'likes': this.likes});
  }
}
