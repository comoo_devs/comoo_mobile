import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';

class MessageType {
  static const String message = 'message';
  static const String event = 'event';
  static const String product = 'product';
  static const String reference = 'reference';
}

class Message {
  String id;
  String text;
  String type;
  DocumentReference from;
  DocumentReference product;
  String name;
  DateTime date;

  Message(this.text, this.from, this.date, this.type, {this.product});

  Message.fromSnapshot(DocumentSnapshot snap)
      : id = snap.documentID,
        type = snap.data['type'],
        text = snap.data['text'],
        from = snap.data['from'],
        product = snap.data['product'],
        date = (snap.data['date'] is DateTime)
            ? snap.data['date']
            : (snap.data['date'] as Timestamp).toDate();

  Future<User> fetchUser() async {
    if (from == null) return null;

    return new User.fromSnapshot(await from.get());
  }

  Future<Product> fetchProduct() async {
    if (product == null) return null;
    var productDoc = await product.get();
    if (!productDoc.exists) return null;
    Product _product = new Product.fromSnapshot(productDoc);
    return _product;
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'product': this.product,
      'text': this.text,
      'type': this.type,
      'from': this.from,
      'date': this.date
    };
  }
}
