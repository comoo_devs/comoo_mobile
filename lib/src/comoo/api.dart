import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:cloud_functions/cloud_functions.dart';

class ComooApi {
  final Firestore db = Firestore.instance;
  final CloudFunctions api = CloudFunctions.instance;

  Future<User> fetchUser(String uid) async {
    try {
      DocumentSnapshot userSnapshot =
          await db.collection('users').document(uid).get();
      return new User.fromSnapshot(userSnapshot);
    } catch (e) {
      print('Error fetching User $uid:' + e.toString());
      return null;
    }
  }

  Future updateUserInterests(String uid, List interests) async {
    return db
        .collection('users')
        .document(uid)
        .updateData({'interests': interests});
  }

  // Future<String> fetchUserInviteCode(String uid) async {
  //   var querySnap = await db
  //       .collection('invites')
  //       .where('uid', isEqualTo: uid)
  //       .limit(1)
  //       .getDocuments();
  //   if (querySnap.documents.length > 0) {
  //     return querySnap.documents[0]["code"];
  //   } else {
  //     String code = generate("abcdefghijlmnopqrstuvxz123456789", 6);
  //     code = code.toUpperCase();
  //     await db
  //         .collection('invites')
  //         .document()
  //         .setData(<String, dynamic>{"code": code, "uid": uid});

  //     return code;
  //   }
  // }

  Future<String> fetchUserBalance(String uid) async {
    var userDoc = await db.collection('users').document(uid).get();
    var balance = userDoc["balance"] ?? 0;
    return balance.toString();
  }

  Future<bool> fetchUserHasRequestedProduct(
      String uid, String productId) async {
    var userRef = db.collection("users").document(uid);
    var requestsSnapshot = await db
        .collection("products")
        .document(productId)
        .collection("requests")
        .where("user", isEqualTo: userRef)
        .getDocuments();
    List<String> requestsIds = new List();
    requestsSnapshot.documents.forEach((req) {
      if (req['status'] != "cancelled") {
        requestsIds.add(req.documentID);
      }
    });
    return requestsIds.isNotEmpty;
  }

  // Future requestProduct(String productId) async {
  //   final dynamic result = await CloudFunctions.instance.call(
  //       functionName: "requestProduct",
  //       parameters: {"productId": productId}).timeout(Duration(seconds: 30));
  //   return result;
  // }

  Future<DocumentSnapshot> fetchGroup(String groupId) {
    return db.collection("groups").document(groupId).get();
  }

  Future<QuerySnapshot> fetchGroupUsers(String groupId) {
    return db
        .collection("groups")
        .document(groupId)
        .collection("users")
        .getDocuments();
  }
}
