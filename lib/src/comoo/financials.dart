class Financials {
  String amount;
  String type;
  String description;
  String entryDate;
  String reference;
  String referenceType;
  String accountId;
  String transactionType;
  String balance;
  String customerRef;

  Financials.fromJson(Map<String, dynamic> json)
      : amount = json["amount"],
        type = json["type"],
        description = json["description"],
        entryDate = json["entry_date"] ,
        reference = json["reference"],
        referenceType = json["reference_type"],
        accountId = json["account_id"],
        transactionType = json["transaction_type"],
        balance = json["balance"],
        customerRef = json["customer_ref"];
}
