import 'package:cloud_firestore/cloud_firestore.dart';

class Bank {
  Bank({this.name, this.number});

  Bank.fromSnapshot(DocumentSnapshot snapshot)
      : name = snapshot['name'],
        number = snapshot['number'];
  String name;
  String number;
}

enum AccountType {
  blank,
  checking,
  savings,
}

class MapAccountType {
  final Map<AccountType, String> _mapData = <AccountType, String>{
    AccountType.blank: '',
    AccountType.checking: 'CHECKING',
    AccountType.savings: 'SAVING'
  };
  final Map<AccountType, String> _mapName = <AccountType, String>{
    AccountType.blank: 'selecione',
    AccountType.checking: 'Conta Corrente',
    AccountType.savings: 'Conta Poupança'
  };
  String Function(AccountType) get accountName =>
      (AccountType at) => _mapName[at];
  String Function(AccountType) get accountData =>
      (AccountType at) => _mapData[at];
}

class BankAccount {
  BankAccount({this.name, this.type});
  String name;
  AccountType type;
  String number;
  String digit;
}
