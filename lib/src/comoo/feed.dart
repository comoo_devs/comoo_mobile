import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';

enum FeedItemType {
  product,
  post,
}

class FeedItem {
  FeedItem({
    this.id,
    this.ref,
    this.snapshot,
    this.type,
    this.comment,
  }) {
    if (ref != null && snapshot == null) {
      _fetchSnapshot();
    }
  }

  factory FeedItem.fromSnapshot(DocumentSnapshot snap) {
    final String id = snap.documentID;
    final String type = snap.data['type'];
    final DocumentReference ref = snap.data[type];
    final FeedItemType feedItemType = typeFromString(type);
    if (ref == null) {
      return null;
    } else if (feedItemType == null) {
      return null;
    } else {
      // Future<DocumentSnapshot> fut = ref.get().catchError((onError) => null);
      return FeedItem(
        id: id,
        ref: ref,
        type: feedItemType,
        // snapshot: fut,
      );
    }
  }
  String id;
  DocumentReference ref;
  DocumentSnapshot snapshot;
  FeedItemType type;
  DocumentSnapshot comment;

  static typeFromString(String type) {
    type = type.toLowerCase();
    switch (type) {
      case 'product':
        return FeedItemType.product;
        break;
      case 'post':
        return FeedItemType.post;
        break;
      default:
        return null;
        break;
    }
  }

  Future<void> _fetchSnapshot() async {
    final DocumentSnapshot snap = await ref.get();
    if (snap.exists) {
      snapshot = snap;
    }
  }
}

class FeedGroup {
  FeedGroup({
    this.id,
    this.name,
    this.description,
    this.location,
    this.city,
    this.state,
    this.quantity,
    this.askToJoin,
    this.avatar,
  });
  String id;
  String name;
  String description;
  String location;
  String city;
  String state;
  int quantity;
  bool askToJoin;
  String avatar;

  String get subtitle => (location?.isEmpty ?? true)
      ? (city?.isEmpty ?? true)
          ? state ?? ''
          : (state?.isEmpty ?? true) ? city : '$city - $state'
      : (city?.isEmpty ?? true) ? location : '$location - $city';
}
