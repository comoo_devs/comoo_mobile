library authentication;

import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

User currentUser;

class UserData {
  String displayName = "";
  String nickname = "";
  String email = "";
  String uid = "";
  String password = "";
  String confirmPassword = "";
  String photoURL = "";
  String invitationCode = "";
  UserData(
      {this.displayName,
      this.nickname,
      this.email,
      this.uid,
      this.password,
      this.invitationCode});

  void clear() {
    this.displayName = "";
    this.nickname = "";
    this.email = "";
    this.uid = "";
    this.password = "";
    this.confirmPassword = "";
    this.photoURL = "";
    this.invitationCode = "";
  }
}

class UserAuth {
  FirebaseUser firebaseUser;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _db = Firestore.instance;

  Future<FirebaseUser> createUser(UserData userData) async {
    return _auth.createUserWithEmailAndPassword(
        email: userData.email, password: userData.password);
  }

  Future<FirebaseUser> linkWithEmailAndPassword(UserData userData) async {
    // return _auth.linkWithEmailAndPassword(
    //   email: userData.email,
    //   password: userData.password,
    // );
    final AuthCredential credential = EmailAuthProvider.getCredential(
        email: userData.email, password: userData.password);
    final FirebaseUser fireUser = await _auth.currentUser();
    return fireUser.linkWithCredential(credential);
  }

  Future<FirebaseUser> linkWithGoogle(
      GoogleSignInAuthentication googleAuth) async {
    final AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);
    final FirebaseUser fireUser = await _auth.currentUser();
    return fireUser.linkWithCredential(credential);
  }

  Future<FirebaseUser> linkWithFacebook(
      FacebookAccessToken facebookToken) async {
    final AuthCredential credential =
        FacebookAuthProvider.getCredential(accessToken: facebookToken.token);
    final FirebaseUser fireUser = await _auth.currentUser();
    return fireUser.linkWithCredential(credential);
  }

  Future<FirebaseUser> verifyUser(UserData userData) async {
    firebaseUser = await _auth.signInWithEmailAndPassword(
        email: userData.email, password: userData.password);

    await fetchCurrentUser();
    return firebaseUser;
  }

  Future updatePassword(String newPassword) async {
    var curr = await _auth.currentUser();
    return curr.updatePassword(newPassword);
  }

  Future resetPassword(String email) async {
    return _auth.sendPasswordResetEmail(email: email);
  }

  Future signOut() async {
    // await _db.collection("users").document(currentUser.uid).updateData({
    //   "fcmToken": "",
    // });
    currentUser = null;
    if (await FacebookLogin().isLoggedIn) {
      FacebookLogin().logOut();
    }
    if (await GoogleSignIn().isSignedIn()) {
      await GoogleSignIn().signOut();
    }
    return _auth.signOut();
  }

  Future<User> fetchCurrentUser() async {
    if (firebaseUser == null) {
      firebaseUser = await _auth.currentUser();
    }
    if (firebaseUser == null) {
      return null;
    }

    DocumentReference userRef =
        _db.collection('users').document(firebaseUser.uid);
    var userSnapshot = await userRef.get();
    if (!userSnapshot.exists) {
      _auth.signOut();
      return null;
    }

    currentUser = new User.fromSnapshot(userSnapshot);

    return currentUser;
  }

  Future<void> updateEmail(String email) async {
    assert(email != null);
    await fetchCurrentUser();
    final FirebaseAuth _auth = FirebaseAuth.instance;
    FirebaseUser firebaseUser = await _auth.currentUser();
    return await firebaseUser.updateEmail(email);
  }
}

class ComooAuth {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser> currentUser() async {
    return await _auth.currentUser();
  }
}
