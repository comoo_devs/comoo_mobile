class DataConverter {
  static double convertToDouble(dynamic value) {
    switch (value.runtimeType) {
      case int:
        return (value as int).toDouble();
        break;
      case double:
        return (value as double);
        break;
      case String:
        return double.parse(value as String);
        break;
      default:
        return null;
        break;
    }
  }
}
