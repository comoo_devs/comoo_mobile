List list = [
  {
    "id": "moda_feminina",
    "name": "Moda Feminino",
    "icon": "images/icon_feminino.png",
  },
  {
    "id": "moda_masculina",
    "name": "Moda Masculino",
    "icon": "images/icon_masculino.png"
  },
  {
    "id": "moda_unissex",
    "name": "Moda Unissex",
    "icon": "images/icon_unisex.png"
  },
  {
    "id": "bebe",
    "name": "Bebê",
    "icon": "images/icon_bebe.png"
  },
  {
    "id": "crianca",
    "name": "Criança",
    "icon": "images/icon_crianca.png"
  },
  {
    "id": "saude_e_beleza",
    "name": "Saúde e Beleza",
    "icon": "images/icon_saude_beleza.png"
  },
  {
    "id": "casa_e_Decoracao",
    "name": "Casa e Decoração",
    "icon": "images/icon_casa_decoracao.png"
  },
  {
    "id": "eletrodomesticos",
    "name": "Eletrodomésticos",
    "icon": "images/icon_eletrodomesticos.png"
  },
  {
    "id": "eletronicos",
    "name": "Eletrônicos",
    "icon": "images/icons_eletronicos.png"
  },
  {
    "id": "festas_e_casamentos",
    "name": "Festas e Casamentos",
    "icon": "images/icons_festas_casamentos.png"
  },
  {
    "id": "fitness_e_esportes",
    "name": "Fitness e Esportes",
    "icon": "images/icons_fitness.png"
  },
  {
    "id": "jogos_e_games",
    "name": "Jogos e Games",
    "icon": "images/icons_jogos.png"
  },
  {
    "id": "livros_e_revistas",
    "name": "Livros e Revistas",
    "icon": "images/icons_livros_revistas.png"
  },
  {
    "id": "musica",
    "name": "Música",
    "icon": "images/icons_music.png"
  },
  {
    "id": "hobbies_e_lazer",
    "name": "Hobbies e Lazer",
    "icon": "images/icons_hobbies_lazer.png"
  },
  {
    "id": "pets",
    "name": "Pets",
    "icon": "images/icons_pets.png"
  },
  {
    "id": "religiao_e_espiritualidade",
    "name": "Religião e Espiritualidade",
    "icon": "images/icons_religiao.png"
  },
  {
    "id": "outros",
    "name": "Outros",
    "icon": "images/icons_outros.png"
  }
];