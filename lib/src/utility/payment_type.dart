enum PaymentType {
  Both,
  BankSlip,
  CreditCard,
}

class PaymentTypeEnum {
  PaymentType getPaymentType(String value) {
    PaymentType paymentType;
    switch (value) {
      case "bankslip":
        paymentType = PaymentType.BankSlip;
        break;
      case "creditcard":
        paymentType = PaymentType.CreditCard;
        break;
      default:
        paymentType = PaymentType.Both;
    }
    return paymentType;
  }

  String getPaymentTypeName(PaymentType paymentType) {
    String name = "";
    switch (paymentType) {
      case PaymentType.BankSlip:
        name = "Boleto Bancário";
        break;
      case PaymentType.CreditCard:
        name = "Cartão de Crédito";
        break;
      default:
        name = "Boleto Bancário e Cartão de Crédito";
    }
    return name;
  }

  String getPaymentTypeValue(PaymentType paymentType) {
    String value = "";
    switch (paymentType) {
      case PaymentType.BankSlip:
        value = "bankslip";
        break;
      case PaymentType.CreditCard:
        value = "creditcard";
        break;
      default:
        value = "both";
    }
    return value;
  }
}
