import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  final String _uid = 'uid';
  SharedPreferences _prefs;

  Future<bool> isLoggedIn() async {
    if (_prefs == null) {
      _prefs = await SharedPreferences.getInstance();
    }
    final String uid = _prefs.getString(_uid);
    return uid != null;
  }

  Future<String> userUID() async {
    if (_prefs == null) {
      _prefs = await SharedPreferences.getInstance();
    }
    return _prefs.getString(_uid);
  }
}
