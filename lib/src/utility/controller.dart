import 'package:flutter_masked_text/flutter_masked_text.dart';

class ComooController {
  static final MaskedTextController cpfController =
      new MaskedTextController(mask: '000.000.000-00');
  static final MaskedTextController zipController =
      new MaskedTextController(mask: '00000-000');
  static final MaskedTextController areaCodeAndPhoneNumberController =
      new MaskedTextController(mask: '(00) 00000-0000');
  static final MaskedTextController phoneNumberController =
      new MaskedTextController(mask: '00000-0000');
  static final MaskedTextController areaCodeController =
      new MaskedTextController(mask: '00');

  static final MaskedTextController dateController =
      new MaskedTextController(mask: '00/00/0000');

  static MaskedTextController agencyController(String bankType) {
    MaskedTextController agencyController;
    switch (bankType) {
      case "001":
        agencyController = MaskedTextController(mask: "0000-0");
        break;
      case "033":
        agencyController = MaskedTextController(mask: "0000");
        break;
      case "104":
        agencyController = MaskedTextController(mask: "0000");
        break;
      case "237":
        agencyController = MaskedTextController(mask: "0000-0");
        break;
      case "341":
        agencyController = MaskedTextController(mask: "0000");
        break;
      default:
        agencyController = MaskedTextController(mask: "0000");
        break;
    }
    return agencyController;
  }

  static MaskedTextController accountController(String bankType) {
    MaskedTextController accountController;
    switch (bankType) {
      case "001":
        accountController = MaskedTextController(mask: "00000000-0");
        break;
      case "033":
        accountController = MaskedTextController(mask: "00000000-0");
        break;
      case "104":
        accountController = MaskedTextController(mask: "00000000-0");
        break;
      case "237":
        accountController = MaskedTextController(mask: "0000000-0");
        break;
      case "341":
        accountController = MaskedTextController(mask: "00000-0");
        break;
      default:
        accountController = MaskedTextController(mask: "000000000-0");
        break;
    }
    return accountController;
  }
}
