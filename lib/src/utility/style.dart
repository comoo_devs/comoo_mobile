import 'package:flutter/material.dart';

class ComooColors {
  static const Color chumbo = const Color.fromRGBO(60, 55, 70, 1.0);
  static const Color cinzaMedio = const Color.fromRGBO(125, 125, 140, 1.0);
  static const Color cinzaClaro = const Color.fromRGBO(195, 195, 205, 1.0);
  static const Color white500 = const Color.fromRGBO(225, 225, 230, 1.0);
  static const Color gray = const Color.fromRGBO(133, 133, 133, 1.0);
  static const Color pink = const Color.fromRGBO(235, 25, 95, 1.0);
  static const Color roxo = const Color.fromRGBO(70, 29, 88, 1.0); //#461D58
  static const Color turqueza = const Color.fromRGBO(5, 160, 175, 1.0);
}

class ComooStyles {
  static const fontFamily = 'Montserrat';
  static const InputBorder underLineBorder = const UnderlineInputBorder(
      borderSide: BorderSide(color: ComooColors.gray));
  static const TextStyle payment_type = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 15.0);

  static const TextStyle userDetailName = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w600,
      fontSize: 22.0);
  static const TextStyle groupDetailName = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 22.0);
  static const TextStyle groupDetailDesc_bold = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 15.0);
  static const TextStyle groupDetailLocation = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 14.0);
  static const TextStyle groupDetailDesc = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 15.0);
  static const TextStyle payment_type_subtitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 11.0);
  static const TextStyle negociacao = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w600,
      fontSize: 15.0);
  static const TextStyle titulo_negociacao = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w600,
      fontSize: 13.0);
  static const TextStyle productPrice = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.bold,
      fontSize: 18.0);

  static const TextStyle preco_negociacao = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.normal,
      fontSize: 20.0);

  static const TextStyle quero = const TextStyle(
      fontFamily: fontFamily,
      color: Colors.white,
      fontWeight: FontWeight.bold,
      fontSize: 16.0);

  static const TextStyle listTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 14.0);
  static const TextStyle listDescription = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 12.0);
  static const TextStyle alertMessage = const TextStyle(
      fontFamily: fontFamily,
      color: Colors.white,
      fontWeight: FontWeight.w500,
      fontSize: 20.0);
  static const TextStyle welcomeMessage = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w500,
      fontSize: 20.0);
  static const TextStyle botaoRoxo = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w500,
      fontSize: 15.0);
  static const TextStyle welcomeMessageTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.bold,
      fontSize: 28.0);
  static const TextStyle welcomeMessageBold = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.bold,
      fontSize: 20.0);
  static const TextStyle landingMessage = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w500,
      fontSize: 24.0);
  static const TextStyle landingMessageBold = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.bold,
      fontSize: 24.0);
  static const TextStyle requestAccessText = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w500,
      fontSize: 18.0);
  static const TextStyle requestAccessTextBold = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.bold,
      fontSize: 18.0);
  static const TextStyle tosText = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w500,
      fontSize: 12.0);
  static const TextStyle inputText = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w500,
      fontSize: 15.0);

  static const TextStyle dropdownText = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 13.0);
  static const TextStyle inputLabel = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 15.0);
  static const TextStyle productInputLabel = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w500,
      fontSize: 14.0);

  static const TextStyle inputLabelCinzaClaro = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.cinzaClaro,
      fontWeight: FontWeight.w500,
      fontSize: 12.0);
  static const TextStyle inputLabelCinzaMedio = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.cinzaMedio,
      fontWeight: FontWeight.w500,
      fontSize: 12.0);
  static const TextStyle inputLabelRoxo = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w500,
      fontSize: 13.0);
  static const TextStyle nomes = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 12.0);
  static const TextStyle nomeFeed = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w700,
      fontSize: 14.0);
  static const TextStyle chatData = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.cinzaMedio,
      fontWeight: FontWeight.w600,
      fontSize: 12.0);
  static const TextStyle produtoNome = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 14.0);
  static const TextStyle produtoDesc = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 14.0);
  static const TextStyle localizacao = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 13.0);
  static const TextStyle categoriesBold = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 10.0);
  static const TextStyle datas = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 12.0);
  static const TextStyle feed_suggestion = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 11.0);
  static const TextStyle productChatSeller = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 13.0);
  static const TextStyle chatEvent = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w500,
      fontSize: 12.0);
  static const TextStyle chatMessage = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 14.0);
  static const TextStyle notificationTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w600,
      fontSize: 14.0);
  static const TextStyle texto = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.normal,
      fontSize: 14.0);
  static const TextStyle textoSnackBar = const TextStyle(
      fontFamily: fontFamily,
      color: Colors.white,
      fontWeight: FontWeight.normal,
      fontSize: 13.0);
  static const TextStyle balanceTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 12.0);
  static const TextStyle profileTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.bold,
      fontSize: 15.0);
  static const TextStyle profileMenu = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 15.0);

  static const TextStyle product_tags = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 13.0);
  static const TextStyle texto_h2 = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 11.0);
  static const TextStyle botaoNotificacoes = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w600,
      fontSize: 13.0);
  static const TextStyle botaoNotificacoesDisabled = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.cinzaClaro,
      fontWeight: FontWeight.w600,
      fontSize: 13.0);

  static const TextStyle subTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.normal,
      fontSize: 12.0);

  static const TextStyle inputPriceLabel = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w500,
      fontSize: 11.0);

  static const TextStyle texto_roxo = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.normal,
      fontSize: 14.0);

  static const TextStyle statusNegociacaoNotificacoes = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w600,
      fontSize: 14.0);

  static const TextStyle profile_logout = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w600,
      fontSize: 12.0);

  static const TextStyle profile_name = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w600,
      fontSize: 20.0);

  static const TextStyle produto_social = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w500,
      fontSize: 13.0);

  static const TextStyle productChatTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 14.0);
  static const TextStyle texto_bold = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w700,
      fontSize: 14.0);

  static const TextStyle botoes = const TextStyle(
      color: ComooColors.roxo,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w700,
      fontSize: 16.0);

  static const TextStyle botao = const TextStyle(
      fontFamily: fontFamily, fontWeight: FontWeight.w700, fontSize: 16.0);

  static const TextStyle botaoTurquezaChat = const TextStyle(
      color: ComooColors.turqueza,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w600,
      fontSize: 13.0);
  static const TextStyle botaoTurquezaCategorias = const TextStyle(
      color: ComooColors.turqueza,
      fontFamily: fontFamily,
      fontWeight: FontWeight.bold,
      fontSize: 13.0);
  static const TextStyle botaoChat = const TextStyle(
      color: Colors.white,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w600,
      fontSize: 13.0);
  static const TextStyle botaoTurqueza = const TextStyle(
      color: ComooColors.turqueza,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w500,
      fontSize: 16.0);
  static const TextStyle tabBarStyle = const TextStyle(
      fontFamily: fontFamily, fontWeight: FontWeight.w600, fontSize: 16.0);
  static const TextStyle botao_branco = const TextStyle(
      color: Colors.white,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.60,
      fontSize: 15.0);
  static const TextStyle hintIndicacao = const TextStyle(
      color: Colors.white54,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w600,
      fontSize: 15.0);

  static const TextStyle containerTurqueza = const TextStyle(
      color: Colors.white,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w500,
      fontSize: 16.0);

  static const TextStyle containerTurquezaBold = const TextStyle(
      color: Colors.white,
      fontFamily: fontFamily,
      fontWeight: FontWeight.bold,
      fontSize: 16.0);

  static const TextStyle tableBlack = const TextStyle(
      color: ComooColors.chumbo,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w500,
      fontSize: 16.0);

  static const TextStyle tableBlackBold = const TextStyle(
      color: ComooColors.chumbo,
      fontFamily: fontFamily,
      fontWeight: FontWeight.bold,
      fontSize: 16.0);

  static const TextStyle appBar_botao_branco = const TextStyle(
      color: Colors.white,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w700,
      fontSize: 12.0);

  static const TextStyle titulos = const TextStyle(
      color: ComooColors.roxo,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w700,
      fontSize: 18.0);

  static const TextStyle titulos_h2 = const TextStyle(
      color: ComooColors.turqueza,
      fontFamily: fontFamily,
      fontWeight: FontWeight.bold,
      fontSize: 14.0);

  static const TextStyle grupoNovoTitle = const TextStyle(
      color: ComooColors.roxo,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w600,
      fontSize: 30.0);

  static const TextStyle grupoNovo = const TextStyle(
      color: ComooColors.roxo,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w500,
      fontSize: 18.0);

  static const TextStyle indicacao = const TextStyle(
      color: Colors.white,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w500,
      fontSize: 20.0);

  static const TextStyle appBarTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.roxo,
      fontWeight: FontWeight.w700,
      fontSize: 16.0);
  static const TextStyle appBarTitleWhite = const TextStyle(
      fontFamily: fontFamily,
      color: Colors.white,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
      fontSize: 14.0);
  static const TextStyle appBarTitleSixteen = const TextStyle(
      fontFamily: fontFamily,
      color: Colors.white,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
      fontSize: 16.0);
  static const TextStyle transactionProductTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w600,
      fontSize: 15.0);
  static const TextStyle transactionPriceTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 12.0);
  static const TextStyle transactionPaymentType = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 12.0);
  static const TextStyle transactionTotalTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w600,
      fontSize: 14.0);
  static const TextStyle transactionCode = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.cinzaClaro,
      fontWeight: FontWeight.w500,
      fontSize: 13.0);
  static const TextStyle transactionTotal = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w600,
      fontSize: 14.0);
  static const TextStyle transactionPrice = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 13.0);
  static const TextStyle transactionStatus = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 14.0);
  static const TextStyle transactionSeller = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 15.0);
  static const TextStyle transactionSellerBold = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w600,
      fontSize: 15.0);
  static const TextStyle productTitle = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.turqueza,
      fontWeight: FontWeight.w600,
      fontSize: 14.0);
  static const TextStyle productTitlePrice = const TextStyle(
      fontFamily: fontFamily,
      color: ComooColors.chumbo,
      fontWeight: FontWeight.w500,
      fontSize: 13.0);

  static InputDecoration inputCustomDecoration(String labelText) {
    return InputDecoration(
      labelText: labelText.toUpperCase(),
      isDense: true,
      helperStyle: ComooStyles.texto,
      focusedBorder: ComooStyles.underLineBorder,
      labelStyle: ComooStyles.inputLabel.copyWith(color: ComooColors.roxo),
      border: ComooStyles.underLineBorder,
    );
  }

  static InputDecoration inputDecoration(String labelText,
      {String helpText, String hint, Icon icon, Widget suffixIcon}) {
    return InputDecoration(
        suffixIcon: suffixIcon,
        labelText: labelText.toUpperCase(),
        icon: icon ?? null,
        hintText: hint ?? "",
        isDense: true,
        helperText: helpText ?? "",
        helperStyle: ComooStyles.texto,
        focusedBorder: ComooStyles.underLineBorder,
        labelStyle: ComooStyles.inputLabel,
        border: ComooStyles.underLineBorder);
  }

  static InputDecoration inputDecorationRoxoNoBorder(String labelText,
      {String helpText, String hint, Icon icon}) {
    return InputDecoration(
        labelText: labelText.toUpperCase(),
        icon: icon ?? null,
        hintText: hint ?? "",
        isDense: true,
        helperText: helpText ?? "",
        helperStyle: ComooStyles.texto,
        focusedBorder: ComooStyles.underLineBorder,
        labelStyle: ComooStyles.inputLabelRoxo,
        border: ComooStyles.underLineBorder);
  }

  static InputDecoration inputDecorationIndicacao(String labelText,
      {String helpText, String hint}) {
    return InputDecoration(
        labelText: labelText.toUpperCase(),
        helperText: helpText ?? "",
        hintText: hint,
        hintStyle: ComooStyles.hintIndicacao,
        labelStyle: ComooStyles.inputLabel,
        border: InputBorder.none);
  }

  static InputDecoration inputDecorationNoBorder(String labelText,
      {String helpText, Icon icon}) {
    return InputDecoration(
        labelText: labelText.toUpperCase(),
        icon: icon ?? null,
        helperText: helpText ?? "",
        labelStyle: ComooStyles.inputLabel,
        border: InputBorder.none);
  }

  static InputDecoration inputDecorationRoxo(String labelText,
      {String helpText, String hint}) {
    return InputDecoration(
        labelText: labelText.toUpperCase(),
        helperText: helpText ?? "",
        hintText: hint ?? "",
        isDense: true,
        focusedBorder: ComooStyles.underLineBorder,
        labelStyle: ComooStyles.inputLabelRoxo,
        border: ComooStyles.underLineBorder);
  }

  static InputDecoration inputDecorationRoundedRoxo(String labelText,
      {String helpText, String hint}) {
    return InputDecoration(
        isDense: true,
        labelText: labelText.toUpperCase(),
        helperText: helpText ?? "",
        hintText: hint,
        hintStyle: ComooStyles.inputLabelCinzaClaro,
        contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
        labelStyle: ComooStyles.inputLabelRoxo,
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ComooColors.chumbo),
            gapPadding: 0.0,
            borderRadius: BorderRadius.circular(25.0)),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: ComooColors.chumbo),
            gapPadding: 0.0,
            borderRadius: BorderRadius.circular(25.0)));
  }

  static InputDecoration inputRoundedDecoration(String labelText,
      {String helpText}) {
    return InputDecoration(
        labelText: labelText.toUpperCase(),
        helperText: helpText ?? "",
        labelStyle: ComooStyles.inputLabel,
        border: OutlineInputBorder(
            gapPadding: 0.0, borderRadius: BorderRadius.circular(17.0)));
  }
}

class ComooIcons {
  static const IconThemeData roxo =
      const IconThemeData(color: ComooColors.roxo, size: 16.0);
  static const IconThemeData white =
      const IconThemeData(color: Colors.white, size: 16.0);
  static const IconData curtir = const IconData(
    0xe928,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData anuncio = const IconData(
    0xe908,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData convidar = const IconData(
    0xe90b,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData criar_comoo = const IconData(
    0xe90a,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData pedir_produto = const IconData(
    0xe909,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData home = const IconData(
    0xe90c,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData mais = const IconData(
    0xe90d,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData mensagem = const IconData(
    0xe929,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData notificacoes = const IconData(
    0xe90e,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData perfil = const IconData(
    0xe90f,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData saldo = const IconData(
    0xe910,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData comentar = const IconData(
    0xe929,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData estado_produto = const IconData(
    0xe927,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData marca = const IconData(
    0xe926,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData preco = const IconData(
    0xe90e,
    fontFamily: 'comoo_icons',
  );
  static const IconData localizacao = const IconData(
    0xe925,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData tamanho = const IconData(
    0xe92a,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData busca = const IconData(
    0xe911,
    fontFamily: 'comoo_icons',
  );
  static const IconData camera = const IconData(
    0xe92b,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData compras = const IconData(
    0xe906,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData vendas = const IconData(
    0xe903,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData saldo_perfil = const IconData(
    0xe902,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData perfil_perfil = const IconData(
    0xe901,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData meus_anuncios = const IconData(
    0xe904,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData minhas_comoos = const IconData(
    0xe905,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData galeria = const IconData(
    0xe923,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData chatSend = const IconData(
    0xe92d,
    fontFamily: 'comoo_icons_novo',
  );
  static const IconData photoDelete = const IconData(
    0xe92c,
    fontFamily: 'comoo_icons_novo',
  );
}

class ComooAppBar {
  static roundTopBar(String title) {
    ComooAppBar appBar = ComooAppBar();
    return appBar.topBar(
        height: 70.0,
        radius: 15.0,
        top: 20.0,
        right: 70.0,
        fontSize: 12.0,
        label: title,
        color: ComooColors.roxo,
        textColor: ComooColors.cinzaClaro);
  }

  Widget topBar({
    double height,
    double radius,
    double top,
    double right,
    double fontSize,
    String label,
    Color color,
    textColor,
  }) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        height: height,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(radius),
              bottomRight: Radius.circular(radius)),
        ),
        child: Row(
          children: <Widget>[
            Container(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: BackButton(color: Colors.white),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: top, right: right),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    label,
                    style: TextStyle(color: textColor, fontSize: fontSize),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class COMOO {
  static final _Icons icons = _Icons();
  static final _Fonts fonts = _Fonts();
  static final _Colors colors = _Colors();
  static final _Buttons buttons = _Buttons();
}

class _Icons {
  final _Icon search = _Icon("\ue911", 0xe911, 'comoo_icons');
  final _Icon chat = _Icon("\ue906", 0xe906, 'comoo_icons');
  final _Icon preco = _Icon("\ue90e", 0xe90e, 'comoo_icons');
  final _Icon like = _Icon("\ue928", 0xe928, 'comoo_icons_novo');
  final _Icon anuncio = _Icon("\ue908", 0xe908, 'comoo_icons_novo');
  final _Icon convidar = _Icon("\ue90b", 0xe90b, 'comoo_icons_novo');
  final _Icon createGroup = _Icon("\ue90a", 0xe90a, 'comoo_icons_novo');
  final _Icon user = _Icon("\ue909", 0xe909, 'comoo_icons_novo');
  final _Icon userWaves = _Icon("\ue914", 0xe914, 'comoo_icons');
  final _Icon home = _Icon("\ue90c", 0xe90c, 'comoo_icons_novo');
  final _Icon mais = _Icon("\ue90d", 0xe90d, 'comoo_icons_novo');
  final _Icon maisfilled = _Icon("\ue010", 0xe010, 'comoo_icons_novo');
  final _Icon mensagem = _Icon("\ue929", 0xe929, 'comoo_icons_novo');
  final _Icon notificacoes = _Icon("\ue90e", 0xe90e, 'comoo_icons_novo');
  final _Icon perfil = _Icon("\ue90f", 0xe90f, 'comoo_icons_novo');
  final _Icon wallet = _Icon("\ue910", 0xe910, 'comoo_icons_novo');
  final _Icon comentar = _Icon("\ue929", 0xe929, 'comoo_icons_novo');
  final _Icon estadoProduto = _Icon("\ue927", 0xe927, 'comoo_icons_novo');
  final _Icon marca = _Icon("\ue926", 0xe926, 'comoo_icons_novo');
  final _Icon localizacao = _Icon("\ue925", 0xe925, 'comoo_icons_novo');
  final _Icon tamanho = _Icon("\ue92a", 0xe92a, 'comoo_icons_novo');
  final _Icon camera = _Icon("\ue92b", 0xe92b, 'comoo_icons_novo');
  final _Icon buyings = _Icon("\ue906", 0xe906, 'comoo_icons_novo');
  final _Icon sellings = _Icon("\ue903", 0xe903, 'comoo_icons_novo');
  final _Icon saldoPerfil = _Icon("\ue902", 0xe902, 'comoo_icons_novo');
  final _Icon perfilPerfil = _Icon("\ue901", 0xe901, 'comoo_icons_novo');
  final _Icon meusAnuncios = _Icon("\ue904", 0xe904, 'comoo_icons_novo');
  final _Icon minhasComoos = _Icon("\ue905", 0xe905, 'comoo_icons_novo');
  final _Icon galeria = _Icon("\ue923", 0xe923, 'comoo_icons_novo');
  final _Icon chatSend = _Icon("\ue92d", 0xe92d, 'comoo_icons_novo');
  final _Icon photoDelete = _Icon("\ue92c", 0xe92c, 'comoo_icons_novo');
  final _Icon sharedProfit = _Icon("\ue907", 0xe907, 'comoo_icons_novo');
  final _Icon books = _Icon("\ue911", 0xe911, 'comoo_icons_novo');
  final _Icon computer = _Icon("\ue912", 0xe912, 'comoo_icons_novo');
  final _Icon religion = _Icon("\ue913", 0xe913, 'comoo_icons_novo');
  final _Icon dress = _Icon("\ue914", 0xe914, 'comoo_icons_novo');
  final _Icon cake = _Icon("\ue915", 0xe915, 'comoo_icons_novo');
  final _Icon fitness = _Icon("\ue916", 0xe916, 'comoo_icons_novo');
  final _Icon games = _Icon("\ue917", 0xe917, 'comoo_icons_novo');
  final _Icon nature = _Icon("\ue918", 0xe918, 'comoo_icons_novo');
  final _Icon tie = _Icon("\ue919", 0xe919, 'comoo_icons_novo');
  final _Icon music = _Icon("\ue91a", 0xe91a, 'comoo_icons_novo');
  final _Icon dots = _Icon("\ue91b", 0xe91b, 'comoo_icons_novo');
  final _Icon dog = _Icon("\ue91c", 0xe91c, 'comoo_icons_novo');
  final _Icon beauty = _Icon("\ue91d", 0xe91d, 'comoo_icons_novo');
  final _Icon shirt = _Icon("\ue91e", 0xe91e, 'comoo_icons_novo');
  final _Icon baby = _Icon("\ue91f", 0xe91f, 'comoo_icons_novo');
  final _Icon decoration = _Icon("\ue920", 0xe920, 'comoo_icons_novo');
  final _Icon robot = _Icon("\ue921", 0xe921, 'comoo_icons_novo');
  final _Icon fridge = _Icon("\ue922", 0xe922, 'comoo_icons_novo');
}

class _Icon {
  _Icon(this._uid, this.hex, this.fontFamily);
  String _uid;
  int hex;
  String fontFamily;

  IconData get iconData => IconData(hex, fontFamily: fontFamily);
  String get unicode => _uid;
}

class _Fonts {
  final String montSerrat = 'Montserrat';
}

class _Colors {
  /// Hex: #3C3746
  ///
  /// RGB: (60,55,70)
  final Color lead = Color.fromRGBO(60, 55, 70, 1.0);

  /// Hex: #7D7D8B
  ///
  /// RGB: (125,125,140)
  final Color medianGray = const Color.fromRGBO(125, 125, 139, 1.0);

  /// Hex: #C3C3C3
  ///
  /// RGB: (195,195,205)
  final Color lightGray = const Color.fromRGBO(195, 195, 205, 1.0);

  /// Hex: #
  ///
  /// RGB: (225,225,230)
  final Color white500 = const Color.fromRGBO(225, 225, 230, 1.0);

  /// Hex: #
  ///
  /// RGB: (133,133,133)
  final Color gray = const Color.fromRGBO(133, 133, 133, 1.0);

  /// Hex: #
  ///
  /// RGB: (235,25,95)
  final Color pink = const Color.fromRGBO(235, 25, 95, 1.0);

  /// Hex: #461D58
  ///
  /// RGB: (70, 29, 88)
  final Color purple = const Color.fromRGBO(70, 29, 88, 1.0);

  /// Hex: #05a0af
  ///
  /// RGB: (5,160,175)
  final Color turquoise = const Color.fromRGBO(5, 160, 175, 1.0);
}

class _Buttons {
  Widget Function({
    @required BuildContext context,
    @required Function() onPressed,
    @required String text,
    double height,
    double width,
    Color color,
  }) get flatButton => (
          {BuildContext context,
          Function() onPressed,
          String text,
          Color color,
          double height,
          double width}) {
        color = color ??= COMOO.colors.purple;
        final double mediaQuery = MediaQuery.of(context).size.width / 400;
        height = height != null ? height * mediaQuery : null;
        width = width != null ? width * mediaQuery : null;
        return Container(
          height: height ?? null,
          width: width ?? null,
          margin: EdgeInsets.symmetric(vertical: mediaQuery * 10.0),
          child: FlatButton(
            onPressed: onPressed,
            color: color,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
            child: FittedBox(
              child: Text(
                text,
                style: ComooStyles.botao_branco,
              ),
            ),
          ),
        );
      };
}
