import 'package:flutter/widgets.dart';

class ResponsiveDevice {
  /// Manage device responsiveness
  /// defaults to 360x640
  ResponsiveDevice(this.context)
      : _width = 360,
        _height = 640,
        _size = MediaQuery.of(context).size;
  ResponsiveDevice.custom(this.context,
      {@required double width, @required double height})
      : _width = width ?? 360,
        _height = height ?? 640,
        _size = MediaQuery.of(context).size;
  final BuildContext context;
  final double _width;
  final double _height;
  final Size _size;

  double height(double value) {
    final double proportion = value / _height;
    return proportion * _size.height;
  }

  double width(double value) {
    final double proportion = value / _width;
    return proportion * _size.width;
  }
}

// ignore: camel_case_types
class iPhone8PlusResponsive extends ResponsiveDevice {
  /// Inputs iPhone8 Plus dimensions to be easily managed
  /// Dimensions: 414x736
  iPhone8PlusResponsive(BuildContext context)
      : super.custom(context, width: 414, height: 736);
}

// ignore: camel_case_types
class iPhone8Responsive extends ResponsiveDevice {
  /// Inputs iPhone8 Plus dimensions to be easily managed
  /// Dimensions: 414x736
  iPhone8Responsive(BuildContext context)
      : super.custom(context, width: 375, height: 667);
}
