import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/blocs/group_message_feed_bloc.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/comments/group_message_comments.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

class GroupMessageFeedView extends StatelessWidget with Reusables {
  GroupMessageFeedView();
  @override
  Widget build(BuildContext context) {
    final GroupMessageFeedBloc bloc =
        BlocProvider.of<GroupMessageFeedBloc>(context);

    final String photoURL = bloc.groupMessage.user['picture'];
    final String userName = bloc.groupMessage.user['name'];
    final String _userId = bloc.groupMessage.user['id'];
    final DateTime date = bloc.groupMessage.date;
    return StreamBuilder<bool>(
        stream: bloc.isDeleted.stream,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          final bool isDeleted = snapshot.data ?? false;
          if (isDeleted) {
            return Container();
          }
          return Card(
            margin: EdgeInsets.only(bottom: 15.0),
            child: ListView(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              controller: ScrollController(),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    top: responsiveHeight(context, 14),
                    bottom: responsiveHeight(context, 10),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(
                            left: responsiveWidth(context, 15),
                            right: responsiveWidth(context, 10),
                          ),
                          width: responsiveWidth(context, 50),
                          height: responsiveWidth(context, 50),
                          child: ClipOval(
                            child: (photoURL?.isEmpty ?? true)
                                ? null
                                : Image(
                                    image: CachedNetworkImageProvider(photoURL),
                                    fit: BoxFit.cover,
                                  ),
                          ),
                        ),
                        // onTap: () => bloc.navigateToUserDetail(context),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                              // onTap: () => bloc.navigateToUserDetail(context),
                              child: Container(
                                child: RichText(
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(
                                    text: '${userName ?? ''}',
                                    style: ComooStyles.nomeFeed,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              child: RichText(
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                  text: date == null
                                      ? ''
                                      : timeago.format(date, locale: 'pt_BR'),
                                  style: ComooStyles.datas,
                                ),
                              ),
                            ),
                            SizedBox(height: responsiveHeight(context, 8)),
                            Container(
                              width: responsiveWidth(context, 282),
                              padding: EdgeInsets.symmetric(
                                vertical: responsiveHeight(context, 10),
                                horizontal: responsiveWidth(context, 10),
                              ),
                              decoration: BoxDecoration(
                                color: COMOO.colors.white500,
                                borderRadius: BorderRadius.circular(
                                    responsiveWidth(context, 15)),
                              ),
                              child: Text(
                                '${bloc.groupMessage.message ?? ''}',
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                  fontFamily: COMOO.fonts.montSerrat,
                                  fontSize: responsiveWidth(context, 13),
                                  color: COMOO.colors.lead,
                                  letterSpacing:
                                      responsiveWidth(context, -0.108),
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            _buildSocial(context),
                          ],
                        ),
                      ),
                      _userId == bloc.currentUser.uid
                          ? PopupMenuButton<String>(
                              offset: Offset(0, responsiveHeight(context, 30)),
                              onSelected: (String result) {
                                switch (result) {
                                  case 'delete':
                                    bloc.delete();
                                    break;
                                  default:
                                    break;
                                }
                              },
                              itemBuilder: (BuildContext context) =>
                                  <PopupMenuEntry<String>>[
                                const PopupMenuItem<String>(
                                  value: 'delete',
                                  child: const Text('Deletar publicação'),
                                ),
                              ],
                            )
                          : Container()
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildSocial(BuildContext context) {
    final GroupMessageFeedBloc bloc =
        BlocProvider.of<GroupMessageFeedBloc>(context);
    return StreamBuilder<bool>(
      stream: bloc.hasLiked.stream,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        final bool _liked = snapshot.data ?? false;
        return IconTheme(
          data: ComooIcons.roxo,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              IconButton(
                icon: Center(
                  child: Icon(
                    _liked ? Icons.favorite : ComooIcons.curtir,
                    size: responsiveWidth(context, 28),
                  ),
                ),
                iconSize: responsiveWidth(context, 28),
                alignment: Alignment.centerRight,
                onPressed: () => bloc.onLike(),
              ),
              Text(
                bloc.groupMessage.likes.toString(),
                style: ComooStyles.texto_roxo,
              ),
              IconButton(
                icon: Icon(
                  ComooIcons.comentar,
                  size: 28.0,
                ),
                alignment: Alignment.centerRight,
                onPressed: () => _navigateToComments(context),
              ),
              StreamBuilder<int>(
                  stream: bloc.commentsQty.stream,
                  builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                    String quantity = '';
                    if (snapshot.data != null) {
                      quantity = '${snapshot.data}';
                    }
                    return Text(
                      quantity,
                      style: ComooStyles.texto_roxo,
                    );
                  }),
              Expanded(
                child: Container(),
              ),
              Container(
                height: responsiveHeight(context, 48.0),
                padding: EdgeInsets.only(
                  left: responsiveWidth(context, 10.0),
                  right: responsiveWidth(context, 10.0),
                  bottom: responsiveHeight(context, 10.0),
                  top: responsiveHeight(context, 8.0),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Future<void> _navigateToComments(BuildContext context) async {
    final GroupMessageFeedBloc bloc =
        BlocProvider.of<GroupMessageFeedBloc>(context);
    Navigator.of(context).push<void>(MaterialPageRoute(
      builder: (BuildContext context) =>
          GroupMessageCommentsPage(bloc.groupMessage),
    ));
  }
}
