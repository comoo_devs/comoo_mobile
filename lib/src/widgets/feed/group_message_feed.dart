import 'package:comoo/src/blocs/group_message_feed_bloc.dart';
import 'package:comoo/src/comoo/group_message.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:flutter/material.dart';

import 'group_message_feed_view.dart';

class GroupMessageFeed extends StatefulWidget {
  GroupMessageFeed({@required this.groupMessage, Key key}) : super(key: key);
  final GroupMessage groupMessage;
  @override
  State<StatefulWidget> createState() => _GroupMessageFeedState();
}

class _GroupMessageFeedState extends State<GroupMessageFeed> {
  GroupMessageFeedBloc bloc;
  @override
  void initState() {
    super.initState();

    bloc = GroupMessageFeedBloc(widget.groupMessage);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GroupMessageFeedBloc>(
      bloc: bloc,
      child: GroupMessageFeedView(),
    );
  }
}
