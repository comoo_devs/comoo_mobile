// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:cloud_functions/cloud_functions.dart';
// import 'package:comoo/src/comoo/authentication.dart';
// import 'package:comoo/src/comoo/comoo.dart';
// import 'package:comoo/src/comoo/negotiation.dart';
// import 'package:comoo/src/comoo/product.dart';
// import 'package:comoo/src/comoo/user.dart';
// import 'package:comoo/src/pages/common/chat/direct_chat.dart';
// import 'package:comoo/src/pages/common/group/group_detail.dart';
// import 'package:comoo/src/pages/common/product/product_page.dart';
// import 'package:comoo/src/pages/common/product/product_republish.dart';
// import 'package:comoo/src/pages/common/user/user_detail.dart';
// import 'package:comoo/src/pages/home/bottom_menu/product_creation/product_create.dart';
// import 'package:comoo/src/utility/network.dart';
// import 'package:comoo/src/utility/style.dart';
// import 'package:comoo/src/widgets/comments/product_comments.dart';
// import 'package:comoo/src/widgets/feed/product_feed.dart';
// import 'package:comoo/src/widgets/loading_container.dart';
// import 'package:comoo/src/widgets/reusables.dart';
// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:rxdart/rxdart.dart';
// import 'package:timeago/timeago.dart' as timeago;

// class OldProductFeed extends StatelessWidget with Reusables {
//   OldProductFeed(Product product, {Key key})
//       : _product = product,
//         // liked = currentUser.likes.indexOf(product.id) == -1 ? false : true,
//         _liked = BehaviorSubject<bool>.seeded(
//             (currentUser.likes ?? <String>[]).indexOf(product.id) == -1
//                 ? false
//                 : true),
//         _likeCount = product.likes ?? 0,
//         super(key: key);
//   Product _product;
//   final Firestore db = Firestore.instance;
//   final NetworkApi api = NetworkApi();
//   final _controller = PageController();
//   static const _kDuration = const Duration(milliseconds: 300);
//   static const _kCurve = Curves.ease;
//   final bool loading = false;
//   final String _loadingMessage = 'Carregando...';
//   final NumberFormat formatter =
//       NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');
//   // bool liked;

//   BehaviorSubject<bool> _liked; // = BehaviorSubject<bool>.seeded(false);
//   Function(bool) get changeLike => _liked.sink.add;
//   Stream<bool> get liked => _liked.asBroadcastStream();
//   int _likeCount;
//   BehaviorSubject<bool> _loadingLike = BehaviorSubject<bool>.seeded(false);
//   Function(bool) get changeLoadingLike => _loadingLike.sink.add;
//   Stream<bool> get isLoadingLike => _loadingLike.asBroadcastStream();
//   BehaviorSubject<bool> _loading = BehaviorSubject<bool>.seeded(false);
//   Function(bool) get changeLoading => _loading.sink.add;
//   Stream<bool> get isLoading => _loading.asBroadcastStream();
//   BehaviorSubject<bool> _deleted = BehaviorSubject<bool>.seeded(false);
//   Function(bool) get changeDeleted => _deleted.sink.add;
//   Stream<bool> get isDeleted => _deleted.asBroadcastStream();

//   BehaviorSubject<bool> _userHasRequested = BehaviorSubject<bool>.seeded(true);
//   Function(bool) get changeRequested => _userHasRequested.sink.add;
//   Stream<bool> get userHasRequested => _userHasRequested.asBroadcastStream();
//   BehaviorSubject<bool> _loadingRequest = BehaviorSubject<bool>.seeded(false);
//   Function(bool) get changeRequestLoading => _loadingRequest.sink.add;
//   Stream<bool> get loadingRequest => _loadingRequest.asBroadcastStream();

//   @override
//   Widget build(BuildContext context) {
//     // print('${_product.id} ${_product.title} height: ${_product.height}');
//     // print('${_product.photos[0]}');
//     fetchUserRequest();
//     timeago.setLocaleMessages('pt_Br', timeago.PtBrMessages());
//     final double mediaQuery = MediaQuery.of(context).size.width / 400;
//     // return Card(
//     //   child: Container(
//     //     color: Colors.amber,
//     //     width: MediaQuery.of(context).size.width,
//     //     height: mediaQuery * _product.height,
//     //   ),
//     // );
//     return StreamBuilder<bool>(
//         initialData: false,
//         stream: isDeleted,
//         builder: (BuildContext context, AsyncSnapshot<bool> deletedSnapshot) {
//           return deletedSnapshot.data
//               ? Container()
//               : Card(
//                   margin: EdgeInsets.only(bottom: 5.0),
//                   child: Stack(
//                     children: <Widget>[
//                       Column(
//                         crossAxisAlignment: CrossAxisAlignment.stretch,
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           _sellerInfo(context),
//                           Container(
//                             padding: EdgeInsets.only(
//                               left: mediaQuery * 15.0,
//                               top: mediaQuery * 10.0,
//                               bottom: mediaQuery * 12.0,
//                               right: mediaQuery * 5.0,
//                             ),
//                             child: RichText(
//                               overflow: TextOverflow.ellipsis,
//                               text: TextSpan(
//                                   style: ComooStyles.produtoNome,
//                                   text: '${_product.title} ',
//                                   children: [
//                                     TextSpan(
//                                         style: ComooStyles.produtoDesc,
//                                         text: _product.description)
//                                   ]),
//                               maxLines: 10,
//                             ),
//                           ),
//                           // _productImage(context),
//                           // _product.photos.length <= 1
//                           //     ? Container(
//                           //         height: mediaQuery * 10.0,
//                           //       )
//                           //     : Container(
//                           //         height: mediaQuery * 30.0,
//                           //         color: Colors.white,
//                           //         padding: EdgeInsets.all(mediaQuery * 10.0),
//                           //         child: Center(
//                           //           child: DotsIndicator(
//                           //             controller: _controller,
//                           //             itemCount: _product.photos.length,
//                           //             onPageSelected: (int page) {
//                           //               _controller.animateToPage(
//                           //                 page ?? 0,
//                           //                 duration: _kDuration,
//                           //                 curve: _kCurve,
//                           //               );
//                           //             },
//                           //           ),
//                           //         ),
//                           //       ),
//                           _productInfo(context),
//                           _productActions(context),
//                         ],
//                       ),
//                       StreamBuilder<bool>(
//                         initialData: false,
//                         stream: isLoading,
//                         builder: (BuildContext context,
//                             AsyncSnapshot<bool> snapshot) {
//                           return snapshot.data
//                               ? Container(
//                                   child: LoadingContainer(
//                                       message: _loadingMessage))
//                               : Container();
//                         },
//                       ),
//                     ],
//                   ),
//                 );
//         });
//   }

//   Widget _sellerInfo(BuildContext context) {
//     final double mediaQuery = MediaQuery.of(context).size.width / 400;
//     final TextStyle menuItemStyle = TextStyle(
//       fontFamily: COMOO.fonts.montSerrat,
//       fontSize: mediaQuery * 15.0,
//       letterSpacing: mediaQuery * 0.21,
//     );
//     return Container(
//       padding: EdgeInsets.symmetric(horizontal: mediaQuery * 15.0),
//       child: Row(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.start,
//         children: <Widget>[
//           GestureDetector(
//             onTap: () {
//               _navigateToSellerDetail(context);
//             },
//             child: CachedNetworkImage(
//               imageUrl: _product.user['picture'],
//               fit: BoxFit.cover,
//               alignment: Alignment.center,
//               height: mediaQuery * 50.0,
//               width: mediaQuery * 50.0,
//               imageBuilder: (BuildContext context,
//                       ImageProvider<dynamic> imageProvider) =>
//                   Center(
//                     child: CircleAvatar(
//                       backgroundImage: imageProvider,
//                       radius: mediaQuery * 25.0,
//                       backgroundColor: Colors.white,
//                     ),
//                   ),
//               placeholder: (BuildContext context, String text) => Center(
//                     child: Container(), //CircularProgressIndicator(),
//                   ),
//             ),
//           ),
//           SizedBox(
//             width: mediaQuery * 10.0,
//           ),
//           Expanded(
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisAlignment: MainAxisAlignment.start,
//               children: <Widget>[
//                 RichText(
//                   text: TextSpan(
//                       text: _product.user['name'],
//                       style: ComooStyles.nomeFeed,
//                       recognizer: TapGestureRecognizer()
//                         ..onTap = () {
//                           _navigateToSellerDetail(context);
//                         }),
//                 ),
//                 Container(
//                   child: RichText(
//                     overflow: TextOverflow.ellipsis,
//                     maxLines: 2,
//                     text: TextSpan(
//                         text: '${_product.location ?? ''} ',
//                         style: ComooStyles.localizacao,
//                         children: <TextSpan>[
//                           TextSpan(
//                             text:
//                                 '${timeago.format(_product.date, locale: 'pt_Br')}',
//                             style: ComooStyles.datas,
//                           )
//                         ]),
//                   ),
//                 ),
//                 Container(
//                   height: mediaQuery * 20.0,
//                   child: ListView(
//                     scrollDirection: Axis.horizontal,
//                     children: _product.groups == null
//                         ? <Widget>[Container()]
//                         : _product.groups
//                             .map((dynamic g) => GestureDetector(
//                                   child: Padding(
//                                     padding: EdgeInsets.only(
//                                         right: mediaQuery * 6.0),
//                                     child: Text(
//                                       g['name'],
//                                       style: ComooStyles.localizacao,
//                                     ),
//                                   ),
//                                   onTap: () {
//                                     _navigateToGroupDetail(context, g['id']);
//                                   },
//                                 ))
//                             .toList(),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           _product.user['uid'] == currentUser.uid
//               ? _product.underNegotiation == false
//                   ? PopupMenuButton<ProductMenu>(
//                       onSelected: (ProductMenu result) {
//                         switch (result) {
//                           case ProductMenu.update:
//                             _updateProduct(context);
//                             break;
//                           case ProductMenu.republish:
//                             _republishProduct(context);
//                             break;
//                           case ProductMenu.delete:
//                             _deleteProduct(context);
//                         }
//                       },
//                       itemBuilder: (BuildContext context) =>
//                           <PopupMenuEntry<ProductMenu>>[
//                             PopupMenuItem<ProductMenu>(
//                               value: ProductMenu.update,
//                               child: Text(
//                                 'EDITAR ANÚNCIO',
//                                 style: menuItemStyle,
//                               ),
//                             ),
//                             PopupMenuItem<ProductMenu>(
//                               value: ProductMenu.republish,
//                               child: Text(
//                                 'REPOSTAR ANÚNCIO',
//                                 style: menuItemStyle,
//                               ),
//                             ),
//                             PopupMenuItem<ProductMenu>(
//                               value: ProductMenu.delete,
//                               child: Text(
//                                 'EXCLUIR ANÚNCIO',
//                                 style: menuItemStyle,
//                               ),
//                             ),
//                           ],
//                     )
//                   : Container()
//               : Container()
//         ],
//       ),
//     );
//   }

//   Widget _productImage(BuildContext context) {
//     final double width = MediaQuery.of(context).size.width;
//     final double mediaQuery = width / 400;
//     // final Key _productKey = Key('future#${_product.id}');
//     final double productWidth = _product.width ?? width;
//     final double ratio = width / productWidth;
//     // print('${_product.width}x${_product.height}');
//     // print('ratio: $ratio');
//     return StreamBuilder<bool>(
//         initialData: false,
//         stream: isLoadingLike,
//         builder: (BuildContext likeloadingContext, likeLoading) {
//           return GestureDetector(
//             onDoubleTap: likeLoading.data ? null : onLike,
//             onTap: () async {
//               await Navigator.of(context).push(MaterialPageRoute<void>(
//                   builder: (context) => ProductPage(_product)));
//               _fetchProduct();
//               fetchUserRequest();
//             },
//             child: Stack(
//               children: <Widget>[
//                 Container(
//                   constraints: BoxConstraints(
//                     maxHeight: _product.height == null
//                         ? 375.0
//                         : _product.height * ratio,
//                     maxWidth: MediaQuery.of(context).size.width,
//                   ),
//                   child: PageView.builder(
//                     controller: _controller,
//                     itemCount: _product.photos.length,
//                     physics: AlwaysScrollableScrollPhysics(),
//                     itemBuilder: (BuildContext context, int index) {
//                       return Container(
//                         child: CachedNetworkImage(
//                           imageUrl: _product.photos[index],
//                           fit: BoxFit.cover,
//                           // fadeInDuration: const Duration(milliseconds: 0),
//                           // fadeInCurve: Curves.linear,
//                           placeholder: (BuildContext context, String url) =>
//                               Center(
//                                 child: CircularProgressIndicator(),
//                               ),
//                           // CachedNetworkImage(
//                           //   imageUrl: _product.thumbs[index],
//                           //   fit: BoxFit.fitWidth,
//                           //   // fadeOutCurve: Curves.easeOutQuad,
//                           //   fadeOutDuration:
//                           //       const Duration(milliseconds: 0),
//                           //   placeholder: (BuildContext context,
//                           //           String thumbText) =>
//                           //       Center(
//                           //         child: CircularProgressIndicator(),
//                           //       ),
//                           // ),
//                           errorWidget: (BuildContext context, String url,
//                                   Object error) =>
//                               Center(
//                                 child: CircularProgressIndicator(),
//                               ),
//                         ),
//                       );
//                     },
//                   ),
//                 ),
//                 Positioned(
//                   right: mediaQuery * 20.0,
//                   top: mediaQuery * 20.0,
//                   child: Container(
//                     width: mediaQuery * 120.0,
//                     height: mediaQuery * 30.0,
//                     alignment: FractionalOffset.center,
//                     decoration: BoxDecoration(
//                         color: Colors.white,
//                         borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
//                     child: FittedBox(
//                       child: Text(
//                         formatter.format(_product.price),
//                         style: ComooStyles.productPrice,
//                         textAlign: TextAlign.center,
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           );
//         });
//   }

//   Widget _productInfo(BuildContext context) {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Container(
//       height: mediaQuery * 28.0,
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceAround,
//         children: <Widget>[
//           Row(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Icon(
//                 ComooIcons.marca,
//                 size: mediaQuery * 28.0,
//               ),
//               SizedBox(
//                 width: mediaQuery * 10.0,
//               ),
//               Text(
//                 _product.brand,
//                 overflow: TextOverflow.ellipsis,
//                 style: ComooStyles.product_tags,
//               ),
//             ],
//           ),
//           Row(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Icon(
//                 ComooIcons.tamanho,
//                 size: mediaQuery * 28.0,
//               ),
//               SizedBox(
//                 width: mediaQuery * 10.0,
//               ),
//               Text(
//                 _product.size,
//                 overflow: TextOverflow.ellipsis,
//                 style: ComooStyles.product_tags,
//               ),
//             ],
//           ),
//           Row(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Icon(
//                 ComooIcons.estado_produto,
//                 size: mediaQuery * 28.0,
//               ),
//               SizedBox(
//                 width: mediaQuery * 10.0,
//               ),
//               Text(_product.condition, style: ComooStyles.product_tags),
//             ],
//           ),
//         ],
//       ),
//     );
//   }

//   Widget _productActions(BuildContext context) {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Container(
//       height: mediaQuery * 58.0,
//       child: IconTheme(
//         data: ComooIcons.roxo,
//         child: StreamBuilder<bool>(
//           stream: liked,
//           builder: (BuildContext likeContext, AsyncSnapshot<bool> likeSnap) {
//             return Row(
//               mainAxisAlignment: MainAxisAlignment.start,
//               children: <Widget>[
//                 StreamBuilder<bool>(
//                   stream: isLoadingLike,
//                   initialData: false,
//                   builder:
//                       (BuildContext context, AsyncSnapshot<bool> loadingLike) {
//                     return IconButton(
//                       icon: Icon(
//                         likeSnap.data ?? false
//                             ? Icons.favorite
//                             : ComooIcons.curtir,
//                         size: 28.0,
//                       ),
//                       alignment: Alignment.centerRight,
//                       onPressed: loadingLike.data ? null : onLike,
//                     );
//                   },
//                 ),
//                 Text('$_likeCount', style: ComooStyles.texto_roxo),
//                 IconButton(
//                   icon: Icon(
//                     ComooIcons.comentar,
//                     size: mediaQuery * 28.0,
//                   ),
//                   alignment: Alignment.centerRight,
//                   onPressed: () {
//                     _navigateToComments(context);
//                   },
//                 ),
//                 Text(_product.commentsQty.toString(),
//                     style: ComooStyles.produto_social),
//                 Expanded(
//                   child: Container(),
//                 ),
//                 Container(
//                   height: mediaQuery * 48.0,
// //              width: mediaQuery * 76.0,
//                   padding: EdgeInsets.only(
//                     left: mediaQuery * 10.0,
//                     right: mediaQuery * 10.0,
//                     bottom: mediaQuery * 10.0,
//                     top: mediaQuery * 8.0,
//                   ),
//                   child: StreamBuilder<bool>(
//                     initialData: false,
//                     stream: loadingRequest,
//                     builder: (BuildContext context,
//                         AsyncSnapshot<bool> loadingSnap) {
//                       return StreamBuilder<bool>(
//                         initialData: true,
//                         stream: userHasRequested,
//                         builder: (BuildContext context,
//                             AsyncSnapshot<bool> requestedSnap) {
//                           return RaisedButton(
//                               elevation: 0.0,
//                               color: ComooColors.pink,
//                               child: Text('QUERO', style: ComooStyles.quero),
//                               shape: RoundedRectangleBorder(
//                                   borderRadius:
//                                       BorderRadius.circular(mediaQuery * 20.0)),
//                               onPressed: loadingSnap.data
//                                   ? null
//                                   : requestedSnap.data
//                                       ? null
//                                       : () {
//                                           requestProduct(context);
//                                         });
//                         },
//                       );
//                     },
//                   ),
//                 ),
//               ],
//             );
//           },
//         ),
//       ),
//     );
//   }

//   // Future<ui.Image> _fetchImageDimensions(String photo) async {
//   //   Image image = Image.network(photo);
//   //   Completer<ui.Image> completer = Completer<ui.Image>();
//   //   try {
//   //     image.image.resolve(ImageConfiguration()).addListener(
//   //         (ImageInfo info, bool _) => completer.complete(info.image));
//   //     return completer.future;
//   //   } catch (e) {
//   //     return null;
//   //   }
//   //   // .catchError((onError) {
//   //   //   print(onError);
//   //   //   return null;
//   //   // });
//   //   // print('${widget.product.id} ${snapshot.height}');
//   //   // height = snapshot.height * 1.0;
//   // }

//   Future<void> fetchUserRequest() async {
//     var _hasRequested = false;

//     if (currentUser.uid == _product.user['uid']) {
//       _hasRequested = true;
//     } else {
//       _hasRequested = await comoo.api
//           .fetchUserHasRequestedProduct(currentUser.uid, _product.id);
//     }
//     changeRequested(_hasRequested);
//     changeLike((currentUser.likes ?? <String>[]).indexOf(_product.id) == -1
//         ? false
//         : true);
//   }

//   void _showRequestAlert(BuildContext context) {
//     showCustomDialog<void>(
//       context: context,
//       barrierDismissible: false,
//       alert: AlertDialog(),
//       // alert: customRoundAlert(
//       //   context: context,
//       //   title: 'Requisição sendo enviada.\nAguarde um momento.',
//       //   actions: [],
//       // ),
//     );
//   }

//   Future<void> requestProduct(BuildContext context) async {
//     changeRequestLoading(true);
//     // final dynamic result = await comoo.api.requestProduct(widget.product.id);
//     final dynamic result = await CloudFunctions.instance
//         .call(
//             functionName: 'requestProduct',
//             parameters: <String, String>{'productId': _product.id})
//         .timeout(Duration(seconds: 30))
//         .catchError((onError) {
//           print(onError);
//           return null;
//         });

//     if (result == null) {
//       return showCustomDialog<void>(
//         context: context,
//         alert: customRoundAlert(
//           context: context,
//           title: 'Um erro inesperado aconteceu, tente novamente mais tarde.',
//         ),
//       );
//     }
//     if (result['isFirst'] == true) {
//       var _negotiationData = result['negotiation'];
//       await db
//           .collection('negotiations')
//           .document(_negotiationData['id'])
//           .get()
//           .then((snapshot) async {
//         changeRequestLoading(false);
//         changeRequested(true);
//         var negotiation = Negotiation.fromSnapshot(snapshot);
//         await Navigator.of(context).push(
//           MaterialPageRoute(builder: (context) => DirectChatView(negotiation)
//               // NegotiationPage(negotiation, goToChat: true),
//               ),
//         );
//       });
//       fetchUserRequest();
//     } else {
//       changeRequestLoading(false);
//       changeRequested(true);
//       var count = result['count'];
//       final double mediaQuery = MediaQuery.of(context).size.width / 400;
//       showDialog<void>(
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             contentPadding: EdgeInsets.all(mediaQuery * 0.0),
//             content: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: <Widget>[
//                 Container(
//                   height: mediaQuery * 186.0,
//                   width: mediaQuery * 329.0,
//                   decoration: BoxDecoration(
//                     color: ComooColors.pink,
//                     borderRadius: BorderRadius.all(
//                       Radius.circular(mediaQuery * 20.0),
//                     ),
//                   ),
//                   child: Center(
//                     child: Column(
//                       mainAxisSize: MainAxisSize.min,
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: <Widget>[
//                         Text(
//                           'Seu QUERO foi enviado!\r\nVocê é o $countº da fila',
//                           textAlign: TextAlign.center,
//                           style: TextStyle(
//                               color: Colors.white, fontSize: mediaQuery * 18.0),
//                         ),
//                         SizedBox(height: mediaQuery * 26.0),
//                         ButtonBar(
//                           mainAxisSize: MainAxisSize.min,
//                           alignment: MainAxisAlignment.center,
//                           children: <Widget>[
//                             Container(
//                               height: mediaQuery * 41.0,
//                               width: mediaQuery * 163.0,
//                               decoration: BoxDecoration(
//                                 border: Border.all(
//                                   width: 1.0,
//                                   color: Colors.white,
//                                 ),
//                                 borderRadius: BorderRadius.circular(20.0),
//                               ),
//                               child: FlatButton(
//                                 shape: RoundedRectangleBorder(
//                                   borderRadius: BorderRadius.circular(20.0),
//                                 ),
//                                 onPressed: () {
//                                   Navigator.of(context).pop();
//                                 },
//                                 child: Text(
//                                   'OK',
//                                   style: TextStyle(
//                                     color: Colors.white,
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ],
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           );
//         },
//       );
//     }
//   }

//   Future<void> onLike() async {
//     changeLoadingLike(true);
//     final bool like = _liked.value ?? false;
//     print(like);
//     if (!like) {
//       currentUser.addToLiked(_product.id);
//       _product.addLike().whenComplete(() => changeLoadingLike(false));
//       _likeCount++;
//     } else {
//       currentUser.removeToLiked(_product.id);
//       _product.removeLike().whenComplete(() => changeLoadingLike(false));
//       _likeCount--;
//     }
//     _fetchProduct();
//     changeLike(!like);
//   }

//   Future<void> _fetchProduct() async {
//     final Product newProduct = await api.getProduct(_product.id);
//     if (newProduct == null) {
//       changeDeleted(true);
//       return;
//     }
//     changeLoadingLike(true);
//     _product = newProduct;
//     changeLoadingLike(false);
//     // changeLikeCount(newProduct.likes);
//   }

//   void _updateProduct(BuildContext context) {
//     Navigator.of(context).push(
//       MaterialPageRoute<void>(
//           builder: (context) =>
//               ProductCreateView(product: _product, openCamera: false)),
//     );
//   }

//   void _republishProduct(BuildContext context) {
//     Navigator.of(context).push(
//       MaterialPageRoute<void>(
//           builder: (context) => ProductRepublishView(product: _product)),
//     );
//   }

//   Future<void> _deleteProduct(BuildContext context) async {
//     final double mediaQuery = MediaQuery.of(context).size.width / 400;
//     final bool willDelete = await showCustomDialog<bool>(
//       context: context,
//       barrierDismissible: false, // user must tap button!
//       alert: customRoundAlert(
//         context: context,
//         title: 'Tem certeza que deseja excluir o produto?',
//         actions: [
//           Container(
//             height: mediaQuery * 41.0,
//             decoration: BoxDecoration(
//               border: Border.all(
//                 width: 1.0,
//                 color: Colors.white,
//               ),
//               borderRadius: BorderRadius.circular(20.0),
//             ),
//             child: FlatButton(
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(20.0),
//               ),
//               child: Text(
//                 'CANCELAR',
//                 style: TextStyle(
//                   color: Colors.white,
//                 ),
//               ),
//               onPressed: () {
//                 Navigator.of(context).pop(false);
//               },
//             ),
//           ),
//           Container(
//             height: mediaQuery * 41.0,
//             decoration: BoxDecoration(
//               border: Border.all(
//                 width: 1.0,
//                 color: Colors.white,
//               ),
//               borderRadius: BorderRadius.circular(20.0),
//             ),
//             child: FlatButton(
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(20.0)),
//               child: Text(
//                 'EXCLUIR',
//                 style: TextStyle(
//                   color: Colors.white,
//                 ),
//               ),
//               onPressed: () async {
//                 Navigator.of(context).pop(true);
//               },
//             ),
//           ),
//         ],
//       ),
//     );

//     if (willDelete) {
//       changeLoading(true);

//       _product.delete().then((_) {
//         changeLoading(false);
//         changeDeleted(true);
//       }).catchError((onError) {
//         changeLoading(false);
//         changeDeleted(false);
//       });
//     }
//   }

//   Future<void> _navigateToSellerDetail(BuildContext context) async {
//     final DocumentSnapshot snap =
//         await db.collection('users').document(_product.user['uid']).get();
//     Navigator.of(context).push(MaterialPageRoute(
//       builder: (BuildContext context) =>
//           UserDetailView(User.fromSnapshot(snap)),
//     ));
//   }

//   void _navigateToGroupDetail(BuildContext context, String id) {
//     Navigator.of(context).push(MaterialPageRoute(
//         builder: (BuildContext context) => GroupDetailView(id: id)));
//   }

//   void _navigateToComments(BuildContext context) {
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//         builder: (context) => ProductCommentsView(_product),
//       ),
//     );
//   }

//   // void _testLoading(Function(bool) func) {
//   //   func(true);
//   //   Timer(Duration(seconds: 20), () {
//   //     func(false);
//   //   });
//   // }
// }

// /*
// class OldProductFeed extends StatefulWidget {
//   OldProductFeed(this.product, {Key key}) : super(key: key);

//   final Product product;

//   @override
//   State createState() {
//     return _ProductFeedState();
//   }
// }

// class _ProductFeedState extends State<OldProductFeed> with Reusables {
//   User _user;
//   final _controller = PageController();
//   static const _kDuration = const Duration(milliseconds: 300);

//   static const _kCurve = Curves.ease;
//   bool _loading = false;
//   bool userHasRequested = false;
//   String _loadingMessage = 'Carregando...';
//   bool liked = false;
//   final Firestore db = Firestore.instance;
//   bool deleted = false;
//   final NumberFormat formatter =
//       NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');

//   fetchUserRequest() async {
//     var _hasRequested = false;

//     if (currentUser.uid == widget.product.user['uid']) {
//       _hasRequested = true;
//     } else {
//       _hasRequested = await comoo.api
//           .fetchUserHasRequestedProduct(currentUser.uid, widget.product.id);
//     }
//     if (mounted) {
//       setState(() {
//         userHasRequested = _hasRequested;
//       });
//     }
//   }

//   Future<void> requestProduct() async {
//     setState(() {
//       _loadingMessage = 'Requisitando produto...';
//       _loading = true;
//     });
//     // final dynamic result = await comoo.api.requestProduct(widget.product.id);
//     final dynamic result = await CloudFunctions.instance
//         .call(
//             functionName: 'requestProduct',
//             parameters: {'productId': widget.product.id})
//         .timeout(Duration(seconds: 30))
//         .catchError((onError) {
//           print(onError);
//           return null;
//         });

//     Navigator.of(context).pop();
//     if (result == null) {
//       return showCustomDialog(
//         context: context,
//         alert: customRoundAlert(
//           context: context,
//           title: 'Um erro inesperado aconteceu, tente novamente mais tarde.',
//         ),
//       );
//     }
//     if (result['isFirst'] == true) {
//       var _negotiationData = result['negotiation'];
//       await db
//           .collection('negotiations')
//           .document(_negotiationData['id'])
//           .get()
//           .then((snapshot) async {
//         setState(() {
//           _loading = false;
//           userHasRequested = true;
//         });
//         var negotiation = Negotiation.fromSnapshot(snapshot);
//         await Navigator.of(context).push(
//           MaterialPageRoute(builder: (context) => DirectChatView(negotiation)
//               // NegotiationPage(negotiation, goToChat: true),
//               ),
//         );
//       });
//       fetchUserRequest();
//     } else {
//       setState(() {
//         _loading = false;
//         userHasRequested = true;
//       });
//       var count = result['count'];
//       final mediaQuery = MediaQuery.of(context).size.width / 400;
//       showDialog<void>(
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             contentPadding: EdgeInsets.all(mediaQuery * 0.0),
//             content: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: <Widget>[
//                 Container(
//                   height: mediaQuery * 186.0,
//                   width: mediaQuery * 329.0,
//                   decoration: BoxDecoration(
//                     color: ComooColors.pink,
//                     borderRadius: BorderRadius.all(
//                       Radius.circular(mediaQuery * 20.0),
//                     ),
//                   ),
//                   child: Center(
//                     child: Column(
//                       mainAxisSize: MainAxisSize.min,
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: <Widget>[
//                         Text(
//                           'Seu QUERO foi enviado!\r\nVocê é o $countº da fila',
//                           textAlign: TextAlign.center,
//                           style: TextStyle(
//                               color: Colors.white, fontSize: mediaQuery * 18.0),
//                         ),
//                         SizedBox(height: mediaQuery * 26.0),
//                         ButtonBar(
//                           mainAxisSize: MainAxisSize.min,
//                           alignment: MainAxisAlignment.center,
//                           children: <Widget>[
//                             Container(
//                               height: mediaQuery * 41.0,
//                               width: mediaQuery * 163.0,
//                               decoration: BoxDecoration(
//                                 border: Border.all(
//                                   width: 1.0,
//                                   color: Colors.white,
//                                 ),
//                                 borderRadius: BorderRadius.circular(20.0),
//                               ),
//                               child: FlatButton(
//                                 shape: RoundedRectangleBorder(
//                                   borderRadius: BorderRadius.circular(20.0),
//                                 ),
//                                 onPressed: () {
//                                   Navigator.of(context).pop();
//                                 },
//                                 child: Text(
//                                   'OK',
//                                   style: TextStyle(
//                                     color: Colors.white,
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ],
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           );
//         },
//       );
//     }
//   }

//   void _navigateToUserDetail(User user) {
//     Navigator.of(context).push(MaterialPageRoute(
//         builder: (BuildContext context) => UserDetailView(user)));
//   }

//   void _navigateToGroupDetail(String id) {
//     Navigator.of(context).push(MaterialPageRoute(
//         builder: (BuildContext context) => GroupDetailView(id: id)));
//   }

//   @override
//   void initState() {
//     super.initState();
//     fetchUserRequest();
//     _user = User(widget.product.user['uid'], widget.product.user['name'],
//         widget.product.user['picture']);
//     if (currentUser.likes != null)
//       liked = currentUser.likes.indexOf(widget.product.id) == -1 ? false : true;

//     // _fetchImageDimensions();
//   }

//   Future<ui.Image> _fetchImageDimensions(String photo) async {
//     Image image = Image.network(photo);
//     Completer<ui.Image> completer = Completer<ui.Image>();
//     image.image.resolve(ImageConfiguration()).addListener(
//         (ImageInfo info, bool _) => completer.complete(info.image));
//     return completer.future.catchError((onError) {
//       print(onError);
//       return null;
//     });
//     // print('${widget.product.id} ${snapshot.height}');
//     // height = snapshot.height * 1.0;
//   }

//   Widget _buildSellerRow() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     final TextStyle menuItemStyle = TextStyle(
//       fontFamily: COMOO.fonts.montSerrat,
//       fontSize: mediaQuery * 15.0,
//       letterSpacing: mediaQuery * 0.21,
//     );
//     return Padding(
//       padding: EdgeInsets.only(top: mediaQuery * 14.0),
//       child: Row(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.start,
//         children: <Widget>[
//           GestureDetector(
//             child: Container(
//               margin: EdgeInsets.only(
//                 left: mediaQuery * 15.0,
//                 right: mediaQuery * 10.0,
//               ),
//               width: mediaQuery * 50.0,
//               height: mediaQuery * 50.0,
//               child: ClipOval(
//                 child: Image(
//                   image: AdvancedNetworkImage(widget.product.user['picture'],
//                       useDiskCache: true),
//                   fit: BoxFit.cover,
//                 ),
//               ),
//             ),
//             onTap: () {
//               _navigateToUserDetail(_user);
//             },
//           ),
//           Expanded(
//             child: Column(
//               children: <Widget>[
//                 GestureDetector(
//                   onTap: () {
//                     _navigateToUserDetail(_user);
//                   },
//                   child: Container(
//                     child: Text(
//                       widget.product.user['name'],
//                       style: ComooStyles.nomeFeed,
//                     ),
//                   ),
//                 ),
//                 Container(
//                   child: RichText(
//                     overflow: TextOverflow.ellipsis,
//                     text: TextSpan(
//                         text: widget.product.location + ' ' ?? 'LOCAL ',
//                         style: ComooStyles.localizacao,
//                         children: <TextSpan>[
//                           TextSpan(
//                               text: timeago.format(widget.product.date,
//                                       locale: 'pt_BR') +
//                                   ' ',
//                               style: ComooStyles.datas),
//                         ]),
//                   ),
//                 ),
//                 Container(
//                   height: mediaQuery * 20.0,
//                   child: ListView(
//                       scrollDirection: Axis.horizontal,
//                       children: widget.product.groups == null
//                           ? [Container()]
//                           : widget.product.groups
//                               .map(
//                                 (g) => GestureDetector(
//                                       child: Padding(
//                                         padding: EdgeInsets.only(
//                                             right: mediaQuery * 8.0),
//                                         child: Text(
//                                           g['name'],
//                                           style: ComooStyles.localizacao,
//                                         ),
//                                       ),
//                                       onTap: () {
//                                         _navigateToGroupDetail(g['id']);
//                                       },
//                                     ),
//                               )
//                               .toList()),
//                 ),
//               ],
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisAlignment: MainAxisAlignment.start,
//             ),
//           ),
//           _user.uid == currentUser.uid
//               ? widget.product.underNegotiation == false
//                   ? PopupMenuButton<ProductMenu>(
//                       onSelected: (ProductMenu result) {
//                         switch (result) {
//                           case ProductMenu.update:
//                             _updateProduct();
//                             break;
//                           case ProductMenu.republish:
//                             _republishProduct();
//                             break;
//                           case ProductMenu.delete:
//                             _deleteProduct();
//                         }
//                       },
//                       itemBuilder: (BuildContext context) =>
//                           <PopupMenuEntry<ProductMenu>>[
//                             PopupMenuItem<ProductMenu>(
//                               value: ProductMenu.update,
//                               child: Text(
//                                 'EDITAR ANÚNCIO',
//                                 style: menuItemStyle,
//                               ),
//                             ),
//                             PopupMenuItem<ProductMenu>(
//                               value: ProductMenu.republish,
//                               child: Text(
//                                 'REPOSTAR ANÚNCIO',
//                                 style: menuItemStyle,
//                               ),
//                             ),
//                             PopupMenuItem<ProductMenu>(
//                               value: ProductMenu.delete,
//                               child: Text(
//                                 'EXCLUIR ANÚNCIO',
//                                 style: menuItemStyle,
//                               ),
//                             ),
//                           ],
//                     )
//                   : Container()
//               : Container()
//         ],
//       ),
//     );
//   }

//   Widget _buildPhotoRow() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     Key _productKey = Key('future#${widget.product.id}');

//     return Stack(
//       children: <Widget>[
//         // _child,
//         FutureBuilder<ui.Image>(
//           key: _productKey,
//           future: _fetchImageDimensions(widget.product.photos[0]),
//           builder: (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
//             double height = widget.product.height ?? 500.0;
//             if (snapshot.hasData) {
//               var ratio =
//                   snapshot.data.width / MediaQuery.of(context).size.width;
//               final double imageHeight = (snapshot.data.height / ratio);
//               height = imageHeight < height ? imageHeight : height;
//               widget.product.height = height;
//             }
//             return AnimatedContainer(
//               curve: Curves.easeIn,
//               duration: Duration(milliseconds: 300),
//               height: height,
//               child: Container(
//                 constraints: BoxConstraints(
//                   maxWidth: MediaQuery.of(context).size.width,
//                 ),
//                 // height: height,
//                 child: PageView.builder(
//                   controller: _controller,
//                   itemCount: widget.product.photos.length,
//                   physics: AlwaysScrollableScrollPhysics(),
//                   itemBuilder: (BuildContext context, int index) {
//                     return Container(
//                       child: CachedNetworkImage(
//                         imageUrl: widget.product.photos[index],
//                         fit: BoxFit.fitWidth,
//                         fadeInCurve: Curves.easeIn,
//                         placeholder: (BuildContext context, String photoText) =>
//                             CachedNetworkImage(
//                               imageUrl: widget.product.thumbs[index],
//                               fit: BoxFit.fitWidth,
//                               fadeOutCurve: Curves.easeOut,
//                               fadeOutDuration: const Duration(milliseconds: 1),
//                               placeholder:
//                                   (BuildContext context, String thumbText) =>
//                                       Center(
//                                         child: CircularProgressIndicator(),
//                                       ),
//                             ),
//                       ),
//                       // child: Image(
//                       //   image: CachedNetworkImageProvider(
//                       //       widget.product.photos[index]),
//                       //   fit: BoxFit.fitWidth,
//                       // ),
//                     );
//                   },
//                 ),
//               ),
//             );
//           },
//         ),
//         Positioned(
//           right: mediaQuery * 20.0,
//           top: mediaQuery * 20.0,
//           child: Container(
//             width: mediaQuery * 120.0,
//             height: mediaQuery * 30.0,
//             alignment: FractionalOffset.center,
//             decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
//             child: FittedBox(
//               child: Text(
//                 formatter.format(widget.product.price),
//                 style: ComooStyles.productPrice,
//                 textAlign: TextAlign.center,
//               ),
//             ),
//           ),
//         ),
//       ],
//     );
//   }

//   Widget _buildTags() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceAround,
//       children: <Widget>[
//         Row(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Icon(
//               ComooIcons.marca,
//               size: mediaQuery * 28.0,
//             ),
//             SizedBox(
//               width: mediaQuery * 10.0,
//             ),
//             Text(
//               widget.product.brand,
//               overflow: TextOverflow.ellipsis,
//               style: ComooStyles.product_tags,
//             ),
//           ],
//         ),
//         Row(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Icon(
//               ComooIcons.tamanho,
//               size: mediaQuery * 28.0,
//             ),
//             SizedBox(
//               width: mediaQuery * 10.0,
//             ),
//             Text(
//               widget.product.size,
//               overflow: TextOverflow.ellipsis,
//               style: ComooStyles.product_tags,
//             ),
//           ],
//         ),
//         Row(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Icon(
//               ComooIcons.estado_produto,
//               size: mediaQuery * 28.0,
//             ),
//             SizedBox(
//               width: mediaQuery * 10.0,
//             ),
//             Text(widget.product.condition, style: ComooStyles.product_tags),
//           ],
//         ),
//       ],
//     );
//   }

//   void _updateProduct() {
//     Navigator.of(context).push(MaterialPageRoute(
//         builder: (context) => ProductCreateView(
//               product: widget.product,
//               openCamera: false,
//             )));
//   }

//   void _republishProduct() {
//     Navigator.of(context).push(MaterialPageRoute(
//         builder: (context) => ProductRepublishView(
//               product: widget.product,
//             )));
//   }

//   Future<void> _deleteProduct() async {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     bool willDelete = await showCustomDialog<bool>(
//       context: context,
//       barrierDismissible: false, // user must tap button!
//       alert: customRoundAlert(
//         context: context,
//         title: 'Tem certeza que deseja excluir o produto?',
//         actions: [
//           Container(
//             height: mediaQuery * 41.0,
//             decoration: BoxDecoration(
//               border: Border.all(
//                 width: 1.0,
//                 color: Colors.white,
//               ),
//               borderRadius: BorderRadius.circular(20.0),
//             ),
//             child: FlatButton(
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(20.0),
//               ),
//               child: Text(
//                 'CANCELAR',
//                 style: TextStyle(
//                   color: Colors.white,
//                 ),
//               ),
//               onPressed: () {
//                 Navigator.of(context).pop(false);
//               },
//             ),
//           ),
//           Container(
//             height: mediaQuery * 41.0,
//             decoration: BoxDecoration(
//               border: Border.all(
//                 width: 1.0,
//                 color: Colors.white,
//               ),
//               borderRadius: BorderRadius.circular(20.0),
//             ),
//             child: FlatButton(
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(20.0)),
//               child: Text(
//                 'EXCLUIR',
//                 style: TextStyle(
//                   color: Colors.white,
//                 ),
//               ),
//               onPressed: () async {
//                 Navigator.of(context).pop(true);
//               },
//             ),
//           ),
//         ],
//       ),
//       // return AlertDialog(
//       //   title: Text('Deletear produto?'),
//       //   content: Text(
//       //       'Isso irá remover o produto de todos os grupos que foi publicado.'),
//       //   actions: <Widget>[
//       //     FlatButton(
//       //       child: Text('Cancelar'),
//       //       onPressed: () {
//       //         Navigator.of(context).pop(false);
//       //       },
//       //     ),
//       //     FlatButton(
//       //       child: Text('DELETAR'),
//       //       onPressed: () {
//       //         Navigator.of(context).pop(true);
//       //       },
//       //     ),
//       //   ],
//       // );
//     );

//     if (willDelete) {
//       setState(() {
//         _loading = true;
//         _loadingMessage = 'Apagando Publicação';
//       });

//       widget.product.delete().then((_) {
//         if (this.mounted) {
//           setState(() {
//             _loading = false;
//             deleted = true;
//           });
//         }
//       }).catchError((onError) {
//         // showCustomSnackbar(
//         //   content:
//         //       Text('Falha ao excluir produto, tente novamente mais tarde.'),
//         // );
//         if (this.mounted) {
//           setState(() {
//             _loading = false;
//             deleted = false;
//           });
//         }
//       });
//     }
//   }

//   onLike() {
//     setState(() {
//       if (!liked) {
//         currentUser.addToLiked(widget.product.id);
//         widget.product.addLike();
//       } else {
//         currentUser.removeToLiked(widget.product.id);
//         widget.product.removeLike();
//       }
//       liked = !liked;
//     });
//   }

//   Widget _buildSocialInteractionRow() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return IconTheme(
//         data: ComooIcons.roxo,
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: <Widget>[
//             IconButton(
//               icon: Icon(
//                 liked ? Icons.favorite : ComooIcons.curtir,
//                 size: 28.0,
//               ),
//               alignment: Alignment.centerRight,
//               onPressed: onLike,
//             ),
//             Text(widget.product.likes.toString(),
//                 style: ComooStyles.texto_roxo),
//             IconButton(
//               icon: Icon(
//                 ComooIcons.comentar,
//                 size: mediaQuery * 28.0,
//               ),
//               alignment: Alignment.centerRight,
//               onPressed: () {
//                 _showComments();
//               },
//             ),
//             Text(widget.product.commentsQty.toString(),
//                 style: ComooStyles.produto_social),
//             Expanded(
//               child: Container(),
//             ),
//             Container(
//               height: mediaQuery * 48.0,
// //              width: mediaQuery * 76.0,
//               padding: EdgeInsets.only(
//                 left: mediaQuery * 10.0,
//                 right: mediaQuery * 10.0,
//                 bottom: mediaQuery * 10.0,
//                 top: mediaQuery * 8.0,
//               ),
//               child: RaisedButton(
//                   elevation: 0.0,
//                   color: ComooColors.pink,
//                   child: Text('QUERO', style: ComooStyles.quero),
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
//                   onPressed: _loading
//                       ? null
//                       : userHasRequested
//                           ? null
//                           : () {
//                               _showRequestAlert();
//                               requestProduct();
//                             }),
//             )
//           ],
//         ));
//   }

//   _showRequestAlert() {
//     showCustomDialog(
//       context: context,
//       barrierDismissible: false,
//       alert: AlertDialog(),
//       // alert: customRoundAlert(
//       //   context: context,
//       //   title: 'Requisição sendo enviada.\nAguarde um momento.',
//       //   actions: [],
//       // ),
//     );
//   }

//   _showComments() {
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//         builder: (context) => ProductCommentsView(widget.product),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     if (deleted) return Container();

//     return Card(
//       margin: EdgeInsets.only(bottom: 5.0),
//       child: Stack(children: <Widget>[
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.stretch,
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: <Widget>[
//             _buildSellerRow(),
//             Container(
//               padding: EdgeInsets.only(
//                 left: mediaQuery * 15.0,
//                 top: mediaQuery * 10.0,
//                 bottom: mediaQuery * 12.0,
//                 right: mediaQuery * 5.0,
//               ),
//               child: RichText(
//                 overflow: TextOverflow.ellipsis,
//                 text: TextSpan(
//                     style: ComooStyles.produtoNome,
//                     text: '${widget.product.title} ',
//                     children: [
//                       TextSpan(
//                           style: ComooStyles.produtoDesc,
//                           text: widget.product.description)
//                     ]),
//                 maxLines: 10,
//               ),
//             ),
//             GestureDetector(
//               onDoubleTap: onLike,
//               onTap: () {
//                 // selectedProduct = widget.product;
//                 // Navigator.of(context).pushNamed('/product_page');
//                 Navigator.of(context).push(MaterialPageRoute(
//                     builder: (context) => ProductPage(widget.product)));

//                 fetchUserRequest();
//               },
//               child: _buildPhotoRow(),
//             ),
//             widget.product.photos.length <= 1
//                 ? Container(
//                     height: mediaQuery * 10.0,
//                   )
//                 : Container(
//                     height: mediaQuery * 30.0,
//                     color: Colors.white,
//                     padding: EdgeInsets.all(mediaQuery * 10.0),
//                     child: Center(
//                       // key: Key('dots#${widget.product.id}'),
//                       child: DotsIndicator(
//                         controller: _controller,
//                         itemCount: widget.product.photos.length,
//                         onPageSelected: (int page) {
//                           _controller.animateToPage(
//                             page ?? 0,
//                             duration: _kDuration,
//                             curve: _kCurve,
//                           );
//                         },
//                       ),
//                     ),
//                   ),
//             Container(
//               height: mediaQuery * 28.0,
//               child: _buildTags(),
//             ),
//             Container(
//                 height: mediaQuery * 58.0, child: _buildSocialInteractionRow()),
//           ],
//         ),
//         _loading
//             ? Container(
//                 // height: mediaQuery * 550.0,
//                 child: LoadingContainer(message: _loadingMessage))
//             : Container()
//       ]),
//     );
//   }
// }
// */
