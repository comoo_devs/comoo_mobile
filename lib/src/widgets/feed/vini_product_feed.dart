// import 'dart:async';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:comoo/src/comoo/authentication.dart';
// import 'package:comoo/src/comoo/product.dart';
// import 'package:comoo/src/comoo/user.dart';
// import 'package:comoo/src/pages/common/group/group_detail.dart';
// import 'package:comoo/src/pages/common/product/product_page.dart';
// import 'package:comoo/src/pages/common/transaction/negotiation_page.dart';
// import 'package:comoo/src/pages/common/user/user_detail.dart';
// import 'package:comoo/src/pages/home/bottom_menu/product_creation/product_create.dart';
// import 'package:comoo/src/utility/style.dart';
// import 'package:comoo/src/widgets/comments/product_comments.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_advanced_networkimage/provider.dart';
// import 'package:timeago/timeago.dart' as timeago;
// import 'package:comoo/src/comoo/negotiation.dart';
// import 'package:comoo/src/widgets/dots_indicator.dart';
// import 'dart:math';
// import 'package:intl/intl.dart';
// import 'package:cloud_functions/cloud_functions.dart';
// import './../loading_container.dart';
// import 'package:comoo/src/comoo/comoo.dart';

// enum ProductMenu { update, delete }

// class ViniProductFeed extends StatefulWidget {
//   final Product product;

//   ViniProductFeed(this.product, {Key key}) : super(key: key);

//   @override
//   State createState() {
//     return new _ProductFeedState();
//   }
// }

// class _ProductFeedState extends State<ViniProductFeed> {
//   User _user;
//   final _controller = new PageController();
//   static const _kDuration = const Duration(milliseconds: 300);

//   static const _kCurve = Curves.ease;
//   bool _loading = false;
//   bool userHasRequested = false;
//   String _loadingMessage = "Carregando...";
//   bool liked = false;
//   final Firestore db = Firestore.instance;
//   bool deleted = false;
//   final NumberFormat formatter =
//       NumberFormat.currency(locale: "pt_BR", symbol: "R\$");

//   fetchUserRequest() async {
//     var _hasRequested = false;

//     if (currentUser.uid == widget.product.user["uid"]) {
//       _hasRequested = true;
//     } else {
//       _hasRequested = await comoo.api
//           .fetchUserHasRequestedProduct(currentUser.uid, widget.product.id);
//     }
//     if (mounted) {
//       setState(() {
//         userHasRequested = _hasRequested;
//       });
//     }
//   }

//   requestProduct() async {
//     setState(() {
//       _loadingMessage = "Requisitando produto...";
//       _loading = true;
//     });
//     // final dynamic result = await comoo.api.requestProduct(widget.product.id);
//     final dynamic result = await CloudFunctions.instance.call(
//         functionName: "requestProduct",
//         parameters: {
//           "productId": widget.product.id
//         }).timeout(Duration(seconds: 30));

//     if (result["isFirst"] == true) {
//       var _negotiationData = result["negotiation"];
//       await db
//           .collection("negotiations")
//           .document(_negotiationData["id"])
//           .get()
//           .then((snapshot) async {
//         setState(() {
//           _loading = false;
//           userHasRequested = true;
//         });
//         var negotiation = new Negotiation.fromSnapshot(snapshot);
//         await Navigator.of(context).push(
//           new MaterialPageRoute(
//             builder: (context) =>
//                 new NegotiationPage(negotiation, goToChat: true),
//           ),
//         );
//         // Navigator.of(context).push(new MaterialPageRoute(
//         //     builder: (context) => new DirectChatView(negotiation)));
//       });
//       fetchUserRequest();
//     } else {
//       setState(() {
//         _loading = false;
//         userHasRequested = true;
//       });
//       var count = result["count"];
//       showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             // contentPadding: EdgeInsets.all(0.0),
//             content: Container(
//               child: Container(
//                 // padding: const EdgeInsets.all(21.0),
//                 child: new Text(
//                   "Seu QUERO foi enviado!\r\nVocê é o $countº da fila",
//                   style: ComooStyles.welcomeMessage,
//                 ),
//               ),
//             ),
//             actions: <Widget>[
//               FlatButton(
//                 child: Text("OK"),
//                 onPressed: () {
//                   Navigator.of(context).pop();
//                 },
//               )
//             ],
//           );
//         },
//       );
//     }
//   }

//   void _navigateToUserDetail(User user) {
//     Navigator.of(context).push(new MaterialPageRoute(
//         builder: (BuildContext context) => new UserDetailView(user)));
//   }

//   void _navigateToGroupDetail(String id) {
//     Navigator.of(context).push(new MaterialPageRoute(
//         builder: (BuildContext context) => new GroupDetailView(id: id)));
//   }

//   @override
//   void initState() {
//     super.initState();
//     fetchUserRequest();
//     _user = new User(widget.product.user["uid"], widget.product.user["name"],
//         widget.product.user["picture"]);
//     if (currentUser.likes != null)
//       liked = currentUser.likes.indexOf(widget.product.id) == -1 ? false : true;
//   }

//   Widget _buildSellerRow() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Padding(
//       padding: EdgeInsets.only(top: mediaQuery * 14.0),
//       child: new Row(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.start,
//         children: <Widget>[
//           new GestureDetector(
//             child: new Container(
//               margin: EdgeInsets.only(
//                 left: mediaQuery * 15.0,
//                 right: mediaQuery * 10.0,
//               ),
//               width: mediaQuery * 50.0,
//               height: mediaQuery * 50.0,
//               child: new ClipOval(
//                 child: Image(
//                   image: new AdvancedNetworkImage(
//                       widget.product.user["picture"],
//                       useDiskCache: true),
//                   fit: BoxFit.cover,
//                 ),
//               ),
//             ),
//             onTap: () {
//               _navigateToUserDetail(_user);
//             },
//           ),
//           new Expanded(
//             child: new Column(
//               children: <Widget>[
//                 new GestureDetector(
//                   onTap: () {
//                     _navigateToUserDetail(_user);
//                   },
//                   child: new Container(
//                     child: Text(
//                       widget.product.user["name"],
//                       style: ComooStyles.nomeFeed,
//                     ),
//                   ),
//                 ),
//                 new Container(
//                   child: new RichText(
//                     overflow: TextOverflow.ellipsis,
//                     text: new TextSpan(
//                         text: widget.product.location + " " ?? "LOCAL ",
//                         style: ComooStyles.localizacao,
//                         children: <TextSpan>[
//                           new TextSpan(
//                               text: timeago.format(widget.product.date,
//                                       locale: 'pt_BR') +
//                                   ' ',
//                               style: ComooStyles.datas),
//                         ]),
//                   ),
//                 ),
//                 new Container(
//                   height: mediaQuery * 20.0,
//                   child: new ListView(
//                       scrollDirection: Axis.horizontal,
//                       children: widget.product.groups == null
//                           ? [Container()]
//                           : widget.product.groups
//                               .map(
//                                 (g) => new GestureDetector(
//                                       child: Padding(
//                                         padding: EdgeInsets.only(
//                                             right: mediaQuery * 8.0),
//                                         child: new Text(
//                                           g["name"],
//                                           style: ComooStyles.localizacao,
//                                         ),
//                                       ),
//                                       onTap: () {
//                                         _navigateToGroupDetail(g["id"]);
//                                       },
//                                     ),
//                               )
//                               .toList()),
//                 ),
//               ],
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisAlignment: MainAxisAlignment.start,
//             ),
//           ),
//           _user.uid == currentUser.uid
//               ? new PopupMenuButton<ProductMenu>(
//                   onSelected: (ProductMenu result) {
//                     switch (result) {
//                       case ProductMenu.update:
//                         _updateProduct();
//                         break;
//                       case ProductMenu.delete:
//                         _deleteProduct();
//                     }
//                   },
//                   itemBuilder: (BuildContext context) =>
//                       <PopupMenuEntry<ProductMenu>>[
//                         const PopupMenuItem<ProductMenu>(
//                           value: ProductMenu.update,
//                           child: const Text('Editar produto'),
//                         ),
//                         const PopupMenuItem<ProductMenu>(
//                           value: ProductMenu.delete,
//                           child: const Text('Deletar produto'),
//                         ),
//                       ],
//                 )
//               : new Container()
//         ],
//       ),
//     );
//   }

//   Widget _buildPhotoRow() {
//     // final mediaQuery = MediaQuery.of(context).size.width / 400;
//     final double width = MediaQuery.of(context).size.width;
//     final double mediaQuery = width / 400;
//     // final Key _productKey = Key('future#${_product.id}');
//     final double productWidth = widget.product.width ?? width;
//     final double ratio = width / productWidth;
//     final double height =
//         widget.product.height == null ? width : widget.product.height * ratio;
//     Widget _child;
//     if (widget.product.photos.length <= 1) {
//       _child = Container(
//         constraints: BoxConstraints(maxHeight: mediaQuery * height),
//         // width: 475.0,
//         // height: 475.0,
//         decoration: BoxDecoration(
//             image: DecorationImage(
//                 image: AdvancedNetworkImage(widget.product.photos[0]),
//                 fit: BoxFit.cover)),
//         alignment: FractionalOffset.topCenter,
//         // child: new Image(
//         //     image: AdvancedNetworkImage(widget.product.photos[0],
//         //         useDiskCache: true),
//         //     fit: BoxFit.contain),
//       );
//     } else {
//       _child = Container(
//         constraints: BoxConstraints(maxHeight: mediaQuery * height),
//         child: new PageView.builder(
//             controller: _controller,
//             itemCount: widget.product.photos.length,
//             physics: AlwaysScrollableScrollPhysics(),
//             itemBuilder: (BuildContext context, int index) {
//               return new Container(
//                   child: new Image(
//                       image: AdvancedNetworkImage(widget.product.photos[index],
//                           useDiskCache: true),
//                       fit: BoxFit.fitWidth));
//             }),
//       );
//     }

//     return Stack(
//       children: <Widget>[
//         _child,
//         new Positioned(
//           right: mediaQuery * 20.0,
//           top: mediaQuery * 20.0,
//           child: new Container(
//             width: mediaQuery * 120.0,
//             height: mediaQuery * 30.0,
//             alignment: FractionalOffset.center,
//             decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
//             child: Text(
//               formatter.format(widget.product.price),
//               style: ComooStyles.productPrice,
//               textAlign: TextAlign.center,
//             ),
//           ),
//         ),
//       ],
//     );
//   }

//   Widget _buildTags() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return new Row(
//       mainAxisAlignment: MainAxisAlignment.spaceAround,
//       children: <Widget>[
//         new Row(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             new Icon(
//               ComooIcons.marca,
//               size: mediaQuery * 28.0,
//             ),
//             new SizedBox(
//               width: mediaQuery * 10.0,
//             ),
//             new Text(
//               widget.product.brand,
//               overflow: TextOverflow.ellipsis,
//               style: ComooStyles.product_tags,
//             ),
//           ],
//         ),
//         new Row(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             new Icon(
//               ComooIcons.tamanho,
//               size: mediaQuery * 28.0,
//             ),
//             new SizedBox(
//               width: mediaQuery * 10.0,
//             ),
//             new Text(
//               widget.product.size,
//               overflow: TextOverflow.ellipsis,
//               style: ComooStyles.product_tags,
//             ),
//           ],
//         ),
//         new Row(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             new Icon(
//               ComooIcons.estado_produto,
//               size: mediaQuery * 28.0,
//             ),
//             new SizedBox(
//               width: mediaQuery * 10.0,
//             ),
//             new Text(widget.product.condition, style: ComooStyles.product_tags),
//           ],
//         ),
//       ],
//     );
//   }

//   _updateProduct() {
//     Navigator.of(context).push(new MaterialPageRoute(
//         builder: (context) => new ProductCreateView(
//               product: widget.product,
//               openCamera: false,
//             )));
//   }

//   _deleteProduct() async {
//     bool willDelete = await showDialog<bool>(
//       context: context,
//       barrierDismissible: false, // user must tap button!
//       builder: (BuildContext context) {
//         return new AlertDialog(
//           title: new Text('Deletear produto?'),
//           content: new Text(
//               'Isso irá remover o produto de todos os grupos que foi publicado.'),
//           actions: <Widget>[
//             new FlatButton(
//               child: new Text('Cancelar'),
//               onPressed: () {
//                 Navigator.of(context).pop(false);
//               },
//             ),
//             new FlatButton(
//               child: new Text('DELETAR'),
//               onPressed: () {
//                 Navigator.of(context).pop(true);
//               },
//             ),
//           ],
//         );
//       },
//     );

//     if (willDelete) {
//       setState(() {
//         _loading = true;
//         _loadingMessage = "Apagando Publicação";
//       });

//       widget.product.delete().then((_) {
//         if (this.mounted) {
//           setState(() {
//             _loading = false;
//             deleted = true;
//           });
//         }
//       });
//     }
//   }

//   onLike() {
//     setState(() {
//       if (!liked) {
//         currentUser.addToLiked(widget.product.id);
//         widget.product.addLike();
//       } else {
//         currentUser.removeToLiked(widget.product.id);
//         widget.product.removeLike();
//       }
//       liked = !liked;
//     });
//   }

//   Widget _buildSocialInteractionRow() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return new IconTheme(
//         data: ComooIcons.roxo,
//         child: new Row(
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: <Widget>[
//             new IconButton(
//               icon: new Icon(
//                 liked ? Icons.favorite : ComooIcons.curtir,
//                 size: 28.0,
//               ),
//               alignment: Alignment.centerRight,
//               onPressed: onLike,
//             ),
//             new Text(widget.product.likes.toString(),
//                 style: ComooStyles.texto_roxo),
//             new IconButton(
//               icon: new Icon(
//                 ComooIcons.comentar,
//                 size: mediaQuery * 28.0,
//               ),
//               alignment: Alignment.centerRight,
//               onPressed: () {
//                 _showComments();
//               },
//             ),
//             new Text(widget.product.commentsQty.toString(),
//                 style: ComooStyles.produto_social),
//             new Expanded(
//               child: new Container(),
//             ),
//             new Container(
//               height: mediaQuery * 48.0,
// //              width: mediaQuery * 76.0,
//               padding: EdgeInsets.only(
//                 left: mediaQuery * 10.0,
//                 right: mediaQuery * 10.0,
//                 bottom: mediaQuery * 10.0,
//                 top: mediaQuery * 8.0,
//               ),
//               child: new RaisedButton(
//                   elevation: 0.0,
//                   color: ComooColors.pink,
//                   child: Text("QUERO", style: ComooStyles.quero),
//                   shape: new RoundedRectangleBorder(
//                       borderRadius:
//                           new BorderRadius.circular(mediaQuery * 20.0)),
//                   onPressed: userHasRequested
//                       ? null
//                       : () {
//                           requestProduct();
//                         }),
//             )
//           ],
//         ));
//   }

//   _showComments() {
//     Navigator.push(
//       context,
//       new MaterialPageRoute(
//         builder: (context) => new ProductCommentsView(widget.product),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     if (deleted) return new Container();

//     return new Card(
//       margin: EdgeInsets.only(bottom: 5.0),
//       child: Stack(children: <Widget>[
//         new Column(
//           crossAxisAlignment: CrossAxisAlignment.stretch,
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: <Widget>[
//             _buildSellerRow(),
//             new Container(
//               padding: EdgeInsets.only(
//                 left: mediaQuery * 15.0,
//                 top: mediaQuery * 10.0,
//                 bottom: mediaQuery * 12.0,
//               ),
//               child: new RichText(
//                 overflow: TextOverflow.ellipsis,
//                 text: new TextSpan(
//                     style: ComooStyles.produtoNome,
//                     text: widget.product.title + ' ',
//                     children: [
//                       new TextSpan(
//                           style: ComooStyles.produtoDesc,
//                           text: widget.product.description)
//                     ]),
//                 maxLines: 3,
//               ),
//             ),
//             GestureDetector(
//               onDoubleTap: onLike,
//               onTap: () {
//                 // selectedProduct = widget.product;
//                 // Navigator.of(context).pushNamed("/product_page");
//                 Navigator.of(context).push(new MaterialPageRoute(
//                     builder: (context) => new ProductPage(widget.product)));

//                 fetchUserRequest();
//               },
//               child: _buildPhotoRow(),
//             ),
//             widget.product.photos.length <= 1
//                 ? new Container(
//                     height: mediaQuery * 10.0,
//                   )
//                 : new Container(
//                     height: mediaQuery * 30.0,
//                     color: Colors.white,
//                     padding: EdgeInsets.all(mediaQuery * 10.0),
//                     child: new Center(
//                       child: new DotsIndicator(
//                         controller: _controller,
//                         itemCount: widget.product.photos.length,
//                         onPageSelected: (int page) {
//                           _controller.animateToPage(
//                             page,
//                             duration: _kDuration,
//                             curve: _kCurve,
//                           );
//                         },
//                       ),
//                     ),
//                   ),
//             new Container(
//               height: mediaQuery * 28.0,
//               child: _buildTags(),
//             ),
//             Container(
//                 height: mediaQuery * 58.0, child: _buildSocialInteractionRow()),
//           ],
//         ),
//         _loading
//             ? new Container(
//                 height: mediaQuery * 550.0,
//                 child: LoadingContainer(message: _loadingMessage))
//             : new Container()
//       ]),
//     );
//   }
// }
