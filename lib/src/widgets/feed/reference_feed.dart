import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/message.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:timeago/timeago.dart' as timeago;

import './../comments/reference_comments.dart';

class ReferenceFeed extends StatefulWidget {
  final Message message;
  final User user;
  final String groupId;

  ReferenceFeed(this.message, this.user, this.groupId);

  @override
  State createState() {
    return new _ReferenceFeedState(this.message, this.user, this.groupId);
  }
}

class _ReferenceFeedState extends State<ReferenceFeed> {
  final Message message;
  final User user;
  final String groupId;

  _ReferenceFeedState(this.message, this.user, this.groupId);

  void _navigateToUserDetail(User user) {
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new UserDetailView(user)));
  }

  _showComments() {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new ReferenceComments(message.id, groupId),
      ),
    );
  }

  Widget _buildUserRow() {
    return new Row(
      children: <Widget>[
        new GestureDetector(
          child: new Container(
            margin: const EdgeInsets.only(right: 16.0),
            width: 40.0,
            height: 40.0,
            child: new ClipOval(
              child: new Image(
                image: AdvancedNetworkImage(user.photoURL),
                fit: BoxFit.contain,
              ),
            ),
          ),
          onTap: () {
            _navigateToUserDetail(user);
          },
        ),
        new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new GestureDetector(
              onTap: () {
                _navigateToUserDetail(user);
              },
              child: new Container(
                child: new RichText(
                  overflow: TextOverflow.ellipsis,
                  text: new TextSpan(
                      text: user.name + ' ',
                      style: ComooStyles.nomes,
                      children: <TextSpan>[
                        new TextSpan(
                            text: timeago.format(this.message.date,
                                locale: 'pt_BR'),
                            style: ComooStyles.texto),
                      ]),
                ),
              ),
            ),
            new Text("Está procurando indicações.")
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Card(
      margin: const EdgeInsets.only(bottom: 10.0),
      child: new Column(
        children: <Widget>[
          _buildUserRow(),
          new Container(
            height: 200.0,
            decoration: BoxDecoration(
                gradient: new LinearGradient(
                    colors: [ComooColors.roxo, ComooColors.pink])
//              color: ComooColors.roxo
                ),
            alignment: Alignment.center,
            child: Text(
              this.message.text,
              style: ComooStyles.indicacao,
            ),
          ),
          new Padding(
            padding: const EdgeInsets.only(bottom: 5.0),
            child: new FlatButton(
                onPressed: () {},
                child: new FlatButton(
                  child: new Text("ver todos comentários"),
                  onPressed: () {
                    _showComments();
                  },
                )),
          ),
        ],
      ),
    );
  }
}
