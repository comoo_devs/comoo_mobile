// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:comoo/src/utility/style.dart';
// import 'package:comoo/src/widgets/reusables.dart';
// import 'package:flutter/material.dart';
// import 'package:timeago/timeago.dart' as timeago;

// class FeedHeader extends StatelessWidget with Reusables {
//   FeedHeader({
//     @required this.photoURL,
//     @required this.userName,
//     this.date,
//   });
//   final String photoURL;
//   final String userName;
//   final DateTime date;
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: EdgeInsets.only(
//         top: responsiveHeight(context, 14),
//         bottom: responsiveHeight(context, 10),
//       ),
//       child: Row(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.start,
//         children: <Widget>[
//           GestureDetector(
//             child: Container(
//               margin: EdgeInsets.only(
//                 left: responsiveWidth(context, 15),
//                 right: responsiveWidth(context, 10),
//               ),
//               width: responsiveWidth(context, 50),
//               height: responsiveWidth(context, 50),
//               child: ClipOval(
//                 child: (photoURL?.isEmpty ?? true)
//                     ? null
//                     : Image(
//                         image: CachedNetworkImageProvider(photoURL),
//                         fit: BoxFit.cover,
//                       ),
//               ),
//             ),
//             // onTap: () => _navigateToUserDetail(),
//           ),
//           Expanded(
//             child: Column(
//               children: <Widget>[
//                 GestureDetector(
//                   onTap: () {
//                     // _navigateToUserDetail();
//                   },
//                   child: Container(
//                     child: RichText(
//                       overflow: TextOverflow.ellipsis,
//                       text: TextSpan(
//                         text: '${userName ?? ''}',
//                         style: ComooStyles.nomeFeed,
//                       ),
//                     ),
//                   ),
//                 ),
//                 Container(
//                   child: RichText(
//                     overflow: TextOverflow.ellipsis,
//                     text: TextSpan(
//                       text: date == null
//                           ? ''
//                           : timeago.format(date, locale: 'pt_BR'),
//                       style: ComooStyles.datas,
//                     ),
//                   ),
//                 ),
//                 // Container(
//                 //   height: 20.0,
//                 //   child: ListView(
//                 //       scrollDirection: Axis.horizontal,
//                 //       children: widget.post.groups
//                 //           .map(
//                 //             (g) => GestureDetector(
//                 //               child: Padding(
//                 //                 padding: const EdgeInsets.only(right: 8.0),
//                 //                 child: Text(
//                 //                   g['name'],
//                 //                   style: ComooStyles.localizacao,
//                 //                 ),
//                 //               ),
//                 //               onTap: () {
//                 //                 // _navigateToGroupDetail(g);
//                 //               },
//                 //             ),
//                 //           )
//                 //           .toList()),
//                 // ),
//               ],
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisAlignment: MainAxisAlignment.start,
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   //   return Padding(
//   //     padding: const EdgeInsets.only(top: 14.0, bottom: 10.0),
//   //     child: Row(
//   //       crossAxisAlignment: CrossAxisAlignment.start,
//   //       mainAxisAlignment: MainAxisAlignment.start,
//   //       children: <Widget>[
//   //         Expanded(
//   //           child: Column(
//   //             children: <Widget>[
//   //               GestureDetector(
//   //                 onTap: () {
//   //                   _navigateToUserDetail();
//   //                 },
//   //                 child: Container(
//   //                   child: RichText(
//   //                     overflow: TextOverflow.ellipsis,
//   //                     text: TextSpan(
//   //                       text: widget.post.user['name'] + ' ',
//   //                       style: ComooStyles.nomeFeed,
//   //                     ),
//   //                   ),
//   //                 ),
//   //               ),
//   //               Container(
//   //                 child: RichText(
//   //                   overflow: TextOverflow.ellipsis,
//   //                   text: TextSpan(
//   //                     text: timeago.format(widget.post.date, locale: 'pt_BR'),
//   //                     style: ComooStyles.datas,
//   //                   ),
//   //                 ),
//   //               ),
//   //               Container(
//   //                 height: 20.0,
//   //                 child: ListView(
//   //                     scrollDirection: Axis.horizontal,
//   //                     children: widget.post.groups
//   //                         .map(
//   //                           (g) => GestureDetector(
//   //                                 child: Padding(
//   //                                   padding: const EdgeInsets.only(right: 8.0),
//   //                                   child: Text(
//   //                                     g['name'],
//   //                                     style: ComooStyles.localizacao,
//   //                                   ),
//   //                                 ),
//   //                                 onTap: () {
//   //                                   _navigateToGroupDetail(g);
//   //                                 },
//   //                               ),
//   //                         )
//   //                         .toList()),
//   //               ),
//   //             ],
//   //             crossAxisAlignment: CrossAxisAlignment.start,
//   //             mainAxisAlignment: MainAxisAlignment.start,
//   //           ),
//   //         ),
//   //         _user.uid == currentUser.uid
//   //             ? PopupMenuButton<OwnerMenu>(
//   //                 onSelected: (OwnerMenu result) {
//   //                   switch (result) {
//   //                     // case OwnerMenu.update:
//   //                     //   _update();
//   //                     //   break;
//   //                     case OwnerMenu.delete:
//   //                       _delete();
//   //                       break;
//   //                     default:
//   //                       break;
//   //                   }
//   //                 },
//   //                 itemBuilder: (BuildContext context) =>
//   //                     <PopupMenuEntry<OwnerMenu>>[
//   //                       // const PopupMenuItem<OwnerMenu>(
//   //                       //   value: OwnerMenu.update,
//   //                       //   child: const Text('Editar publicação'),
//   //                       // ),
//   //                       const PopupMenuItem<OwnerMenu>(
//   //                         value: OwnerMenu.delete,
//   //                         child: const Text('Deletar publicação'),
//   //                       ),
//   //                     ],
//   //               )
//   //             : Container()
//   //       ],
//   //     ),
//   //   );
// }
