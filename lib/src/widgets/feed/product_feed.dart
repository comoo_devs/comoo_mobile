import 'dart:async';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/product_bloc.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/comments.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/chat/direct_chat.dart';
import 'package:comoo/src/pages/common/group/group_detail.dart';
import 'package:comoo/src/pages/common/product/product_image_page.dart';
import 'package:comoo/src/pages/common/product/product_page.dart';
import 'package:comoo/src/pages/common/product/product_republish.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:comoo/src/pages/home/bottom_menu/product_creation/product_create.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/comments/product_comments.dart';
import 'package:comoo/src/widgets/custom_cache_manager.dart';
import 'package:comoo/src/widgets/custom_tooltip.dart';
import 'package:comoo/src/widgets/dots_indicator.dart';
import 'package:comoo/src/widgets/post_user_info_row.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:pinch_zoom_image/pinch_zoom_image.dart';

import './../loading_container.dart';

enum ProductMenu { update, delete, republish }

class ProductFeed extends StatefulWidget {
  ProductFeed(this.product, {Key key, this.callback})
      : super(key: key ?? Key('autokey@productfeed/${product.id}'));
  final Product product;
  final VoidCallback callback;
  @override
  State<StatefulWidget> createState() => _ProductFeedState();
}

class _ProductFeedState extends State<ProductFeed> {
  ProductBloc bloc;
  @override
  void initState() {
    super.initState();
    timeago.setLocaleMessages('pt_BR', timeago.PtBrMessages());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = ProductBloc.seeded(widget.product);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProductBloc>(
      bloc: bloc,
      child: ProductFeedStateless(callback: widget.callback),
    );
  }
}

class ProductFeedStateless extends StatelessWidget with Reusables {
  ProductFeedStateless({this.callback});
  final Firestore _db = Firestore.instance;
  final Function callback;
  final NumberFormat formatter =
      NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;

  @override
  Widget build(BuildContext context) {
    final ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
    final Size size = MediaQuery.of(context).size;
    final Product product = bloc.initialProduct;
    // print('building ${product.title} ${++count} times');
    // final double productHeight = product?.height ?? size.width;
    final double productWidth = bloc?.initialProduct?.height ?? size.width;
    final double ratio = size.width / productWidth;
    // final double height = 70 + 100.0 + (ratio * productHeight);
    // print('preferred: ${size.width}x${size.width * 2.5}  -  actual: ${size.width}x$height');
    // print('ratio: $ratio');
    // print('product width: $productWidth');
    return StreamBuilder<bool>(
        stream: bloc.isDeleted,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.data ?? false) {
            return Container();
          }
          return Stack(
            children: <Widget>[
              Card(
                margin: EdgeInsets.only(bottom: 15.0),
                // elevation: 5.0,
                child: AnimatedContainer(
                  curve: Curves.easeIn,
                  duration: const Duration(milliseconds: 200),
                  child: _buildProductInfo(context, product, ratio),
                ),
              ),
              Positioned(
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                child: StreamBuilder<bool>(
                  stream: bloc.isLoading,
                  builder:
                      (BuildContext context, AsyncSnapshot<bool> snapshot) {
                    final bool _loading = snapshot.data ?? false;
                    return _loading
                        ? LoadingContainer(message: 'Carregando...')
                        : Container();
                  },
                ),
              ),
            ],
          );
        });
  }

  Widget _buildProductInfo(
      BuildContext context, Product product, double ratio) {
    final ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
    // print('${DateTime.now()} rebuilded ${product.title}');
    return Column(
      // shrinkWrap: true,
      // physics: NeverScrollableScrollPhysics(),
      // scrollDirection: Axis.vertical,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        // Container(
        //   constraints: BoxConstraints(
        //     maxWidth: MediaQuery.of(context).size.width,
        //     maxHeight: responsive(context, 70),
        //   ),
        //   child: SizedBox.expand(
        //     child: _sellerInfo(context, product),
        //   ),
        // ),
        _sellerInfo(context, product),
        Container(
          padding: EdgeInsets.only(
            left: responsive(context, 15.0),
            top: responsive(context, 10.0),
            bottom: responsive(context, 12.0),
            right: responsive(context, 5.0),
          ),
          alignment: Alignment.centerLeft,
          child: CustomToolTip(
            text: '${product.title} ${product.description}',
            child: RichText(
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.left,
              text: TextSpan(
                style: ComooStyles.produtoNome,
                text: '${product.title} ',
                children: [
                  TextSpan(
                    style: ComooStyles.produtoDesc,
                    text: product.description,
                  ),
                ],
              ),
              maxLines: 3,
            ),
          ),
        ),
        Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: product.height * ratio,
          ),
          // color: Colors.redAccent,
          child: _productImage(context, bloc, product),
        ),
        product.photos.length <= 1
            ? Container(
                height: responsive(context, 10.0),
              )
            : Container(
                height: responsive(context, 30.0),
                color: Colors.white,
                padding: EdgeInsets.all(responsive(context, 10.0)),
                child: Center(
                  child: DotsIndicator(
                    controller: bloc.photoController,
                    itemCount: product.photos.length,
                    onPageSelected: (int page) {
                      bloc.photoController.animateToPage(
                        page ?? 0,
                        duration: _kDuration,
                        curve: _kCurve,
                      );
                    },
                  ),
                ),
              ),

        Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: responsive(context, 35),
          ),
          child: SizedBox.expand(
            child: _productInfo(context, product),
          ),
        ),
        _productActions(context, bloc, product),
        // AnimatedContainer(
        //   key: Key('${product.id}_last_comment_animated_container'),
        //   curve: Curves.easeIn,
        //   duration: const Duration(milliseconds: 200),
        //   child:
        StreamBuilder<Comment>(
          key: Key('${product.id}_last_comment'),
          stream: bloc.lastComment,
          builder: (BuildContext context, AsyncSnapshot<Comment> snapshot) {
            if (snapshot.hasData) {
              final Comment comment = snapshot.data;
              return Container(
                // margin: EdgeInsets.only(bottom: responsive(context, 5.0)),
                child: ListTile(
                  dense: true,
                  // key: GlobalObjectKey(index),
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(comment.user.photoURL),
                  ),
                  title: RichText(
                      text: TextSpan(
                          text: '${comment.user.name} ',
                          style: ComooStyles.texto_bold,
                          children: <TextSpan>[
                        TextSpan(
                          style: ComooStyles.datas,
                          text: timeago.format(comment.date, locale: 'pt_BR'),
                        )
                      ])),
                  subtitle: Text(
                    comment.text,
                    style: ComooStyles.produtoDesc,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              );
            }
            return Container();
          },
        ),
        // ),
      ],
    );
  }
  // final ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
  // print('builded ${bloc.initialProduct.title} ${++bloc.times} times');
  // return Container(
  //   margin: EdgeInsets.only(bottom: responsive(context, 10)),
  //   child: StreamBuilder<Product>(
  //     stream: bloc.product,
  //     builder: (BuildContext context, AsyncSnapshot<Product> snap) {
  //       if (snap.hasData) {
  //         final Product product = snap.data;
  //         return Card(
  //           margin: EdgeInsets.only(bottom: 10.0),
  //           elevation: 5.0,
  //           child: PreferredSize(
  //             preferredSize: Size(MediaQuery.of(context).size.width,
  //                 responsive(context, 70 + product.height + 100.0)),
  //             child: Container(
  //               child: Stack(
  //                 children: <Widget>[
  //                   Column(
  //                     crossAxisAlignment: CrossAxisAlignment.stretch,
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: <Widget>[
  //                       _sellerInfo(context, product),
  //                       Container(
  //                         padding: EdgeInsets.only(
  //                           left: responsive(context, 15.0),
  //                           top: responsive(context, 10.0),
  //                           bottom: responsive(context, 12.0),
  //                           right: responsive(context, 5.0),
  //                         ),
  //                         child: RichText(
  //                           overflow: TextOverflow.ellipsis,
  //                           text: TextSpan(
  //                               style: ComooStyles.produtoNome,
  //                               text: '${product.title} ',
  //                               children: [
  //                                 TextSpan(
  //                                     style: ComooStyles.produtoDesc,
  //                                     text: product.description)
  //                               ]),
  //                           maxLines: 10,
  //                         ),
  //                       ),
  //                       // _productImage(context, bloc, product),
  //                       // product.photos.length <= 1
  //                       //     ? Container(
  //                       //         height: responsive(context, 10.0),
  //                       //       )
  //                       //     : Container(
  //                       //         height: responsive(context, 30.0),
  //                       //         color: Colors.white,
  //                       //         padding:
  //                       //             EdgeInsets.all(responsive(context, 10.0)),
  //                       //         child: Center(
  //                       //           child: DotsIndicator(
  //                       //             controller: bloc.photoController,
  //                       //             itemCount: product.photos.length,
  //                       //             onPageSelected: (int page) {
  //                       //               bloc.photoController.animateToPage(
  //                       //                 page ?? 0,
  //                       //                 duration: _kDuration,
  //                       //                 curve: _kCurve,
  //                       //               );
  //                       //             },
  //                       //           ),
  //                       //         ),
  //                       //       ),
  //                       _productInfo(context, product),
  //                       _productActions(context, bloc, product),
  //                       AnimatedContainer(
  //                         curve: Curves.easeIn,
  //                         duration: const Duration(milliseconds: 200),
  //                         child: StreamBuilder<Comment>(
  //                           stream: bloc.lastComment,
  //                           builder: (BuildContext context,
  //                               AsyncSnapshot<Comment> snapshot) {
  //                             if (snapshot.hasData) {
  //                               final Comment comment = snapshot.data;
  //                               return ListTile(
  //                                 // key: GlobalObjectKey(index),
  //                                 leading: CircleAvatar(
  //                                   backgroundImage:
  //                                       NetworkImage(comment.user.photoURL),
  //                                 ),
  //                                 title: RichText(
  //                                     text: TextSpan(
  //                                         text: '${comment.user.name} ',
  //                                         style: ComooStyles.texto_bold,
  //                                         children: [
  //                                       TextSpan(
  //                                         style: ComooStyles.texto,
  //                                         text: timeago.format(comment.date,
  //                                             locale: 'pt_BR'),
  //                                       )
  //                                     ])),
  //                                 subtitle: Text(comment.text),
  //                               );
  //                             }
  //                             return Container();
  //                           },
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                   StreamBuilder<bool>(
  //                     stream: bloc.isLoading,
  //                     builder: (BuildContext context,
  //                         AsyncSnapshot<bool> snapshot) {
  //                       return snapshot.data ?? false
  //                           ? Container(child: LoadingContainer())
  //                           : Container();
  //                     },
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ),
  //         );
  //       }
  //       return Container(
  //         height: responsive(context, 375.0),
  //         width: MediaQuery.of(context).size.width,
  //         // child: Center(child: CircularProgressIndicator()),
  //       );
  //     },
  //   ),
  // );
  // }

  Widget _sellerInfo(BuildContext context, Product _product) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final TextStyle menuItemStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: mediaQuery * 15.0,
      letterSpacing: mediaQuery * 0.21,
    );
    final bool showMenu =
        ((_product?.user['uid'] == currentUser?.uid) ?? false) &&
            !(_product?.underNegotiation ?? false);
    return PostUserInfoRow(
      userPhotoURL: _product?.user['picture'] ?? '',
      userName: _product?.user['name'] ?? '',
      location: _product?.location ?? '',
      dateString: timeago.format(_product.date, locale: 'pt_BR'),
      groups: _product.groups,
      onGroupTap: (String id) {
        if (callback == null) {
          _navigateToGroupDetail(context, id);
        } else {
          callback();
        }
      },
      onImageTap: callback ??
          () {
            _navigateToSellerDetail(context, _product);
          },
      onNameTap: callback ??
          () {
            _navigateToSellerDetail(context, _product);
          },
      menu: showMenu
          ? PopupMenuButton<ProductMenu>(
              onSelected: (ProductMenu result) {
                switch (result) {
                  case ProductMenu.update:
                    _updateProduct(context, _product);
                    break;
                  case ProductMenu.republish:
                    _republishProduct(context, _product);
                    break;
                  case ProductMenu.delete:
                    _deleteProduct(context, _product);
                }
              },
              itemBuilder: (BuildContext context) =>
                  <PopupMenuEntry<ProductMenu>>[
                PopupMenuItem<ProductMenu>(
                  value: ProductMenu.update,
                  child: Text(
                    'EDITAR ANÚNCIO',
                    style: menuItemStyle,
                  ),
                ),
                PopupMenuItem<ProductMenu>(
                  value: ProductMenu.republish,
                  child: Text(
                    'REPOSTAR ANÚNCIO',
                    style: menuItemStyle,
                  ),
                ),
                PopupMenuItem<ProductMenu>(
                  value: ProductMenu.delete,
                  child: Text(
                    'EXCLUIR ANÚNCIO',
                    style: menuItemStyle,
                  ),
                ),
              ],
            )
          : Container(),
    );
  }

  Widget _productImage(
      BuildContext context, ProductBloc bloc, Product product) {
    final Size size = MediaQuery.of(context).size;
    final double mediaQuery = size.width / 400;
    // final Key _productKey = Key('future#${_product.id}');
    final double productWidth = bloc?.initialProduct?.width ?? size.width;
    final double productHeight = bloc?.initialProduct?.height ?? size.height;
    final double ratio = size.width / productWidth;
    final double height = productHeight * ratio;
    // final double height =
    //     product.height == null ? 375.0 : product.height * ratio;
    // print('${bloc.initialProduct.width}x${bloc.initialProduct.height}');
    // print('ratio: $ratio');
    return StreamBuilder<bool>(
        initialData: false,
        stream: bloc.isLoadingLike,
        builder: (BuildContext likeloadingContext, snapLikeLoading) {
          final bool likeLoading = snapLikeLoading.data ?? false;
          return InkWell(
            onDoubleTap: callback != null
                ? callback
                : likeLoading ? null : () => bloc.onLike(product),
            onTap: callback ??
                () async {
                  final Product prod = await bloc.fetchProduct(product.id);
                  await Navigator.of(context).push(MaterialPageRoute<void>(
                      builder: (context) => ProductPage(prod)));
                  bloc.reloadProduct(prod);
                },
            child: Stack(
              children: <Widget>[
                // Container(
                //   constraints: BoxConstraints.tight(Size(
                //     MediaQuery.of(context).size.width,
                //     height +
                //         responsive(
                //             context, product.photos.length <= 1 ? 10 : 30),
                //   )),
                //   child: ProductImage(
                //     product: product,
                //     width: MediaQuery.of(context).size.width,
                //     height: height,
                //   ),
                // ),
                Container(
                  constraints: BoxConstraints.tight(
                      Size(MediaQuery.of(context).size.width, height)),
                  child: PageView.builder(
                    key: Key('feed#${product.id}/pageview'),
                    controller: bloc.photoController,
                    itemCount: product.photos.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      final String url = product?.photos[index] ?? '';
                      if (url.isEmpty) {
                        return Container();
                      }
                      return PinchZoomImage(
                        zoomedBackgroundColor: Colors.white.withOpacity(0.2),
                        hideStatusBarWhileZooming: true,
                        image: Container(
                          constraints: BoxConstraints.tight(
                              Size(MediaQuery.of(context).size.width, height)),
                          child: CachedNetworkImage(
                            cacheManager: CustomCacheManager(),
                            imageUrl: product.photos[index],
                            fit: BoxFit.cover,
                            placeholder: (BuildContext context, String url) =>
                                Center(
                              child: CircularProgressIndicator(),
                            ),
                            errorWidget: (BuildContext context, String url,
                                    Object error) =>
                                Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Positioned(
                  right: mediaQuery * 20.0,
                  top: mediaQuery * 20.0,
                  child: Container(
                    width: mediaQuery * 120.0,
                    height: mediaQuery * 30.0,
                    alignment: FractionalOffset.center,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
                    child: FittedBox(
                      child: Text(
                        formatter.format(product.price),
                        style: ComooStyles.productPrice,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _productInfo(BuildContext context, Product _product) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      height: mediaQuery * 28.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                ComooIcons.marca,
                size: mediaQuery * 28.0,
              ),
              SizedBox(
                width: mediaQuery * 10.0,
              ),
              Text(
                _product.brand,
                overflow: TextOverflow.ellipsis,
                style: ComooStyles.product_tags,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                ComooIcons.tamanho,
                size: mediaQuery * 28.0,
              ),
              SizedBox(
                width: mediaQuery * 10.0,
              ),
              Text(
                _product.size,
                overflow: TextOverflow.ellipsis,
                style: ComooStyles.product_tags,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                ComooIcons.estado_produto,
                size: mediaQuery * 28.0,
              ),
              SizedBox(
                width: mediaQuery * 10.0,
              ),
              Text(_product?.condition ?? '', style: ComooStyles.product_tags),
            ],
          ),
        ],
      ),
    );
  }

  Widget _productActions(
      BuildContext context, ProductBloc bloc, Product product) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      // height: mediaQuery * 58.0,
      child: IconTheme(
        data: ComooIcons.roxo,
        child: StreamBuilder<bool>(
          stream: bloc.liked,
          builder: (BuildContext likeContext, AsyncSnapshot<bool> likeSnap) {
            // print('likes: ${product.likes}');
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                StreamBuilder<bool>(
                  stream: bloc.isLoadingLike,
                  initialData: false,
                  builder:
                      (BuildContext context, AsyncSnapshot<bool> loadingLike) {
                    return IconButton(
                      icon: Icon(
                        likeSnap.data ?? false
                            ? Icons.favorite
                            : ComooIcons.curtir,
                        size: 28.0,
                      ),
                      alignment: Alignment.centerRight,
                      onPressed: loadingLike.data
                          ? null
                          : callback ??
                              () {
                                bloc.onLike(product);
                              },
                    );
                  },
                ),
                Text(
                  '${product?.likes ?? ''}',
                  style: ComooStyles.texto_roxo,
                ),
                IconButton(
                  icon: Icon(
                    ComooIcons.comentar,
                    size: mediaQuery * 28.0,
                  ),
                  alignment: Alignment.centerRight,
                  onPressed: callback ??
                      () {
                        _navigateToComments(context, product);
                      },
                ),
                StreamBuilder<int>(
                    stream: bloc.commentsQty,
                    builder:
                        (BuildContext context, AsyncSnapshot<int> snapshot) {
                      return Text(
                        '${snapshot.data ?? 0}',
                        style: ComooStyles.produto_social,
                      );
                    }),
                Expanded(
                  child: Container(),
                ),
                Container(
                  height: mediaQuery * 48.0,
                  //  width: mediaQuery * 76.0,
                  padding: EdgeInsets.only(
                    left: mediaQuery * 10.0,
                    right: mediaQuery * 10.0,
                    bottom: mediaQuery * 10.0,
                    top: mediaQuery * 10.0,
                  ),
                  child: StreamBuilder<bool>(
                    initialData: false,
                    stream: bloc.loadingRequest,
                    builder: (BuildContext context,
                        AsyncSnapshot<bool> loadingSnap) {
                      return StreamBuilder<bool>(
                        initialData: true,
                        stream: bloc.userHasRequested,
                        builder: (BuildContext context,
                            AsyncSnapshot<bool> requestedSnap) {
                          return RaisedButton(
                            elevation: 0.0,
                            color: ComooColors.pink,
                            child: Text('COMPRAR', style: ComooStyles.quero),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(mediaQuery * 20.0)),
                            onPressed: loadingSnap.data
                                ? null
                                : requestedSnap.data
                                    ? null
                                    : callback ??
                                        () async {
                                          final Map<String, dynamic> result =
                                              await bloc
                                                  .requestProduct(product);
                                          print(result);
                                          if (result == null) {
                                            showCustomDialog<void>(
                                              context: context,
                                              alert: customRoundAlert(
                                                context: context,
                                                title:
                                                    'Um erro inesperado aconteceu, tente novamente mais tarde.',
                                              ),
                                            );
                                          } else {
                                            final bool isFirst =
                                                result['isFirst'] ?? false;
                                            if (isFirst) {
                                              //
                                              final Negotiation negotiation =
                                                  result['negotiation']
                                                      as Negotiation;

                                              await Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DirectChatView(
                                                            negotiation)
                                                    // NegotiationPage(negotiation, goToChat: true),
                                                    ),
                                              );
                                              bloc.reloadProduct(product);
                                            } else {
                                              final int count = result['count'];
                                              _showNotFirstDialog(
                                                  context, count);
                                            }
                                          }
                                        },
                          );
                        },
                      );
                    },
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  void _showNotFirstDialog(BuildContext context, int count) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.all(mediaQuery * 0.0),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: mediaQuery * 186.0,
                width: mediaQuery * 329.0,
                decoration: BoxDecoration(
                  color: ComooColors.pink,
                  borderRadius: BorderRadius.all(
                    Radius.circular(mediaQuery * 20.0),
                  ),
                ),
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Seu QUERO foi enviado!\r\nVocê é o $count\º da fila',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white, fontSize: mediaQuery * 18.0),
                      ),
                      SizedBox(height: mediaQuery * 26.0),
                      ButtonBar(
                        mainAxisSize: MainAxisSize.min,
                        alignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: mediaQuery * 41.0,
                            width: mediaQuery * 163.0,
                            decoration: BoxDecoration(
                              border: Border.all(
                                width: 1.0,
                                color: Colors.white,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                'OK',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _navigateToGroupDetail(BuildContext context, String id) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => GroupDetailView(id: id)));
  }

  Future<void> _navigateToSellerDetail(
      BuildContext context, Product product) async {
    final DocumentSnapshot snap =
        await _db.collection('users').document(product.user['uid']).get();
    Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) =>
          UserDetailView(User.fromSnapshot(snap)),
    ));
  }

  void _navigateToComments(BuildContext context, Product product) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProductCommentsView(product),
      ),
    );
  }

  Future<void> _updateProduct(BuildContext context, Product _product) async {
    final ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
    final Product product = await bloc.fetchProduct(_product.id);
    await Navigator.of(context).push(
      MaterialPageRoute<void>(
          builder: (context) =>
              ProductCreateView(product: product, openCamera: false)),
    );

    bloc.reloadProduct(product);
  }

  Future<void> _republishProduct(BuildContext context, Product product) async {
    final ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
    await Navigator.of(context).push(
      MaterialPageRoute<void>(
          builder: (context) => ProductRepublishView(product: product)),
    );
    bloc.reloadProduct(product);
  }

  Future<void> _deleteProduct(BuildContext context, Product product) async {
    final ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final bool willDelete = await showCustomDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      alert: customRoundAlert(
        context: context,
        title: 'Tem certeza que deseja excluir o produto?',
        actions: [
          Container(
            height: mediaQuery * 41.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FittedBox(
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Text(
                  'CANCELAR',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ),
          ),
          Container(
            height: mediaQuery * 41.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FittedBox(
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                child: Text(
                  'EXCLUIR',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: () async {
                  Navigator.of(context).pop(true);
                },
              ),
            ),
          ),
        ],
      ),
    );
    if (willDelete ?? false) {
      bloc.delete(product);
    }
  }
}
