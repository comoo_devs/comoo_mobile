import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/comments.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/post.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
//import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:rxdart/rxdart.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:comoo/src/pages/common/group/group_detail.dart';
import 'package:comoo/src/widgets/comments/post_comments.dart';

class PostFeed extends StatefulWidget {
  final Post post;
  final Product product;

  PostFeed({this.post, this.product, Key key}) : super(key: key);

  @override
  State createState() {
    return _PostFeedState();
  }
}

class _PostFeedState extends State<PostFeed> {
  final Firestore _db = Firestore.instance;
  User _user;
  bool _liked = false;
  bool _deleted = false;

  final BehaviorSubject<Comment> _lastComment = BehaviorSubject<Comment>();
  Function(Comment) get addLastComment => _lastComment.sink.add;
  Stream<Comment> get lastCommentStream => _lastComment.stream;

  void _navigateToUserDetail() {
    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => UserDetailView(_user)));
  }

  void _navigateToGroupDetail(group) {
    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => GroupDetailView(id: group['id'])));
  }

  void _navigateToComments() {
    Navigator.push(
      context,
      MaterialPageRoute<void>(
        builder: (context) => PostCommentsView(widget.post),
      ),
    );
  }

  // _fetchUserRequest() async {}

  // _update() {}

  Future<void> _delete() async {
    await widget.post.delete();
    if (mounted) {
      setState(() {
        _deleted = true;
      });
    }
  }

  Future<void> _fetchLastComment() async {
    final QuerySnapshot query = await _db
        .collection('posts')
        .document(widget.post.id)
        .collection('comments')
        .orderBy('date', descending: true)
        .limit(1)
        .getDocuments();
    if (query.documents.isNotEmpty) {
      final DocumentSnapshot doc = query.documents.first;
      if (doc.exists) {
        final DocumentReference userRef = doc.data['from'];
        final DocumentSnapshot userDoc = await userRef.get();

        final User user = User.fromSnapshot(userDoc);
        final Comment comment = Comment(
          doc.data['text'],
          user,
          (doc.data['date'] is Timestamp)
              ? DateTime.fromMillisecondsSinceEpoch(
                  (doc.data['date'] as Timestamp).millisecondsSinceEpoch)
              : (doc.data['date'] as DateTime),
        );
        if (!_lastComment.isClosed) {
          addLastComment(comment);
        }
      }
    }
  }

  @override
  void dispose() {
    _lastComment?.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    if (widget.product != null) {
      // _fetchUserRequest();
      _user = User(widget.product.user['uid'], widget.product.user['name'],
          widget.product.user['picture']);
    } else {
      //POST
      _user = User(widget.post.user['uid'], widget.post.user['name'],
          widget.post.user['picture']);
    }
    _fetchLastComment();
    if (currentUser.likes != null)
      _liked = currentUser.likes.indexOf(widget.post.id) == -1 ? false : true;
  }

  Widget _buildHeader() {
    return Padding(
      padding: const EdgeInsets.only(top: 14.0, bottom: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            child: Container(
              margin: const EdgeInsets.only(left: 15.0, right: 10.0),
              width: 50.0,
              height: 50.0,
              child: ClipOval(
                child: Image(
                  image: AdvancedNetworkImage(widget.post.user['picture'],
                      useDiskCache: true),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            onTap: () {
              _navigateToUserDetail();
            },
          ),
          Expanded(
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    _navigateToUserDetail();
                  },
                  child: Container(
                    child: RichText(
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        text: widget.post.user['name'] + ' ',
                        style: ComooStyles.nomeFeed,
                      ),
                    ),
                  ),
                ),
                Container(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    text: TextSpan(
                      text: timeago.format(widget.post.date, locale: 'pt_BR'),
                      style: ComooStyles.datas,
                    ),
                  ),
                ),
                Container(
                  height: 20.0,
                  child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: widget.post.groups
                          .map(
                            (g) => GestureDetector(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Text(
                                      g['name'],
                                      style: ComooStyles.localizacao,
                                    ),
                                  ),
                                  onTap: () {
                                    _navigateToGroupDetail(g);
                                  },
                                ),
                          )
                          .toList()),
                ),
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
            ),
          ),
          _user.uid == currentUser.uid
              ? PopupMenuButton<OwnerMenu>(
                  onSelected: (OwnerMenu result) {
                    switch (result) {
                      // case OwnerMenu.update:
                      //   _update();
                      //   break;
                      case OwnerMenu.delete:
                        _delete();
                        break;
                      default:
                        break;
                    }
                  },
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<OwnerMenu>>[
                        // const PopupMenuItem<OwnerMenu>(
                        //   value: OwnerMenu.update,
                        //   child: const Text('Editar publicação'),
                        // ),
                        const PopupMenuItem<OwnerMenu>(
                          value: OwnerMenu.delete,
                          child: const Text('Deletar publicação'),
                        ),
                      ],
                )
              : Container()
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Container(
      height: 240.0,
      padding: EdgeInsets.all(33.0),
      decoration: BoxDecoration(
          gradient:
              LinearGradient(colors: [ComooColors.roxo, ComooColors.pink])),
      alignment: Alignment.center,
      child: Text(
        this.widget.post.message,
        style: ComooStyles.indicacao,
      ),
    );
  }

  Widget _buildSocial() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return IconTheme(
        data: ComooIcons.roxo,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            IconButton(
              icon: Icon(
                _liked ? Icons.favorite : ComooIcons.curtir,
                size: 28.0,
              ),
              alignment: Alignment.centerRight,
              onPressed: () {
                setState(() {
                  if (!_liked) {
                    currentUser.addToLiked(widget.post.id);
                    widget.post.addLike();
                  } else {
                    currentUser.removeToLiked(widget.post.id);
                    widget.post.removeLike();
                  }
                  _liked = !_liked;
                });
              },
            ),
            Text(widget.post.likes.toString(), style: ComooStyles.texto_roxo),
            IconButton(
              icon: Icon(
                ComooIcons.comentar,
                size: 28.0,
              ),
              alignment: Alignment.centerRight,
              onPressed: () {
                _navigateToComments();
              },
            ),
            Text(widget.post.commentsQty.toString(),
                style: ComooStyles.texto_roxo),
            Expanded(
              child: Container(),
            ),
            Container(
              height: mediaQuery * 48.0,
//              width: mediaQuery * 76.0,
              padding: EdgeInsets.only(
                left: mediaQuery * 10.0,
                right: mediaQuery * 10.0,
                bottom: mediaQuery * 10.0,
                top: mediaQuery * 8.0,
              ),
              // child: RaisedButton(
              //   elevation: 0.0,
              //   color: ComooColors.roxo,
              //   child: Text('TENHO', style: ComooStyles.quero),
              //   shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
              //   onPressed: () {
              //     showDialog(
              //       context: context,
              //       builder: (BuildContext context) {
              //         return AlertDialog(
              //           shape: RoundedRectangleBorder(
              //             borderRadius: BorderRadius.all(
              //                 Radius.circular(mediaQuery * 32.0)),
              //           ),
              //           contentPadding: EdgeInsets.only(top: mediaQuery * 10.0),
              //           content: Container(
              //             width: mediaQuery * 300.0,
              //             height: mediaQuery * 200.0,
              //             child: Center(
              //               child: Text(
              //                 'Funcionalidade necessitando de fluxo!',
              //               ),
              //             ),
              //           ),
              //         );
              //       },
              //     );
              //   },
              // ),
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    if (_deleted)
      return Container(
//        alignment: Alignment.center,
//        child: Text(
//          'Publicação deleteda com sucesso',
//          style: ComooStyles.texto_roxo,
//        ),
          );

    List<Widget> children = List<Widget>();
    children.add(_buildHeader());
    if (widget.product != null) {
//      children.add(_buildTitle());
//      children.add(_buildPhotos());
//      children.add(_buildDots());
//      children.add(_buildTags());
    } else {
      children.add(_buildContent());
    }
    children.add(_buildSocial());
    children.add(AnimatedContainer(
      curve: Curves.easeIn,
      duration: const Duration(milliseconds: 200),
      child: StreamBuilder<Comment>(
          stream: lastCommentStream,
          builder: (BuildContext context, AsyncSnapshot<Comment> snapshot) {
            if (snapshot.hasData) {
              final Comment comment = snapshot.data;
              return ListTile(
                dense: true,
                // key: GlobalObjectKey(index),
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(comment.user.photoURL),
                ),
                title: RichText(
                    text: TextSpan(
                        text: '${comment.user.name} ',
                        style: ComooStyles.texto_bold,
                        children: [
                      TextSpan(
                        style: ComooStyles.texto,
                        // text: comment.text,
                        text: timeago.format(comment.date, locale: 'pt_BR'),
                      )
                    ])),
                subtitle: Text(comment.text),
              );
            }
            return Container();
          }),
    ));

    Widget card;
    card = Card(
      margin: EdgeInsets.only(bottom: 15.0),
      // elevation: 5.0,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: children),
    );
    return card;
  }
}

enum OwnerMenu { update, delete }
