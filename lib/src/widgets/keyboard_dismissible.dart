import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class KeyboardDismissible extends StatelessWidget {
  final Widget child;

  KeyboardDismissible({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
        SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
      },
      child: child,
    );
  }
}
