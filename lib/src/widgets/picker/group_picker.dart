import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';

class GroupPicker extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _GroupPickerState();
  }
}

class _GroupPickerState extends State<GroupPicker> {
  _GroupPickerState();

  List<GroupItem> _groups = List<GroupItem>();
  bool isLoading = false;
  FirebaseAuth auth = FirebaseAuth.instance;
  Firestore db = Firestore.instance;

  final Color baseColor = Color.fromRGBO(70, 30, 90, 1.0);
  final FocusNode searchFocusNode = FocusNode();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(
        new SnackBar(content: Text(value, style: ComooStyles.textoSnackBar)));
  }

  _selectGroups(selectedGroups) {
    var groups = selectedGroups.map((g) => g.group).toList();
    Navigator.of(context).pop(groups);
  }

  Future _fetchCurrentUserGroups() async {
    db
        .collection('users')
        .document(currentUser.uid)
        .collection('groups')
        .getDocuments()
        .then((QuerySnapshot query) {
      query.documents.forEach((DocumentSnapshot snapshot) {
        var groupInfo = snapshot.data;
        Group group = Group(
            id: snapshot.documentID,
            name: groupInfo['name'],
            description: groupInfo['description'] ?? "",
            avatar: groupInfo['avatar']);
        setState(() {
          _groups.add(new GroupItem(group, selected: false));
        });
      });
    }).whenComplete(() {
      setState(() {
        isLoading = false;
      });
    });
  }

  String _search = "";

  Widget _buildGroupList() {
    List<GroupItem> groupList = List<GroupItem>();
    if (_search.isNotEmpty == true)
      groupList = _groups
          .where((g) => g.group.name
              .startsWith(new RegExp('$_search', caseSensitive: false)))
          .toList();
    else
      groupList = _groups;

    return ListView.builder(
        itemCount: groupList.length,
        itemBuilder: (context, index) {
          GroupItem group = groupList[index];
          return CheckboxListTile(
            value: group.selected ?? false,
            controlAffinity: ListTileControlAffinity.trailing,
            secondary: ClipOval(
              child: Image.network(
                group.group.avatar,
                fit: BoxFit.cover,
                width: 40.0,
                height: 40.0,
              ),
            ),
            title: Text(group.group.name),
            subtitle: Text(group.group.description),
            onChanged: (value) {
              setState(() {
                searchFocusNode.unfocus();
                group.selected = value;
              });
            },
          );
        });
  }

  @override
  void initState() {
    isLoading = true;
    super.initState();
    _fetchCurrentUserGroups();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: Colors.white,
        title: const Text('GRUPOS'),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                var selectedGroups =
                    _groups.where((g) => g.selected == true).toList();
                if (selectedGroups.length == 0) {
                  showInSnackBar('Selecione os grupos para publicar.');
                } else {
                  _selectGroups(selectedGroups);
                }
              },
              child: Text('PUBLICAR',
                  style: const TextStyle(fontWeight: FontWeight.w300)))
        ],
      ),
      body: Column(
        children: <Widget>[
          Form(
            child: Container(
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    _search = text;
                  });
                },
                autofocus: false,
                focusNode: searchFocusNode,
                decoration: InputDecoration(
                    icon: Icon(Icons.search),
                    hintText: 'Digite o nome do grupo'),
              ),
            ),
          ),
          isLoading
              ? LinearProgressIndicator()
              : Expanded(child: _buildGroupList())
        ],
      ),
    );
  }
}

class GroupItem {
  GroupItem(this.group, {this.selected});

  Group group;
  bool selected = false;
}
