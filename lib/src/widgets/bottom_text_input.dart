import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';

class BottomTextInput extends StatelessWidget {
  BottomTextInput();
  @override
  Widget build(BuildContext context) {
    return _buildTextComposer(context);
  }

  Widget _buildTextComposer(context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return IconTheme(
        data: IconThemeData(color: ComooColors.roxo),
        child: Container(
          color: ComooColors.cinzaClaro,
          margin: EdgeInsets.symmetric(horizontal: mediaQuery * 8.0),
          child: Row(
            children: <Widget>[
              Flexible(
                child: Container(
                  // color: Colors.white,
                  height: mediaQuery * 40.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(mediaQuery * 20.0),
                  ),
                  child: TextField(
                    // enabled: negotiation.status != 'cancelled' &&
                    //     negotiation.status != 'finished',
                    // controller: _textController,
                    onChanged: (String text) {
                      // if (mounted) {
                      //   setState(() {
                      //     _isComposing = text.length > 0 &&
                      //         negotiation.status != 'cancelled' &&
                      //         negotiation.status != 'finished';
                      //   });
                      // }
                    },
                    textCapitalization: TextCapitalization.sentences,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: mediaQuery * 16.0,
                      fontWeight: FontWeight.w500,
                      color: ComooColors.chumbo,
                    ),
                    onSubmitted: (String
                        text) {}, //_isComposing ? _handleSubmitted : null,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(
                        left: mediaQuery * 10.0,
                        top: mediaQuery * 10.0,
                      ),
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: mediaQuery * 14.0,
                      ),
                      hintText: 'Enviar mensagem',
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: mediaQuery * 4.0),
                child: IconButton(
                  icon: Icon(
                    ComooIcons.chatSend,
                    size: mediaQuery * 35.0,
                  ),
                  onPressed: () {},
                  // onPressed: _isComposing
                  //     ? () => _handleSubmitted(_textController.text)
                  //     : null,
                ),
              ),
            ],
          ),
        ));
  }
}
