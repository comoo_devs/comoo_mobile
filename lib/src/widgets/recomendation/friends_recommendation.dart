import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';

class FriendsRecommendation extends StatelessWidget {
  // final List<User> users;
  FriendsRecommendation({Key key, @required this.users}) : super(key: key);

  final List<User> users;

  // final NetworkApi api = NetworkApi();
  // final CloudFunctions _cloud = CloudFunctions.instance;

  // Stream<dynamic> _fetchRecommendation() {
  //   return _cloud
  //       .call(
  //           functionName: 'fetchUserRecommendation',
  //           parameters: <String, dynamic>{})
  //       .timeout(const Duration(seconds: 60))
  //       .catchError((e) {
  //         print(e);
  //         return <Map<String, dynamic>>[];
  //       })
  //       .asStream();
  // }

  @override
  Widget build(BuildContext context) {
    // final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return StreamBuilder<List<User>>(
      // stream: _fetchRecommendation(),
      stream: Future.value(users).asStream(),
      builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
        // print(snapshot.hasData);
        // print('${DateTime.now()} ${snapshot.data}');
        return snapshot.hasData
            ? buildSnapshotList(context, snapshot.data)
            : Container();
      },
    );
  }

  Widget buildSnapshotList(BuildContext context, List<User> data) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    // data.shuffle();
    if (data.isEmpty) {
      return Container();
    }
    return Padding(
      padding: EdgeInsets.only(left: mediaQuery * 1.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: mediaQuery * 17.0),
            child: Text(
              'Pessoas com interesses em comum',
              style: ComooStyles.titulos_h2,
            ),
          ),
          SizedBox(height: mediaQuery * 8.0),
          Container(
            // padding: EdgeInsets.only(left: 17.0),
            child: ListView.builder(
              // controller: pageController,
              scrollDirection: Axis.horizontal,
              itemExtent: mediaQuery * 68.0,
              itemCount: data.length,
              itemBuilder: (BuildContext context, int index) {
                final User user = data[index];
                return GestureDetector(
                  onTap: () {
                    // final Firestore _db = Firestore.instance;
                    // final DocumentSnapshot snap = await _db
                    //     .collection('users')
                    //     .document(data[index]['uid'])
                    //     .get();
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) =>
                            UserDetailView(user)));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CachedNetworkImage(
                        width: mediaQuery * 58.0,
                        height: mediaQuery * 58.0,
                        imageUrl: user.photoURL,
                        fit: BoxFit.cover,
                        imageBuilder:
                            (BuildContext context, ImageProvider provider) =>
                                CircleAvatar(
                          backgroundImage: provider,
                          radius: mediaQuery * 30.0,
                          backgroundColor: Colors.white,
                        ),
                        placeholder: (BuildContext context, String text) =>
                            Center(
                          child: Container(
                            width: mediaQuery * 58.0,
                            height: mediaQuery * 58.0,
                          ), //CircularProgressIndicator(),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: mediaQuery * 4.0)),
                      Container(
                        width: mediaQuery * 62.0,
                        child: Text(
                          user.name.padRight(10),
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: ComooStyles.texto_h2,
                        ),
                      )
                    ],
                  ),
                );
                // return FutureBuilder<DocumentSnapshot>(
                //   future: data[index % data.length],
                //   builder: (BuildContext context,
                //       AsyncSnapshot<DocumentSnapshot> snapshot) {
                //     return snapshot.hasData
                //         ? snapshot.data.exists
                //             ? buildSnapshotData(context, snapshot.data)
                //             : Container()
                //         : Container();
                //     // : Center(child: CircularProgressIndicator());
                //   },
                // );
              },
            ),
            height: mediaQuery * 84.0,
          ),
          Divider(color: ComooColors.cinzaMedio),
        ],
      ),
    );
  }

  Widget buildSnapshotData(context, data) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final User user = User.fromSnapshot(data);
    // _markAsShowed(user);
//                print(user);
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => UserDetailView(user)));
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              child: (user?.photoURL ?? '').isEmpty
                  ? Center(
                      child: Container(
                        width: mediaQuery * 58.0,
                        height: mediaQuery * 58.0,
                      ), //CircularProgressIndicator(),
                    )
                  : CachedNetworkImage(
                      width: mediaQuery * 58.0,
                      height: mediaQuery * 58.0,
                      imageUrl: user.photoURL,
                      fit: BoxFit.cover,
                      imageBuilder: (BuildContext context,
                              ImageProvider<dynamic> provider) =>
                          CircleAvatar(
                        backgroundImage: provider,
                        radius: mediaQuery * 30.0,
                        backgroundColor: Colors.white,
                      ),
                      placeholder: (BuildContext context, String text) =>
                          Center(
                        child: Container(
                          width: mediaQuery * 58.0,
                          height: mediaQuery * 58.0,
                        ), //CircularProgressIndicator(),
                      ),
                      errorWidget:
                          (BuildContext context, String url, Object error) =>
                              Center(
                        child: Container(
                          width: mediaQuery * 58.0,
                          height: mediaQuery * 58.0,
                        ), //CircularProgressIndicator(),
                      ),
                    ),
            ),
            Padding(padding: EdgeInsets.only(top: mediaQuery * 4.0)),
            Container(
              width: mediaQuery * 62.0,
              child: Text(
                user.name.padRight(10),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: ComooStyles.texto_h2,
              ),
            )
          ],
        ),
      ),
    );
  }
}
