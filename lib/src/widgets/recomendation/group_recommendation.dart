import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/pages/common/group/group_detail.dart';
import 'package:flutter/cupertino.dart';

class GroupRecommendation extends StatelessWidget {
  final List<Group> groups;
  final String title;
  GroupRecommendation(this.title, this.groups);
  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Padding(
      padding: EdgeInsets.only(
          top: mediaQuery * 15.0,
          bottom: mediaQuery * 12.0,
          left: mediaQuery * 1.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(left: mediaQuery * 12.0),
              child: Text(
                title,
                style: ComooStyles.titulos_h2,
              )),
          Padding(
            padding: EdgeInsets.only(top: mediaQuery * 8.63),
          ),
          Container(
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemExtent: mediaQuery * 68.0,
              itemCount: groups.length,
              itemBuilder: (BuildContext context, int index) {
                final Group group = groups[index];
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        fullscreenDialog: false,
                        builder: (BuildContext context) =>
                            GroupDetailView(group: group)));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CachedNetworkImage(
                        width: mediaQuery * 62.0,
                        height: mediaQuery * 62.0,
                        imageUrl: group.avatar,
                        fit: BoxFit.cover,
                        imageBuilder: (BuildContext context,
                                ImageProvider<dynamic> provider) =>
                            CircleAvatar(
                          backgroundImage: provider,
                          radius: mediaQuery * 30.0,
                          backgroundColor: Colors.white,
                        ),
                        placeholder: (BuildContext context, String text) =>
                            Center(
                          child: Container(
                            width: mediaQuery * 62.0,
                            height: mediaQuery * 62.0,
                          ), //CircularProgressIndicator(),
                        ),
                      ),
                      // Container(
                      //   width: mediaQuery * 62.0,
                      //   height: mediaQuery * 62.0,
                      //   decoration: BoxDecoration(
                      //       image: DecorationImage(
                      //         fit: BoxFit.cover,
                      //         image: CachedNetworkImageProvider(group.avatar),
                      //       ),
                      //       shape: BoxShape.circle),
                      // ),
                      Padding(padding: EdgeInsets.only(top: mediaQuery * 4.0)),
                      Container(
                        width: mediaQuery * 62.0,
                        child: Text(
                          group.name,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: ComooStyles.feed_suggestion,
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
            height: mediaQuery * 84.0,
//            constraints: BoxConstraints(
//              minWidth: 500.0
//            ),
          ),
        ],
      ),
    );
  }
}
