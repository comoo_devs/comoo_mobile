import 'package:flutter/widgets.dart';

class TabWidget {
  TabWidget({@required this.tab, @required this.widget});

  Widget tab;
  Widget widget;
}
