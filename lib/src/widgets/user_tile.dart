import 'package:comoo/src/blocs/user_tile_bloc.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

import 'network_image.dart';

class UserTile extends StatefulWidget {
  UserTile(this.user, {this.fromSignup = false, this.subtitle});
  final User user;
  final bool fromSignup;
  final Widget subtitle;
  @override
  State<StatefulWidget> createState() => _UserTileState();
}

class _UserTileState extends State<UserTile> {
  UserTileBloc bloc;
  @override
  void initState() {
    bloc = UserTileBloc.seeded(widget.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserTileBloc>(
      bloc: bloc,
      child: UserTileStateless(
        widget.user,
        fromSignup: widget.fromSignup,
        subtitle: widget.subtitle,
      ),
    );
  }
}

class UserTileStateless extends StatelessWidget with Reusables {
  UserTileStateless(this.user, {this.fromSignup = false, this.subtitle});
  final User user;
  final bool fromSignup;
  final Widget subtitle;
  @override
  Widget build(BuildContext context) {
    final UserTileBloc bloc = BlocProvider.of<UserTileBloc>(context);
    final TextStyle nameStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: responsive(context, 10.0),
      fontWeight: FontWeight.w600,
      color: COMOO.colors.lead,
    );
    return ListTile(
      onTap: fromSignup ? null : () => openUser(context),
      leading: CachedCircleAvatar(
        url: user.photoURL,
      ),
      title: Text(
        '${user.name}',
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
        style: TextStyle(
          fontFamily: COMOO.fonts.montSerrat,
          color: COMOO.colors.lead,
          fontSize: responsive(context, 14.0),
          letterSpacing: responsive(context, 0.25),
          fontWeight: FontWeight.w500,
        ),
      ),
      subtitle: subtitle,
      trailing: user.uid == currentUser.uid
          ? null
          : StreamBuilder<bool>(
              stream: bloc.isFriend,
              builder: (BuildContext context, AsyncSnapshot<bool> isFriend) {
                return Container(
                  constraints: BoxConstraints(
                    maxWidth: responsive(context, 77.0),
                    maxHeight: responsive(context, 24.0),
                  ),
                  child: (isFriend.data ?? false)
                      ? OutlineButton(
                          onPressed: () {
                            bloc.editFriend(false);
                          },
                          color: COMOO.colors.lead,
                          disabledBorderColor: COMOO.colors.lead,
                          disabledTextColor: COMOO.colors.lead,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                                responsive(context, 30.0)),
                          ),
                          child: FittedBox(
                            child: Center(
                              child: Text(
                                'SEGUINDO',
                                style: nameStyle,
                              ),
                            ),
                          ),
                        )
                      : RaisedButton(
                          elevation: 0.0,
                          onPressed: () {
                            bloc.editFriend(true);
                          },
                          color: COMOO.colors.lead,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                                responsive(context, 30.0)),
                          ),
                          child: FittedBox(
                            child: Center(
                              child: Text(
                                'SEGUIR',
                                style: nameStyle.copyWith(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                );
              },
            ),
    );
  }

  Future<void> openUser(BuildContext context) async {
    await Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => UserDetailView(user),
    ));
  }
}
