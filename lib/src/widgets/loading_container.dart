import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';

class LoadingContainer extends StatelessWidget {
  LoadingContainer({this.message});

  final String message;

  @override
  Widget build(BuildContext context) {
    return new Container(
      color: Colors.white70,
      child: new Center(
        child: new Container(
            height: 200.0,
            width: 200.0,
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: <BoxShadow>[new BoxShadow(color: ComooColors.roxo)],
                borderRadius: new BorderRadius.circular(20.0)),
            child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Theme(
                      data: ThemeData(accentColor: ComooColors.roxo),
                      child: new CircularProgressIndicator()),
                  new SizedBox(
                    height: 30.0,
                  ),
                  new Text(
                    this.message ?? "Carregando...",
                    style: ComooStyles.titulos,
                    textAlign: TextAlign.center,
                  )
                ])),
      ),
    );
  }
}
