import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class RequestWithdraw extends StatefulWidget {
  @override
  _RequestWithdrawState createState() {
    return new _RequestWithdrawState();
  }
}

class _RequestWithdrawState extends State<RequestWithdraw> {
  bool _isLoading = false;
  bool _isNotEmpty = false;
  TextEditingController valueTextController =
      new MoneyMaskedTextController(leftSymbol: 'R\$ ');

  _requestWithraw(BuildContext context) async {
    setState(() {
      _isLoading = true;
    });
    await CloudFunctions.instance
        .getHttpsCallable(functionName: 'requestWithdraw')
        .call(<String, dynamic>{'amount': valueTextController.text})
        .timeout(Duration(seconds: 30))
        .then((_) {
          _displaySuccessAlert();
        })
        .catchError((onError) {
          print(onError);
          _displayErrorAlert();
        });
  }

  _displaySuccessAlert() async {
    setState(() {
      _isLoading = false;
    });
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text('SOLICITAÇÃO EFETUADA', style: ComooStyles.texto_bold),
            content: Text(
              'Sua solicitação de transferência bancária foi efetuada com sucesso!',
              style: ComooStyles.texto,
            ),
          );
        });
    setState(() {
      _isLoading = false;
    });
    Navigator.pop(context);
  }

  _displayErrorAlert() async {
    setState(() {
      _isLoading = false;
    });
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text('ALGO DEU ERRADO', style: ComooStyles.texto_bold),
            content: Text(
              'Sua solicitação de transferência bancária não foi processada, verifique a conexão e tente novamente!',
              style: ComooStyles.texto,
            ),
          );
        });
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? new Container(
            height: 200.0,
            width: 200.0,
            child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new CircularProgressIndicator(),
                  new SizedBox(
                    height: 30.0,
                  ),
                  new Text(
                    'Processando...',
                    style: ComooStyles.titulos,
                  )
                ]))
        : new Padding(
            padding: const EdgeInsets.all(16.0),
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Text(
                  'Valor da transferência',
                  style: ComooStyles.appBarTitle,
                ),
                new TextField(
                  keyboardType: TextInputType.number,
                  controller: valueTextController,
                  onChanged: (value) {
                    setState(() {
                      _isNotEmpty = valueTextController.text.trim().isNotEmpty;
                    });
                  },
                  style: ComooStyles.inputText,
                ),
                new Center(
                    child: new FlatButton(
                        onPressed: _isNotEmpty
                            ? () {
                                _requestWithraw(context);
                              }
                            : null,
                        child: Text(
                          'CONTINUAR',
                          style: ComooStyles.botao,
                        ),
                        textColor: ComooColors.roxo,
                        disabledTextColor: ComooColors.cinzaClaro))
              ],
            ));
  }

  @override
  void dispose() {
    valueTextController.dispose();
    super.dispose();
  }
}
