// import 'package:comoo/src/comoo/api.dart';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/comoo.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/group/group_detail.dart';
import 'package:comoo/src/pages/signup/signup.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
// import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:convert';

import 'package:share/share.dart';

class Reusables {
  String defaultImage() {
    return 'https://firebasestorage.googleapis.com/v0/b/app-comoo.appspot.com/o/defaultUser.png?alt=media&token=44335ced-6b07-4511-8f73-d9a4a2a6d1eb';
  }

  void hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
  }

  void showSnackBarMessage(BuildContext context, String value) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(value, style: ComooStyles.textoSnackBar),
      duration: const Duration(seconds: 3),
    ));
  }

  Future<T> showCustomDialog<T>({
    @required BuildContext context,
    bool barrierDismissible,
    Color color,
    AlertDialog alert,
  }) {
    return showGeneralDialog(
      context: context,
      pageBuilder: (
        BuildContext buildContext,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
      ) {
        return SafeArea(
          child: Builder(builder: (BuildContext context) {
            return Builder(builder: (context) => alert);
          }),
        );
      },
      barrierDismissible: barrierDismissible ?? true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: color ?? Colors.white70,
      transitionDuration: const Duration(milliseconds: 150),
      transitionBuilder: _buildMaterialDialogTransitions,
    );
  }

  Widget _buildMaterialDialogTransitions(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    return FadeTransition(
      opacity: CurvedAnimation(
        parent: animation,
        curve: Curves.easeOut,
      ),
      child: child,
    );
  }

  AlertDialog customRoundAlert({
    @required BuildContext context,
    String title,
    TextAlign textAlign = TextAlign.start,
    List<Widget> actions,
    Widget subtitle,
    Color color,
  }) {
    assert(title != null);
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    List<Widget> buttons = actions ??
        [
          Container(
            height: mediaQuery * 41.0,
            width: mediaQuery * 163.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ];
    return AlertDialog(
      contentPadding: EdgeInsets.all(mediaQuery * 0.0),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: mediaQuery * 329.0,
            decoration: BoxDecoration(
              color: color ?? ComooColors.pink,
              borderRadius: BorderRadius.all(
                Radius.circular(mediaQuery * 32.0),
              ),
            ),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //  SizedBox(height: mediaQuery * 26.0),
                  Container(
                    margin: EdgeInsets.only(
                      top: mediaQuery * 31.0,
                      left: mediaQuery * 10.0,
                      right: mediaQuery * 10.0,
                      bottom: mediaQuery * 0.0,
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: mediaQuery * 8.0,
                    ),
                    child: Text(
                      title,
                      textAlign: textAlign,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: mediaQuery * 18.0,
                      ),
                    ),
                  ),
                  subtitle ?? Container(),
                  ButtonBar(
                    mainAxisSize: MainAxisSize.min,
                    alignment: MainAxisAlignment.center,
                    children: buttons,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  AlertDialog customRoundAlertTitle({
    @required BuildContext context,
    String title,
    List<Widget> actions,
    String subtitle,
    Color color,
    TextAlign titleAlign,
    TextAlign subtitleAlign,
  }) {
    assert(title != null);
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    List<Widget> buttons = actions ??
        [
          Container(
            height: mediaQuery * 41.0,
            width: mediaQuery * 163.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ];
    return AlertDialog(
      contentPadding: EdgeInsets.all(mediaQuery * 0.0),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: mediaQuery * 329.0,
            decoration: BoxDecoration(
              color: color ?? ComooColors.pink,
              borderRadius: BorderRadius.all(
                Radius.circular(mediaQuery * 32.0),
              ),
            ),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //  SizedBox(height: mediaQuery * 26.0),
                  Container(
                    margin: EdgeInsets.only(
                      top: mediaQuery * 31.0,
                      left: mediaQuery * 10.0,
                      right: mediaQuery * 10.0,
                      bottom: mediaQuery * 10.0,
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: mediaQuery * 8.0,
                    ),
                    child: Text(
                      title,
                      textAlign: titleAlign ?? TextAlign.start,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: mediaQuery * 18.0,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: mediaQuery * 0.0,
                      left: mediaQuery * 10.0,
                      right: mediaQuery * 10.0,
                      bottom: mediaQuery * 10.0,
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: mediaQuery * 8.0,
                    ),
                    child: Text(
                      subtitle,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: mediaQuery * 18.0,
                      ),
                    ),
                  ),
                  ButtonBar(
                    mainAxisSize: MainAxisSize.min,
                    alignment: MainAxisAlignment.center,
                    children: buttons,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  showCustomSnackbar({
    GlobalKey<ScaffoldState> scaffoldKey,
    Widget content,
    Color color,
  }) {
    assert(scaffoldKey != null);
    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: color ?? ComooColors.roxo,
        content: content ??
            Text(
              'Um erro inesperado aconteceu, tente novamente mais tarde',
              style: ComooStyles.textoSnackBar,
            ),
      ),
    );
  }

  showSnackbarError({
    @required GlobalKey<ScaffoldState> scaffoldKey,
    @required String text,
    Color color,
    Duration duration,
  }) {
    assert(scaffoldKey != null);
    final BuildContext context = scaffoldKey.currentContext;
    final ScaffoldState state = scaffoldKey.currentState;
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    state.showSnackBar(
      SnackBar(
        duration: duration ?? Duration(seconds: 3),
        backgroundColor: color ?? COMOO.colors.lead,
        content: Container(
          constraints: BoxConstraints(
            maxHeight: mediaQuery * 30.0,
          ),
          child: FittedBox(
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  text,
                  style: ComooStyles.textoSnackBar,
                  maxLines: 2,
                  softWrap: true,
                  overflow: TextOverflow.clip,
                ),
                GestureDetector(
                  child: Center(
                    child: Icon(Icons.highlight_off),
                  ),
                  onTap: () {
                    scaffoldKey.currentState.hideCurrentSnackBar(
                      reason: SnackBarClosedReason.dismiss,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<String> retrieveDynamicLink() async {
    PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.retrieveDynamicLink();
    if (data == null) {
      data = await FirebaseDynamicLinks.instance.retrieveDynamicLink();
    }
    final Uri deepLink = data?.link;

    if (deepLink != null) {
      String path = deepLink.path;
      if (path.isNotEmpty) {
        return path;
      }
    }
    return null;
  }

  Future<String> decodePath({String path}) async {
    return await _decode(path);
  }

  Future<String> _decode(String code) async {
    var bytesbase64 = base64.decode(code);
    return latin1.decode(bytesbase64);
  }

  Future<String> _encode(String code) async {
    var byteslatin1 = latin1.encode(code);
    return base64.encode(byteslatin1);
  }

  Future<String> createGroupDynamicLink(bool short, Group group) async {
    // String code = await _encode(group.id);
    // final DynamicLinkParameters parameters = DynamicLinkParameters(
    //   socialMetaTagParameters: SocialMetaTagParameters(
    //     description: group.description,
    //     imageUrl: Uri.dataFromString(group.avatar),
    //     title: group.name,
    //   ),
    //   domain: 'comoo.page.link',
    //   link: Uri.parse('https://comoo.io/c/$code'),
    //   androidParameters: AndroidParameters(
    //     packageName: 'io.comoo',
    //     minimumVersion: 0,
    //   ),
    //   dynamicLinkParametersOptions: DynamicLinkParametersOptions(
    //     shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
    //   ),
    //   iosParameters: IosParameters(
    //     bundleId: 'io.comoo',
    //     appStoreId: '1397595044',
    //     minimumVersion: '1.6',
    //   ),
    // );

    // Uri url;
    // if (short) {
    //   final ShortDynamicLink shortLink = await parameters.buildShortLink();
    //   url = shortLink.shortUrl;
    // } else {
    //   url = await parameters.buildUrl();
    // }
    final NetworkApi api = NetworkApi();
    final String message = await api.generateGroupInviteLink(group);
    return message;
  }

  Future<Uri> createInviteDynamicLink(String inviteCode, User user,
      {bool short}) async {
    String code = await _encode(inviteCode);
    String name = user.name.split(' ').first;
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      socialMetaTagParameters: SocialMetaTagParameters(
        description: 'Digite o código de convite $inviteCode para entrar',
        imageUrl: Uri.dataFromString(user.photoURL),
        title: '$name te convidou para COMOO',
      ),
      uriPrefix: 'comoo.page.link',
      link: Uri.parse('https://comoo.io/i/$code'),
      androidParameters: AndroidParameters(
        packageName: 'io.comoo',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        bundleId: 'io.comoo',
        appStoreId: '1397595044',
        minimumVersion: '1.6',
      ),
    );

    Uri url;
    if (short ?? true) {
      final ShortDynamicLink shortLink = await parameters.buildShortLink();
      url = shortLink.shortUrl;
    } else {
      url = await parameters.buildUrl();
    }
    return url;
  }

  checkDynamicLinks(BuildContext context, State widget) async {
    final String path = await retrieveDynamicLink();
    // Check if is a valid link
    print(path);
    if (path != null) {
      User user = currentUser;
      List<String> paths = path.split('/');
      if (user == null) {
        print('login');
        print(path);
        if (paths[1] == 'i') {
          print('invite');
          String uid = paths[2]; //await decodePath(path: paths[2]);
          print(uid);
          if (widget.mounted) {
            print('landing_page to go to sign_up');
            if (uid != null) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => SignUpPage(code: uid),
                ),
              );
            }
          }
        }
        // Navigator.of(context).pushNamed('/login');
        // await showCustomDialog(
        //   context: context,
        //   barrierDismissible: true,
        //   alert: customRoundAlert(
        //     context: context,
        //     title: 'Faça o login para poder acessar a página da comoonidade!',
        //   ),
        // );
      } else {
        print(path);
        if (paths[1] == 'c') {
          print('group');
          String uid = await decodePath(path: paths[2]);
          print(uid);
          if (widget.mounted) {
            print('main_page go to group detail');
            if (uid != null) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => GroupDetailView(id: uid),
                ),
              );
            }
          }
        }
      }
    }
  }

  Future<bool> isConnected() async {
    try {
      await CloudFunctions.instance
          .getHttpsCallable(functionName: 'isOnline')
          .call(<String, String>{}).timeout(Duration(seconds: 15));
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<void> showMessageDialog(BuildContext context, String text) {
    return showCustomDialog(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: text,
      ),
    );
  }

  Future<void> showConnectionErrorMessage(BuildContext context) {
    return showCustomDialog(
      context: context,
      alert: customRoundAlertTitle(
        context: context,
        title: 'Erro de conexão!',
        subtitle:
            'Certifique-se que você está conectado à internet antes de continuar.',
      ),
    );
  }

  Future<void> customCupertinoModal(
      {@required BuildContext context,
      Function(int) onChange,
      List<Widget> children,
      int initialItem}) async {
    onChange = onChange ??= (index) {};
    final double mediaQuery = MediaQuery.of(context).size.width / 400;

    return showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: mediaQuery * 216.0,
          padding: EdgeInsets.only(top: mediaQuery * 6.0),
          color: CupertinoColors.white,
          child: DefaultTextStyle(
            style: TextStyle(
              color: COMOO.colors.lead,
              fontSize: mediaQuery * 22.0,
            ),
            child: GestureDetector(
              // Blocks taps from propagating to the modal sheet and popping.
              onTap: () {
                Navigator.of(context).pop();
              },
              child: SafeArea(
                top: false,
                child: CupertinoPicker(
                  scrollController: FixedExtentScrollController(
                      initialItem: initialItem ?? 0),
                  itemExtent: mediaQuery * 32.0,
                  useMagnifier: true,
                  magnification: mediaQuery * 1.2,
                  backgroundColor: CupertinoColors.white,
                  onSelectedItemChanged: onChange,
                  children: children,
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> shareGroupLink(BuildContext context, Group group) async {
    showCustomDialog(
      barrierDismissible: false,
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Gerando link',
        actions: [],
      ),
    );

    // Uri uri = await createGroupDynamicLink(true, group);
    // print(uri);
    final NetworkApi api = NetworkApi();
    final String message = await api.generateGroupInviteLink(group);
    Share.share('$message').then((dynamic result) {
      Navigator.of(context).pop();
    });
  }

  Future<void> createAndShareInviteCode(context) async {
    final User user = await comoo.currentUser();
    if (user.sellerStatus == 'incomplete' || user.sellerStatus == null) {
      await showIncompleteSignupDialog(context).then((result) {
        if (result != null) if (result) {
          // Navigator.of(context).pushNamed('/user_profile');
          Navigator.of(context).pushNamed('/sign_up_personal');
        }
      });
    } else if (user.sellerStatus == 'waiting') {
      await showWaitingSignupDialog(context);
    } else if (user.sellerStatus == 'accepted') {
      // final String code = await comoo.api.fetchUserInviteCode(user.uid);
      // final Uri uri = await createInviteDynamicLink(code, user);
      // // print(uri);
      // final String message =
      //     'Oi! Estou te indicando pra a COMOO, a mais nova rede social de compra '
      //     'e venda com lucro compartilhado. Crie ou participe de comoonidades, '
      //     'convide amigos e ganhe dinheiro todas as vezes que eles comprarem ou '
      //     'venderem. Use esse código: $code para o seu cadastro ser aprovado! '
      //     'Baixe agora: $uri';
      final NetworkApi api = NetworkApi();
      final String message = await api.fetchInviteMessage();
      await Share.share(message);
      // Share.share(
      //     'Oi! Estou te indicando pra a COMOO, a mais nova rede social de compra e venda. '
      //     'Use esse código: $code para o seu cadastro ser aprovado! Baixe agora: $uri');
    }
  }

  Future<bool> showIncompleteSignupDialog(context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return showCustomDialog<bool>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title:
            'Para começar a vender na COMOO você precisa acrescentar mais dados ao seu cadastro',
        actions: <Widget>[
          new Container(
            height: mediaQuery * 41.0,
            decoration: new BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: new BorderRadius.circular(20.0),
            ),
            child: new FlatButton(
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              child: new Text(
                'DEPOIS',
                style: new TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
          new Container(
            height: mediaQuery * 41.0,
            decoration: new BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: new BorderRadius.circular(20.0),
            ),
            child: new FlatButton(
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: new Text(
                'AGORA',
                style: new TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> showWaitingSignupDialog(context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.all(0.0),
          content: Container(
            child: Container(
              padding: EdgeInsets.all(mediaQuery * 21.0),
              child: new Text(
                'Cadastro pendente. A gente te avisa assim que seu cadastro for aprovado.',
                style: ComooStyles.welcomeMessage,
              ),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: new Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  Future<bool> checkPermissions(ImageSource source) async {
    if (source == ImageSource.camera) {
      final PermissionStatus permission = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.camera);
      print(permission);
      if (permission != PermissionStatus.granted) {
        final Map<PermissionGroup, PermissionStatus> permissions =
            await PermissionHandler()
                .requestPermissions(<PermissionGroup>[PermissionGroup.camera]);
        if (permissions[PermissionGroup.camera] != PermissionStatus.granted) {
          return false;
        }
      }
    }
    return true;
  }

  double responsive(BuildContext context, double value) {
    final double responsivessness = MediaQuery.of(context).size.width / 400;
    return responsivessness * value;
  }

  double responsiveWidth(BuildContext context, double value,
      {double width = 375}) {
    value = value ??= 0;
    final deviceWidth = MediaQuery.of(context).size.width;

    final double responsivessness = (value / width) * deviceWidth;
    return responsivessness;
  }

  double responsiveHeight(BuildContext context, double value,
      {double height = 667}) {
    value = value ??= 0;
    final deviceHeight = MediaQuery.of(context).size.height;

    final double responsivessness = (value / height) * deviceHeight;
    return responsivessness;
  }

  Future<File> fileFromURL(String url) async {
    return await DefaultCacheManager().getSingleFile(url);
  }

  Future<void> welcomeNavigation(BuildContext context, User user) async {
    // final String encoded = json.encode(user.toMap());
    // print(encoded);
    if (user != null) {
      // Go to HOME
      Navigator.of(context).pushReplacementNamed('/home');
      return;
      // Ignore this
      final bool goPersonal = user.cpf == null || (user.cpf ?? '').isEmpty;
      if (goPersonal) {
        print('go fill personal data');
        Navigator.of(context).pushReplacementNamed('/sign_up_personal');
      } else {
        final bool goCategories =
            user.interests == null || (user.interests ?? <dynamic>[]).isEmpty;
        if (goCategories) {
          print('go fill categories');
          Navigator.of(context).pushReplacementNamed('/cat_sign_up');
        } else {
          final Firestore db = Firestore.instance;
          final QuerySnapshot query = await db
              .collection('users')
              .document(user.uid)
              .collection('groups')
              .getDocuments();
          final bool goGroups = query.documents.isEmpty;
          if (goGroups) {
            print('go fill groups');
            Navigator.of(context).pushReplacementNamed('/sign_up_groups');
          } else {
            Navigator.of(context).pushReplacementNamed('/home');
          }
          // Navigator.of(context).pushReplacementNamed('/sign_up_groups');
        }
      }
    } else {
      Navigator.of(context).pushReplacementNamed('/landing_page');
    }
  }

  Size deviceSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }
}
