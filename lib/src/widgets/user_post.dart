import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:comoo/src/utility/style.dart';
import './picker/group_picker.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/group.dart';
//import 'package:comoo/src/comoo/comoo.dart';

class UserPost extends StatefulWidget {
  @override
  State createState() {
    return new _UserPostState();
  }
}

class _UserPostState extends State<UserPost> {
  bool _isLoading = false;
  bool _isNotEmpty = false;
  final txtController = new TextEditingController();
  final _db = Firestore.instance;

  _navigateAndDisplaySelection(BuildContext context) async {
    List selectedGroups = await Navigator.push(context,
        new MaterialPageRoute(builder: (context) => new GroupPicker())) as List;

    var batch = _db.batch();
    if (selectedGroups.isNotEmpty) {
      setState(() {
        _isLoading = true;
      });
      List<Map<String, dynamic>> groups =
          selectedGroups.map((group) => (group as Group).toJson()).toList();
      var postRef = _db.collection("posts").document();
      var post = {
        "id": postRef.documentID,
        "date": new DateTime.now(),
        "message": this.txtController.text,
        "type": "post",
        "groups": groups,
        "user": {
          "uid": currentUser.uid,
          "name": currentUser.name,
          "picture": currentUser.photoURL
        }
      };
      batch.setData(postRef, post);

      selectedGroups.forEach((group) {
        var groupId = group.id;
        var feed = _db
            .collection('groups')
            .document(groupId)
            .collection('feed')
            .document(postRef.documentID);
        batch.setData(feed, {
          "type": "post",
          "post": postRef,
          "from": currentUser.uid,
          "date": new DateTime.now(),
        });
      });

      batch.commit().then((_) {
        _displaySuccessAlert();
      });
    }
  }

  _displaySuccessAlert() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text("PEDIDO ENVIADO", style: ComooStyles.texto_bold),
            content: Text(
              "Seu pedido foi enviado para os grupos selecionados",
              style: ComooStyles.texto,
            ),
          );
        });
    setState(() {
      _isLoading = false;
    });
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? new Container(
            height: 200.0,
            width: 200.0,
            child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new CircularProgressIndicator(),
                  new SizedBox(
                    height: 30.0,
                  ),
                  new Text(
                    "Publicando...",
                    style: ComooStyles.titulos,
                  )
                ]))
        : new Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            new Padding(
              padding: const EdgeInsets.all(16.0),
              child: new Row(
                children: <Widget>[
                  new Container(
                    margin: const EdgeInsets.only(right: 16.0),
                    width: 40.0,
                    height: 40.0,
                    child: new ClipOval(
                      child: new Image(
                        image: AdvancedNetworkImage(currentUser.photoURL),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          currentUser.name,
                          style: ComooStyles.nomes,
                        ),
                        new TextField(
                          controller: txtController,
                          onChanged: (value) {
                            setState(() {
                              _isNotEmpty =
                                  txtController.text.trim().isNotEmpty;
                            });
                          },
                          style: ComooStyles.inputText,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintStyle: ComooStyles.inputText,
                              hintText: "O que você está precisando?"),
                          maxLines: 3,
                        )
                      ],
                    ),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
              ),
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new FlatButton(
                  onPressed: _isNotEmpty
                      ? () {
                          _navigateAndDisplaySelection(context);
                        }
                      : null,
                  child: Text(
                    "ESCOLHER GRUPOS",
                    style: ComooStyles.botao,
                  ),
                  textColor: ComooColors.roxo,
                  disabledTextColor: ComooColors.cinzaClaro,
                )
              ],
            )
          ]);
  }

  @override
  void dispose() {
    txtController.dispose();
    super.dispose();
  }
}
