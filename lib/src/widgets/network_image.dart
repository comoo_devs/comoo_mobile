import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CustomNetworkImage extends StatelessWidget {
  final String url;

  CustomNetworkImage({Key key, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return CachedNetworkImage(
      imageUrl: url,
      fit: BoxFit.cover,
      alignment: Alignment.center,
      height: mediaQuery * 50.0,
      width: mediaQuery * 50.0,
      imageBuilder:
          (BuildContext context, ImageProvider<dynamic> imageProvider) =>
              Center(
                child: CircleAvatar(
                  backgroundImage: imageProvider,
                  radius: mediaQuery * 25.0,
                  backgroundColor: Colors.white,
                ),
              ),
      placeholder: (BuildContext context, String text) => Center(
            child: Container(), //CircularProgressIndicator(),
          ),
    );
  }
}

class CachedCircleAvatar extends StatelessWidget {
  CachedCircleAvatar({Key key, @required this.url, this.size = 50.0})
      : super(key: key);
  final String url;
  final double size;

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    // print('Size: $size');
    return SizedBox(
      height: mediaQuery * size,
      width: mediaQuery * size,
      child: Container(
        child: CachedNetworkImage(
          imageUrl: url,
          fit: BoxFit.cover,
          alignment: Alignment.center,
          // height: mediaQuery * size,
          // width: mediaQuery * size,
          imageBuilder:
              (BuildContext context, ImageProvider<dynamic> imageProvider) =>
                  Center(
                    child: CircleAvatar(
                      backgroundImage: imageProvider,
                      radius: size / 2,
                      backgroundColor: Colors.white,
                    ),
                  ),
          placeholder: (BuildContext context, String text) => Center(
                child: Container(), //CircularProgressIndicator(),
              ),
        ),
      ),
    );
  }
}
