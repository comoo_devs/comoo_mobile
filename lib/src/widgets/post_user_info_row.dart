import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class PostUserInfoRow extends StatelessWidget {
  PostUserInfoRow({
    this.userPhotoURL = '',
    this.userName = '',
    this.location = '',
    this.dateString = '',
    this.groups = const [],
    this.onGroupTap,
    this.onImageTap,
    this.onNameTap,
    this.menu,
  });

  final String userPhotoURL;
  final String userName;
  final String location;
  final String dateString;
  final List groups;
  final VoidCallback onImageTap;
  final VoidCallback onNameTap;
  final Function(String) onGroupTap;
  final Widget menu;

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    // final TextStyle menuItemStyle = TextStyle(
    //   fontFamily: COMOO.fonts.montSerrat,
    //   fontSize: mediaQuery * 15.0,
    //   letterSpacing: mediaQuery * 0.21,
    // );
    return Container(
      padding: EdgeInsets.symmetric(horizontal: mediaQuery * 15.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            onTap: onImageTap,
            child: CachedNetworkImage(
              imageUrl: userPhotoURL,
              fit: BoxFit.cover,
              alignment: Alignment.center,
              height: mediaQuery * 50.0,
              width: mediaQuery * 50.0,
              imageBuilder: (BuildContext context,
                      ImageProvider<dynamic> imageProvider) =>
                  Center(
                child: CircleAvatar(
                  backgroundImage: imageProvider,
                  radius: mediaQuery * 25.0,
                  backgroundColor: Colors.white,
                ),
              ),
              placeholder: (BuildContext context, String text) => Center(
                child: Container(), //CircularProgressIndicator(),
              ),
            ),
          ),
          SizedBox(width: mediaQuery * 10.0),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(top: mediaQuery * 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                      text: userName,
                      style: ComooStyles.nomeFeed,
                      recognizer: TapGestureRecognizer()..onTap = onNameTap,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Container(
                    child: RichText(
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      text: TextSpan(
                          text: '$location ',
                          style: ComooStyles.localizacao,
                          children: <TextSpan>[
                            TextSpan(
                              text: dateString,
                              style: ComooStyles.datas,
                            )
                          ]),
                    ),
                  ),
                  Container(
                    height: mediaQuery * 20.0,
                    child: ListView(
                      key:
                          Key('groups_key/$userName/$userPhotoURL/$dateString'),
                      scrollDirection: Axis.horizontal,
                      controller: ScrollController(keepScrollOffset: false),
                      children: groups == null
                          ? <Widget>[Container()]
                          : groups
                              .map<Widget>((dynamic g) => InkWell(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        right: mediaQuery * 6.0,
                                      ),
                                      child: Center(
                                        child: Text(
                                          g['name'],
                                          textAlign: TextAlign.center,
                                          style:
                                              ComooStyles.localizacao.copyWith(
                                            fontSize: mediaQuery * 13.0,
                                            // height: mediaQuery,
                                          ),
                                        ),
                                      ),
                                    ),
                                    onTap: () => onGroupTap(g['id']),
                                  ))
                              .toList(),
                    ),
                  ),
                ],
              ),
            ),
          ),
          menu ?? Container(),
        ],
      ),
    );
  }
}
