import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InputStream extends StatelessWidget with Reusables {
  const InputStream({
    Key key,
    this.initialData,
    @required this.stream,
    @required this.onChanged,
    this.textCapitalization = TextCapitalization.words,
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.showObscureIcon = false,
    this.onObscureClick,
    this.style,
    this.decoration = const InputDecoration(),
    this.color,
    this.showError = true,
    this.controller,
    this.focus,
    this.textInputAction = TextInputAction.done,
    this.onSubmitted,
    this.enabled = true,
    this.maxLength,
    this.hintText,
    this.textAlign = TextAlign.start,
  }) : super(key: key);

  final String initialData;
  final Stream<String> stream;
  final ValueChanged<String> onChanged;
  final TextCapitalization textCapitalization;
  final TextInputType keyboardType;
  final bool obscureText;
  final bool showObscureIcon;
  final Function onObscureClick;
  final InputDecoration decoration;
  final TextStyle style;
  final Color color;
  final bool showError;
  final TextEditingController controller;
  final FocusNode focus;
  final TextInputAction textInputAction;
  final ValueChanged<String> onSubmitted;
  final bool enabled;
  final int maxLength;
  final String hintText;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) {
    // controller.text = initialData ?? '';
    return StreamBuilder<String>(
      // key: _streamBuilderKey,
      initialData: initialData,
      stream: stream,
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        return Stack(
          children: <Widget>[
            TextField(
              textAlign: textAlign,
              onChanged: onChanged,
              textCapitalization: textCapitalization,
              obscureText: obscureText,
              controller: controller,
              enabled: enabled,
              keyboardType: keyboardType,
              style: style,
              autocorrect: false,
              maxLength: maxLength,
              focusNode: focus,
              textInputAction: textInputAction,
              onSubmitted: onSubmitted,
              decoration: decoration.copyWith(
                errorText: snapshot.error,
                hintText: hintText,
                errorMaxLines: 3,
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.symmetric(vertical: responsive(context, 5)),
              padding: EdgeInsets.only(right: responsive(context, 10)),
              child: showObscureIcon
                  ? InkWell(
                      onTap: () => onObscureClick(obscureText),
                      child: Icon(
                        obscureText ? Icons.visibility_off : Icons.visibility,
                        color: COMOO.colors.medianGray,
                      ),
                    )
                  : Container(),
            )
          ],
        );
      },
    );
  }
}

class DropdownInputStream<T> extends StatelessWidget {
  const DropdownInputStream({
    Key key,
    this.initialData,
    @required this.stream,
    @required this.onTap,
    this.textCapitalization = TextCapitalization.words,
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.style,
    this.decoration = const InputDecoration(),
    this.color,
    this.showError = true,
    this.controller,
    this.focus,
    this.enabled = true,
  }) : super(key: key);

  final T initialData;
  final Stream<T> stream;
  final Function onTap;
  final TextCapitalization textCapitalization;
  final TextInputType keyboardType;
  final bool obscureText;
  final InputDecoration decoration;
  final TextStyle style;
  final Color color;
  final bool showError;
  final TextEditingController controller;
  final FocusNode focus;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Stack(
      children: <Widget>[
        StreamBuilder<T>(
          // key: _streamBuilderKey,
          initialData: initialData,
          stream: stream,
          builder: (BuildContext context, AsyncSnapshot<T> snapshot) {
            return TextField(
              textCapitalization: textCapitalization,
              obscureText: obscureText,
              controller: controller,
              enabled: enabled,
              autocorrect: false,
              onTap: () {
                onTap();
              },
              focusNode: focus,
              keyboardType: keyboardType,
              style: style,
              decoration: decoration.copyWith(
                errorText: snapshot.error,
              ),
            );
          },
        ),
        Positioned(
          bottom: mediaQuery * 5.0,
          right: mediaQuery * 0.0,
          child: const Icon(Icons.expand_more),
        ),
      ],
    );
  }
}

class CupertinoDropdownInputStream<T> extends StatelessWidget with Reusables {
  const CupertinoDropdownInputStream({
    Key key,
    this.initialData,
    @required this.stream,
    @required this.onChange,
    @required this.convertToString,
    @required this.children,
    @required this.items,
    this.textCapitalization = TextCapitalization.words,
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.style,
    this.decoration = const InputDecoration(),
    this.color,
    this.showError = true,
    this.controller,
    this.focus,
    this.enabled = true,
  })  : assert(items != null),
        assert(children != null),
        super(key: key);

  final T initialData;
  final Stream<T> stream;
  final void Function(T) onChange;
  final String Function(T) convertToString;
  final TextCapitalization textCapitalization;
  final TextInputType keyboardType;
  final bool obscureText;
  final InputDecoration decoration;
  final TextStyle style;
  final Color color;
  final bool showError;
  final TextEditingController controller;
  final FocusNode focus;
  final bool enabled;
  final List<Widget> children;
  final List<T> items;

  @override
  Widget build(BuildContext context) {
    // final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return StreamBuilder<T>(
      // key: _streamBuilderKey,
      initialData: initialData,
      stream: stream,
      builder: (BuildContext context, AsyncSnapshot<T> snapshot) {
        return TextField(
          textCapitalization: textCapitalization,
          obscureText: obscureText,
          controller: controller,
          enabled: enabled,
          autocorrect: false,
          onTap: () async {
            FocusScope.of(context).requestFocus(FocusNode());
            SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
            int initialItem = items.indexOf(snapshot.data);
            initialItem = initialItem == -1 ? 0 : initialItem;
            // onTap();
            await customCupertinoModal(
              context: context,
              initialItem: initialItem,
              onChange: (int index) {
                final T item = items[index];
                onChange(item);
                controller.text = convertToString(item);
              },
              children: children,
            );
          },
          focusNode: focus,
          keyboardType: keyboardType,
          style: style,
          decoration: decoration.copyWith(
            errorText: snapshot.error,
            // suffix: const Icon(Icons.expand_more),
          ),
        );
      },
    );
  }
}
