import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CustomButton extends StatelessWidget with Reusables {
  CustomButton({
    @required this.text,
    @required this.onPressed,
    this.filled = true,
    this.color = const Color(0xFF00A1AD),
    this.textColor = Colors.white,
  }) : assert(text != null);
  final String text;
  final Function onPressed;
  final bool filled;
  final Color color;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 0.0,
      color: color,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(responsive(context, 30.0)),
      ),
      padding: EdgeInsets.all(0),
      child: FittedBox(
          child: Text(
        text,
        style: TextStyle(
          fontFamily: COMOO.fonts.montSerrat,
          fontSize: responsive(context, 11.0),
          color: textColor,
          fontWeight: FontWeight.w600,
        ),
      )),
      onPressed: onPressed,
    );
  }
}

class FacebookButton extends StatelessWidget with Reusables {
  FacebookButton({@required this.onPressed, this.text = 'Entrar com Facebook'});
  final Function onPressed;
  final String text;
  @override
  Widget build(BuildContext context) {
    final TextStyle buttonTextStyle = Theme.of(context).textTheme.button;
    return Container(
      width: MediaQuery.of(context).size.width * 0.65,
      height: MediaQuery.of(context).size.height * 0.055,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(responsive(context, 5)),
        ),
        color: const Color(0xFF4968AD),
        onPressed: onPressed,
        padding: EdgeInsets.all(0),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                horizontal: responsiveWidth(context, 48),
              ),
              child: FittedBox(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: buttonTextStyle.copyWith(
                    fontSize: responsive(context, 12),
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                // alignment: Alignment.centerLeft,
                constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * 0.075,
                  maxHeight: MediaQuery.of(context).size.width * 0.08,
                ),
                margin: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.018,
                  top: MediaQuery.of(context).size.height * 0.01,
                  bottom: MediaQuery.of(context).size.height * 0.01,
                ),
                child: SizedBox.expand(
                  child: Container(
                    alignment: Alignment.bottomRight,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          BorderRadius.circular(responsive(context, 3)),
                    ),
                    child: FittedBox(
                      child: Icon(
                        FontAwesomeIcons.facebookF,
                        color: const Color(0xFF4968AD),
                        size: MediaQuery.of(context).size.width * 0.063,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GoogleButton extends StatelessWidget with Reusables {
  GoogleButton({@required this.onPressed, this.text = 'Entrar com Google'});
  final Function onPressed;
  final String text;
  @override
  Widget build(BuildContext context) {
    final TextStyle buttonTextStyle = Theme.of(context).textTheme.button;
    final Size device = MediaQuery.of(context).size;
    return Container(
      width: MediaQuery.of(context).size.width * 0.65,
      height: MediaQuery.of(context).size.height * 0.055,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        color: const Color(0xFFDF492F),
        onPressed: onPressed,
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(
            horizontal: responsiveWidth(context, 37.87),
          ),
          child: FittedBox(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: buttonTextStyle.copyWith(
                fontSize: responsive(context, 12),
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
