import 'package:comoo/src/blocs/group_tile_bloc.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/pages/common/group/group_detail.dart';
import 'package:comoo/src/pages/signup/signup_groups/signup_groups_view.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

import 'buttons.dart';
import 'network_image.dart';

class GroupTile extends StatefulWidget {
  GroupTile({@required this.group, this.fromSignup = false})
      : super(key: Key('GroupTile@${group.id}'));
  final Group group;
  final bool fromSignup;
  @override
  State<StatefulWidget> createState() => _GroupTileState();
}

class _GroupTileState extends State<GroupTile> {
  GroupTileBloc bloc;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    bloc = GroupTileBloc(widget.group);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GroupTileBloc>(
      bloc: bloc,
      child: GroupTileStateless(
        group: widget.group,
        fromSignup: widget.fromSignup,
      ),
    );
  }
}

class GroupTileStateless extends StatelessWidget with Reusables {
  const GroupTileStateless({@required this.group, this.fromSignup = false});
  final Group group;
  final bool fromSignup;
  @override
  Widget build(BuildContext context) {
    final GroupTileBloc bloc = BlocProvider.of<GroupTileBloc>(context);
    final TextStyle titleStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: responsive(context, 14.0),
      color: COMOO.colors.turquoise,
      fontWeight: FontWeight.w600,
      letterSpacing: responsive(context, 0.257),
    );
    final TextStyle subtitleStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: responsive(context, 12.0),
      color: COMOO.colors.gray,
      fontWeight: FontWeight.w500,
    );
    final TextStyle descriptionStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: responsive(context, 14.0),
      color: COMOO.colors.lead,
      fontWeight: FontWeight.w500,
    );
    // print(group.address);
    return ListTile(
      // isThreeLine: true,
      leading: CachedCircleAvatar(
        url: group.avatar,
      ),
      title: GestureDetector(
        onTap: () => fromSignup
            ? openGroupFromSignup(context, bloc)
            : openGroup(context, bloc),
        child: Stack(
          children: <Widget>[
            Image.asset(
              group.askToJoin
                  ? 'images/padlock_closed.png'
                  : 'images/padlock_open.png',
              width: responsive(context, 14.0),
              height: responsive(context, 20.0),
            ),
            // SizedBox(width: responsive(context, 10.0)),
            Container(
              margin: EdgeInsets.only(left: responsive(context, 18.0)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.ltr,
                children: <Widget>[
                  Text(
                    '${group.name}',
                    style: titleStyle,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    group.subtitle,
                    style: subtitleStyle,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    '${group.description ?? ''}',
                    style: descriptionStyle,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      trailing: group.admin
          ? null
          : Container(
              constraints: BoxConstraints.tight(
                  Size(responsive(context, 77.0), responsive(context, 24.0))),
              child: StreamBuilder<bool>(
                initialData: false,
                stream: bloc.isMember,
                builder: (BuildContext context, AsyncSnapshot<bool> isMember) {
                  return group.askToJoin
                      ? CustomButton(
                          text: isMember.data ? 'SOLICITADO' : 'SOLICITAR',
                          onPressed: isMember.data
                              ? () => bloc.requestExit()
                              : () => bloc.requestEntry(),
                          color: isMember.data
                              ? Theme.of(context).disabledColor
                              : COMOO.colors.purple,
                        )
                      : CustomButton(
                          text: isMember.data ? 'ENTROU' : 'ENTRAR',
                          onPressed: isMember.data
                              ? () => bloc.requestExit()
                              : () => bloc.requestEntry(),
                          color: isMember.data
                              ? Theme.of(context).disabledColor
                              : COMOO.colors.turquoise,
                        );
                },
              ),
            ),
    );
  }

  Future<void> openGroupFromSignup(
      BuildContext context, GroupTileBloc bloc) async {
    final bool result =
        await Navigator.of(context).push(MaterialPageRoute<bool>(
      builder: (BuildContext context) => SignupGroupsView(group: group),
    ));
    if (result ?? false) {
      bloc.requestEntry();
    }
  }

  Future<void> openGroup(BuildContext context, GroupTileBloc bloc) async {
    await Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => GroupDetailView(group: group),
    ));
  }
}
