import 'package:flutter/material.dart';
import 'dart:math';
import 'package:comoo/src/utility/style.dart';

class DotsIndicator extends AnimatedWidget {
  DotsIndicator({
    this.controller,
    this.itemCount,
    this.onPageSelected,
    this.color: Colors.white,
    Key key,
  }) : super(listenable: controller, key: key);
  final ScrollController _controller = ScrollController();

  /// The PageController that this DotsIndicator is representing.
  final PageController controller;

  /// The number of items managed by the PageController
  final int itemCount;

  /// Called when a dot is tapped
  final ValueChanged<int> onPageSelected;

  /// The color of the dots.
  ///
  /// Defaults to `Colors.white`.
  final Color color;

  // The base size of the dots
  static const double _kDotSize = 7.0;

  // The increase in the size of the selected dot
  static const double _kMaxZoom = 1.0;

  // The distance between the center of each dot
  static const double _kDotSpacing = 12.0;

  Widget _buildDot(int index) {
    if (!controller.hasClients) {
      return Container();
    }
    final int page = controller?.position?.minScrollExtent == null
        ? (controller?.initialPage ?? 0)
        : (controller?.page?.toInt() ?? 0);
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - (page - index).abs(),
      ),
    );
    double zoom = 1.0 + (_kMaxZoom - 1.0) * selectedness;
    return Container(
      width: _kDotSpacing,
      child: Center(
        child: Material(
          color: page == index ? ComooColors.chumbo : ComooColors.cinzaClaro,
          type: MaterialType.circle,
          child: Container(
            width: _kDotSize * zoom,
            height: _kDotSize * zoom,
            child: InkWell(
              onTap: () => onPageSelected(index),
            ),
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return Container(
      height: 25.0,
      width: _kDotSpacing * itemCount,
      alignment: FractionalOffset.center,
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        controller: _controller,
        children: List<Widget>.generate(itemCount, _buildDot),
      ),
    );
  }
}
