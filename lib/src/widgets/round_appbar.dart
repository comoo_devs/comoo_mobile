import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';

class CustomAppBar extends AppBar {
  CustomAppBar.round({
    Key key,
    @required Color color,
    @required Widget title,
    List<Widget> actions,
    Widget leading,
    IconThemeData iconTheme,
    Brightness brightness,
  }) : super(
          centerTitle: true,
          brightness: brightness,
          iconTheme: iconTheme ?? IconThemeData(color: Colors.white),
          textTheme: TextTheme(title: ComooStyles.appBarTitleWhite),
          elevation: 0.0,
          title: title,
          actions: actions,
          backgroundColor: color,
          leading: leading,
          // backgroundColor: Colors.transparent,
          // flexibleSpace: Container(
          //   decoration: BoxDecoration(
          //     color: color,
          //     borderRadius:
          //         BorderRadius.vertical(bottom: Radius.circular(10.0)),
          //   ),
          // ),
        );
}
