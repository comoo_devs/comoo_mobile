import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

enum TextComposerPosition { top, bottom }

class TextComposer extends StatelessWidget with Reusables {
  TextComposer({
    @required this.child,
    this.hintText,
    this.prefixIcon,
    this.suffixIcon,
    this.enabled = true,
    this.controller,
    this.onChanged,
    this.onSubmitted,
    this.onPressed,
    this.position = TextComposerPosition.bottom,
  });

  final Widget child;
  final String hintText;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final bool enabled;
  final TextEditingController controller;
  final Function(String) onChanged;
  final Function(String) onSubmitted;
  final Function() onPressed;
  final TextComposerPosition position;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        child ?? Container(),
        Positioned(
          left: 0,
          right: 0,
          top: position == TextComposerPosition.top ? 0 : null,
          bottom: position == TextComposerPosition.bottom ? 0 : null,
          child: _textComposer(context),
        ),
      ],
    );
  }

  Widget _textComposer(context) {
    return IconTheme(
      data: IconThemeData(color: COMOO.colors.lead),
      child: Container(
        height: responsiveHeight(context, 50),
        color: COMOO.colors.lightGray,
        padding: EdgeInsets.symmetric(
          horizontal: responsiveWidth(context, 5),
        ),
        child: Row(
          children: <Widget>[
            prefixIcon ?? Container(),
            SizedBox(width: responsiveWidth(context, 5)),
            Flexible(
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    height: responsiveHeight(context, 35),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(
                        responsiveWidth(context, 20),
                      ),
                    ),
                    padding: EdgeInsets.only(
                      right: responsiveWidth(context, 36),
                    ),
                    child: TextField(
                      enabled: enabled,
                      controller: controller,
                      onChanged: onChanged,
                      textCapitalization: TextCapitalization.sentences,
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: responsiveWidth(context, 16),
                        fontWeight: FontWeight.w500,
                        color: ComooColors.chumbo,
                      ),
                      onSubmitted: onSubmitted,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(
                          left: responsiveWidth(context, 10),
                          top: responsiveHeight(context, 10),
                        ),
                        border: InputBorder.none,
                        hintStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: responsiveWidth(context, 14),
                        ),
                        hintText: hintText,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    bottom: 0,
                    right: responsiveWidth(context, 0),
                    child: Container(
                      child: IconButton(
                        icon: Icon(
                          ComooIcons.chatSend,
                          size: responsiveWidth(context, 35),
                        ),
                        padding: EdgeInsets.all(0),
                        iconSize: responsiveWidth(context, 35),
                        onPressed: onPressed,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: responsiveWidth(context, 5)),
            suffixIcon ?? Container(),
          ],
        ),
      ),
    );
  }
}
