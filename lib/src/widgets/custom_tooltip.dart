import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomToolTip extends StatelessWidget {
  CustomToolTip({@required Widget child, @required String text})
      : _text = text ?? '',
        _child = child ?? Container();

  final Widget _child;
  final String _text;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: _child,
      onLongPress: () async {
        final RenderBox button = context.findRenderObject();
        final RenderBox overlay =
            Overlay.of(context).context.findRenderObject();
        final RelativeRect position = RelativeRect.fromRect(
          Rect.fromPoints(
            button.localToGlobal(Offset.zero, ancestor: overlay),
            button.localToGlobal(button.size.topLeft(Offset.zero),
                ancestor: overlay),
          ),
          Offset.zero & overlay.size,
        );

        final bool result = await showMenu(
          context: context,
          position: position,
          items: <PopupMenuEntry<bool>>[
            PopupMenuItem(
              value: true,
              child: Text('Copiar'),
            ),
          ],
        );
        if (result ?? false) {
          print('copied $_text');
          Clipboard.setData(ClipboardData(text: _text));
        }
      },
    );
  }
}
