import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/comoo/comments.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/comoo.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/chat/direct_chat.dart';
import 'package:comoo/src/pages/common/product/product_page.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../network_image.dart';

class ProductCommentsView extends StatefulWidget {
  final Product product;

  ProductCommentsView(this.product);

  @override
  State<ProductCommentsView> createState() => _ProductCommentsState();
}

// class ProductCommentsViewState extends State<ProductCommentsView>
//     with Reusables {
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   final Firestore _db = Firestore.instance;
//   final Product product;
//   final TextEditingController _textController = TextEditingController();
//   final ScrollController _scrollController = ScrollController();
//   bool _isComposing = false;
//   // final NetworkApi api = NetworkApi();

//   // final BehaviorSubject<List<User>> _friends = BehaviorSubject<List<User>>();
//   // Function(List<User>) get addFriends => _friends.sink.add;
//   // Stream<List<User>> get friends => _friends.stream;
//   // final BehaviorSubject<bool> _showMentionModal = BehaviorSubject<bool>();
//   // Function(bool) get changeMentionModal => _showMentionModal.sink.add;
//   // Stream<bool> get showMentionModal => _showMentionModal.stream;
//   // final BehaviorSubject<String> _mentionText = BehaviorSubject<String>();
//   // Function(String) get addMentionText => _mentionText.sink.add;
//   // Stream<String> get mentionText => _mentionText.stream;
//   // final ScrollController mentionController = ScrollController();

//   List<Comment> _comments = List<Comment>();

//   ProductCommentsViewState(this.product);

//   _loadProductComments() async {
//     QuerySnapshot query = await _db
//         .collection('products')
//         .document(product.id)
//         .collection('comments')
//         .orderBy('date')
//         .getDocuments();

//     List<Future> fetchCommentInfo = List<Future>();

//     query.documents.forEach((DocumentSnapshot snap) {
//       DocumentReference userRef = snap['from'];
//       fetchCommentInfo.add(userRef.get().then((userSnap) {
//         final User user = User.fromSnapshot(userSnap);
//         // Map<String, DocumentReference> refs = <String, DocumentReference>{};
//         // if (snap['mentions'] != null &&
//         //     snap['mentions'] is Map<dynamic, dynamic>) {
//         //   final Map<dynamic, dynamic> map =
//         //       (snap['mentions'] as Map<dynamic, dynamic>);
//         //   for (dynamic k in map.keys) {
//         //     final String key = k as String;
//         //     final DocumentReference ref = map[k] as DocumentReference;
//         //     refs.putIfAbsent(key, () => ref);
//         //   }
//         // }
//         // return Comment.fromSnapshot(snap, user, refs: refs);

//         return Comment(
//             snap['text'],
//             user,
//             (snap['date'] is Timestamp)
//                 ? DateTime.fromMillisecondsSinceEpoch(
//                     (snap['date'] as Timestamp).millisecondsSinceEpoch)
//                 : (snap['date'] as DateTime));
//       }));
//     });

//     Future.wait(fetchCommentInfo).then((List comments) {
//       if (comments.length > 0) {
//         setState(() {
//           _comments.addAll(comments.cast<Comment>());
//         });
//       }
//     });
//   }

//   Future<void> _postNewComment(Comment comment) async {
//     // Future<void> _postNewComment(String text) async {
//     // final Map<String, DocumentReference> refs = _fetchRefs(text);
//     // final Comment comment = Comment(text, currentUser, DateTime.now());
//     // comment.addRefs(refs);
//     // final bool result = await api.createProductComment(context,
//     //     product: product, comment: comment);

//     final DocumentReference productRef =
//         _db.collection('products').document(product.id);
//     final DocumentReference newComment =
//         productRef.collection('comments').document();
//     final WriteBatch batch = _db.batch();

//     batch.setData(newComment, <String, dynamic>{
//       'text': comment.text,
//       'from': _db.collection('users').document(comment.user.uid),
//       'date': comment.date
//     });

//     product.commentsQty++;
//     batch.updateData(
//         productRef, <String, int>{'commentsQty': product.commentsQty});

//     return batch.commit();
//     // if (result ?? false) {
//     //   return comment;
//     // }
//     // return null;
//   }

//   Future<void> _handleSubmitted(String text) async {
//     _textController.clear();
//     // changeMentionModal(false);
//     setState(() {
//       _isComposing = false;
//     });
//     if (text.isEmpty) {
//       return;
//     }
//     Comment comment = Comment(text, currentUser, DateTime.now());
//     await _postNewComment(comment);
//     // final Comment comment = await _postNewComment(text);

//     if (mounted && comment != null) {
//       setState(() {
//         _comments.add(comment);
//       });
//       _scrollToTheBottom();
//     }
//   }

//   void _scrollToTheBottom() {
//     // print(_scrollController.position.maxScrollExtent);
//     _scrollController.animateTo(
//       _scrollController.position.maxScrollExtent * 1.05,
//       duration: const Duration(milliseconds: 1000),
//       curve: Curves.decelerate,
//     );
//   }

//   Widget _buildTextComposer() {
//     return IconTheme(
//         data: IconThemeData(color: ComooColors.roxo),
//         child: Container(
//           margin: const EdgeInsets.symmetric(horizontal: 8.0),
//           child: Row(
//             children: <Widget>[
//               Flexible(
//                 child: TextField(
//                   controller: _textController,
//                   onChanged: (String text) {
//                     // _checkIfHasMentions(text);
//                     if (mounted) {
//                       setState(() {
//                         _isComposing = text.length > 0;
//                       });
//                     }
//                   },
//                   textCapitalization: TextCapitalization.sentences,
//                   onSubmitted: _handleSubmitted,
//                   decoration: InputDecoration.collapsed(
//                       hintText: 'Escreva um comentário'),
//                 ),
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 4.0),
//                 child: IconButton(
//                     icon: Icon(COMOO.icons.chatSend.iconData),
//                     onPressed: _isComposing
//                         ? () => _handleSubmitted(_textController.text)
//                         : null),
//               ),
//             ],
//           ),
//         ));
//   }

//   // void _checkIfHasMentions(String text) {
//   //   final int atIndex = text.lastIndexOf('@');
//   //   // print('FOUND @: $atIndex');
//   //   final bool found = atIndex == 0 ||
//   //       (atIndex != -1 && text.substring(atIndex - 1, atIndex) == ' ');
//   //   // print('Found? $found');
//   //   if (found) {
//   //     final String mention = text.substring(atIndex, text.length);
//   //     // print(mention);
//   //     if (RegExp(r' ').hasMatch(mention)) {
//   //       changeMentionModal(false);
//   //     } else {
//   //       // print('Is mention: ${mention.substring(1, mention.length)}');
//   //       changeMentionModal(true);
//   //       addMentionText(mention.substring(1, mention.length));
//   //     }
//   //   } else {
//   //     changeMentionModal(false);
//   //   }
//   // }

//   @override
//   void initState() {
//     super.initState();
//     _loadProductComments();
//     // _loadUserFriends();
//     _scrollController.addListener(() {
//       // print('${_scrollController.offset} ${_scrollController.position.maxScrollExtent}');
//     });
//   }

//   @override
//   void dispose() {
//     // _friends?.close();
//     // _mentionText?.close();
//     // _showMentionModal?.close();
//     super.dispose();
//   }

//   // Future<void> _loadUserFriends() async {
//   //   QuerySnapshot query = await _db
//   //       .collection('users')
//   //       .document(currentUser.uid)
//   //       .collection('friends')
//   //       .getDocuments();

//   //   print('Friends: ${query.documents.length}');
//   //   final List<User> friendList = <User>[];
//   //   for (DocumentSnapshot snap in query.documents) {
//   //     final DocumentReference ref = snap.data['id'];
//   //     // print('Ref: $ref ${ref.documentID}');
//   //     final DocumentSnapshot snapshot = await ref.get();
//   //     friendList.add(User.fromSnapshot(snapshot));
//   //   }
//   //   addFriends(friendList);
//   // }

//   @override
//   Widget build(BuildContext context) {
//     print('BUILD');
//     return Scaffold(
//       key: _scaffoldKey,
//       appBar: AppBar(
//         elevation: 0.5,
//         brightness: Theme.of(context).brightness,
//         title: Text('COMENTÁRIOSs', style: ComooStyles.appBarTitle),
//         backgroundColor: Colors.white,
//         iconTheme: ComooIcons.roxo,
//       ),
//       body: GestureDetector(
//         onTap: () {
//           FocusScope.of(context).requestFocus(FocusNode());
//           SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
//         },
//         child: Column(
//           // mainAxisSize: MainAxisSize.min,
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: <Widget>[
//             Flexible(
//               child: ListView.builder(
//                 addAutomaticKeepAlives: true,
//                 semanticChildCount: 15,
//                 shrinkWrap: true,
//                 controller: _scrollController,
//                 itemCount: _comments.length,
//                 itemBuilder: (BuildContext c, int index) {
//                   Comment comment = _comments[index];
//                   // return _buildCommentTile(context, index, comment);
//                   print('${comment.text}');
//                   return ListTile(
//                     key: GlobalObjectKey(index),
//                     leading: CircleAvatar(
//                       backgroundImage: NetworkImage(comment.user.photoURL),
//                     ),
//                     title: RichText(
//                         text: TextSpan(
//                             text: '${comment.user.name} ',
//                             style: ComooStyles.texto_bold,
//                             children: [
//                           TextSpan(style: ComooStyles.texto, text: comment.text)
//                         ])),
//                     subtitle:
//                         Text(timeago.format(comment.date, locale: 'pt_BR')),
//                   );
//                 },
//               ),
//             ),
//             // StreamBuilder<bool>(
//             //   initialData: false,
//             //   stream: showMentionModal,
//             //   builder: (BuildContext c, AsyncSnapshot<bool> snapMention) {
//             //     return AnimatedContainer(
//             //       curve: Curves.ease,
//             //       duration: const Duration(milliseconds: 400),
//             //       constraints: BoxConstraints(
//             //           maxHeight:
//             //               snapMention.data ? responsive(context, 120.0) : 0.0),
//             //       child: _buildMentionWidget(context),
//             //     );
//             //   },
//             // ),
//             Divider(height: 1.0),
//             Container(
//               height: responsive(context, 55.0),
//               decoration: BoxDecoration(color: Theme.of(context).cardColor),
//               child: _buildTextComposer(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

class CommentTextSpan {
  CommentTextSpan({@required this.style, @required this.text, this.ref});

  TextStyle style;
  String text;
  DocumentReference ref;
}

class _ProductCommentsState extends State<ProductCommentsView> with Reusables {
  final Firestore _db = Firestore.instance;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _textController = TextEditingController();
  final ScrollController _scrollController = ScrollController();
  bool _isComposing = false;
  final NetworkApi api = NetworkApi();
  List<Comment> _comments = List<Comment>();

  final BehaviorSubject<List<Friend>> _friends =
      BehaviorSubject<List<Friend>>();
  final BehaviorSubject<bool> _showMentionModal = BehaviorSubject<bool>();
  final BehaviorSubject<String> _mentionText = BehaviorSubject<String>();
  final BehaviorSubject<bool> _hasRequested =
      BehaviorSubject<bool>.seeded(true);

  Function(List<Friend>) get addFriends => _friends.sink.add;
  Function(bool) get changeMentionModal => _showMentionModal.sink.add;
  Function(String) get addMentionText => _mentionText.sink.add;
  Function(bool) get editHasRequest => _hasRequested.sink.add;

  Stream<List<Friend>> get friends => _friends.stream;
  Stream<bool> get showMentionModal => _showMentionModal.stream;
  Stream<String> get mentionText => _mentionText.stream;
  Stream<bool> get hasRequested => _hasRequested.stream;

  final ScrollController mentionController = ScrollController();

  Future<Comment> _postNewComment(String text) async {
    final Map<String, DocumentReference> refs = _fetchRefs(text);
    final Comment comment = Comment(text, currentUser, DateTime.now());
    comment.addRefs(refs);
    api.createProductComment(context,
        product: widget.product, comment: comment);
    return comment;
  }

  void _checkIfHasMentions(String text) {
    final int atIndex = text.lastIndexOf('@');
    // print('FOUND @: $atIndex');
    final bool found = atIndex == 0 ||
        (atIndex != -1 && text.substring(atIndex - 1, atIndex) == ' ');
    // print('Found? $found');
    if (found) {
      final String mention = text.substring(atIndex, text.length);
      // print(mention);
      if (RegExp(r' ').hasMatch(mention)) {
        changeMentionModal(false);
      } else {
        // print('Is mention: ${mention.substring(1, mention.length)}');
        changeMentionModal(true);
        addMentionText(mention.substring(1, mention.length));
      }
    } else {
      changeMentionModal(false);
    }
  }

  _loadProductComments() async {
    QuerySnapshot query = await _db
        .collection('products')
        .document(widget.product.id)
        .collection('comments')
        .orderBy('date')
        .getDocuments();

    List<Future> fetchCommentInfo = List<Future>();

    query.documents.forEach((DocumentSnapshot snap) {
      DocumentReference userRef = snap['from'];
      fetchCommentInfo.add(userRef.get().then((userSnap) {
        final User user = User.fromSnapshot(userSnap);
        Map<String, DocumentReference> refs = <String, DocumentReference>{};
        if (snap['mentions'] != null &&
            snap['mentions'] is Map<dynamic, dynamic>) {
          final Map<dynamic, dynamic> map =
              (snap['mentions'] as Map<dynamic, dynamic>);
          for (dynamic k in map.keys) {
            final String key = k as String;
            final DocumentReference ref = map[k] as DocumentReference;
            refs.putIfAbsent(key, () => ref);
          }
        }
        return Comment.fromSnapshot(snap, user, refs: refs);

        // return Comment(
        //     snap['text'],
        //     user,
        //     (snap['date'] is Timestamp)
        //         ? DateTime.fromMillisecondsSinceEpoch(
        //             (snap['date'] as Timestamp).millisecondsSinceEpoch)
        //         : (snap['date'] as DateTime));
      }));
    });

    Future.wait(fetchCommentInfo).then((List comments) {
      if (comments.length > 0) {
        setState(() {
          _comments.addAll(comments.cast<Comment>());
        });
      }
    });
  }

  Future<void> fetchUserRequest() async {
    bool _hasRequested = false;

    if (currentUser.uid == widget.product.user['uid']) {
      _hasRequested = true;
    } else {
      _hasRequested = await comoo.api
          .fetchUserHasRequestedProduct(currentUser.uid, widget.product.id);
    }
    editHasRequest(_hasRequested);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    fetchUserRequest();
  }

  @override
  void initState() {
    super.initState();
    _loadProductComments();
    _loadUserFriends();
    _scrollController.addListener(() {
      // print('${_scrollController.offset} ${_scrollController.position.maxScrollExtent}');
    });
    _textController.addListener(() {
      if (_showMentionModal.value ?? false) {
        if (mentionController?.hasClients ?? false) {
          mentionController.jumpTo(0.0);
        }
      }
    });
  }

  @override
  void dispose() {
    _friends?.close();
    _mentionText?.close();
    _showMentionModal?.close();
    _hasRequested?.close();
    super.dispose();
  }

  Future<void> _loadUserFriends() async {
    QuerySnapshot query = await _db
        .collection('users')
        .document(currentUser.uid)
        .collection('friends')
        .getDocuments();

    // print('Friends: ${query.documents.length}');
    final List<Friend> friendList = <Friend>[];
    final List<Friend> aux =
        query.documents.map<Friend>((DocumentSnapshot snap) {
      final String name = snap.data['name'] ?? '';
      final String photoURL = snap.data['photoURL'] ?? '';
      final String nickname = snap.data['nickname'] ?? '';
      if (name.isEmpty || photoURL.isEmpty || nickname.isEmpty) {
        // print('|>>>  ${snap.documentID}  -  $name  \n$photoURL');
        return null;
      }
      return Friend(
        uid: snap.documentID,
        photoURL: photoURL.trim(),
        name: name.trim(),
        nickname: nickname.trim(),
      );
    }).toList();
    aux.removeWhere((Friend f) => f == null);
    friendList.addAll(aux);
    // for (DocumentSnapshot snap in query.documents) {
    //   final DocumentReference ref = snap.data['id'];
    //   // print('Ref: $ref ${ref.documentID}');
    //   final DocumentSnapshot snapshot = await ref.get();
    //   // print('${snapshot.documentID} ${snapshot.data}');
    //   if (mounted) {
    //     friendList.add(User.fromSnapshot(snapshot));
    //   }
    // }
    if (mounted) {
      addFriends(friendList);
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        title: Text(
          'comentários',
          style: TextStyle(
            fontFamily: COMOO.fonts.montSerrat,
            fontWeight: FontWeight.w500,
            color: Colors.white,
            fontSize: responsive(context, 15),
            letterSpacing: responsive(context, -0.125),
          ),
        ),
        color: COMOO.colors.lead,
      ),
      // appBar: AppBar(
      //   elevation: 0.5,
      //   brightness: Theme.of(context).brightness,
      //   title: Text('COMENTÁRIOS', style: ComooStyles.appBarTitle),
      //   backgroundColor: Colors.white,
      //   iconTheme: ComooIcons.roxo,
      // ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
          SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
        },
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            StreamBuilder<bool>(
              stream: hasRequested,
              builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                final bool userHasRequested = snapshot.data ?? true;
                return ProductTile(
                  widget.product,
                  onRequested:
                      userHasRequested ? null : () => requestProduct(context),
                );
              },
            ),
            Divider(),
            Expanded(
              child: ListView.builder(
                addAutomaticKeepAlives: true,
                semanticChildCount: 15,
                shrinkWrap: true,
                controller: _scrollController,
                itemCount: _comments.length,
                itemBuilder: (BuildContext c, int index) {
                  Comment comment = _comments[index];
                  return _buildCommentTile(context, index, comment);
                },
              ),
            ),
            StreamBuilder<bool>(
              initialData: false,
              stream: showMentionModal,
              builder: (BuildContext c, AsyncSnapshot<bool> snapMention) {
                return AnimatedContainer(
                  curve: Curves.ease,
                  duration: const Duration(milliseconds: 400),
                  constraints: BoxConstraints(
                      maxHeight: snapMention.data
                          ? responsive(context, height / 3)
                          : 0.0),
                  child: _buildMentionWidget(context),
                );
              },
            ),
            Divider(height: 1.0),
            Container(
              height: responsive(context, 55.0),
              decoration: BoxDecoration(color: Theme.of(context).cardColor),
              child: _buildTextComposer(),
            ),
          ],
        ),
      ),
    );
  }

  Map<String, DocumentReference> _fetchRefs(String text) {
    final String pattern = '@';
    final Iterable<Match> matches = pattern.allMatches(text);
    // print('IsEmpty? ${matches.length}');
    if (matches.isNotEmpty) {
      final List<String> nicknames = <String>[];
      for (Match m in matches) {
        final String match =
            text.substring(m.end, text.length).split(' ').first;
        nicknames.add(match);
        // print('${m.start} ${m.end} ($match)');
      }
      final List<Friend> friends =
          _friends.hasValue ? _friends.value : <Friend>[];
      if (friends.isNotEmpty) {
        final List<Friend> matchedNicknames = friends
            .where((Friend u) => nicknames.contains(u.nickname))
            .toList();
        if (matchedNicknames.isNotEmpty) {
          final Map<String, DocumentReference> refs =
              <String, DocumentReference>{};
          for (Friend u in matchedNicknames) {
            refs.putIfAbsent(
                u.nickname, () => _db.collection('users').document(u.uid));
          }
          // refs.addAll(matchedNicknames
          //     .map<Map<String, DocumentReference>>((User u) =>
          //         <String, DocumentReference>{
          //           u.nickname: _db.collection('users').document(u.uid)
          //         }));
          return refs;
        }
      }
    }
    return <String, DocumentReference>{};
  }

  Future<void> _handleSubmitted(String text) async {
    _textController.clear();
    changeMentionModal(false);
    setState(() {
      _isComposing = false;
    });
    if (text.isEmpty) {
      return;
    }
    final Comment comment = await _postNewComment(text);

    if (mounted && comment != null) {
      setState(() {
        _comments.add(comment);
      });
      _scrollToTheBottom();
    }
  }

  void _scrollToTheBottom() {
    // print(_scrollController.position.maxScrollExtent);
    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent * 1.05,
      duration: const Duration(milliseconds: 1000),
      curve: Curves.decelerate,
    );
  }

  Widget _buildTextComposer() {
    return IconTheme(
        data: IconThemeData(color: ComooColors.roxo),
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Row(
            children: <Widget>[
              Flexible(
                child: TextField(
                  controller: _textController,
                  onChanged: (String text) {
                    _checkIfHasMentions(text);
                    if (mounted) {
                      setState(() {
                        _isComposing = text.length > 0;
                      });
                    }
                  },
                  textCapitalization: TextCapitalization.sentences,
                  onSubmitted: _handleSubmitted,
                  decoration: InputDecoration.collapsed(
                      hintText: 'Escreva um comentário'),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 4.0),
                child: IconButton(
                    icon: Icon(COMOO.icons.chatSend.iconData),
                    onPressed: _isComposing
                        ? () => _handleSubmitted(_textController.text)
                        : null),
              ),
            ],
          ),
        ));
  }

  Widget _buildCommentTile(BuildContext context, int index, Comment comment) {
    return ListTile(
      key: GlobalObjectKey(index),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(comment.user.photoURL),
      ),
      title: _buildCommentTitle(context, comment),
      subtitle: Text(timeago.format(comment.date, locale: 'pt_BR')),
    );
  }

  Widget _buildCommentTitle(BuildContext context, Comment comment) {
    final TextStyle textStyle = ComooStyles.texto;
    final TextStyle mentionTextStyle = textStyle.copyWith(
      color: Colors.blue,
    );
    if (comment.refs.isEmpty) {
      return RichText(
          text: TextSpan(
              text: '${comment.user.name} ',
              style: ComooStyles.texto_bold,
              children: [
            TextSpan(style: ComooStyles.texto, text: comment.text)
          ]));
    } else {
      // if (comment.refs is Map<String, DocumentReference>) {
      //   for (String key in comment.refs.keys) {
      //     print('$key : ${comment.refs[key]}');
      //   }
      // }
      final String text = comment.text;
      final String pattern = '@';
      final Iterable<Match> matches = pattern.allMatches(text);

      final List<CommentTextSpan> texts = <CommentTextSpan>[];
      print(text);
      if (matches.isNotEmpty) {
        int index = 0;
        final List<String> nicknames = <String>[];
        for (Match m in matches) {
          final String nickname =
              text.substring(m.end, text.length).split(' ').first;
          final int length = m.start + nickname.length + 1;
          nicknames.add(nickname);
          final String prefixText = text.substring(index, m.start);
          print('PrefixText: $prefixText');
          texts.add(CommentTextSpan(
            text: prefixText,
            style: textStyle,
          ));
          print('Mention: ${text.substring(m.start, length)}');
          texts.add(CommentTextSpan(
            text: text.substring(m.start, length),
            style: mentionTextStyle,
            ref: comment.refs[nickname],
          ));
          final String ending = text.substring(length, text.length);
          print('Ending: \'$ending\'');
          final String suffixText = ending.split('@').first;
          texts.add(CommentTextSpan(
            text: suffixText,
            style: textStyle,
          ));
          index = length + suffixText.length;
          print('${m.start} ${(m.start + nickname.length)} ($nickname)');
        }
      }

      return RichText(
        text: TextSpan(
          text: '${comment.user.name} ',
          style: ComooStyles.texto_bold,
          children: texts.isEmpty
              ? <TextSpan>[
                  TextSpan(
                    text: comment.text,
                    style: textStyle,
                  ),
                ]
              : texts
                  .map<TextSpan>((CommentTextSpan cts) => TextSpan(
                      text: cts.text,
                      style: cts.style,
                      recognizer: cts.ref != null
                          ? (TapGestureRecognizer()
                            ..onTap = () {
                              _navigateToUserProfile(cts.ref);
                            })
                          : null))
                  .toList(),
        ),
      );
    }
  }

  Widget _buildMentionWidget(BuildContext context) {
    return Container(
      color: Color(0xFFEFEFEF),
      child: StreamBuilder<List<Friend>>(
          initialData: <Friend>[],
          stream: friends,
          builder: (BuildContext context, AsyncSnapshot<List<Friend>> snap) {
            // print(snap.hasData);
            return snap.data.isEmpty
                ? Center(child: CircularProgressIndicator())
                : _buildFriendSearch(context, snap.data);
          }),
    );
  }

  Widget _buildFriendSearch(BuildContext context, List<Friend> friends) {
    return StreamBuilder<String>(
      initialData: '',
      stream: mentionText,
      builder: (BuildContext context, AsyncSnapshot<String> snap) {
        List<Friend> list = snap.data?.isEmpty ?? true
            ? friends
            : friends
                .where((Friend u) =>
                    u is Friend ? u.nickname.startsWith(snap.data) : false)
                .toList();
        return _buildFriendList(context, list);
      },
    );
  }

  Widget _buildFriendList(BuildContext context, List<Friend> friends) {
    if (friends.isEmpty) {
      return Container(height: 0.0);
    }
    final TextStyle style = TextStyle(
      color: COMOO.colors.purple,
      fontFamily: COMOO.fonts.montSerrat,
    );
    final TextStyle subStyleActive = style.copyWith(color: COMOO.colors.gray);
    final TextStyle subStyleInactive =
        style.copyWith(color: COMOO.colors.lightGray);
    return Container(
      child: ListView.builder(
        controller: mentionController,
        itemCount: friends.length,
        itemBuilder: (BuildContext context, int index) {
          if (friends[index] is Future) {
            return Container(
                padding: EdgeInsets.all(responsive(context, 15.0)),
                child: const Center(child: CircularProgressIndicator()));
          }

          final Friend item = friends[index];
          if ((item?.name ?? '').isEmpty) {
            return Container();
          }
          final List<String> names = item.name.split(' ');
          final String name =
              names.length > 1 ? '${names.first} ${names.last}' : item.name;
          return ListTile(
            dense: true,
            onTap: () {
              _onMentionTap(context, item);
              changeMentionModal(false);
              mentionController.jumpTo(0.0);
            },
            leading: CircleAvatar(
              backgroundColor: Colors.white,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.circular(responsive(context, 30.0)),
                    image: DecorationImage(
                        image: CachedNetworkImageProvider(
                      item?.photoURL ?? defaultImage,
                    ))),
              ),
            ),
            title: Text(
              '$name',
              style: style.copyWith(fontWeight: FontWeight.bold),
            ),
            // subtitle: Text(
            //   '@${item.nickname}',
            //   style: style.copyWith(color: COMOO.colors.gray),
            // ),
            subtitle: StreamBuilder<String>(
              stream: mentionText,
              builder: (BuildContext context, AsyncSnapshot<String> snap) {
                final String typed = (snap.data ?? '');
                // print(item.nickname.lastIndexOf(snap.data));
                final String activeText = typed.isEmpty ? '' : '$typed';
                final String inactiveText = typed.isEmpty
                    ? '${item.nickname}'
                    : '${item.nickname.substring(typed.length)}';
                return RichText(
                  text: TextSpan(
                    text: '@',
                    style: subStyleActive,
                    children: <TextSpan>[
                      TextSpan(text: activeText, style: subStyleActive),
                      TextSpan(text: inactiveText, style: subStyleInactive),
                    ],
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }

  void _onMentionTap(BuildContext context, Friend user) {
    // print(user.name);
    final String text = _textController.text;
    final int index = text.lastIndexOf('@');
    if (index != -1) {
      final String newText = '${text.substring(0, index)}@${user.nickname} ';
      if (mounted) {
        setState(() {
          _textController.text = newText;
        });
      }
    }
  }

  void _navigateToUserProfile(DocumentReference ref) {
    // print(ref.documentID);
    ref.get().then((DocumentSnapshot snap) {
      final User user = User.fromSnapshot(snap);
      Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => UserDetailView(user),
      ));
    });
  }

  Future<void> requestProduct(BuildContext context) async {
    editHasRequest(true);
    final Map<String, dynamic> result =
        await api.requestProduct(widget.product.id);
    print(result);
    if (result == null) {
      editHasRequest(false);
      showCustomDialog<void>(
        context: context,
        alert: customRoundAlert(
          context: context,
          title: 'Um erro inesperado aconteceu, tente novamente mais tarde.',
        ),
      );
    } else {
      final bool isFirst = result['isFirst'] ?? false;
      if (isFirst) {
        //
        final Negotiation negotiation = result['negotiation'] as Negotiation;

        await Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => DirectChatView(negotiation)
              // NegotiationPage(negotiation, goToChat: true),
              ),
        );
      } else {
        final int count = result['count'];
        _showNotFirstDialog(context, count);
      }
    }
  }

  void _showNotFirstDialog(BuildContext context, int count) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.all(mediaQuery * 0.0),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: mediaQuery * 186.0,
                width: mediaQuery * 329.0,
                decoration: BoxDecoration(
                  color: ComooColors.pink,
                  borderRadius: BorderRadius.all(
                    Radius.circular(mediaQuery * 20.0),
                  ),
                ),
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Seu QUERO foi enviado!\r\nVocê é o $count\º da fila',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white, fontSize: mediaQuery * 18.0),
                      ),
                      SizedBox(height: mediaQuery * 26.0),
                      ButtonBar(
                        mainAxisSize: MainAxisSize.min,
                        alignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: mediaQuery * 41.0,
                            width: mediaQuery * 163.0,
                            decoration: BoxDecoration(
                              border: Border.all(
                                width: 1.0,
                                color: Colors.white,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                'OK',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class ProductTile extends StatelessWidget with Reusables {
  ProductTile(this.product, {@required this.onRequested});
  final Product product;
  final Function onRequested;

  final NumberFormat formatter =
      NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');

  @override
  Widget build(BuildContext context) {
    final ThemeData contextTheme = Theme.of(context);
    final ThemeData theme = contextTheme.copyWith(
      textTheme: contextTheme.textTheme.copyWith(
        title: contextTheme.textTheme.title.copyWith(
          fontFamily: COMOO.fonts.montSerrat,
          fontSize: responsive(context, 14),
          color: COMOO.colors.purple,
          fontWeight: FontWeight.w600,
        ),
        subtitle: contextTheme.textTheme.title.copyWith(
          fontFamily: COMOO.fonts.montSerrat,
          fontSize: responsive(context, 13),
          color: COMOO.colors.lead,
          fontWeight: FontWeight.w500,
          letterSpacing: responsive(context, 0.52),
        ),
      ),
    );
    return ListTile(
      leading: LimitedBox(
        maxWidth: responsive(context, 50),
        maxHeight: responsive(context, 50),
        child: CachedCircleAvatar(url: product.photos[0]),
      ),
      title: Text(
        product.title,
        style: theme.textTheme.title,
      ),
      subtitle: Text(
        formatter.format(product.price),
        style: theme.textTheme.subtitle,
      ),
      trailing: SizedBox(
        width: responsive(context, 100.0),
        height: responsive(context, 32.0),
        child: RaisedButton(
          elevation: 0.0,
          color: COMOO.colors.turquoise,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(responsive(context, 30.0)),
          ),
          padding: EdgeInsets.all(0),
          child: Text(
            'COMPRAR',
            style: TextStyle(
              fontFamily: COMOO.fonts.montSerrat,
              fontSize: responsive(context, 13.0),
              color: Colors.white,
              fontWeight: FontWeight.w600,
              letterSpacing: responsive(context, 0.3),
            ),
          ),
          onPressed: onRequested,
        ),
      ),
      onTap: () => Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => ProductPage(product),
      )),
    );
  }
}
