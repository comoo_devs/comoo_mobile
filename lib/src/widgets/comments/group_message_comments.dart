import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/comments.dart';
import 'package:comoo/src/comoo/group_message.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

class GroupMessageCommentsPage extends StatefulWidget {
  GroupMessageCommentsPage(this.groupMessage);
  final GroupMessage groupMessage;
  @override
  State<StatefulWidget> createState() => _GroupMessageCommentsPageState();
}

class _GroupMessageCommentsPageState extends State<GroupMessageCommentsPage> {
  final Firestore _db = Firestore.instance;
  final TextEditingController _textController = TextEditingController();
  bool _isComposing = false;

  List<Comment> _comments = List<Comment>();

  @override
  void initState() {
    super.initState();
    _loadProductComments();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.5,
        brightness: Theme.of(context).brightness,
        title: Text('COMENTÁRIOS', style: ComooStyles.appBarTitle),
        backgroundColor: Colors.white,
        iconTheme: ComooIcons.roxo,
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Flexible(
              child: ListView.builder(
                itemBuilder: (_, index) {
                  Comment comment = _comments[index];
                  return ListTile(
                    key: GlobalObjectKey(index),
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(comment.user.photoURL),
                    ),
                    title: RichText(
                        text: TextSpan(
                            text: comment.user.name + ' ',
                            style: ComooStyles.texto_bold,
                            children: [
                          TextSpan(style: ComooStyles.texto, text: comment.text)
                        ])),
                    subtitle:
                        Text(timeago.format(comment.date, locale: 'pt_BR')),
                  );
                },
                itemCount: _comments.length,
              ),
            ),
            Divider(height: 1.0),
            Container(
              decoration: BoxDecoration(color: Theme.of(context).cardColor),
              child: _buildTextComposer(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextComposer() {
    return IconTheme(
        data: IconThemeData(color: ComooColors.roxo),
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Row(
            children: <Widget>[
              Flexible(
                child: TextField(
                  controller: _textController,
                  onChanged: (String text) {
                    setState(() {
                      _isComposing = text.length > 0;
                    });
                  },
                  textCapitalization: TextCapitalization.sentences,
                  onSubmitted: _handleSubmitted,
                  decoration: InputDecoration.collapsed(
                      hintText: 'Escreva um comentário'),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 4.0),
                child: IconButton(
                    icon: Icon(COMOO.icons.chatSend.iconData),
                    onPressed: _isComposing
                        ? () => _handleSubmitted(_textController.text)
                        : null),
              ),
            ],
          ),
        ));
  }

  void _handleSubmitted(String text) async {
    _textController.clear();
    setState(() {
      _isComposing = false;
    });
    Comment comment = Comment(text, currentUser, DateTime.now());
    await _postNewComment(comment);
    setState(() {
      _comments.add(comment);
    });
  }

  Future<Comment> _postNewComment(Comment comment) async {
    // final Map<String, DocumentReference> refs = _fetchRefs(text);
    // final Comment comment = Comment(text, currentUser, DateTime.now());
    // comment.addRefs(refs);

    NetworkApi().createGroupMessageComment(context,
        id: widget.groupMessage.id, comment: comment);
    return comment;
  }

  Future<void> _loadProductComments() async {
    QuerySnapshot query = await _db
        .collection('loose_comments')
        .document(this.widget.groupMessage.id)
        .collection('comments')
        .orderBy('date')
        .getDocuments();

    List<Future> fetchCommentInfo = List<Future>();

    query.documents.forEach((DocumentSnapshot snap) {
      DocumentReference userRef = snap['from'];
      fetchCommentInfo.add(userRef.get().then((userSnap) {
        User user = User.fromSnapshot(userSnap);
        return Comment(
            snap['text'],
            user,
            (snap['date'] is Timestamp)
                ? DateTime.fromMillisecondsSinceEpoch(
                    (snap['date'] as Timestamp).millisecondsSinceEpoch)
                : (snap['date'] as DateTime));
      }));
    });

    Future.wait(fetchCommentInfo).then((List comments) {
      if (comments.length > 0) {
        setState(() {
          _comments.addAll(comments.cast<Comment>());
        });
      }
    });
  }
}
