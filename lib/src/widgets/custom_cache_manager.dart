import 'dart:io';
import 'package:path/path.dart' as p;

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:path_provider/path_provider.dart';

class CustomCacheManager extends BaseCacheManager {
  static const key = "comooCachedFiles";
  static CustomCacheManager _instance;

  factory CustomCacheManager() {
    if (_instance == null) {
      _instance = CustomCacheManager._();
    }
    return _instance;
  }

  CustomCacheManager._()
      : super(
          key,
          maxAgeCacheObject: Duration(days: 1),
          maxNrOfCacheObjects: 100,
          // fileFetcher: _customHttpGetter,
        );

  @override
  Future<String> getFilePath() async {
    final Directory directory = await getTemporaryDirectory();
    return p.join(directory.path, key);
  }

  // static Future<FileFetcherResponse> _customHttpGetter(String url, {Map<String,String> headers}) async {
  //   return HttpFileFetcherResponse(await http.get(url, headers: headers));
  // }
}
