import 'package:flutter/material.dart';

class MessagePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final double height = size.height;
    final double width = size.width;
    final Paint paint = Paint();

    final Path turquoiseTriangle = Path()
      ..addPolygon(<Offset>[
        Offset(0.0, height - height * 20.53 / 300),
        Offset(0.0, height),
        Offset(width, height),
        Offset(width, height - height * 20.53 / 105),
      ], true);
    paint.color = Color(0xFF00A1AD);
    canvas.drawPath(turquoiseTriangle, paint);
    final Path pinkTriangle = Path()
      ..addPolygon(<Offset>[
        Offset(0.0, height - height * 20.53 / 100),
        Offset(0.0, height),
        Offset(width, height),
        Offset(width, height - height * 20.53 / 200),
      ], true);
    paint.color = Color(0xFFEC1260);
    canvas.drawPath(pinkTriangle, paint);
  }

  @override
  bool shouldRepaint(MessagePainter oldDelegate) => this == oldDelegate;
}
