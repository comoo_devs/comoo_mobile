import 'package:flutter/material.dart';

class RectPainter extends CustomPainter {
  RectPainter({
    this.topLeft = 0.0,
    this.topRight = 0.0,
    this.bottomLeft = 1.0,
    this.bottomRight = 1.0,
    this.color = const Color(0xFFEC1260),
  })  : assert(topLeft >= 0.0 && topLeft <= 1.0),
        assert(topRight >= 0.0 && topRight <= 1.0),
        assert(bottomLeft >= 0.0 && bottomLeft <= 1.0),
        assert(bottomRight >= 0.0 && bottomRight <= 1.0);

  final double topLeft;
  final double topRight;
  final double bottomLeft;
  final double bottomRight;
  final Color color;

  @override
  void paint(Canvas canvas, Size size) {
    final double height = size.height;
    final double width = size.width;
    final Paint paint = Paint();

    final Path pinkTriangle = Path()
      ..addPolygon(<Offset>[
        Offset(0.0, height * topLeft), //topLeft
        Offset(0.0, height * bottomLeft), //bottomLeft
        Offset(width, height * bottomRight), //bottomRight
        Offset(width, height * topRight), //topRight
      ], true);
    paint.color = color;
    canvas.drawPath(pinkTriangle, paint);
  }

  @override
  bool shouldRepaint(RectPainter oldDelegate) => this == oldDelegate;
}
