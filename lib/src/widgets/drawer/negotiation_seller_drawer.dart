import 'package:comoo/src/comoo/negotiation.dart';
import 'package:flutter/material.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:comoo/src/utility/style.dart';

class SellerDrawer extends StatelessWidget {
  SellerDrawer(
      {Key key,
      this.negotiation,
      this.onExtend,
      this.onConfirmDelivery,
      this.onCancel})
      : super(key: key);
  final Negotiation negotiation;
  final VoidCallback onExtend;
  final VoidCallback onConfirmDelivery;
  final VoidCallback onCancel;

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
              padding: new EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Text(
                "GERENCIE A TRANSAÇÃO",
                style: ComooStyles.titulos,
              )),
          new Card(
            margin: const EdgeInsets.symmetric(vertical: 20.0),
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new ListTile(
                  leading: new ClipOval(
                    child: new Image(
                      image: new AdvancedNetworkImage(this.negotiation.thumb),
                      fit: BoxFit.cover,
                      width: 40.0,
                      height: 40.0,
                    ),
                  ),
                  title: new Text(
                    this.negotiation.title,
                    overflow: TextOverflow.ellipsis,
                  ),
                  trailing: new Text("R\$ ${negotiation.price}"),
                )
              ],
            ),
          ),
          Padding(
              padding: new EdgeInsets.only(left: 20.0, right: 20.0),
              child: this.negotiation.status == "paid"
                  ? new Text(
                      "Chegou o momento de enviar o produto para o comprador! Após efetuar o envio clique no botão abaixo",
                      style: ComooStyles.texto,
                    )
                  : new Text(
                      "Vendedor, você só poderá cancelar ou extender o prazo após as primeiras 24horsas a partir do início da transação.",
                      style: ComooStyles.texto,
                    )),
          new Row(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.all(10.0),
                child: this.negotiation.status == "paid"
                    ? new OutlineButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        borderSide:
                            new BorderSide(width: 4.0, color: ComooColors.roxo),
                        child: new Text(
                          "Confirmar envio",
                          style: ComooStyles.texto_roxo,
                        ),
                        onPressed: onConfirmDelivery,
                      )
                    : new OutlineButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        borderSide:
                            new BorderSide(width: 4.0, color: ComooColors.roxo),
                        child: new Text(
                          "Extender Transação",
                          style: ComooStyles.texto_roxo,
                        ),
                        onPressed: onExtend,
                      ),
              ),
              new OutlineButton(
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)),
                borderSide: new BorderSide(width: 3.0, color: ComooColors.roxo),
                disabledTextColor: ComooColors.cinzaClaro,
                textColor: ComooColors.roxo,
                child: new Text("Cancelar"),
                onPressed: this.negotiation.status != "paid" ? onCancel : null,
              ),
            ],
          )
        ],
      ),
    );
  }
}
