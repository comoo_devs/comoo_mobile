import 'package:comoo/src/comoo/negotiation.dart';
import 'package:flutter/material.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:comoo/src/utility/style.dart';

class BuyerDrawer extends StatelessWidget {
  BuyerDrawer({Key key, this.negotiation, this.onConfirm, this.onCancel})
      : super(key: key);
  final Negotiation negotiation;
  final VoidCallback onConfirm;
  final VoidCallback onCancel;

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
              padding: new EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Text(
                "EFETUE O PAGAMENTO",
                style: ComooStyles.titulos,
              )),
          new Card(
            margin: const EdgeInsets.symmetric(vertical: 20.0),
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new ListTile(
                  leading: new ClipOval(
                    child: new Image(
                      image: new AdvancedNetworkImage(this.negotiation.thumb),
                      fit: BoxFit.cover,
                      width: 40.0,
                      height: 40.0,
                    ),
                  ),
                  title: new Text(
                    this.negotiation.title,
                    overflow: TextOverflow.ellipsis,
                  ),
                  trailing: new Text("R\$ ${negotiation.price}"),
                )
              ],
            ),
          ),
          Padding(
              padding: new EdgeInsets.only(left: 20.0, right: 20.0),
              child: new Text(
                "Você tem o prazo máximo para conclusão da transação até 24horas, tente tirar todas as dúvidas o quanto antes.",
                style: ComooStyles.texto,
              )),
          new Row(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.all(10.0),
                child: new OutlineButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  borderSide:
                      new BorderSide(width: 4.0, color: ComooColors.roxo),
                  child: new Text(
                    "Continuar com Pagamento",
                    style: ComooStyles.texto_roxo,
                  ),
                  onPressed: onConfirm,
                ),
              ),
              new OutlineButton(
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)),
                borderSide: new BorderSide(width: 3.0, color: ComooColors.roxo),
                child: new Text(
                  "Desistir",
                  style: ComooStyles.texto_roxo,
                ),
                onPressed: onCancel,
              ),
            ],
          )
        ],
      ),
    );
  }
}
