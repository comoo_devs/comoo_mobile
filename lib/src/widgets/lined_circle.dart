import 'package:flutter/material.dart';
import 'dart:math';

class LinedCircle extends StatelessWidget {
  final Color color;
  final double strokeWidth;
  final Widget child;

  LinedCircle({
    this.child,
    this.color = Colors.black,
    this.strokeWidth = 2.5,
  });

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: LinedCirclePainter(color: color, strokeWidth: strokeWidth),
      child: child,
    );
  }
}

class LinedCirclePainter extends CustomPainter {
  final Color color;
  final double strokeWidth;

  LinedCirclePainter({@required this.color, @required this.strokeWidth});

  @override
  void paint(Canvas canvas, Size size) {
    final double singleAngle = (pi * 2);

    // for (int i = 0; i < dashes; i++) {
    final Paint paint = Paint()
      ..color = color
      ..strokeWidth = strokeWidth
      ..style = PaintingStyle.stroke;

    canvas.drawArc(
      Offset.zero & size,
      singleAngle,
      singleAngle,
      false,
      paint,
    );
    // }
  }

  @override
  bool shouldRepaint(LinedCirclePainter oldDelegate) {
    return color != oldDelegate.color;
  }
}
