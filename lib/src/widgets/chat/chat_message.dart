//import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:comoo/src/comoo/message.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/feed/product_feed.dart';
import 'package:flutter/material.dart';
//import 'package:timeago/timeago.dart' as timeago;

import 'package:comoo/src/widgets/feed/reference_feed.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:intl/intl.dart';

class ChatMessage extends StatefulWidget {
  final Message message;
  final User user;
  final Product product;
  final List photos;
  final Group group;

  ChatMessage({
    Key key,
    this.user,
    this.message,
    this.product,
    this.photos,
    this.group,
  }) : super(key: key);

  @override
  _ChatMessageState createState() {
    return _ChatMessageState();
  }
}

class _ChatMessageState extends State<ChatMessage> {
  _ChatMessageState();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    if (widget.message.type == 'event') {
      return Container(
        padding: EdgeInsets.all(mediaQuery * 20.0),
        child: Text(
          widget.message.text,
          style: ComooStyles.chatEvent,
        ),
        alignment: Alignment.center,
      );
    }

    //bool toBeContinued = false;
    bool isFirst = true;
    if (widget.message.type == 'product') {
      if (widget.product == null || widget.product.status == 'deleted') {
        return Container();
      }
      return ProductFeed(widget.product);
    } else if (widget.message.type == 'reference') {
      return ReferenceFeed(widget.message, widget.user, widget.group.id);
    }
    return Container(
      padding: EdgeInsets.only(
        left: mediaQuery * 14.0,
        bottom: mediaQuery * 20.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(
                right: mediaQuery * 16.0,
              ),
              child: !isFirst
                  ? Container(
                      width: mediaQuery * 40.0,
                    )
                  : ClipOval(
                      child: Image(
                        image: AdvancedNetworkImage(this.widget.user.photoURL),
                        fit: BoxFit.cover,
                        width: mediaQuery * 50.0,
                        height: mediaQuery * 50.0,
                      ),
                    )),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: isFirst
                      ? Container(
                          margin: EdgeInsets.only(
                            bottom: mediaQuery * 5.0,
                          ),
                          child: Text.rich(
                            TextSpan(
                              text: widget.user.name + ' ',
                              style: ComooStyles.nomes,
                              children: [
                                TextSpan(
                                    text: DateFormat('dd/MM/yyyy HH:mm')
                                        .format(widget.message.date),
                                    style: ComooStyles.chatData)
                              ],
                            ),
                          ),
                        )
                      : Container(),
                ),
                Container(
                  padding: EdgeInsets.all(mediaQuery * 9.0),
                  decoration: BoxDecoration(
                    color: ComooColors.white500,
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(mediaQuery * 15.0),
                      topRight: Radius.circular(mediaQuery * 15.0),
                      bottomLeft: Radius.circular(mediaQuery * 15.0),
                    ),
                  ),
                  child: Text(
                    widget.message.text,
                    style: ComooStyles.chatMessage,
                  ),
                ),
//                Container(
//                  child: !toBeContinued
//                      ? Text(
//                          DateFormat('dd/MM/yyyy hh:mm')
//                              .format(widget.message.date),
//                          style: ComooStyles.texto,
//                        )
//                      : Container(),
//                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
