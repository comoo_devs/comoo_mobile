import 'package:comoo/src/blocs/button_bloc.dart';
import 'package:comoo/src/provider/provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';

class StreamRaisedButton extends StatelessWidget {
  StreamRaisedButton({
    Key key,
    @required this.text,
    @required this.onPressed,
    this.style,
    this.color = const Color(0xFF471D58),
  }) : super(key: key);

  final String text;
  final TextStyle style;
  final Function onPressed;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final ButtonBloc bloc = Provider.of(context).button;
    return StreamBuilder<bool>(
      stream: bloc.loading,
      initialData: false,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        return RaisedButton(
          onPressed: onPressed == null
              ? null
              : snapshot.data
                  ? null
                  : () async {
                      bloc.changeLoading(true);
                      await onPressed();
                      bloc.changeLoading(false);
                    },
          elevation: 0.0,
          color: color,
          disabledColor: color.withOpacity(0.5),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(mediaQuery * 20),
          ),
          child: snapshot.data
              ? Container(
                  height: mediaQuery * 20.0,
                  child: FittedBox(
                    child: Center(child: const CircularProgressIndicator()),
                  ),
                )
              : FittedBox(
                  child: Text(
                    text,
                    style: style ??
                        TextStyle(
                          fontFamily: COMOO.fonts.montSerrat,
                          color: Colors.white,
                          fontSize: mediaQuery * 15.0,
                          letterSpacing: mediaQuery * 0.6,
                          fontWeight: FontWeight.w500,
                        ),
                  ),
                ),
        );
      },
    );
  }
}
