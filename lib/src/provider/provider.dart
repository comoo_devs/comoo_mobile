import 'package:comoo/src/blocs/app_bloc.dart';
import 'package:flutter/material.dart';

class Provider extends InheritedWidget {
  Provider({
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  final AppBloc bloc = AppBloc();

  @override
  bool updateShouldNotify(_) => true;

  static AppBloc of(BuildContext context) {
    final Provider provider = context.inheritFromWidgetOfExactType(Provider);
    return provider.bloc;
  }
}
