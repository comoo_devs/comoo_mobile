import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class DebugPage extends StatelessWidget {
  DebugPage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('DEBUG'),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('Next'),
        // onPressed: _fetchLocation,
        onPressed: () => Navigator.of(context).pushNamed('/cat_sign_up'),
      ),
    );
  }

  Future<void> _fetchLocation() async {
    final bool isLocationServiceEnabled =
        await Geolocator().isLocationServiceEnabled();
    print('isLocationServiceEnabled: $isLocationServiceEnabled');
    if (isLocationServiceEnabled) {
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      print(position);
      if (position != null) {
        List<Placemark> placemarks = await Geolocator()
            .placemarkFromCoordinates(position.latitude, position.longitude);
        for (Placemark p in placemarks) {
          print(p);
          print(p.country);
          print(p.locality);
          print(p.subLocality);
          print(p.administrativeArea);
          print(p.postalCode);
        }
      }
    }
  }
}
