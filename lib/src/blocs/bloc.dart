import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class Bloc<T> extends BlocBase {
  Bloc() : _subject = BehaviorSubject<T>();
  Bloc.seeded(T seed) : _subject = BehaviorSubject<T>.seeded(seed);

  final BehaviorSubject<T> _subject;

  void add(T value) {
    if (!_subject.isClosed) {
      _subject.sink.add(value);
    }
  }

  Stream<T> get stream => _subject.stream;

  T get value => _subject.value;

  bool get isClosed => _subject.isClosed;
  bool get isPaused => _subject.isPaused;
  bool get isBroadcast => _subject.isBroadcast;

  void dispose() {
    _subject?.close();
  }
}
