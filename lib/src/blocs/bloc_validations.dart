import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:comoo/src/comoo/bank.dart';
import 'package:comoo/src/comoo/validations.dart';

class BlocValidations {
  static Validations validations = Validations();

  final StreamTransformer<String, String> validatePhone =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String phone, EventSink<String> sink) {
    final String phoneNumber = phone
        .replaceAll('(', '')
        .replaceAll(')', '')
        .replaceAll(' ', '')
        .replaceAll('-', '')
        .trim();
    if (phoneNumber.length < 11) {
      sink.addError('Telefone inválido');
    } else {
      sink.add(phone);
    }
  });

  final StreamTransformer<String, String> validateCPF =
      StreamTransformer<String, String>.fromHandlers(
    handleData: (String cpf, EventSink<String> sink) {
      final String validation = validations.validateCPF(cpf);
      validation == null ? sink.add(cpf) : sink.addError(validation);
    },
  );

  final StreamTransformer<AccountType, AccountType> validateAccountType =
      StreamTransformer<AccountType, AccountType>.fromHandlers(
    handleData: (AccountType type, EventSink<AccountType> sink) {
      switch (type) {
        case AccountType.blank:
          sink.addError('Selecione um tipo de conta.');
          break;
        default:
          sink.add(type);
          break;
      }
    },
  );

  final StreamTransformer<Bank, Bank> validateBank =
      StreamTransformer<Bank, Bank>.fromHandlers(
    handleData: (Bank bank, EventSink<Bank> sink) {
      bank.number == '0'
          ? sink.addError('Selecione um banco.')
          : sink.add(bank);
    },
  );

  final StreamTransformer<String, String> validateEmptyAndOnlyNumbers =
      StreamTransformer<String, String>.fromHandlers(
    handleData: (String agency, EventSink<String> sink) {
      agency.isEmpty
          ? sink.addError('Este campo não pode ser vazio.')
          : RegExp(r'^[0-9]*$').hasMatch(agency)
              ? sink.add(agency)
              : sink.addError('Use somente números.');
    },
  );
  final StreamTransformer<String, String> validateOnlyNumbers =
      StreamTransformer<String, String>.fromHandlers(
    handleData: (String value, EventSink<String> sink) {
      RegExp(r'^[0-9]*$').hasMatch(value)
          ? sink.add(value)
          : sink.addError('Use somente números.');
    },
  );
  final StreamTransformer<String, String> validateEmpty =
      StreamTransformer<String, String>.fromHandlers(
    handleData: (String value, EventSink<String> sink) {
      value.isEmpty
          ? sink.addError('Este campo não pode ser vazio.')
          : sink.add(value);
    },
  );

  final StreamTransformer<String, String> validateZipCode =
      StreamTransformer<String, String>.fromHandlers(
    handleData: (String zip, EventSink<String> sink) async {
      if (zip.isEmpty) {
        sink.addError('CEP é um campo obrigatório.');
      } else {
        final String zipCode = zip.replaceAll(RegExp(r'-'), '');
        if (zipCode.length < 8) {
          sink.addError('Digite um CEP válido.');
        } else {
          final http.Response resp = await http.get(
              'http://apps.widenet.com.br/busca-cep/api/cep.json?code=$zipCode');
          if (resp.statusCode == 200) {
            final dynamic jObj = json.decode(resp.body);
            if (jObj.containsKey('erro')) {
              sink.addError('CEP inválido!');
            } else {
              sink.add(zip);
            }
          }
        }
      }
    },
  );

  final StreamTransformer<String, String> validateWithdraw =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String withdraw, EventSink<String> sink) {
    if (!RegExp(r'^R\$[0-9.,]*$').hasMatch(withdraw)) {
      sink.addError('Digite apenas números.');
      return;
    } else {
      final String value = withdraw.substring(2, withdraw.length);
      String aux = value.replaceAll('.', '#');
      aux = aux.replaceAll(',', '%');
      aux = aux.replaceAll('#', ',');
      aux = aux.replaceAll('%', '.');
      value.isEmpty
          ? sink.addError('Digite uma quantia válida.')
          : double.parse(aux).compareTo(0) > 0
              ? sink.add(withdraw)
              : sink.addError('Digite uma quantia válida.');
    }
  });

  final StreamTransformer<String, String> validatePassword =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String password, EventSink<String> sink) {
    password.length < 6
        ? sink.addError('Mínimo de 6 caracteres')
        : sink.add(password);
  });

  final StreamTransformer<String, String> validateBirthdate =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String birthdate, EventSink<String> sink) {
    if (birthdate.isEmpty) {
      // sink.addError('Data de nascimento é um campo obrigatório!');
    } else {
      final String _birthdate = birthdate.trim().replaceAll('/', '');
      if (_birthdate.length < 8) {
        sink.addError('Digite uma data no formato dd/mm/aaaaa');
      } else {
        try {
          final List<String> fields = birthdate.split('/');
          final int day = int.parse(fields[0]);
          final int month = int.parse(fields[1]);
          final int year = int.parse(fields[2]);
          final DateTime now = DateTime.now();
          if (day < 1 ||
              day > 31 ||
              month < 1 ||
              month > 12 ||
              year < (now.year - 100) ||
              year > now.year) {
            sink.addError('Digite uma data válida!');
          } else {
            sink.add(birthdate);
          }
        } on FormatException catch (e) {
          print(e);
          sink.addError('Digite somente números!');
        } catch (e) {
          sink.addError('Data inválida!');
        }
      }
    }
  });

  final StreamTransformer<String, String> validateCreditCardNumber =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String number, EventSink<String> sink) {
    if (number.isEmpty) {
      // sink.addError('Obrigatório');
    } else {
      final _number = number.replaceAll(' ', '').trim();
      if (_number.isEmpty) {
        sink.addError('Este campo não pode ser vazio.');
      } else {
        if (RegExp(r'^[0-9]*$').hasMatch(_number)) {
          sink.add(number);
        } else {
          sink.addError('Use somente números.');
        }
      }
    }
  });
  final StreamTransformer<String, String> validateCreditCardExpiration =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String expiration, EventSink<String> sink) {
    //THIS CODE WILL BREAK ON THE YEAR 2100!!!
    if (expiration.isEmpty) {
      // sink.addError('Obrigatório!');
    } else {
      final String _expiration = expiration.trim().replaceAll('/', '');
      if (_expiration.length < 4) {
        sink.addError('Digite no formato mm/aa');
      } else {
        try {
          final List<String> fields = expiration.split('/');
          final int month = int.parse(fields[0]);
          final int year = 2000 + int.parse(fields[1]);
          final DateTime date = DateTime(year, month);
          final DateTime now = DateTime.now();
          if (month < 1 || month > 12) {
            sink.addError('Digite uma data válida!');
          } else {
            if (date.year < now.year) {
              sink.addError('Validade expirada!');
            } else if (date.year == now.year) {
              if (date.month < now.month) {
                sink.addError('Validade expirada!');
              } else {
                sink.add(expiration);
              }
            } else {
              sink.add(expiration);
            }
          }
        } on FormatException catch (_) {
          // print(e);
          sink.addError('Digite somente números!');
        } catch (e) {
          sink.addError('Data inválida!');
        }
      }
    }
  });

  final StreamTransformer<String, String> validateNickname =
      StreamTransformer<String, String>.fromHandlers(
    handleData: (String value, EventSink<String> sink) {
      if (value.isEmpty) {
        sink.addError('Preenchimento obrigatório.');
      } else {
        final RegExp nicknameExp = RegExp(r'^[a-z0-9\._-]{3,20}$');
        if (!nicknameExp.hasMatch(value)) {
          if (value.length < 3) {
            sink.addError(
                'Nome de usuário inválido.\nTamanho mínimo 3 caracteres.');
            return;
          }
          if (value.length > 20) {
            sink.addError(
                'Nome de usuário inválido.\nTamanho máximo 20 caracteres.');
            return;
          }
          sink.addError(
              'Nome de usuário inválido.\nApenas letras minúsculas, números e os símbolos _ - .');
          return;
        }
        sink.add(value);
      }
    },
  );
  final StreamTransformer<String, String> validateEmail =
      StreamTransformer<String, String>.fromHandlers(
    handleData: (String value, EventSink<String> sink) {
      if (value.isEmpty) {
        sink.addError('E-mail é obrigatório.');
      } else {
        final RegExp nameExp =
            RegExp(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');
        if (!nameExp.hasMatch(value)) {
          sink.addError('Endereço de e-mail inválido');
        } else {
          sink.add(value);
        }
      }
    },
  );
}
