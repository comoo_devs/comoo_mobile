import 'package:comoo/api/network.dart';
import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class SignUpInviteCodeBloc implements BlocBase {
  SignUpInviteCodeBloc() : _inviteCode = BehaviorSubject<String>();
  SignUpInviteCodeBloc.seeded(String code)
      : _inviteCode = BehaviorSubject<String>.seeded(code);

  final NetworkApi api = NetworkApi();

  final TextEditingController codeController = TextEditingController();

  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  final BehaviorSubject<String> _inviteCode;
  final BehaviorSubject<bool> _tos = BehaviorSubject<bool>();

  Function(bool) get editLoading => _loading.sink.add;
  Function(String) get editInviteCode => _inviteCode.sink.add;
  Function(bool) get editToS => _tos.sink.add;

  Stream<bool> get isLoading => _loading.stream;
  Stream<String> get inviteCode => _inviteCode.stream;
  Stream<bool> get tos => _tos.stream;

  Stream<bool> get isValid => Observable.combineLatest2(
      inviteCode, tos, (String code, bool tos) => true);

  @override
  void dispose() {
    _loading?.close();
    _inviteCode?.close();
    _tos?.close();
  }

  bool get getTos => _tos.value ?? false;

  Future<bool> handleSubmitted(BuildContext context) async {
    final dynamic u = await api.auth.fetchCurrentUser();

    print('>> Is logged in? ${u != null}');
    final String code = _inviteCode.value ?? '';
    editLoading(true);
    final bool result = await api.updateOriginator(context, code);
    editLoading(false);
    return result ?? false;
  }
}
