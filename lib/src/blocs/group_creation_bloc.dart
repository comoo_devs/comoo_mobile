import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class GroupCreationBloc implements BlocBase {
  GroupCreationBloc({String id}) {
    if (id != null) {
      _fetchGroup(id);
    }
  }
  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  Function(bool) get editLoading => _loading.sink.add;
  Stream<bool> get isLoading => _loading.stream;
  @override
  void dispose() {
    _loading?.close();
  }

  Future<void> _fetchGroup(String id) async {}
}
