import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/api/group_message_api.dart';
import 'package:comoo/src/comoo/authentication.dart' as authentication;
import 'package:comoo/src/comoo/comoo.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/group_message.dart';
import 'package:comoo/src/comoo/post.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/user/user_friend.dart';
import 'package:comoo/src/widgets/feed/group_message_feed.dart';
import 'package:comoo/src/widgets/feed/post_feed.dart';
import 'package:comoo/src/widgets/feed/product_feed.dart';
import 'package:flutter/widgets.dart';

import 'bloc.dart';
import 'bloc_base.dart';

class GroupChatBloc implements BlocBase {
  GroupChatBloc(this.group) {
    _init();
  }

  User currentUser = authentication.currentUser;

  final Group group;
  final GroupMessageApi api = GroupMessageApi();
  final Firestore db = Firestore.instance;

  final TextEditingController messageController = TextEditingController();

  final Bloc<bool> loading = Bloc<bool>();
  final Bloc<QuerySnapshot> query = Bloc<QuerySnapshot>();
  final Bloc<List<Widget>> feed = Bloc<List<Widget>>();
  final Bloc<String> message = Bloc<String>();
  StreamSubscription _streamListener;

  @override
  void dispose() {
    loading?.dispose();
    query?.dispose();
    feed?.dispose();
    message?.dispose();
    _streamListener?.cancel();
  }

  Future<void> _init() async {
    api.openedGroupFeed(group.id);

    _streamListener = db
        .collection('groups')
        .document(group.id)
        .collection('feed')
        .orderBy('date', descending: true)
        .snapshots()
        .listen(_onListen);
  }

  Future<void> _onListen(QuerySnapshot snapshot) async {
    loading.add(true);
    query.add(snapshot);
    final List<Future<Widget>> cm = List<Future<Widget>>();
    snapshot.documentChanges.forEach((DocumentChange documentChange) {
      if (documentChange.type == DocumentChangeType.added) {
        Future<Widget> widgets;
        final String type = documentChange.document['type'];
        switch (type) {
          case 'product':
            DocumentReference ref =
                documentChange.document['product'] as DocumentReference;
            if (ref != null) {
              widgets = ref.get().then<Widget>((DocumentSnapshot snap) {
                final Product product = Product.fromSnapshot(snap);
                return ProductFeed(
                  product,
                  key: Key(snap.documentID),
                );
              }).catchError((onError) {
                print('|>> onError => onQuerySnapshot: $onError'
                    '\n > product: ${documentChange.document.documentID}'
                    '\n > exists: ${documentChange.document.exists}');
                return null;
              });
            }
            break;
          case 'post':
            DocumentReference ref =
                documentChange.document['post'] as DocumentReference;
            if (ref != null) {
              widgets = ref.get().then<Widget>((DocumentSnapshot snap) {
                return PostFeed(
                    post: Post.fromSnapshot(snap), key: Key(snap.documentID));
              }).catchError((onError) {
                print('|>> onError => onQuerySnapshot: $onError'
                    '\n > post: ${documentChange.document.documentID}'
                    '\n > exists: ${documentChange.document.exists}');
                return null;
              });
            }
            break;
          case 'loose_comment':
            DocumentReference ref =
                documentChange.document['loose_comment'] as DocumentReference;
            if (ref != null) {
              widgets = ref.get().then<Widget>((DocumentSnapshot snap) {
                return GroupMessageFeed(
                  groupMessage: GroupMessage.fromSnapshot(snap),
                  key: Key(snap.documentID),
                );
              }).catchError((onError) {
                print('|>> onError => onQuerySnapshot: $onError'
                    '\n > loose_comment: ${documentChange.document.documentID}'
                    '\n > exists: ${documentChange.document.exists}');
                return null;
              });
            }
            break;
          default:
            break;
        }
        cm.add(widgets ?? Future.value(Container()));
      }
    });
    await Future.wait(cm).then((List<Widget> newMessages) {
      final List<Widget> oldFeed = feed.value ?? <Widget>[];
      feed.add(<Widget>[...newMessages, ...oldFeed]);
    });
    loading.add(false);
  }

  Future<void> addUsersToGroup(
    String fromID,
    String fromName,
    List users,
  ) async {
    users.forEach((u) {
      UserFriend user = UserFriend.fromSnapshot(u);
      CloudFunctions.instance
          .getHttpsCallable(functionName: 'generateUserNotifications')
          .call(<String, dynamic>{
        'type': 'group_invite',
        'ref': group.id,
        'userId': user.uid,
        'senderId': currentUser.uid,
      }).catchError((error) {
        print('>> generateUserNotifications Error!\n$error');
        var result = {'result': 'error!'};
        return result;
      });
    });
  }

  Future<void> removeUser(User user) async {
    loading.add(true);
    String id = group.id;
    var batch = db.batch();
    var groupUserRef = db
        .collection('groups')
        .document(id)
        .collection('users')
        .document(user.uid);
    var userGroupRef = db
        .collection('users')
        .document(user.uid)
        .collection('groups')
        .document(id);

    batch.delete(groupUserRef);
    batch.delete(userGroupRef);
    await batch.commit();
    loading.add(false);
  }

  Future<void> reloadCurrentUser() async {
    currentUser = await comoo.currentUser();
  }

  Future<void> submitTextComposing({String text}) async {
    final String msg = text ?? message.value ?? '';
    // print(msg);
    message.add('');
    messageController.text = '';
    await api.createGroupMessage(null, id: group.id, message: msg);
  }
}
