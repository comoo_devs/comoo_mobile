import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/comoo/feed.dart';
import 'package:rxdart/rxdart.dart';

class FeedItemBloc implements BlocBase {
  FeedItemBloc({this.item}) {
    _init();
  }

  final FeedItem item;
  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  final BehaviorSubject<DocumentSnapshot> _doc =
      BehaviorSubject<DocumentSnapshot>();

  Function(bool) get editLoading => _loading.sink.add;
  Function(DocumentSnapshot) get editDoc => _doc.sink.add;

  Stream<bool> get isLoading => _loading.stream;
  Stream<DocumentSnapshot> get document => _doc.stream;

  @override
  void dispose() {
    _loading?.close();
    _doc?.close();
    itemSnapshotSubscription?.cancel();
  }

  StreamSubscription itemSnapshotSubscription;

  Future<void> _init() async {
    itemSnapshotSubscription =
        item.ref.snapshots().listen((DocumentSnapshot snap) {
      if (!_doc.isClosed) {
        editDoc(snap);
      }
    });
  }
}
