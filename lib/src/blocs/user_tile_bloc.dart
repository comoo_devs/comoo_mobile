import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/api/network.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';

class UserTileBloc implements BlocBase {
  UserTileBloc.seeded(User user) {
    _user = BehaviorSubject<User>.seeded(user);
    _checkIfIsFriend(user.uid);
  }

  final NetworkApi api = NetworkApi();

  BehaviorSubject<User> _user;
  final BehaviorSubject<bool> _isFriend = BehaviorSubject<bool>();

  Function(bool) get _editFriend => _isFriend.sink.add;

  Stream<bool> get isFriend => _isFriend.stream;

  void editFriend(bool value) {
    _editFriend(value);
    if (value) {
      api.addFriend(_user?.value?.uid);
    } else {
      api.removeFriend(_user?.value?.uid);
    }
  }

  @override
  void dispose() {
    _user?.close();
    _isFriend?.close();
  }

  Future<void> _checkIfIsFriend(String uid) async {
    final FirebaseUser fireUser = await FirebaseAuth.instance.currentUser();
    if (fireUser != null) {
      final Firestore _db = Firestore.instance;
      final QuerySnapshot query = await _db
          .collection('users')
          .document(fireUser.uid)
          .collection('friends')
          .getDocuments();
      if (query.documents.isNotEmpty) {
        final DocumentSnapshot doc = query.documents.firstWhere(
            (DocumentSnapshot snap) => snap.documentID == uid,
            orElse: () => null);
        if (doc != null) {
          _editFriend(true);
        }
      }
    }
  }
}
