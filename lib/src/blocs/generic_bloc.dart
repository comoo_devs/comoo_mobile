import 'dart:async';

import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class GenericBloc<T> implements BlocBase {
  GenericBloc() : _value = BehaviorSubject<T>();
  GenericBloc.seeded(T value) : _value = BehaviorSubject<T>.seeded(value);

  final BehaviorSubject<T> _value;
  void Function(T) get add => _value.sink.add;
  Observable<T> get stream => _value.shareValue();

  T get value => _value.value;

  Observable<T> debounce(
      {Duration duration = const Duration(milliseconds: 500)}) {
    return _value.debounceTime(duration);
  }

  StreamSubscription<T> listen(void Function(T) func) {
    return _value.listen(func);
  }

  @override
  void dispose() {
    _value?.close();
  }
}
