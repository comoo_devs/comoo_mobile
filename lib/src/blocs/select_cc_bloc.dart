import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/credit_card.dart';
import 'package:rxdart/rxdart.dart';

class SelectCreditCardBloc {
  SelectCreditCardBloc();
  final Firestore _db = Firestore.instance;
  BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  BehaviorSubject<List<DocumentSnapshot>> _docs =
      BehaviorSubject<List<DocumentSnapshot>>();
  BehaviorSubject<bool> _hasData = BehaviorSubject<bool>();
  BehaviorSubject<CreditCard> _card = BehaviorSubject<CreditCard>();

  Function(bool) get changeLoading => _loading.sink.add;
  Function(bool) get changeHasData => _hasData.sink.add;
  Function(CreditCard) get changeCard => _card.sink.add;

  Stream<CreditCard> get card => _card.stream;
  Stream<bool> get isLoading => _loading.stream;
  Stream<bool> get hasData => _hasData.stream;
  Stream<List<DocumentSnapshot>> get docs => _db
          .collection('users')
          .document(currentUser.uid)
          .collection('credit_card')
          .snapshots()
          .transform(StreamTransformer.fromHandlers(handleData:
              (QuerySnapshot query, EventSink<List<DocumentSnapshot>> sink) {
        sink.add(query.documents);
        // sink.add(query.documents.map((DocumentSnapshot snap) => User.fromSnapshot(snap));
        changeHasData(query.documents.isNotEmpty);
      }));

  CreditCard get cardValue => _card.value;

  void dispose() {
    _loading.close();
    _hasData.close();
    _docs.close();
    _card.close();
  }
}
