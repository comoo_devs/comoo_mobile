import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/comments.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/api/network.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ProductBloc implements BlocBase {
  ProductBloc.seeded(Product product)
      : _product = BehaviorSubject<Product>.seeded(product),
        _commentsQty = BehaviorSubject<int>.seeded(product?.commentsQty ?? 0),
        initialProduct = product {
    _checkIfUserHasLiked(product);
    _checkIfUserHasRequested(product);
    _fetchLastComment();
  }

  final NetworkApi api = NetworkApi();
  Product initialProduct;
  int times = 0;

  final PageController photoController =
      PageController(initialPage: 0, keepPage: false);

  final BehaviorSubject<Product> _product;
  final BehaviorSubject<bool> _deleted = BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<bool> _loadingLike =
      BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<bool> _liked = BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<bool> _loadingRequest =
      BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<bool> _requested = BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<Comment> _lastComment = BehaviorSubject<Comment>();
  final BehaviorSubject<int> _commentsQty;

  Function(Product) get editProduct => _product.sink.add;
  Function(bool) get editDelete => _deleted.sink.add;
  Function(bool) get editLoading => _loading.sink.add;
  Function(bool) get editLikeLoading => _loadingLike.sink.add;
  Function(bool) get editLike => _liked.sink.add;
  Function(bool) get editLoadingRequest => _loadingRequest.sink.add;
  Function(bool) get editRequested => _requested.sink.add;
  Function(Comment) get editLastComment => _lastComment.sink.add;
  Function(int) get editCommentsQty => _commentsQty.sink.add;

  Stream<Product> get product => _product.stream;
  Stream<bool> get isDeleted => _deleted.stream;
  Stream<bool> get isLoading => _loading.stream;
  Stream<bool> get isLoadingLike => _loadingLike.stream;
  Stream<bool> get liked => _liked.stream;
  Stream<bool> get loadingRequest => _loadingRequest.stream;
  Stream<bool> get userHasRequested => _requested.stream;
  Stream<Comment> get lastComment => _lastComment.stream;
  Stream<int> get commentsQty => _commentsQty.stream;

  @override
  void dispose() {
    _product?.close();
    _deleted?.close();
    _loading?.close();
    _loadingLike?.close();
    _liked?.close();
    _loadingRequest?.close();
    _requested?.close();
    _lastComment?.close();
    _commentsQty?.close();
    photoController?.dispose();
  }

  Future<void> _fetchLastComment() async {
    final Product product = _product.value;
    if (product != null) {
      api.fetchProductComments(product.id).then((List<Comment> comments) {
        initialProduct.commentsQty = comments.length;
        if (!_commentsQty.isClosed) {
          editCommentsQty(comments.length);
        }
      });
      final List<Comment> comments =
          await api.fetchProductComments(product.id, limit: 1);
      if (comments.isNotEmpty) {
        if (!_lastComment.isClosed) {
          editLastComment(comments.first);
        }
      }

      // print(result);
    }
  }

  Future<void> reloadProduct(Product product) async {
    final Product fetchedProduct = await api.fetchProductFromId(product.id);
    initialProduct = fetchedProduct;
    _checkIfUserHasLiked(product);
    _checkIfUserHasRequested(product);
    if (!_product.isClosed) {
      editProduct(fetchedProduct);
    }
  }

  Future<void> onLike(Product _product) async {
    editLikeLoading(true);
    final bool hasLiked = _liked.value ?? false;
    editLike(!hasLiked);
    if (hasLiked) {
      _product.likes--;
      currentUser.removeToLiked(_product.id);
      _product.removeLike().whenComplete(() => editLikeLoading(false));
    } else {
      _product.likes++;
      currentUser.addToLiked(_product.id);
      _product.addLike().whenComplete(() => editLikeLoading(false));
    }
    editProduct(_product);
    // final bool like = _liked.value ?? false;
    // print(like);
    // if (!like) {
    //   currentUser.addToLiked(_product.id);
    //   _product.addLike().whenComplete(() => editLikeLoading(false));
    //   // _likeCount++;
    // } else {
    //   currentUser.removeToLiked(_product.id);
    //   _product.removeLike().whenComplete(() => editLikeLoading(false));
    //   // _likeCount--;
    // }
    // // reloadProduct(_product);
    // editLike(!like);
  }

  Future<Map<String, dynamic>> requestProduct(Product prod) async {
    editLoading(true);
    editLoadingRequest(true);
    final Map<String, dynamic> result = await api.requestProduct(prod.id);
    editLoadingRequest(false);
    editLoading(false);
    if (result != null) {
      editRequested(true);
    }
    return result;
  }

  Future<void> _checkIfUserHasRequested(Product product) async {
    if (currentUser != null) {
      if (currentUser.uid == product.user['uid']) {
        editRequested(true);
      } else {
        final bool req =
            await api.fetchUserHasRequestedProduct(currentUser.uid, product.id);
        if (!_requested.isClosed) {
          editRequested(req);
        }
      }
    }
  }

  void _checkIfUserHasLiked(Product product) {
    if (currentUser != null) {
      final bool req = currentUser.likes.contains(product.id);
      editLike(req);
    }
  }

  Future<void> delete(Product product) async {
    editLoading(true);
    final bool hasDeleted = await api.deleteProduct(product.id);
    editLoading(false);
    if (hasDeleted) {
      editDelete(true);
      editProduct(null);
    }
  }

  Future<Product> fetchProduct(String id) async {
    return await api.fetchProductFromId(id);
  }
}
