import 'package:comoo/src/blocs/bloc_validations.dart';
import 'package:comoo/src/comoo/bank.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ProfileBankBloc with BlocValidations, MapAccountType {
  TextEditingController bankController = TextEditingController();
  TextEditingController agencyNumberController = TextEditingController();
  TextEditingController agencyDigitController = TextEditingController();
  TextEditingController accountController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  TextEditingController accountDigitController = TextEditingController();

  BehaviorSubject<bool> _editBankData = BehaviorSubject<bool>();
  BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  BehaviorSubject<Bank> _bank = BehaviorSubject<Bank>();
  BehaviorSubject<String> _agencyNumber = BehaviorSubject<String>();
  BehaviorSubject<String> _agencyDigit = BehaviorSubject<String>();
  BehaviorSubject<AccountType> _accountType = BehaviorSubject<AccountType>();
  BehaviorSubject<String> _accountNumber = BehaviorSubject<String>();
  BehaviorSubject<String> _accountDigit = BehaviorSubject<String>();

  Function(bool) get changeEditBankData => _editBankData.sink.add;
  Function(bool) get changeLoading => _loading.sink.add;
  Function(Bank) get changeBank => _bank.sink.add;
  Function(String) get changeAgencyNumber => _agencyNumber.sink.add;
  Function(String) get changeAgencyDigit => _agencyDigit.sink.add;
  Function(AccountType) get changeAccountType => _accountType.sink.add;
  Function(String) get changeAccountNumber => _accountNumber.sink.add;
  Function(String) get changeAccountDigit => _accountDigit.sink.add;

  Stream<bool> get editBankData => _editBankData.stream;
  Stream<bool> get isLoading => _loading.stream;
  Stream<Bank> get bank => _bank.transform(validateBank);
  Stream<String> get agencyNumber =>
      _agencyNumber.transform(validateEmptyAndOnlyNumbers);
  Stream<String> get agencyDigit => _agencyDigit.stream;
  Stream<AccountType> get accountType =>
      _accountType.transform(validateAccountType);
  Stream<String> get accountNumber =>
      _accountNumber.transform(validateEmptyAndOnlyNumbers);
  Stream<String> get accountDigit => _accountDigit.stream;

  String get bankValue => _bank.value?.number;
  String get accountTypeValue => accountData(_accountType.value);
  String get agencyValue =>
      '${_agencyNumber.value ?? ''}${(_agencyDigit.value ?? '').isEmpty ? '' : '-' + _agencyDigit.value}';
  String get accountValue =>
      '${_accountNumber.value ?? ''}${(_accountDigit.value ?? '').isEmpty ? '' : '-' + _accountDigit.value}';

  Stream<bool> get submitValid => Observable.combineLatest4(
      bank, agencyNumber, accountType, accountNumber, (a, b, c, d) => true);

  void dispose() {
    _editBankData.close();
    _loading.close();
    _bank.close();
    _agencyNumber.close();
    _agencyDigit.close();
    _accountType.close();
    _accountNumber.close();
    _accountDigit.close();
  }
}
