import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';

class BuyingsBloc implements BlocBase {
  BuyingsBloc() {
    _fireUser().listen(_fetchNegotiations);
  }
  final BehaviorSubject<QuerySnapshot> _buyings =
      BehaviorSubject<QuerySnapshot>();
  Function(QuerySnapshot) get addNegotiation => _buyings.sink.add;
  Stream<QuerySnapshot> get buyings => _buyings.stream;

  Stream<FirebaseUser> _fireUser() async* {
    yield await FirebaseAuth.instance.currentUser();
  }

  @override
  void dispose() {
    _buyings?.close();
  }

  Future<void> _fetchNegotiations(FirebaseUser user) async {
    final DocumentReference ref =
        Firestore.instance.collection('users').document(user.uid);
    Firestore.instance
        .collection('negotiations')
        .where('client', isEqualTo: ref)
        .orderBy('lastUpdate', descending: true)
        .snapshots()
        .listen(_buyings.sink.add);
  }
}
