import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/api/network.dart';
import 'package:rxdart/rxdart.dart';

class GroupTileBloc implements BlocBase {
  GroupTileBloc(this.group)
      : _isMember = BehaviorSubject<bool>.seeded(group.isMember ?? false);

  final NetworkApi api = NetworkApi();
  final Group group;

  final BehaviorSubject<bool> _isMember;

  Function(bool) get editIsMember => _isMember.sink.add;

  Stream<bool> get isMember => _isMember.stream;

  @override
  void dispose() {
    _isMember?.close();
  }

  void requestEntry() {
    editIsMember(true);
    api.requestGroupEntry(group.id);
  }

  void requestExit() {
    editIsMember(false);
    api.requestGroupExit(group.id);
  }
}
