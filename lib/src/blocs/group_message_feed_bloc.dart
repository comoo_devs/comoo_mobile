import 'package:comoo/api/group_message_api.dart';
import 'package:comoo/src/comoo/authentication.dart' as authentication;
import 'package:comoo/src/comoo/group_message.dart';
import 'package:comoo/src/comoo/user.dart';

import 'bloc.dart';
import 'bloc_base.dart';

class GroupMessageFeedBloc implements BlocBase {
  GroupMessageFeedBloc(this.groupMessage)
      : currentUser = authentication.currentUser,
        hasLiked = Bloc<bool>.seeded(
            authentication.currentUser.likes.contains(groupMessage.id)),
        commentsQty = Bloc<int>() {
    _init();
  }

  User currentUser;
  GroupMessage groupMessage;
  final GroupMessageApi api = GroupMessageApi();

  final Bloc<bool> loading = Bloc<bool>();
  final Bloc<bool> hasLiked;
  final Bloc<int> commentsQty;
  final Bloc<bool> isDeleted = Bloc<bool>();

  @override
  void dispose() {
    loading?.dispose();
    hasLiked?.dispose();
    isDeleted?.dispose();
  }

  Future<void> _init() async {
    final List comments = await api.fetchGroupComments(groupMessage.id);
    commentsQty.add(comments.length);
  }

  Future<void> delete() async {
    loading.add(true);
    print(DateTime.now());
    final result = await api.deleteGroupMessage(null, groupMessage.id);
    print(result);
    print(DateTime.now());
    isDeleted.add(true);
    loading.add(false);
  }

  Future<void> onLike() async {
    bool _liked = hasLiked.value ?? false;
    if (!_liked) {
      groupMessage.likes++;
      api.likeGroupMessage(null, groupMessage.id);
      currentUser.likes.add(groupMessage.id);
    } else {
      groupMessage.likes--;
      api.likeGroupMessage(null, groupMessage.id);
      currentUser.likes.remove(groupMessage.id);
    }
    hasLiked.add(!_liked);
  }
}
