import 'dart:io';

import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/blocs/bloc_validations.dart';
import 'package:comoo/api/network.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rxdart/rxdart.dart';

class SignupBloc with BlocValidations implements BlocBase {
  SignupBloc()
      : _code = BehaviorSubject<String>(),
        super();
  SignupBloc.seeded({String code})
      : _code = BehaviorSubject<String>.seeded(code),
        super();

  final NetworkApi api = NetworkApi();
  final FacebookLogin facebookLogin = FacebookLogin();
  final GoogleSignIn googleSignIn = GoogleSignIn();

  final BehaviorSubject<String> _uid = BehaviorSubject<String>();
  Function(String) get editUID => _uid.sink.add;
  Stream<String> get uid => _uid.transform(validateEmail);

  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  Function(bool) get editLoading => _loading.sink.add;
  Stream<bool> get isLoading => _loading.stream;

  final BehaviorSubject<File> _image = BehaviorSubject<File>();
  Function(File) get addImage => _image.sink.add;
  Stream<File> get image => _image.stream;

  final BehaviorSubject<bool> _social = BehaviorSubject<bool>();
  Function(bool) get editSocial => _social.sink.add;
  Stream<bool> get isSocial => _social.stream;
  final BehaviorSubject<String> _googleId = BehaviorSubject<String>();
  Function(String) get editGoogleId => _googleId.sink.add;
  Stream<String> get googleId => _googleId.transform(validateEmail);
  final BehaviorSubject<String> _facebookId = BehaviorSubject<String>();
  Function(String) get editFacebookId => _facebookId.sink.add;
  Stream<String> get facebookId => _facebookId.transform(validateEmail);

  final TextEditingController codeController = TextEditingController();
  final BehaviorSubject<String> _code;
  Function(String) get editCode => _code.sink.add;
  Stream<String> get code => _code.stream;

  final TextEditingController nameController = TextEditingController();
  final BehaviorSubject<String> _name = BehaviorSubject<String>();
  Function(String) get editName => _name.sink.add;
  Stream<String> get name => _name.stream;

  final TextEditingController emailController = TextEditingController();
  final BehaviorSubject<String> _email = BehaviorSubject<String>();
  Function(String) get editEmail => _email.sink.add;
  Stream<String> get email => _email.transform(validateEmail);

  final TextEditingController nicknameController = TextEditingController();
  final BehaviorSubject<String> _nickname = BehaviorSubject<String>();
  Function(String) get editNickname => _nickname.sink.add;
  Stream<String> get nickname => _nickname.transform(validateNickname);

  final BehaviorSubject<String> _password = BehaviorSubject<String>();
  Function(String) get addPassword => _password.sink.add;
  Stream<String> get password => _password.transform(validatePassword);

  final BehaviorSubject<bool> _obscurePassword = BehaviorSubject<bool>();
  Function(bool) get editObscurePassword => _obscurePassword.sink.add;
  Stream<bool> get obscurePassword => _obscurePassword.stream;

  final BehaviorSubject<bool> _tos = BehaviorSubject<bool>();
  Function(bool) get editToS => _tos.sink.add;
  Stream<bool> get tos => _tos.stream;

  final BehaviorSubject<String> _passwordConfirmation =
      BehaviorSubject<String>();
  Function(String) get addPasswordConfirmation =>
      _passwordConfirmation.sink.add;
  Stream<String> get passwordConfirmation =>
      _passwordConfirmation.transform(validatePassword);

  Stream<bool> get isValid =>
      Observable.combineLatest5<File, String, String, String, String, bool>(
          image,
          name,
          nickname,
          email,
          password,
          (File img, String n, String nick, String em, String pass) => true);

  bool get getTos => _tos.value ?? false;
  File get getImage => _image?.value;
  String get getCode => (_code?.value) ?? '';
  String get getName => (_name?.value) ?? '';
  String get getNickname => (_nickname?.value) ?? '';
  String get getEmail => (_email?.value) ?? '';
  String get getPassword => (_password?.value) ?? '';
  String get getGoogleId => (_googleId?.value) ?? '';
  String get getFacebookId => (_facebookId?.value) ?? '';
  String get getUID => (_uid?.value) ?? '';

  @override
  void dispose() {
    _uid?.close();
    _loading?.close();
    _social?.close();
    _googleId?.close();
    _facebookId?.close();
    _code?.close();
    _name?.close();
    _email?.close();
    _nickname?.close();
    _image?.close();
    _password?.close();
    _obscurePassword?.close();
    _tos?.close();
    _passwordConfirmation?.close();
  }

  Future<bool> createUserViaEmail(BuildContext context) async {
    final Map<String, dynamic> parameters = <String, dynamic>{
      'name': (getName ?? '').trim(),
      'email': getEmail ?? '',
      'nickname': getNickname ?? '',
      'password': getPassword ?? '',
    };

    final bool result = await api.signUp(context, parameters);
    if (result ?? false) {
      final FirebaseAuth userAuth = FirebaseAuth.instance;
      final FirebaseUser user = await userAuth.signInWithEmailAndPassword(
          email: getEmail, password: getPassword);

      final File _avatarImage = getImage ?? null;
      if (_avatarImage != null) {
        final String url = await _uploadAvatar(user.uid, _avatarImage);
        await api.updateImage(context, url);
      }
      return true;
    } else {
      return false;
    }
  }

  Future<String> _uploadAvatar(String uid, File file) async {
    final StorageReference ref =
        FirebaseStorage.instance.ref().child('/users/$uid/avatar.jpg');
    final StorageTaskSnapshot uploadTask = await ref.putFile(file).onComplete;
    final String result = await uploadTask.ref.getDownloadURL().then((onValue) {
      return onValue.toString();
    }).catchError((onError) {
      return '';
    });
    return result;
  }

  Future<void> handleFacebookAuth(BuildContext context,
      {Function onSuccess}) async {
    editLoading(true);
    bool isLoggedIn = await facebookLogin.isLoggedIn;
    if (isLoggedIn) await facebookLogin.logOut();
    final FacebookLoginResult facebookResult = await facebookLogin
        .logInWithReadPermissions(['email', 'public_profile']).catchError(
            (onError) {
      print('Facebook Erro sinistro!');
    });
    switch (facebookResult.status) {
      case FacebookLoginStatus.loggedIn:
        print('getting login information.');
        print(facebookResult);
        try {
          await _handleFacebookSignup(context, facebookResult, onSuccess);
        } catch (e) {
          print(e);
          // showInSnackBar('Ocorreu um erro ao tentar fazer o login via Facebook. Tente novamente mais tarde.');
        }
        break;
      case FacebookLoginStatus.cancelledByUser:
        print('Cancelled');
        break;
      case FacebookLoginStatus.error:
        print('Error');
        print(facebookResult.errorMessage);
        // showInSnackBar('Ocorreu um erro ao tentar fazer o login via Facebook. Tente novamente mais tarde.');
        break;
    }
    editLoading(false);
  }

  Future<void> _handleFacebookSignup(BuildContext context,
      FacebookLoginResult facebookResult, Function onSuccess) async {
    final FirebaseAuth auth = FirebaseAuth.instance;
    final AuthCredential credential = FacebookAuthProvider.getCredential(
        accessToken: facebookResult.accessToken.token);
    final FirebaseUser signedInUser = await auth
        .signInWithCredential(credential)
        .timeout(Duration(seconds: 60));
    UserInfo userInfo = signedInUser.providerData.first;
    for (UserInfo info in signedInUser.providerData) {
      if (info.providerId == 'facebook.com') {
        userInfo = info;
        break;
      }
    }
    final Map<String, dynamic> parameters = <String, dynamic>{
      'uid': signedInUser.uid,
      'facebookID': facebookResult.accessToken.userId,
      'photoURL': userInfo.photoUrl,
      'name': userInfo.displayName,
      'email': userInfo.email,
      'nickname': userInfo?.email?.split('@')?.first,
      'facebookAccessToken': facebookResult.accessToken.token,
    };
    final bool result = await api.signUp(context, parameters);
    if (result ?? false) {
      onSuccess();
    }
    // print('Signed in as $userInfo');
    // print('ID ${userInfo.uid}');
    // editUID(userInfo.uid);
    // editSocial(true);
    // editName(userInfo.displayName);
    // nameController.text = userInfo.displayName;
    // editEmail(userInfo.email);
    // emailController.text = userInfo.email;
    // editFacebookId(userInfo.uid);
    // final File photoUrl = await api.fileFromURL(userInfo.photoUrl);
    // addImage(photoUrl);
  }

  Future<void> handleGoogleAuth(BuildContext context,
      {Function onSuccess}) async {
    editLoading(true);
    GoogleSignInAccount gUser;
    bool isSignIn = await googleSignIn.isSignedIn();
    if (isSignIn) await googleSignIn.signOut();
    try {
      if (gUser == null) {
        gUser = await googleSignIn.signIn();
      }
    } catch (e) {
      // showInSnackBar('Ocorreu um erro ao tentar fazer o login via Google. Tente novamente mais tarde.');
    }
    if (gUser == null) {
      editLoading(false);
      // showInSnackBar('Você cancelou o acesso via Google');
      return null;
    }
    GoogleSignInAuthentication googleCredentials;
    try {
      googleCredentials = await gUser.authentication;
    } catch (error) {
      editLoading(false);
      return null;
    }
    await _handleGoogleSignUp(context, googleCredentials, onSuccess);
    editLoading(false);
  }

  Future<void> _handleGoogleSignUp(BuildContext context,
      GoogleSignInAuthentication googleCredentials, Function onSuccess) async {
    final AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleCredentials.idToken,
        accessToken: googleCredentials.accessToken);
    FirebaseUser user;
    try {
      final FirebaseAuth auth = FirebaseAuth.instance;
      user = await auth.signInWithCredential(credential);
    } catch (e) {
      editLoading(false);
      return null;
    }

    UserInfo userInfo = user.providerData.first;
    for (UserInfo info in user.providerData) {
      if (info.providerId == 'google.com') {
        userInfo = info;
        break;
      }
    }

    final Map<String, dynamic> parameters = <String, dynamic>{
      'uid': user.uid,
      'googleID': userInfo.uid,
      'photoURL': userInfo.photoUrl,
      'name': userInfo.displayName,
      'email': userInfo.email,
      'nickname': userInfo.email.split('@').first,
      'googleIDToken': googleCredentials.idToken,
      'googleAccessToken': googleCredentials.accessToken,
    };
    final bool result = await api.signUp(context, parameters);
    if (result ?? false) {
      onSuccess();
    }
    // editUID(user.uid);
    // editGoogleId(userInfo.uid);
    // editName(userInfo.displayName);
    // nameController.text = userInfo.displayName;
    // editEmail(userInfo.email);
    // emailController.text = userInfo.email;
    // editSocial(true);
    // try {
    //   final File photoUrl = await api.fileFromURL(userInfo.photoUrl);
    //   addImage(photoUrl);
    // } catch (e) {
    //   print(e);
    // }
  }
}
