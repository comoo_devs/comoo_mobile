// import 'package:flutter/material.dart';
import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/blocs/bloc_validations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:rxdart/rxdart.dart';

class CreditCardBloc with BlocValidations implements BlocBase {
  final MaskedTextController numberController =
      MaskedTextController(mask: '0000 0000 0000 0000');
  final MaskedTextController cpfController =
      MaskedTextController(mask: '000.000.000-00');
  final MaskedTextController expirationDateController =
      MaskedTextController(mask: '00/00');
  final TextEditingController nameController = TextEditingController();
  final MaskedTextController birthdateController =
      MaskedTextController(mask: '00/00/0000');
  BehaviorSubject<String> _number = BehaviorSubject<String>();
  BehaviorSubject<String> _expirationDate = BehaviorSubject<String>();
  BehaviorSubject<String> _name = BehaviorSubject<String>();
  BehaviorSubject<String> _cvc = BehaviorSubject<String>();
  BehaviorSubject<String> _cpf = BehaviorSubject<String>();
  BehaviorSubject<String> _birthdate = BehaviorSubject<String>();
  BehaviorSubject<bool> _loading = BehaviorSubject<bool>();

  Function(String) get changeNumber => _number.sink.add;
  Function(String) get changeExpirationDate => _expirationDate.sink.add;
  Function(String) get changeName => _name.sink.add;
  Function(String) get changeCVC => _cvc.sink.add;
  Function(String) get changeCPF => _cpf.sink.add;
  Function(String) get changeBirthdate => _birthdate.sink.add;
  Function(bool) get changeLoading => _loading.sink.add;

  Stream<String> get number =>
      _number.transform(validateEmpty).transform(validateCreditCardNumber);
  Stream<String> get expirationDate => _expirationDate
      .transform(validateEmpty)
      .transform(validateCreditCardExpiration);
  Stream<String> get name => _name.transform(validateEmpty);
  Stream<String> get cvc =>
      _cvc.transform(validateEmpty).transform(validateOnlyNumbers);
  Stream<String> get cpf =>
      _cpf.transform(validateEmpty).transform(validateCPF);
  Stream<String> get birthdate =>
      _birthdate.transform(validateEmpty).transform(validateBirthdate);
  Stream<bool> get isLoading => _loading.stream;

  CreditCardBlocValue _values = CreditCardBlocValue();

  CreditCardBlocValue get values => _values;

  String get cvcValue => _cvc.value;

  Stream<bool> get submitValid =>
      Observable.combineLatest5(number, expirationDate, name, cpf, birthdate,
          (a, b, c, d, e) {
        _values = CreditCardBlocValue(
          number: a,
          expirationDate: b,
          name: c,
          cpf: d,
          birthdate: e,
        );
        return true;
      }).asBroadcastStream();
  Stream<bool> get submitValidCVC => Observable.combineLatest6(
          number, expirationDate, name, cpf, birthdate, cvc,
          (a, b, c, d, e, f) {
        _values = CreditCardBlocValue(
            number: a,
            expirationDate: b,
            name: c,
            cpf: d,
            birthdate: e,
            cvc: f);
        return true;
      }).asBroadcastStream();

  @override
  void dispose() {
    _number.close();
    _expirationDate.close();
    _name.close();
    _cvc.close();
    _cpf.close();
    _birthdate.close();
    _loading.close();
  }

  void drain() {
    // _number.drain();
    // _expirationDate.drain();
    // _name.drain();
    // _cvc.drain();
    // _cpf.drain();
    // _birthdate.drain();
    changeNumber('');
    numberController.updateText('');
    changeExpirationDate('');
    expirationDateController.updateText('');
    changeName('');
    nameController.text = '';
    changeCPF('');
    cpfController.updateText('');
    changeCVC('');
    changeBirthdate('');
    birthdateController.updateText('');
  }
}

class CreditCardBlocValue {
  CreditCardBlocValue({
    this.name,
    this.number,
    this.cvc,
    this.cpf,
    this.birthdate,
    this.expirationDate,
  });

  String name;
  String number;
  String cvc;
  String cpf;
  String birthdate;
  String expirationDate;

  Map<String, String> get toMap => <String, String>{
        'number': number,
        'expiration': expirationDate,
        'name': name,
        'cpf': cpf,
        'birthDate': birthdate,
        'cvc': cvc,
      };
}
