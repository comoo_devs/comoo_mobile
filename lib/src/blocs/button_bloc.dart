import 'package:rxdart/rxdart.dart';

class ButtonBloc {
  BehaviorSubject<bool> _loading = BehaviorSubject<bool>();

  Function(bool) get changeLoading => _loading.sink.add;

  Stream<bool> get loading => _loading.stream;

  void dispose() {
    _loading.close();
  }
}
