import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/api/network.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';

class SignupGroupDetailBloc implements BlocBase {
  factory SignupGroupDetailBloc.seeded({Group group, String id}) {
    assert(group != null || id != null);
    if (group != null) {
      return SignupGroupDetailBloc._(group.id);
    }
    return SignupGroupDetailBloc._(id);
  }

  SignupGroupDetailBloc._(String id) {
    _fetchGroup(id);
  }

  final NetworkApi api = NetworkApi();

  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  Function(bool) get editLoading => _loading.sink.add;
  Stream<bool> get isLoading => _loading.stream;

  final BehaviorSubject<bool> _askToJoin = BehaviorSubject<bool>();
  Function(bool) get editAskToJoin => _askToJoin.sink.add;
  Stream<bool> get askToJoin => _askToJoin.stream;

  final BehaviorSubject<Group> _group = BehaviorSubject<Group>();
  Function(Group) get editGroup => _group.sink.add;
  Stream<Group> get group => _group.stream;

  final BehaviorSubject<List<FeedGroup>> _feed =
      BehaviorSubject<List<FeedGroup>>();
  Function(List<FeedGroup>) get editFeed =>
      _feed.isClosed ? (List<FeedGroup> f) {} : _feed.sink.add;
  Stream<List<FeedGroup>> get feed => _feed.stream;
  StreamSubscription feedSubscription;

  final BehaviorSubject<List<User>> _members = BehaviorSubject<List<User>>();
  Function(List<User>) get editMembers =>
      _members.isClosed ? (List<User> m) {} : _members.sink.add;
  Stream<List<User>> get members => _members.stream;
  StreamSubscription membersSubscription;

  final BehaviorSubject<int> _memberQuantity = BehaviorSubject<int>();
  Function(int) get editMemberQuantity => _memberQuantity.sink.add;
  Stream<int> get memberQuantity => _memberQuantity.stream;

  final BehaviorSubject<bool> _isMember = BehaviorSubject<bool>();
  Function(bool) get editMember => _isMember.sink.add;
  Stream<bool> get isMember => _isMember.stream;

  final BehaviorSubject<bool> _requestedAccess = BehaviorSubject<bool>();
  Function(bool) get editRequestedAccess => _requestedAccess.sink.add;
  Stream<bool> get requestedAccess => _requestedAccess.stream;

  @override
  void dispose() {
    _loading?.close();
    _askToJoin?.close();
    _group?.close();
    _feed?.close();
    feedSubscription?.cancel();
    _isMember?.close();
    _requestedAccess?.close();
    _members?.close();
    membersSubscription?.cancel();
    _memberQuantity?.close();
  }

  Future<void> _fetchGroup(String id) async {
    editLoading(true);
    final Firestore db = Firestore.instance;
    db
        .collection('groups')
        .document(id)
        .get()
        .timeout(const Duration(seconds: 60))
        .then((DocumentSnapshot snap) {
          final Group group = Group.fromSnapshot(snap);
          if (group.askToJoin) {
            _fetchMembers(id);
          } else {
            _fetchFeed(id);
          }
          editAskToJoin(group.askToJoin);
          editGroup(group);
        })
        .catchError((error) => print(error))
        .whenComplete(() => editLoading(false));

    final FirebaseUser fireUser =
        await FirebaseAuth.instance.currentUser().catchError((onError) => null);
    if (fireUser != null) {
      final DocumentSnapshot snapshot = await db
          .collection('users')
          .document(fireUser.uid)
          .get()
          .catchError((onError) => null);
      if (snapshot?.exists ?? false) {
        final dynamic req = snapshot.data['group_requests'];
        if (req != null) {
          final bool isContained = (req as List<dynamic>).contains(id) ?? false;
          editRequestedAccess(isContained);
        }
      }
    }
  }

  void _fetchFeed(String id) {
    final Firestore db = Firestore.instance;
    print('id: $id');
    feedSubscription = db
        .collection('groups')
        .document(id)
        .collection('feed')
        .orderBy('date', descending: true)
        .snapshots()
        .listen((QuerySnapshot query) async {
      print('query: $query');
      final List<FeedGroup> feeds = <FeedGroup>[];
      for (DocumentSnapshot snap in query.documents) {
        final FeedGroup feedGroup = FeedGroup.fromSnapshot(snap);
        final DocumentSnapshot snapshot = await feedGroup.ref.get();
        feedGroup.snapshot = snapshot;
        feeds.add(feedGroup);
        editFeed(feeds);
      }
    }, onDone: () => print('DONE'));
  }

  void _fetchMembers(String id) {
    final Firestore db = Firestore.instance;
    print('id: $id');
    membersSubscription = db
        .collection('groups')
        .document(id)
        .collection('users')
        .orderBy('name', descending: false)
        .snapshots()
        .listen((QuerySnapshot query) async {
      // print('query: $query');
      final List<User> members = <User>[];
      editMemberQuantity(query.documents.length);
      for (DocumentSnapshot snap in query.documents) {
        final DocumentSnapshot snapshot =
            await db.collection('users').document(snap.documentID).get();
        final User user = User.fromSnapshot(snapshot);
        members.add(user);
        editMembers(members);
      }
    }, onDone: () => print('DONE'));
  }

  Group get getGroup => _group?.value;

  void requestEntry() {
    // editIsMember(true);
    editRequestedAccess(true);
    api.requestGroupEntry(_group?.value?.id ?? '');
  }
}

enum FeedGroupType {
  product,
  post,
  undefined,
}

class FeedGroup {
  factory FeedGroup.fromSnapshot(DocumentSnapshot snapshot) {
    if (!snapshot.exists) {
      return null;
    }
    DateTime date;
    if (snapshot.data['date'] != null) {
      date = (snapshot.data['date'] is Timestamp)
          ? DateTime.fromMillisecondsSinceEpoch(
              (snapshot.data['date'] as Timestamp).millisecondsSinceEpoch)
          : (snapshot.data['date'] as DateTime);
    }
    FeedGroupType type;
    // FutureOr<DocumentSnapshot> item;
    DocumentReference ref;
    if (snapshot.data['type'] != null) {
      String name = '';
      switch (snapshot.data['type']) {
        case 'product':
          name = 'product';
          type = FeedGroupType.product;
          break;
        case 'post':
          name = 'post';
          type = FeedGroupType.post;
          break;
        default:
          type = FeedGroupType.undefined;
          break;
      }
      ref = snapshot.data[name] as DocumentReference;
      // item = (snapshot.data[name] as DocumentReference)
      //     .get()
      //     .catchError((e) => null);
    }
    String from;
    if (snapshot.data['from'] != null) {
      if (snapshot.data['from'] is String) {
        from = snapshot.data['from'];
      }
    }
    if (date == null || type == FeedGroupType.undefined || from == null) {
      print('date: $date');
      print('type: $type');
      print('from: $from');
      return null;
    }
    return FeedGroup._fromSnapshot(
        date: date, from: from, type: type, ref: ref);
  }
  FeedGroup._fromSnapshot(
      {this.date, this.from, this.type, this.snapshot, this.ref});

  DateTime date;
  String from;
  FeedGroupType type;
  DocumentSnapshot snapshot;
  DocumentReference ref;
}
