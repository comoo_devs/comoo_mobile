import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:rxdart/rxdart.dart';

class ProfileAccessBloc extends BlocBase {
  BehaviorSubject<bool> _editAccessData = BehaviorSubject<bool>();
  Function(bool) get changeEditAccessData => _editAccessData.sink.add;
  Stream<bool> get editAccessData => _editAccessData.stream;
  void dispose() {
    _editAccessData.close();
  }
}
