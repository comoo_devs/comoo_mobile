import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/feed.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/home/feed/feed_item.dart';
import 'package:comoo/api/network.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class FeedBloc implements BlocBase {
  FeedBloc({
    @required this.bottomModalCallback,
    @required this.scrollController,
    @required this.refreshStream,
    this.tabController,
  }) {
    _init();
    // tabController.addListener(() {
    //   // tabController.animateTo(tabController.index);
    //   editTab(tabController.index);
    // });
    _refreshSubscription = refreshStream.listen(_handleRefresh);
  }

  final NetworkApi api = NetworkApi();

  final Function bottomModalCallback;
  final ScrollController scrollController; // = ScrollController();
  final Stream<bool> refreshStream;
  final TabController tabController;
  final PageController pageController = PageController();

  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<bool> _loadingFeedGroups =
      BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<List<Group>> _groups = BehaviorSubject<List<Group>>();
  final BehaviorSubject<List<User>> _users = BehaviorSubject<List<User>>();
  final BehaviorSubject<List<FeedItem>> _feed =
      BehaviorSubject<List<FeedItem>>();
  final BehaviorSubject<List<Widget>> _feedWidgets =
      BehaviorSubject<List<Widget>>();
  final BehaviorSubject<List<FeedGroup>> _feedGroups =
      BehaviorSubject<List<FeedGroup>>();
  final BehaviorSubject<int> _tab = BehaviorSubject<int>.seeded(0);
  final BehaviorSubject<List<DocumentSnapshot>> _feedSnapshot =
      BehaviorSubject<List<DocumentSnapshot>>();

  Function(bool) get editLoading => _loading.sink.add;
  Function(bool) get editLoadingFeedGroups => _loadingFeedGroups.sink.add;
  Function(List<Group>) get editGroups => _groups.sink.add;
  Function(List<User>) get editUsers => _users.sink.add;
  Function(List<FeedItem>) get editFeedItens => _feed.sink.add;
  Function(List<Widget>) get editFeedWidgets => _feedWidgets.sink.add;
  Function(List<FeedGroup>) get editFeedGroups => _feedGroups.sink.add;
  Function(int) get editTab => _tab.sink.add;
  Function(List<DocumentSnapshot>) get editFeedSnapshot =>
      _feedSnapshot.sink.add;

  Stream<bool> get isLoading => _loading.stream;
  Stream<bool> get isLoadingFeedGroups => _loadingFeedGroups.stream;
  Stream<List<Group>> get groupRecommendation => _groups.stream;
  Stream<List<User>> get userRecommendation => _users.stream;
  Stream<List<FeedItem>> get feed => _feed.stream;
  Stream<List<DocumentSnapshot>> get feedSnapshots => _feedSnapshot.stream;
  Stream<List<Widget>> get feedWidgets =>
      _feed.asyncMap((List<FeedItem> items) async {
        final List<Widget> list = <Widget>[];
        for (FeedItem item in items) {
          final DocumentSnapshot itemSnap = await item.ref.get();
          final QuerySnapshot commentQuery = await item.ref
              .collection('comments')
              .orderBy('date', descending: true)
              .limit(1)
              .getDocuments();
          if (commentQuery.documents.isNotEmpty) {
            item.comment = commentQuery.documents.first;
          }
          item.snapshot = itemSnap;
          list.add(FeedItemCard(
            item: item,
            key: Key('${item.id}#feed'),
          ));
        }
        return list;
      });
  Stream<List<FeedGroup>> get feedGroups => _feedGroups.stream;
  Stream<int> get tab => _tab.stream;

  @override
  void dispose() {
    _loading?.close();
    _loadingFeedGroups?.close();
    _groups?.close();
    _users?.close();
    _feed?.close();
    _feedWidgets?.close();
    _feedGroups?.close();
    _tab?.close();
    _refreshSubscription?.cancel();
    _feedItemSubscription?.cancel();
    _feedSnapshot?.close();
    _feedSnapshotSubs?.cancel();
  }

  StreamSubscription<bool> _refreshSubscription;
  StreamSubscription<List<FeedItem>> _feedItemSubscription;
  StreamSubscription<List<DocumentSnapshot>> _feedSnapshotSubs;

  Future<void> _loadFeed(String uid) async {
    _feedSnapshotSubs =
        api.fetchFeedSnapshots(uid).listen((List<DocumentSnapshot> list) {
      if (!_feedSnapshot.isClosed) {
        _feedSnapshot.sink.add(list ?? <DocumentSnapshot>[]);
      }
    });
  }

  Future<void> _init() async {
    editLoading(true);
    // _feedItemSubscription =
    //     api.fetchFeedItemsStream().listen(_feedStreamListener);
    // _feed.addStream();
    _fetchRecommendationGroups();
    _fetchRecommendationUsers();
    _fetchFeedGroups();
    // _feedSnapshot.addStream(api.fetchFeedSnapshots());
    FirebaseUser fireUser = await api.fetchFireUser();
    fireUser = fireUser ??= await api.fetchFireUser();
    if (fireUser != null) {
      _loadFeed(fireUser.uid);
    } else {
      await Future.delayed(const Duration(milliseconds: 500));
      fireUser = await api.fetchFireUser();
      fireUser = fireUser ??= await api.fetchFireUser();
      if (fireUser != null) {
        _loadFeed(fireUser.uid);
      }
    }
    // await _fetchFeedItens();
    // editLoading(false);
    //     .map<List<FeedItem>>((QuerySnapshot query) {
    //   final List<DocumentChange> docs = query.documentChanges;
    //   final List<FeedItem> feedList = <FeedItem>[];
    //   for (DocumentChange doc in docs) {
    //     if (doc.document.exists) {
    //       final DocumentSnapshot snap = doc.document;
    //       final FeedItem item = FeedItem.fromSnapshot(snap);
    //       if (item != null) {
    //         feedList.add(item);
    //       }
    //     }
    //   }
    //   return feedList;
    // }).listen((List<FeedItem> list) {
    //   editLoading(false);
    //   if (!_feed.isClosed) {
    //     editFeedItens(list);
    //   }
    // });
    editLoading(false);
  }

  // Future<void> _feedStreamListener(QuerySnapshot query) async {
  // Future<void> _feedStreamListener(List<FeedItem> list) async {
  //   // final List<DocumentChange> docs = query.documentChanges;
  //   // final List<FeedItem> feedList = <FeedItem>[];
  //   // for (DocumentChange doc in docs) {
  //   //   if (doc.document.exists) {
  //   //     final DocumentSnapshot docSnap = doc.document;
  //   //     final FeedItem item = FeedItem.fromSnapshot(docSnap);
  //   //     if (item != null) {
  //   //       // final DocumentSnapshot snap = await item.ref.get();
  //   //       // if (snap.exists) {
  //   //       // item.snapshot = snap;
  //   //       feedList.add(item);
  //   //       // if (!_feed.isClosed) {
  //   //       //   editFeedItens(feedList);
  //   //       //   if (_loading.value ?? false) {
  //   //       //     editLoading(false);
  //   //       //   }
  //   //       // }
  //   //       // }
  //   //     }
  //   //   }
  //   // }
  //   if (!_feed.isClosed) {
  //     editFeedItens(list);
  //   }
  //   // if (!_feedWidgets.isClosed) {
  //   //   final List<Widget> widgets = list.isEmpty
  //   //       ? <Widget>[]
  //   //       : list.map<Widget>((FeedItem item) {
  //   //           final FeedItemCard card = FeedItemCard(
  //   //             item: item,
  //   //             key: Key('${item.id}#feed'),
  //   //           );
  //   //           return card;
  //   //         }).toList();
  //   //   editFeedWidgets(widgets);
  //   // }
  //   editLoading(false);
  //   // if (feedList.isEmpty) {
  //   //   editLoading(false);
  //   //   if (!_feed.isClosed) {
  //   //     editFeedItens(<FeedItem>[]);
  //   //   }
  //   // }
  // }

  void editPage(int index) {
    pageController.animateToPage(
      index,
      curve: Curves.easeIn,
      duration: const Duration(milliseconds: 400),
    );
  }

  Future<void> _fetchRecommendationGroups() async {
    final List<Group> groups = await api.fetchGroupRecommendation();
    editGroups(groups ?? <Group>[]);
  }

  Future<void> _fetchRecommendationUsers() async {
    final List<User> users = await api.fetchFriendRecommendation();
    editUsers(users ?? <User>[]);
  }

  Future<void> _fetchFeedItens() async {
    final List<FeedItem> result = await api.fetchFeedItens();
    final List<FeedItem> list = result ?? <FeedItem>[];
    if (!_feed.isClosed) {
      editFeedItens(list);
    }
  }

  Future<void> _fetchFeedGroups({bool loadingIndicator = true}) async {
    // print('>> LoadingIndicator: $loadingIndicator');
    if (loadingIndicator) {
      editLoadingFeedGroups(true);
    }
    await refreshGroups();
    if (loadingIndicator) {
      editLoadingFeedGroups(false);
    }
  }

  void _handleRefresh(bool value) {
    if (value ?? false) {
      handleRefresh();
    }
  }

  Future<void> handleRefresh() async {
    print('Refreshed at ${DateTime.now()}');
    _fetchRecommendationGroups();
    _fetchRecommendationUsers();
    _fetchFeedGroups(loadingIndicator: false);
    _fetchFeedItens();
  }

  Future<Group> fetchGroup(String id) async {
    final Group group = await api.fetchGroupFromId(id);
    return group;
  }

  Future<String> fetchUserInviteMessage() async {
    return api.fetchUserInviteMessage();
  }

  Future<void> refreshGroups() async {
    final List<FeedGroup> result = await api.fetchFeedGroups();
    // print('feed_groups: ${result?.length}');
    editFeedGroups(result ?? <FeedGroup>[]);
  }
}
