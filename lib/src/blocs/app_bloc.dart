import 'package:comoo/src/blocs/button_bloc.dart';
import 'package:comoo/src/blocs/credit_card_bloc.dart';
import 'package:comoo/src/blocs/profile_access_bloc.dart';
import 'package:comoo/src/blocs/profile_bank_bloc.dart';
import 'package:comoo/src/blocs/profile_personal_bloc.dart';
import 'package:comoo/src/blocs/select_cc_bloc.dart';
import 'package:comoo/src/blocs/withdraw_bloc.dart';

class AppBloc {
  AppBloc()
      : profileAccess = ProfileAccessBloc(),
        creditCard = CreditCardBloc(),
        profileBank = ProfileBankBloc(),
        withdraw = WithdrawBloc(),
        button = ButtonBloc(),
        selectCreditCard = SelectCreditCardBloc(),
        personalProfile = ProfilePersonalBloc();

  CreditCardBloc creditCard;
  ProfilePersonalBloc personalProfile;
  ProfileAccessBloc profileAccess;
  ProfileBankBloc profileBank;
  WithdrawBloc withdraw;
  SelectCreditCardBloc selectCreditCard;

  ButtonBloc button;
}
