import 'dart:async';

import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/api/network.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignupGroupsBloc implements BlocBase {
  factory SignupGroupsBloc() {
    return SignupGroupsBloc._();
  }
  SignupGroupsBloc._() {
    this.editLoading(true);
    api.fetchGroupRecommendationWithFeed().then((List<Group> groups) {
      if (groups.isNotEmpty) {
        editHasRecommendation(true);
        _suggested.sink.add(groups);
        _canProgress.zipWith(
            SharedPreferences.getInstance().asStream(),
            (bool subject, SharedPreferences prefs) =>
                (subject ?? false) ||
                (prefs.getBool('_canProgressFromGroups') ?? false));
      } else {
        editCanProgress(true);
      }
    }).whenComplete(() => this.editLoading(false));
  }

  final NetworkApi api = NetworkApi();
  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<bool> _hasRecommendation =
      BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<List<Group>> _suggested =
      BehaviorSubject<List<Group>>();
  final BehaviorSubject<bool> _canProgress =
      BehaviorSubject<bool>.seeded(false);

  Function(bool) get editLoading => _loading.sink.add;
  Function(bool) get editHasRecommendation => _hasRecommendation.sink.add;
  // Function(List<Group>) get editSuggested => _suggested.sink.add;
  Function(bool) get _editCanProgress => _canProgress.sink.add;

  Stream<bool> get isLoading => _loading.stream;
  Stream<bool> get hasRecommendation => _hasRecommendation.stream;
  Stream<List<Group>> get suggested => _suggested.stream;
  Stream<bool> get canProgress => _canProgress.stream;

  Future<void> editCanProgress(bool value) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setBool('_canProgressFromGroups', value);
    _editCanProgress(value);
  }

  Future<bool> canProgressPrefs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('_canProgressFromGroups') ?? false;
  }

  StreamSubscription canProgressSubs;

  @override
  void dispose() {
    _loading?.close();
    _suggested?.close();
    _hasRecommendation?.close();
    _canProgress?.close();
    canProgressSubs?.cancel();
  }
}
