import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:rxdart/rxdart.dart';

class NotificationBloc implements BlocBase {
  final BehaviorSubject<int> _notificationsCounter = BehaviorSubject<int>();
  Function(int) get editNotificationsCounter => _notificationsCounter.sink.add;
  Stream<int> get notificationsCounter => _notificationsCounter.stream;
  final BehaviorSubject<int> _chatsCounter = BehaviorSubject<int>();
  Function(int) get editChatsCounter => _chatsCounter.sink.add;
  Stream<int> get chatsCounter => _chatsCounter.stream;
  @override
  void dispose() {
    _notificationsCounter?.close();
    _chatsCounter?.close();
  }
}
