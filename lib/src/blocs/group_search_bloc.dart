import 'dart:async';

import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/api/network.dart';
import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class GroupSearchBloc implements BlocBase {
  GroupSearchBloc() {
    _searchSubs = _search
        .debounceTime(const Duration(milliseconds: 200))
        .listen(_listenType);
  }
  final NetworkApi api = NetworkApi();

  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  final BehaviorSubject<String> _search = BehaviorSubject<String>();
  final BehaviorSubject<List<GroupSearch>> _searchedGroups =
      BehaviorSubject<List<GroupSearch>>();

  Function(bool) get editLoading => _loading.sink.add;
  Function(String) get editSearch => _search.sink.add;
  Function(List<GroupSearch>) get editSearchedGroups =>
      _searchedGroups.sink.add;

  Stream<bool> get isLoading => _loading.stream;
  Stream<String> get search => _search.stream;
  Stream<List<GroupSearch>> get searchedGroups => _searchedGroups.stream;

  final FocusNode searchFocusNode = FocusNode();

  StreamSubscription _searchSubs;

  @override
  void dispose() {
    _loading?.close();
    _search?.close();
    _searchSubs?.cancel();
    _searchedGroups?.close();
  }

  Future<void> _listenType(String text) async {
    print('searching for: $text');
    editLoading(true);
    final List<GroupSearch> groups = await api.searchGroupByName(text);
    editLoading(false);
    print('Lenght: ${groups.length}');
    editSearchedGroups(groups);
  }

  // Future<bool> requestGroupEntry(GroupSearch group) {
  //   //
  // }

  // Future<bool> requestGroupExit(GroupSearch group) {
  //   //
  // }
}
