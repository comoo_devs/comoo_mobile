import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class UserDetailBloc implements BlocBase {
  final BehaviorSubject<bool> _loadng = BehaviorSubject<bool>();

  Function(bool) get editLoading => _loadng.sink.add;

  Stream<bool> get isLoading => _loadng.stream;

  @override
  void dispose() {
    _loadng?.close();
  }
}
