import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/api/network.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class ChatsBloc implements BlocBase {
  ChatsBloc({@required this.updateCounters}) {
    fetchNegotiations();
  }
  final NetworkApi api = NetworkApi();
  final Function() updateCounters;
  final Firestore _db = Firestore.instance;

  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<List<Negotiation>> _chats =
      BehaviorSubject<List<Negotiation>>();
  // final BehaviorSubject<List<Negotiation>> _sellings =
  //     BehaviorSubject<List<Negotiation>>();
  // final BehaviorSubject<List<Negotiation>> _buyings =
  //     BehaviorSubject<List<Negotiation>>();

  Function(bool) get editLoading => _loading.sink.add;
  Function(List<Negotiation>) get editChats => _chats.sink.add;

  Stream<bool> get isLoading => _loading.stream;
  Stream<List<Negotiation>> get chats => _chats.stream;
  // Stream<List<Negotiation>> get sellings => _sellings.stream;
  // Stream<List<Negotiation>> get buyings => _buyings.stream;

  // Stream<List<Negotiation>> get newChats => Observable.zip2(_sellings, _buyings,
  //         (List<Negotiation> sellings, List<Negotiation> buyings) {
  //       sellings.addAll(buyings);
  //       sellings
  //           .sort((Negotiation a, Negotiation b) => a.date.compareTo(b.date));
  //       return sellings;
  //     });

  @override
  void dispose() {
    _loading?.close();
    _chats?.close();
    // _sellings?.close();
    // _buyings?.close();
  }

  Future<void> fetchNegotiations() async {
    editLoading(true);
    // api
    //     .fetchUserSellingsStream()
    //     .listen((List<Negotiation> list) => _sellings?.sink?.add(list));
    // api
    //     .fetchUserBuyingsStream()
    //     .listen((List<Negotiation> list) => _buyings?.sink?.add(list));
    List<Negotiation> negotiations = List();
    final List<dynamic> res = await api.getUserNegotiations();
    if (res.isNotEmpty) {
      final Iterable<Future<Negotiation>> result = res.map<Future<Negotiation>>(
        (dynamic item) async {
          Negotiation negotiation = Negotiation.fromJson(
            item,
            item['id'],
            product: _db.collection('products').document(item['product']),
            buyer: _db.collection('users').document(item['client']),
            seller: _db.collection('users').document(item['seller']),
          );
          // print('${negotiation.title} ${negotiation.unreadMessages}');
          if (negotiation.lastMessage.isEmpty) {
            QuerySnapshot messagesQuery = await _db
                .collection('negotiations')
                .document(item['id'])
                .collection('messages')
                .orderBy('date', descending: true)
                .getDocuments();
            DocumentSnapshot messageSnap = messagesQuery.documents.isEmpty
                ? null
                : messagesQuery.documents.first;
            negotiation.message = messageSnap == null
                ? null
                : NegotiationMessage.fromSnapshot(messageSnap);
            negotiation.lastMessage =
                negotiation.message == null ? '' : negotiation.message.text;
          }
          await negotiation.buyer.get().then((buyer) {
            negotiation.buyerName = buyer.data['name'];
          });
          await negotiation.seller.get().then((seller) {
            negotiation.sellerName = seller.data['name'];
          });
          negotiations.add(negotiation);
          editChats(negotiations);
          // if (!streamController.isClosed) {
          //   streamController.sink.add(negotiations);
          // }
          return negotiation;
        },
      );
      print('Im in');
      await Future.wait<Negotiation>(result)
          .catchError((onError) => print('error fetching negotiations'));
    } else {
      editChats(<Negotiation>[]);
    }
    editLoading(false);
    print('Im out');
  }
}
