import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:notification_permissions/notification_permissions.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RequestPermissionBloc implements BlocBase {
  RequestPermissionBloc() {
    init();
  }
  final NotificationPermissions permissionManager = NotificationPermissions();
  final String _key = 'notificationPermission';

  final BehaviorSubject<bool> _hasNotificationPermission =
      BehaviorSubject<bool>();
  Function(bool) get editNotificationPermission =>
      _hasNotificationPermission?.sink?.add;
  Stream<bool> get hasNotificationPermission =>
      _hasNotificationPermission.stream;
  @override
  void dispose() {
    _hasNotificationPermission?.close();
  }

  Future<void> init() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final bool notificationPermission = pref.getBool(_key) ?? false;
    bool hasGrantedPermission = notificationPermission;
    if (!notificationPermission) {
      final PermissionStatus status =
          await NotificationPermissions.getNotificationPermissionStatus();
      // print('Permission status: $status');
      switch (status) {
        case PermissionStatus.granted:
          hasGrantedPermission = true;
          break;
        case PermissionStatus.unknown:
          hasGrantedPermission = false;
          break;
        case PermissionStatus.denied:
          hasGrantedPermission = false;
          break;
      }
    }
    if (!_hasNotificationPermission.isClosed) {
      editNotificationPermission(hasGrantedPermission);
    }
  }

  // Future<void> _listener() async {
  //   Stream.fromFuture(
  //       NotificationPermissions.getNotificationPermissionStatus());
  // }

  Future<void> requestPermission() async {
    await NotificationPermissions.requestNotificationPermissions();
    final PermissionStatus status =
        await NotificationPermissions.getNotificationPermissionStatus();
    print(status);
  }

  Future<void> dismiss() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool(_key, true);
    editNotificationPermission(true);
  }
}
