import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:rxdart/rxdart.dart';

class ProfileBloc extends BlocBase {
  ProfileBloc();

  final UserAuth _auth = UserAuth();
  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>.seeded(false);

  Function(bool) get changeLoading => _loading.sink.add;

  Stream<bool> get isLoading => _loading.stream;

  Stream<User> get fetchUser => _auth.fetchCurrentUser().asStream();

  void dispose() {
    _loading.close();
  }
}
