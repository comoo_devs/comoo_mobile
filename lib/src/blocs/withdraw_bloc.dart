import 'package:comoo/src/blocs/bloc_validations.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:rxdart/rxdart.dart';

class WithdrawBloc with BlocValidations {
  MoneyMaskedTextController withdrawController =
      MoneyMaskedTextController(leftSymbol: 'R\$');

  BehaviorSubject<String> _withdraw = BehaviorSubject<String>();

  Function(String) get changeWithdraw => _withdraw.sink.add;

  Stream<String> get withdraw => _withdraw.transform(validateWithdraw);

  void dispose() {
    _withdraw.close();
    withdrawController.clear();
  }
}
