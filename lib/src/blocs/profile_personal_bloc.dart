import 'dart:async';

import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/blocs/bloc_validations.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/utility/states_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:rxdart/rxdart.dart';

class ProfilePersonalBloc extends BlocBase with BlocValidations {
  ProfilePersonalBloc()
      : birthdateController = MaskedTextController(mask: '00/00/0000'),
        cpfController = MaskedTextController(mask: '000.000.000-00'),
        phoneController = MaskedTextController(mask: '(00) 00000-0000'),
        zipController = MaskedTextController(mask: '00000-00'),
        addressNameController = TextEditingController(),
        addressComplementController = TextEditingController(),
        addressNumberController = TextEditingController(),
        addressDistrictController = TextEditingController(),
        stateController = TextEditingController(),
        cityController = TextEditingController(),
        _birthdate = BehaviorSubject<String>(),
        _cpf = BehaviorSubject<String>(),
        _phone = BehaviorSubject<String>(),
        _zip = BehaviorSubject<String>(),
        _addressName = BehaviorSubject<String>(),
        _addressComplement = BehaviorSubject<String>(),
        _addressDistrict = BehaviorSubject<String>(),
        _addressNumber = BehaviorSubject<String>(),
        _state = BehaviorSubject<StateItem>(),
        _city = BehaviorSubject<String>(),
        super();

  factory ProfilePersonalBloc.seeded({
    @required User user,
    @required List<StateItem> states,
  }) {
    assert(user != null);
    assert(states != null);
    String _birthdate = user.birthDate;
    String _cpf = user.cpf.isEmpty ? null : user.cpf;
    String _phone;
    if (user.phone != null) {
      if (user.phone.number != null && user.phone.areaCode != null) {
        _phone = user.phone.areaCode + user.phone.number;
      }
    }
    Address address = Address();
    StateItem state;
    if (user.address != null) {
      address = user.address;
      state = states.firstWhere(
        (StateItem s) => s.name == address.state,
        orElse: () => null,
      );
      if (address.number != null) {
        if (address.number.compareTo('0') == 0) {
          address.number = null;
        }
      }
    }
    return ProfilePersonalBloc._seeded(
      birthdate: _birthdate,
      cpf: _cpf,
      phone: _phone,
      address: address,
      state: state,
    );
  }
  ProfilePersonalBloc._seeded({
    String birthdate,
    String cpf,
    String phone,
    Address address,
    StateItem state,
    String city,
  })  : assert(address != null),
        birthdateController = birthdate == null
            ? MaskedTextController(mask: '00/00/0000')
            : MaskedTextController(text: birthdate, mask: '00/00/0000'),
        _birthdate = birthdate == null
            ? BehaviorSubject<String>()
            : BehaviorSubject<String>.seeded(birthdate),
        cpfController = cpf == null
            ? MaskedTextController(mask: '000.000.000-00')
            : MaskedTextController(text: cpf, mask: '000.000.000-00'),
        _cpf = cpf == null
            ? BehaviorSubject<String>()
            : BehaviorSubject<String>.seeded(cpf),
        phoneController = phone == null
            ? MaskedTextController(mask: '(00) 00000-0000')
            : MaskedTextController(text: phone, mask: '(00) 00000-0000'),
        _phone = phone == null
            ? BehaviorSubject<String>()
            : BehaviorSubject<String>.seeded(phone),
        zipController = address.zipCode == null
            ? MaskedTextController(mask: '00000-000')
            : MaskedTextController(text: address.zipCode, mask: '00000-000'),
        _zip = address.zipCode == null
            ? BehaviorSubject<String>()
            : BehaviorSubject<String>.seeded(address.zipCode),
        addressNameController = address.street == null
            ? TextEditingController()
            : TextEditingController(text: address.street),
        _addressName = address.street == null
            ? BehaviorSubject<String>()
            : BehaviorSubject<String>.seeded(address.street),
        addressDistrictController = address.district == null
            ? TextEditingController()
            : TextEditingController(text: address.district),
        _addressDistrict = address.district == null
            ? BehaviorSubject<String>()
            : BehaviorSubject<String>.seeded(address.district),
        addressNumberController = address.number == null
            ? TextEditingController()
            : TextEditingController(text: address.number),
        _addressNumber = address.number == null || address.number == '0'
            ? BehaviorSubject<String>()
            : BehaviorSubject<String>.seeded(address.number),
        addressComplementController = address.complement == null
            ? TextEditingController()
            : TextEditingController(text: address.complement),
        _addressComplement = address.complement == null
            ? BehaviorSubject<String>()
            : BehaviorSubject<String>.seeded(address.complement),
        stateController = state == null
            ? TextEditingController()
            : TextEditingController(text: state.displayName),
        _state = state == null
            ? BehaviorSubject<StateItem>()
            : BehaviorSubject<StateItem>.seeded(state),
        cityController = address.city == null
            ? TextEditingController()
            : TextEditingController(text: address.city),
        _city = address.city == null
            ? BehaviorSubject<String>()
            : BehaviorSubject<String>.seeded(address.city),
        super();

  final MaskedTextController phoneController;
  final MaskedTextController birthdateController;
  final MaskedTextController cpfController;
  final MaskedTextController zipController;
  final TextEditingController addressNameController;
  final TextEditingController addressNumberController;
  final TextEditingController addressComplementController;
  final TextEditingController addressDistrictController;
  final TextEditingController stateController;
  final TextEditingController cityController;

  final BehaviorSubject<bool> _edit = BehaviorSubject<bool>();
  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  final BehaviorSubject<String> _phone;
  final BehaviorSubject<String> _birthdate;
  final BehaviorSubject<String> _cpf;
  final BehaviorSubject<String> _zip;
  final BehaviorSubject<String> _addressName;
  final BehaviorSubject<String> _addressNumber;
  final BehaviorSubject<String> _addressComplement;
  final BehaviorSubject<String> _addressDistrict;
  final BehaviorSubject<StateItem> _state;
  final BehaviorSubject<String> _stateStringSubject = BehaviorSubject<String>();
  final BehaviorSubject<String> _ufSubject = BehaviorSubject<String>();
  final BehaviorSubject<String> _city;

  Function(bool) get changeEdit => _edit.sink.add;
  Function(bool) get changeLoading => _loading.sink.add;
  Function(String) get changePhone => _phone.sink.add;
  Function(String) get changeBirthdate => _birthdate.sink.add;
  Function(String) get changeCPF => _cpf.sink.add;
  Function(String) get changeZip => _zip.sink.add;
  Function(String) get changeAddressName => _addressName.sink.add;
  Function(String) get changeAddressNumber => _addressNumber.sink.add;
  Function(String) get changeAddressComplement => _addressComplement.sink.add;
  Function(String) get changeAddressDistrict => _addressDistrict.sink.add;
  Function(StateItem) get changeState => _state.sink.add;
  Function(String) get changeCity => _city.sink.add;
  Function(String) get addStateString => _stateStringSubject.sink.add;
  Function(String) get addUF => _ufSubject.sink.add;

  Stream<bool> get edit => _edit.stream;
  Stream<bool> get isLoading => _loading.stream;
  Stream<String> get phone => _phone.transform(validatePhone);
  Stream<String> get birthdate => _birthdate.transform(validateBirthdate);
  Stream<String> get cpf => _cpf.stream.transform(validateCPF);
  Stream<String> get zip => _zip.transform(validateZipCode);
  Stream<String> get addressName => _addressName.stream;
  Stream<String> get addressNumber =>
      _addressNumber.transform(validateOnlyNumbers);
  Stream<String> get addressComplement => _addressComplement.stream;
  Stream<String> get addressDistrict => _addressDistrict.stream;
  Stream<StateItem> get state => _state.stream;
  Stream<String> get city => _city.stream;
  Stream<String> get stateString => _stateStringSubject.stream;
  Stream<String> get uf => _ufSubject.stream;

  Stream<bool> get submitValid => Observable.combineLatest8(
      phone,
      birthdate,
      cpf,
      zip,
      addressName,
      addressDistrict,
      state,
      city,
      (a, b, c, d, e, f, g, h) => true);

  String get birthdateValue => _birthdate.value;
  Map<String, String> get phoneMapValue => <String, String>{
        'countryCode': '55',
        'number': _getPhoneNumber(_phone.value),
        // _phone.value.substring(2, _phone.value.length).replaceAll('-', ''),
        'areaCode': _getPhoneArea(_phone.value), //_phone.value.substring(1, ),
      };
  String get cpfValue => _cpf.value;
  String get zipValue => _zip.value;
  String get validPhone => _phone.value;
  String get addressNameValue => _addressName.value;
  String get addressNumberValue => _addressNumber.value;
  String get addressComplementValue => _addressComplement.value;
  String get addressDistrictValue => _addressDistrict.value;
  StateItem get stateValue => _state.value;
  String get validState => _stateStringSubject.value;
  String get validUF => _ufSubject.value;
  String get cityValue => _city.value;

  void dispose() {
    _edit?.close();
    _loading?.close();
    _phone?.close();
    _birthdate?.close();
    _cpf?.close();
    _zip?.close();
    _addressName?.close();
    _addressNumber?.close();
    _addressComplement?.close();
    _addressDistrict?.close();
    _state?.close();
    _city?.close();
    _stateStringSubject?.close();
    _ufSubject?.close();
  }

  String _getPhoneNumber(String phone) {
    phone = phone.replaceAll('(', '');
    phone = phone.replaceAll(')', '');
    phone = phone.replaceAll('-', '');
    phone = phone.replaceAll(' ', '');
    return phone.substring(2);
  }

  String _getPhoneArea(String phone) {
    phone = phone.replaceAll('(', '');
    phone = phone.replaceAll(')', '');
    phone = phone.replaceAll('-', '');
    phone = phone.replaceAll(' ', '');
    return phone.substring(0, 2);
  }
}
