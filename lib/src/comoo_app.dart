import 'package:comoo/src/blocs/buyings_bloc.dart';
import 'package:comoo/src/comoo/comoo.dart';
import 'package:comoo/src/pages/home/bottom_menu/group_creation/add_group.dart';
import 'package:comoo/src/pages/landing/landing_page.dart';
// import 'package:comoo/src/pages/landing/login/login.dart';
import 'package:comoo/src/pages/landing/request_access.dart';
import 'package:comoo/src/pages/signup/signup.dart';
// import 'package:comoo/src/pages/signup/tos.dart';
import 'package:comoo/src/pages/signup/signup_categories/categories_signup.dart';
import 'package:comoo/src/pages/common/transaction/bank_slip_page.dart';
import 'package:comoo/src/pages/home/profile/help/faq.dart';
import 'package:comoo/src/pages/home/profile/categories/user_categories.dart';
import 'package:comoo/src/pages/home/profile/groups/user_groups.dart';
// import 'package:comoo/src/pages/user/user_information.dart';
// import 'package:comoo/src/pages/user/user_interests.dart';
import 'package:comoo/src/pages/home/profile/products/user_products.dart';
import 'package:comoo/src/pages/signup/signup_groups/signup_groups.dart';
import 'package:comoo/src/pages/signup/signup_invite_code/signup_invite_code.dart';
import 'package:comoo/src/pages/signup/signup_personal/signup_personal.dart';
import 'package:comoo/src/pages/update_page.dart';
import 'package:comoo/src/pages/welcome.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/provider/provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/pages/home/main_page.dart';
import 'package:comoo/src/pages/home/profile/purchases/buy_history.dart';
import 'package:comoo/src/pages/home/profile/sales/sell_history.dart';
import 'package:comoo/src/pages/home/profile/shared_profit/shared_profit.dart';
import 'package:comoo/src/pages/home/profile/bank_statement/my_extract.dart';
import 'package:comoo/src/pages/home/profile/my_profile/user_profile.dart';
import 'pages/home/search/search_page.dart';
import 'pages/update_tos_page.dart';

class ComooApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Comoo comoo = Comoo();
    final ThemeData defaultTheme = Theme.of(context);
    return Provider(
      child: MaterialApp(
        // showSemanticsDebugger: true,
        locale: Locale('pt_BR'),
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: COMOO.colors.turquoise,
          accentColor: COMOO.colors.purple,
          brightness: Brightness.light,
          fontFamily: 'Montserrat',
          textTheme: defaultTheme.textTheme.copyWith(
            button: TextStyle(
              fontFamily: COMOO.fonts.montSerrat,
              color: Colors.white,
              letterSpacing: 0.56,
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          ),
          buttonTheme: defaultTheme.buttonTheme.copyWith(
            buttonColor: Color(0xFF05A0AF),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            textTheme: ButtonTextTheme.primary,
          ),
          dialogTheme: DialogTheme(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(32.0),
            ),
          ),
          // scaffoldBackgroundColor: Colors.white,
          tabBarTheme: TabBarTheme(
            labelColor: Color.fromRGBO(5, 160, 175, 1.0),
            unselectedLabelColor: Color.fromRGBO(60, 55, 70, 1.0),
          ),
        ),
        title: 'COMOO',
        home: WelcomePage(comoo),
//        home: SignUpGroups(),
        routes: <String, WidgetBuilder>{
          '/update': (BuildContext context) => UpdatePage(),
          '/agreeToS': (BuildContext context) => UpdateToSPage(),
          '/landing_page': (BuildContext context) => LandingPage(),
          '/login': (BuildContext context) => LandingPage(),
          '/sign_up': (BuildContext context) => SignUpPage(),
          '/sign_up_personal': (BuildContext context) => SignUpPersonal(),
          '/sign_up_groups': (BuildContext context) => SignUpGroups(),
          // '/tos': (BuildContext context) => TermsOfServicePage(),
          '/cat_sign_up': (BuildContext context) => CategoriesSignUpPage(),
          '/home': (BuildContext context) => MainPage(comoo),
          '/request_access': (BuildContext context) => RequestAccessPage(),
          '/add_group': (BuildContext context) => AddGroupPage(),
          '/user_products': (BuildContext context) => UserProductsPage(),
          '/user_groups': (BuildContext context) => UserGroupsPage(),
          // '/user_interests': (BuildContext context) => UserInterestsView(),
          // '/user_information': (BuildContext context) => UserInformationView(),
          '/faq': (BuildContext context) => FAQPage(),
          '/user_profile': (BuildContext context) => UserProfilePage(),
          '/buy_history': (BuildContext context) => BlocProvider<BuyingsBloc>(
                bloc: BuyingsBloc(),
                child: BuyHistory(),
              ),
          '/sell_history': (BuildContext context) => SellHistory(),
          '/shared_profit': (BuildContext context) => SharedProfitPage(),
          '/my_extract': (BuildContext context) => MyExtract(),
          '/user_categories': (BuildContext context) => UserCategoriesPage(),
          // '/create_payment': (BuildContext context) => CreatePaymentMethod(),
          '/bank_slip': (BuildContext context) => BankSlipPage(),
        },
        onGenerateRoute: (RouteSettings settings) {
          final String route = settings.name;
          switch (route) {
            case '/search':
              return MaterialPageRoute(
                builder: (BuildContext context) => SearchPage(),
              );
              break;
            case '/sign_up_personal':
              final Object arguments = settings.arguments;
              final showBackButton =
                  arguments is bool ? arguments ?? true : true;
              return MaterialPageRoute(
                builder: (BuildContext context) =>
                    SignUpPersonal(showBackButton: showBackButton),
              );
              break;
            default:
              return MaterialPageRoute(
                builder: (BuildContext context) => WelcomePage(comoo),
              );
              break;
          }
        },
        showPerformanceOverlay: false,
      ),
    );
  }
}
