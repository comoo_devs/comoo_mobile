import 'package:comoo/src/blocs/withdraw_bloc.dart';
import 'package:comoo/src/provider/provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:comoo/src/widgets/keyboard_dismissible.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:comoo/src/widgets/stream_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class WithdrawPage extends StatelessWidget with Reusables {
  WithdrawPage({Key key}) : super(key: key);

  final NetworkApi api = NetworkApi();
  final NumberFormat formater =
      NumberFormat.currency(locale: "pt_BR", symbol: "R\$");

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    WithdrawBloc bloc = Provider.of(context).withdraw;
    final TextStyle textStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      color: Color(0xFF3C3746),
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w500,
      letterSpacing: mediaQuery * -0.144,
    );
    return KeyboardDismissible(
      child: Scaffold(
        appBar: CustomAppBar.round(
          color: COMOO.colors.lead,
          title: Text('Meu extrato', style: ComooStyles.appBarTitleSixteen),
        ),
        body: Container(
          alignment: Alignment.center,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: mediaQuery * 62.0),
            children: <Widget>[
              SizedBox(height: mediaQuery * 52.0),
              Image.asset(
                'images/logo_new.png',
                height: mediaQuery * 75,
                width: mediaQuery * 75,
              ),
              SizedBox(height: mediaQuery * 11.0),
              Text(
                'Seu saldo disponível',
                textAlign: TextAlign.center,
                style: textStyle,
              ),
              SizedBox(height: mediaQuery * 11.0),
              FutureBuilder<dynamic>(
                future: _fetchBalance(),
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  final double balance = snapshot.hasData
                      ? (snapshot.data['current']['amount'] as int) / 100
                      : 0.0;
                  return Text(
                    '${formater.format(balance)}',
                    textAlign: TextAlign.center,
                    style: textStyle.copyWith(fontSize: mediaQuery * 25.0),
                  );
                },
              ),
              SizedBox(height: mediaQuery * 87.0),
              Text(
                'Quanto você deseja resgatar?',
                textAlign: TextAlign.center,
                style: textStyle,
              ),
              InputStream(
                stream: bloc.withdraw,
                onChanged: bloc.changeWithdraw,
                style: textStyle.copyWith(fontSize: mediaQuery * 25.0),
                keyboardType: TextInputType.number,
                controller: bloc.withdrawController,
                decoration: InputDecoration(hintText: 'R\$ 0,00'),
                textCapitalization: TextCapitalization.none,
                // textAlign: TextAlign.center,
              ),
              SizedBox(height: mediaQuery * 92.0),
              StreamBuilder<String>(
                stream: bloc.withdraw,
                builder:
                    (BuildContext context, AsyncSnapshot<String> snapshot) {
                  return StreamRaisedButton(
                    onPressed: snapshot.hasData
                        ? () async {
                            await _transferValues(context, bloc);
                          }
                        : null,
                    text: 'TRANSFERIR',
                  );
                },
              ),
              SizedBox(height: mediaQuery * 102.0),
            ],
          ),
        ),
      ),
    );
  }

  Future<dynamic> _fetchBalance() async {
    return api.getAllBalance();
  }

  Future<void> _transferValues(BuildContext context, WithdrawBloc bloc) async {
    print('');
    final int value = (bloc.withdrawController.numberValue * 100).floor();
    final bool success = await api.createTransfer(context, <String, int>{
      "amount": value,
    });
    if (success ?? false) {
      _showSuccessDialog(context);
    } else {
      showCustomDialog(
        context: context,
        alert: customRoundAlert(
            context: context,
            title: 'Falha ao enviar requisição.\n'
                'Tente novamente mais tarde.'),
      );
    }
  }

  _showSuccessDialog(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    showCustomDialog(
      context: context,
      alert: AlertDialog(
        contentPadding: EdgeInsets.all(mediaQuery * 0.0),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: mediaQuery * 329.0,
              decoration: BoxDecoration(
                color: ComooColors.pink,
                borderRadius: BorderRadius.all(
                  Radius.circular(mediaQuery * 32.0),
                ),
              ),
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //  SizedBox(height: mediaQuery * 26.0),
                    Container(
                      margin: EdgeInsets.only(
                        top: mediaQuery * 31.0,
                        left: mediaQuery * 10.0,
                        right: mediaQuery * 10.0,
                        bottom: mediaQuery * 10.0,
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: mediaQuery * 8.0,
                      ),
                      child: Text(
                        'Sua transferência foi\n'
                            'realizada com sucesso!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: COMOO.fonts.montSerrat,
                          color: Colors.white,
                          fontSize: mediaQuery * 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: mediaQuery * 0.0,
                        left: mediaQuery * 10.0,
                        right: mediaQuery * 10.0,
                        bottom: mediaQuery * 10.0,
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: mediaQuery * 8.0,
                      ),
                      child: Text(
                        'Em no máximo 48 horas o\n'
                            'saldo estará disponível na\n'
                            'sua conta bancária.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: COMOO.fonts.montSerrat,
                          color: Colors.white,
                          fontSize: mediaQuery * 20.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    ButtonBar(
                      mainAxisSize: MainAxisSize.min,
                      alignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: mediaQuery * 41.0,
                          width: mediaQuery * 163.0,
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1.0,
                              color: Colors.white,
                            ),
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            elevation: 0.0,
                            color: Colors.white,
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'OK',
                              style: TextStyle(
                                color: COMOO.colors.pink,
                                fontFamily: COMOO.fonts.montSerrat,
                                fontSize: mediaQuery * 15.0,
                                letterSpacing: mediaQuery * 0.6,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
