import 'dart:async';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/pages/common/product/product_page.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:intl/intl.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/message.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/transaction/transaction_view.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'package:comoo/src/widgets/chat/chat_message.dart';

// import 'package:comoo/widgets/negotiation_seller_drawer.dart';
// import 'package:comoo/widgets/negotiation_buyer_drawer.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:shared_preferences/shared_preferences.dart';

class DirectChatView extends StatefulWidget {
  final Negotiation negotiation;

  DirectChatView(this.negotiation);

  @override
  State createState() {
    return _DirectChatState(this.negotiation);
  }
}

class _DirectChatState extends State<DirectChatView> with Reusables {
  final NetworkApi api = NetworkApi();
  _DirectChatState(this.negotiation)
      : _isBuyer = (negotiation.buyer.documentID == currentUser?.uid) ?? false;

  final bool _isBuyer;
  static final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>();

  Negotiation negotiation;
  final TextEditingController _textController = TextEditingController();
  final Firestore db = Firestore.instance;
  String counterpart = '';
  bool _loading = false;
  // bool _firstTime = false;
  String _loadingMessage = 'Carregando...';

  StreamSubscription<QuerySnapshot> streamSubscription;
  bool _isComposing = false;
  final List<ChatMessage> messages = <ChatMessage>[];

  Future<void> _onQuerySnapshot(QuerySnapshot snapshot) async {
    final List<Future> cm = <Future>[];
    snapshot.documentChanges.forEach((DocumentChange documentChange) {
      if (documentChange.type == DocumentChangeType.added) {
        //fetch data to avoid redraw on setState()
        final Message message = Message.fromSnapshot(documentChange.document);
        cm.add(message.fetchUser().then((User user) {
          return ChatMessage(
              key: GlobalObjectKey(message.id), message: message, user: user);
        }));
      }
    });
    Future.wait(cm).then((List newMessages) {
      if (newMessages.length > 0) {
        if (mounted) {
          setState(() {
            messages.insertAll(0, newMessages.cast<ChatMessage>());
            messages.sort((ChatMessage m1, ChatMessage m2) =>
                m2.message.date.compareTo(m1.message.date));
          });
        }
      }
    });
  }

  Future<void> _handleSubmitted(String text) async {
    if (negotiation.status == 'canceled') {
      Scaffold.of(context).showSnackBar(
          SnackBar(content: Text('Esta transação foi cancelada')));
      return;
    }

    _textController.clear();
    if (mounted) {
      setState(() {
        _isComposing = false;
      });
    }
    if (text?.isNotEmpty ?? false) {
      _sendMessage(text: text);

      final DocumentReference negotiationDocRef =
          db.collection('negotiations').document(negotiation.id);
      WriteBatch batch = db.batch();
      batch.updateData(negotiationDocRef, {
        'lastUpdate': DateTime.now(),
      });
      batch.commit();
    }
  }

  void _sendMessage({String text}) async {
    var messageRef = db
        .collection('negotiations')
        .document(this.negotiation.id)
        .collection('messages')
        .document();
    DocumentReference userRef =
        db.collection('users').document(currentUser.uid);
    Message newMessage =
        Message(text, userRef, DateTime.now(), MessageType.message);

    await messageRef.setData(newMessage.toJson());

    //Message Notification
    CloudFunctions.instance
        .getHttpsCallable(functionName: 'generateUserNotifications')
        .call(<String, dynamic>{
      'type': 'negotiation_chat',
      'ref': negotiation.id,
      'userId': currentUser.uid,
      'text': text,
    }).catchError((error) {
      print('>> generateUserNotifications Error!\n$error');
      var result = {'result': 'error!'};
      return result;
    });
    // var batch = db.batch();
    // bool isCurrentUserBuyer = currentUser.uid == negotiation.buyer.documentID;
    // var userSnapshot = isCurrentUserBuyer
    //     ? await negotiation.seller.get()
    //     : await negotiation.buyer.get();

    // var userRequestNotificationRef = db
    //     .collection('users')
    //     .document(userSnapshot.documentID)
    //     .collection('notifications')
    //     .document();

    // batch.setData(userRequestNotificationRef, {
    //   'id': userRequestNotificationRef.documentID,
    //   'thumb': negotiation.thumb,
    //   'from': currentUser.uid,
    //   'type': 'negotiation_chat',
    //   'date': DateTime.now(),
    //   'name': currentUser.name,
    //   'title': currentUser.name,
    //   'group': negotiation.id,
    //   'text': text,
    // });
    // batch.commit();
  }

  Widget _buildTextComposer() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return IconTheme(
        data: IconThemeData(color: ComooColors.roxo),
        child: Container(
          color: ComooColors.cinzaClaro,
          margin: EdgeInsets.symmetric(horizontal: mediaQuery * 8.0),
          child: Row(
            children: <Widget>[
              Flexible(
                child: Container(
                  // color: Colors.white,
                  height: mediaQuery * 40.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(mediaQuery * 20.0),
                  ),
                  child: TextField(
                    enabled: negotiation.status != 'cancelled' &&
                        negotiation.status != 'finished',
                    controller: _textController,
                    onChanged: (String text) {
                      if (mounted) {
                        setState(() {
                          _isComposing = text.length > 0 &&
                              negotiation.status != 'cancelled' &&
                              negotiation.status != 'finished';
                        });
                      }
                    },
                    textCapitalization: TextCapitalization.sentences,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: mediaQuery * 16.0,
                      fontWeight: FontWeight.w500,
                      color: ComooColors.chumbo,
                    ),
                    onSubmitted: _isComposing ? _handleSubmitted : null,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(
                        left: mediaQuery * 10.0,
                        top: mediaQuery * 10.0,
                      ),
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: mediaQuery * 14.0,
                      ),
                      hintText: 'Enviar mensagem',
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: mediaQuery * 4.0),
                child: IconButton(
                    icon: Icon(
                      ComooIcons.chatSend,
                      size: mediaQuery * 35.0,
                    ),
                    onPressed: _isComposing
                        ? () => _handleSubmitted(_textController.text)
                        : null),
              ),
            ],
          ),
        ));
  }

  Future<void> cancellNegotiation() async {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    await showCustomDialog<bool>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Tem certeza que deseja cancelar essa transação?\n'
            'Essa ação não poderá ser desfeita.',
        textAlign: TextAlign.center,
        actions: [
          Container(
            height: mediaQuery * 25.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              // color: Colors.white,
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: Text(
                'SIM',
                style: TextStyle(
                  color: COMOO.colors.pink,
                ),
              ),
            ),
          ),
          Container(
            height: mediaQuery * 25.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              child: Text(
                'NÃO',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    ).then((bool result) {
      if (result ?? false) {
        setState(() {
          _loading = true;
          _loadingMessage = 'Cancelando transação';
        });
        negotiation.status = 'cancelled';
        return CloudFunctions.instance
            .getHttpsCallable(functionName: 'cancelNegotiation')
            .call(<String, dynamic>{'negotiationId': this.negotiation.id}).then(
                (_) {
          setState(() {
            _loading = false;
          });
          Navigator.pop(context);
          return true;
        }).catchError((onError) => false);
      }
      return result;
    });
  }

  Widget _productDescription() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final NumberFormat formater =
        NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR');
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: mediaQuery * 14.0, vertical: mediaQuery * 8.0),
      child: Column(
        children: <Widget>[
          GestureDetector(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: mediaQuery * 55.0,
                  width: mediaQuery * 55.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      image: DecorationImage(
                          image: NetworkImage(
                            negotiation.thumb,
                          ),
                          fit: BoxFit.fill)),
                ),
                SizedBox(
                  width: mediaQuery * 12.0,
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        negotiation.title,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: ComooStyles.texto_bold,
                      ),
                      Text(
                        // this.counterpart,
                        formater.format(negotiation.price),
                        style: ComooStyles.productChatSeller,
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        timeago.format(negotiation.lastUpdate, locale: 'pt_BR'),
                        textAlign: TextAlign.right,
                        style: ComooStyles.productChatSeller,
                      ),
                      Text(
                        negotiation.location ?? '',
                        textAlign: TextAlign.right,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: ComooStyles.productChatSeller,
                      )
                    ],
                  ),
                ),
              ],
            ),
            onTap: _fetchProduct,
          ),
          SizedBox(
            height: mediaQuery * 15.0,
          ),
          negotiation.buyer.documentID == currentUser.uid
              ? _buildCustomerActions()
              : _buildSellerActions(),
          _buildStatusBar()
        ],
      ),
    );
  }

  Future<void> _sendDeliveryNotification() async {
    final bool result = await showDialog<bool>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                'CONFIRMAR ENVIO',
                style: ComooStyles.productChatTitle,
              ),
              content: Text(
                'VOCÊ ESTÁ PRESTES A INFORMAR AO CLIENTE QUE JÁ ENVIOU O PRODUTO, TEM CERTEZA DISSO?',
                style: ComooStyles.texto,
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('CANCELAR'),
                  onPressed: () => Navigator.of(context).pop(false),
                ),
                FlatButton(
                  child: Text('CONFIRMAR'),
                  onPressed: () => Navigator.of(context).pop(true),
                )
              ]);
        });

    if (result ?? false) {
      //SEND
      setState(() {
        _loading = true;
        _loadingMessage = 'Enviando Informação...';
      });
      this.negotiation.status = 'sent';
      db.collection('negotiations').document(this.negotiation.id).updateData(
          {'status': 'sent', 'deliveryDate': DateTime.now()}).then((_) {
        setState(() {
          _loading = false;
        });
      });
    }
  }

  _buildSellerActions() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    switch (negotiation.status) {
      case 'cancelled':
        return Container();
      case 'paid':
      case 'wait_send':
        return RaisedButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          color: ComooColors.roxo,
          child: Text(
            'CONFIRMAR ENTREGA',
            style: ComooStyles.botao_branco,
          ),
          onPressed: () {
            _sendDeliveryNotification();
          },
        );
      case 'sent':
        return Text(
          'PRODUTO ENVIADO',
          style: ComooStyles.texto_bold,
        );
      default:
        Widget extendTermButton;
        Widget cancelButton;
        if (negotiation.date != null) {
          DateTime now = DateTime.now();
          cancelButton = Container(
            height: mediaQuery * 23.0,
            child: RaisedButton(
              // borderSide: BorderSide(color: ComooColors.turqueza),
              elevation: 0.0,
              color: ComooColors.turqueza,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
              // shape: RoundedRectangleBorder(
              //     borderRadius: BorderRadius.circular(14.0)),
              onPressed: negotiation.status == 'under negotiation'
                  ? now.difference(negotiation.date).inMilliseconds >= 86400000
                      ? () {
                          cancellNegotiation();
                        }
                      : null
                  : null,
              child: Text(
                'CANCELAR VENDA',
                style: ComooStyles.botaoChat,
              ),
            ),
          );
        }
        // if (negotiation.lastUpdate != null) {
        //   DateTime now = DateTime.now();
        //   now.difference(negotiation.lastUpdate).inMilliseconds >= 3600000
        //       ? extendTermButton = FlatButton(
        //           shape: RoundedRectangleBorder(
        //               borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
        //           color: ComooColors.turqueza,
        //           child: Text(
        //             'ESTENDER PRAZO',
        //             style: ComooStyles.botao_branco,
        //           ),
        //           onPressed: () {
        //             _transferValues();
        //           },
        //         )
        //       : extendTermButton = FlatButton(
        //           shape: RoundedRectangleBorder(
        //               borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
        //           color: ComooColors.cinzaClaro,
        //           child: Text(
        //             'ESTENDER PRAZO',
        //             style: ComooStyles.botao_branco,
        //           ),
        //           onPressed: () {
        //             // _sendDeliveryNotification();
        //           },
        //         );
        //   // return button;
        // }
        return cancelButton != null
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  cancelButton,
                  extendTermButton ?? Container(),
                ],
              )
            : Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  extendTermButton ?? Container(),
                ],
              );
    }
  }

  _confirmDelivery() async {
    var result = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                'CONFIRMAR RECEBIMENTO',
                style: ComooStyles.texto_bold,
              ),
              content: Text(
                  'Ao confirmar, você informa que recebeu o produto e a transação será concluída.',
                  style: ComooStyles.texto),
              actions: <Widget>[
                FlatButton(
                  child: Text('CANCELAR'),
                  onPressed: () => Navigator.of(context).pop(false),
                ),
                FlatButton(
                  child: Text('CONFIRMAR'),
                  onPressed: () => Navigator.of(context).pop(true),
                )
              ]);
        });

    if (result == true) {
      //SEND
      setState(() {
        _loading = true;
        _loadingMessage = 'Finalizando transação...';
      });
      this.negotiation.status = 'finished';
      db.collection('negotiations').document(this.negotiation.id).updateData(
          {'status': 'finished', 'receivedDate': DateTime.now()}).then((_) {
        setState(() {
          this.negotiation.status = 'finished';
          _loading = false;
        });
      });
      //  CloudFunctions.instance
      //       .call(functionName: 'finishNegotiation', parameters: {
      //         'customerId': this.negotiation.buyer.documentID,
      //         'sellerId': this.negotiation.seller.documentID,
      //         'negotiationId': this.negotiation.id
      //       })
      //       .timeout(Duration(minutes: 1))
      //       .then(((result) {
      //         setState(() {
      //           this.negotiation.status = 'finished';
      //           _loading = false;
      //         });
      //       }))
      //       .catchError((error) {
      //         print(error);
      //         setState(() {
      //           this.negotiation.status = 'finished';
      //           _loading = false;
      //         });
      //         final snackBar = SnackBar(
      //             content: Text('Não foi possível finalizar transação.'));
      //         _scaffoldKey.currentState.showSnackBar(snackBar);
      //       });
    }
  }

  Widget _buildCustomerActions() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    if (negotiation.status == 'cancelled' || negotiation.status == 'finished')
      return Container();
    if (negotiation.status == 'paid')
      return Container(
        child: Text(
          'AGUARDANDO ENVIO',
          style: ComooStyles.texto_bold,
        ),
      );
    if (negotiation.status == 'sent' || negotiation.status == 'wait_send') {
      return RaisedButton(
        elevation: 0.0,
        color: ComooColors.turqueza,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(14.0)),
        onPressed: () {
          _confirmDelivery();
        },
        child: Text(
          'CONFIRMAR RECEBIMENTO',
          style: ComooStyles.botao_branco,
        ),
      );
    }
    List<Widget> children = List<Widget>();
    if (negotiation.status == 'under negotiation' ||
        negotiation.status == 'pending') {
      children.add(Container(
        height: mediaQuery * 23.0,
        child: OutlineButton(
          borderSide: BorderSide(color: ComooColors.turqueza),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(14.0)),
          onPressed: negotiation.status == 'under negotiation'
              ? () {
                  cancellNegotiation();
                }
              : null,
          child: Text(
            'LIBERAR',
            style: ComooStyles.botaoTurquezaChat,
          ),
        ),
      ));
    }
    if (negotiation.payment != null &&
        negotiation.payment['method'] == 'bank_slip' &&
        negotiation.status != 'under negotiation') {
      children.add(Container(
        height: 23.0,
        child: RaisedButton(
          color: ComooColors.roxo,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(14.0)),
          onPressed: () {
            _launchURL(negotiation.payment['pdf']);
          },
          child: Text(
            'VER BOLETO',
            style: ComooStyles.botaoChat,
          ),
        ),
      ));
    }

    if (negotiation.status == 'under negotiation') {
      children.add(
        Container(
          height: mediaQuery * 23.0,
          child: RaisedButton(
            elevation: 0.0,
            color: ComooColors.turqueza,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
            onPressed: negotiation.paid
                ? null
                : () {
                    _confirmNegotiation();
                  },
            child: Text(
              'COMPRAR',
              style: ComooStyles.botaoChat,
            ),
          ),
        ),
      );
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }

  Widget _buildStatusBar() {
    if (negotiation.status == 'cancelled') {
      return Container(
        alignment: FractionalOffset.center,
        child: Text(
          'TRANSAÇÃO CANCELADA',
          textAlign: TextAlign.center,
          style: ComooStyles.texto_bold,
        ),
      );
    } else {
      return Container();
    }
  }

  Future<void> _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  // Future<void> _checkFirstTime() async {
  //   if (this.negotiation.seller.documentID == currentUser.uid) {
  //     return false;
  //   }
  //   final SharedPreferences pref = await SharedPreferences.getInstance();
  //   bool firstTimeNegotiating = false;
  //   try {
  //     firstTimeNegotiating = pref.getBool('firstTimeNegotiating');
  //     if (firstTimeNegotiating == null) {
  //       pref.setBool('firstTimeNegotiating', false);
  //       firstTimeNegotiating = true;
  //     }
  //   } catch (e) {
  //     firstTimeNegotiating = false;
  //   }
  //   setState(() {
  //     _firstTime = firstTimeNegotiating;
  //   });
  // }

  Future<void> notifyOpenedChatPage() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setString('chatOpened', widget.negotiation.id);
  }

  Future<void> unnotifyOpenedChatPage() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setString('chatOpened', null);
  }

  @override
  void deactivate() {
    api.clearChatCounter(widget.negotiation.id);
    unnotifyOpenedChatPage();
    super.deactivate();
  }

  @override
  void initState() {
    api.clearChatCounter(widget.negotiation.id);
    notifyOpenedChatPage();
    super.initState();
    // _checkFirstTime();
    _fetchCounterpartInformation();
    this.streamSubscription = db
        .collection('negotiations')
        .document(this.negotiation.id)
        .collection('messages')
        .orderBy('date', descending: true)
        .snapshots()
        .listen(_onQuerySnapshot);
    // _fetchNegotiation();
  }

  Future<void> _fetchCounterpartInformation() async {
    if (currentUser.uid == widget.negotiation.buyer.documentID) {
      widget.negotiation.seller.get().then((userSnapshot) {
        counterpart = userSnapshot.data['name'];
      });
    } else {
      widget.negotiation.buyer.get().then((userSnapshot) {
        counterpart = userSnapshot.data['name'];
      });
    }
  }

  Future<void> _fetchNegotiation() async {
    final DocumentSnapshot snap =
        await db.collection('negotiations').document(this.negotiation.id).get();
    if (mounted) {
      setState(() {
        negotiation = Negotiation.fromSnapshot(snap);
      });
    }
  }

  Future<void> _confirmNegotiation() async {
    if (currentUser.sellerStatus == 'accepted') {
      await Navigator.push(
          context,
          MaterialPageRoute<void>(
              builder: (context) =>
                  TransactionView(negotiation: this.negotiation)));
      // Navigator.pop(context, result);
      await _fetchNegotiation();
      print(negotiation.paid);
    } else {
      showIncompleteStatusDialog().then((result) {
        if (result != null) if (result) {
          // Navigator.of(context).pushNamed('/user_profile');
          Navigator.of(context).pushNamed('/sign_up_personal');
        }
      });
    }
  }

  Future<bool> showIncompleteStatusDialog() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return showCustomDialog<bool>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title:
            'Para começar a comprar na COMOO você precisa acrescentar mais dados ao seu cadastro',
        actions: <Widget>[
          Container(
            height: mediaQuery * 41.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              child: Text(
                'DEPOIS',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Container(
            height: mediaQuery * 41.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: Text(
                'AGORA',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _fetchProduct() async {
    DocumentSnapshot snap = await negotiation.product.get();
    Product product = Product.fromSnapshot(snap);
    Navigator.of(context).push(
        MaterialPageRoute<void>(builder: (context) => ProductPage(product)));
  }

  @override
  Widget build(BuildContext context) {
    // final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.roxo,
        title: Text(
          counterpart,
          style: ComooStyles.appBarTitleWhite,
        ),
      ),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              _productDescription(),
              Divider(
                color: ComooColors.chumbo,
              ),
              Flexible(
                fit: FlexFit.tight,
                child: messages.isEmpty
                    ? Center(child: CircularProgressIndicator())
                    : ListView.builder(
                        itemCount: messages.length + 1,
                        reverse: true,
                        itemBuilder: (BuildContext context, int index) {
                          if (index == messages.length) {
                            return _buildNegotiationMessage(context);
                          }
                          return messages[index];
                        },
                      ),
              ),
              const Divider(height: 1.0),
              Container(
                decoration: BoxDecoration(
                  // color: Theme.of(context).cardColor,
                  color: ComooColors.cinzaClaro,
                ),
                child: _buildTextComposer(),
              ),
            ],
          ),
          _loading ? LoadingContainer(message: _loadingMessage) : Container(),
        ],
      ),
    );
  }

  Widget _buildNegotiationMessage(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: responsive(context, 15.0)),
      child: Center(
        child: _isBuyer
            ? Text(
                'Oba, tá na hora de comprar! O produto está reservado para você por 24 horas.'
                ' Aqui você pode tirar todas as dúvidas com o vendedor e combinar a'
                ' forma e o local de entrega do produto. Boa compra!',
                style: ComooStyles.productChatSeller,
                textAlign: TextAlign.left,
              )
            : counterpart?.isEmpty ?? true
                ? Container()
                : RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: '${counterpart ?? ''}',
                      style: ComooStyles.productChatSeller
                          .copyWith(fontWeight: FontWeight.bold),
                      children: <TextSpan>[
                        TextSpan(
                            text: ' iniciou o processo de compra do seu produto'
                                ' Esclareça todas as dúvidas e não se esqueça de combinar a entrega do produto.'
                                ' Boa venda!',
                            style: ComooStyles.productChatSeller)
                      ],
                    ),
                  ),
      ),
    );
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    super.dispose();
  }
}
