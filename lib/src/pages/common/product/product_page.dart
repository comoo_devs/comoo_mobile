import 'dart:async';
import 'dart:ui' as ui;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/comoo/comoo.dart';
import 'package:comoo/src/pages/common/product/product_republish.dart';
import 'package:comoo/src/pages/home/bottom_menu/product_creation/product_create.dart';
import 'package:comoo/src/widgets/custom_cache_manager.dart';
import 'package:comoo/src/widgets/custom_popupmenu.dart';
import 'package:comoo/src/widgets/custom_tooltip.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/dots_indicator.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/pages/common/chat/direct_chat.dart';
import 'package:comoo/src/widgets/feed/product_feed.dart';
import 'package:comoo/src/widgets/recomendation/group_recommendation.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/comments.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:intl/intl.dart';
import 'package:pinch_zoom_image/pinch_zoom_image.dart';

// Product product =  Product();

class ProductPage extends StatefulWidget {
  ProductPage(this.product);
  final Product product;
  // final bool scroll;

  @override
  State createState() {
    return _ProductPageState();
  }
}

class _ProductPageState extends State<ProductPage> with Reusables {
  _ProductPageState();
  Product product;
  bool userHasRequested = true;
  bool _isLoading = false;
  // bool _scroll = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final Firestore db = Firestore.instance;
  final _photoController = PageController();
  // final ScrollController _pageScrollController = ScrollController();

  final NumberFormat formatter =
      NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');
  static const Duration _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;
  bool liked = false;
  bool loadingLike = false;
  User _user;
  List<Group> _groups = List<Group>();
  List<Comment> _comments = List<Comment>();

  final TextEditingController _textController = TextEditingController();
  bool _isComposing = false;
  // bool userHasRequested = false;
  // bool _loading = false;
  // String _loadingMessage = 'Carregando...';

  void _navigateToUserDetail(User user) {
    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => UserDetailView(user)));
  }

  // void _navigateToGroupDetail(String id) {
  //   Navigator.of(context).push(MaterialPageRoute<void>(
  //       builder: (BuildContext context) => GroupDetailView(id: id)));
  // }

  Widget _buildWantButton() {
    // print(userHasRequested);
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      height: mediaQuery * 48.0,
      // width: 76.0,
      padding: EdgeInsets.only(
        left: mediaQuery * 10.0,
        right: mediaQuery * 10.0,
        bottom: mediaQuery * 10.0,
        top: mediaQuery * 8.0,
      ),
      child: RaisedButton(
          elevation: 0.0,
          color: ComooColors.pink,
          child: Text('COMPRAR', style: ComooStyles.quero),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          onPressed: userHasRequested
              ? null
              : () {
                  _showRequestAlert();
                  requestProduct();
                }),
    );
  }

  _showRequestAlert() {
    showCustomDialog(
      context: context,
      barrierDismissible: false,
      alert: customRoundAlert(
        context: context,
        title: 'Requisição sendo enviada.\nAguarde um momento.',
        actions: [],
      ),
    );
  }

  Widget _buildSellerRow() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Padding(
      padding: EdgeInsets.only(top: mediaQuery * 14.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            child: Container(
              margin: EdgeInsets.only(
                left: mediaQuery * 15.0,
                right: mediaQuery * 10.0,
              ),
              width: mediaQuery * 50.0,
              height: mediaQuery * 50.0,
              child: ClipOval(
                child: Image(
                  image: CachedNetworkImageProvider(
                    product.user['picture'],
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            onTap: () {
              _navigateToUserDetail(_user);
            },
          ),
          Expanded(
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    _navigateToUserDetail(_user);
                  },
                  child: Container(
                    child: Text(
                      product.user['name'],
                      style: ComooStyles.nomeFeed,
                    ),
                  ),
                ),
                Container(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    text: TextSpan(
                        text: '${product.location} ',
                        style: ComooStyles.localizacao,
                        children: <TextSpan>[
                          TextSpan(
                              text: timeago.format(product.date,
                                      locale: 'pt_BR') +
                                  ' ',
                              style: ComooStyles.datas),
                        ]),
                  ),
                ),
                // Container(
                //   height: mediaQuery * 20.0,
                //   child: ListView(
                //     scrollDirection: Axis.horizontal,
                //     children: product.groups
                //         .map(
                //           (g) => GestureDetector(
                //                 child: Padding(
                //                   padding:
                //                       EdgeInsets.only(right: mediaQuery * 8.0),
                //                   child: Text(
                //                     g['name'],
                //                     style: ComooStyles.localizacao,
                //                   ),
                //                 ),
                //                 onTap: () {
                //                   _navigateToGroupDetail(g['id']);
                //                 },
                //               ),
                //         )
                //         .toList(),
                //   ),
                // ),
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
            ),
          ),
          // _user.uid == currentUser.uid
          //     ?  PopupMenuButton<ProductMenu>(
          //         onSelected: (ProductMenu result) {
          //           switch (result) {
          //             case ProductMenu.update:
          //               _updateProduct();
          //               break;
          //             case ProductMenu.delete:
          //               _deleteProduct();
          //           }
          //         },
          //         itemBuilder: (BuildContext context) =>
          //             <PopupMenuEntry<ProductMenu>>[
          //               const PopupMenuItem<ProductMenu>(
          //                 value: ProductMenu.update,
          //                 child: const Text('Editar produto'),
          //               ),
          //               const PopupMenuItem<ProductMenu>(
          //                 value: ProductMenu.delete,
          //                 child: const Text('Deletar produto'),
          //               ),
          //             ],
          //       )
          //     :  Container()
          _buildWantButton(),
        ],
      ),
    );
  }

  Widget _buildPhotoRow() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    Widget _child;
    double maxHeight = 275.0;
    if (product.photos.length <= 1) {
      _child = Container(
        // height: 475.0,
        constraints: BoxConstraints(
          maxHeight: maxHeight,
          maxWidth: MediaQuery.of(context).size.width,
        ),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: CachedNetworkImageProvider(product.photos[0]),
                fit: BoxFit.cover)),
        alignment: FractionalOffset.topCenter,
      );
    } else {
      _child = Container(
        // height: 475.0,
        constraints: BoxConstraints(
          maxHeight: maxHeight,
          maxWidth: MediaQuery.of(context).size.width,
        ),
        child: PageView.builder(
            controller: _photoController,
            itemCount: product.photos.length,
            physics: AlwaysScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return PinchZoomImage(
                zoomedBackgroundColor: Colors.white.withOpacity(0.2),
                hideStatusBarWhileZooming: true,
                image: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Image(
                    // image: AdvancedNetworkImage(product.photos[index],
                    //     useDiskCache: true),
                    image: CachedNetworkImageProvider(product.photos[index]),
                    fit: BoxFit.fitWidth,
                  ),
                ),
              );
            }),
      );
    }

    return Stack(
      children: <Widget>[
        GestureDetector(
          child: _child,
          onTap: () {
            String photo = product.photos[
                product.photos.length <= 1 ? 0 : _photoController.page.round()];
            Navigator.of(context).push(MaterialPageRoute<void>(
              builder: (context) => ProductImagePage(
                photo: photo,
              ),
            ));
          },
        ),
        Positioned(
          right: mediaQuery * 20.0,
          top: mediaQuery * 20.0,
          child: Container(
            width: mediaQuery * 120.0,
            height: mediaQuery * 30.0,
            alignment: FractionalOffset.center,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
            child: Text(
              formatter.format(product.price),
              style: ComooStyles.productPrice,
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Positioned(
          right: mediaQuery * 10.0,
          bottom: mediaQuery * 10.0,
          child: Icon(
            Icons.crop_free,
            color: ComooColors.chumbo,
          ),
        ),
      ],
    );
  }

  Widget _buildPage(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    // final double pageSize = 90.0 * 5 * 30 * 28 * 250;
    // if (_scroll) {
    //   Timer(Duration(milliseconds: 400), () {
    //     final double extent = product.description.length %
    //         (MediaQuery.of(context).size.width / 50);
    //     print('$extent ${MediaQuery.of(context).size.width / 50}');
    //     print(
    //         'scroll: ${_pageScrollController.position.maxScrollExtent} ${product.description.length}');
    //     _pageScrollController.animateTo(
    //       0.0,
    //       duration: const Duration(seconds: 1),
    //       curve: Curves.easeOut,
    //     );
    //   });
    //   _scroll = false;
    // }

    return Container(
      height: MediaQuery.of(context).size.height,
      child: ListView(
        // shrinkWrap: true,
        // controller: _pageScrollController,
        children: <Widget>[
          GestureDetector(
            onDoubleTap: loadingLike ? null : onLike,
            child: _buildPhotoRow(),
          ),
          product.photos.length <= 1
              ? Container(
                  height: mediaQuery * 10.0,
                )
              : Container(
                  height: mediaQuery * 30.0,
                  color: Colors.white,
                  padding: EdgeInsets.all(mediaQuery * 10.0),
                  child: Center(
                    child: DotsIndicator(
                      controller: _photoController,
                      itemCount: product.photos.length,
                      onPageSelected: (int page) {
                        _photoController.animateToPage(
                          page,
                          duration: _kDuration,
                          curve: _kCurve,
                        );
                      },
                    ),
                  ),
                ),
          _buildSellerRow(),
          Divider(height: mediaQuery * 1.0),
          _buildProductDescription(),
          Container(
            height: 28.0,
            child: _buildTags(),
          ),
          Divider(height: mediaQuery * 3.0),
          Container(
              constraints: BoxConstraints(
                maxHeight: responsive(context, 150.0),
              ),
              child: _buildComoos()),
          Divider(height: mediaQuery * 3.0),
          Container(
              padding: EdgeInsets.only(
                right: mediaQuery * 15.0,
                left: mediaQuery * 15.0,
              ),
              child: _buildSocialInteractionRow()),
          Divider(height: mediaQuery * 1.0),
          Container(
              constraints: BoxConstraints(
                maxHeight: responsive(context, 90.0 * 4),
              ),
              child: _buildProductCommentView()),
          Divider(height: mediaQuery * 1.0),
          Container(
            decoration: BoxDecoration(color: Theme.of(context).cardColor),
            child: _buildTextComposer(),
          ),
        ],
      ),
    );
  }

  Widget _buildProductCommentView() {
    // final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      height: responsive(context, 90.0 * product.commentsQty),
      child: ListView.builder(
        // reverse: true,
        itemBuilder: (_, index) {
          Comment comment = _comments[index];
          return ListTile(
            key: GlobalObjectKey(index),
            leading: CircleAvatar(
              backgroundImage: NetworkImage(comment.user.photoURL),
            ),
            title: RichText(
                text: TextSpan(
                    text: comment.user.name + ' ',
                    style: ComooStyles.texto_bold,
                    children: [
                  TextSpan(style: ComooStyles.texto, text: comment.text)
                ])),
            subtitle: Text(timeago.format(comment.date, locale: 'pt_BR')),
          );
        },
        itemCount: _comments.length,
      ),
    );
  }

  Widget _buildTextComposer() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return IconTheme(
        data: IconThemeData(color: ComooColors.roxo),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: mediaQuery * 8.0),
          child: Row(
            children: <Widget>[
              Flexible(
                child: TextField(
                  controller: _textController,
                  onChanged: (String text) {
                    setState(() {
                      _isComposing = text.length > 0;
                    });
                  },
                  onSubmitted: _handleSubmitted,
                  decoration: InputDecoration.collapsed(
                      hintText: 'Escreva um comentário'),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: mediaQuery * 4.0),
                child: IconButton(
                    icon: Icon(COMOO.icons.chatSend.iconData),
                    onPressed: _isComposing
                        ? () => _handleSubmitted(_textController.text)
                        : null),
              ),
            ],
          ),
        ));
  }

  void _handleSubmitted(String text) async {
    _textController.clear();
    setState(() {
      _isComposing = false;
    });
    final Comment comment = Comment(text, currentUser, DateTime.now());
    await _postNewComment(comment);
    setState(() {
      _comments.add(comment);
    });
  }

  Future _postNewComment(Comment comment) {
    final DocumentReference productRef =
        db.collection('products').document(product.id);
    final DocumentReference newComment =
        productRef.collection('comments').document();
    final WriteBatch batch = db.batch();

    batch.setData(newComment, <String, dynamic>{
      'text': comment.text,
      'from': db.collection('users').document(comment.user.uid),
      'date': comment.date
    });

    product.commentsQty++;
    batch.updateData(
        productRef, <String, int>{'commentsQty': product.commentsQty});

    return batch.commit();
  }

  Widget _buildComoos() {
    if (_groups.isNotEmpty)
      return Container(
          // height: 135.0,
          child: GroupRecommendation(
              'Comoonidades em que foi anunciado:', _groups));
    else
      return Container();
  }

  Widget _buildProductDescription() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      padding: EdgeInsets.all(mediaQuery * 15.0),
      alignment: Alignment.centerLeft,
      child: CustomToolTip(
        text: product.description,
        child: Text(
          product.description,
          style: ComooStyles.produtoDesc,
          textAlign: TextAlign.left,
        ),
      ),
    );
  }

  Widget _buildSocialInteractionRow() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return IconTheme(
        data: ComooIcons.roxo,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text('Comentários', style: ComooStyles.produto_social),
            Expanded(
              child: Container(),
            ),
            // Likes Icon
            IconButton(
              icon: Icon(
                liked ? Icons.favorite : ComooIcons.curtir,
                size: mediaQuery * 28.0,
              ),
              alignment: Alignment.centerRight,
              onPressed: loadingLike ? null : onLike,
            ),
            Text(product.likes.toString(), style: ComooStyles.texto_roxo),
            // Commants Icon
            IconButton(
              icon: Icon(
                ComooIcons.comentar,
                size: mediaQuery * 28.0,
              ),
              alignment: Alignment.centerRight,
              onPressed: () {
                // _showComments();
              },
            ),
            Text(product.commentsQty.toString(),
                style: ComooStyles.produto_social),
          ],
        ));
  }

  requestProduct() async {
    setState(() {
      // _loadingMessage = 'Requisitando produto...';
      // _loading = true;
    });
    // final dynamic result = await comoo.api.requestProduct(widget.product.id);
    final dynamic result = await CloudFunctions.instance
        .getHttpsCallable(functionName: 'requestProduct')
        .call(<String, dynamic>{'productId': product.id})
        .timeout(Duration(seconds: 30))
        .then((HttpsCallableResult result) => result.data)
        .catchError((onError) => <String, dynamic>{});

    Navigator.of(context).pop();
    if (result['isFirst'] == true) {
      var _negotiationData = result['negotiation'];
      await db
          .collection('negotiations')
          .document(_negotiationData['id'])
          .get()
          .then((snapshot) async {
        setState(() {
          // _loading = false;
          userHasRequested = true;
        });
        var negotiation = Negotiation.fromSnapshot(snapshot);
        await Navigator.of(context).push(MaterialPageRoute<void>(
            builder: (context) => DirectChatView(negotiation)
            // builder: (context) => NegotiationPage(negotiation, goToChat: true),
            ));
      });
      fetchUserRequest();
    } else {
      setState(() {
        // _loading = false;
        userHasRequested = true;
      });
      final int count = result['count'];
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            // contentPadding: EdgeInsets.all(0.0),
            content: Container(
              child: Container(
                // padding: const EdgeInsets.all(21.0),
                child: Text(
                  'Você é o $countº da fila.\r\n A gente te avisa se a fila andar.',
                  style: ComooStyles.welcomeMessage,
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

  Widget _buildTags() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              ComooIcons.marca,
              size: mediaQuery * 28.0,
            ),
            SizedBox(
              width: mediaQuery * 10.0,
            ),
            Text(
              product.brand,
              overflow: TextOverflow.ellipsis,
              style: ComooStyles.product_tags,
            ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              ComooIcons.tamanho,
              size: mediaQuery * 28.0,
            ),
            SizedBox(
              width: mediaQuery * 10.0,
            ),
            Text(
              product.size,
              overflow: TextOverflow.ellipsis,
              style: ComooStyles.product_tags,
            ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              ComooIcons.estado_produto,
              size: mediaQuery * 28.0,
            ),
            SizedBox(
              width: mediaQuery * 10.0,
            ),
            Text(product.condition, style: ComooStyles.product_tags),
          ],
        ),
      ],
    );
  }

  Future<void> onLike() async {
    if (mounted) {
      setState(() {
        loadingLike = true;
      });
    }
    if (!liked) {
      currentUser.addToLiked(product.id);
      await product.addLike();
    } else {
      currentUser.removeToLiked(product.id);
      await product.removeLike();
    }
    if (mounted) {
      setState(() {
        liked = !liked;
        loadingLike = false;
      });
    }
  }

  Future<void> fetchUserRequest() async {
    bool _hasRequested = false;

    if (currentUser.uid == product.user['uid']) {
      _hasRequested = true;
    } else {
      _hasRequested = await comoo.api
          .fetchUserHasRequestedProduct(currentUser.uid, product.id);
    }
    if (mounted) {
      setState(() {
        userHasRequested = _hasRequested;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    this.product = this.widget.product;
    liked = (currentUser.likes ?? <String>[]).indexOf(product.id) == -1
        ? false
        : true;
    _isLoading = true;
    // _scroll = widget.scroll;
    fetchUserRequest();
    _user = User(
        product.user['uid'], product.user['name'], product.user['picture']);
    _loadGroups();
    _loadProductComments();
    // _pageScrollController.addListener(() {
    //   print(
    //       '${_pageScrollController.offset} ${_pageScrollController.position.viewportDimension} ${_pageScrollController.position.maxScrollExtent}');
    // });
  }

  Future<void> _refresh() async {
    final DocumentSnapshot snap =
        await db.collection('products').document(widget.product.id).get();
    product = Product.fromSnapshot(snap);
    _loadGroups();
    _loadProductComments();
  }

  void _loadGroups() {
    // List<Group> groups =  List();
    if (product.groups.isNotEmpty) {
      product.groups.forEach((g) async {
        final DocumentSnapshot snap =
            await db.collection('groups').document(g['id']).get();
        if (snap != null) {
          final Group group = Group.fromSnapshot(snap);
          if (_groups.where((Group g) => g.id == group.id).isEmpty) {
            if (mounted) {
              setState(() {
                _groups.add(group);
              });
            }
          }
        }
      });
    }

    setState(() {
      _isLoading = false;
    });
    // print('IsLoading? ' + _isLoading.toString());
  }

  Future<void> _loadProductComments() async {
    final QuerySnapshot query = await db
        .collection('products')
        .document(product.id)
        .collection('comments')
        .orderBy('date')
        .getDocuments();

    final List<Future> fetchCommentInfo = List<Future>();

    query.documents.forEach((DocumentSnapshot snap) {
      final DocumentReference userRef = snap['from'];
      fetchCommentInfo.add(userRef.get().then((userSnap) {
        final User user = User.fromSnapshot(userSnap);
        DateTime date;
        if (snap['date'] is DateTime) {
          date = snap['date'];
        } else {
          date = DateTime.fromMicrosecondsSinceEpoch(
              (snap['date'] as Timestamp).microsecondsSinceEpoch);
        }
        return Comment(
          snap['text'],
          user,
          date,
        );
      }));
    });

    Future.wait(fetchCommentInfo).then((List comments) {
      if (comments.length > 0) {
        setState(() {
          _comments.addAll(comments.cast<Comment>());
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final TextStyle menuItemStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: mediaQuery * 15.0,
      letterSpacing: mediaQuery * 0.21,
    );
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: COMOO.colors.lead,
        title: Text(product.title),
        actions: <Widget>[
          _user.uid == currentUser.uid
              ? widget.product.underNegotiation == false
                  ? PopupMenuButton<ProductMenu>(
                      onSelected: (ProductMenu result) {
                        switch (result) {
                          case ProductMenu.update:
                            _updateProduct();
                            break;
                          case ProductMenu.republish:
                            _republishProduct();
                            break;
                          case ProductMenu.delete:
                            _deleteProduct();
                            break;
                        }
                      },
                      offset: Offset(0.0, mediaQuery * 62.0),
                      itemBuilder: (BuildContext context) =>
                          <PopupMenuEntry<ProductMenu>>[
                        PopupMenuItem<ProductMenu>(
                          value: ProductMenu.update,
                          child: Text(
                            'EDITAR ANÚNCIO',
                            style: menuItemStyle,
                          ),
                        ),
                        CustomPopupMenuDivider(
                          height: mediaQuery * 3.0,
                          color: COMOO.colors.lead,
                        ),
                        PopupMenuItem<ProductMenu>(
                          value: ProductMenu.republish,
                          child: Text(
                            'REPOSTAR ANÚNCIO',
                            style: menuItemStyle,
                          ),
                        ),
                        CustomPopupMenuDivider(
                          height: mediaQuery * 3.0,
                          color: COMOO.colors.lead,
                        ),
                        PopupMenuItem<ProductMenu>(
                          value: ProductMenu.delete,
                          child: Text(
                            'EXCLUIR ANÚNCIO',
                            style: menuItemStyle,
                          ),
                        ),
                      ],
                    )
                  : Container()
              : Container(),
        ],
      ),
      body: _isLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : _buildPage(context),
    );
  }

  void _updateProduct() async {
    final bool refresh = await Navigator.of(context).push(
      MaterialPageRoute<bool>(
        builder: (context) => ProductCreateView(
          product: widget.product,
          openCamera: false,
        ),
      ),
    );
    if (refresh ?? false) {
      _refresh();
    }
  }

  Future<void> _republishProduct() async {
    final bool refresh =
        await Navigator.of(context).push(MaterialPageRoute<bool>(
            builder: (context) => ProductRepublishView(
                  product: widget.product,
                )));
    if (refresh ?? false) {
      _refresh();
    }
  }

  Future<void> _deleteProduct() async {
    final bool willDelete = await showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Deletear produto?'),
          content: Text(
              'Isso irá remover o produto de todos os grupos que foi publicado.'),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: Text('DELETAR'),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );

    if (willDelete ?? false) {
      setState(() {
        // _loading = true;
        // _loadingMessage = 'Apagando Publicação';
      });

      widget.product.delete().then((_) {
        if (mounted) {
          setState(() {
            // _loading = false;
          });
        }
        Navigator.of(context).pop();
      });
    }
  }
}

class ProductImagePage extends StatelessWidget {
  ProductImagePage({this.photo}) : image = Image.network(photo);
  final String photo;

  final Image image;

  @override
  Widget build(BuildContext context) {
    final Completer<ui.Image> completer = Completer<ui.Image>();
    ImageStreamListener listener =
        ImageStreamListener((ImageInfo info, bool result) {
      completer.complete(info.image);
    });
    image.image.resolve(ImageConfiguration()).addListener(listener);
    // (ImageInfo info, bool result) => completer.complete(info.image));
    return Scaffold(
      backgroundColor: Colors.black87,
      body: SafeArea(
        child: Center(
          child: Stack(
            children: [
              Container(
                height: double.infinity,
                width: double.infinity,
                color: Colors.black87,
                child: FutureBuilder<ui.Image>(
                  future: completer.future,
                  builder:
                      (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
                    if (snapshot.hasData) {
                      return GestureDetector(
                        child: Hero(
                          tag: 'product_image',
                          child: Container(
                            constraints: BoxConstraints(
                              maxHeight: snapshot.data.height * 1.0,
                              maxWidth: snapshot.data.width * 1.0,
                            ),
                            width: MediaQuery.of(context).size.width,
                            alignment: Alignment.center,
                            child: PinchZoomImage(
                              zoomedBackgroundColor:
                                  Colors.white.withOpacity(0.2),
                              hideStatusBarWhileZooming: true,
                              image: CachedNetworkImage(
                                cacheManager: CustomCacheManager(),
                                imageUrl: photo,
                                fit: BoxFit.fitWidth,
                                placeholder: (BuildContext context,
                                        String text) =>
                                    Center(child: CircularProgressIndicator()),
                              ),
                            ),
                          ),
                        ),
                        onTap: () => Navigator.of(context).pop(),
                      );
                    } else {
                      return LoadingContainer();
                    }
                  },
                ),
              ),
              Positioned(
                top: 0.0,
                left: 0.0,
                child: IconButton(
                  icon: Icon(
                    Icons.clear,
                    color: Colors.white,
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
