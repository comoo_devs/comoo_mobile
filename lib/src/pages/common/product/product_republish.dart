import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/dashed_circle.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:rxdart/rxdart.dart';

class ProductRepublishView extends StatefulWidget {
  ProductRepublishView({this.product});

  final Product product;

  @override
  _ProductRepublishState createState() => _ProductRepublishState();
}

class _ProductRepublishState extends State<ProductRepublishView>
    with Reusables {
  final CloudFunctions api = CloudFunctions.instance;
  final BehaviorSubject<List<dynamic>> _groups =
      BehaviorSubject<List<dynamic>>();
  Function(dynamic) get changeGroup => _groups.sink.add;
  Stream<List<dynamic>> get groupsStream => _groups.stream;
  final BehaviorSubject<bool> _isLoading = BehaviorSubject<bool>();
  Function(bool) get changeLoading => _isLoading.sink.add;
  Stream<bool> get isLoadingStream => _isLoading.stream;

  Widget _buildEmptyList() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      child: Container(
        padding: EdgeInsets.only(top: mediaQuery * 42.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                left: mediaQuery * 25.0,
                right: mediaQuery * 45.0,
              ),
              child: Text(
                'Falha ao carregar lista de grupos.\n'
                'Recarregue a página ou clique no botão abaixo para tentar novamente.',
                style: TextStyle(
                  color: ComooColors.roxo,
                  fontFamily: COMOO.fonts.montSerrat,
                  fontWeight: FontWeight.normal,
                  fontSize: mediaQuery * 20.0,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: mediaQuery * 34.0),
              child: COMOO.buttons.flatButton(
                context: context,
                onPressed: () {
                  _fetchGroupList();
                },
                height: 35.0,
                width: 280.0,
                text: 'RECARREGAR',
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSnapshotList(AsyncSnapshot<List<dynamic>> snapshot) {
    // print(snapshot.data.documents.length);
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final List<dynamic> documents = snapshot.data;
    // print('isEmpty? ${documents.isEmpty}');
    if (documents.isEmpty) {
      return _buildEmptyList();
    }
    // changeState(true);
    final int lenght = documents.length + 2;
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        if (index == 0) {
          return Container();
        }
        return Divider(
          color: COMOO.colors.lead,
        );
      },
      itemCount: lenght,
      itemBuilder: (BuildContext context, int i) {
        if (i == 0) {
          return Container(
            padding: EdgeInsets.only(
              top: mediaQuery * 25.0,
              left: mediaQuery * 21.0,
              bottom: mediaQuery * 17.0,
            ),
            child: Text(
              'Selecione as comoonidades em que você quer repostar o seu produto:',
              style: TextStyle(
                fontSize: mediaQuery * 15.0,
                fontWeight: FontWeight.w600,
                letterSpacing: mediaQuery * -0.144,
                color: COMOO.colors.purple,
                fontFamily: COMOO.fonts.montSerrat,
              ),
            ),
          );
        } else if (i == lenght - 1) {
          return Container(
            padding: EdgeInsets.symmetric(
              horizontal: mediaQuery * 62.0,
              vertical: mediaQuery * 29.0,
            ),
            child: Container(
              width: mediaQuery * 252.0,
              height: mediaQuery * 35.0,
              margin: EdgeInsets.symmetric(vertical: mediaQuery * 10.0),
              child: FlatButton(
                onPressed: _handleSelected,
                color: COMOO.colors.turquoise,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
                child: Text(
                  'REPOSTAR ANÚNCIO',
                  style: ComooStyles.botao_branco,
                ),
              ),
            ),
          );
        }
        final dynamic doc = documents[i - 1];
        final Widget thumb = Padding(
          padding: EdgeInsets.all(mediaQuery * 4.0),
          child: CircleAvatar(
            backgroundImage: AdvancedNetworkImage(doc['thumb']),
          ),
        );
        return ListTile(
          leading: doc['admin']
              ? DashedCircle(
                  color: COMOO.colors.purple,
                  dashes: 25,
                  child: thumb,
                )
              : thumb,
          title: Text(
            '${doc['name']}',
            style: TextStyle(
              fontFamily: COMOO.fonts.montSerrat,
              color: COMOO.colors.turquoise,
              fontWeight: FontWeight.w600,
              fontSize: mediaQuery * 14.0,
              letterSpacing: mediaQuery * 0.257,
            ),
          ),
          subtitle: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '${doc['location']} - ${doc['city']}',
                style: TextStyle(
                  fontFamily: COMOO.fonts.montSerrat,
                  color: COMOO.colors.medianGray,
                  fontSize: mediaQuery * 12.0,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                '${doc['description']}',
                style: TextStyle(
                  fontFamily: COMOO.fonts.montSerrat,
                  color: COMOO.colors.lead,
                  fontSize: mediaQuery * 12.0,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
          trailing: IconButton(
            iconSize: mediaQuery * 28.0,
            icon: Container(
              // padding: EdgeInsets.only(right: mediaQuery * 25.0),
              child: Image.asset(
                doc['posted']
                    ? 'images/checked_turquoise.png'
                    : 'images/add_white.png',
                alignment: Alignment.centerLeft,
                height: mediaQuery * 28.0,
                width: mediaQuery * 28.0,
              ),
            ),
            onPressed: () {
              setState(() {
                doc['posted'] = !doc['posted'];
              });
            },
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      appBar: CustomAppBar.round(
        color: COMOO.colors.turquoise,
        title: Text(
          widget.product.title,
          style: TextStyle(
            fontFamily: COMOO.fonts.montSerrat,
            fontSize: mediaQuery * 15.0,
            fontWeight: FontWeight.w500,
            letterSpacing: mediaQuery * -0.125,
          ),
        ),
        actions: <Widget>[
          StreamBuilder<bool>(
            initialData: false,
            stream: isLoadingStream,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              return FlatButton(
                child: Text(
                  'REPOSTAR',
                  style: TextStyle(
                    fontFamily: COMOO.fonts.montSerrat,
                    fontSize: mediaQuery * 13.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: mediaQuery * -0.108,
                    color: Colors.white,
                  ),
                ),
                onPressed: snapshot.data ? null : _handleSelected,
              );
            },
          ),
        ],
      ),
      body: StreamBuilder<bool>(
        initialData: false,
        stream: isLoadingStream,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          return snapshot.data
              ? LoadingContainer()
              : StreamBuilder<List<dynamic>>(
                  stream: groupsStream,
                  builder: (BuildContext context,
                      AsyncSnapshot<List<dynamic>> snapshot) {
                    // print('FriendsPicker: ${snapshot.connectionState} - has data? ${snapshot.hasData}');
                    switch (snapshot.connectionState) {
                      case ConnectionState.active:
                        return snapshot.hasData
                            ? _buildSnapshotList(snapshot)
                            : _buildEmptyList();
                        break;
                      case ConnectionState.done:
                        return snapshot.hasData
                            ? _buildSnapshotList(snapshot)
                            : _buildEmptyList();
                        break;
                      case ConnectionState.none:
                        return _buildEmptyList();
                        break;
                      case ConnectionState.waiting:
                        return snapshot.hasData
                            ? _buildSnapshotList(snapshot)
                            : LoadingContainer();
                        break;
                      default:
                        return Container();
                        break;
                    }
                  },
                );
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _fetchGroupList();
  }

  @override
  void dispose() {
    super.dispose();
    _groups.close();
    _isLoading.close();
  }

  Future<void> _handleSelected() async {
    print('heeey');
    changeLoading(true);
    final List<String> selected = _groups.value
        .where((dynamic group) => group['posted'])
        .map((dynamic group) => group['id'])
        .toList()
        .cast<String>();
    // print(selected);
    final bool isAdded = await _addSelected(selected);
    if (isAdded ?? false) {
      await showMessageDialog(
          context, 'Seu anúncio foi repostado com sucesso!');
      Navigator.of(context).pop(true);
    } else {
      showMessageDialog(context, 'Falha ao enviar os dados, tente novamente!');
      changeLoading(false);
    }
  }

  Future<bool> _addSelected(List<String> selected) async {
    if (await isConnected()) {
      return await api
          .getHttpsCallable(functionName: 'repost')
          .call(<String, dynamic>{
            'productId': widget.product.id,
            'groups': selected,
          })
          .timeout(Duration(seconds: 60))
          .then((dynamic result) {
            print(result);
            return true;
          })
          .catchError((onError) {
            print(onError);
            return false;
          });
    } else {
      return false;
    }
  }

  Future<void> _fetchGroupList() async {
    if (await isConnected()) {
      // print('has connection');
      await api
          .getHttpsCallable(functionName: 'getGroupsToRepost')
          .call(<String, String>{
            'productId': widget.product.id,
          })
          .timeout(Duration(seconds: 60))
          .then((HttpsCallableResult result) {
            // print(result);
            if (mounted) {
              changeGroup(result.data);
            }
          })
          .catchError((onError) {
            print(onError);
            if (mounted) {
              changeGroup(<dynamic>[]);
            }
          });
    } else {
      // print('no connection');
      changeGroup(<dynamic>[]);
    }
  }
}
