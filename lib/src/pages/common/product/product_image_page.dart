// import 'dart:async';
// import 'dart:ui' as ui;

// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:comoo/src/widgets/custom_cache_manager.dart';
// import 'package:comoo/src/widgets/loading_container.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/widgets/custom_cache_manager.dart';
import 'package:comoo/src/widgets/dots_indicator.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

class ProductImageHeroNew extends StatefulWidget {
  ProductImageHeroNew({
    Key key,
    this.product,
    this.onTap,
    this.width,
    this.height,
    this.pageController,
    this.initialPage,
  });

  final Product product;
  final VoidCallback onTap;
  final double width;
  final double height;
  final PageController pageController;
  final int initialPage;

  @override
  State<StatefulWidget> createState() => _ProductImageHeroNewState();
}

class _ProductImageHeroNewState extends State<ProductImageHeroNew> {
  PageController pageController;
  @override
  void initState() {
    super.initState();

    print('INIT STATE');
    int initialPage = 0;
    if (widget.pageController != null) {
      widget.pageController.addListener(_listener);
      if (widget.pageController.hasClients) {
        initialPage = widget.pageController?.page?.toInt() ?? 0;
      }
    }

    pageController = PageController(initialPage: initialPage);
  }

  @override
  void dispose() {
    super.dispose();

    pageController?.dispose();
    widget.pageController.removeListener(_listener);
  }

  Future<void> _listener() async {
    //
    final int page = widget.pageController.page.toInt();
    print('\n\n>>> PAGE: $page\n\n');
    if (pageController.hasClients) {
      pageController?.jumpToPage(page);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      height: widget.height,
      child: Hero(
        tag: widget.product.id,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: widget.onTap,
            child: PageView.builder(
              key: Key('feed#${widget.product.id}/pageview'),
              controller: pageController,
              itemCount: widget.product.photos.length,
              scrollDirection: Axis.horizontal,
              onPageChanged: (int page) {
                if (widget.pageController != null) {
                  widget.pageController.jumpToPage(page);
                }
              },
              itemBuilder: (BuildContext context, int index) {
                final String url = widget.product?.photos[index] ?? '';
                if (url.isEmpty) {
                  return Container();
                }
                return Container(
                  constraints:
                      BoxConstraints.tight(Size(widget.width, widget.height)),
                  child: CachedNetworkImage(
                    cacheManager: CustomCacheManager(),
                    imageUrl: url,
                    fit: BoxFit.cover,
                    placeholder: (BuildContext context, String url) => Center(
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget:
                        (BuildContext context, String url, Object error) =>
                            Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

class ProductImageHero extends StatelessWidget {
  ProductImageHero({
    Key key,
    this.photo,
    this.onTap,
    this.width,
  }) : super(key: key);

  final String photo;
  final VoidCallback onTap;
  final double width;

  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Hero(
        tag: photo,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap,
            child: CachedNetworkImage(
              cacheManager: CustomCacheManager(),
              imageUrl: photo,
              fit: BoxFit.cover,
              placeholder: (BuildContext context, String url) => Center(
                child: CircularProgressIndicator(),
              ),
              errorWidget: (BuildContext context, String url, Object error) =>
                  Center(
                child: CircularProgressIndicator(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   final Completer<ui.Image> completer = Completer<ui.Image>();
  //   ImageStreamListener listener =
  //       ImageStreamListener((ImageInfo info, bool result) {
  //     completer.complete(info.image);
  //   });
  //   image.image.resolve(ImageConfiguration()).addListener(listener);
  //   // (ImageInfo info, bool result) => completer.complete(info.image));
  //   return Scaffold(
  //     body: Center(
  //       child: Stack(
  //         children: [
  //           Container(
  //             height: double.infinity,
  //             width: double.infinity,
  //             color: Colors.black87,
  //             child: FutureBuilder<ui.Image>(
  //               future: completer.future,
  //               builder:
  //                   (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
  //                 if (snapshot.hasData) {
  //                   return GestureDetector(
  //                     child: Hero(
  //                       tag: 'product_image',
  //                       child: Container(
  //                         constraints: BoxConstraints(
  //                           maxHeight: snapshot.data.height * 1.0,
  //                           maxWidth: snapshot.data.width * 1.0,
  //                         ),
  //                         child: CachedNetworkImage(
  //                           cacheManager: CustomCacheManager(),
  //                           imageUrl: photo,
  //                           fit: BoxFit.fitWidth,
  //                           placeholder: (BuildContext context, String text) =>
  //                               Center(child: CircularProgressIndicator()),
  //                         ),
  //                       ),
  //                     ),
  //                     onTap: () => Navigator.of(context).pop(),
  //                   );
  //                 } else {
  //                   return LoadingContainer();
  //                 }
  //               },
  //             ),
  //           ),
  //           Positioned(
  //             top: 0.0,
  //             left: 0.0,
  //             child: IconButton(
  //               icon: Icon(
  //                 Icons.clear,
  //                 color: Colors.white,
  //               ),
  //               onPressed: () => Navigator.of(context).pop(),
  //             ),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }
}

class ProductImage extends StatefulWidget {
  ProductImage({this.width, this.height, this.product});
  final Product product;
  final double width;
  final double height;

  @override
  _ProductImageState createState() => _ProductImageState();
}

class _ProductImageState extends State<ProductImage> with Reusables {
  final PageController pageController = PageController();

  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        children: <Widget>[
          SizedBox(
            width: 0,
            height: 0,
            child: PageView.builder(
              itemCount: widget.product.photos.length,
              controller: pageController,
              itemBuilder: (_, index) {
                return Container(
                  width: 0,
                  height: 0,
                );
              },
            ),
          ),
          SizedBox(
            width: widget.width,
            height: widget.height,
            child: Center(
              child: ProductImageHeroNew(
                width: widget.width,
                height: widget.height,
                product: widget.product,
                pageController: pageController,
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute<void>(
                    builder: (BuildContext context) {
                      return Scaffold(
                        body: Container(
                          // The blue background emphasizes that it's a new route.
                          color: Colors.black,
                          // padding: const EdgeInsets.all(16.0),
                          alignment: Alignment.center,
                          child: ProductImageHeroNew(
                            width: widget.width,
                            height: widget.height,
                            product: widget.product,
                            pageController: pageController,
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      );
                    },
                  ));
                },
              ),
            ),
          ),
          // widget.product.photos.length <= 1
          //     ? Container(
          //         height: responsive(context, 10.0),
          //       )
          //     : Container(
          //         height: responsive(context, 30.0),
          //         color: Colors.white,
          //         padding: EdgeInsets.all(responsive(context, 10.0)),
          //         child: Center(
          //           child: DotsIndicator(
          //             controller: pageController,
          //             itemCount: widget.product.photos.length,
          //             onPageSelected: (int page) {
          //               pageController.animateToPage(
          //                 page ?? 0,
          //                 duration: _kDuration,
          //                 curve: _kCurve,
          //               );
          //             },
          //           ),
          //         ),
          //       ),
        ],
      ),
    );
  }
}
