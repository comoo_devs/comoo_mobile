import 'package:comoo/src/blocs/group_chat_bloc.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:flutter/material.dart';

import 'group_chat_view.dart';

class GroupChatPage extends StatefulWidget {
  GroupChatPage(this.group);
  final Group group;
  @override
  State<StatefulWidget> createState() => _GroupChatState();
}

class _GroupChatState extends State<GroupChatPage> {
  GroupChatBloc bloc;

  @override
  void initState() {
    super.initState();

    bloc = GroupChatBloc(widget.group);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GroupChatBloc>(
      bloc: bloc,
      child: GroupChatView(),
    );
  }
}
