import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/group.dart';
// import 'package:comoo/comoo/message.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
// import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/user/friends_picker.dart';
import 'package:comoo/src/pages/common/user/user_friend.dart';
import 'package:comoo/src/pages/home/bottom_menu/group_creation/add_group.dart';
import 'package:comoo/api/network.dart';
// import 'package:comoo/comoo/user.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/bottom_text_input.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
// import 'package:comoo/widgets/chat_message.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/widgets/feed/product_feed.dart';
import 'package:rxdart/rxdart.dart';
import 'package:share/share.dart';
import '../group_detail.dart';
import 'package:comoo/src/comoo/post.dart';
import 'package:comoo/src/widgets/feed/post_feed.dart';
import 'package:comoo/src/pages/home/bottom_menu/product_creation/product_create.dart';
import 'package:comoo/src/comoo/comoo.dart';

class GroupChatPage extends StatefulWidget {
  GroupChatPage(this.group);

  final Group group;

  @override
  State createState() {
    return _GroupChatPageState(group);
  }
}

class _GroupChatPageState extends State<GroupChatPage> with Reusables {
  _GroupChatPageState(this._group);

  final NetworkApi api = NetworkApi();

  StreamSubscription<QuerySnapshot> streamSubscription;
  // final BehaviorSubject<List<Future<Widget>>> _feed =
  //     BehaviorSubject<List<Future<Widget>>>();
  // Function(List<Future<Widget>>) get changeFeed => _feed.sink.add;
  // Stream<List<Future<Widget>>> get feed => _feed.stream;
  final BehaviorSubject<QuerySnapshot> _query =
      BehaviorSubject<QuerySnapshot>();
  Function(QuerySnapshot) get changeQuery => _query.sink.add;
  Stream<QuerySnapshot> get query => _query.stream;

  final Group _group;
  // bool _isLoading = false;
  // final TextEditingController _textController = TextEditingController();

//  bool _isComposing = false;
  final FirebaseAuth auth = FirebaseAuth.instance;
  final Firestore db = Firestore.instance;

  var messages = List<Widget>();

//  Future _buildFeedItem(DocumentReference postRef) async {
//    var postDoc = await postRef.get();
//    if (!postDoc.exists) return null;
//    var post = Post.fromSnapshot(postDoc);
//    return Future.value(PostFeed(post: post, key: Key(post.id)));
//  }

  Future<void> _onQuerySnapshot(QuerySnapshot snapshot) async {
    setState(() {
      // _isLoading = true;
    });
    changeQuery(snapshot);
    List<Future<Widget>> cm = List<Future<Widget>>();
    snapshot.documentChanges.forEach((DocumentChange documentChange) {
      if (documentChange.type == DocumentChangeType.added) {
        Future<Widget> message;
        if (documentChange.document['type'] == 'product') {
          DocumentReference productRef =
              documentChange.document['product'] as DocumentReference;
          message = productRef.get().then<Widget>((DocumentSnapshot snap) {
            final Product product = Product.fromSnapshot(snap);
            return ProductFeed(
              product,
              key: Key(snap.documentID),
            );
          }).catchError((onError) {
            print('|>> onError => onQuerySnapshot'
                '\n > product: ${documentChange.document.documentID}'
                '\n > exists: ${documentChange.document.exists}');
            return null;
          });
        } else {
          DocumentReference postRef =
              documentChange.document['post'] as DocumentReference;
          message = postRef.get().then<Widget>((DocumentSnapshot snap) {
            return PostFeed(
                post: Post.fromSnapshot(snap), key: Key(snap.documentID));
          }).catchError((onError) {
            print('|>> onError => onQuerySnapshot'
                '\n > post: ${documentChange.document.documentID}'
                '\n > exists: ${documentChange.document.exists}');
            return null;
          });
//          cm.add(_buildFeedItem(
//              documentChange.document['post'] as DocumentReference));
        }
        cm.add(message ?? Container());
        // changeFeed(cm);
        //fetch data to avoid redraw on setState()
//        Message message = Message.fromSnapshot(documentChange.document);
//        cm.add(message.fetchUser().then((User user) {
//          return message.fetchProduct().then((Product product) async {
//            List photoList = List();
//            if (message.product != null) {
//              var photos = await message.product.collection('photos')
//                  .getDocuments();
//              photoList = photos.documents
//                  .map((DocumentSnapshot snap) => snap['downloadURL'])
//                  .toList();
//            }
//            return ChatMessage(
//              key: GlobalObjectKey(message.id),
//              message: message,
//              user: user,
//              product: product,
//              photos :photoList,
//              group: this._group,
//            );
//          });
//        }));

      }
    });
    Future.wait(cm).then((List<Widget> newMessages) {
      if (newMessages.length > 0) {
        setState(() {
          messages.insertAll(0, newMessages);
        });
      }
      // setState(() {
      //   _isLoading = false;
      // });
    });
  }

//  Future<Null> _handleSubmitted(String text) async {
//    _textController.clear();
//    setState(() {
//      _isComposing = false;
//    });
//    _sendMessage(text: text);
//  }

//  void _sendMessage({String text}) async {
//    var messageRef = db
//        .collection('groups')
//        .document(this._group.id)
//        .collection('messages')
//        .document();
//    DocumentReference userRef =
//        db.collection('users').document(currentUser.uid);
//    Message newMessage =
//        Message(text, userRef, DateTime.now(), MessageType.message);
//
//    await messageRef.setData(newMessage.toJson());
//  }

//  Widget _buildTextComposer() {
//    return IconTheme(
//        data: IconThemeData(color: ComooColors.roxo),
//        child: Container(
//          margin: const EdgeInsets.symmetric(horizontal: 8.0),
//          child: Row(
//            children: <Widget>[
//              Flexible(
//                child: TextField(
//                  controller: _textController,
//                  onChanged: (String text) {
//                    setState(() {
//                      _isComposing = text.length > 0;
//                    });
//                  },
//                  onSubmitted: _handleSubmitted,
//                  decoration: InputDecoration.collapsed(
//                      hintText: 'Enviar mensagem'),
//                ),
//              ),
//              Container(
//                margin: EdgeInsets.symmetric(horizontal: 4.0),
//                child: IconButton(
//                    icon: Icon(Icons.send),
//                    onPressed: _isComposing
//                        ? () => _handleSubmitted(_textController.text)
//                        : null),
//              ),
//            ],
//          ),
//        ));
//  }

  Future<void> createProduct(bool openCamera) async {
    currentUser = await comoo.currentUser();
    if (currentUser.sellerStatus == 'incomplete') {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              contentPadding: EdgeInsets.all(0.0),
              content: Container(
                child: Container(
                  padding: const EdgeInsets.all(21.0),
                  child: Text(
                    'Para começar a vender na COMOO você precisa acrescentar mais dados ao seu cadastro',
                    style: ComooStyles.welcomeMessage,
                  ),
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: Text(
                    'DEPOIS',
                    style: ComooStyles.botoes,
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: Text(
                    'AGORA',
                    style: ComooStyles.botoes,
                  ),
                ),
              ],
            );
          }).then((result) {
        if (result) {
          // Navigator.of(context).pushNamed('/user_profile');
          Navigator.of(context).pushNamed('/sign_up_personal');
        }
      });
    } else if (currentUser.sellerStatus == 'waiting') {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              contentPadding: EdgeInsets.all(0.0),
              content: Container(
                child: Container(
                  padding: const EdgeInsets.all(21.0),
                  child: Text(
                    'Seu cadastro ainda está sobre analise, aguarde notificação.',
                    style: ComooStyles.welcomeMessage,
                  ),
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else {
      Navigator.of(context)
          .push(MaterialPageRoute(
              builder: (BuildContext context) => ProductCreateView(
                    openCamera: openCamera,
                  )))
          .then((result) {
        if (result != null) {
          setState(() {});
        }
      });
    }
  }

  Future<void> inviteUsers() async {
    List friends = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FriendsPickerPage(group: widget.group),
      ),
    ) as List;

    if (friends == null || friends.isEmpty) {
      return false;
    }
    _addUsersToGroup(
      currentUser.uid,
      currentUser.name,
      friends,
    );
    showCustomDialog(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Seu convite foi enviado com sucesso :)',
      ),
    );
  }

  Future<void> copyLink() async {
    showCustomDialog(
      barrierDismissible: false,
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Gerando link',
        actions: [],
      ),
    );

    final String message = await createGroupDynamicLink(true, widget.group);
    print(message);
    Share.share('$message').then((result) {
      Navigator.of(context).pop();
    });
  }

  Future<void> _addUsersToGroup(
    String fromID,
    String fromName,
    List users,
  ) async {
    // print(widget.group.id);
    // WriteBatch batch = db.batch();
    users.forEach((u) {
      UserFriend user = UserFriend.fromSnapshot(u);
      CloudFunctions.instance
          .getHttpsCallable(functionName: 'generateUserNotifications')
          .call(<String, dynamic>{
        'type': 'group_invite',
        'ref': widget.group.id,
        'userId': user.uid,
        'senderId': currentUser.uid,
      }).catchError((error) {
        print('>> generateUserNotifications Error!\n$error');
        var result = {'result': 'error!'};
        return result;
      });
      // var userRequestNotificationRef = db
      //     .collection('users')
      //     .document(user.uid)
      //     .collection('notifications')
      //     .document();
      // print('${user.uid} ${userRequestNotificationRef.documentID}');

      // batch.setData(
      //   userRequestNotificationRef,
      //   <String, dynamic>{
      //     'id': userRequestNotificationRef.documentID,
      //     'thumb': widget.group.avatar,
      //     'from': fromID,
      //     'type': 'group_invite',
      //     'date': DateTime.now(),
      //     'name': fromName,
      //     'group': widget.group.id,
      //     'text':
      //         'Convidou você para entrar na comoonidade ${widget.group.name}',
      //   },
      // );
    });
    // batch.commit();
  }

  // void _addFriendsToGroup(List friends) {
  //   var groupRef = db.collection('groups').document(widget.group.id);
  //   WriteBatch batch = db.batch();
  //   friends.forEach((user) {
  //     UserFriend friend = UserFriend.fromSnapshot(user);
  //     var groupFriendRef = groupRef.collection('users').document(friend.uid);
  //     batch.setData(groupFriendRef, <String, dynamic>{
  //       'name': friend.name,
  //       'id': friend.uid,
  //       'admin': false,
  //     });
  //     var userGroupRef = db
  //         .collection('users')
  //         .document(friend.uid)
  //         .collection('groups')
  //         .document(groupRef.documentID);
  //     batch.setData(
  //       userGroupRef,
  //       <String, dynamic>{
  //         'name': widget.group.name,
  //         'description': widget.group.description,
  //         'id': groupRef.documentID,
  //         'admin': false,
  //         'avatar': widget.group.avatar
  //       },
  //       merge: true,
  //     );
  //   });
  //   batch.commit();
  // }

  @override
  void initState() {
    super.initState();

    api.openedGroupFeed(widget.group.id);

    streamSubscription = db
        .collection('groups')
        .document(this._group.id)
        .collection('feed')
        .orderBy('date', descending: true)
        .snapshots()
        .listen(_onQuerySnapshot);
  }

  Future<void> _navigateToGroup() async {
    final bool result = await Navigator.of(context).push(
        MaterialPageRoute<bool>(
            fullscreenDialog: false,
            builder: (BuildContext context) =>
                GroupDetailView(group: widget.group)));
    if (result ?? false) {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    // final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: InkWell(
          onTap: _navigateToGroup,
          child: Container(
            alignment: Alignment.center,
            child: Text(widget.group.name),
          ),
        ),
        actions: <Widget>[
          _showMenu(context),
          // IconButton(
          //     icon: Icon(ComooIcons.camera),
          //     iconSize: mediaQuery * 34.0,
          //     padding: EdgeInsets.only(right: mediaQuery * 15.0),
          //     color: Colors.white,
          //     onPressed: () {
          //       createProduct(true);
          //     })
        ],
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            StreamBuilder<QuerySnapshot>(
              stream: query,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                // print('${snapshot.hasData} ${snapshot.data}');
                switch (snapshot.connectionState) {
                  case ConnectionState.active:
                    return snapshot.hasData
                        ? buildSnapshotQueryData(snapshot.data)
                        : buildEmptyData();
                    break;
                  case ConnectionState.waiting:
                    return LoadingContainer();
                    break;
                  case ConnectionState.done:
                    return snapshot.hasData
                        ? buildSnapshotQueryData(snapshot.data)
                        : buildEmptyData();
                    break;
                  case ConnectionState.none:
                    return snapshot.hasData
                        ? buildSnapshotQueryData(snapshot.data)
                        : buildEmptyData();
                    break;
                  default:
                    return Container();
                    break;
                }
              },
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                color: COMOO.colors.lightGray,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.06,
                child: Row(
                  children: <Widget>[
                    LimitedBox(
                      maxWidth: MediaQuery.of(context).size.width * 0.85,
                      maxHeight: MediaQuery.of(context).size.height * 0.06,
                      child: BottomTextInput(),
                    ),
                    Container(
                      // margin: EdgeInsets.symmetric(horizontal: 5.0),
                      // color: Colors.amber,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: COMOO.colors.purple,
                      ),
                      child: Container(
                        margin: EdgeInsets.only(
                          right: responsiveWidth(context, 8),
                        ),
                        child: IconButton(
                          icon: Icon(
                            COMOO.icons.camera.iconData,
                            size: responsiveWidth(context, 22),
                            color: Colors.white,
                          ),
                          onPressed: () {},
                          // onPressed: _isComposing
                          //     ? () => _handleSubmitted(_textController.text)
                          //     : null,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _onSelectMenu(String value) async {
    print(value);
    switch (value) {
      case 'invite':
        inviteUsers();
        break;
      case 'link':
        showCustomDialog(
          barrierDismissible: false,
          context: context,
          alert: customRoundAlert(
            context: context,
            title: 'Gerando link',
            actions: [],
          ),
        );

        final String message = await createGroupDynamicLink(true, widget.group);
        print(message);
        Share.share('$message').then((result) {
          Navigator.of(context).pop();
        });
        break;
      case 'profile':
        Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) =>
              GroupDetailView(group: widget.group),
        ));
        break;
      case 'update':
        Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => AddGroupPage(id: widget.group.id),
        ));
        break;
      case 'leave':
        final double mediaQuery = MediaQuery.of(context).size.width / 400;
        bool confirmLeave = await showCustomDialog(
          context: context,
          alert: customRoundAlert(
            context: context,
            title:
                'Tem certeza que deseja deixar de participar da comoonidade?',
            actions: [
              Container(
                height: mediaQuery * 41.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: Text(
                    'CANCELAR',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                height: mediaQuery * 41.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: Text(
                    'SIM',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
        if (confirmLeave ?? false) {
          _removeUser(currentUser);
          Navigator.of(context).pop(true);
        }
        break;
    }
  }

  _removeUser(User user) async {
    if (mounted)
      setState(() {
        // _isLoading = true;
      });
    String id = widget.group.id;
    var batch = db.batch();
    var groupUserRef = db
        .collection('groups')
        .document(id)
        .collection('users')
        .document(user.uid);
    var userGroupRef = db
        .collection('users')
        .document(user.uid)
        .collection('groups')
        .document(id);

    batch.delete(groupUserRef);
    batch.delete(userGroupRef);
    await batch.commit();
    if (mounted)
      setState(() {
        // _isLoading = false;
      });
  }

  Widget _showMenu(BuildContext context) {
    final List<PopupMenuEntry<String>> entries = <PopupMenuEntry<String>>[];
    final bool _isAdmin = widget.group.userAdmin == currentUser.uid;
    final bool _isMember = true;
    final TextStyle menuStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      color: COMOO.colors.lead,
      fontSize: responsive(context, 12.0),
      fontWeight: FontWeight.w500,
      letterSpacing: responsive(context, 0.17),
    );
    if (_isMember) {
      entries.add(PopupMenuItem<String>(
        value: 'invite',
        child: Text(
          'ADICIONAR AMIGOS',
          style: menuStyle,
        ),
      ));
      entries.add(PopupMenuItem<String>(
        value: 'link',
        child: Text(
          'CONVIDAR VIA LINK',
          style: menuStyle,
        ),
      ));
    }
    entries.add(PopupMenuItem<String>(
      value: 'profile',
      child: Text(
        'PERFIL COMOONIDADE',
        style: menuStyle,
      ),
    ));
    if (_isAdmin) {
      // entries.add(PopupMenuItem<String>(
      //   value: 'update',
      //   child: Text(
      //     'Editar Comoonidade',
      //     style: menuStyle,
      //   ),
      // ));
    } else {
      entries.add(PopupMenuItem<String>(
        value: 'leave',
        child: Text(
          'Sair da comoonidade',
          style: menuStyle,
        ),
      ));
    }
    return PopupMenuButton<String>(
      onSelected: _onSelectMenu,
      offset: Offset(0.0, responsive(context, 50.0)),
      itemBuilder: (BuildContext context) => entries,
    );
  }

  Widget buildSnapshotQueryData(QuerySnapshot snapshot) {
    if (snapshot.documents.isEmpty) {
      return buildEmptyData();
    }
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    // return StreamBuilder<List<Future<Widget>>>(
    //   stream: feed,
    //   builder:
    //       (BuildContext context, AsyncSnapshot<List<Future<Widget>>> snapshot) {
    //     return snapshot.hasData
    //         ? buildFeedData(snapshot.data)
    //         : LoadingContainer(
    //             message: 'Carregando feed...',
    //           );
    //   },
    // );
    return ListView.builder(
      padding: EdgeInsets.only(top: mediaQuery * 10.0),
      itemCount: messages.length,
      itemBuilder: (context, index) {
        // print('building item ${index + 1} of ${messages.length}');
        if (messages.elementAt(index) == null) {
          return Container();
        }
        return messages[index];
      },
    );
    // return ListView.builder(
    //   padding: EdgeInsets.only(top: mediaQuery * 10.0),
    //   itemCount: snapshot.documentChanges.length,
    //   // itemExtent: mediaQuery * 500.0,
    //   itemBuilder: (BuildContext context, int index) {
    //     // return Container();
    //     final DocumentChange documentChange = snapshot.documentChanges[index];
    //     if (documentChange.type != DocumentChangeType.added) {
    //       return Container();
    //     }
    //     DocumentReference docRef;
    //     final String type = documentChange.document['type'];
    //     switch (type) {
    //       case 'product':
    //         docRef = documentChange.document['product'] as DocumentReference;
    //         return FutureBuilder<DocumentSnapshot>(
    //           key: Key(docRef.documentID),
    //           future: docRef.get(),
    //           builder: (BuildContext context,
    //               AsyncSnapshot<DocumentSnapshot> snapshot) {
    //             print('${snapshot.hasData} ${snapshot.data}');
    //             return snapshot.hasData
    //                 ? ProductFeed(
    //                     Product.fromSnapshot(snapshot.data),
    //                     key: Key(snapshot.data.documentID),
    //                   )
    //                 : Container(
    //                     height: mediaQuery * 300.0,
    //                     child: Center(
    //                       child: CircularProgressIndicator(),
    //                     ),
    //                   );
    //           },
    //         );
    //         break;

    //       case 'post':
    //         docRef = documentChange.document['post'] as DocumentReference;
    //         return FutureBuilder<DocumentSnapshot>(
    //           key: Key(docRef.documentID),
    //           future: docRef.get(),
    //           builder: (BuildContext context,
    //               AsyncSnapshot<DocumentSnapshot> snapshot) {
    //             print('${snapshot.hasData} ${snapshot.data}');
    //             return snapshot.hasData
    //                 ? PostFeed(
    //                     post: Post.fromSnapshot(snapshot.data),
    //                     key: Key(snapshot.data.documentID),
    //                   )
    //                 : Container(
    //                     height: mediaQuery * 300.0,
    //                     child: Center(
    //                       child: CircularProgressIndicator(),
    //                     ),
    //                   );
    //           },
    //         );
    //         break;

    //       default:
    //         return Container();
    //     }
    //   },
    // );
  }

  Widget buildProductFeed(DocumentSnapshot snap) {
    final Product product = Product.fromSnapshot(snap);
    return ProductFeed(
      product,
      key: Key(snap.documentID),
    );
  }

  Widget buildPostFeed(DocumentSnapshot snap) {
    return PostFeed(post: Post.fromSnapshot(snap), key: Key(snap.documentID));
  }

  Widget buildFeedData(List<Future<Widget>> feeds) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return ListView.builder(
      padding: EdgeInsets.only(top: mediaQuery * 10.0),
      controller: TrackingScrollController(),
      physics: ClampingScrollPhysics(),
      itemCount: feeds.length,
      itemBuilder: (context, index) {
        if (feeds.elementAt(index) == null) {
          return Container();
        }
        return FutureBuilder<Widget>(
          future: feeds[index],
          builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
            return snapshot.hasData
                ? mounted ? snapshot.data : Container()
                : Container(
                    height: mediaQuery * 200.0,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
          },
        );
      },
    );
  }

  Widget buildEmptyData() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      decoration: backgroundDeco(),
      padding: EdgeInsets.only(
        top: mediaQuery * 40.0,
        left: mediaQuery * 15.0,
        right: mediaQuery * 15.0,
      ),
      child: Column(
        children: <Widget>[
          Text(
            'Oba,\ncomoonidade nova!',
            style: ComooStyles.grupoNovoTitle,
          ),
          Padding(
            padding: EdgeInsets.only(
              // top: mediaQuery * 1.0,
              bottom: mediaQuery * 15.0,
              left: mediaQuery * 15.0,
            ),
            child: Text(
              'Seja o primeiro a anunciar e convide outros usuários para participar da sua comoonidade.',
              style: ComooStyles.grupoNovo.copyWith(
                height: mediaQuery * 1.7,
              ),
            ),
          ),
          COMOO.buttons.flatButton(
            context: context,
            onPressed: () {
              createProduct(false);
            },
            height: 35.0,
            width: 280.0,
            text: 'CRIAR UM ANÚNCIO',
          ),
          widget.group.userAdmin == currentUser.uid
              ? COMOO.buttons.flatButton(
                  context: context,
                  onPressed: () {
                    inviteUsers();
                  },
                  height: 35.0,
                  width: 280.0,
                  text: 'CONVIDAR NA COMOO',
                )
              : Container(),
          COMOO.buttons.flatButton(
            context: context,
            onPressed: () {
              copyLink();
            },
            height: 35.0,
            width: 280.0,
            text: 'ENVIAR LINK DE CONVITE',
          ),
        ],
      ),
    );
  }

  Decoration backgroundDeco() {
    return BoxDecoration(
      image: DecorationImage(
        image: AssetImage('images/skateBackground.jpeg'),
        fit: BoxFit.cover,
      ),
    );
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    super.dispose();
    // _feed.close();
    _query.close();
  }
}
