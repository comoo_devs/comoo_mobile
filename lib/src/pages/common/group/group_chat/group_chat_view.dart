import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/group_chat_bloc.dart';
import 'package:comoo/src/pages/common/user/friends_picker.dart';
import 'package:comoo/src/pages/home/bottom_menu/group_creation/add_group.dart';
import 'package:comoo/src/pages/home/bottom_menu/product_creation/product_create.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:comoo/src/widgets/text_composer.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

import '../group_detail.dart';

class GroupChatView extends StatelessWidget with Reusables {
  GroupChatView();
  @override
  Widget build(BuildContext context) {
    final GroupChatBloc bloc = BlocProvider.of<GroupChatBloc>(context);
    return Scaffold(
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: InkWell(
          onTap: () => _navigateToGroup(context),
          child: Container(
            alignment: Alignment.center,
            child: Text(bloc.group.name),
          ),
        ),
        actions: <Widget>[
          _showMenu(context),
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: bloc.query.stream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          Widget child = Container();
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              child = snapshot.hasData
                  ? buildSnapshotQueryData(context, snapshot.data)
                  : buildEmptyData(context);
              break;
            case ConnectionState.waiting:
              return LoadingContainer();
              break;
            case ConnectionState.done:
              child = snapshot.hasData
                  ? buildSnapshotQueryData(context, snapshot.data)
                  : buildEmptyData(context);
              break;
            case ConnectionState.none:
              child = snapshot.hasData
                  ? buildSnapshotQueryData(context, snapshot.data)
                  : buildEmptyData(context);
              break;
          }
          return StreamBuilder<String>(
            stream: bloc.message.stream,
            builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
              final bool isComposing =
                  snapshot.hasData && snapshot.data.isNotEmpty;
              return TextComposer(
                child: child,
                hintText: 'Escreva um comentário',
                onChanged: bloc.message.add,
                controller: bloc.messageController,
                onPressed: isComposing
                    ? () {
                        hideKeyboard(context);
                        bloc.submitTextComposing();
                      }
                    : null,
                onSubmitted: isComposing
                    ? (String text) {
                        hideKeyboard(context);
                        bloc.submitTextComposing(text: text);
                      }
                    : null,
                suffixIcon: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: COMOO.colors.lead,
                  ),
                  child: IconButton(
                    icon: Padding(
                      padding: EdgeInsets.only(
                        right: responsiveWidth(context, 8),
                      ),
                      child: Icon(
                        COMOO.icons.camera.iconData,
                        size: responsiveWidth(context, 22),
                      ),
                    ),
                    padding: const EdgeInsets.all(0),
                    onPressed: () => createProduct(context, true),
                    color: Colors.white,
                    iconSize: responsiveWidth(context, 28),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }

  Future<void> _navigateToGroup(BuildContext context) async {
    final GroupChatBloc bloc = BlocProvider.of<GroupChatBloc>(context);
    final bool result =
        await Navigator.of(context).push(MaterialPageRoute<bool>(
      fullscreenDialog: false,
      builder: (BuildContext context) => GroupDetailView(group: bloc.group),
    ));
    if (result ?? false) {
      Navigator.of(context).pop();
    }
  }

  Widget _showMenu(BuildContext context) {
    final GroupChatBloc bloc = BlocProvider.of<GroupChatBloc>(context);
    final List<PopupMenuEntry<String>> entries = <PopupMenuEntry<String>>[];
    final bool _isAdmin = bloc.group.userAdmin == bloc.currentUser.uid;
    final bool _isMember = true;
    final TextStyle menuStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      color: COMOO.colors.lead,
      fontSize: responsive(context, 12.0),
      fontWeight: FontWeight.w500,
      letterSpacing: responsive(context, 0.17),
    );
    if (_isMember) {
      entries.add(PopupMenuItem<String>(
        value: 'invite',
        child: Text(
          'ADICIONAR AMIGOS',
          style: menuStyle,
        ),
      ));
      entries.add(PopupMenuItem<String>(
        value: 'link',
        child: Text(
          'CONVIDAR VIA LINK',
          style: menuStyle,
        ),
      ));
    }
    entries.add(PopupMenuItem<String>(
      value: 'profile',
      child: Text(
        'PERFIL COMOONIDADE',
        style: menuStyle,
      ),
    ));
    if (!_isAdmin) {
      entries.add(PopupMenuItem<String>(
        value: 'leave',
        child: Text(
          'Sair da comoonidade',
          style: menuStyle,
        ),
      ));
    }
    return PopupMenuButton<String>(
      onSelected: (String value) => _onSelectMenu(context, value),
      offset: Offset(0.0, responsive(context, 50.0)),
      itemBuilder: (BuildContext context) => entries,
    );
  }

  Future<void> _onSelectMenu(BuildContext context, String value) async {
    final GroupChatBloc bloc = BlocProvider.of<GroupChatBloc>(context);

    // print(value);
    switch (value) {
      case 'invite':
        inviteUsers(context);
        break;
      case 'link':
        showCustomDialog(
          barrierDismissible: false,
          context: context,
          alert: customRoundAlert(
            context: context,
            title: 'Gerando link',
            actions: [],
          ),
        );

        final String message = await createGroupDynamicLink(true, bloc.group);
        print(message);
        Share.share('$message').then((result) {
          Navigator.of(context).pop();
        });
        break;
      case 'profile':
        Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => GroupDetailView(group: bloc.group),
        ));
        break;
      case 'update':
        Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => AddGroupPage(id: bloc.group.id),
        ));
        break;
      case 'leave':
        final double mediaQuery = MediaQuery.of(context).size.width / 400;
        bool confirmLeave = await showCustomDialog(
          context: context,
          alert: customRoundAlert(
            context: context,
            title:
                'Tem certeza que deseja deixar de participar da comoonidade?',
            actions: [
              Container(
                height: mediaQuery * 41.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: Text(
                    'CANCELAR',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                height: mediaQuery * 41.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: Text(
                    'SIM',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
        if (confirmLeave ?? false) {
          bloc.removeUser(bloc.currentUser);
          Navigator.of(context).pop(true);
        }
        break;
    }
  }

  Future<void> inviteUsers(BuildContext context) async {
    final GroupChatBloc bloc = BlocProvider.of<GroupChatBloc>(context);
    List friends = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FriendsPickerPage(group: bloc.group),
      ),
    ) as List;

    if (friends == null || friends.isEmpty) {
      return false;
    }
    bloc.addUsersToGroup(
      bloc.currentUser.uid,
      bloc.currentUser.name,
      friends,
    );
    showCustomDialog(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Seu convite foi enviado com sucesso :)',
      ),
    );
  }

  Widget buildSnapshotQueryData(BuildContext context, QuerySnapshot snapshot) {
    if (snapshot.documents.isEmpty) {
      return buildEmptyData(context);
    }
    final GroupChatBloc bloc = BlocProvider.of<GroupChatBloc>(context);
    return StreamBuilder<List<Widget>>(
      stream: bloc.feed.stream,
      builder: (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
        final List<Widget> feed = snapshot.data;
        if (!snapshot.hasData || snapshot.hasError) {
          return Container();
        }
        return RefreshIndicator(
          onRefresh: () =>
              Future<void>.delayed(const Duration(milliseconds: 200)),
          child: Scrollbar(
            child: ListView.builder(
              padding: EdgeInsets.only(top: responsiveHeight(context, 10.0)),
              itemCount: feed.length,
              itemBuilder: (context, index) {
                // print('building item ${index + 1} of ${feed.length}');
                if (feed.elementAt(index) == null) {
                  return Container();
                }
                return feed[index];
              },
            ),
          ),
        );
      },
    );
  }

  Decoration backgroundDeco() {
    return BoxDecoration(
      image: DecorationImage(
        image: AssetImage('images/skateBackground.jpeg'),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget buildEmptyData(BuildContext context) {
    final GroupChatBloc bloc = BlocProvider.of<GroupChatBloc>(context);
    return Container(
      decoration: backgroundDeco(),
      padding: EdgeInsets.only(
        top: responsiveHeight(context, 40.0),
        left: responsiveWidth(context, 15.0),
        right: responsiveWidth(context, 15.0),
      ),
      child: Column(
        children: <Widget>[
          Text(
            'Oba,\ncomoonidade nova!',
            style: ComooStyles.grupoNovoTitle,
          ),
          Padding(
            padding: EdgeInsets.only(
              // top: responsiveWidth(context, 1.0),
              bottom: responsiveHeight(context, 15.0),
              left: responsiveWidth(context, 15.0),
            ),
            child: Text(
              'Seja o primeiro a anunciar e convide outros usuários para participar da sua comoonidade.',
              style: ComooStyles.grupoNovo.copyWith(
                height: 1.7,
              ),
            ),
          ),
          COMOO.buttons.flatButton(
            context: context,
            onPressed: () => createProduct(context, false),
            height: 35.0,
            width: 280.0,
            text: 'CRIAR UM ANÚNCIO',
          ),
          bloc.group.userAdmin == bloc.currentUser.uid
              ? COMOO.buttons.flatButton(
                  context: context,
                  onPressed: () {
                    inviteUsers(context);
                  },
                  height: 35.0,
                  width: 280.0,
                  text: 'CONVIDAR NA COMOO',
                )
              : Container(),
          COMOO.buttons.flatButton(
            context: context,
            onPressed: () => copyLink(context),
            height: 35.0,
            width: 280.0,
            text: 'ENVIAR LINK DE CONVITE',
          ),
        ],
      ),
    );
  }

  Future<void> copyLink(BuildContext context) async {
    final GroupChatBloc bloc = BlocProvider.of<GroupChatBloc>(context);
    showCustomDialog(
      barrierDismissible: false,
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Gerando link',
        actions: [],
      ),
    );

    final String message = await createGroupDynamicLink(true, bloc.group);
    print(message);
    Share.share('$message').then((result) {
      Navigator.of(context).pop();
    });
  }

  Future<void> createProduct(BuildContext context, bool openCamera) async {
    final GroupChatBloc bloc = BlocProvider.of<GroupChatBloc>(context);
    bloc.reloadCurrentUser();
    if (bloc.currentUser.sellerStatus == 'incomplete') {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              contentPadding: EdgeInsets.all(0.0),
              content: Container(
                child: Container(
                  padding: const EdgeInsets.all(21.0),
                  child: Text(
                    'Para começar a vender na COMOO você precisa acrescentar mais dados ao seu cadastro',
                    style: ComooStyles.welcomeMessage,
                  ),
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: Text(
                    'DEPOIS',
                    style: ComooStyles.botoes,
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: Text(
                    'AGORA',
                    style: ComooStyles.botoes,
                  ),
                ),
              ],
            );
          }).then((result) {
        if (result) {
          // Navigator.of(context).pushNamed('/user_profile');
          Navigator.of(context).pushNamed('/sign_up_personal');
        }
      });
    } else if (bloc.currentUser.sellerStatus == 'waiting') {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              contentPadding: EdgeInsets.all(0.0),
              content: Container(
                child: Container(
                  padding: const EdgeInsets.all(21.0),
                  child: Text(
                    'Seu cadastro ainda está sobre analise, aguarde notificação.',
                    style: ComooStyles.welcomeMessage,
                  ),
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else {
      Navigator.of(context)
          .push(MaterialPageRoute(
              builder: (BuildContext context) => ProductCreateView(
                    openCamera: openCamera,
                  )))
          .then((result) {
        // if (result != null) {
        //   setState(() {});
        // }
      });
    }
  }
}
