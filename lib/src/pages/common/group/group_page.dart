import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/group_detail_bloc.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/feed/product_feed.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:comoo/src/widgets/user_tile.dart';
import 'package:flutter/material.dart';

class SignupGroupPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final SignupGroupDetailBloc bloc =
        BlocProvider.of<SignupGroupDetailBloc>(context);
    return StreamBuilder<bool>(
      stream: bloc.askToJoin,
      builder: (BuildContext context, AsyncSnapshot<bool> askToJoin) {
        return askToJoin.hasData
            ? askToJoin.data
                ? _PrivateGroup(bloc)
                : _PublicGroup(bloc, key: Key(bloc.getGroup?.id))
            : Scaffold(
                body: Container(),
              );
      },
    );
  }
}

class _PrivateGroup extends StatelessWidget with Reusables {
  _PrivateGroup(this.bloc);
  final SignupGroupDetailBloc bloc;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Group>(
      stream: bloc.group,
      builder: (BuildContext context, AsyncSnapshot<Group> snapshot) {
        if (!snapshot.hasData) {
          return LoadingContainer();
        }
        if (snapshot.data == null) {
          return LoadingContainer();
        }
        final Group group = snapshot.data;
        return Scaffold(
          appBar: CustomAppBar.round(
            color: ComooColors.chumbo,
            title: Text('Perfil da comoonidade'),
          ),
          body: ListView(
            children: <Widget>[
              SizedBox(
                height: responsive(context, 30.0),
              ),
              Container(
                alignment: FractionalOffset.center,
                child: Container(
                  width: responsive(context, 202.0),
                  height: responsive(context, 202.0),
                  decoration: BoxDecoration(
                    color: ComooColors.chumbo,
                    shape: BoxShape.circle,
                    border: Border.all(color: ComooColors.chumbo),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: CachedNetworkImageProvider(
                          group.avatar ?? defaultImage()),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: responsive(context, 28.0),
                  horizontal: responsive(context, 50.0),
                ),
                alignment: FractionalOffset.center,
                child: Text(
                  group.name,
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: ComooColors.chumbo,
                    fontSize: responsive(context, 22.0),
                    fontWeight: FontWeight.w600,
                    letterSpacing: responsive(context, 0.31),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  left: responsive(context, 15.0),
                ),
                child: Text(
                  'Descrição do grupo:',
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: ComooColors.chumbo,
                    fontSize: responsive(context, 15.0),
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: responsive(context, 15.0),
                  vertical: responsive(context, 10.0),
                ),
                child: Text(
                  group.description,
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: ComooColors.chumbo,
                    fontSize: responsive(context, 15.0),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              group.location.isNotEmpty
                  ? Container(
                      height: responsive(context, 30.0),
                      padding: EdgeInsets.only(
                        left: responsive(context, 12.0),
                      ),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            ComooIcons.localizacao,
                            size: responsive(context, 34.0),
                          ),
                          Text(
                            group.location ?? '',
                            style: ComooStyles.groupDetailLocation,
                          )
                        ],
                      ),
                    )
                  : Container(),
              StreamBuilder<bool>(
                stream: bloc.isMember,
                builder: (BuildContext context, AsyncSnapshot<bool> isMember) {
                  return Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      (isMember.data ?? false)
                          ? _openGroupFeedButton(context, bloc, group)
                          : StreamBuilder<bool>(
                              stream: bloc.requestedAccess,
                              builder: (BuildContext context,
                                  AsyncSnapshot<bool> requestedAccess) {
                                return (requestedAccess.data ?? false)
                                    ? _requestedAccessButton(
                                        context, bloc, group)
                                    : _requestAccessButton(
                                        context, bloc, group);
                              },
                            ),
                      Positioned(
                        left: responsive(context, 14.0),
                        top: 5.0,
                        bottom: 5.0,
                        child: Image.asset(
                          group.askToJoin
                              ? 'images/padlock_closed.png'
                              : 'images/padlock_open.png',
                          width: responsive(context, 25.0),
                          height: responsive(context, 45.0),
                        ),
                      ),
                    ],
                  );
                },
              ),
              Divider(),
              _groupMemberList(context, bloc, group),
              SizedBox(height: responsive(context, 20.0)),
            ],
          ),
        );
      },
    );
  }

  Widget _openGroupFeedButton(
      BuildContext context, SignupGroupDetailBloc bloc, Group group) {
    return RaisedButton(
      elevation: 0.0,
      color: group.askToJoin ? COMOO.colors.purple : COMOO.colors.lead,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(responsive(context, 30.0)),
      ),
      child: Container(
        constraints: BoxConstraints.tight(
            Size(responsive(context, 252.0), responsive(context, 35.0))),
        child: Center(
          child: FittedBox(
            child: Text(
              group.askToJoin ? 'VER ANÚNCIOS' : 'ENTRAR',
              style: ComooStyles.botao_branco,
            ),
          ),
        ),
      ),
      onPressed: () {
        // _goToGroup();
      },
    );
  }

  Widget _requestedAccessButton(
      BuildContext context, SignupGroupDetailBloc bloc, Group group) {
    return OutlineButton(
      color: ComooColors.roxo,
      disabledBorderColor: COMOO.colors.turquoise,
      disabledTextColor: COMOO.colors.turquoise,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(responsive(context, 30.0))),
      child: Container(
        constraints: BoxConstraints.tight(
            Size(responsive(context, 252.0), responsive(context, 35.0))),
        child: Center(
          child: FittedBox(
            child: Text(
              'SOLICITAÇÃO ENVIADA',
              style: ComooStyles.botao,
            ),
          ),
        ),
      ),
      onPressed: null,
    );
  }

  Widget _requestAccessButton(
      BuildContext context, SignupGroupDetailBloc bloc, Group group) {
    return FlatButton(
      color: ComooColors.turqueza,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(responsive(context, 30.0)),
      ),
      child: Container(
        constraints: BoxConstraints.tight(
            Size(responsive(context, 252.0), responsive(context, 35.0))),
        child: Center(
          child: FittedBox(
            child: Text(
              group.askToJoin ? 'SOLICITAR PARTICIPAÇÃO' : 'ENTRAR',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: Colors.white,
                fontSize: responsive(context, 15.0),
                fontWeight: FontWeight.w500,
                letterSpacing: responsive(context, 0.60),
              ),
            ),
          ),
        ),
      ),
      onPressed: () => bloc.requestEntry(),
    );
  }

  Widget _groupMemberList(
      BuildContext context, SignupGroupDetailBloc bloc, Group group) {
    // print('lenght: ${group.users.length}');
    return StreamBuilder<List<User>>(
      stream: bloc.members,
      builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
        return snapshot.hasData
            ? ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                itemExtent: responsive(context, 60.0),
                itemCount: snapshot.data.length + 1,
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    return Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: responsive(context, 20.0)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Membros da comoonidade',
                            style: TextStyle(
                              fontFamily: COMOO.fonts.montSerrat,
                              fontWeight: FontWeight.w600,
                              fontSize: responsive(context, 14.0),
                              color: COMOO.colors.lead,
                            ),
                          ),
                          StreamBuilder<int>(
                            stream: bloc.memberQuantity,
                            builder: (BuildContext context,
                                AsyncSnapshot<int> quantity) {
                              return Row(
                                children: <Widget>[
                                  Text(
                                    '${quantity.data ?? 0}',
                                    style: TextStyle(
                                      fontFamily: COMOO.fonts.montSerrat,
                                      fontSize: responsive(context, 12.0),
                                      fontWeight: FontWeight.w600,
                                      color: COMOO.colors.gray,
                                      letterSpacing: responsive(context, 0.25),
                                    ),
                                  ),
                                  Icon(
                                    ComooIcons.perfil,
                                    // semanticLabel: members.toString(),
                                    textDirection: TextDirection.ltr,
                                    color: COMOO.colors.gray,
                                    size: responsive(context, 24.0),
                                  ),
                                ],
                              );
                            },
                          ),
                        ],
                      ),
                    );
                  }
                  final User user = snapshot.data[--index];
                  return UserTile(user, fromSignup: true);
                },
              )
            : Center(child: CircularProgressIndicator());
      },
    );
  }
}

class _PublicGroup extends StatelessWidget with Reusables {
  _PublicGroup(this.bloc, {Key key}) : super(key: key);
  final SignupGroupDetailBloc bloc;
  @override
  Widget build(BuildContext context) {
    print('PUBLIC GROUP');
    return StreamBuilder<Group>(
      stream: bloc.group,
      builder: (BuildContext context, AsyncSnapshot<Group> groupSnapshot) {
        final Group group = groupSnapshot.data;
        return Scaffold(
          appBar: CustomAppBar.round(
            title: Text(
              '${(group?.name) ?? ''}',
              overflow: TextOverflow.fade,
            ),
            color: COMOO.colors.lead,
          ),
          bottomNavigationBar: Container(
            constraints: BoxConstraints.tight(Size(
                MediaQuery.of(context).size.width, responsive(context, 50))),
            child: RaisedButton(
              elevation: 0.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.zero)),
              color: COMOO.colors.pink,
              onPressed: () async {
                final bool result = await _showDialog(context);
                if (result ?? false) {
                  Navigator.of(context).pop(true);
                }
              },
              child: Text(
                'ENTRAR NESSA COMOONIDADE',
                style: TextStyle(
                  fontFamily: COMOO.fonts.montSerrat,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: responsive(context, 13.0),
                ),
              ),
            ),
          ),
          body: StreamBuilder<List<FeedGroup>>(
            stream: bloc.feed,
            builder:
                (BuildContext context, AsyncSnapshot<List<FeedGroup>> feed) {
              print('${feed.hasData} ${feed.connectionState}');
              print('${feed.data?.length}');
              return feed.hasData ? _GroupFeedList(feed.data) : Container();
            },
          ),
        );
      },
    );
  }

  Future<bool> _showDialog(BuildContext context) async {
    return showCustomDialog(
        context: context,
        alert: customRoundAlert(
          context: context,
          title: 'Comoonidade legal?',
          subtitle: Padding(
            padding: EdgeInsets.symmetric(horizontal: responsive(context, 8.0)),
            child: Text(
              'Clique no botāo ENTRAR\n'
              'e comece a comprar\n'
              'e vender agora.',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: responsive(context, 18.0),
              ),
            ),
          ),
          actions: <Widget>[
            Container(
              height: responsive(context, 41.0),
              // width: responsive(context, 163.0),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.0,
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text(
                  'FECHAR',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Container(
              height: responsive(context, 41.0),
              // width: responsive(context, 163.0),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.0,
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                color: Colors.white,
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
                child: Text(
                  'ENTRAR',
                  style: TextStyle(
                    color: COMOO.colors.pink,
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  // Widget _buildSnapshotData(BuildContext context, List<DocumentSnapshot> data) {
  //   return Container();
  // }
}

class _GroupFeedList extends StatefulWidget {
  _GroupFeedList(this.feed);
  final List<FeedGroup> feed;
  @override
  State<StatefulWidget> createState() => _GroupFeedListState();
}

class _GroupFeedListState extends State<_GroupFeedList> {
  final ScrollController controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    return _GroupFeedListStateless(widget.feed, controller: controller);
  }
}

class _GroupFeedListStateless extends StatelessWidget with Reusables {
  _GroupFeedListStateless(this.feed, {this.controller, this.key})
      : assert(feed != null),
        super(key: key);
  final Key key;
  final List<FeedGroup> feed;
  final ScrollController controller;
  final NetworkApi api = NetworkApi();
  @override
  Widget build(BuildContext context) {
    return feed.isEmpty ? buildEmptyData(context) : buildDataList(context);
  }

  // static Iterable<FutureOr<Widget>> _parseSnapshotToWidget(
  //     List<DocumentSnapshot> snaps) sync* {
  //   yield* snaps.map<FutureOr<Widget>>((DocumentSnapshot snapshot) {
  //     if (snapshot.exists) {
  //       final String type = snapshot.data['type'];
  //       switch (type) {
  //         case 'product':
  //           DocumentReference productRef =
  //               snapshot.data['product'] as DocumentReference;
  //           return productRef.get().then((DocumentSnapshot snap) {
  //             final Product product = Product.fromSnapshot(snap);
  //             return ProductFeed(product, key: Key(snap.documentID));
  //           }).catchError((onError) {
  //             print('onError');
  //             return Container();
  //           });
  //           break;
  //         case 'post':
  //           DocumentReference postRef =
  //               snapshot.data['post'] as DocumentReference;
  //           return postRef.get().then<Widget>((DocumentSnapshot snap) {
  //             final Post post = Post.fromSnapshot(snap);
  //             return PostFeed(post: post, key: Key(snap.documentID));
  //           }).catchError((onError) {
  //             print('onError');
  //             return Container();
  //           });
  //           break;
  //         default:
  //           return Container();
  //           break;
  //       }
  //     }
  //     return Container();
  //   });
  // }

  Widget buildEmptyData(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      decoration: backgroundDeco(),
      padding: EdgeInsets.only(
        top: mediaQuery * 40.0,
        left: mediaQuery * 15.0,
        right: mediaQuery * 15.0,
      ),
      child: Column(
        children: <Widget>[
          Text(
            'Oba,\ncomoonidade nova!',
            style: ComooStyles.grupoNovoTitle,
          ),
          Padding(
            padding: EdgeInsets.only(
              // top: mediaQuery * 1.0,
              bottom: mediaQuery * 15.0,
              left: mediaQuery * 15.0,
            ),
            child: Text(
              'Seja o primeiro a anunciar e convide outros usuários para participar da sua comoonidade.',
              style: ComooStyles.grupoNovo.copyWith(
                height: mediaQuery * 1.7,
              ),
            ),
          ),
          COMOO.buttons.flatButton(
            context: context,
            onPressed: () {
              // createProduct(false);
            },
            height: 35.0,
            width: 280.0,
            text: 'CRIAR UM ANÚNCIO',
          ),
          // widget.group.userAdmin == currentUser.uid
          //     ? COMOO.buttons.flatButton(
          //         context: context,
          //         onPressed: () {
          //           // inviteUsers();
          //         },
          //         height: 35.0,
          //         width: 280.0,
          //         text: 'CONVIDAR NA COMOO',
          //       )
          //     : Container(),
          COMOO.buttons.flatButton(
            context: context,
            onPressed: () {
              // copyLink();
            },
            height: 35.0,
            width: 280.0,
            text: 'ENVIAR LINK DE CONVITE',
          ),
        ],
      ),
    );
  }

  Decoration backgroundDeco() {
    return BoxDecoration(
      image: DecorationImage(
        image: AssetImage('images/skateBackground.jpeg'),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget buildDataList(BuildContext context) {
    // return Container();
    return ListView.builder(
      key: key,
      padding: EdgeInsets.symmetric(vertical: responsive(context, 10.0)),
      controller: controller,
      cacheExtent: MediaQuery.of(context).size.height * 3,
      // shrinkWrap: true,
      semanticChildCount: 10,
      // itemExtent: 500,
      scrollDirection: Axis.vertical,
      // physics: BouncingScrollPhysics(),
      // itemCount: feed.length,
      // itemBuilder: (BuildContext context, int index) {
      //   // print('index: ${index % feed.length}');
      //   return feed[index];
      // },
      // controller: ScrollController(),
      itemCount: feed.length,
      // physics: BouncingScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        // print(feed[index]);
        return feed[index] == null
            ? Container()
            : buildData(context, feed[index]);
        // return Text('${feed[index]}');
        // return FutureBuilder<Widget>(
        //   future: feed[index],
        //   builder: (BuildContext context, AsyncSnapshot<Widget> snap) {
        //     print('${snap.hasData} ${snap.data}');
        //     return snap.hasData
        //         ? Container(
        //             child: snap?.data,
        //           )
        //         : Center(child: CircularProgressIndicator());
        //   },
        // );
      },
    );
  }

  Widget buildData(BuildContext context, FeedGroup feedGroup) {
    switch (feedGroup.type) {
      case FeedGroupType.post:
        return Container();
        break;
      case FeedGroupType.product:
        final DocumentSnapshot snap = feedGroup.snapshot;
        if (snap.exists) {
          final Product product = Product.fromSnapshot(snap);
          final double width = MediaQuery.of(context).size.width;
          final double photoRatio = width / (product.width ?? width);
          final double photoHeight =
              product.height == null ? 375.0 : product.height * photoRatio;
          double text = (product.title.length * 1.5) / 40 +
              product.description.length / 40;
          text = text > 10 ? 10 : text;
          // print('${product.title} - ${text.ceilToDouble()}');
          final double height = 380 + (text * 16) + photoHeight;
          return Container(
              constraints: BoxConstraints.expand(
                  width: width, height: responsive(context, height)),
              child: ProductFeed(
                product,
                callback: () async {
                  final bool result = await _showDialog(context);
                  if (result ?? false) {
                    Navigator.of(context).pop(true);
                  }
                },
              ));
        }
        return Container();
        break;
      case FeedGroupType.undefined:
      default:
        return Container();
    }
  }

  Future<bool> _showDialog(BuildContext context) async {
    return showCustomDialog(
        context: context,
        alert: customRoundAlert(
          context: context,
          title: 'Comoonidade legal?',
          subtitle: Padding(
            padding: EdgeInsets.symmetric(horizontal: responsive(context, 8.0)),
            child: Text(
              'Clique no botāo ENTRAR\n'
              'e comece a comprar\n'
              'e vender agora.',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: responsive(context, 18.0),
              ),
            ),
          ),
          actions: <Widget>[
            Container(
              height: responsive(context, 41.0),
              // width: responsive(context, 163.0),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.0,
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text(
                  'FECHAR',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Container(
              height: responsive(context, 41.0),
              // width: responsive(context, 163.0),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.0,
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                color: Colors.white,
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
                child: Text(
                  'ENTRAR',
                  style: TextStyle(
                    color: COMOO.colors.pink,
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
