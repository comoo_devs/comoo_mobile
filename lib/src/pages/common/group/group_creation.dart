import 'package:comoo/src/blocs/group_creation_bloc.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:flutter/material.dart';

class GroupCreation extends StatefulWidget {
  GroupCreation({this.id});
  final String id;
  @override
  State<GroupCreation> createState() => _GroupCreationState();
}

class _GroupCreationState extends State<GroupCreation> {
  GroupCreationBloc bloc;
  @override
  void initState() {
    bloc = GroupCreationBloc(id: widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GroupCreationBloc>(
        bloc: bloc, child: GroupCreationPage());
  }
}

class GroupCreationPage extends StatelessWidget {
  GroupCreationPage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container());
  }
}
