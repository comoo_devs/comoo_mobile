import 'dart:async';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/pages/common/user/friends_picker.dart';
import 'package:comoo/src/pages/common/user/user_friend.dart';
import 'package:comoo/src/pages/common/user/user_suggestion.dart';
import 'package:comoo/src/utility/app_preferences.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:comoo/src/widgets/user_tile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/pages/common/group/group_chat/group_chat.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:comoo/src/comoo/comoo.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:share/share.dart';
import 'package:comoo/src/pages/home/bottom_menu/group_creation/add_group.dart';

class GroupDetailView extends StatefulWidget {
  final Group group;
  final String id;

  GroupDetailView({this.group, this.id, Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return GroupDetailViewState();
  }
}

class GroupDetailViewState extends State<GroupDetailView> with Reusables {
  final AppPreferences prefs = AppPreferences();

  Group group = Group();

  List users = List();
  int members = 0;

  // final double _appBarHeight = 256.0;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final Firestore _db = Firestore.instance;

  // bool _isMember = false;
  bool _requestedAccess = false;
  bool _loading = true;
  bool _isAdmin = false;
  bool _isMember = false;

  @override
  void initState() {
    super.initState();
    if (widget.group == null) {
      this._loading = true;
      this._fetchGroup(widget.id);
    } else {
      this._loading = false;
      this.group = widget.group;
    }
    _checkIfRequestedAccess();
    this._checkIfAdmin();
    _checkIfIsMember();
    _loadFriendList();
  }

  _loadFriendList() async {
    await currentUser.fetchFriendReference();
  }

  _checkIfRequestedAccess() async {
    final FirebaseUser fireUser = await FirebaseAuth.instance.currentUser();
    await _db.collection('users').document(fireUser.uid).get().then((user) {
      final List<String> groupRequests = user['group_requests']?.cast<String>();
      if (groupRequests != null) {
        print(group.id);
        for (String groupId in groupRequests) {
          _requestedAccess = false;
          if (group.id == groupId) {
            _requestedAccess = true;
            break;
          }
        }
      }
    });
  }

  _checkIfIsMember() async {
    String id = this.group == null ? widget.id : this.group.id;
    var queryResult = await _db
        .collection('groups')
        .document(id)
        .collection('users')
        .where('id', isEqualTo: currentUser.uid)
        .getDocuments();

    if (queryResult.documents.isNotEmpty) {
      if (this.mounted)
        setState(() {
          _isMember = true;
        });
    }
  }

  Future _fetchGroup(String groupId) async {
    if (this.mounted)
      setState(() {
        _loading = true;
      });

    Group _group = Group.fromSnapshot(await comoo.api.fetchGroup(groupId));

    if (this.mounted) {
      setState(() {
        _loading = false;
        this.group = _group;
      });
    }
  }

  Future<QuerySnapshot> _fetchUsers() async {
    String id = this.group == null ? widget.id : this.group.id;
    Future<QuerySnapshot> future = comoo.api.fetchGroupUsers(id);
    future.asStream().listen((data) {
      if (this.mounted) {
        setState(() {
          members = data.documents.length;
        });
      }
    });
    return future;
  }

  void showInSnackBar(String value) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Padding(
        padding: EdgeInsets.all(mediaQuery * 8.0),
        child: Text(value, style: ComooStyles.textoSnackBar),
      ),
      duration: Duration(seconds: 2),
    ));
  }

  Future _checkIfAdmin() async {
    if (this.mounted)
      setState(() {
        _loading = true;
      });
    String id = widget.group == null ? widget.id : widget.group.id;
    var groups = await _db
        .collection('users')
        .document(currentUser.uid)
        .collection('groups')
        .where('id', isEqualTo: id)
        .getDocuments();

    if (groups.documents.isNotEmpty) {
      if (this.mounted) {
        setState(() {
          _isAdmin = groups.documents.first.data['admin'] ?? false;
          _loading = false;
        });
      }
    } else {
      if (this.mounted)
        setState(() {
          _isAdmin = false;
          _loading = false;
        });
    }
  }

  Future<QuerySnapshot> _fetchGroupParticipation() async {
    String id = this.group == null ? widget.id : this.group.id;
    var queryResult = await _db
        .collection('groups')
        .document(id)
        .collection('users')
        .where('id', isEqualTo: currentUser.uid)
        .getDocuments();

    if (queryResult.documents.isNotEmpty) {
      if (this.mounted)
        setState(() {
          _isMember = true;
        });
    }

    return Future.value(queryResult);
  }

  Future<DocumentSnapshot> _loadUser(String id) {
    return _db.collection('users').document(id).get();
    // var userRef = await _db.collection('users').document(id).get();
    // var user = User.fromSnapshot(userRef);
    // if (user == null) {
    //   print(user.name);
    // }
    // if (user.uid == currentUser.uid) {
    //   _fetchGroupParticipation = true;
    //   // setState(() {
    //   if ((userRef['group_requests'] as List)
    //           .firstWhere((e) => e == widget.group.id) !=
    //       null) {
    //     _requestedAccess = true;
    //   }
    //   //   _fetchGroupParticipation = true;
    //   // });
    // }
    // return user;
  }

  _goToGroup() {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => GroupChatPage(this.group)));
  }

  _showAlert() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          content: Container(
            child: Text('Requisição sendo enviada.\nAguarde um momento.'),
          ),
        );
      },
    );
  }

  _joinGroup() async {
    setState(() {
      _requestedAccess = true;
    });
    try {
      await CloudFunctions.instance
          .getHttpsCallable(functionName: 'requestJoinGroup')
          .call(<String, String>{
        'groupId': widget.id ?? widget.group.id,
      }).then((HttpsCallableResult result) {
        print(result);
        return result.data;
      }).timeout(Duration(seconds: 60));
    } on TimeoutException catch (e) {
      print('>> requestJoinGroup TimeoutError!\n$e');
      setState(() {
        _requestedAccess = false;
      });
    } on CloudFunctionsException catch (e) {
      print('>> requestJoinGroup CloudFunctionsError!\n${e.message}');
      setState(() {
        _requestedAccess = false;
      });
    } catch (e) {
      print('>> requestJoinGroup UnkownError!\n$e');
      setState(() {
        _requestedAccess = false;
      });
    }
    Navigator.of(context).pop();
    // String id = this.group == null ? widget.id : this.group.id;
    // var groupRef = await _db.collection('groups').document(id).get();

    // // if (widget.group.askToJoin) {
    // var batch = _db.batch();

    // var userRef = _db.collection('users').document(currentUser.uid);
    // final DocumentSnapshot userSnap = await userRef.get();
    // List<dynamic> oldGroupRequests = userSnap['group_requests'];
    // List<dynamic> groupRequests = [];
    // if (groupRequests == null) {
    //   groupRequests = [id];
    // } else {
    //   groupRequests.addAll(oldGroupRequests);
    //   groupRequests.add(id);
    // }
    // batch.updateData(
    //   userRef,
    //   <String, List<dynamic>>{'group_requests': groupRequests},
    // );
    // await CloudFunctions.instance
    //     .call(functionName: 'generateUserNotifications', parameters: {
    //   'type': 'group_request',
    //   'ref': id,
    //   'userId': currentUser.uid,
    // }).catchError((error) {
    //   print('>> generateUserNotifications Error!\n$error');
    //   var result = {'result': 'error!'};
    //   return result;
    // });
    // batch.commit().then((_) {
    //   Navigator.pop(context);
    //   if (mounted)
    //     setState(() {
    //       _requestedAccess = true;
    //     });
    //   showInSnackBar(
    //       'Solicitação enviada para o administrador da comoonidade.');
    // });
  }

  // Future<void> _displayUserDialog(User user) async {
  //   print(this._isAdmin);
  //   if (this._isAdmin) {
  //     String option = await showDialog(
  //         context: context,
  //         builder: (BuildContext context) {
  //           return SimpleDialog(
  //             children: <Widget>[
  //               SimpleDialogOption(
  //                 child: Text(
  //                   'Ver ' + user.name.split(' ').first,
  //                   style: ComooStyles.texto,
  //                 ),
  //                 onPressed: () {
  //                   Navigator.of(context).pop('view');
  //                 },
  //               ),
  //               SimpleDialogOption(
  //                 child: Text('Remover ' + user.name.split(' ').first,
  //                     style: ComooStyles.texto),
  //                 onPressed: () {
  //                   Navigator.of(context).pop('remove');
  //                 },
  //               ),
  //             ],
  //           );
  //         });
  //     if (option == 'view') {
  //       Navigator.of(context).push(MaterialPageRoute(
  //           builder: (BuildContext context) => UserDetailView(user)));
  //     } else if (option == 'remove') {
  //       bool willRemove = await showDialog(
  //           context: context,
  //           builder: (BuildContext context) {
  //             return AlertDialog(
  //               content: Text(
  //                 'Deseja remover ' +
  //                     user.name +
  //                     ' da comoonidade ' +
  //                     group.name,
  //                 style: ComooStyles.welcomeMessage,
  //               ),
  //               actions: <Widget>[
  //                 FlatButton(
  //                   child: Text(
  //                     'Cancelar',
  //                     style: ComooStyles.botaoTurqueza,
  //                   ),
  //                   onPressed: () {
  //                     Navigator.of(context).pop(false);
  //                   },
  //                 ),
  //                 FlatButton(
  //                   child: Text('OK', style: ComooStyles.botaoTurqueza),
  //                   onPressed: () {
  //                     Navigator.of(context).pop(true);
  //                   },
  //                 ),
  //               ],
  //             );
  //           });
  //       if (willRemove) {
  //         _removeUser(user);
  //       }
  //     }
  //   } else {
  //     Navigator.of(context).push(MaterialPageRoute(
  //         builder: (BuildContext context) => UserDetailView(user)));
  //   }
  // }

  _removeUser(User user) async {
    if (mounted)
      setState(() {
        _loading = true;
      });
    String id = this.group == null ? widget.id : this.group.id;
    var batch = _db.batch();
    var groupUserRef = _db
        .collection('groups')
        .document(id)
        .collection('users')
        .document(user.uid);
    var userGroupRef = _db
        .collection('users')
        .document(user.uid)
        .collection('groups')
        .document(id);

    batch.delete(groupUserRef);
    batch.delete(userGroupRef);
    await batch.commit();
    if (mounted)
      setState(() {
        _loading = false;
      });
  }

  void _selectedMenu(String value) async {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    if (value == 'leave') {
      bool confirmLeave = await showCustomDialog(
        context: context,
        alert: customRoundAlert(
          context: context,
          title: 'Tem certeza que deseja deixar de participar da comoonidade?',
          actions: [
            Container(
              height: mediaQuery * 41.0,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.0,
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text(
                  'CANCELAR',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Container(
              height: mediaQuery * 41.0,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.0,
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
                child: Text(
                  'SIM',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
      // builder: (BuildContext context) {
      //   return AlertDialog(
      //     content: Text(
      //       'Tem certeza que deseja deixar de participar da comoonidade?',
      //       style: ComooStyles.welcomeMessage,
      //     ),
      //     actions: <Widget>[
      //       FlatButton(
      //         child: Text('Cancelar', style: ComooStyles.botaoTurqueza),
      //         onPressed: () {
      //           Navigator.of(context).pop(false);
      //         },
      //       ),
      //       FlatButton(
      //         child: Text('Sim', style: ComooStyles.botaoTurqueza),
      //         onPressed: () {
      //           Navigator.of(context).pop(true);
      //         },
      //       ),
      //     ],
      //   );
      // });

      if (confirmLeave ?? false) {
        _removeUser(currentUser);
        Navigator.of(context).pop(true);
      }
    } else if (value == 'update') {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => AddGroupPage(id: this.group.id)));
    } else if (value == 'link') {
      showCustomDialog(
        barrierDismissible: false,
        context: context,
        alert: customRoundAlert(
          context: context,
          title: 'Gerando link',
          actions: [],
        ),
      );

      final String message = await createGroupDynamicLink(true, group);
      print(message);
      Share.share('$message').then((result) {
        Navigator.of(context).pop();
      });
    } else if (value == 'invite') {
      inviteUsers();
    } else if (value == 'suggestion') {
      print('suggestion');
      _makeGroupSuggestion();
    }
  }

  Future<void> _makeGroupSuggestion() async {
    await Navigator.push(
      context,
      MaterialPageRoute<void>(
        builder: (context) => UserSuggestionPage(
          group: widget.group,
        ),
      ),
    );
  }

  Future<void> inviteUsers() async {
    Group group;
    if (widget.group == null) {
      final DocumentSnapshot snap =
          await _db.collection('groups').document(widget.id).get();
      group = Group.fromSnapshot(snap);
    } else {
      group = widget.group;
    }
    List friends = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FriendsPickerPage(
          group: group,
        ),
      ),
    ) as List;

    if (friends == null || friends.isEmpty) {
      return false;
    }
    // _addFriendsToGroup(friends);
    _addUsersToGroup(
      currentUser.uid,
      currentUser.name,
      friends,
    );
    showCustomDialog(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Seu convite foi enviado com sucesso :)',
      ),
    );
  }

  _addUsersToGroup(
    String fromID,
    String fromName,
    List users,
  ) async {
    // print(widget.group.id);
    // WriteBatch batch = _db.batch();
    users.forEach((u) {
      UserFriend user = UserFriend.fromSnapshot(u);
      CloudFunctions.instance
          .getHttpsCallable(functionName: 'generateUserNotifications')
          .call(<String, dynamic>{
        'type': 'group_invite',
        'ref': group.id,
        'userId': user.uid,
        'senderId': currentUser.uid,
      }).catchError((error) {
        print('>> generateUserNotifications Error!\n$error');
        var result = {'result': 'error!'};
        return result;
      });
      // var userRequestNotificationRef = _db
      //     .collection('users')
      //     .document(user.uid)
      //     .collection('notifications')
      //     .document();
      // print('${user.uid} ${userRequestNotificationRef.documentID}');

      // batch.setData(
      //   userRequestNotificationRef,
      //   <String, dynamic>{
      //     'id': userRequestNotificationRef.documentID,
      //     'thumb': widget.group.avatar,
      //     'from': fromID,
      //     'type': 'group_invite',
      //     'date': DateTime.now(),
      //     'name': fromName,
      //     'group': widget.group.id,
      //     'text':
      //         'Convidou você para entrar na comoonidade ${widget.group.name}',
      //   },
      // );
    });
    // batch.commit();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text('Perfil da Comoonidade'),
        actions: _isMember
            ? <Widget>[
                PopupMenuButton<String>(
                  onSelected: _selectedMenu,
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    _isAdmin
                        ? const PopupMenuItem<String>(
                            value: 'update',
                            child: const Text('Editar Comoonidade'),
                          )
                        : const PopupMenuItem<String>(
                            value: 'leave',
                            child: const Text('Sair da comoonidade'),
                          ),
                    PopupMenuItem<String>(
                      value: 'link',
                      child: Text('Convidar via link'),
                    ),
                    PopupMenuItem<String>(
                      value: 'invite',
                      child: Text('Adicionar amigos'),
                    ),
                    !_isAdmin
                        ? PopupMenuItem<String>(
                            value: 'suggestion',
                            child: const Text('Sugerir comoonidade'),
                          )
                        : null,
                  ],
                ),
              ]
            : null,
      ),
      body: Stack(
        children: <Widget>[
          _loading
              ? LoadingContainer()
              : SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: mediaQuery * 30.0,
                      ),
                      Container(
                        alignment: FractionalOffset.center,
                        child: Container(
                          width: mediaQuery * 202.0,
                          height: mediaQuery * 202.0,
                          decoration: BoxDecoration(
                            color: ComooColors.chumbo,
                            shape: BoxShape.circle,
                            border: Border.all(color: ComooColors.chumbo),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AdvancedNetworkImage(group.avatar ??
                                  'https://firebasestorage.googleapis.com/v0/b/app-comoo.appspot.com/o/defaultUser.png?alt=media&token=44335ced-6b07-4511-8f73-d9a4a2a6d1eb'),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                          vertical: mediaQuery * 28.0,
                          horizontal: mediaQuery * 50.0,
                        ),
                        alignment: FractionalOffset.center,
                        child: Text(
                          group.name,
                          style: TextStyle(
                            fontFamily: ComooStyles.fontFamily,
                            color: ComooColors.chumbo,
                            fontSize: mediaQuery * 22.0,
                            fontWeight: FontWeight.w600,
                            letterSpacing: mediaQuery * 0.31,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          left: mediaQuery * 15.0,
                        ),
                        child: Text(
                          'Descrição da comoonidade:',
                          style: TextStyle(
                            fontFamily: ComooStyles.fontFamily,
                            color: ComooColors.chumbo,
                            fontSize: mediaQuery * 15.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: mediaQuery * 15.0,
                          vertical: mediaQuery * 10.0,
                        ),
                        child: Text(
                          group.description,
                          style: TextStyle(
                            fontFamily: ComooStyles.fontFamily,
                            color: ComooColors.chumbo,
                            fontSize: mediaQuery * 15.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      group.location.isNotEmpty
                          ? Container(
                              height: mediaQuery * 30.0,
                              padding: EdgeInsets.only(
                                left: mediaQuery * 12.0,
                              ),
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    ComooIcons.localizacao,
                                    size: mediaQuery * 34.0,
                                  ),
                                  Text(
                                    group.location ?? '',
                                    style: ComooStyles.groupDetailLocation,
                                  )
                                ],
                              ),
                            )
                          : Container(),
                      Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          _isMember
                              ? RaisedButton(
                                  elevation: 0.0,
                                  color: group.askToJoin
                                      ? COMOO.colors.purple
                                      : COMOO.colors.lead,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        responsive(context, 30.0)),
                                  ),
                                  child: Container(
                                    constraints: BoxConstraints.tight(Size(
                                        responsive(context, 252.0),
                                        responsive(context, 35.0))),
                                    child: Center(
                                      child: FittedBox(
                                        child: Text(
                                          group.askToJoin
                                              ? 'VER ANÚNCIOS'
                                              : 'ENTRAR',
                                          style: ComooStyles.botao_branco,
                                        ),
                                      ),
                                    ),
                                  ),
                                  onPressed: () {
                                    _goToGroup();
                                  },
                                )
                              : _requestedAccess
                                  ? OutlineButton(
                                      color: ComooColors.roxo,
                                      disabledBorderColor:
                                          COMOO.colors.turquoise,
                                      disabledTextColor: COMOO.colors.turquoise,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              responsive(context, 30.0))),
                                      child: Container(
                                        constraints: BoxConstraints.tight(Size(
                                            responsive(context, 252.0),
                                            responsive(context, 35.0))),
                                        child: Center(
                                          child: FittedBox(
                                            child: Text(
                                              'SOLICITAÇÃO ENVIADA',
                                              style: ComooStyles.botao,
                                            ),
                                          ),
                                        ),
                                      ),
                                      onPressed: null,
                                    )
                                  : FutureBuilder<QuerySnapshot>(
                                      future: _fetchGroupParticipation(),
                                      builder: (BuildContext context,
                                          AsyncSnapshot<QuerySnapshot>
                                              snapshot) {
                                        if (!snapshot.hasData)
                                          return Container();

                                        if (snapshot.data.documents.isEmpty) {
                                          return FlatButton(
                                            color: ComooColors.turqueza,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      responsive(
                                                          context, 30.0)),
                                            ),
                                            child: Container(
                                              constraints: BoxConstraints.tight(
                                                  Size(
                                                      responsive(
                                                          context, 252.0),
                                                      responsive(
                                                          context, 35.0))),
                                              child: Center(
                                                child: FittedBox(
                                                  child: Text(
                                                    group.askToJoin
                                                        ? 'SOLICITAR PARTICIPAÇÃO'
                                                        : 'ENTRAR',
                                                    style: TextStyle(
                                                      fontFamily: ComooStyles
                                                          .fontFamily,
                                                      color: Colors.white,
                                                      fontSize: responsive(
                                                          context, 15.0),
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      letterSpacing: responsive(
                                                          context, 0.60),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            onPressed: () {
                                              _showAlert();
                                              _joinGroup();
                                            },
                                          );
                                        } else {
                                          return RaisedButton(
                                            elevation: 0.0,
                                            color: group.askToJoin
                                                ? COMOO.colors.purple
                                                : COMOO.colors.lead,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      responsive(
                                                          context, 30.0)),
                                            ),
                                            child: Container(
                                              constraints: BoxConstraints.tight(
                                                  Size(
                                                      responsive(
                                                          context, 252.0),
                                                      responsive(
                                                          context, 35.0))),
                                              child: Center(
                                                child: FittedBox(
                                                  child: Text(
                                                    group.askToJoin
                                                        ? 'VER ANÚNCIOS'
                                                        : 'ENTRAR',
                                                    style: ComooStyles
                                                        .botao_branco,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            onPressed: () {
                                              _goToGroup();
                                            },
                                          );
                                        }
                                      },
                                    ),
                          Positioned(
                            left: responsive(context, 14.0),
                            top: 5.0,
                            bottom: 5.0,
                            child: Image.asset(
                              group.askToJoin
                                  ? 'images/padlock_closed.png'
                                  : 'images/padlock_open.png',
                              width: responsive(context, 20.0),
                              height: responsive(context, 36.0),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: ComooColors.cinzaMedio),
                      Container(
                        padding: EdgeInsets.only(
                          left: mediaQuery * 18.0,
                          bottom: mediaQuery * 16.0,
                          right: mediaQuery * 12.0,
                        ),
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Membros da Comoonidade',
                                style: ComooStyles.groupDetailDesc_bold,
                              ),
                              // TextField(
                              //   i
                              // ),
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      members.toString(),
                                      style: TextStyle(
                                        fontFamily: ComooStyles.fontFamily,
                                        color: ComooColors.cinzaMedio,
                                        fontSize: mediaQuery * 14.0,
                                        fontWeight: FontWeight.w600,
                                        letterSpacing: mediaQuery * 0.25,
                                      ),
                                    ),
                                    Icon(
                                      ComooIcons.perfil,
                                      semanticLabel: members.toString(),
                                      textDirection: TextDirection.ltr,
                                      color: ComooColors.cinzaMedio,
                                      size: mediaQuery * 24.0,
                                    ),
                                  ],
                                ),
                              ),
                            ]),
                      ),
                      FutureBuilder<QuerySnapshot>(
                          future: _fetchUsers(),
                          builder: (BuildContext context,
                              AsyncSnapshot<QuerySnapshot> snapshot) {
                            if (!snapshot.hasData)
                              return Center(child: CircularProgressIndicator());
                            return Column(
                                children: snapshot.data.documents
                                    .map((DocumentSnapshot document) {
                              return FutureBuilder(
                                  future: _loadUser(document.documentID),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<DocumentSnapshot>
                                          snapshot) {
                                    if (!snapshot.hasData) {
                                      return Container();
                                    }
                                    User user =
                                        User.fromSnapshot(snapshot.data);
                                    if (users
                                        .where((u) => u.uid == user.uid)
                                        .isEmpty) {
                                      // print(user.uid);
                                      users.add(user);
                                    }
                                    return UserTile(
                                      user,
                                      subtitle: user.uid == group.userAdmin
                                          ? Text(
                                              'Administrador',
                                              style: TextStyle(
                                                fontFamily:
                                                    ComooStyles.fontFamily,
                                                color: ComooColors.chumbo,
                                                fontSize: mediaQuery * 14.0,
                                                fontWeight: FontWeight.bold,
                                                letterSpacing:
                                                    mediaQuery * 0.25,
                                              ),
                                            )
                                          : null,
                                    );
                                    // print('${user.uid} ${user.name} ${_isFriend(user)}');
                                    // return ListTile(
                                    //   onTap: () {
                                    //     if (user.uid != currentUser.uid)
                                    //       _displayUserDialog(user);
                                    //   },
                                    //   leading: Container(
                                    //     // margin: EdgeInsets.only(
                                    //     //     right: mediaQuery * 16.0,
                                    //     // ),
                                    //     width: 50.0,
                                    //     height: 50.0,
                                    //     child: ClipOval(
                                    //       child: Image(
                                    //         image: AdvancedNetworkImage(
                                    //             user.photoURL),
                                    //         fit: BoxFit.cover,
                                    //       ),
                                    //     ),
                                    //   ),
                                    //   title: Text(
                                    //     user.name,
                                    //     style: TextStyle(
                                    //       fontFamily: ComooStyles.fontFamily,
                                    //       color: ComooColors.chumbo,
                                    //       fontSize: mediaQuery * 14.0,
                                    //       fontWeight: FontWeight.w500,
                                    //       letterSpacing: mediaQuery * 0.25,
                                    //     ),
                                    //   ),
                                    //   subtitle: user.uid == group.userAdmin
                                    //       ? Text(
                                    //           'Administrador',
                                    //           style: TextStyle(
                                    //             fontFamily:
                                    //                 ComooStyles.fontFamily,
                                    //             color: ComooColors.chumbo,
                                    //             fontSize: mediaQuery * 14.0,
                                    //             fontWeight: FontWeight.bold,
                                    //             letterSpacing:
                                    //                 mediaQuery * 0.25,
                                    //           ),
                                    //         )
                                    //       : null,
                                    //   trailing: (user.uid == currentUser.uid)
                                    //       ? null
                                    //       : FlatButton(
                                    //           onPressed: () {
                                    //             _isFriend(user)
                                    //                 ? _removeFriend(user)
                                    //                 : _addFriend(user);
                                    //             // ? _addFriend(user)
                                    //             // : _removeFriend(user);
                                    //           },
                                    //           child: _isFriend(user)
                                    //               ? Image.asset(
                                    //                   'images/checked_turquoise.png',
                                    //                   height: mediaQuery * 30.0,
                                    //                   width: mediaQuery * 30.0,
                                    //                 )
                                    //               : Image.asset(
                                    //                   'images/add_white.png',
                                    //                   height: mediaQuery * 30.0,
                                    //                   width: mediaQuery * 30.0,
                                    //                 ),
                                    //           // child: Icon(
                                    //           //   _isFriend(user)
                                    //           //       ? Icons.check_circle
                                    //           //       : Icons.add_circle_outline,
                                    //           //   color: ComooColors.turqueza,
                                    //           // ),
                                    //         ),
                                    // );
                                  });
                            }).toList());
                          })
                    ],
                  ),
                )
        ],
      ),
    );
  }

  // bool _isFriend(User user) {
  //   bool isFriend = false;
  //   List friends =
  //       (currentUser.friends.where((uid) => uid == user.uid)).toList();
  //   // print('isEmpty? ${friends.isEmpty}');
  //   isFriend = friends.isNotEmpty;
  //   // print(currentUser.friends.length);
  //   return isFriend;
  // }

  // _addFriend(User user) async {
  //   var friend = _db
  //       .collection('users')
  //       .document(currentUser.uid)
  //       .collection('friends')
  //       .document(user.uid);

  //   await friend.setData({
  //     'id': _db.collection('users').document(user.uid),
  //     'origin': 'added'
  //   }).then((_) {
  //     this.showInSnackBar(
  //         '${user.name} adicionado à sua rede de relacionamentos.');
  //     currentUser.friends.add(user.uid);
  //     if (mounted)
  //       setState(() {
  //         currentUser.friends.add(user.uid);
  //         // _isFriend = true;
  //       });
  //   }).catchError((error) {
  //     this.showInSnackBar('$error');
  //   });
  // }

  // _removeFriend(User user) async {
  //   print(user.name);
  //   print(user.uid);
  //   await _db
  //       .collection('users')
  //       .document(currentUser.uid)
  //       .collection('friends')
  //       .document(user.uid)
  //       .delete();
  //   currentUser.friends.remove(user.uid);
  //   this.showInSnackBar(
  //       '${user.name} foi removido da sua rede de relacionamentos.');
  //   if (mounted) {
  //     setState(() {
  //       currentUser.friends.remove(user.uid);
  //       // _isFriend = false;
  //     });
  //   }
  // }
}
