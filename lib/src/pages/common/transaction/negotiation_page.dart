import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/pages/common/product/product_page.dart';
import 'package:comoo/src/pages/common/transaction/transaction_view.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:flutter/services.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:comoo/src/pages/common/chat/direct_chat.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:url_launcher/url_launcher.dart';

class NegotiationPage extends StatefulWidget {
  final Negotiation negotiation;
  final bool goToChat;

  NegotiationPage(this.negotiation, {this.goToChat});

  @override
  State createState() => _NegotiationPageState();
}

class _NegotiationPageState extends State<NegotiationPage> with Reusables {
  static final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>();
  final Firestore _db = Firestore.instance;
  Negotiation negotiation;
  bool isCurrentUserBuyer = false;
  User _buyer;
  User _seller;
  bool _isLoading = false;

  //Fake values
  // double _payment_tax = 1.48;
  // double _total = 0.0;

  // Widget _buildNegotiationCard() {
  //   return ListTile(
  //     leading: Image(
  //       image: AdvancedNetworkImage(negotiation.thumb),
  //       fit: BoxFit.cover,
  //       width: 40.0,
  //       height: 40.0,
  //     ),
  //     title: RichText(
  //       text: TextSpan(
  //         text: negotiation.title + ' ',
  //         style: ComooStyles.texto,
  //         children: <TextSpan>[
  //           TextSpan(
  //               text: timeago.format(negotiation.date, locale: 'pt_BR'),
  //               style: ComooStyles.subTitle),
  //         ],
  //       ),
  //     ),
  //     subtitle: Text(negotiation.lastMessage ?? ''),
  //     trailing: ClipRect(
  //       child: Container(
  //           // child: Text(_status(negotiation), style: ComooStyles.statusNegociacaoNotificacoes,),
  //           ),
  //     ),
  //     onTap: () {
  //       Navigator.of(context).push(new MaterialPageRoute(
  //           builder: (context) => DirectChatView(negotiation)));
  //     },
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text(
          isCurrentUserBuyer ? 'Detalhe da compra' : 'minhas vendas',
          style: ComooStyles.appBarTitleWhite,
        ),
        actions: <Widget>[
          negotiation.status == 'under negotiation'
              ? isCurrentUserBuyer
                  ? FlatButton(
                      onPressed: _handlePay,
                      child: Text(
                        'COMPRAR',
                        style: ComooStyles.appBar_botao_branco,
                      ),
                    )
                  : Container()
              : Container(),
          // FlatButton(
          //   onPressed: () {
          //     print(negotiation.id);
          //     // print(negotiation.bankSlipCode);
          //     // print(negotiation.bankSlipLink);
          //     print(negotiation.orderId);
          //     print(negotiation.paymentId);
          //     print(negotiation.paymentDescriptor is BankSlipPayment);
          //     print(negotiation.paymentDescriptor is CreditCardPayment);
          //   },
          //   child: Text(
          //     'DEBUG',
          //     style: TextStyle(
          //       color: Colors.red,
          //     ),
          //   ),
          // ),
        ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView(
              children: <Widget>[
                itemContainer(context),
                Divider(
                  height: 4.0,
                  color: Colors.black87,
                ),
                sellerContainer(context),
                productValue(context),
                paymentMethod(context),
                totalPrice(context),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
                  child: negotiation.orderId != null ||
                          (negotiation.paymentDescriptor != null &&
                              isCurrentUserBuyer)
                      ? Divider(
                          height: 4.0,
                          color: Colors.black54,
                        )
                      : Container(),
                ),
                negotiation.orderId != null
                    ? Container(
                        margin: EdgeInsets.symmetric(
                          vertical: mediaQuery * 5.0,
                        ),
                        padding: EdgeInsets.only(
                          left: mediaQuery * 20.0,
                        ),
                        child: Text(
                            'Código da transação: ${negotiation.orderId}',
                            style: TextStyle(color: Colors.grey)),
                      )
                    : Container(),
                negotiation.status == 'paid'
                    ? confirmReceivementButton(context)
                    : Container(),
                negotiation.paymentDescriptor != null && isCurrentUserBuyer
                    ? negotiation.paymentDescriptor is BankSlipPayment
                        ? _buildBankSlipOptions()
                        : Container()
                    : Container(),
                buildButtons(),
              ],
            ),
    );
  }

  _buildBankSlipOptions() {
    var mediaQuery = MediaQuery.of(context).size.width / 400;
    // return Container(
    //   margin: EdgeInsets.symmetric(
    //     vertical: mediaQuery * 20.0,
    //   ),
    //   padding: EdgeInsets.symmetric(
    //     horizontal: mediaQuery * 100.0,
    //   ),
    //   child: OutlineButton(
    //     // padding: EdgeInsets.symmetric(
    //     //   vertical: mediaQuery * 5.0,
    //     //   horizontal: mediaQuery * 10.0,
    //     // ),
    //     borderSide: BorderSide(color: ComooColors.chumbo),
    //     shape: RoundedRectangleBorder(
    //         borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
    //     onPressed: () {
    //       print((negotiation.paymentDescriptor as BankSlipPayment).bankSlipUrl);
    //       Navigator.of(context).push(new MaterialPageRoute(
    //         builder: (BuildContext context) =>
    //             BankSlipPage(negotiation: negotiation),
    //       ));
    //     },
    //     child: Text(
    //       'visualizar boleto',
    //       style: TextStyle(
    //         fontFamily: ComooStyles.fontFamily,
    //         color: ComooColors.chumbo,
    //         fontSize: mediaQuery * 12.0,
    //         fontWeight: FontWeight.w500,
    //       ),
    //     ),
    //   ),
    // );
    BankSlipPayment bankSlipPayment = widget.negotiation.paymentDescriptor;
    TextStyle sectionTitleStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 16.0,
      fontWeight: FontWeight.bold,
    );
    TextStyle sectionTextStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.cinzaMedio,
      fontSize: mediaQuery * 14.0,
      fontWeight: FontWeight.w500,
    );
    TextStyle sectionTextStyleBold = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w600,
    );
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: mediaQuery * 20.0,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(vertical: mediaQuery * 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Código de barras'.toUpperCase(),
                  style: sectionTitleStyle,
                ),
                Text(
                  'Copie e cole a linha digitável a seguir no site ou aplicativo do seu banco',
                  style: sectionTextStyle,
                ),
                Container(
                  height: mediaQuery * 50.0,
                  margin: EdgeInsets.symmetric(
                    vertical: mediaQuery * 5.0,
                  ),
                  decoration: BoxDecoration(
                    // color: Color.fromRGBO(211, 211, 211, 0.5),
                    border: Border.all(
                      width: mediaQuery * 1.2,
                      color: ComooColors.cinzaClaro,
                    ),
                    borderRadius: BorderRadius.circular(mediaQuery * 15.0),
                  ),
                  child: Center(
                    child: Text(
                      bankSlipPayment.bankSlipCode,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(
                    vertical: mediaQuery * 25.0,
                  ),
                  padding: EdgeInsets.symmetric(
                    horizontal: mediaQuery * 65.0,
                  ),
                  child: Container(
                    height: mediaQuery * 35.0,
                    child: RaisedButton(
                      color: ComooColors.turqueza,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(mediaQuery * 30.0),
                      ),
                      onPressed: () {
                        Clipboard.setData(new ClipboardData(
                            text: bankSlipPayment.bankSlipCode));
                        showCustomSnackbar(
                          scaffoldKey: _scaffoldKey,
                          content: Text(
                            'Código de barras copiado para área de transferência.',
                            style: ComooStyles.textoSnackBar,
                          ),
                        );
                      },
                      child: Text(
                        'COPIAR CÓDIGO',
                        style: TextStyle(
                          fontFamily: ComooStyles.fontFamily,
                          color: Colors.white,
                          fontSize: mediaQuery * 15.0,
                          fontWeight: FontWeight.w500,
                          letterSpacing: mediaQuery * 0.60,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Versão para impressão'.toUpperCase(),
                        style: sectionTitleStyle,
                      ),
                      RichText(
                        text: bankSlipPayment.date == null
                            ? TextSpan(
                                text:
                                    'Imprima e pague seu boleto bancário em qualquer agência até o vencimento.',
                                style: sectionTextStyle,
                              )
                            : TextSpan(
                                text:
                                    'Imprima e pague seu boleto bancário em qualquer agência até o vencimento (',
                                style: sectionTextStyle,
                                children: [
                                  TextSpan(
                                    text: '${bankSlipPayment.date}',
                                    style: sectionTextStyleBold,
                                  ),
                                  TextSpan(
                                    text: ').',
                                    style: sectionTextStyle,
                                  ),
                                ],
                              ),
                      ),
                      Text(
                        'Não imprima em modo rascunho',
                        style: sectionTextStyle,
                      ),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(
                    vertical: mediaQuery * 25.0,
                  ),
                  padding: EdgeInsets.symmetric(
                    horizontal: mediaQuery * 65.0,
                  ),
                  child: Container(
                    height: mediaQuery * 35.0,
                    child: RaisedButton(
                      color: ComooColors.turqueza,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(mediaQuery * 30.0),
                      ),
                      onPressed: () {
                        // showInSnackBar('Imprimir boleto');
                        launch(bankSlipPayment.bankSlipUrl);
                      },
                      child: Text(
                        'Imprimir Boleto'.toUpperCase(),
                        style: TextStyle(
                          fontFamily: ComooStyles.fontFamily,
                          color: Colors.white,
                          fontSize: mediaQuery * 15.0,
                          fontWeight: FontWeight.w500,
                          letterSpacing: mediaQuery * 0.60,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildButtons() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final DateTime now = DateTime.now();

    List<Widget> children = [];
    if (isCurrentUserBuyer) {
      children.add(Container(
        height: mediaQuery * 30.0,
        width: mediaQuery * 130.0,
        child: RaisedButton(
          // borderSide: BorderSide(color: ComooColors.turqueza),
          elevation: 0.0,
          color: ComooColors.turqueza,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
          // shape: RoundedRectangleBorder(
          //     borderRadius: BorderRadius.circular(14.0)),
          onPressed: negotiation.status == 'under negotiation'
              ? () {
                  cancellNegotiation();
                }
              : null,
          child: const Text(
            'LIBERAR',
            style: ComooStyles.botaoChat,
          ),
        ),
      ));
      children.add(Container(
        height: mediaQuery * 30.0,
        width: mediaQuery * 130.0,
        child: RaisedButton(
          // borderSide: BorderSide(color: ComooColors.turqueza),
          elevation: 0.0,
          color: ComooColors.turqueza,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
          // shape: RoundedRectangleBorder(
          //     borderRadius: BorderRadius.circular(14.0)),
          onPressed:
              negotiation.status == 'under negotiation' ? _handlePay : null,
          child: const Text(
            'COMPRAR',
            style: ComooStyles.botaoChat,
          ),
        ),
      ));
    } else {
      children.add(Container(
        height: mediaQuery * 30.0,
        child: RaisedButton(
          // borderSide: BorderSide(color: ComooColors.turqueza),
          elevation: 0.0,
          color: ComooColors.turqueza,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(mediaQuery * 14.0)),
          // shape: RoundedRectangleBorder(
          //     borderRadius: BorderRadius.circular(14.0)),
          onPressed: negotiation.status == 'under negotiation'
              ? now.difference(negotiation.date).inMilliseconds >= 86400000
                  ? () {
                      cancellNegotiation();
                    }
                  : null
              : null,
          child: const Text(
            'CANCELAR VENDA',
            style: ComooStyles.botaoChat,
          ),
        ),
      ));
    }
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: mediaQuery * 10.0,
        vertical: mediaQuery * 14.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: children.length > 1
            ? MainAxisAlignment.spaceBetween
            : MainAxisAlignment.center,
        children: children,
      ),
    );
  }

  Future<void> _handlePay() async {
    // Navigator.of(context).pop();
    await Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (context) => TransactionView(negotiation: negotiation),
      ),
    );
    await _fetchNegotiationInfo();
  }

  Future<void> cancellNegotiation() async {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    await showCustomDialog<bool>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Tem certeza que deseja cancelar essa transação?\n'
            'Essa ação não poderá ser desfeita.',
        textAlign: TextAlign.center,
        actions: [
          Container(
            height: mediaQuery * 25.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              // color: Colors.white,
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: Text(
                'SIM',
                style: TextStyle(
                  color: COMOO.colors.pink,
                ),
              ),
            ),
          ),
          Container(
            height: mediaQuery * 25.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              child: Text(
                'NÃO',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    ).then((result) {
      if (result ?? false) {
        setState(() {
          _isLoading = true;
        });
        negotiation.status = 'cancelled';
        return CloudFunctions.instance
            .getHttpsCallable(functionName: 'cancelNegotiation')
            .call(<String, dynamic>{'negotiationId': this.negotiation.id}).then(
                (_) {
          setState(() {
            _isLoading = false;
          });
          Navigator.pop(context);
        });
      }
      return result;
    });
  }

  Future<void> _fetchProduct() async {
    DocumentSnapshot snap = await negotiation.product.get();
    Product product = Product.fromSnapshot(snap);
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => ProductPage(product)));
  }

  Widget itemContainer(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: mediaQuery * 10.0,
      ),
      child: GestureDetector(
        onTap: () {
          _fetchProduct();
        },
        child: Container(
          padding: EdgeInsets.all(0.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                    left: mediaQuery * 10.0, right: mediaQuery * 10.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    width: mediaQuery * 60.0,
                    height: mediaQuery * 60.0,
                    decoration: ShapeDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AdvancedNetworkImage(negotiation.thumb,
                            useDiskCache: true),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(mediaQuery * 15.0)),
                      ),
                    ),
                  ),
                ),
              ),
              Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: mediaQuery * 5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          // alignment: Alignment.topCenter,
                          margin: EdgeInsets.only(bottom: mediaQuery * 5.0),
                          child: Text(
                            negotiation.title,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: mediaQuery * 15,
                                color: ComooColors.turqueza),
                          ),
                        ),
                        // SizedBox(height: mediaQuery * 15.0),
                        saleStatus(context)
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget saleStatus(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      // width: mediaQuery * 250.0,
      constraints: BoxConstraints(maxWidth: mediaQuery * 300.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            negotiation.statusName.toUpperCase(),
            style: TextStyle(
              fontSize: mediaQuery * 14.0,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              // left: mediaQuery * 40.0,
              right: mediaQuery * 1.0,
            ),
            child: Text(negotiation.date.day.toString() +
                '/' +
                negotiation.date.month.toString() +
                '/' +
                negotiation.date.year.toString()),
          ),
        ],
      ),
    );
  }

  Widget sellerContainer(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(
            vertical: mediaQuery * 10.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: mediaQuery * 12.5),
                    child: Container(
                      width: mediaQuery * 50.0,
                      height: mediaQuery * 50.0,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AdvancedNetworkImage(
                                isCurrentUserBuyer
                                    ? _seller.photoURL
                                    : _buyer.photoURL,
                                useDiskCache: true),
                          ),
                          shape: BoxShape.circle),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: mediaQuery * 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          isCurrentUserBuyer ? 'Vendedor(a):' : 'Comprador(a):',
                          style: TextStyle(
                            fontFamily: ComooStyles.fontFamily,
                            color: ComooColors.cinzaMedio,
                          ),
                        ),
                        SizedBox(height: mediaQuery * 5),
                        Container(
                          constraints: BoxConstraints(
                            maxWidth: mediaQuery * 250.0,
                          ),
                          child: Text(
                            isCurrentUserBuyer ? _seller.name : _buyer.name,
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(right: mediaQuery * 20.0),
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (context) => DirectChatView(negotiation)));
                  },
                  icon: Icon(
                    COMOO.icons.chat.iconData,
                    color: COMOO.colors.lead,
                    size: 32.0,
                    textDirection: TextDirection.rtl,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
          child: Divider(
            height: 4.0,
            color: Colors.black54,
          ),
        ),
      ],
    );
  }

  Widget productValue(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(
            vertical: mediaQuery * 10.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: mediaQuery * 20.0),
                child: Text(
                  isCurrentUserBuyer ? 'Valor do produto' : 'Valor anunciado:',
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: ComooColors.chumbo,
                    fontSize: mediaQuery * 14.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: mediaQuery * 20.0),
                child: Text('R\$ ${negotiation.price.toStringAsFixed(2)}',
                    style: TextStyle(
                      fontFamily: ComooStyles.fontFamily,
                      color: ComooColors.chumbo,
                      fontSize: mediaQuery * 14.0,
                      fontWeight: FontWeight.w600,
                    )),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
          child: Divider(
            height: 4.0,
            color: Colors.black54,
          ),
        ),
      ],
    );
  }

  Widget paymentMethod(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    TextStyle titleBold = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 14.0,
      fontWeight: FontWeight.w600,
    );
    TextStyle subTitle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 14.0,
      fontWeight: FontWeight.w500,
    );
    if (negotiation.paymentDescriptor != null) {
      // _total = negotiation.paymentDescriptor.total;
      String paymentType;
      if (negotiation.paymentDescriptor is BankSlipPayment) {
        paymentType = 'Boleto Bancário';
        BankSlipPayment bankSlipPayment = negotiation.paymentDescriptor;
        return Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                vertical: mediaQuery * 10.0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: mediaQuery * 20.0),
                        child: Text(
                          'Forma de pagamento:',
                          style: titleBold,
                        ),
                      ),
                      isCurrentUserBuyer
                          ? Container()
                          : Padding(
                              padding:
                                  EdgeInsets.only(right: mediaQuery * 20.0),
                              child: Text(
                                'R\$ ${bankSlipPayment.tax}',
                                style: subTitle,
                              ),
                            ),
                    ],
                  ),
                  SizedBox(height: mediaQuery * 5),
                  Padding(
                    padding: EdgeInsets.only(left: mediaQuery * 20.0),
                    child: Text(
                      '$paymentType', // (+ R\$ ${bankSlipPayment.tax})',
                      style: subTitle,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
              child: Divider(
                height: 4.0,
                color: Colors.black54,
              ),
            ),
          ],
        );
      } else if (negotiation.paymentDescriptor is CreditCardPayment) {
        paymentType = 'Cartão de Crédito';
        CreditCardPayment creditCardPayment = negotiation.paymentDescriptor;
        return Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                vertical: mediaQuery * 10.0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: mediaQuery * 20.0),
                        child: Text(
                          'Forma de pagamento:',
                          style: titleBold,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: mediaQuery * 20.0),
                        child: Text(
                          'R\$ ${creditCardPayment.total.toStringAsFixed(2)}',
                          style: titleBold,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: mediaQuery * 5.0),
                    padding: EdgeInsets.only(left: mediaQuery * 20.0),
                    child: Text(
                      '$paymentType (${creditCardPayment.installmentAmount}x R\$ ${creditCardPayment.portion.toStringAsFixed(2)})',
                      style: subTitle,
                    ),
                  ),
                  isCurrentUserBuyer
                      ? Container(
                          margin: EdgeInsets.only(top: mediaQuery * 5.0),
                          padding: EdgeInsets.only(left: mediaQuery * 20.0),
                          child: Text(
                            '${creditCardPayment.brand} ****${creditCardPayment.number}',
                            style: subTitle,
                          ),
                        )
                      : Container(),
                  isCurrentUserBuyer
                      ? Container(
                          margin: EdgeInsets.only(top: mediaQuery * 5.0),
                          padding: EdgeInsets.only(left: mediaQuery * 20.0),
                          child: Text(
                            '${creditCardPayment.name}',
                            style: subTitle,
                          ),
                        )
                      : Container(),
                  isCurrentUserBuyer
                      ? Container(
                          margin: EdgeInsets.only(top: mediaQuery * 5.0),
                          padding: EdgeInsets.only(left: mediaQuery * 20.0),
                          child: Text(
                            'Validade: ${creditCardPayment.date}',
                            style: subTitle,
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
              child: Divider(
                height: 4.0,
                color: Colors.black54,
              ),
            ),
          ],
        );
      } else {
        return Container();
      }
    }
    return Container();
  }

  Widget totalPrice(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    if (negotiation.paymentDescriptor != null) {
      final double total = isCurrentUserBuyer
          ? negotiation.paymentDescriptor.total
          : negotiation.paymentDescriptor.sellerTotal;
      return Container(
        margin: EdgeInsets.symmetric(
          vertical: mediaQuery * 20.0,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                    left: mediaQuery * 20.0,
                  ),
                  child: Text(
                    isCurrentUserBuyer ? 'VALOR TOTAL' : 'VALOR A RECEBER',
                    style: TextStyle(
                      fontSize: mediaQuery * 18.0,
                      fontWeight: FontWeight.w600,
                      color: ComooColors.turqueza,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(right: mediaQuery * 20),
                  child: Text(
                    'R\$ ${total.toStringAsFixed(2)}',
                    style: TextStyle(
                      fontSize: mediaQuery * 18.0,
                      fontWeight: FontWeight.w600,
                      color: ComooColors.turqueza,
                    ),
                  ),
                ),
              ],
            ),
            isCurrentUserBuyer
                ? Container()
                : negotiation.paymentDescriptor.releaseDate == null
                    ? Container(
                        margin: EdgeInsets.only(
                          top: mediaQuery * 5.0,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                left: mediaQuery * 20.0,
                              ),
                              child: Text(
                                'Aguardando pagamento',
                                style: TextStyle(
                                  fontFamily: ComooStyles.fontFamily,
                                  color: ComooColors.chumbo,
                                  fontSize: mediaQuery * 13.0,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : Container(
                        margin: EdgeInsets.only(
                          top: mediaQuery * 5.0,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                left: mediaQuery * 20.0,
                              ),
                              child: Text(
                                'Data da liberação do pagamento:',
                                style: TextStyle(
                                  fontFamily: ComooStyles.fontFamily,
                                  color: ComooColors.chumbo,
                                  fontSize: mediaQuery * 13.0,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                right: mediaQuery * 20,
                              ),
                              child: Text(
                                negotiation.paymentDescriptor.releaseDate ??
                                    'dd/mm/aaa',
                                style: TextStyle(
                                  fontFamily: ComooStyles.fontFamily,
                                  color: ComooColors.chumbo,
                                  fontSize: mediaQuery * 13.0,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
            // isCurrentUserBuyer
            //     ? Container()
            //     : negotiation.paymentDescriptor.releaseDate == null
            //         ? Container()
            //         : Container(
            //             margin: EdgeInsets.only(
            //               top: mediaQuery * 5.0,
            //             ),
            //             child: GestureDetector(
            //               onTap: () {
            //                 Navigator.of(context).push(
            //                   MaterialPageRoute(
            //                     builder: (context) => AnticipatePaymentPage(),
            //                   ),
            //                 );
            //               },
            //               child: Row(
            //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //                 children: <Widget>[
            //                   Container(
            //                     padding: EdgeInsets.only(
            //                       left: mediaQuery * 20.0,
            //                     ),
            //                     child: Text(
            //                       'Deseja antecipar seu recebimento?',
            //                       style: TextStyle(
            //                         fontFamily: ComooStyles.fontFamily,
            //                         color: ComooColors.cinzaMedio,
            //                         fontSize: mediaQuery * 13.0,
            //                         fontWeight: FontWeight.w600,
            //                       ),
            //                     ),
            //                   ),
            //                 ],
            //               ),
            //             ),
            //           ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  Widget confirmReceivementButton(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: mediaQuery * 25.0,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: mediaQuery * 65.0,
      ),
      child: Container(
        height: mediaQuery * 35.0,
        child: RaisedButton(
          color: ComooColors.turqueza,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(mediaQuery * 30.0),
          ),
          onPressed: () {
            if (isCurrentUserBuyer) {
              _finishNegotiation();
            } else {
              _sendDeliveryNotification();
            }
          },
          child: Text(
            isCurrentUserBuyer ? 'CONFIRMAR RECEBIMENTO' : 'CONFIRMAR ENTREGA',
            style: TextStyle(
              fontFamily: ComooStyles.fontFamily,
              color: Colors.white,
              fontSize: mediaQuery * 15.0,
              fontWeight: FontWeight.w500,
              letterSpacing: mediaQuery * 0.60,
            ),
          ),
        ),
      ),
    );
  }

  // String _status(Negotiation negotiation) {
  //   switch (negotiation.status) {
  //     case 'under negotiation':
  //       return 'AGUARDANDO PAGAMENTO';
  //     case 'pending':
  //       return 'PROCESSANDO PAGAMENTO';
  //     case 'cancelled':
  //       return 'CANCELADA';
  //     case 'paid':
  //       return 'PAGO';
  //     case 'sent':
  //       return 'ENVIADO';
  //     case 'finished':
  //       return 'FINALIZADO';
  //     case 'expired':
  //       return 'VENCIDO';
  //     default:
  //       return negotiation.status;
  //   }
  // }

  Future<void> _fetchNegotiationInfo() async {
    await _db
        .collection('negotiations')
        .document(negotiation.id)
        .get()
        .then((snap) {
      if (mounted) {
        setState(() {
          negotiation = Negotiation.fromSnapshot(snap);
        });
      }
    });
  }

  Future<void> _fetchInfo() async {
    isCurrentUserBuyer = currentUser.uid == negotiation.buyer.documentID;
    DocumentSnapshot snap;
    if (isCurrentUserBuyer) {
      snap = await _db
          .collection('users')
          .document(negotiation.seller.documentID)
          .get();
    } else {
      snap = await _db
          .collection('users')
          .document(negotiation.buyer.documentID)
          .get();
    }

    setState(() {
      if (isCurrentUserBuyer) {
        _buyer = currentUser;
        _seller = User.fromSnapshot(snap);
      } else {
        _seller = currentUser;
        _buyer = User.fromSnapshot(snap);
      }
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    this.negotiation = this.widget.negotiation;
    _isLoading = true;
    _fetchNegotiationInfo();
    _fetchInfo();
  }

  // Future<void> _checkNavigation() async {
  //   if (this.widget.goToChat != null) {
  //     if (this.widget.goToChat) {
  //       print('>> negotiation page => direct chat');
  //       final result = await Navigator.of(context).push(new MaterialPageRoute(
  //           builder: (context) => DirectChatView(negotiation)));
  //       print(result);
  //       if (result != null) {
  //         if (result) {
  //           print('<< popping negotiation page');
  //           Navigator.pop(context);
  //         }
  //       }
  //     }
  //   }
  // }

  Future<void> _sendDeliveryNotification() async {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final bool result = await showCustomDialog<bool>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title:
            'Você está prestes a infomar ao cliente que já enviou o produto, tem certeza disso?',
        actions: [
          Container(
            margin: EdgeInsets.symmetric(
              // horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'CANCELAR',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(false),
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(
              // horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'CONFIRMAR',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(true),
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
        ],
      ),
    );

    if (result == true) {
      //SEND
      setState(() {
        _isLoading = true;
      });
      negotiation.status = 'sent';
      _db.collection('negotiations').document(negotiation.id).updateData(
          {'status': 'sent', 'deliveryDate': DateTime.now()}).then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
  }

  Future<void> _finishNegotiation() async {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final bool result = await showCustomDialog<bool>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title:
            'Ao confirmar, você informa que recebeu o produto e a transação será concluída.',
        actions: [
          Container(
            margin: EdgeInsets.symmetric(
              // horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'CANCELAR',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(false),
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(
              // horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'CONFIRMAR',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(true),
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          )
        ],
      ),
    );
    if (result == true) {
      setState(() {
        _isLoading = true;
      });
      negotiation.status = 'finished';
      _db.collection('negotiations').document(negotiation.id).updateData(
          {'status': 'finished', 'receivedDate': DateTime.now()}).then((_) {
        setState(() {
          negotiation.status = 'finished';
          _isLoading = false;
        });
      });
    }
  }
}
