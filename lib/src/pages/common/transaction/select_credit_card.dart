import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/blocs/credit_card_bloc.dart';
import 'package:comoo/src/blocs/select_cc_bloc.dart';
import 'package:comoo/src/comoo/credit_card.dart';
import 'package:comoo/src/pages/common/transaction/create_credit_card.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/provider/provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';

class CreditCardSelection extends StatelessWidget {
  final CreditCardBloc creditCardBloc = CreditCardBloc();
//   @override
//   State<StatefulWidget> createState() => _CreditCardSelectionState();
// }

// class _CreditCardSelectionState extends State<CreditCardSelection> {
//   final List<CreditCard> cards = <CreditCard>[];

  // Future<void> _fetchData() async {
  //   final CollectionReference colRef = Firestore.instance
  //       .collection('users')
  //       .document(currentUser.uid)
  //       .collection('credit_card');
  //   try {
  //     final QuerySnapshot query = await colRef.getDocuments();

  //     for (DocumentSnapshot snap in query.documents) {
  //       final CreditCard card = CreditCard.fromSnapshot(snap);
  //       card.number =
  //           card.number.substring(card.number.length - 4, card.number.length);
  //       if (mounted) {
  //         setState(() {
  //           cards.add(card);
  //         });
  //       }
  //     }
  //   } catch (e) {
  //     print(e);
  //     _fetchData();
  //   }
  // }

  // @override
  // void initState() {
  //   super.initState();
  //   _fetchData();
  // }

  // @override
  // void dispose() {
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    SelectCreditCardBloc bloc = Provider.of(context).selectCreditCard;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: Navigator.of(context).pop,
        ),
        title: Text(
          'seus cartões',
          style: TextStyle(
            fontFamily: COMOO.fonts.montSerrat,
            color: Colors.white,
            fontSize: mediaQuery * 15.0,
            fontWeight: FontWeight.w500,
            letterSpacing: mediaQuery * -0.125,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          StreamBuilder<bool>(
              initialData: false,
              stream: bloc.hasData,
              builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                return FlatButton(
                  onPressed: snapshot.data
                      ? () {
                          _save(context, bloc);
                        }
                      : null,
                  child: Text(
                    'SALVAR',
                    style: TextStyle(
                      fontFamily: COMOO.fonts.montSerrat,
                      fontSize: mediaQuery * 13.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      letterSpacing: mediaQuery * -0.108,
                    ),
                  ),
                );
              }),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: mediaQuery * 36.0,
              vertical: mediaQuery * 39.0,
            ),
            child: RaisedButton(
              color: Color(0xFF461D58),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
              elevation: 0.0,
              onPressed: () {
                _addCard(context);
              },
              child: FittedBox(
                child: Text(
                  'ADICIONAR CARTÃO DE CRÉDITO',
                  style: ComooStyles.botao_branco,
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: mediaQuery * 18.0,
              bottom: mediaQuery * 10.0,
            ),
            child: Text(
              'Ou selecione um cartão cadastrado:',
              style: TextStyle(
                fontFamily: COMOO.fonts.montSerrat,
                fontSize: mediaQuery * 15.0,
                color: Color(0xFF3C3746),
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          StreamBuilder<List<DocumentSnapshot>>(
            stream: bloc.docs,
            builder: (BuildContext context,
                AsyncSnapshot<List<DocumentSnapshot>> snapshot) {
              return snapshot.hasData
                  ? snapshot.data.isEmpty
                      ? Container()
                      : buildCreditCards(context, bloc, snapshot.data)
                  : Center(child: CircularProgressIndicator());
            },
          ),
        ],
      ),
    );
  }

  Widget buildCreditCards(BuildContext context, SelectCreditCardBloc bloc,
      List<DocumentSnapshot> cards) {
    final double mediaQuery = 1.02; //MediaQuery.of(context).size.width / 400;
    return Column(
      children: cards.map<Widget>((DocumentSnapshot snap) {
        final CreditCard card = CreditCard.fromSnapshot(snap);
        if (card.isDefault) {
          bloc.changeCard(card);
        }
        return CardTile(
          leading: IconButton(
            icon: card.isDefault
                ? Icon(Icons.check_circle)
                : Icon(Icons.check_circle_outline),
            color: COMOO.colors.turquoise,
            iconSize: mediaQuery * 22.0,
            onPressed: () {
              _setDefault(card);
            },
          ),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '${card.brand} ****${(card.number).substring(card.number.length - 4, card.number.length)}',
                style: TextStyle(
                  fontFamily: COMOO.fonts.montSerrat,
                  fontSize: mediaQuery * 14.0,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF3C3746),
                  height: mediaQuery * 1.2,
                ),
              ),
              Text(
                '${card.name}\nValidade ${card.month}/${card.year}',
                style: TextStyle(
                  fontFamily: COMOO.fonts.montSerrat,
                  fontSize: mediaQuery * 14.0,
                  fontWeight: FontWeight.normal,
                  color: Color(0xFF3C3746),
                  height: mediaQuery * 1.2,
                ),
              ),
            ],
          ),
          trailing: IconButton(
            padding: EdgeInsets.all(0),
            icon: FittedBox(
              child: Text(
                'EXCLUIR',
                style: TextStyle(
                  fontFamily: COMOO.fonts.montSerrat,
                  fontSize: mediaQuery * 11.0,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFF868693),
                ),
              ),
            ),
            onPressed: () {
              _deleteCard(card);
            },
          ),
          // trailing: Text.rich(
          //   TextSpan(
          //       text: 'EXCLUIR',
          //       style: TextStyle(
          //         fontFamily: COMOO.fonts.montSerrat,
          //         fontSize: mediaQuery * 11.0,
          //         fontWeight: FontWeight.w600,
          //         color: Color(0xFF868693),
          //       ),
          //       recognizer: TapGestureRecognizer()
          //         ..onTap = () {
          //           _deleteCard(card);
          //         }),
          // ),
        );
      }).toList(),
    );
  }

  void _save(BuildContext context, SelectCreditCardBloc bloc) {
    Navigator.of(context).pop(bloc.cardValue);
  }

  void _setDefault(CreditCard card) {
    try {
      CloudFunctions.instance
          .getHttpsCallable(functionName: 'setDefaultCard')
          .call(
        <String, String>{'cardId': card.uid},
      ).timeout(const Duration(seconds: 60));
    } on CloudFunctionsException catch (e) {
      print('>> setDefaultCard error => $e');
    } catch (e) {
      print(e);
    }
  }

  Future<void> _addCard(BuildContext context) async {
    final CreditCard card =
        await Navigator.of(context).push(MaterialPageRoute<CreditCard>(
      builder: (BuildContext context) => BlocProvider<CreditCardBloc>(
        bloc: creditCardBloc,
        child: CreateCreditCard(),
      ),
    ));
    if (card != null) {
      Navigator.of(context).pop(card);
    }
  }

  Future<void> _deleteCard(CreditCard card) async {
    await CloudFunctions.instance
        .getHttpsCallable(functionName: 'removeCreditCard')
        .call(<String, dynamic>{
          'uid': card.uid,
          'id': card.apiID,
        })
        .timeout(Duration(minutes: 1))
        .then((result) {
          // showInSnackBar('Cartão removido com sucesso!');
        })
        .catchError((onError) {
          if (onError is CloudFunctionsException) {
            print(onError.code);
            print(onError.details);
            print(onError.message);
          }
          // showInSnackBar('Erro ao remover o cartão.');
        });
  }
}

class CardTile extends StatelessWidget {
  CardTile({
    @required this.leading,
    @required this.title,
    this.trailing,
    this.hasDivider = false,
  });

  final Widget leading;
  final Widget title;
  final Widget trailing;
  final bool hasDivider;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 18.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              leading,
              title,
            ],
          ),
          trailing ?? Container(),
        ],
      ),
    );
  }
}

/*
CreditCard selectedCreditCard;

class CreditCardSelection extends StatefulWidget {
  CreditCardSelection();

  @override
  _CreditCardSelectionState createState() => _CreditCardSelectionState();
}

class _CreditCardSelectionState extends State<CreditCardSelection> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final Firestore _db = Firestore.instance;

  List<CreditCard> cardList;
  // bool _isLoading = false;

  Future<void> fetchCreditCard() async {
    setState(() {
      // _isLoading = true;
    });
    CreditCard card;
    cardList.clear();
    await _db
        .collection('users')
        .document(currentUser.uid)
        .collection('credit_card')
        .getDocuments()
        .then((QuerySnapshot querySnapshot) {
      print('Has card? ${querySnapshot.documents.isNotEmpty}');
      if (querySnapshot.documents.isNotEmpty) {
        querySnapshot.documents.forEach((snap) {
          card = CreditCard.fromSnapshot(snap);
          card.number =
              card.number.substring(card.number.length - 4, card.number.length);
          cardList.add(card);
          print('Card: ${card.uid}');
          if (card.isDefault) {
            if (this.mounted) {
              setState(() {
                // selectedCreditCard = card;
                selectCard(card.uid);
                // _isLoading = false;
                card.isSelected = true;
              });
            }
            return;
          }
        });
        // if (!_isCreditCard) {
        //   setState(() {
        //     defaultCard = card;
        //     _isCreditCard = true;
        //     _isLoading = false;
        //   });
        // }
      }
    });
  }

  getCardBrandPath(String brand) {
    print(brand);
    switch (brand) {
      case 'MASTERCARD':
        return 'images/master.png';
      case 'VISA':
        return 'images/visa.png';
      case 'AMEX':
        return 'images/amex.png';
      case 'HIPERCARD':
        return 'images/hipercard.png';
    }
  }

  selectCard(String uid) {
    cardList.forEach((card) {
      _scaffoldKey.currentState.setState(() {
        if (card.uid == uid) {
          card.isSelected = true;
          selectedCreditCard = card;
        } else {
          card.isSelected = false;
        }
      });
    });
  }

  Future<void> addCard() async {
    var cardNumber = await Navigator.of(context).push(MaterialPageRoute(
        fullscreenDialog: true,
        builder: (BuildContext context) => new CreatePaymentMethod()));

    print(cardNumber);
    if (cardNumber != null) {
      await fetchCreditCard();
      setState(() {
        // _isLoading = false;
      });
    }
  }

  removeCard(CreditCard card) async {
    await CloudFunctions.instance
        .call(functionName: 'removeCreditCard', parameters: {
          'uid': card.uid,
          'id': card.apiID,
        })
        .timeout(Duration(minutes: 1))
        .then((result) {
          showInSnackBar('Cartão removido com sucesso!');
        })
        .catchError((onError) {
          if (onError is CloudFunctionsException) {
            print(onError.code);
            print(onError.details);
            print(onError.message);
          }
          // showInSnackBar('Erro ao remover o cartão.');
        });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    List<Widget> cards = new List();
    cards.add(Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.symmetric(
          horizontal: mediaQuery * 15.0, vertical: mediaQuery * 20.0),
      child: Text(
        'Selecione o cartão:',
        style: TextStyle(
          color: ComooColors.cinzaMedio,
        ),
      ),
    ));
    for (int index = 0; index < cardList.length; index++) {
      CreditCard card = cardList[index];
      cards.add(Dismissible(
        key: Key(card.uid),
        background: Container(
          alignment: Alignment.centerRight,
          color: Colors.redAccent,
          padding: EdgeInsets.only(right: mediaQuery * 20.0),
          child: Icon(
            Icons.delete,
            color: Colors.black,
          ),
        ),
        direction: DismissDirection.endToStart,
        onDismissed: (DismissDirection direction) {
          _scaffoldKey.currentState.setState(() {
            print('remove card');
            removeCard(card);
          });
        },
        child: ListTile(
          selected: card.isSelected ?? false,
          onTap: () {
            print('selected ${card.uid}');
            setState(() {
              selectCard(card.uid);
            });
            // Navigator.pop(context, card);
          },
          // leading: Text(
          //   '${card.brand}',
          // ),
          leading: new Image.asset(
            getCardBrandPath(card.brand),
            // 'images/master.png',
            width: mediaQuery * 40.0,
            height: mediaQuery * 40.0,
          ),
          title: Text(
            '**** ${card.number}',
          ),
          trailing: card.isSelected
              ? Icon(
                  Icons.check,
                  color: Colors.greenAccent,
                )
              : null,
        ),
      ));
    }
    cards.add(Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: mediaQuery * 10.0),
        child: GestureDetector(
          onTap: () {
            print('add new card');
            addCard();
          },
          child: Text(
            '+ Adicionar cartão de crédito',
            style: ComooStyles.texto_bold,
          ),
        ),
      ),
    ));
    return Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        iconTheme: new IconThemeData(color: ComooColors.white500),
        backgroundColor: ComooColors.chumbo,
        centerTitle: true,
        // leading: IconButton(
        //   icon: Icon(Icons.close),
        //   onPressed: () {
        //     Navigator.of(context).pop(selectedCard.number);
        //   },
        // ),
        title: Text(
          'Opções de Pagamento',
          style: ComooStyles.appBar_botao_branco,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            // mainAxisSize: MainAxisSize.min,
            children: cards,
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(value, style: ComooStyles.textoSnackBar)));
  }
}

*/
