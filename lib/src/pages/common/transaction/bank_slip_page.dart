// import 'dart:io';

import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class BankSlipPage extends StatefulWidget {
  final Negotiation negotiation;
  BankSlipPage({this.negotiation});
  @override
  _BankSlipPageState createState() => _BankSlipPageState();
}

class _BankSlipPageState extends State<BankSlipPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  // String _purchaseID;
  // String _productName;
  // double _price;
  // String _barCode;
  // String _bankSlipUrl;
  BankSlipPayment bankSlipPayment;
  Product product;

  DateTime _expirationDate;

  @override
  void initState() {
    super.initState();

    bankSlipPayment = widget.negotiation.paymentDescriptor;
    // _purchaseID = widget.negotiation.paymentId;
    // _productName = '';
    // _price = widget.negotiation.price;
    // _barCode = widget.negotiation.bankSlipCode;
    // _bankSlipUrl = widget.negotiation.bankSlipLink;
    _expirationDate = DateTime(2018, 12, 10);
    _fetchProduct();
  }

  Future<void> _fetchProduct() async {
    widget.negotiation.product.get().then((snap) {
      setState(() {
        product = Product.fromSnapshot(snap);
      });
    });
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(
        new SnackBar(content: Text(value, style: ComooStyles.textoSnackBar)));
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    var formater = NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR');
    TextStyle sectionTitleStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 16.0,
      fontWeight: FontWeight.bold,
    );
    TextStyle sectionTextStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.cinzaMedio,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w500,
    );
    TextStyle sectionTextStyleBold = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w600,
    );
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text('Pedido de compra ${widget.negotiation.orderId ?? ''}',
            style: ComooStyles.appBarTitleSixteen),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: mediaQuery * 14.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //Row title
            Container(
              padding: EdgeInsets.only(top: mediaQuery * 20.0),
              child: Text(
                'Detalhes do pedido:',
                style: sectionTitleStyle,
              ),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              title: Text(
                product?.title ?? '',
                style: sectionTextStyle,
              ),
              trailing: Text(
                formater.format(widget.negotiation.price),
                style: sectionTextStyleBold,
              ),
            ),
            Container(
              width: double.infinity,
              color: Color.fromRGBO(211, 211, 211, 0.5),
              child: Column(
                children: <Widget>[
                  ListTile(
                    contentPadding: EdgeInsets.all(0.0),
                    title: Container(
                      padding: EdgeInsets.only(right: mediaQuery * 30.0),
                      alignment: Alignment.centerRight,
                      child: Text(
                        'Subtotal',
                        style: sectionTextStyle,
                      ),
                    ),
                    trailing: Text(
                      formater.format(widget.negotiation.price),
                      style: sectionTextStyle,
                    ),
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.all(0.0),
                    title: Container(
                      padding: EdgeInsets.only(right: mediaQuery * 30.0),
                      alignment: Alignment.centerRight,
                      child: Text(
                        'Taxa boleto',
                        style: sectionTextStyle,
                      ),
                    ),
                    trailing: Text(
                      formater.format(bankSlipPayment.tax),
                      style: sectionTextStyle,
                    ),
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.all(0.0),
                    title: Container(
                      padding: EdgeInsets.only(right: mediaQuery * 30.0),
                      alignment: Alignment.centerRight,
                      child: Text(
                        'Total desta compra',
                        style: sectionTextStyleBold,
                      ),
                    ),
                    trailing: Text(
                      formater.format(bankSlipPayment.total),
                      style: sectionTextStyle.apply(color: Colors.redAccent),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(vertical: mediaQuery * 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Código de barras',
                    style: sectionTitleStyle,
                  ),
                  Text(
                    'Copie e cole a linha digitável a seguir no site ou aplicativo do seu banco',
                    style: sectionTextStyle,
                  ),
                  Container(
                    height: mediaQuery * 50.0,
                    margin: EdgeInsets.symmetric(
                      vertical: mediaQuery * 5.0,
                    ),
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(211, 211, 211, 0.5),
                      border: Border.all(
                        width: mediaQuery * 1.2,
                        color: ComooColors.cinzaClaro,
                      ),
                      borderRadius: BorderRadius.circular(mediaQuery * 5.0),
                    ),
                    child: Center(
                      child: Text(
                        bankSlipPayment.bankSlipCode,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Clipboard.setData(new ClipboardData(
                          text: bankSlipPayment.bankSlipCode));
                      showInSnackBar(
                          'Código de barras copiado para área de transferência.');
                    },
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Text(
                        'Copiar código',
                        style: TextStyle(
                          fontFamily: ComooStyles.fontFamily,
                          color: Colors.lightBlue,
                          fontSize: mediaQuery * 18.0,
                          letterSpacing: mediaQuery * 0.25,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Versão para impressão:',
                    style: sectionTitleStyle,
                  ),
                  RichText(
                    text: TextSpan(
                      text:
                          'Imprima e pague seu boleto bancário em qualquer agência até o vencimento (',
                      style: sectionTextStyle,
                      children: [
                        TextSpan(
                          text:
                              '${_expirationDate.day}/${_expirationDate.month}/${_expirationDate.year}',
                          style: sectionTextStyleBold,
                        ),
                        TextSpan(
                          text: ').',
                          style: sectionTextStyle,
                        ),
                      ],
                    ),
                  ),
                  Text(
                    'Não imprima em modo rascunho',
                    style: sectionTextStyle,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: mediaQuery * 14.0),
              height: mediaQuery * 45.0,
              width: double.infinity,
              child: RaisedButton(
                elevation: 0.0,
                color: Colors.orangeAccent,
                onPressed: () {
                  // showInSnackBar('Imprimir boleto');
                  launch(bankSlipPayment.bankSlipUrl);
                },
                child: Text(
                  'Imprimir boleto',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
