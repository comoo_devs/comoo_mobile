import 'package:comoo/src/blocs/credit_card_bloc.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:flutter/material.dart';

class InputCreditCard extends StatelessWidget {
  InputCreditCard(this.bloc, {this.showCVC = true});
  final CreditCardBloc bloc;
  final bool showCVC;

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    TextStyle labelFormStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.turqueza,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w500,
    );
    final InputDecoration decoration = InputDecoration(
      isDense: true,
      helperStyle: ComooStyles.texto,
      focusedBorder: ComooStyles.underLineBorder,
      labelStyle: labelFormStyle,
      border: ComooStyles.underLineBorder,
      counterText: '',
    );
    return Column(
      children: <Widget>[
        _card(context),
        // StreamText(
        //   stream: bloc.number,
        //   onChange: bloc.changeNumber,
        //   label: 'numero do cartão'.toUpperCase(),
        //   keyboardType: TextInputType.number,
        //   controller: bloc.numberController,
        //   maxLength: 19,
        // ),

        InputStream(
          stream: bloc.number,
          onChanged: bloc.changeNumber,
          keyboardType: TextInputType.number,
          controller: bloc.numberController,
          maxLength: 19,
          decoration: decoration.copyWith(
            labelText: 'numero do cartão'.toUpperCase(),
          ),
        ),
        InputStream(
          stream: bloc.expirationDate,
          onChanged: bloc.changeExpirationDate,
          keyboardType: TextInputType.number,
          controller: bloc.expirationDateController,
          decoration: decoration.copyWith(
            labelText: 'VALIDADE'.toUpperCase(),
          ),
          maxLength: 5,
        ),
        InputStream(
          stream: bloc.name,
          onChanged: bloc.changeName,
          controller: bloc.nameController,
          decoration: decoration.copyWith(
            labelText: 'Nome completo'.toUpperCase(),
          ),
          maxLength: 100,
        ),
        InputStream(
          stream: bloc.cpf,
          onChanged: bloc.changeCPF,
          decoration: decoration.copyWith(
            labelText: 'Cpf do Titular'.toUpperCase(),
          ),
          keyboardType: TextInputType.number,
          controller: bloc.cpfController,
        ),
        InputStream(
          stream: bloc.birthdate,
          onChanged: bloc.changeBirthdate,
          decoration: decoration.copyWith(
            labelText: 'Data de Nascimento'.toUpperCase(),
          ),
          keyboardType: TextInputType.number,
          controller: bloc.birthdateController,
        ),
        showCVC
            ? InputStream(
                stream: bloc.cvc,
                onChanged: bloc.changeCVC,
                decoration: decoration.copyWith(
                  labelText: 'CVV'.toUpperCase(),
                ),
                keyboardType: TextInputType.number,
                // controller: bloc.cvc,
              )
            : Container(),
      ],
    );
  }

  Widget _card(BuildContext context) {
    final mediaQuery = 1.02; //MediaQuery.of(context).size.width / 400;
    var chipTop = 30.0;
    var cardNumberTop = 76.0;
    var cardNumberLeft = 20.0;
    var cardNameBottom = 30.0;
    var validBottom = 54.0;
    var validLeft = 20.0;
    var cvvRight = 86.0;
    TextStyle inputTextStyle = TextStyle(
      color: ComooColors.chumbo,
      fontFamily: ComooStyles.fontFamily,
      fontSize: mediaQuery * 14.0,
      letterSpacing: mediaQuery * 0.61,
      fontWeight: FontWeight.normal,
    );
    TextStyle fixedTextStyle = TextStyle(
      color: ComooColors.cinzaMedio,
      fontFamily: ComooStyles.fontFamily,
      fontSize: mediaQuery * 10.0,
      letterSpacing: mediaQuery * 0.55,
      fontWeight: FontWeight.w500,
    );
    return Stack(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Image.asset(
            'images/cc_front.png',
            width: mediaQuery * 306.0,
            height: mediaQuery * 203.0,
            fit: BoxFit.contain,
          ),
        ),
        Positioned(
          top: mediaQuery * chipTop,
          left: mediaQuery * cardNumberLeft,
          child: Container(
            height: mediaQuery * 35.0,
            width: mediaQuery * 49.0,
            decoration: BoxDecoration(
              color: ComooColors.cinzaClaro,
              borderRadius: BorderRadius.all(
                Radius.circular(mediaQuery * 10.0),
              ),
              border: Border.all(
                color: ComooColors.cinzaClaro,
                width: mediaQuery * 1.0,
              ),
            ),
          ),
        ),
        Positioned(
          top: mediaQuery * cardNumberTop,
          left: mediaQuery * cardNumberLeft,
          child: StreamBuilder(
            // initialData: '',
            stream: bloc.number,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              print('${DateTime.now()} ${snapshot.data}');
              return Text(
                snapshot.data != null ? '${snapshot.data}' : '',
                style: inputTextStyle.copyWith(
                  fontSize: mediaQuery * 20.0,
                ),
              );
            },
          ),
        ),
        Positioned(
          bottom: mediaQuery * cardNameBottom,
          left: mediaQuery * cardNumberLeft,
          child: StreamBuilder(
            initialData: '',
            stream: bloc.name,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return Text(
                snapshot.data != null ? '${snapshot.data}' : '',
                style: inputTextStyle,
              );
            },
          ),
        ),
        Positioned(
          bottom: mediaQuery * (validBottom + 18.0),
          left: mediaQuery * (validLeft + 1.0),
          child: Text(
            'Validade',
            style: fixedTextStyle,
          ),
        ),
        Positioned(
          bottom: mediaQuery * validBottom,
          left: mediaQuery * validLeft,
          child: StreamBuilder(
            initialData: '',
            stream: bloc.expirationDate,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return Text(
                snapshot.data != null ? '${snapshot.data}' : '',
                style: inputTextStyle,
              );
            },
          ),
        ),
        showCVC
            ? Positioned(
                bottom: mediaQuery * (validBottom + 18.0),
                right: mediaQuery * cvvRight,
                child: Text(
                  'CVV',
                  style: fixedTextStyle,
                ),
              )
            : Container(),
        showCVC
            ? Positioned(
                bottom: mediaQuery * (validBottom),
                right: mediaQuery * cvvRight,
                child: StreamBuilder<String>(
                  initialData: '',
                  stream: bloc.cvc,
                  builder:
                      (BuildContext context, AsyncSnapshot<String> snapshot) {
                    return Text(
                      snapshot.data != null ? '${snapshot.data}' : '',
                      style: inputTextStyle,
                    );
                  },
                ),
              )
            : Container(),
      ],
    );
  }
}

// class StreamText extends StatelessWidget {
//   StreamText({
//     @required this.stream,
//     @required this.onChange,
//     // this.onSubmitted,
//     this.label,
//     this.maxLength,
//     this.textAlign = TextAlign.start,
//     this.keyboardType = TextInputType.text,
//     this.controller,
//     // this.textInputAction,
//   });

//   final Stream<String> stream;
//   final Function onChange;
//   // final void Function(String) onSubmitted;
//   final String label;
//   final int maxLength;
//   final TextAlign textAlign;
//   final TextInputType keyboardType;
//   final TextEditingController controller;

// //   @override
// //   State<StatefulWidget> createState() => _StreamTextState();
// // }

// // class _StreamTextState extends State<StreamText> {

//   @override
//   Widget build(BuildContext context) {
//     final double mediaQuery = MediaQuery.of(context).size.width / 400;
//     TextStyle labelFormStyle = TextStyle(
//       fontFamily: ComooStyles.fontFamily,
//       color: ComooColors.turqueza,
//       fontSize: mediaQuery * 15.0,
//       fontWeight: FontWeight.w500,
//     );
//     return StreamBuilder<String>(
//       stream: stream,
//       builder: (context, AsyncSnapshot<String> snapshot) {
//         return TextField(
//           keyboardType: keyboardType,
//           textCapitalization: TextCapitalization.words,
//           onChanged: onChange,
//           controller: controller,
//           maxLength: maxLength,
//           textAlign: textAlign,
//           autocorrect: false,
//           // onSubmitted: onSubmitted,
//           decoration: InputDecoration(
//             labelText: label,
//             isDense: true,
//             helperStyle: ComooStyles.texto,
//             focusedBorder: ComooStyles.underLineBorder,
//             labelStyle: labelFormStyle,
//             border: ComooStyles.underLineBorder,
//             counterText: '',
//             errorText: snapshot.error,
//           ),
//         );
//       },
//     );
//   }
// }
