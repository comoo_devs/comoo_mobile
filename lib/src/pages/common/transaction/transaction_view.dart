import 'package:comoo/src/blocs/credit_card_bloc.dart';
import 'package:comoo/src/comoo/credit_card.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/pages/common/transaction/input_credit_card.dart';
import 'package:comoo/src/pages/common/transaction/negotiation_page.dart';
import 'package:comoo/src/pages/common/transaction/select_credit_card.dart';
//import 'package:comoo/src/pages/common/transaction/select_credit_card.dart';
import 'package:comoo/src/pages/home/profile/my_profile/user_profile.dart';
import 'package:comoo/src/provider/provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/payment_type.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:comoo/src/widgets/stream_button.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_card_io/flutter_card_io.dart';
// import 'package:flutter/services.dart';
// import 'package:comoo/comoo/validations.dart';
import 'dart:async';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/comoo/comoo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/transaction_payment.dart';
//import 'package:comoo/src/pages/transaction/transaction_confirmation.dart';
import 'package:cloud_functions/cloud_functions.dart';
// import 'dart:convert';
import 'package:comoo/src/comoo/payment_interest.dart';
import 'package:comoo/src/widgets/loading_container.dart';
// import 'package:money/money.dart';
// import 'package:comoo/src/pages/common/transaction/create_payment_method.dart';

class TransactionView extends StatefulWidget {
  TransactionView({Key key, @required this.negotiation}) : super(key: key);

  final Negotiation negotiation;

  @override
  State<TransactionView> createState() => TransactionViewState();
}

class TransactionViewState extends State<TransactionView>
    with Reusables, PaymentTypeEnum {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Firestore _db = Firestore.instance;
  final NetworkApi api = NetworkApi();

  User currentUser;
  String cardNumber;
  String cardholderName;
  String expiryMonth;
  String expiryYear;
  String cvv;
  DateTime _birthDate = DateTime.now();
  String _fullName;
  String _cpf;
  String _phone;
  String _address;
  String _complement;
  String _city;
  String _state;
  String _zip;
  // double bankSlipInterest;
  List<double> creditCardInterests = <double>[];

  PaymentType _paymentType;
  // bool _isCreditCard = false;
  bool _isLoading = false;
  // bool _checkIfPaidBeforeRetry = false;
  List<Interest> interests = List<Interest>();
  String _loadingMessage = 'Carregando...';

  String paymentType = '';
  int installmentAmount = 0;
  CreditCard selectedCard;
  List<CreditCard> cardList = <CreditCard>[];
  // double _finalPrice = 0.0;

  Future<void> fetchCurrentUser() async {
    if (mounted) {
      setState(() {
        _isLoading = true;
      });
    }
    currentUser = await comoo.currentUser();
    await fetchCreditCard();
    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
//     if (mounted) {
//       setState(() {
//         _isLoading = false;
//         if (currentUser.defaultCard != null) {
//           defaultCard = '(' +
//               currentUser.defaultCard['brand'] +
//               ' *****' +
//               this
//                   .currentUser
//                   .defaultCard['display_number']
//                   .toString()
//                   .split('-')
//                   .last +
//               ')';
//         }
//         cardholderName = currentUser.fullName;
//         _fullName = currentUser.fullName;
//         _cpf = currentUser.cpf;
// //        _address = currentUser.address;
// //        _complement = currentUser.addressComplement;
// //        _city = currentUser.city;
// //        _state = currentUser.state;
// //        _zip = currentUser.zipCode;

//         _phone = currentUser.phone.number;
// //        if (currentUser.birthDate.isNotEmpty)
// //          _birthDate = DateTime.parse(currentUser.birthDate);
// //        else
// //          _birthDate = new DateTime.now();
// //        _isLoading = false;
//       });
//     }
  }

  Future<void> fetchCreditCard() async {
    setState(() {
      _isLoading = true;
      selectedCard = null;
    });
    CreditCard card;
    cardList.clear();
    await _db
        .collection('users')
        .document(currentUser.uid)
        .collection('credit_card')
        .getDocuments()
        .then((QuerySnapshot querySnapshot) {
      print('Has card? ${querySnapshot.documents.isNotEmpty}');
      if (querySnapshot.documents.isNotEmpty) {
        querySnapshot.documents.forEach((snap) {
          card = CreditCard.fromSnapshot(snap);
          card.number =
              card.number.substring(card.number.length - 4, card.number.length);
          cardList.add(card);
          print('Card: ${card.uid}');
          if (card.isDefault) {
            if (mounted) {
              setState(() {
                selectedCard = card;
                // _isCreditCard = true;
                _isLoading = false;
                card.isSelected = true;
              });
            }
            return;
          }
        });
      }
    });
    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(value, style: ComooStyles.textoSnackBar)),
    );
  }

  Future<void> handleTransaction() async {
    final FormState form = _formKey.currentState;
    if (form.validate()) {
//      setState(() {
//        _isLoading = true;
//      });
      form.save();

      final TransactionPayment transaction = TransactionPayment();
      transaction.birthDate = this._birthDate;
      transaction.cpf = this._cpf;

      transaction.cardholderName = this.cardholderName;
      transaction.cardNumber = this.cardNumber;
      transaction.cvv = this.cvv;

      transaction.address = this._address;
      transaction.city = this._city;
      transaction.complement = this._complement;
      transaction.state = this._state;
      transaction.zip = this._zip;
      transaction.phone = this._phone;
      transaction.status = 'created';

      await _saveUserDocument();
      // await _createTransaction(transaction);
    } else {
      showInSnackBar('Verifique as informações antes de continuar');
    }
  }

  Future<dynamic> _saveUserDocument() {
    currentUser.fullName = _fullName;
    currentUser.cpf = _cpf;
    currentUser.phone = Phone(number: _phone);
    currentUser.birthDate = _birthDate.toIso8601String();
//    currentUser.address = _address;
//    currentUser.addressComplement = _complement;
//    currentUser.city = _city;
//    currentUser.state = _state;
//    currentUser.zipCode = _zip;

    return this.currentUser.save();
  }

  // Future _createTransaction(TransactionPayment transaction) async {}

  Widget _buildPaymentMethod(CreditCardBloc ccBloc) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final NumberFormat formater =
        NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR');
    List<Widget> children = <Widget>[];
    children = <Widget>[
      Container(
        key: Key('ProductDescription'),
        height: mediaQuery * 85.0,
        child: Row(
          children: <Widget>[
            Container(
              height: mediaQuery * 60.0,
              width: mediaQuery * 60.0,
              child: Image.network(
                widget.negotiation.thumb,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
              width: mediaQuery * 8.0,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  widget.negotiation.title,
                  style: ComooStyles.titulo_negociacao,
                ),
                Text(
                  formater.format(widget.negotiation.price),
                  style: ComooStyles.preco_negociacao,
                )
              ],
            )
          ],
        ),
      ),
      Divider(),
      Text(
        'Escolha a forma de pagamento:',
        style: ComooStyles.negociacao,
      ),
      _paymentType == PaymentType.CreditCard
          ? Container()
          : ListTile(
              leading: paymentType == 'bank_slip'
                  ? Icon(
                      Icons.check_circle,
                      color: ComooColors.turqueza,
                      size: mediaQuery * 23.2,
                    )
                  : Icon(
                      Icons.check_circle_outline,
                      color: ComooColors.cinzaClaro,
                      size: mediaQuery * 23.2,
                    ),
              title: Text(
                'Boleto Bancário',
                style: ComooStyles.payment_type,
              ),
              dense: true,
              onTap: () {
                setState(() {
                  paymentType = 'bank_slip';
                  // _finalPrice = widget.negotiation.price; // + bankSlipInterest;
                  installmentAmount = 0;
                });
              },
              contentPadding: const EdgeInsets.all(0.0),
              // subtitle:  Text(
              //   'valor da compra', // + R\$$bankSlipInterest',
              //   style: ComooStyles.payment_type_subtitle,
              // ),
              trailing: Text(
                formater
                    .format(widget.negotiation.price), // + bankSlipInterest),
                style: ComooStyles.payment_type,
              ),
            ),
      Divider(),
      _paymentType == PaymentType.BankSlip
          ? Container()
          : ListTile(
              onTap: () {
                setState(() {
                  paymentType = 'credit_card';
                  // _finalPrice = widget.negotiation.price * 1.025;
                  installmentAmount = 1;
                });
              },
              contentPadding: const EdgeInsets.all(0.0),
              leading: paymentType == 'credit_card'
                  ? Icon(
                      Icons.check_circle,
                      color: ComooColors.turqueza,
                      size: mediaQuery * 23.2,
                    )
                  : Icon(
                      Icons.check_circle_outline,
                      color: ComooColors.cinzaClaro,
                      size: mediaQuery * 23.2,
                    ),
              title: Text(
                'Cartão de crédito',
                style: ComooStyles.payment_type,
              ),
              subtitle: Text(
                selectedCard != null
                    ? '(${selectedCard?.brand ?? ''} ***${selectedCard?.number ?? ''})'
                    : '',
                style: ComooStyles.payment_type.copyWith(
                    fontWeight: FontWeight.bold, fontSize: mediaQuery * 11.0),
              ),
              trailing: Container(
                height: mediaQuery * 26.0,
                width: mediaQuery * 113.0,
                child: selectedCard == null
                    ? null
                    : OutlineButton(
                        onPressed: _creditCardSelection,
                        color: Color(0xFF868693),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(mediaQuery * 20.0)),
                        child: FittedBox(
                          child: Text(
                            'OUTRO CARTÃO',
                            style: TextStyle(
                              fontFamily: COMOO.fonts.montSerrat,
                              fontSize: mediaQuery * 11.0,
                              color: Color(0xFF868693),
                            ),
                          ),
                        ),
                      ),
              ),
            ),
      _paymentType == PaymentType.BankSlip
          ? Container()
          : ListTile(
              dense: true,
              contentPadding: EdgeInsets.only(
                left: mediaQuery * 44.4,
                top: mediaQuery * 0.0,
                bottom: mediaQuery * 0.0,
              ),
              leading: installmentAmount == 1
                  ? Icon(
                      Icons.check_circle,
                      color: ComooColors.turqueza,
                      size: mediaQuery * 23.2,
                    )
                  : Icon(
                      Icons.check_circle_outline,
                      color: ComooColors.cinzaClaro,
                      size: mediaQuery * 23.2,
                    ),
              onTap: () {
                setState(() {
                  paymentType = 'credit_card';
                  installmentAmount = 1;
                });
              },
              title: Text(
                'à vista',
                style: ComooStyles.payment_type_subtitle,
              ),
              trailing: Text(
                formater.format(widget.negotiation.price * 1.0),
                style: ComooStyles.payment_type_subtitle,
              ),
            ),
    ];
    if (_paymentType != PaymentType.BankSlip) {
      for (int index = 0, times = 2;
          index < creditCardInterests.length;
          index++, times++) {
        final double interest = creditCardInterests[index];
        children.add(ListTile(
          dense: true,
          contentPadding: EdgeInsets.only(
            left: mediaQuery * 44.4,
            top: mediaQuery * 0.0,
            bottom: mediaQuery * 0.0,
          ),
          leading: installmentAmount == times
              ? Icon(
                  Icons.check_circle,
                  color: ComooColors.turqueza,
                  size: mediaQuery * 23.2,
                )
              : Icon(
                  Icons.check_circle_outline,
                  color: ComooColors.cinzaClaro,
                  size: mediaQuery * 23.2,
                ),
          onTap: () {
            setState(() {
              paymentType = 'credit_card';
              // _finalPrice = widget.negotiation.price * interest;
              installmentAmount = times;
            });
          },
          title: Text(
            '${times}x ${formater.format(widget.negotiation.price * interest / times)}',
            style: ComooStyles.payment_type_subtitle,
          ),
          trailing: Text(
            formater.format(widget.negotiation.price * interest),
            style: ComooStyles.payment_type_subtitle,
          ),
        ));
      }
    }
    children.add(Divider());
    if (paymentType == 'credit_card') {
      children.add(selectedCard == null ? _newCreditCard() : _askCVC());
    }
    children.add(Container(
      margin: EdgeInsets.symmetric(vertical: mediaQuery * 10.0),
      padding: EdgeInsets.symmetric(horizontal: mediaQuery * 100.0),
      child: StreamBuilder<String>(
        stream: ccBloc.cvc,
        builder: (BuildContext cvcContext, AsyncSnapshot<String> snapCVC) {
          return StreamBuilder<bool>(
            stream: ccBloc.submitValidCVC,
            builder: (BuildContext submitContext, AsyncSnapshot<bool> ccSnap) {
              return StreamRaisedButton(
                color: COMOO.colors.turquoise,
                onPressed: paymentType.isEmpty
                    ? null
                    : paymentType == 'credit_card'
                        ? selectedCard == null
                            ? ccSnap.hasData
                                ? () {
                                    createOrder(ccBloc);
                                  }
                                : null
                            : snapCVC.hasData && snapCVC.data.isNotEmpty ??
                                    false
                                ? () {
                                    createOrder(ccBloc);
                                  }
                                : null
                        : () {
                            createOrder(ccBloc);
                          },
                text: paymentType == 'bank_slip'
                    ? 'AVANÇAR'
                    : 'FINALIZAR A COMPRA',
              );
              // return RaisedButton(
              //   padding: EdgeInsets.symmetric(
              //     vertical: mediaQuery * 8.0,
              //     horizontal: mediaQuery * 10.0,
              //   ),
              //   color: ComooColors.turqueza,
              //   onPressed: paymentType.isEmpty
              //       ? null
              //       : paymentType == 'credit_card'
              //           ? selectedCard == null
              //               ? ccSnap.hasData
              //                   ? () {
              //                       createOrder();
              //                     }
              //                   : null
              //               : snapCVC.hasData && snapCVC.data.isNotEmpty ??
              //                       false
              //                   ? () {
              //                       createOrder();
              //                     }
              //                   : null
              //           : () {
              //               createOrder();
              //             },
              //   shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
              //   child: FittedBox(
              //     child: Text(
              //       paymentType == 'bank_slip'
              //           ? 'AVANÇAR'
              //           : 'FINALIZAR A COMPRA',
              //       style: ComooStyles.botao_branco,
              //     ),
              //   ),
              // );
            },
          );
        },
      ),
    ));
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(mediaQuery * 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: children,
        ),
      ),
    );
  }

  Future<void> _creditCardSelection() async {
    final CreditCard _card = await Navigator.of(context).push(
      MaterialPageRoute<CreditCard>(
        fullscreenDialog: true,
        builder: (BuildContext context) => CreditCardSelection(),
      ),
    );
    if (_card != null) {
      if (mounted) {
        setState(() {
          selectedCard = _card;
        });
      }
    } else {
      fetchCreditCard();
    }

    // print(_card);
  }

  Widget _newCreditCard() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    CreditCardBloc bloc = Provider.of(context).creditCard;
    return Column(
      children: <Widget>[
        Text(
          'Insira os dados do seu cartão de crédito:',
          style: ComooStyles.negociacao,
        ),
        SizedBox(
          height: mediaQuery * 14.0,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: mediaQuery * 30.0),
          child: InputCreditCard(bloc),
        )
      ],
    );
  }

  Widget _askCVC() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    CreditCardBloc bloc = Provider.of(context).creditCard;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Confirme o CVV do seu cartão de crédito',
          style: ComooStyles.negociacao,
          textAlign: TextAlign.center,
        ),
        Container(
          width: mediaQuery * 158.0,
          child: InputStream(
            stream: bloc.cvc,
            onChanged: bloc.changeCVC,
            // textAlign: TextAlign.center,
            maxLength: 5,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(counterText: ''),
            // onSubmitted: (String value) => createOrder,
          ),
        ),
      ],
    );
  }

  Future<void> payWithBankSlip() async {
    final dynamic result = await CloudFunctions.instance
        .getHttpsCallable(functionName: 'payWithBankSlip')
        .call(<String, String>{
          'negotiationId': widget.negotiation.id,
          // 'price': Money.fromDouble(_finalPrice, Currency('BRL')).amount,
          // 'title': widget.negotiation.title
        })
        .timeout(const Duration(seconds: 60))
        .then((HttpsCallableResult result) {
          return result.data;
        })
        .catchError((onError) {
          return null;
        });
    if (result != null) {
      final Negotiation negotiation = widget.negotiation;
      negotiation.paymentDescriptor = BankSlipPayment.fromSnapshot(result);
      await _showBankSlipDialog();
      Navigator.of(context).pop();
      Navigator.of(context).push(
        MaterialPageRoute<void>(
          builder: (BuildContext context) => NegotiationPage(negotiation),
        ),
      );
    } else {
      showInSnackBar(
          'Erro ao processar o pagamento, verifique sua conexão e tente novamente.');
      setState(() {
        // _checkIfPaidBeforeRetry = true;
        _isLoading = false;
      });
    }
    setState(() {
      _isLoading = false;
    });
  }

  Future<void> payWithNewCard() async {
    final CreditCardBlocValue value = Provider.of(context).creditCard.values;
    Map<String, dynamic> param = <String, dynamic>{
      'negotiationId': widget.negotiation.id,
      'installmentCount': installmentAmount,
    };
    param.addAll(value.toMap);
    final bool isSuccess = await api.creditCardPayment<dynamic>(context, param);
    setState(() {
      _isLoading = false;
    });
    if (isSuccess) {
      await _showDialog();
      Navigator.pop(context, true);
    } else {
      setState(() {
        // _checkIfPaidBeforeRetry = true;
        _isLoading = false;
      });
    }
    setState(() {
      _isLoading = false;
    });
  }

  Future<void> payWithCard(String cardNumber) async {
    // var result;
    if (cardNumber == null || cardNumber == '') {
      showInSnackBar('Selecione um cartão antes de continuar');
      return;
    }
    if (selectedCard == null) {
      CreditCard card = cardList.firstWhere((card) {
        return card.number == cardNumber;
      });
      if (card == null) {
        showInSnackBar('Selecione um cartão antes de continuar');
        return;
      }
      selectedCard = card;
    }
    print(selectedCard.uid);

    // _showDialog(context);

    final CreditCardBloc bloc = Provider.of(context).creditCard;
    Map<String, dynamic> param = <String, dynamic>{
      'negotiationId': widget.negotiation.id,
      'installmentCount': installmentAmount,
      'cardId': selectedCard.uid,
      'cvc': bloc.cvcValue,
    };
    final bool isSuccess = await api.creditCardPayment<dynamic>(context, param);
    if (isSuccess) {
      await _showDialog();
      Navigator.pop(context, true);
    } else {
      setState(() {
        // _checkIfPaidBeforeRetry = true;
        _isLoading = false;
      });
    }
    // final bool result = await CloudFunctions.instance
    //     .call(functionName: 'payWithStoredCreditCard', parameters: {
    //       'negotiationId': widget.negotiation.id,
    //       'cardId': selectedCard.uid,
    //       'installmentCount': installmentAmount,
    //     })
    //     .timeout(Duration(minutes: 2))
    //     .then((result) {
    //       print(result);
    //       return true;
    //     })
    //     .catchError((onError) {
    //       print('payWithStoredCreditCard $onError');
    //       print(onError.message);
    //       return false;
    //     });
    // print(result);
    // if (result) {
    //   await _showDialog();
    //   Navigator.pop(context, true);
    // } else {
    //   showInSnackBar(
    //       'Erro ao processar o pagamento, verifique sua conexão e tente novamente');
    //   setState(() {
    //     _checkIfPaidBeforeRetry = true;
    //     _isLoading = false;
    //   });
    // }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _showBankSlipDialog() {
    final BuildContext context = _scaffoldKey.currentContext;
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return showCustomDialog<void>(
      context: context,
      barrierDismissible: false,
      alert: customRoundAlert(
        context: context,
        title: 'Seu boleto foi gerado!',
        actions: [
          Container(
            height: mediaQuery * 41.0,
            width: mediaQuery * 163.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _showDialog() {
    final BuildContext context = _scaffoldKey.currentContext;
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return customRoundAlert(
          context: context,
          title: 'Seu pagamento foi enviado para analise!',
          actions: [
            Container(
              height: mediaQuery * 41.0,
              width: mediaQuery * 163.0,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.0,
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'OK',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> createOrder(CreditCardBloc bloc) async {
    setState(() {
      _isLoading = true;
    });

    final DocumentSnapshot negotiationSnapshot = await _db
        .collection('negotiations')
        .document(widget.negotiation.id)
        .get();
    final negotiationData = negotiationSnapshot.data;

    if (negotiationData['status'] == 'paid' ||
        negotiationData['payment'] != null ||
        negotiationData['invoice_id'] != null) {
      // _redirectToPurchaseDetail();
      showInSnackBar('O pagamento deste produto já foi realizado!');
      setState(() {
        _isLoading = false;
      });
      return;
    }
    if (widget.negotiation.payment != null) {
      // _redirectToPurchaseDetail();
      setState(() {
        _isLoading = false;
      });
      showInSnackBar('Você já realizou o pagamento!');
    } else {
      setState(() {
        _isLoading = true;
      });

      if (paymentType == 'bank_slip') {
        if (currentUser.address == null) {
          final bool result = await Navigator.of(context).push(
              MaterialPageRoute<bool>(
                  fullscreenDialog: true,
                  builder: (BuildContext context) => UserProfilePage()));
          if (result ?? false) {
            payWithBankSlip();
          } else {
            setState(() {
              _isLoading = false;
            });
          }
        } else {
          payWithBankSlip();
        }
      } else {
        if (selectedCard == null) {
          payWithNewCard();
        } else {
          payWithCard(selectedCard.number);
        }
      }
    }
  }

  @override
  void initState() {
    fetchCurrentUser();
    super.initState();

    // bankSlipInterest = 3.49;
    // creditCardInterests.add(1.0);
    creditCardInterests.add(1.0450); //x2
    creditCardInterests.add(1.0500); //x3
    creditCardInterests.add(1.0550); //x4
    creditCardInterests.add(1.0650); //x5
    creditCardInterests.add(1.0750); //x6
//    creditCardInterests.add(1.0850); //x7
//    creditCardInterests.add(1.0950); //x8
//    creditCardInterests.add(1.1050); //x9
//    creditCardInterests.add(1.1150); //x10
//    creditCardInterests.add(1.1200); //x11
//    creditCardInterests.add(1.1250); //x12
    _fetchPaymentType();
  }

  Future<void> _fetchPaymentType() async {
    final DocumentSnapshot productSnap = await widget.negotiation.product.get();
    final Product product = Product.fromSnapshot(productSnap);
    _paymentType = getPaymentType(product.paymentType);
  }

  @override
  Widget build(BuildContext context) {
    CreditCardBloc ccBloc = Provider.of(context).creditCard;
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar.round(
          color: ComooColors.turqueza,
          title: Text(
            'PAGAMENTO',
            style: ComooStyles.appBar_botao_branco,
          ),
          actions: <Widget>[
            StreamBuilder<String>(
                stream: ccBloc.cvc,
                builder:
                    (BuildContext cvcContext, AsyncSnapshot<String> cvcSnap) {
                  return StreamBuilder<bool>(
                      stream: ccBloc.submitValidCVC,
                      builder: (BuildContext submitContext,
                          AsyncSnapshot<bool> ccSnap) {
                        return FlatButton(
                          onPressed: paymentType.isEmpty
                              ? null
                              : paymentType == 'credit_card'
                                  ? selectedCard == null
                                      ? ccSnap.hasData
                                          ? () {
                                              createOrder(ccBloc);
                                            }
                                          : null
                                      : cvcSnap.hasData &&
                                                  cvcSnap.data.isNotEmpty ??
                                              false
                                          ? () {
                                              createOrder(ccBloc);
                                            }
                                          : null
                                  : () {
                                      createOrder(ccBloc);
                                    },
                          child: Text(
                            'AVANÇAR',
                            style: ComooStyles.appBar_botao_branco,
                          ),
                        );
                      });
                })
          ],
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
            SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
          },
          child: Stack(
            children: <Widget>[
              _buildPaymentMethod(ccBloc),
              _isLoading
                  ? LoadingContainer(
                      message: _loadingMessage,
                    )
                  : Container(),
            ],
          ),
        ));
  }
}
