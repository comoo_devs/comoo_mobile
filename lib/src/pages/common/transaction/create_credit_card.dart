import 'package:comoo/src/blocs/credit_card_bloc.dart';
import 'package:comoo/src/comoo/credit_card.dart';
import 'package:comoo/src/pages/common/transaction/input_credit_card.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/stream_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CreateCreditCard extends StatelessWidget {
  final NetworkApi api = NetworkApi();

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    CreditCardBloc bloc = BlocProvider.of<CreditCardBloc>(context);
    // bloc.clear();
    return StreamBuilder<bool>(
      initialData: false,
      stream: bloc.isLoading,
      builder: (BuildContext context, AsyncSnapshot<bool> loading) {
        return GestureDetector(
          onTap: () {
            // FocusScope.of(context).requestFocus(FocusNode());
            SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
          },
          child: Scaffold(
            appBar: AppBar(
              elevation: 0.0,
              title: Text(
                'adicionar cartão',
                style: TextStyle(
                  fontFamily: COMOO.fonts.montSerrat,
                  color: Colors.white,
                  fontSize: mediaQuery * 15.0,
                  fontWeight: FontWeight.w500,
                  letterSpacing: mediaQuery * -0.125,
                ),
              ),
              centerTitle: true,
              actions: <Widget>[
                StreamBuilder<bool>(
                  stream: bloc.submitValid,
                  builder:
                      (BuildContext submitContext, AsyncSnapshot<bool> snap) {
                    return FlatButton(
                      onPressed: snap.hasData
                          ? () {
                              _addCard(context, bloc);
                            }
                          : null,
                      child: Text(
                        'SALVAR',
                        style: TextStyle(
                          fontFamily: COMOO.fonts.montSerrat,
                          fontSize: mediaQuery * 13.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          letterSpacing: mediaQuery * -0.108,
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
            body: loading.data
                ? LoadingContainer()
                : Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: mediaQuery * 35.0),
                    child: Container(
                      margin: EdgeInsets.only(top: mediaQuery * 27.0),
                      child: ListView(
                        children: <Widget>[
                          InputCreditCard(bloc, showCVC: false),
                          SizedBox(height: mediaQuery * 32.0),
                          StreamBuilder<bool>(
                            stream: bloc.submitValid,
                            builder: (BuildContext submitContext,
                                AsyncSnapshot<bool> snap) {
                              return StreamRaisedButton(
                                onPressed: snap.hasData
                                    ? () {
                                        _addCard(context, bloc);
                                      }
                                    : null,
                                text: 'SALVAR',
                                color: COMOO.colors.turquoise,
                              );
                            },
                          ),
                          SizedBox(height: mediaQuery * 16.0),
                        ],
                      ),
                    ),
                  ),
          ),
        );
      },
    );
  }

  Future<void> _addCard(BuildContext context, CreditCardBloc bloc) async {
    bloc.changeLoading(true);
    final CreditCardBlocValue value = bloc.values;
    final card = await api.createCreditCard(
      context,
      <String, String>{
        'name': value.name,
        'expirationMonth': value.expirationDate.split('/').first,
        'expirationYear': value.expirationDate.split('/').last,
        'number': value.number.replaceAll(r' ', ''),
        'cpf': value.cpf,
        'birthDate': value.birthdate,
      },
    );

    bloc.changeLoading(false);
    if (card != null) {
      Navigator.of(context).pop(CreditCard.fromMap(card));
    }
  }
}
/*
class CreatePaymentMethod extends StatefulWidget {
  final Negotiation negotiation;
  final int installmentCount;
  CreatePaymentMethod({this.negotiation, this.installmentCount});
  @override
  State createState() {
    return new CreatePaymentMethodState();
  }
}

class CreatePaymentMethodState extends State<CreatePaymentMethod>
    with Validations {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  // String name;
  // String number;
  // String verificationValue;
  // String month;
  // String year;
  bool isLoading = false;
  bool _isDefault = false;
  String _cpfErrorText;
  String _dateErrorText;
  String _birthDateErrorText;

  // MaskedTextController cardController =
  //     new MaskedTextController(mask: '0000-0000-0000-0000');

  var creditCardNumber = MaskedTextController(mask: '0000 0000 0000 0000');
  var creditCardName = TextEditingController();
  var creditCardCpf = MaskedTextController(mask: '000.000.000-00');
  var creditCardDate = MaskedTextController(mask: '00/00');
  var ownerBirthdate = MaskedTextController(mask: '00/00/0000');
  // var creditCardMonth =  TextEditingController();
  // var creditCardYear =  TextEditingController();
  var creditCardValidation = TextEditingController();

  StreamController<String> creditCardNumberStream = StreamController<String>();
  StreamController<String> creditCardNameStream = StreamController<String>();
  StreamController<String> creditCardCpfStream = StreamController<String>();
  StreamController<String> creditCardDateStream = StreamController<String>();
  StreamController<String> ownerBirthdateStream = StreamController<String>();
  StreamController<String> creditCardValidationNumberStream =
      StreamController<String>();

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text(value, style: ComooStyles.textoSnackBar)));
  }

  Future<void> addPaymentMethod() async {
    setState(() {
      isLoading = true;
    });
    if (_cpfErrorText != null ||
        _dateErrorText != null ||
        _birthDateErrorText != null) {
      showInSnackBar('Verifique os erros antes de continuar.');
      setState(() {
        isLoading = false;
      });
      return;
    }
    var creditCard = <String, dynamic>{
      'name': creditCardName.text,
      'expirationMonth': creditCardDate.text.split('/').first,
      'expirationYear': creditCardDate.text.split('/').last,
      'number': creditCardNumber.text.replaceAll(r' ', ''),
      'cpf': creditCardCpf.text,
      'birthDate': ownerBirthdate.text,
      'cvc': creditCardValidation.text,
      'default': _isDefault,
    };
    print(creditCard);

    await CloudFunctions.instance
        .call(functionName: 'createCreditCard', parameters: creditCard)
        .timeout(Duration(minutes: 1))
        .then((result) {
      // _redirectToPurchaseDetail();
      showInSnackBar('Cartão salvo com sucesso!');
      Navigator.pop(context, creditCardNumber.text);
    }).catchError((onError) {
      if (onError is CloudFunctionsException) {
        print(onError.code);
        print(onError.details);
        print(onError.message);
      }
      showInSnackBar(
          'Erro ao salvar o cartão, verifique seus dados ou conexão e tente novamente');
    });
    setState(() {
      isLoading = false;
    });
  }

  void showErrors(List<String> errors) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                'ATENÇÃO',
                style: ComooStyles.texto_bold,
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: errors
                      .map((s) => Text(
                            s,
                            style: ComooStyles.texto_roxo,
                          ))
                      .toList(),
                ),
              ),
              actions: <Widget>[
                FlatButton(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    })
              ]);
        });
  }

  Future updateUserDefaultPayment(card) async {
    return Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData({'default_card': card});
  }

  Widget card() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    var chipTop = 24.0;
    var cardNumberTop = 76.0;
    var cardNumberLeft = 34.0;
    var cardNameBottom = 16.0;
    var validBottom = 44.0;
    var validLeft = 34.0;
    var cvvRight = 86.0;
    TextStyle inputTextStyle = TextStyle(
      color: ComooColors.chumbo,
      fontFamily: ComooStyles.fontFamily,
      fontSize: mediaQuery * 14.0,
      letterSpacing: mediaQuery * 0.61,
      fontWeight: FontWeight.normal,
    );
    TextStyle fixedTextStyle = TextStyle(
      color: ComooColors.cinzaMedio,
      fontFamily: ComooStyles.fontFamily,
      fontSize: mediaQuery * 10.0,
      letterSpacing: mediaQuery * 0.55,
      fontWeight: FontWeight.w500,
    );
    return Stack(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: SvgPicture.asset(
            'images/svg/cc_front.svg',
            fit: BoxFit.contain,
            width: mediaQuery * 306.0,
            height: mediaQuery * 203.0,
          ),
        ),
        // Container(
        //   margin: EdgeInsets.symmetric(
        //     horizontal: mediaQuery * 35.0,
        //   ),
        //   height: mediaQuery * 188.0,
        //   width: mediaQuery * 306.0,
        //   decoration: BoxDecoration(
        //     borderRadius: BorderRadius.all(
        //       Radius.circular(mediaQuery * 32.0),
        //     ),
        //     border: Border.all(
        //       color: ComooColors.cinzaClaro,
        //       width: mediaQuery * 1.0,
        //     ),
        //   ),
        // ),
        Positioned(
          top: mediaQuery * chipTop,
          left: mediaQuery * cardNumberLeft,
          child: Container(
            height: mediaQuery * 35.0,
            width: mediaQuery * 49.0,
            decoration: BoxDecoration(
              color: ComooColors.cinzaClaro,
              borderRadius: BorderRadius.all(
                Radius.circular(mediaQuery * 10.0),
              ),
              border: Border.all(
                color: ComooColors.cinzaClaro,
                width: mediaQuery * 1.0,
              ),
            ),
          ),
        ),
        Positioned(
          top: mediaQuery * cardNumberTop,
          left: mediaQuery * cardNumberLeft,
          child: StreamBuilder(
            initialData: '',
            stream: creditCardNumberStream.stream,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return Text(
                '${snapshot.data}',
                style: inputTextStyle.copyWith(
                  fontSize: mediaQuery * 22.0,
                  // height: mediaQuery * 27.0,
                ),
              );
            },
          ),
        ),
        Positioned(
          bottom: mediaQuery * cardNameBottom,
          left: mediaQuery * cardNumberLeft,
          child: StreamBuilder(
            initialData: '',
            stream: creditCardNameStream.stream,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return Text(
                '${snapshot.data}',
                style: inputTextStyle,
              );
            },
          ),
        ),
        Positioned(
          bottom: mediaQuery * (validBottom + 18.0),
          left: mediaQuery * (validLeft + 1.0),
          child: Text(
            'Validade',
            style: fixedTextStyle,
          ),
        ),
        Positioned(
          bottom: mediaQuery * validBottom,
          left: mediaQuery * validLeft,
          child: StreamBuilder(
            initialData: '',
            stream: creditCardDateStream.stream,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return Text(
                '${snapshot.data}',
                style: inputTextStyle,
              );
            },
          ),
        ),
        // Positioned(
        //   bottom: mediaQuery * validBottom,
        //   left: mediaQuery * validLeft,
        //   child: StreamBuilder(
        //     initialData: '',
        //     stream: creditCardMonthStream.stream,
        //     builder: (BuildContext context, AsyncSnapshot snapshot) {
        //       return Text(
        //         '${snapshot.data}',
        //         style: inputTextStyle,
        //       );
        //     },
        //   ),
        // ),
        // Positioned(
        //   bottom: mediaQuery * validBottom,
        //   left: mediaQuery * (validLeft + 18.0),
        //   child: Text(
        //     '/',
        //     style: TextStyle(color: Colors.white),
        //   ),
        // ),
        // Positioned(
        //   bottom: mediaQuery * validBottom,
        //   left: mediaQuery * (validLeft + 25.0),
        //   child: StreamBuilder(
        //     initialData: '',
        //     stream: creditCardYearStream.stream,
        //     builder: (BuildContext context, AsyncSnapshot snapshot) {
        //       return Text(
        //         '${snapshot.data}',
        //         style: inputTextStyle,
        //       );
        //     },
        //   ),
        // ),
        Positioned(
          bottom: mediaQuery * (validBottom + 18.0),
          right: mediaQuery * cvvRight,
          child: Text(
            'CVV',
            style: fixedTextStyle,
          ),
        ),
        Positioned(
          bottom: mediaQuery * (validBottom),
          right: mediaQuery * cvvRight,
          child: StreamBuilder(
            initialData: '',
            stream: creditCardValidationNumberStream.stream,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return Text(
                '${snapshot.data}',
                style: inputTextStyle,
              );
            },
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size.width / 400;
    TextStyle labelFormStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.turqueza,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w500,
    );
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: ComooColors.turqueza,
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          'NOVO CARTÃO DE CRÉDITO',
          style: ComooStyles.appBarTitleWhite,
        ),
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.fromLTRB(mediaQuery * 35.0, mediaQuery * 10.0,
                  mediaQuery * 35.0, mediaQuery * 10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      bottom: mediaQuery * 20.0,
                    ),
                    child: card(),
                  ),
                  TextField(
                    keyboardType: TextInputType.number,
                    onChanged: (String value) {
                      creditCardNumberStream.sink.add(value);
                    },
                    controller: creditCardNumber,
                    maxLength: 19,
                    // decoration: ComooStyles.inputDecoration('numero do cartão'),
                    decoration: InputDecoration(
                      labelText: 'numero do cartão'.toUpperCase(),
                      isDense: true,
                      helperStyle: ComooStyles.texto,
                      focusedBorder: ComooStyles.underLineBorder,
                      labelStyle: labelFormStyle,
                      border: ComooStyles.underLineBorder,
                      counterText: '',
                    ),
                  ),
                  TextField(
                    keyboardType: TextInputType.number,
                    onChanged: (String value) {
                      creditCardDateStream.sink.add(value);
                    },
                    controller: creditCardDate,
                    // decoration: ComooStyles.inputDecoration('VALIDADE'),
                    decoration: InputDecoration(
                      labelText: 'VALIDADE'.toUpperCase(),
                      isDense: true,
                      helperStyle: ComooStyles.texto,
                      focusedBorder: ComooStyles.underLineBorder,
                      labelStyle: labelFormStyle,
                      border: ComooStyles.underLineBorder,
                      counterText: '',
                      errorText: _dateErrorText,
                    ),
                    maxLength: 5,
                    maxLengthEnforced: true,
                  ),
                  TextField(
                    onChanged: (String value) {
                      creditCardNameStream.sink.add(value);
                    },
                    controller: creditCardName,
                    // decoration: ComooStyles.inputDecoration('Nome completo'),
                    decoration: InputDecoration(
                      labelText: 'Nome completo'.toUpperCase(),
                      isDense: true,
                      helperStyle: ComooStyles.texto,
                      focusedBorder: ComooStyles.underLineBorder,
                      labelStyle: labelFormStyle,
                      border: ComooStyles.underLineBorder,
                    ),
                    textCapitalization: TextCapitalization.characters,
                  ),
                  TextField(
                    onChanged: (String value) {
                      creditCardCpfStream.sink.add(value);
                    },
                    controller: creditCardCpf,
                    // decoration: ComooStyles.inputDecoration('Cpf do Titular'),
                    decoration: InputDecoration(
                      labelText: 'Cpf do Titular'.toUpperCase(),
                      // isDense: true,
                      helperStyle: ComooStyles.texto,
                      focusedBorder: ComooStyles.underLineBorder,
                      labelStyle: labelFormStyle,
                      border: ComooStyles.underLineBorder,
                      counterText: '',
                      errorText: _cpfErrorText,
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  TextField(
                    onChanged: (String value) {
                      ownerBirthdateStream.sink.add(value);
                    },
                    controller: ownerBirthdate,
                    // decoration: ComooStyles.inputDecoration('Cpf do Titular'),
                    decoration: InputDecoration(
                      labelText: 'Data de Nascimento'.toUpperCase(),
                      // isDense: true,
                      helperStyle: ComooStyles.texto,
                      focusedBorder: ComooStyles.underLineBorder,
                      labelStyle: labelFormStyle,
                      border: ComooStyles.underLineBorder,
                      counterText: '',
                      errorText: _birthDateErrorText,
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  TextField(
                    keyboardType: TextInputType.number,
                    onChanged: (String value) {
                      creditCardValidationNumberStream.sink.add(value);
                    },
                    controller: creditCardValidation,
                    // decoration: ComooStyles.inputDecoration('CVV'),
                    decoration: InputDecoration(
                      labelText: 'CVV'.toUpperCase(),
                      isDense: true,
                      helperStyle: ComooStyles.texto,
                      focusedBorder: ComooStyles.underLineBorder,
                      labelStyle: labelFormStyle,
                      border: ComooStyles.underLineBorder,
                      counterText: '',
                    ),
                  ),
                  Container(
                    child: CheckboxListTile(
                      value: _isDefault,
                      onChanged: (bool value) {
                        setState(() {
                          _isDefault = value;
                        });
                      },
                      title: Text('Tornar cartão padrão'),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(
                      vertical: mediaQuery * 25.0,
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: mediaQuery * 15.0,
                    ),
                    child: Container(
                      height: mediaQuery * 35.0,
                      child: RaisedButton(
                        color: ComooColors.turqueza,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                        onPressed: () {
                          addPaymentMethod();
                        },
                        child: Text(
                          'SALVAR',
                          style: TextStyle(
                            fontFamily: ComooStyles.fontFamily,
                            color: Colors.white,
                            fontSize: mediaQuery * 15.0,
                            fontWeight: FontWeight.w500,
                            letterSpacing: mediaQuery * 0.60,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          isLoading ? LoadingContainer() : Container()
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    creditCardCpf.addListener(() {
      setState(() {
        _cpfErrorText = validateCPF(creditCardCpf.text);
      });
    });
    creditCardDate.addListener(() {
      setState(() {
        _dateErrorText = validateMonthYear(creditCardDate.text);
      });
    });
    ownerBirthdate.addListener(() {
      setState(() {
        _birthDateErrorText = validateBirthDate(ownerBirthdate.text);
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    creditCardNumberStream.close();
    creditCardNameStream.close();
    creditCardCpfStream.close();
    creditCardDateStream.close();
    ownerBirthdateStream.close();
    creditCardValidationNumberStream.close();
  }
}
*/
