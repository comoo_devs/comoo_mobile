import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';

class AnticipatePaymentPage extends StatefulWidget {
  @override
  _AnticipatePaymentPageState createState() => _AnticipatePaymentPageState();
}

class _AnticipatePaymentPageState extends State<AnticipatePaymentPage>
    with Reusables {
  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  final api = CloudFunctions.instance;
  double _balance = 0.0;
  var withdrawController = new MoneyMaskedTextController(leftSymbol: 'R\$ ');
  final NumberFormat formater =
      NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');

  _fetchBalance() async {
    return await api
        .getHttpsCallable(functionName: 'getBalance')
        .call()
        .timeout(Duration(seconds: 60))
        .then((HttpsCallableResult result) {
      // print(result);
      return result.data;
    }).catchError((error) {
      // print(error);
      var errorMessage = {
        'current': {'amount': 0, 'currency': 'BRL'},
        'future': {'amount': 0, 'currency': 'BRL'},
      };
      return errorMessage;
    });
  }

  _setAnticipations(amount) async {
    await api
        .getHttpsCallable(functionName: 'setAnticipations')
        .call(<String, dynamic>{
      'amount': amount,
    }).then((result) {
      Navigator.of(context).pop();
      showCustomDialog(
        context: context,
        alert: customRoundAlert(
            context: context,
            title:
                'R\$$amount transferido para a sua conta bancária com sucesso!'),
      );
    }).catchError((error) {
      Navigator.of(context).pop();
      print('setAnticipations $error');
      _showErrorDialog();
    }).timeout(Duration(seconds: 40));
  }

  _showErrorDialog() {
    showCustomDialog(
      context: context,
      alert: customRoundAlert(
          context: context,
          title: 'Falha ao enviar requisição.\nTente novamente mais tarde.'),
    );
  }

  _showEmptyFundsDialog(amount, fee) {
    showCustomDialog(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Você ainda não possui fundos para antecipar.',
      ),
    );
  }

  _transferValues() async {
    // print('${withdrawController.numberValue}');
    var mediaQuery = MediaQuery.of(context).size.width / 400;

    await api
        .getHttpsCallable(functionName: 'getAnticipationesEstimates')
        .call(<String, dynamic>{
      'amount': withdrawController.numberValue,
    }).then((HttpsCallableResult httpResult) {
      final dynamic result = httpResult.data;
      Navigator.of(context).pop();
      print(result);
      var amount = result['amount'] * 1.0;
      var fee = result['fee'] * 1.0;
      if (amount <= 0.0) {
        _showEmptyFundsDialog(amount, fee);
      } else {
        showCustomDialog(
          context: context,
          alert: customRoundAlert(
            context: context,
            title:
                'Taxa de antecipação: R\$$fee\nValor a receber: R\$$amount\nVocê deseja antecipar esse valor de R\$$amount?',
            actions: [
              new Container(
                height: mediaQuery * 41.0,
                decoration: new BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Colors.white,
                  ),
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                child: new FlatButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _showWaitingDialog();
                    _setAnticipations(amount);
                  },
                  child: new Text(
                    'SIM',
                    style: new TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              new Container(
                height: mediaQuery * 41.0,
                decoration: new BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Colors.white,
                  ),
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                child: new FlatButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: new Text(
                    'NÃO',
                    style: new TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }
    }).catchError((error) {
      Navigator.of(context).pop();
      print('getAnticipationesEstimates $error');
      _showErrorDialog();
      return;
    }).timeout(Duration(seconds: 40));
    // final mediaQuery = MediaQuery.of(context).size.width / 400;
    // return showDialog(
    //   context: context,
    //   builder: (BuildContext context) {
    //     return AlertDialog(
    //       shape: RoundedRectangleBorder(
    //         borderRadius: BorderRadius.all(Radius.circular(mediaQuery * 32.0)),
    //       ),
    //       contentPadding: EdgeInsets.only(top: mediaQuery * 10.0),
    //       content: Container(
    //         width: mediaQuery * 300.0,
    //         height: mediaQuery * 200.0,
    //         child: Center(
    //           child: Text('Funcionalidade em manutenção!'),
    //         ),
    //       ),
    //     );
    //   },
    // );
  }

  _showWaitingDialog() {
    showCustomDialog(
      context: context,
      barrierDismissible: false,
      alert: customRoundAlert(
          context: context,
          title: 'Enviando solicitação...\nAguarde',
          actions: []),
    );
  }

  _handleSubmit() {
    _showWaitingDialog();
    _transferValues();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    TextStyle balanceStyle = TextStyle(fontSize: mediaQuery * 24.0);
    return Scaffold(
      key: _scaffoldState,
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text(
          'Antecipar Recebimento',
          style: ComooStyles.appBarTitleSixteen,
        ),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new FutureBuilder(
              future: _fetchBalance(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  // int current = snapshot.data['current']['amount'];
                  int future = snapshot.data['future']['amount'];
                  _balance = future / 100;
                  var balance = formater.format(_balance);
                  return Text(
                    'Saldo a receber: $balance',
                    style: balanceStyle,
                  );
                } else {
                  var balance = formater.format(_balance);
                  return Text(
                    'Saldo a receber: $balance',
                    style: balanceStyle,
                  );
                }
              },
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: mediaQuery * 30.0),
              child: TextField(
                controller: withdrawController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: '',
                  labelText: 'Quanto você deseja antecipar?',
                ),
              ),
            ),
            Container(
              child: OutlineButton(
                onPressed: _handleSubmit,
                child: Text(
                  'CALCULAR',
                  style: ComooStyles.botaoTurquezaChat,
                ),
                borderSide: BorderSide(color: ComooColors.turqueza),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(mediaQuery * 20.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
