import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:rxdart/rxdart.dart';

class UserSuggestionPage extends StatefulWidget {
  UserSuggestionPage({@required this.group});
  final Group group;

  @override
  _UserSuggestionPageState createState() {
    return _UserSuggestionPageState();
  }
}

class _UserSuggestionPageState extends State<UserSuggestionPage>
    with Reusables {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final CloudFunctions api = CloudFunctions.instance;

  final BehaviorSubject<bool> _isEmpty = BehaviorSubject<bool>();
  Function(bool) get changeState => _isEmpty.sink.add;
  Stream<bool> get isEmptyStream => _isEmpty.stream;
  final BehaviorSubject<List<dynamic>> _users =
      BehaviorSubject<List<dynamic>>();
  Function(dynamic) get changeUser => _users.sink.add;
  Stream<List<dynamic>> get usersStream => _users.stream;
  final BehaviorSubject<bool> _isLoading = BehaviorSubject<bool>();
  Function(bool) get changeLoading => _isLoading.sink.add;
  Stream<bool> get isLoadingStream => _isLoading.stream;

  Future<void> _handleSelected() async {
    changeState(false);
    changeLoading(true);
    final List<String> selected = _users.value
        .where((dynamic user) => user['suggested'])
        .map((dynamic user) => user['id'])
        .toList()
        .cast<String>();
    final bool isAdded = await _addSelected(selected);
    if (isAdded ?? false) {
      await showMessageDialog(
          context, 'Seus convites foram enviados com sucesso!');
      Navigator.of(context).pop();
    } else {
      showMessageDialog(context, 'Falha ao enviar os dados, tente novamente!');
      changeState(true);
      changeLoading(false);
    }
  }

  _buildEmptyList() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      // decoration: backgroundDecoration(),
      child: Container(
        padding: EdgeInsets.only(top: mediaQuery * 42.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                left: mediaQuery * 25.0,
                right: mediaQuery * 45.0,
              ),
              child: Text(
                'Todos os seus amigos já estão nesta comoonidade.\n'
                'Adicione mais amigos para poder sugerir.',
                style: TextStyle(
                  color: ComooColors.roxo,
                  fontFamily: COMOO.fonts.montSerrat,
                  fontWeight: FontWeight.normal,
                  fontSize: mediaQuery * 20.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSnapshotList(AsyncSnapshot<List<dynamic>> snapshot) {
    // print(snapshot.data.documents.length);
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final List<dynamic> documents = snapshot.data;
    // print('isEmpty? ${documents.isEmpty}');
    if (documents.isEmpty) {
      return _buildEmptyList();
    }
    changeState(true);
    return ListView.builder(
        itemCount: documents.length,
        itemBuilder: (BuildContext context, int i) {
          final dynamic doc = documents[i];
          return ListTile(
            leading: CircleAvatar(
              backgroundImage: AdvancedNetworkImage(doc['photo']),
            ),
            title: Text(
              '${doc['name']}',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.chumbo,
              ),
            ),
            subtitle: Text(
              '\@${doc['nickname']}',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.cinzaMedio,
                fontSize: mediaQuery * 12.0,
              ),
            ),
            trailing: IconButton(
              iconSize: mediaQuery * 35.0,
              icon: Container(
                // padding: EdgeInsets.only(right: mediaQuery * 25.0),
                child: Image.asset(
                  doc['suggested']
                      ? 'images/checked_turquoise.png'
                      : 'images/add_white.png',
                  alignment: Alignment.centerLeft,
                ),
              ),
              onPressed: () {
                setState(() {
                  doc['suggested'] = !doc['suggested'];
                });
              },
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: COMOO.colors.lead,
        title: const Text(
          'Convidar Amigos',
          style: ComooStyles.appBarTitleWhite,
        ),
        actions: <Widget>[
          StreamBuilder<bool>(
            initialData: false,
            stream: isEmptyStream,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              return snapshot.data
                  ? FlatButton(
                      onPressed: () {
                        _handleSelected();
                      },
                      child: Text(
                        'SUGERIR',
                        style: ComooStyles.appBar_botao_branco,
                      ))
                  : Container();
            },
          ),
        ],
      ),
      body: StreamBuilder<bool>(
        initialData: false,
        stream: isLoadingStream,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          return snapshot.data
              ? LoadingContainer()
              : StreamBuilder<List<dynamic>>(
                  stream: usersStream,
                  builder: (BuildContext context,
                      AsyncSnapshot<List<dynamic>> snapshot) {
                    // print('FriendsPicker: ${snapshot.connectionState} - has data? ${snapshot.hasData}');
                    switch (snapshot.connectionState) {
                      case ConnectionState.active:
                        return snapshot.hasData
                            ? _buildSnapshotList(snapshot)
                            : _buildEmptyList();
                        break;
                      case ConnectionState.done:
                        return snapshot.hasData
                            ? _buildSnapshotList(snapshot)
                            : _buildEmptyList();
                        break;
                      case ConnectionState.none:
                        return _buildEmptyList();
                        break;
                      case ConnectionState.waiting:
                        return snapshot.hasData
                            ? _buildSnapshotList(snapshot)
                            : LoadingContainer();
                        break;
                      default:
                        return Container();
                        break;
                    }
                  },
                );
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _fetchFriendsList();
  }

  @override
  void dispose() {
    super.dispose();
    _isLoading.close();
    _isEmpty.close();
    _users.close();
  }

  Future<void> _fetchFriendsList() async {
    if (await isConnected()) {
      // print('has connection');
      await api
          .getHttpsCallable(functionName: 'getFriendsToSuggestGroup')
          .call(<String, String>{
            'groupId': widget.group.id,
          })
          .timeout(Duration(seconds: 60))
          .then((HttpsCallableResult result) {
            // print(result);
            if (mounted) {
              changeUser(result.data);
            }
          })
          .catchError((onError) {
            print(onError);
            if (mounted) {
              changeUser(<dynamic>[]);
            }
          });
    } else {
      // print('no connection');
      changeUser(<dynamic>[]);
    }
  }

  Future<bool> _addSelected(List<String> selected) async {
    if (await isConnected()) {
      return await api
          .getHttpsCallable(functionName: 'groupSuggestion')
          .call(<String, dynamic>{
            'groupId': widget.group.id,
            'ids': selected,
          })
          .then((HttpsCallableResult result) {
            return result.data;
          })
          .timeout(Duration(seconds: 60))
          .catchError((onError) {
            print(onError);
            return false;
          });
    } else {
      return false;
    }
  }
}
