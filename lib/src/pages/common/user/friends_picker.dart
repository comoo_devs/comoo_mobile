import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
// import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:rxdart/rxdart.dart';

class FriendsPickerPage extends StatefulWidget {
  FriendsPickerPage({@required this.group});
  final Group group;

  @override
  State<StatefulWidget> createState() {
    return _FriendsPickerPageState();
  }
}

class _FriendsPickerPageState extends State<FriendsPickerPage> with Reusables {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final Firestore db = Firestore.instance;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<User> selectedFriends = List();
  final BehaviorSubject<bool> _isEmpty = BehaviorSubject<bool>();
  Function(bool) get changeState => _isEmpty.sink.add;
  Stream<bool> get isEmptyStream => _isEmpty.stream;
  final BehaviorSubject<List<dynamic>> _users =
      BehaviorSubject<List<dynamic>>();
  Function(dynamic) get changeUser => _users.sink.add;
  Stream<List<dynamic>> get usersStream => _users.stream;
  final BehaviorSubject<bool> _isLoading = BehaviorSubject<bool>();
  Function(bool) get changeLoading => _isLoading.sink.add;
  Stream<bool> get isLoadingStream => _isLoading.stream;
  final CloudFunctions api = CloudFunctions.instance;

  Future<void> _handleSelected() async {
    changeState(false);
    changeLoading(true);
    final List<String> selected = _users.value
        .where((dynamic user) => user['invited'])
        .map((dynamic user) => user['id'])
        .toList()
        .cast<String>();
    final bool isAdded = await _addSelected(selected);
    if (isAdded ?? false) {
      await showMessageDialog(
          context, 'Seus convites foram enviados com sucesso!');
      Navigator.of(context).pop();
    } else {
      showMessageDialog(context, 'Falha ao enviar os dados, tente novamente!');
      changeState(true);
      changeLoading(false);
    }
  }

  _buildEmptyList() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      decoration: backgroundDecoration(),
      child: Container(
        padding: EdgeInsets.only(top: mediaQuery * 42.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                left: mediaQuery * 25.0,
                right: mediaQuery * 45.0,
              ),
              child: Text(
                'Ainda não há usuários com interesse nessas categorias.\n'
                'Envie um link de convite para seus amigos que ainda não estão na COMOO.',
                style: TextStyle(
                  color: ComooColors.roxo,
                  fontFamily: COMOO.fonts.montSerrat,
                  fontWeight: FontWeight.normal,
                  fontSize: mediaQuery * 20.0,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: mediaQuery * 34.0),
              child: COMOO.buttons.flatButton(
                context: context,
                onPressed: () async {
                  // shareGroupLink(context, widget.group);
                  await createAndShareInviteCode(context);
                },
                height: 35.0,
                width: 280.0,
                text: 'ENVIAR LINK DE CONVITE',
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSnapshotList(AsyncSnapshot<List<dynamic>> snapshot) {
    // print(snapshot.data.documents.length);
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final List<dynamic> documents = snapshot.data;
    // print('isEmpty? ${documents.isEmpty}');
    if (documents.isEmpty) {
      return _buildEmptyList();
    }
    changeState(true);
    return ListView.builder(
        itemCount: documents.length,
        itemBuilder: (BuildContext context, int i) {
          final dynamic doc = documents[i];
          return ListTile(
            leading: CircleAvatar(
              backgroundImage: AdvancedNetworkImage(doc['photo']),
            ),
            title: Text(
              '${doc['name']}',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.chumbo,
              ),
            ),
            subtitle: Text(
              '\@${doc['nickname']}',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.cinzaMedio,
                fontSize: mediaQuery * 12.0,
              ),
            ),
            trailing: IconButton(
              iconSize: mediaQuery * 35.0,
              icon: Container(
                // padding: EdgeInsets.only(right: mediaQuery * 25.0),
                child: Image.asset(
                  doc['invited']
                      ? 'images/checked_turquoise.png'
                      : 'images/add_white.png',
                  alignment: Alignment.centerLeft,
                ),
              ),
              onPressed: () {
                setState(() {
                  doc['invited'] = !doc['invited'];
                });
              },
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: COMOO.colors.lead,
        title: const Text(
          'Perfil da COMOO',
          style: ComooStyles.appBarTitleWhite,
        ),
        actions: <Widget>[
          StreamBuilder<bool>(
            initialData: false,
            stream: isEmptyStream,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              return snapshot.data
                  ? FlatButton(
                      onPressed: () {
                        _handleSelected();
                      },
                      child: Text(
                        'AVANÇAR',
                        style: ComooStyles.appBar_botao_branco,
                      ))
                  : Container();
            },
          ),
        ],
      ),
      body: StreamBuilder<bool>(
        initialData: false,
        stream: isLoadingStream,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          return snapshot.data
              ? LoadingContainer()
              : StreamBuilder<List<dynamic>>(
                  stream: usersStream,
                  builder: (BuildContext context,
                      AsyncSnapshot<List<dynamic>> snapshot) {
                    // print('FriendsPicker: ${snapshot.connectionState} - has data? ${snapshot.hasData}');
                    switch (snapshot.connectionState) {
                      case ConnectionState.active:
                        return snapshot.hasData
                            ? _buildSnapshotList(snapshot)
                            : _buildEmptyList();
                        break;
                      case ConnectionState.done:
                        return snapshot.hasData
                            ? _buildSnapshotList(snapshot)
                            : _buildEmptyList();
                        break;
                      case ConnectionState.none:
                        return _buildEmptyList();
                        break;
                      case ConnectionState.waiting:
                        return snapshot.hasData
                            ? _buildSnapshotList(snapshot)
                            : LoadingContainer();
                        break;
                      default:
                        return Container();
                        break;
                    }
                  },
                );
        },
      ),
    );
  }

  Decoration backgroundDecoration() {
    return new BoxDecoration(
      image: new DecorationImage(
        image: AssetImage('images/paper_planes.png'),
        fit: BoxFit.cover,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _fetchUserList();
  }

  @override
  void dispose() {
    super.dispose();
    _isEmpty.close();
    _users.close();
    _isLoading.close();
  }

  Future<void> _fetchUserList() async {
    if (await isConnected()) {
      // print('has connection');
      await api
          .getHttpsCallable(functionName: 'getUsersToAddInGroup')
          .call(<String, String>{
            'groupId': widget.group.id,
          })
          .timeout(Duration(seconds: 60))
          .then((HttpsCallableResult result) {
            print(result.data);
            if (mounted) {
              changeUser(result.data);
            }
          })
          .catchError((onError) {
            print(onError);
            if (mounted) {
              changeUser(<dynamic>[]);
            }
          });
    } else {
      // print('no connection');
      changeUser(<dynamic>[]);
    }
  }

  Future<bool> _addSelected(List<String> selected) async {
    if (await isConnected()) {
      return await api
          .getHttpsCallable(functionName: 'sendInvitesFromGroup')
          .call(<String, dynamic>{
            'groupId': widget.group.id,
            'users': selected,
          })
          .then((HttpsCallableResult result) => result.data)
          .timeout(Duration(seconds: 60))
          .catchError((onError) {
            print(onError);
            return false;
          });
    } else {
      return false;
    }
  }
}
