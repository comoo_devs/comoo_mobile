import 'dart:async';

import 'package:comoo/src/widgets/group_tile.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:comoo/src/comoo/authentication.dart';

class UserDetailView extends StatefulWidget {
  final User user;

  UserDetailView(this.user, {Key key}) : super(key: key);

  @override
  State createState() {
    return UserDetailViewState();
  }
}

class UserDetailViewState extends State<UserDetailView> with Reusables {
  StreamController<List<Group>> streamController = StreamController();

  User user;
  final Firestore _db = Firestore.instance;
  bool _isFriend = false;
  bool _isLoadingUser = false;
  // bool _isLoadingGroups = false;
  bool _hasGroups = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  // bool _requestedAccess = false;
  List<String> _groupRequests = List();

  _fetchUserFriendship() async {
//    if (currentUser.friends.isEmpty) {
    await currentUser.fetchFriendReference();
//    }

    if (mounted) {
      // currentUser.friends.forEach((f) => print(f));
      // print('User ${user.uid}');
      setState(() {
        _isLoadingUser = false;
        _isFriend =
            (currentUser.friends.where((uid) => uid == user.uid).isNotEmpty);
      });
      // print('isFriend? $_isFriend');
    }
  }

  _removeFriend() async {
    String id = this.user.uid;
    await _db
        .collection('users')
        .document(currentUser.uid)
        .collection('friends')
        .document(id)
        .delete();
    currentUser.friends.remove(user.uid);
    this.showInSnackBar(
        '${user.name} foi removido da sua rede de relacionamentos.');
    if (mounted) {
      setState(() {
        currentUser.friends.remove(id);
        _isFriend = false;
      });
    }
  }

  _addAsAFriend() async {
    var friend = _db
        .collection('users')
        .document(currentUser.uid)
        .collection('friends')
        .document(this.user.uid);

    await friend.setData({
      'id': _db.collection('users').document(this.user.uid),
      'origin': 'added'
    }).then((_) {
      this.showInSnackBar(
          '${user.name} adicionado à sua rede de relacionamentos.');
      currentUser.friends.add(user.uid);
      setState(() {
        _isFriend = true;
      });
    }).catchError((error) {
      this.showInSnackBar('$error');
    });

//    var notification = _db.collection('users').document(this.user.uid).collection('notification').document();
//    notification.setData({
//      'type': ''
//    });
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(value, style: ComooStyles.textoSnackBar),
      ),
      duration: Duration(seconds: 10),
    ));
  }

  _fetchUserDetails() async {
    DocumentSnapshot snap =
        await _db.collection('users').document(user.uid).get();
    user = User.fromSnapshot(snap);
    user.groups = await user.fetchGroupReference();
    currentUser.groups = await currentUser.fetchGroupReference();
    List<Group> groups = List();
    // List<DocumentSnapshot> snaps = List();
    final groupIds = user.groups.map((uid) async {
      await _db
          .collection('groups')
          .document(uid)
          .get()
          .then((groupSnap) async {
        // print('Group: ' + groupSnap.documentID);
        Group group = Group.fromSnapshot(groupSnap);
        await _db
            .collection('groups')
            .document(groupSnap.documentID)
            .collection('users')
            .getDocuments()
            .then((groupUsersSnap) {
          groupUsersSnap.documents.forEach((u) {
            group.addUserFromSnapshot(u);
          });
          if (this.mounted) {
            setState(() {
              _hasGroups = true;
            });
          }
          groups.add(group);

          if (!streamController.isClosed) {
            streamController.sink.add(groups);
          }
        });
      });
    });
    Future.wait(groupIds).whenComplete(() => streamController.close());

    await _db.collection('users').document(currentUser.uid).get().then((user) {
      final groupRequests = user['group_requests'];
      if (groupRequests != null) {
        (groupRequests as List).forEach((groupId) {
          _groupRequests.add(groupId);
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _isLoadingUser = true;
    user = widget.user;
    _fetchUserDetails();
    _fetchUserFriendship();
  }

  @override
  void dispose() {
    super.dispose();

    streamController.close();
  }

  @override
  Widget build(BuildContext context) {
    var split = user.name.split(' ');
    String firstName = split[0];
    String lastName = split.length == 1 ? '' : split[split.length - 1];
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text(firstName + ' ' + lastName),
      ),
      body: _isLoadingUser
          ? LoadingContainer()
          : CustomScrollView(
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate(
                    <Widget>[
                      SizedBox(
                        height: mediaQuery * 30.0,
                      ),
                      Center(
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: mediaQuery * 165.0,
                              height: mediaQuery * 165.0,
                              child: Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  Container(
                                    child: Container(
                                      width: mediaQuery * 150.0,
                                      height: mediaQuery * 150.0,
                                      decoration: BoxDecoration(
                                        color: ComooColors.chumbo,
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                            color: ComooColors.chumbo),
                                        image: (user?.photoURL ?? '').isEmpty
                                            ? null
                                            : DecorationImage(
                                                fit: BoxFit.cover,
                                                image: AdvancedNetworkImage(
                                                    user.photoURL),
                                              ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      right: 0.0,
                                      top: mediaQuery * 10.0,
                                      child: GestureDetector(
                                        child: Container(
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                          ),
                                          child: Image.asset(
                                            _isFriend
                                                ? 'images/checked_turquoise.png'
                                                : 'images/add_white.png',
                                            width: mediaQuery * 35.0,
                                            height: mediaQuery * 35.0,
                                          ),
                                          // child: Icon(
                                          //   _isFriend
                                          //       ? Icons.check_circle
                                          //       : Icons.add_circle_outline,
                                          //   color: ComooColors.turqueza,
                                          //   size: mediaQuery * 35.0,
                                          // ),
                                        ),
                                        onTap: () {
                                          _isFriend
                                              ? _removeFriend()
                                              : _addAsAFriend();
                                        },
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: mediaQuery * 12.0,
                          left: mediaQuery * 100.0,
                          right: mediaQuery * 100.0,
                        ),
                        alignment: FractionalOffset.center,
                        child: Text(
                          user.name,
                          style: TextStyle(
                            fontFamily: ComooStyles.fontFamily,
                            color: ComooColors.turqueza,
                            fontSize: mediaQuery * 20.0,
                            fontWeight: FontWeight.w600,
                            letterSpacing: mediaQuery * 0.28,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      user.nickname == '' || user.nickname == null
                          ? Container()
                          : Container(
                              padding:
                                  EdgeInsets.only(bottom: mediaQuery * 28.0),
                              alignment: FractionalOffset.center,
                              child: Text(
                                '@' + user.nickname,
                                style: TextStyle(
                                  fontFamily: ComooStyles.fontFamily,
                                  color: ComooColors.chumbo,
                                  fontSize: mediaQuery * 14.0,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: mediaQuery * 0.25,
                                ),
                              ),
                            ),
                      user.description == '' || user.description == null
                          ? Container()
                          : Container(
                              padding: EdgeInsets.only(
                                bottom: mediaQuery * 28.0,
                                left: mediaQuery * 35.0,
                                right: mediaQuery * 35.0,
                              ),
                              alignment: FractionalOffset.center,
                              child: Text(
                                user.description,
                                style: TextStyle(
                                  fontFamily: ComooStyles.fontFamily,
                                  color: ComooColors.chumbo,
                                  fontSize: mediaQuery * 14.0,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: mediaQuery * 0.25,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                      _hasGroups
                          ? Container(
                              padding: EdgeInsets.only(
                                left: mediaQuery * 18.0,
                                top: mediaQuery * 8.0,
                              ),
                              child: Text(
                                'Comoonidades que participa:',
                                style: TextStyle(
                                  color: ComooColors.chumbo,
                                  fontSize: mediaQuery * 14.0,
                                  fontFamily: ComooStyles.fontFamily,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            )
                          : Container(),
                      StreamBuilder<List<Group>>(
                        stream: streamController.stream,
                        builder: (
                          BuildContext context,
                          AsyncSnapshot<List<Group>> snapshot,
                        ) {
                          // print('${snapshot.connectionState} ${snapshot.hasData}');
                          // return Container();

                          switch (snapshot.connectionState) {
                            case ConnectionState.active:
                              if (snapshot.hasData)
                                return _loadSnapshotData(snapshot);
                              return _loadEmptyData();
                              break;
                            case ConnectionState.done:
                              if (snapshot.hasData)
                                return _loadSnapshotData(snapshot);
                              return _loadEmptyData();
                              break;
                            case ConnectionState.none:
                              return _loadEmptyData();
                              break;
                            case ConnectionState.waiting:
                              if (snapshot.hasData)
                                return _loadSnapshotData(snapshot);
                              return Center(child: CircularProgressIndicator());
                              break;
                            default:
                              return Container();
                              break;
                          }
                        },
                      )
                    ],
                  ),
                )
              ],
            ),
    );
  }

  Widget _loadEmptyData() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      margin: EdgeInsets.fromLTRB(
        mediaQuery * 16.0,
        0.0,
        mediaQuery * 16.0,
        mediaQuery * 24.0,
      ),
      child: Center(
        child: RichText(
          text: TextSpan(
            text:
                '${user.name.trim().split(' ').first} ainda não possui comoonidades.\n',
            style: TextStyle(
              fontFamily: ComooStyles.fontFamily,
              color: ComooColors.cinzaMedio,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
        ),
      ),
    );
  }

  // Future<void> _goToGroupDetail(Group group) async {
  //   await Navigator.of(context).push(new MaterialPageRoute(
  //       builder: (BuildContext context) => GroupDetailView(group: group)));
  //   _fetchUserFriendship();
  // }

  Widget _loadSnapshotData(AsyncSnapshot<List<Group>> snapshot) {
    final List<Group> groups = snapshot.data ?? <Group>[];
    return ListView.builder(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: groups.length,
      itemBuilder: (BuildContext context, int index) {
        final Group group = groups[index];
        group.isMember = _isMember(group);
        return GroupTile(group: group);
      },
    );
    // final mediaQuery = MediaQuery.of(context).size.width / 400;
    // return Column(
    //   children: snapshot.data.map((Group group) {
    //     // final group = Group.fromSnapshot(document);
    //     return ListTile(
    //       onTap: () {
    //         _goToGroupDetail(group);
    //       },
    //       leading: Container(
    //         // margin: EdgeInsets.only(right: mediaQuery * 12.0),
    //         width: mediaQuery * 50.0,
    //         height: mediaQuery * 50.0,
    //         child: ClipOval(
    //           child: Image(
    //             image: AdvancedNetworkImage(group.avatar),
    //             fit: BoxFit.cover,
    //           ),
    //         ),
    //       ),
    //       title: Text(
    //         group.name,
    //         style: TextStyle(
    //           fontFamily: ComooStyles.fontFamily,
    //           color: ComooColors.chumbo,
    //           fontSize: mediaQuery * 14.0,
    //           fontWeight: FontWeight.w500,
    //           letterSpacing: mediaQuery * 0.25,
    //         ),
    //       ),
    //       subtitle: Text(
    //         group.users.length.toString() + ' membros',
    //         style: TextStyle(
    //           color: ComooColors.cinzaMedio,
    //           fontFamily: ComooStyles.fontFamily,
    //           fontSize: mediaQuery * 12.0,
    //           letterSpacing: mediaQuery * 0.25,
    //           fontWeight: FontWeight.w500,
    //         ),
    //       ),
    //       trailing: _isCurrentUserAdmin(group)
    //           ? null
    //           : FlatButton(
    //               onPressed: () {
    //                 if (_isMember(group)) {
    //                   // _requestGroupExit(group);
    //                 } else {
    //                   _requestGroupEntry(group);
    //                 }
    //               },
    //               child: Image.asset(
    //                 _isMember(group)
    //                     ? 'images/checked_turquoise.png'
    //                     : 'images/add_white.png',
    //                 width: mediaQuery * 28.0,
    //                 height: mediaQuery * 28.0,
    //               ),
    //             ),
    //     );
    //   }).toList(),
    // );
  }

  // bool _isCurrentUserAdmin(Group group) {
  //   bool isAdmin = false;
  //   isAdmin = group.userAdmin == currentUser.uid;
  //   return isAdmin;
  // }

  // Future<void> _requestGroupExit(group) async {
  //   // showInSnackBar('Funcionalidade em construção!');

  //   bool confirmLeave = await showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           content: Text(
  //             'Tem certeza que deseja deixar de participar da comoonidade?',
  //             style: ComooStyles.welcomeMessage,
  //           ),
  //           actions: <Widget>[
  //             FlatButton(
  //               child: Text('Cancelar', style: ComooStyles.botaoTurqueza),
  //               onPressed: () {
  //                 Navigator.of(context).pop(false);
  //               },
  //             ),
  //             FlatButton(
  //               child: Text('Sim', style: ComooStyles.botaoTurqueza),
  //               onPressed: () {
  //                 Navigator.of(context).pop(true);
  //               },
  //             ),
  //           ],
  //         );
  //       });

  //   if (confirmLeave) {
  //     _removeUser(group);
  //   }
  // }

  // Future<void> _removeUser(Group group) async {
  //   bool hasRequested = false;
  //   await _db
  //       .collection('users')
  //       .document(currentUser.uid)
  //       .get()
  //       .then((user) {
  //     final groupRequests = user['group_requests'];
  //     (groupRequests as List).forEach((groupId) {
  //       hasRequested = (group.id == groupId);
  //     });
  //   });
  //   if (hasRequested) {
  //     print('remove request');
  //   } else {
  //     print('remove from group');
  //     // var batch = _db.batch();
  //     // var groupUserRef = _db
  //     //     .collection('groups')
  //     //     .document(group.id)
  //     //     .collection('users')
  //     //     .document(currentUser.uid);
  //     // var userGroupRef = _db
  //     //     .collection('users')
  //     //     .document(currentUser.uid)
  //     //     .collection('groups')
  //     //     .document(group.id);

  //     // batch.delete(groupUserRef);
  //     // batch.delete(userGroupRef);
  //     // await batch.commit();
  //     // setState(() {
  //     //   currentUser.groups.remove(group.id);
  //     //   _requestedAccess = false;
  //     // });
  //   }
  // }

  bool _isMember(Group group) {
    bool isMember = false;
    isMember = currentUser.groups.contains(group.id) ||
        _groupRequests.contains(group.id);
    return isMember;
  }

  // Future<void> _requestGroupEntry(Group group) async {
  //   var groupRef = await _db.collection('groups').document(group.id).get();

  //   var batch = _db.batch();

  //   var userRef = _db.collection('users').document(currentUser.uid);
  //   var userSnapshot = await userRef.get();
  //   if (userSnapshot['group_requests'] != null) {
  //     List requests = List<String>.from(userSnapshot['group_requests']);
  //     requests.add(group.id);
  //     batch.updateData(userRef, {
  //       'group_requests': requests,
  //     });
  //   } else {
  //     batch.setData(
  //         userRef,
  //         {
  //           'group_requests': [group.id]
  //         },
  //         merge: true);
  //   }
  //   var notification = _db
  //       .collection('users')
  //       .document(groupRef['admin'])
  //       .collection('notifications')
  //       .document();

  //   batch.setData(notification, {
  //     'id': notification.documentID,
  //     'thumb': currentUser.photoURL,
  //     'from': currentUser.uid,
  //     'type': 'group_request',
  //     'name': currentUser.name,
  //     'text':
  //         'Solicitou sua permissão para entrar na comoonidade ${group.name}',
  //     'date': DateTime.now(),
  //     'group': group.id,
  //     'approved': false
  //   });
  //   currentUser.groups.add(group.id);
  //   batch.commit().then((_) {
  //     // setState(() {
  //     // _requestedAccess = true;
  //     // });
  //     showInSnackBar(
  //         'Solicitação enviada para o administrador da comoonidade.');
  //   });
  // }
}
