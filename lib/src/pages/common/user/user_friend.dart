class UserFriend {
  String uid;
  String name;
  String photoUrl;

  UserFriend({this.uid, this.name, this.photoUrl});

  UserFriend.fromSnapshot(snap)
      : uid = snap['id'],
        name = snap['name'],
        photoUrl = snap['photoURL'];
}
