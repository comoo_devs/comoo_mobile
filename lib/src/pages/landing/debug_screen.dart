import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/painters/rect_painter.dart';
import 'package:flutter/material.dart';

class DebugScreen extends StatefulWidget {
  DebugScreen();
  @override
  State<StatefulWidget> createState() => _DebugScreenState();
}

class _DebugScreenState extends State<DebugScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Placeholder(
              fallbackHeight: MediaQuery.of(context).size.height,
            ),
            CustomPaint(
              painter: RectPainter(
                color: COMOO.colors.pink,
                topLeft: 0.1694,
                topRight: 0.04281,
              ),
              child: CustomPaint(
                painter: RectPainter(
                  color: COMOO.colors.purple,
                  topRight: 0.1681,
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 35),
                  child: ListView(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    children: <Widget>[
                      Text(
                        'Já tenho conta',
                        style: TextStyle(
                          color: COMOO.colors.turquoise,
                        ),
                      ),
                      SizedBox(height: 17),
                      Text(
                        'EMAIL',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: 9),
                      TextField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          filled: true,
                          fillColor: Colors.white70,
                        ),
                      ),
                      SizedBox(height: 13),
                      Text(
                        'SENHA',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: 9),
                      TextField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          filled: true,
                          fillColor: Colors.white70,
                        ),
                      ),
                      SizedBox(height: 22),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 77),
                        child: RaisedButton(
                          elevation: 0.0,
                          onPressed: () {},
                          child: Text('CONTINUAR'),
                        ),
                      ),
                      SizedBox(height: 19),
                      Text(
                        'Esqueci meu login e senha',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Placeholder(),
            Placeholder(),
            Placeholder(),
          ],
        ),
      ),
    );
  }
}
