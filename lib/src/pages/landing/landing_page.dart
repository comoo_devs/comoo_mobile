import 'dart:ui' as UI;
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/landing/landing_login.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

import 'landing_bloc.dart';

class LandingPage extends StatefulWidget {
  LandingPage();

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  LandingBloc bloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = LandingBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LandingBloc>(
      bloc: bloc,
      child: _LandingPageView(),
    );
  }
}

class _LandingPageView extends StatelessWidget with Reusables {
  _LandingPageView();
  @override
  Widget build(BuildContext context) {
    final LandingBloc bloc = BlocProvider.of<LandingBloc>(context);
    return Scaffold(
      backgroundColor: COMOO.colors.white500,
      body: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () => hideKeyboard(context),
            child: SingleChildScrollView(
              physics: const ClampingScrollPhysics(),
              child: Container(
                constraints: BoxConstraints.tightFor(
                  width: MediaQuery.of(context).size.width,
                  // height: MediaQuery.of(context).size.height,
                ),
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    _landingButtons(context),
                    LandingPageLogin(),
                  ],
                ),
              ),
            ),
          ),
          StreamBuilder<bool>(
            stream: bloc.isLoading,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              final bool _isLoading = snapshot.data ?? false;
              return _isLoading
                  ? Center(
                      child: AnimatedOpacity(
                      opacity: _isLoading ? 1.0 : 0.0,
                      duration: Duration(milliseconds: 500),
                      child: _buildLoading(),
                    ))
                  : Container();
            },
          ),
        ],
      ),
    );
  }

  Widget _landingButtons(BuildContext context) {
    final TextStyle buttonTextStyle = Theme.of(context).textTheme.button;
    // final LandingBloc bloc = BlocProvider.of<LandingBloc>(context);
    return Container(
      color: Colors.white,
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: responsive(context, 62)),
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          SizedBox(height: MediaQuery.of(context).size.height * 0.11),
          Image.asset(
            'images/logo_fb.png',
            width: responsive(context, 302),
            height: responsive(context, 62.0),
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.058),
          Text(
            'Ainda nāo tem conta?',
            style: Theme.of(context).textTheme.body2.copyWith(
                  color: COMOO.colors.purple,
                  fontWeight: FontWeight.w600,
                  fontSize: responsive(context, 20),
                ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.022),
          Container(
            constraints: BoxConstraints(
                minWidth: responsive(context, 234),
                maxHeight: responsive(context, 34)),
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(responsive(context, 21.0)),
              ),
              onPressed: () => Navigator.of(context).pushNamed('/sign_up'),
              child: FittedBox(
                child: Text(
                  'CADASTRE-SE',
                  textAlign: TextAlign.center,
                  style: buttonTextStyle,
                ),
              ),
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.035),
          // SizedBox(height: responsive(context, 14)),
          // Text(
          //   'OU',
          //   style: Theme.of(context).textTheme.body2.copyWith(
          //         color: COMOO.colors.lead,
          //         fontWeight: FontWeight.bold,
          //       ),
          //   textAlign: TextAlign.center,
          // ),
          // SizedBox(height: responsive(context, 14)),
        ],
      ),
    );
  }

  Widget _buildLoading() {
    return ClipRect(
      child: BackdropFilter(
        filter: UI.ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration:
              BoxDecoration(color: Colors.grey.shade200.withOpacity(0.5)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  'CARREGANDO...',
                  style: ComooStyles.appBarTitle,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
