import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/comoo.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/validations.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'dart:ui' as UI;

import 'dart:async';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';

class LoginPage extends StatefulWidget {
  final Comoo comoo;

  LoginPage(this.comoo);

  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage>
    with WidgetsBindingObserver, Reusables {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  UserAuth userAuth = UserAuth();
  UserData userData = UserData();
  bool _autoValidate = false;
  Validations validations = Validations();
  final googleSignIn = GoogleSignIn();
  final auth = FirebaseAuth.instance;
  final db = Firestore.instance;

  final BehaviorSubject<bool> _showPassword = BehaviorSubject<bool>();
  Function(bool) get editShowPassword => _showPassword.sink.add;
  Stream<bool> get showPassword => _showPassword.stream;

  @override
  void dispose() {
    _showPassword?.close();
    super.dispose();
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: Text(value, style: ComooStyles.textoSnackBar),
      duration: Duration(seconds: 10),
    ));
  }

  void _resetPassword(email) async {
    userAuth.resetPassword(email).then((value) {
      showInSnackBar('E-mail enviado para: $email');
    }).catchError((onError) {
      showInSnackBar('Verifique o erro e tente novamente: $onError.message');
    });
  }

  void _showCancelledMessage() {
    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
    showInSnackBar('Você cancelou o login utilizando o Facebook.');
  }

  void _showErrorOnUI(String error) {
    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
    showInSnackBar('Verfique o erro e tente novamente: $error');
  }

  Future _onFacebookSignIn(String token) async {
    final AuthCredential credential =
        FacebookAuthProvider.getCredential(accessToken: token);
    auth.signInWithCredential(credential).then((FirebaseUser user) async {
      UserInfo userInfo = user.providerData.firstWhere(
          (UserInfo info) => info.providerId != 'firebase',
          orElse: () => null);
      if (userInfo != null) {
        await db
            .collection('users')
            .where('fbId', isEqualTo: userInfo.uid)
            .getDocuments()
            .then((QuerySnapshot query) {
          if (query.documents.isNotEmpty) {
            final User user = User.fromSnapshot(query.documents.first);
            welcomeNavigation(context, user);
            // Navigator.of(context).pushNamedAndRemoveUntil(
            //     '/home', (Route<dynamic> route) => false);
          } else {
            showInSnackBar('Não existe cadastro com esses dados de acesso.');
            setState(() {
              _isLoading = false;
            });
          }
        });
      }
    }).catchError((onError) {
      //TODO: check if user already existis and link accounts?
      _showErrorOnUI(onError.message);
    });
  }

  Future<Null> loginWithFacebook() async {
    if (mounted) {
      setState(() {
        _isLoading = true;
      });
    }
    var facebookLogin = FacebookLogin();
    bool isLoggedIn = await facebookLogin.isLoggedIn;
    if (isLoggedIn) await facebookLogin.logOut();
    var result = await facebookLogin
        .logInWithReadPermissions(['email', 'public_profile']);

    // print(result.status);
    // print(result.accessToken);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        _onFacebookSignIn(result.accessToken.token);
        break;
      case FacebookLoginStatus.cancelledByUser:
        _showCancelledMessage();
        if (mounted) {
          setState(() {
            _isLoading = false;
          });
        }
        break;
      case FacebookLoginStatus.error:
        // _showErrorOnUI(result.errorMessage);
        _showErrorOnUI('Falha ao tentar conectar-se ao Facebook.'
            ' Tente novamente mais tarde.');
        if (mounted) {
          setState(() {
            _isLoading = false;
          });
        }
        break;
    }

    setState(() {
      _isLoading = true;
    });
  }

  Future loginWithGoogle() async {
    if (mounted) {
      setState(() {
        _isLoading = true;
      });
    }
    GoogleSignInAccount gUser; // = googleSignIn.currentUser;
    bool isSignIn = await googleSignIn.isSignedIn();
    if (isSignIn) {
      await googleSignIn.signOut();
    }
    // if (gUser == null) gUser = await googleSignIn.signInSilently();
    try {
      if (gUser == null) {
        gUser = await googleSignIn.signIn().timeout(Duration(seconds: 60));
      }
      print('gUser $gUser');
    } catch (e) {
      print(e);
      print(e.message);
      showInSnackBar(
          'Ocorreu um erro ao tentar fazer o login via Google. Tente novamente mais tarde.');
    }
    print('gUser $gUser');
    if (gUser == null) {
      // showInSnackBar('Você cancelou o acesso via Google');
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
      return null;
    }
    final GoogleSignInAuthentication googleAuth =
        await gUser.authentication.catchError((error) {
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    });
    final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
    FirebaseUser user =
        await auth.signInWithCredential(credential).catchError((error) {
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    });
    UserInfo userInfo = user.providerData.firstWhere((UserInfo info) {
      print(info.providerId);
      return info.providerId != 'firebase';
    }, orElse: () => null);
    print('UID Provider: ${userInfo.uid}');

    if (userInfo != null) {
      //Check if user has register
      print('UID: ${userInfo.uid}');
      await db
          .collection('users')
          .where('googleId', isEqualTo: userInfo.uid)
          .getDocuments()
          .then((QuerySnapshot query) {
        print('Length: ${query.documents.length}');
        if (query.documents.isNotEmpty) {
          final User user = User.fromSnapshot(query.documents.first);
          welcomeNavigation(context, user);
          // Navigator.of(context).pushNamedAndRemoveUntil(
          //     '/home', (Route<dynamic> route) => false);
        } else {
          showInSnackBar('Não existe cadastro com esses dados de acesso.');
          if (mounted) {
            setState(() {
              _isLoading = false;
            });
          }
        }
      });
    }
    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  // Future<bool> _checkCompletedProfile(
  //     FirebaseUser user, String token, String accessToken) async {
  //   bool profileComplete = false;
  //   DocumentReference userDoc = db.collection('users').document(user.uid);
  //   DocumentSnapshot userSnapshot = await userDoc.get();

  //   if (userSnapshot.exists) {
  //     //Update access token
  //     await userDoc.updateData(
  //         <String, dynamic>{'token': token, 'accessToken': accessToken});

  //     if (userSnapshot.data['status'] == 'completed') {
  //       profileComplete = true;
  //     }
  //   } else {
  //     //Set user basic information
  //     await userDoc.setData(<String, dynamic>{
  //       'name': user.displayName,
  //       'email': user.email,
  //       'token': token,
  //       'accessToken': accessToken,
  //       'photoURL': user.photoUrl,
  //       'status': 'incomplete'
  //     });
  //   }

  //   return profileComplete;
  // }

  void _showForgotPasswordDialog(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    String _email = '';
    showCustomDialog(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Insira um e-mail para resgatar o seu acesso',
        subtitle: Container(
          padding: EdgeInsets.symmetric(
            horizontal: mediaQuery * 31.0,
          ),
          child: TextField(
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: mediaQuery * 18.0,
            ),
            decoration: InputDecoration(
              hintText: 'nome@email.com',
              hintStyle: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 0.65),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: ComooColors.white500,
                  style: BorderStyle.solid,
                ),
              ),
            ),
            keyboardType: TextInputType.emailAddress,
            onChanged: (String email) {
              _email = email;
            },
            onSubmitted: (value) {
              _resetPassword(value);
              Navigator.of(context).pop();
            },
          ),
        ),
        actions: [
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'ENVIAR',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                _resetPassword(_email);
              },
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _handleSubmitted() {
    if (_isLoading) return;
    setState(() {
      _isLoading = true;
    });
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autoValidate = true; // Start validating on every change.
      setState(() {
        _isLoading = false;
      });
      showInSnackBar('Verfique as informações antes de continuar.');
    } else {
      form.save();
      userAuth.verifyUser(userData).then((firebaseUser) {
        setState(() {
          _isLoading = false;
        });
        print(firebaseUser);
        if (firebaseUser != null) {
          welcomeNavigation(context, currentUser);
          // Navigator.of(context).pushNamedAndRemoveUntil(
          //     '/home', (Route<dynamic> route) => false);
        } else {
          showInSnackBar('Usuário e senha inválidos, tente novamente.');
        }
      }).catchError((onError) {
        print(onError);
        setState(() {
          _isLoading = false;
        });
        showInSnackBar('Usuário e senha inválidos, tente novamente.');
      });
    }
  }

  void signUp() {
    Navigator.of(context).pushNamed('/sign_up');
  }

  Widget _buildAuthForm() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            decoration: ComooStyles.inputDecoration('Email'),
            style: ComooStyles.inputText,
            keyboardType: TextInputType.emailAddress,
            autocorrect: false,
            validator: validations.validateEmail,
            onSaved: (String email) {
              userData.email = email;
            },
          ),
          StreamBuilder<bool>(
              stream: showPassword,
              builder: (BuildContext context, AsyncSnapshot<bool> snap) {
                final isVisible = snap.data ?? false;
                return TextFormField(
                  obscureText: !isVisible,
                  decoration: ComooStyles.inputDecoration('SENHA').copyWith(
                    suffixIcon: IconButton(
                      onPressed: () => editShowPassword(!isVisible),
                      icon: Icon(
                          isVisible ? Icons.visibility : Icons.visibility_off),
                    ),
                  ),
                  style: ComooStyles.inputText,
                  keyboardType: TextInputType.text,
                  validator: validations.validatePassword,
                  onSaved: (String password) {
                    userData.password = password;
                  },
                );
              }),
          Container(
            width: mediaQuery * 151.0,
            height: mediaQuery * 38.0,
            margin: EdgeInsets.only(bottom: mediaQuery * 15.0),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                side: BorderSide(
                  width: mediaQuery * 1.2,
                  color: ComooColors.roxo,
                ),
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
              child: Text(
                'CONTINUAR',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: ComooColors.roxo,
                  fontFamily: ComooStyles.fontFamily,
                  fontWeight: FontWeight.normal,
                  fontSize: mediaQuery * 15.0,
                  letterSpacing: mediaQuery * 0.6,
                ),
              ),
              onPressed: _handleSubmitted,
            ),
          ),
          MaterialButton(
            child: Text(
              'Esqueci meu login ou senha',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.roxo,
                fontWeight: FontWeight.bold,
                fontSize: mediaQuery * 12.0,
              ),
            ),
            onPressed: () {
              _showForgotPasswordDialog(context);
            },
          ),
        ],
      ),
    );
  }

  Widget _buildLogo() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Image.asset(
      'images/logo_fb.png',
      width: mediaQuery * 302,
      height: mediaQuery * 62.0,
    );
  }

  Widget _buildFacebookLoginButton() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      padding: EdgeInsets.only(top: mediaQuery * 46.0),
      child: RawMaterialButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(mediaQuery * 17.5)),
        onPressed: () {
          loginWithFacebook();
        },
        fillColor: ComooColors.turqueza,
        constraints: BoxConstraints.expand(
            width: mediaQuery * 252.0, height: mediaQuery * 35.0),
        child: Text(
          'ENTRAR COM FACEBOOK',
          style: ComooStyles.botao_branco,
        ),
      ),
    );
  }

  Widget _buildGoogleLoginButton() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
        padding: EdgeInsets.only(top: mediaQuery * 20.0),
        child: RawMaterialButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(mediaQuery * 17.5)),
          onPressed: () {
            loginWithGoogle();
          },
          fillColor: ComooColors.turqueza,
          constraints: BoxConstraints.expand(
              width: mediaQuery * 252.0, height: mediaQuery * 35.0),
          child: Text(
            'ENTRAR COM GOOGLE',
            style: ComooStyles.botao_branco,
          ),
        ));
  }

  Widget _buildLoading() {
    return ClipRect(
      child: BackdropFilter(
        filter: UI.ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration:
              BoxDecoration(color: Colors.grey.shade200.withOpacity(0.5)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  'CARREGANDO...',
                  style: ComooStyles.appBarTitle,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      checkDynamicLinks(context, this);
    }
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        leading: Container(),
        elevation: 0.0,
        backgroundColor: Colors.white,
        brightness: Theme.of(context).brightness,
        iconTheme: IconThemeData(color: ComooColors.chumbo),
      ),
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: mediaQuery * 35.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: mediaQuery * 32.0),
                  child: _buildLogo(),
                ),
                _buildFacebookLoginButton(),
                _buildGoogleLoginButton(),
                Container(
                  padding: EdgeInsets.symmetric(vertical: mediaQuery * 24.0),
                  child: MaterialButton(
                    child: Text(
                      'Ainda não tem conta? Cadastre-se',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: ComooStyles.fontFamily,
                        color: ComooColors.roxo,
                        fontWeight: FontWeight.bold,
                        fontSize: mediaQuery * 12.0,
                      ),
                    ),
                    onPressed: () {
                      signUp();
                    },
                  ),
                ),
                _buildAuthForm()
              ],
            ),
          ),
          _isLoading
              ? Center(
                  child: AnimatedOpacity(
                      opacity: _isLoading ? 1.0 : 0.0,
                      duration: Duration(milliseconds: 500),
                      child: _buildLoading()))
              : Container(),
        ],
      ),
    );
  }
}
