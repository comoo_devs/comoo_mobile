import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/buttons.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:comoo/src/widgets/painters/rect_painter.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

import 'landing_bloc.dart';

class LandingPageLogin extends StatelessWidget with Reusables {
  LandingPageLogin();
  @override
  Widget build(BuildContext context) {
    final LandingBloc bloc = BlocProvider.of<LandingBloc>(context);
    final InputDecoration fieldDecoration = InputDecoration(
      contentPadding: EdgeInsets.symmetric(
        horizontal: responsive(context, 15),
        vertical: responsive(context, 8),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide.none,
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide.none,
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide.none,
      ),
      filled: true,
      fillColor: Colors.white,
    );
    return CustomPaint(
      painter: RectPainter(color: Colors.white),
      child: CustomPaint(
        painter: RectPainter(
          color: COMOO.colors.white500,
          topRight: 0.15,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 35),
          child: ListView(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height * 0.042),
              Text(
                'Já tenho conta',
                style: Theme.of(context).textTheme.caption.copyWith(
                      color: COMOO.colors.lead,
                      fontWeight: FontWeight.w600,
                      fontSize: responsive(context, 18),
                    ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.017),
              // Text(
              //   'E-MAIL',
              //   style: Theme.of(context).textTheme.caption.copyWith(
              //         color: Colors.white,
              //         fontWeight: FontWeight.w500,
              //       ),
              // ),
              // SizedBox(height: responsive(context, 9)),
              InputStream(
                stream: bloc.email,
                onChanged: bloc.editEmail,
                decoration: fieldDecoration.copyWith(hintText: 'email'),
                controller: bloc.emailController,
                keyboardType: TextInputType.emailAddress,
                textCapitalization: TextCapitalization.none,
                onSubmitted: (String s) {},
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.013),
              // Text(
              //   'SENHA',
              //   style: Theme.of(context).textTheme.caption.copyWith(
              //         color: Colors.white,
              //         fontWeight: FontWeight.w500,
              //       ),
              // ),
              // SizedBox(height: responsive(context, 9)),
              StreamBuilder<bool>(
                  stream: bloc.passwordVisibility,
                  builder:
                      (BuildContext context, AsyncSnapshot<bool> snapshot) {
                    final bool isVisible = snapshot.data ?? false;
                    return Stack(
                      children: <Widget>[
                        InputStream(
                          stream: bloc.password,
                          obscureText: !isVisible,
                          onChanged: bloc.editPassword,
                          controller: bloc.passwordController,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.none,
                          onSubmitted: (String s) {},
                          decoration: fieldDecoration.copyWith(
                            hintText: 'senha',
                            contentPadding: fieldDecoration.contentPadding.add(
                                EdgeInsets.only(
                                    right: responsive(context, 32))),
                          ),
                        ),
                        Positioned(
                          right: 0,
                          top: 0,
                          bottom: 0,
                          child: IconButton(
                            iconSize: responsive(context, 24),
                            color: COMOO.colors.medianGray,
                            icon: Icon(isVisible
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () =>
                                bloc.editPasswordVisibility(!isVisible),
                          ),
                        )
                      ],
                    );
                  }),
              SizedBox(height: MediaQuery.of(context).size.height * 0.013),
              Container(
                child: RaisedButton(
                  elevation: 0.0,
                  onPressed: () async {
                    print('try login');
                    bloc.editLoading(true);
                    final String result = (await bloc.handleLogin()) ?? '';
                    bloc.editLoading(false);
                    print('result: \'$result\'');
                    switch (result) {
                      case '':
                        showSnackBarMessage(context,
                            'Verfique as informações antes de continuar.');
                        break;
                      case 'ok':
                        final User currentUser = bloc.fetchCurrentUser;
                        welcomeNavigation(context, currentUser);
                        break;
                      case 'invalid':
                        showSnackBarMessage(context,
                            'Usuário ou senha inválidos, tente novamente.');
                        break;
                      case 'not_found':
                        showSnackBarMessage(context,
                            'Não existe cadastro com esses dados de acesso.');
                        break;
                      case '':
                        showSnackBarMessage(context,
                            'Usuário desabilitado. Entre em contato com o suporte.');
                        break;
                      case 'error':
                        showSnackBarMessage(context,
                            'Erro ao tentar conectar-se. Tente novamente dentro de alguns minutos.');
                        break;
                      default:
                        break;
                    }
                  },
                  child: FittedBox(child: Text('CONTINUAR')),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.019),
              Text(
                'OU',
                style: Theme.of(context).textTheme.body2.copyWith(
                      color: COMOO.colors.purple,
                      fontWeight: FontWeight.bold,
                      fontSize: responsive(context, 14),
                    ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.029),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.098,
                ),
                height: MediaQuery.of(context).size.height * 0.055,
                child: FacebookButton(
                  onPressed: () => bloc.handleFacebookLogin(
                    onSuccess: (User user) => welcomeNavigation(context, user),
                    onInvalid: () => showSnackBarMessage(
                      context,
                      'Não existe cadastro com esses dados de acesso.',
                    ),
                    onError: () => showSnackBarMessage(
                      context,
                      'Falha ao tentar conectar-se ao Facebook. Tente novamente mais tarde.',
                    ),
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.011),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.098,
                ),
                height: MediaQuery.of(context).size.height * 0.055,
                child: GoogleButton(
                  onPressed: () => bloc.handleGoogleLogin(
                    onSuccess: (User user) => welcomeNavigation(context, user),
                    onInvalid: () => showSnackBarMessage(
                      context,
                      'Não existe cadastro com esses dados de acesso.',
                    ),
                    onError: () => showSnackBarMessage(
                      context,
                      'Falha ao tentar conectar-se ao Google. Tente novamente mais tarde.',
                    ),
                  ),
                ),
              ),

              SizedBox(height: MediaQuery.of(context).size.height * 0.028),
              GestureDetector(
                onTap: () => _showForgotPasswordDialog(context),
                child: Text(
                  'Esqueci meu login ou senha',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: COMOO.colors.purple,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.029),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showForgotPasswordDialog(BuildContext context) async {
    final LandingBloc bloc = BlocProvider.of<LandingBloc>(context);
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    String _email = '';
    await showCustomDialog<void>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Insira um e-mail para resgatar o seu acesso',
        textAlign: TextAlign.center,
        subtitle: Container(
          padding: EdgeInsets.symmetric(
            horizontal: mediaQuery * 31.0,
          ),
          child: TextField(
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: mediaQuery * 18.0,
            ),
            decoration: InputDecoration(
              hintText: 'nome@email.com',
              hintStyle: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 0.65),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: ComooColors.white500,
                  style: BorderStyle.solid,
                ),
              ),
            ),
            keyboardType: TextInputType.emailAddress,
            onChanged: (String email) {
              _email = email;
            },
            onSubmitted: (value) async {
              Navigator.of(context).pop();
              final bool result = await bloc.resetPassword(value);
              if (result ?? false) {
                showSnackBarMessage(context, 'E-mail enviado para: $_email');
              } else {
                showSnackBarMessage(context, 'Erro ao enviar para o $_email');
              }
            },
          ),
        ),
        actions: [
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'ENVIAR',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                Navigator.of(context).pop();
                final result = await bloc.resetPassword(_email);
                if (result ?? false) {
                  showSnackBarMessage(context, 'E-mail enviado para: $_email');
                } else {
                  showSnackBarMessage(context, 'Erro ao enviar para o $_email');
                }
              },
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
