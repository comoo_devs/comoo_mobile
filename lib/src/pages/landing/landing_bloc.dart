import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/bloc_base.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/api/network.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rxdart/rxdart.dart';

class LandingBloc implements BlocBase {
  final NetworkApi api = NetworkApi();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final BehaviorSubject<String> _email = BehaviorSubject<String>();
  final BehaviorSubject<String> _password = BehaviorSubject<String>();
  final BehaviorSubject<bool> _isVisible = BehaviorSubject<bool>();
  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>();

  Function(String) get editEmail => _email.sink.add;
  Function(String) get editPassword => _password.sink.add;
  Function(bool) get editPasswordVisibility => _isVisible.sink.add;
  Function(bool) get editLoading => _loading.sink.add;

  Stream<bool> get passwordVisibility => _isVisible.stream;
  Stream<String> get email => _email.stream;
  Stream<String> get password => _password.stream;
  Stream<bool> get isLoading => _loading.stream;

  @override
  void dispose() {
    _email?.close();
    _password?.close();
    _isVisible?.close();
    _loading?.close();
  }

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  User _user;

  User get fetchCurrentUser => _user;

  Future<bool> resetPassword(String _email) async {
    try {
      await _auth.sendPasswordResetEmail(email: _email);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<String> handleLogin() async {
    print(
        'Trying to login with: ${emailController?.text} and ${passwordController?.text}');
    final String emailValue = emailController?.text ?? '';
    final String passValue = passwordController?.text ?? '';
    if (emailValue.isEmpty || passValue.isEmpty) {
      return '';
    }
    try {
      final FirebaseUser fireUser = await _auth.signInWithEmailAndPassword(
          email: emailValue, password: passValue);
      if (fireUser != null) {
        _user = await api.fetchCurrentUser();
        return 'ok';
      } else {
        return 'error';
      }
    } on PlatformException catch (e) {
      print(e);
      switch (e.code) {
        case 'ERROR_INVALID_EMAIL':
        case 'ERROR_WRONG_PASSWORD':
          return 'invalid';
        case 'ERROR_USER_NOT_FOUND':
          return 'not_found';
        case 'ERROR_USER_DISABLED':
          return 'disabled';
        case 'ERROR_TOO_MANY_REQUESTS':
          return 'too_many_requests';
        default:
          return 'error';
      }
    } catch (e) {
      print(e);
      return 'error';
    }
  }

  Future<void> handleFacebookLogin({
    Function(User) onSuccess,
    Function onInvalid,
    Function onError,
  }) async {
    editLoading(true);
    var facebookLogin = FacebookLogin();
    bool isLoggedIn = await facebookLogin.isLoggedIn;
    if (isLoggedIn) await facebookLogin.logOut();
    var result = await facebookLogin
        .logInWithReadPermissions(['email', 'public_profile']);

    // print(result.status);
    // print(result.accessToken);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        // _onFacebookSignIn(result.accessToken.token);
        final String token = result.accessToken.token;

        final AuthCredential credential =
            FacebookAuthProvider.getCredential(accessToken: token);
        _auth.signInWithCredential(credential).then((FirebaseUser user) async {
          UserInfo userInfo = user.providerData.firstWhere(
              (UserInfo info) => info.providerId != 'firebase',
              orElse: () => null);
          if (userInfo != null) {
            await Firestore.instance
                .collection('users')
                .where('facebookID', isEqualTo: userInfo.uid)
                .getDocuments()
                .then((QuerySnapshot query) {
              if (query.documents.isNotEmpty) {
                final User user = User.fromSnapshot(query.documents.first);
                onSuccess(user);
                // welcomeNavigation(context, user);
                // Navigator.of(context).pushNamedAndRemoveUntil(
                //     '/home', (Route<dynamic> route) => false);
              } else {
                // showInSnackBar('Não existe cadastro com esses dados de acesso.');
                editLoading(false);
                onInvalid();
              }
            });
          }
        }).catchError((onError) {
          //TODO: check if user already existis and link accounts?
          // _showErrorOnUI(onError.message);
        });
        break;
      case FacebookLoginStatus.cancelledByUser:
        // _showCancelledMessage();

        editLoading(false);
        break;
      case FacebookLoginStatus.error:
        // _showErrorOnUI(result.errorMessage);
        // _showErrorOnUI('Falha ao tentar conectar-se ao Facebook.'
        //     ' Tente novamente mais tarde.');

        editLoading(false);
        onError();
        break;
    }

    editLoading(false);
  }

  Future<void> handleGoogleLogin({
    Function(User) onSuccess,
    Function onInvalid,
    Function onError,
  }) async {
    editLoading(true);
    final GoogleSignIn googleSignIn = GoogleSignIn();
    GoogleSignInAccount gUser; // = googleSignIn.currentUser;
    bool isSignIn = await googleSignIn.isSignedIn();
    if (isSignIn) {
      await googleSignIn.signOut();
    }
    // if (gUser == null) gUser = await googleSignIn.signInSilently();
    try {
      if (gUser == null) {
        gUser = await googleSignIn.signIn().timeout(Duration(seconds: 60));
      }
      print('gUser $gUser');
    } catch (e) {
      print(e);
      print(e.message);
      onError();
      // showInSnackBar(
      //     'Ocorreu um erro ao tentar fazer o login via Google. Tente novamente mais tarde.');
    }
    print('gUser $gUser');
    if (gUser == null) {
      // showInSnackBar('Você cancelou o acesso via Google');

      editLoading(false);
      return null;
    }
    final GoogleSignInAuthentication googleAuth =
        await gUser.authentication.catchError((error) {
      editLoading(false);
    });
    final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
    FirebaseUser user =
        await _auth.signInWithCredential(credential).catchError((error) {
      editLoading(false);
    });
    UserInfo userInfo = user.providerData.firstWhere((UserInfo info) {
      print(info.providerId);
      return info.providerId != 'firebase';
    }, orElse: () => null);
    print('UID Provider: ${userInfo.uid}');

    if (userInfo != null) {
      //Check if user has register
      print('UID: ${userInfo.uid}');
      await Firestore.instance
          .collection('users')
          .where('googleID', isEqualTo: userInfo.uid)
          .getDocuments()
          .then((QuerySnapshot query) {
        print('Length: ${query.documents.length}');
        if (query.documents.isNotEmpty) {
          final User user = User.fromSnapshot(query.documents.first);
          onSuccess(user);
          // welcomeNavigation(context, user);
          // Navigator.of(context).pushNamedAndRemoveUntil(
          //     '/home', (Route<dynamic> route) => false);
        } else {
          // showInSnackBar('Não existe cadastro com esses dados de acesso.');
          onInvalid();
          editLoading(false);
        }
      });
    }

    editLoading(false);
  }
}
