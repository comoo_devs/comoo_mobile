import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/validations.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/utility/controller.dart';

class RequestAccessPage extends StatefulWidget {
  @override
  State createState() {
    return _RequestAcessPageState();
  }
}

class _RequestAcessPageState extends State<RequestAccessPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Validations _validations = Validations();
  UserData _userData = UserData();
  String _phone = "";
  bool _isLoading = false;
  bool _autoValidate = false;

  void _showDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Sucesso!'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Seus dados foram enviados com sucesso :)')
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('FECHAR'),
                onPressed: () {
                  // askInviteCode = true;
                  Navigator.of(context).pushReplacementNamed('/landing_page');
                },
              )
            ],
          );
        });
  }

  void _handleSubmitted(BuildContext context) async {
    setState(() {
      _isLoading = true;
    });
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autoValidate = true;
      setState(() {
        _isLoading = false;
      });
    } else {
      form.save();
      Firestore.instance
          .collection('access_request')
          .document()
          .setData(<String, dynamic>{
        'name': _userData.displayName,
        'email': _userData.email,
        'phone': _phone
      }).then((value) {
        setState(() {
          _isLoading = false;
        });
        _showDialog(context);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        iconTheme: IconThemeData(color: ComooColors.roxo),
        centerTitle: true,
        textTheme: TextTheme(
            title: TextStyle(
          fontSize: mediaQuery * 18.0,
          fontWeight: FontWeight.bold,
          color: ComooColors.cinzaMedio,
        )),
        title: Text("LISTA DE ESPERA"),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.only(
          left: mediaQuery * 36.0,
          right: mediaQuery * 34.0,
          top: mediaQuery * 73.0,
          bottom: mediaQuery * 52.0,
        ),
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  bottom: mediaQuery * 54.0,
                ),
                child: RichText(
                  text: TextSpan(
                    text:
                        'Insira seus dados para entrar na lista de espera. Em breve você vai compartilhar alegria aqui na ',
                    style: TextStyle(
                      fontFamily: ComooStyles.fontFamily,
                      fontSize: mediaQuery * 20.0,
                      fontWeight: FontWeight.w500,
                      color: ComooColors.roxo,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "COMOO",
                        style: TextStyle(
                          fontFamily: ComooStyles.fontFamily,
                          fontSize: mediaQuery * 20.0,
                          fontWeight: FontWeight.bold,
                          color: ComooColors.roxo,
                        ),
                      ),
                      TextSpan(
                        text: ".",
                        style: TextStyle(
                          fontFamily: ComooStyles.fontFamily,
                          fontSize: mediaQuery * 20.0,
                          fontWeight: FontWeight.w500,
                          color: ComooColors.roxo,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                child: TextFormField(
                  decoration: ComooStyles.inputDecoration("NOME"),
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: ComooColors.chumbo,
                    fontWeight: FontWeight.w500,
                    fontSize: mediaQuery * 15.0,
                  ),
                  keyboardType: TextInputType.text,
                  validator: _validations.validateName,
                  onSaved: (String name) {
                    _userData.displayName = name;
                  },
                ),
              ),
              Container(
                child: TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: ComooStyles.inputDecoration("E-MAIL"),
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: ComooColors.chumbo,
                    fontWeight: FontWeight.w500,
                    fontSize: mediaQuery * 15.0,
                  ),
                  validator: _validations.validateEmail,
                  onSaved: (String email) {
                    _userData.email = email;
                  },
                ),
              ),
              Container(
                child: TextFormField(
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: ComooColors.chumbo,
                    fontWeight: FontWeight.w500,
                    fontSize: mediaQuery * 15.0,
                  ),
                  decoration: ComooStyles.inputDecoration("celular"),
                  // initialValue: this._phone,
                  keyboardType: TextInputType.number,
                  controller: ComooController.areaCodeAndPhoneNumberController,
                  onSaved: (String phone) {
                    _phone = phone.trim();
                  },
                  validator: _validations.validatePhone,
                ),
              ),
              _isLoading
                  ? CircularProgressIndicator()
                  : OutlineButton(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(mediaQuery * 20.0)),
                      borderSide: BorderSide(color: ComooColors.roxo),
                      highlightElevation: mediaQuery * 10.0,
                      padding: EdgeInsets.symmetric(
                        horizontal: mediaQuery * 43.0,
                        vertical: mediaQuery * 8.0,
                      ),
                      child: Text(
                        "ENVIAR",
                        style: TextStyle(
                          color: ComooColors.roxo,
                          fontFamily: ComooStyles.fontFamily,
                          fontSize: mediaQuery * 15.0,
                          letterSpacing: mediaQuery * 0.6,
                        ),
                      ),
                      onPressed: () {
                        _handleSubmitted(context);
                      },
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
