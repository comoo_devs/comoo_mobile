import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/services.dart';

bool askInviteCode = false;

class OldLandingPage extends StatefulWidget {
  @override
  State createState() {
    return _OldLandingPageState();
  }
}

class _OldLandingPageState extends State<OldLandingPage>
    with WidgetsBindingObserver, Reusables {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  // final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  UserData newUser = UserData();
  // Validations _validations = Validations();

  // bool _autoValidate = false;
  bool _isLoading = false;
  String _loadMessage = 'Carregando ...';
  final Firestore db = Firestore.instance;

  final Connectivity connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> subscription;
  final Flushbar connectionError = Flushbar(
    flushbarPosition: FlushbarPosition.TOP,
    title: 'Erro de conexão',
    message: 'Sem conexão com a internet!',
    backgroundColor: Colors.redAccent,
    boxShadows: <BoxShadow>[
      BoxShadow(color: Colors.red[800]),
    ],
    icon: Icon(
      Icons.error_outline,
      color: Colors.white,
    ),
    // mainButton: FlatButton(
    //   child: Icon(
    //     Icons.close,
    //     color: COMOO.colors.lead,
    //   ),
    //   onPressed: () {
    //     if (!connectionError.isDismissed()) {
    //       connectionError.dismiss();
    //     }
    //   },
    // ),
  );

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text(value, style: ComooStyles.textoSnackBar)));
  }

  // Future<String> _validateCode(String value) async {
  //   Query query = db
  //       .collection('invites')
  //       .where('code', isEqualTo: value.toUpperCase())
  //       .limit(1);
  //   return query.getDocuments().then((querySnapshots) {
  //     if (querySnapshots.documents.isNotEmpty) {
  //       return querySnapshots.documents[0].data['uid'];
  //     } else {
  //       return '';
  //     }
  //   });
  // }

  _handleSignUp() async {
    Navigator.of(context).pushNamed('/sign_up');
    // setState(() {
    //   _isLoading = true;
    // });
    // final FormState form = _formKey.currentState;

    // if (!form.validate()) {
    //   _autoValidate = true;
    //   showInSnackBar('Verifique os erros antes de continuar.');
    //   setState(() {
    //     _isLoading = false;
    //   });
    // } else {
    //   form.save();
    // String originator = await _validateCode(newUser.invitationCode);
    //   if (originator.isEmpty || originator == '') {
    //     showInSnackBar('Este código de convite é invalido.');
    //     setState(() {
    //       _isLoading = false;
    //     });
    //   } else {
    //     origin = originator;
    //     setState(() {
    //       _isLoading = false;
    //     });
    //     Navigator.of(context).pushNamed('/sign_up');
    //   }
    // }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ));
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    final whiteButtonStyle = TextStyle(
      color: Colors.white,
      fontFamily: ComooStyles.fontFamily,
      fontWeight: FontWeight.w500,
      fontSize: mediaQuery * 15.0,
      letterSpacing: mediaQuery * 0.6,
    );
    final tealButtonStyle = TextStyle(
      color: ComooColors.turqueza,
      fontFamily: ComooStyles.fontFamily,
      fontWeight: FontWeight.w500,
      fontSize: mediaQuery * 15.0,
      letterSpacing: mediaQuery * 0.6,
    );
    return Scaffold(
      key: _scaffoldKey,
      body: _isLoading
          ? LoadingContainer(
              message: _loadMessage,
            )
          : SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: mediaQuery * 35.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      top: mediaQuery * 109.0,
                      bottom: mediaQuery * 58.0,
                    ),
                    child: Image.asset(
                      'images/logo_fb.png',
                      width: mediaQuery * 302,
                      height: mediaQuery * 62.0,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: mediaQuery * 5.0),
                    padding: EdgeInsets.only(bottom: mediaQuery * 26.0),
                    alignment: FractionalOffset.center,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: 'Bem-vindo à sua rede social de compra e venda!',
                        style: TextStyle(
                          fontFamily: ComooStyles.fontFamily,
                          color: ComooColors.roxo,
                          fontWeight: FontWeight.bold,
                          fontSize: mediaQuery * 22.0,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: RawMaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(mediaQuery * 21.0)),
                      onPressed: _handleSignUp,
                      fillColor: ComooColors.turqueza,
                      constraints: BoxConstraints.expand(
                        width: mediaQuery * 181.0,
                        height: mediaQuery * 42.0,
                      ),
                      child: Text(
                        'CADASTRE-SE',
                        style: whiteButtonStyle,
                      ),
                    ),
                  ),
                  // Container(
                  //   padding: EdgeInsets.only(bottom: mediaQuery * 59.0),
                  //   child: Form(
                  //     key: _formKey,
                  //     autovalidate: _autoValidate,
                  //     child: Column(
                  //       crossAxisAlignment: CrossAxisAlignment.center,
                  //       mainAxisAlignment: MainAxisAlignment.start,
                  //       children: <Widget>[
                  //         TextFormField(
                  //           decoration: ComooStyles.inputDecoration(
                  //               'Digite seu código de convite'),
                  //           style: ComooStyles.inputText,
                  //           keyboardType: TextInputType.emailAddress,
                  //           validator: _validations.validateInviteCode,
                  //           onSaved: (String code) {
                  //             newUser.invitationCode = code.trim();
                  //           },
                  //         ),
                  //         RawMaterialButton(
                  //           shape: RoundedRectangleBorder(
                  //               borderRadius:
                  //                   BorderRadius.circular(mediaQuery * 21.0)),
                  //           onPressed: _handleSignUp,
                  //           fillColor: ComooColors.turqueza,
                  //           constraints: BoxConstraints.expand(
                  //             width: mediaQuery * 181.0,
                  //             height: mediaQuery * 42.0,
                  //           ),
                  //           child: Text(
                  //             'CONTINUAR',
                  //             style: whiteButtonStyle,
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  // Container(
                  //   child: askInviteCode
                  //       ? RawMaterialButton(
                  //           shape: RoundedRectangleBorder(
                  //             borderRadius:
                  //                 BorderRadius.circular(mediaQuery * 20.0),
                  //           ),
                  //           fillColor: ComooColors.cinzaClaro,
                  //           onPressed: null,
                  //           constraints: BoxConstraints.expand(
                  //             width: mediaQuery * 302.0,
                  //             height: mediaQuery * 42.0,
                  //           ),
                  //           child: Text(
                  //             'PEDIR MEU CONVITE',
                  //             style: whiteButtonStyle,
                  //           ),
                  //         )
                  //       : FlatButton(
                  //           shape: RoundedRectangleBorder(
                  //             side: BorderSide(
                  //               width: mediaQuery * 1.2,
                  //               color: ComooColors.turqueza,
                  //             ),
                  //             borderRadius:
                  //                 BorderRadius.circular(mediaQuery * 20.0),
                  //           ),
                  //           onPressed: () {
                  //             Navigator.of(context)
                  //                 .pushNamed('/request_access');
                  //           },
                  //           child: Text(
                  //             'PEDIR MEU CONVITE',
                  //             style: tealButtonStyle,
                  //           ),
                  //         ),
                  // ),
                  Container(
                    child: FlatButton(
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          width: mediaQuery * 1.2,
                          color: ComooColors.turqueza,
                        ),
                        borderRadius: BorderRadius.circular(mediaQuery * 20.0),
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/login');
                      },
                      child: Text(
                        'JÁ TENHO CADASTRO',
                        style: tealButtonStyle,
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  @override
  void dispose() {
    super.dispose();

    subscription.cancel();
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    // connectionError.mainButton = FlatButton(
    //   child: Icon(
    //     Icons.close,
    //     color: COMOO.colors.lead,
    //   ),
    //   onPressed: () {
    //     if (!connectionError.isDismissed()) {
    //       connectionError.dismiss();
    //     }
    //   },
    // );
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      print(result);
      switch (result) {
        case ConnectivityResult.mobile:
        case ConnectivityResult.wifi:
          if (!connectionError.isDismissed()) {
            connectionError.dismiss();
          }
          break;
        case ConnectivityResult.none:
        default:
          connectionError.show(context);
          break;
      }
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      checkDynamicLinks(context, this);
    }
  }
}
