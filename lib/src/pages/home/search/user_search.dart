import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:comoo/src/utility/responsiveness.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';

class UserSearchPage extends StatelessWidget {
  UserSearchPage(this.stream);

  final Stream<List<UserSearch>> stream;

  @override
  Widget build(BuildContext context) {
    final iPhone8Responsive responsive = iPhone8Responsive(context);
    return Scaffold(
      body: StreamBuilder<List<UserSearch>>(
        stream: stream,
        builder: (context, AsyncSnapshot<List<UserSearch>> snapshot) {
          print(snapshot.data);
//          if (_isUserSearchActive) {
//            return Container(
//              alignment: FractionalOffset.center,
//              child: CircularProgressIndicator(),
//            );
//          }
//          if (_searchText == '') {
//            return Container(
//              alignment: FractionalOffset.center,
//              padding: EdgeInsets.all(responsive.width(48.0)),
//              child: Text(
//                'Pode explorar! Faça uma busca por pessoas ou comoonidades.',
//                textAlign: TextAlign.center,
//                style: ComooStyles.botaoTurqueza,
//              ),
//            );
//          }
          if (snapshot.hasData) {
            if (snapshot.data.isEmpty) {
              return Container(
                alignment: FractionalOffset.center,
                padding: EdgeInsets.all(responsive.width(48.0)),
                child: Text(
                  'Não existem usuários a partir desta busca :(',
                  textAlign: TextAlign.center,
                  style: ComooStyles.botaoRoxo,
                ),
              );
            }
            return _userList(context, snapshot.data);
          }
//          if (_users.isNotEmpty) {
//            return _userList(context, _users);
//          }
          final TextStyle style = TextStyle(
            fontFamily: COMOO.fonts.montSerrat,
            color: COMOO.colors.turquoise,
            fontWeight: FontWeight.w500,
            fontSize: responsive.width(20),
          );
          return Container(
            alignment: FractionalOffset.center,
            padding: EdgeInsets.all(responsive.width(32.0)),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: 'Pode explorar!',
                style: style.copyWith(
                  fontWeight: FontWeight.w600,
                  fontSize: responsive.width(26),
                ),
                children: <InlineSpan>[
                  TextSpan(
                    text: '\n',
                    style: style,
                  ),
                  TextSpan(
                    text:
                        'Faça uma busca por pessoas, produtos ou comoonidades',
                    style: style,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _userList(BuildContext context, List<UserSearch> users) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return Divider();
      },
      addRepaintBoundaries: true,
      padding: EdgeInsets.only(top: mediaQuery * 10.0),
      itemCount: users.length,
      itemBuilder: (BuildContext context, int index) {
        UserSearch user = users[index];
        return ListTile(
          contentPadding: EdgeInsets.symmetric(
            horizontal: mediaQuery * 10.0,
          ),
          leading: GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: ComooColors.cinzaClaro),
                  borderRadius: BorderRadius.circular(mediaQuery * 30.0),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AdvancedNetworkImage(user.photoURL),
                  )),
              width: mediaQuery * 55.0,
              height: mediaQuery * 55.0,
            ),
            onTap: () {
              _openUserInfo(context, user);
            },
          ),
          title: GestureDetector(
            child: Text(
              user.name,
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.chumbo,
                fontWeight: FontWeight.w600,
                fontSize: 12.0,
              ),
            ),
            onTap: () {
              _openUserInfo(context, user);
            },
          ),
          subtitle: GestureDetector(
            child: Text(
              '@${user.nickname}',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.cinzaMedio,
                fontSize: mediaQuery * 12.0,
                fontWeight: FontWeight.w500,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            onTap: () {
              _openUserInfo(context, user);
            },
          ),
          trailing: Container(
            padding: EdgeInsets.only(right: mediaQuery * 25.0),
            child: IconButton(
              iconSize: mediaQuery * 50.0,
              // icon: Icon(
              //   user.isFriend ? Icons.check_circle : Icons.add_circle_outline,
              //   color: ComooColors.turqueza,
              // ),
              icon: Image.asset(
                user.isFriend
                    ? 'images/checked_turquoise.png'
                    : 'images/add_white.png',
                width: mediaQuery * 30.0,
                height: mediaQuery * 30.0,
              ),
              onPressed: () {
//                user.isFriend ? _removeFriend(user) : _addFriend(user);
              },
            ),
          ),
          // onTap: () {
          //   user.isFriend ? print('remove friend') : print('add friend');
          //   setState(() {});
          // Navigator.of(context).push(new MaterialPageRoute(
          //     builder: (context) => GroupChatPage(_groups[index])));
          // },
        );
      },
    );
  }

  Future<void> _openUserInfo(
      BuildContext context, UserSearch userSearch) async {
    final Firestore _db = Firestore.instance;
    final DocumentSnapshot snap =
        await _db.collection('users').document(userSearch.id).get();
    final User user = User.fromSnapshot(snap);

    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => UserDetailView(user)));
  }
}
