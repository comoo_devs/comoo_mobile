import 'package:comoo/src/utility/responsiveness.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';

class SearchHero extends StatelessWidget {
  const SearchHero({
    Key key,
    this.isActive = false,
    this.width,
    this.focus,
    this.stream,
    this.onChanged,
    this.controller,
  }) : super(key: key);

  final bool isActive;
  final double width;
  final FocusNode focus;
  final Stream<String> stream;
  final ValueChanged onChanged;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    final iPhone8Responsive responsive = iPhone8Responsive(context);
    final Widget searchIcon = Image.asset(
      'images/search_black.png',
      height: responsive.height(10),
      width: responsive.width(10),
      color: ComooColors.chumbo,
    );
    final InputDecoration decoration = InputDecoration(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(
          responsive.width(13),
        ),
      ),
      contentPadding: EdgeInsets.only(
        bottom: responsive.height(10),
      ),
      prefixIcon: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: responsive.width(2),
          vertical: responsive.height(5),
        ),
        child: searchIcon,
      ),
    );
    return SizedBox(
      width: width,
      height: responsive.height(25),
      child: GestureDetector(
        onTap:
            isActive ? null : () => Navigator.of(context).pushNamed('/search'),
        child: Hero(
          tag: '#hero/search',
          child: Material(
            color: Colors.transparent,
            child: Theme(
              data: Theme.of(context),
              child: isActive
                  ? StreamBuilder<String>(
                      stream: stream,
                      builder: (BuildContext context,
                          AsyncSnapshot<String> snapshot) {
                        return TextField(
                          focusNode: focus,
                          onChanged: onChanged,
                          controller: controller,
                          decoration: decoration,
                          autofocus: false,
                          style: TextStyle(
                            fontFamily: COMOO.fonts.montSerrat,
                            fontSize: responsive.width(14),
                            color: COMOO.colors.lead,
                            fontWeight: FontWeight.w500,
                            letterSpacing: responsive.width(0.125),
                          ),
                        );
                      })
                  : searchIcon,
            ),
          ),
        ),
      ),
    );
  }
}
