import 'dart:async';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/blocs/generic_bloc.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/utility/responsiveness.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'group_search.dart';
import 'product_search.dart';
import 'search_hero.dart';
import 'user_search.dart';

class SearchPage extends StatefulWidget {
  SearchPage({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage>
    with Reusables, TickerProviderStateMixin {
  TabController _tabController;
  final FocusNode searchFocusNode = FocusNode();
  final TextEditingController searchController = TextEditingController();

  final GenericBloc<bool> loading = GenericBloc<bool>();
  final GenericBloc<String> search = GenericBloc<String>();
  final GenericBloc<List<ProductSearch>> productSearch =
      GenericBloc<List<ProductSearch>>();
  final GenericBloc<List<UserSearch>> userSearch =
      GenericBloc<List<UserSearch>>();
  final GenericBloc<List<GroupSearch>> groupSearch =
      GenericBloc<List<GroupSearch>>();
  List<TabModel> tabList;
  StreamSubscription<String> searchDebouncedSubs;

  @override
  void initState() {
    super.initState();

    _tabController = TabController(vsync: this, length: 3);
    searchDebouncedSubs = search.debounce().listen(_listener);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

//    searchFocusNode.requestFocus();

    final iPhone8Responsive responsive = iPhone8Responsive(context);
  }

  @override
  void dispose() {
    super.dispose();

    searchDebouncedSubs?.cancel();
    search?.dispose();
    userSearch?.dispose();
    groupSearch?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final iPhone8Responsive responsive = iPhone8Responsive(context);
    return GestureDetector(
      onTap: () => hideKeyboard(context),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0.0,
          brightness: Theme.of(context).brightness,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          titleSpacing: responsive.width(10),
          centerTitle: false,
          title: SearchHero(
            width: responsive.width(270),
            isActive: true,
            focus: searchFocusNode,
            stream: search.stream,
            onChanged: search.add,
            controller: searchController,
//            leading: PopupMenuButton<String>(
//              child: Image.asset(
//                'images/search_black.png',
//                height: responsive.height(10),
//                width: responsive.width(10),
//                color: ComooColors.chumbo,
//              ),
//              onSelected: (String value) {
//                searchController.clear();
//                searchFocusNode.unfocus();
//                switch (value) {
//                  case 'product':
//                    _tabController.animateTo(0);
//                    break;
//                  case 'users':
//                    _tabController.animateTo(1);
//                    break;
//                  case 'groups':
//                    _tabController.animateTo(2);
//                    break;
//                }
//              },
//              itemBuilder: (BuildContext context) {
//                return [
//                  PopupMenuItem(
//                    value: 'product',
//                    child: CheckboxListTile(
//                      value: _tabController.index == 0,
//                      title: Text('Produtos'),
//                    ),
//                  ),
//                  PopupMenuItem(
//                    value: 'users',
//                    child: CheckboxListTile(
//                      value: _tabController.index == 1,
//                      title: Text('Pessoas'),
//                    ),
//                  ),
//                  PopupMenuItem(
//                    value: 'groups',
//                    child: CheckboxListTile(
//                      value: _tabController.index == 2,
//                      title: Text('Comoonidades'),
//                    ),
//                  ),
//                ];
//              },
//              offset: Offset(0, responsive.height(40)),
//            ),
          ),
          actions: <Widget>[
            IconButton(
              iconSize: responsive.width(25),
              icon: Icon(
                Icons.close,
                color: ComooColors.chumbo,
              ),
              color: ComooColors.chumbo,
              onPressed: () {
                searchFocusNode.unfocus();
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
        body: StreamBuilder<bool>(
            stream: loading.stream,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              final bool isLoading = snapshot.data ?? false;
//              print('IS LOADING? $isLoading');
              if (isLoading) {
                return LoadingContainer();
              }
              final TextStyle styles = ComooStyles.notificationTitle.copyWith(
                fontSize: responsive.width(12),
              );
              tabList = <TabModel>[
                TabModel(
                  tab: Tab(
                    child: Text(
                      'PRODUTOS',
                      style: styles,
                    ),
                  ),
                  view: ProductSearchPage(productSearch.stream),
                ),
                TabModel(
                  tab: Tab(
                    child: Text(
                      'PESSOAS',
                      style: styles,
                    ),
                  ),
                  view: UserSearchPage(userSearch.stream),
                ),
                TabModel(
                  tab: Tab(
                    child: Text(
                      'COMOONIDADES',
                      style: styles,
                    ),
                  ),
                  view: GroupSearchPage(groupSearch.stream),
                ),
              ];
              return DefaultTabController(
                length: tabList.length,
                initialIndex: 0,
                child: Column(
                  children: <Widget>[
                    TabBar(
                      controller: _tabController,
                      labelColor: ComooColors.roxo,
                      indicatorColor: ComooColors.roxo,
                      indicatorWeight: responsive.width(5.0),
                      labelPadding: EdgeInsets.all(0),
                      onTap: (int value) {
                        searchController.clear();
                        searchFocusNode.unfocus();
                        _tabController.animateTo(value);
                      },
                      tabs: tabList.map((TabModel tb) => tb.tab).toList(),
                    ),
//                    CupertinoSegmentedControl(
//                      children: <dynamic, Widget>{
//                        0: Text('Produtos'),
//                        1: Text('Pessoas'),
//                        2: Text('Comoonidades'),
//                      },
//                      onValueChanged: (dynamic value) => _tabController.animateTo(
//                        value,
//                      ),
//                    ),
                    Expanded(
                      child: TabBarView(
                        controller: _tabController,
                        physics: const NeverScrollableScrollPhysics(),
                        children: List<Widget>.from(tabList.map<Widget>(
                          (TabModel tb) => tb.view,
                        )),
                      ),
                    ),
                  ],
                ),
              );
            }),
      ),
    );
  }

  Future<void> _listener(String value) async {
    print(value);
    final String text = search.value ?? '';
    loading.add(true);
    switch (_tabController.index) {
      case 0:
        await _searchProducts(text);
        break;
      case 1:
        await _searchUsers(text);
        break;
      case 2:
        await _searchGroups(text);
        break;
    }
    loading.add(false);
  }

  Future<void> _searchProducts(String text) async {
    // print(_searchText);
    final HttpsCallableResult httpResult = await CloudFunctions.instance
        .getHttpsCallable(functionName: 'searchProductByTitle')
        .call(<String, dynamic>{'title': text});
    final List<dynamic> result = httpResult.data;
    print('>>> ${httpResult.data}');
    if (result?.isEmpty ?? true) {
      productSearch.add(<ProductSearch>[]);
    } else {
      final List<ProductSearch> products =
          List<ProductSearch>.from(result.map<ProductSearch>((dynamic json) {
        ProductSearch user = ProductSearch.fromJson(json);
        return user;
      }));
      productSearch.add(products);
    }
  }

  Future<void> _searchUsers(String text) async {
    print('>>> Searching for user \'$text\'');
    final HttpsCallableResult httpResult = await CloudFunctions.instance
        .getHttpsCallable(functionName: 'searchUserByNickname')
        .call(<String, dynamic>{'nickname': text});
    final List<dynamic> result = httpResult.data;
    print('>>> ${httpResult.data}');
    if (result?.isEmpty ?? true) {
      userSearch.add(<UserSearch>[]);
    } else {
      final List<UserSearch> users =
          List<UserSearch>.from(result.map<UserSearch>((dynamic json) {
        UserSearch user = UserSearch.fromJson(json);
        return user;
      }));
      userSearch.add(users);
    }
  }

  Future<void> _searchGroups(String text) async {
    // print(_searchText);
    final HttpsCallableResult httpResult = await CloudFunctions.instance
        .getHttpsCallable(functionName: 'searchComooByName')
        .call(<String, dynamic>{'name': text});
    final List<dynamic> result = httpResult.data;
    if (result?.isEmpty ?? true) {
      groupSearch.add(<GroupSearch>[]);
    } else {
      final List<GroupSearch> groups = result.map((dynamic snap) {
        GroupSearch group = GroupSearch.fromJson(snap);
        group.membersQty = snap['members'];
        return group;
      }).toList();
      groupSearch.add(groups);
    }
  }
}

class TabModel {
  TabModel({@required this.tab, @required this.view});
  Widget tab;
  Widget view;
}
