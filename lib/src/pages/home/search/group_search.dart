import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/pages/common/group/group_detail.dart';
import 'package:comoo/src/utility/responsiveness.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';

class GroupSearchPage extends StatelessWidget {
  GroupSearchPage(this.stream);

  final Stream<List<GroupSearch>> stream;

  @override
  Widget build(BuildContext context) {
    final iPhone8Responsive responsive = iPhone8Responsive(context);
    return Scaffold(
      body: StreamBuilder<List<GroupSearch>>(
        stream: stream,
        builder: (context, AsyncSnapshot<List<GroupSearch>> snapshot) {
//          if (_isGroupSearchActive) {
//            return Container(
//              alignment: FractionalOffset.center,
//              child: CircularProgressIndicator(),
//            );
//          }
//          if (_searchText == '') {
//            return Container(
//              alignment: FractionalOffset.center,
//              padding: EdgeInsets.all(responsive.width(48.0)),
//              child: Text(
//                'Pode explorar! Faça uma busca por pessoas ou comoonidades.',
//                textAlign: TextAlign.center,
//                style: ComooStyles.botaoTurqueza,
//              ),
//            );
//          }
          if (snapshot.hasData) {
            if (snapshot.data.isEmpty) {
              return Container(
                alignment: FractionalOffset.center,
                padding: EdgeInsets.all(responsive.width(48.0)),
                child: Text(
                  'Não existem COMOOs a partir desta busca :(',
                  textAlign: TextAlign.center,
                  style: ComooStyles.botaoRoxo,
                ),
              );
            }
            return _groupList(context, snapshot.data);
          }
//          if (_groups.isNotEmpty) {
//            return _groupList(context, _groups);
//          }
          final TextStyle style = TextStyle(
            fontFamily: COMOO.fonts.montSerrat,
            color: COMOO.colors.turquoise,
            fontWeight: FontWeight.w500,
            fontSize: responsive.width(20),
          );
          return Container(
            alignment: FractionalOffset.center,
            padding: EdgeInsets.all(responsive.width(32.0)),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: 'Pode explorar!',
                style: style.copyWith(
                  fontWeight: FontWeight.w600,
                  fontSize: responsive.width(26),
                ),
                children: <InlineSpan>[
                  TextSpan(
                    text: '\n',
                    style: style,
                  ),
                  TextSpan(
                    text:
                        'Faça uma busca por pessoas, produtos ou comoonidades',
                    style: style,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _groupList(BuildContext context, List<GroupSearch> groups) {
    final iPhone8Responsive responsive = iPhone8Responsive(context);
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return Divider();
      },
      addRepaintBoundaries: true,
      padding: EdgeInsets.only(top: responsive.width(10.0)),
      itemCount: groups.length,
      itemBuilder: (BuildContext context, int index) {
        GroupSearch group = groups[index];
        return ListTile(
          contentPadding: EdgeInsets.symmetric(
            horizontal: responsive.width(10.0),
          ),
          leading: GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: ComooColors.cinzaClaro),
                  borderRadius: BorderRadius.circular(responsive.width(30.0)),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AdvancedNetworkImage(group.photoURL),
                  )),
              width: responsive.width(55.0),
              height: responsive.width(55.0),
            ),
            onTap: () {
              _openGroupInfo(context, group);
            },
          ),
          title: GestureDetector(
            child: Text(
              group.name,
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.chumbo,
                fontSize: responsive.width(12.0),
                fontWeight: FontWeight.w600,
              ),
            ),
            onTap: () {
              _openGroupInfo(context, group);
            },
          ),
          subtitle: GestureDetector(
            child: Text(
              '${group.membersQty} membros',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.cinzaMedio,
                fontSize: responsive.width(12.0),
                fontWeight: FontWeight.w500,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            onTap: () {
              _openGroupInfo(context, group);
            },
          ),
          trailing: Container(
            // padding: EdgeInsets.only(right: responsive.width(5.0)),
            child: IconButton(
              alignment: Alignment.center,
              iconSize: responsive.width(50.0),
              // icon: Icon(
              //   group.inGroup ? Icons.check_circle : Icons.add_circle_outline,
              //   color: ComooColors.turqueza,
              // ),
              icon: Image.asset(
                group.inGroup
                    ? 'images/checked_turquoise.png'
                    : 'images/add_white.png',
                width: responsive.width(30.0),
                height: responsive.width(30.0),
              ),
              onPressed: () {
//                group.inGroup
//                ? _requestGroupExit(group)
//                : _requestGroupEntry(group);
              },
            ),
          ),
          // onTap: () {
          // Navigator.of(context).push(new MaterialPageRoute(
          //     builder: (context) => GroupChatPage(_groups[index])));
          // },
        );
      },
    );
  }

  Future<void> _openGroupInfo(
      BuildContext context, GroupSearch groupSearch) async {
    print('open group info');
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) =>
            GroupDetailView(id: groupSearch.id)));
  }
}
