import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/pages/common/product/product_page.dart';
import 'package:comoo/src/utility/responsiveness.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';

class ProductSearchPage extends StatelessWidget {
  ProductSearchPage(this.stream);

  final Stream<List<ProductSearch>> stream;
  @override
  Widget build(BuildContext context) {
    final iPhone8Responsive responsive = iPhone8Responsive(context);
    return Scaffold(
      body: StreamBuilder<List<ProductSearch>>(
        stream: stream,
        builder: (context, AsyncSnapshot<List<ProductSearch>> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.isEmpty) {
              return Container(
                alignment: FractionalOffset.center,
                padding: EdgeInsets.all(responsive.width(48.0)),
                child: Text(
                  'Não existem produtos a partir desta busca :(',
                  textAlign: TextAlign.center,
                  style: ComooStyles.botaoRoxo,
                ),
              );
            }
            return _productList(context, snapshot.data);
          }
          final TextStyle style = TextStyle(
            fontFamily: COMOO.fonts.montSerrat,
            color: COMOO.colors.turquoise,
            fontWeight: FontWeight.w500,
            fontSize: responsive.width(20),
          );
          return Container(
            alignment: FractionalOffset.center,
            padding: EdgeInsets.all(responsive.width(32.0)),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: 'Pode explorar!',
                style: style.copyWith(
                  fontWeight: FontWeight.w600,
                  fontSize: responsive.width(26),
                ),
                children: <InlineSpan>[
                  TextSpan(
                    text: '\n',
                    style: style,
                  ),
                  TextSpan(
                    text:
                        'Faça uma busca por pessoas, produtos ou comoonidades',
                    style: style,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _productList(BuildContext context, List<ProductSearch> products) {
    final iPhone8Responsive responsive = iPhone8Responsive(context);

    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return Divider();
      },
      addRepaintBoundaries: true,
      padding: EdgeInsets.only(top: responsive.width(10.0)),
      itemCount: products.length,
      itemBuilder: (BuildContext context, int index) {
        ProductSearch product = products[index];
        return ListTile(
          contentPadding: EdgeInsets.symmetric(
            horizontal: responsive.width(10.0),
          ),
          leading: GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: ComooColors.cinzaClaro),
                  borderRadius: BorderRadius.circular(responsive.width(30.0)),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AdvancedNetworkImage(product.photoURL),
                  )),
              width: responsive.width(55.0),
              height: responsive.width(55.0),
            ),
            onTap: () {
              _openProductInfo(context, product);
            },
          ),
          title: GestureDetector(
            child: Text(
              product.title,
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.chumbo,
                fontWeight: FontWeight.w600,
                fontSize: 12.0,
              ),
            ),
            onTap: () {
              _openProductInfo(context, product);
            },
          ),
        );
      },
    );
  }

  Future<void> _openProductInfo(
      BuildContext context, ProductSearch search) async {
    final Firestore _db = Firestore.instance;
    final DocumentSnapshot snap =
        await _db.collection('products').document(search.id).get();
    final Product product = Product.fromSnapshot(snap);

    Navigator.of(context).push(new MaterialPageRoute(
      builder: (BuildContext context) => ProductPage(product),
    ));
  }
}
