import 'dart:async';
import 'package:comoo/src/pages/common/group/group_detail.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:flutter/services.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';

class SearchNavigationPage extends StatefulWidget {
  SearchNavigationPage({Key key}) : super(key: key);

  @override
  State createState() {
    return SearchNavigationPageState();
  }
}

class SearchNavigationPageState extends State<SearchNavigationPage> {
  String _searchText = '';
  bool _isUserSearchActive = false;
  bool _isGroupSearchActive = false;
  final FocusNode searchFocusNode = FocusNode();
  final Firestore _db = Firestore.instance;
  final CloudFunctions api = CloudFunctions.instance;

  List<TabModel> tabList;

  StreamController<List<UserSearch>> userSearchStreamCtrl = StreamController();
  StreamController<List<GroupSearch>> groupSearchStreamCtrl =
      StreamController();
  Stream<List<UserSearch>> userSearchStream;
  Stream<List<GroupSearch>> groupSearchStream;
  List<UserSearch> _users = List();
  List<GroupSearch> _groups = List();

  _fetchUserSearchData() async {
    // print(_searchText);
    List<UserSearch> users = List();
    final HttpsCallableResult httpResult = await CloudFunctions.instance
        .getHttpsCallable(functionName: 'searchUserByNickname')
        .call(
      <String, dynamic>{
        'nickname': _searchText,
      },
    );
    final List<dynamic> result = httpResult.data;
    users = result.map((snap) {
      // print(item['id']);
      UserSearch user = UserSearch.fromSnapshot(snap);
      // users.add(user);
      // userSearchStreamCtrl.sink.add(users);
      // setState(() {});
      return user;
    }).toList();
    if (mounted) {
      setState(() {
        _isUserSearchActive = false;
        _users = users;
        userSearchStreamCtrl.sink.add(users);
      });
    }
  }

  _fetchGroupSearchData() async {
    // print(_searchText);
    List<GroupSearch> groups = List();
    final HttpsCallableResult httpResult = await CloudFunctions.instance
        .getHttpsCallable(functionName: 'searchComooByName')
        .call(
      <String, dynamic>{
        'name': _searchText,
      },
    );
    final List<dynamic> result = httpResult.data;
    groups = result.map((snap) {
      GroupSearch group = GroupSearch.fromSnapshot(snap);
      group.membersQty = snap['members'];
      return group;
    }).toList();
    setState(() {
      _isGroupSearchActive = false;
      _groups = groups;
      groupSearchStreamCtrl.sink.add(groups);
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.transparent,
    //   statusBarIconBrightness: Theme.of(context).brightness,
    // ));
    final TextStyle styles =
        ComooStyles.notificationTitle; //.copyWith(fontSize: mediaQuery * 10.0);

    tabList = <TabModel>[
      TabModel(
        tab: Tab(
          child: Text(
            'PESSOAS',
            style: styles,
          ),
        ),
        view: _userSearchTab(),
      ),
      TabModel(
        tab: Tab(
          child: Text(
            'COMOONIDADES',
            style: styles,
          ),
        ),
        view: _groupSearchTab(),
      ),
      // TabModel(
      //   tab: Tab(
      //     child: Text(
      //       'PRODUTOS',
      //       style: styles,
      //     ),
      //   ),
      //   view: Container(
      //     child: Center(
      //       child: Text('Products'),
      //     ),
      //   ),
      // ),
    ];
    return Scaffold(
      appBar: AppBar(
        elevation: 0.5,
        brightness: Theme.of(context).brightness,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        titleSpacing: mediaQuery * 40.0,
        title: Form(
          child: Container(
            child: TextField(
              autocorrect: true,
              onChanged: (text) {
                setState(() {
                  _searchText = text;
                  if (_searchText == '') {
                    _isUserSearchActive = false;
                    _isGroupSearchActive = false;
                    _users.clear();
                  } else {
                    _isUserSearchActive = true;
                    _isGroupSearchActive = true;
                    _fetchUserSearchData();
                    _fetchGroupSearchData();
                  }
                });
              },
              autofocus: true,
              focusNode: searchFocusNode,
              decoration: InputDecoration(
                // icon: Icon(
                //   Icons.search,
                //   color: ComooColors.turqueza,
                // ),
                icon: Image.asset(
                  'images/search_black.png',
                  height: mediaQuery * 30.0,
                  width: mediaQuery * 30.0,
                ), //Icon(COMOO.icons.search.iconData),
                // iconSize: mediaQuery * 30.0,
                hintText: '',
                border: InputBorder.none,
              ),
            ),
          ),
        ),
        actions: <Widget>[
          Container(
            width: mediaQuery * 95.0,
            child: IconButton(
              iconSize: mediaQuery * 34.0,
              icon: Text(
                'CANCELAR',
                style: TextStyle(
                  fontFamily: ComooStyles.fontFamily,
                  color: ComooColors.chumbo,
                  fontSize: mediaQuery * 12.0,
                  fontWeight: FontWeight.bold,
                  letterSpacing: mediaQuery * 0.60,
                ),
              ),
              color: ComooColors.chumbo,
              onPressed: () {
                searchFocusNode.unfocus();
                Navigator.of(context).pop();
              },
            ),
          ),
        ],
      ),
      body: DefaultTabController(
        length: tabList.length,
        initialIndex: 0,
        child: Column(
          children: <Widget>[
            TabBar(
              labelColor: ComooColors.roxo,
              indicatorColor: ComooColors.roxo,
              indicatorWeight: mediaQuery * 5.0,
              tabs: tabList.map((TabModel tb) => tb.tab).toList(),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  // FocusScope.of(context).requestFocus(FocusNode());
                  SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
                },
                child: TabBarView(
                  children: tabList.map((TabModel tb) => tb.view).toList(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _openUserInfo(UserSearch userSearch) async {
    DocumentSnapshot snap =
        await _db.collection('users').document(userSearch.id).get();
    User user = User.fromSnapshot(snap);

    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => UserDetailView(user)));
  }

  _removeFriend(UserSearch user) async {
    await _db
        .collection('users')
        .document(currentUser.uid)
        .collection('friends')
        .document(user.id)
        .delete();

    // this.showInSnackBar(
    //     '${user.name} foi removido da sua rede de relacionamentos.');
    if (mounted) {
      setState(() {
        currentUser.friends.remove(user.id);
        user.isFriend = false;
      });
    }
  }

  _addFriend(UserSearch user) async {
    var friend = _db
        .collection('users')
        .document(currentUser.uid)
        .collection('friends')
        .document(user.id);

    await friend.setData({
      'id': _db.collection('users').document(user.id),
      'origin': 'added'
    }).then((_) {
      // this.showInSnackBar(
      //     '${user.name} adicionado à sua rede de relacionamentos.');
      setState(() {
        currentUser.friends.add(user.id);
        user.isFriend = true;
        // _isFriend = true;
      });
    }).catchError((error) {
      // this.showInSnackBar('$error');
    });
  }

  _userList(List<UserSearch> users) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return ListView.builder(
      addRepaintBoundaries: true,
      padding: EdgeInsets.only(top: mediaQuery * 10.0),
      itemCount: users.length,
      itemBuilder: (context, index) {
        UserSearch user = users[index];
        return ListTile(
          contentPadding: EdgeInsets.symmetric(
            horizontal: mediaQuery * 10.0,
          ),
          leading: GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: ComooColors.cinzaClaro),
                  borderRadius: BorderRadius.circular(mediaQuery * 30.0),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AdvancedNetworkImage(user.photoURL),
                  )),
              width: mediaQuery * 55.0,
              height: mediaQuery * 55.0,
            ),
            onTap: () {
              _openUserInfo(user);
            },
          ),
          title: GestureDetector(
            child: Text(
              user.name,
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.chumbo,
                fontWeight: FontWeight.w600,
                fontSize: 12.0,
              ),
            ),
            onTap: () {
              _openUserInfo(user);
            },
          ),
          subtitle: GestureDetector(
            child: Text(
              '@${user.nickname}',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.cinzaMedio,
                fontSize: mediaQuery * 12.0,
                fontWeight: FontWeight.w500,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            onTap: () {
              _openUserInfo(user);
            },
          ),
          trailing: Container(
            padding: EdgeInsets.only(right: mediaQuery * 25.0),
            child: IconButton(
              iconSize: mediaQuery * 50.0,
              // icon: Icon(
              //   user.isFriend ? Icons.check_circle : Icons.add_circle_outline,
              //   color: ComooColors.turqueza,
              // ),
              icon: Image.asset(
                user.isFriend
                    ? 'images/checked_turquoise.png'
                    : 'images/add_white.png',
                width: mediaQuery * 30.0,
                height: mediaQuery * 30.0,
              ),
              onPressed: () {
                user.isFriend ? _removeFriend(user) : _addFriend(user);
              },
            ),
          ),
          // onTap: () {
          //   user.isFriend ? print('remove friend') : print('add friend');
          //   setState(() {});
          // Navigator.of(context).push(new MaterialPageRoute(
          //     builder: (context) => GroupChatPage(_groups[index])));
          // },
        );
      },
    );
  }

  Widget _userSearchTab() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return StreamBuilder<List<UserSearch>>(
      stream: userSearchStream,
      builder:
          (BuildContext context, AsyncSnapshot<List<UserSearch>> snapshot) {
        if (_isUserSearchActive) {
          return Container(
            alignment: FractionalOffset.center,
            child: CircularProgressIndicator(),
          );
        }
        if (_searchText == '') {
          return Container(
            alignment: FractionalOffset.center,
            padding: EdgeInsets.all(mediaQuery * 48.0),
            child: Text(
              'Pode explorar! Faça uma busca por pessoas ou comoonidades.',
              textAlign: TextAlign.center,
              style: ComooStyles.botaoTurqueza,
            ),
          );
        }
        if (snapshot.hasData) {
          if (snapshot.data.isEmpty) {
            return Container(
              alignment: FractionalOffset.center,
              padding: EdgeInsets.all(mediaQuery * 48.0),
              child: Text(
                'Não existem usuários a partir desta busca :(',
                textAlign: TextAlign.center,
                style: ComooStyles.botaoRoxo,
              ),
            );
          }
          return _userList(snapshot.data);
        }
        if (_users.isNotEmpty) {
          return _userList(_users);
        }
        return Container(
          alignment: FractionalOffset.center,
          padding: EdgeInsets.all(mediaQuery * 48.0),
          child: Text(
            'Não existem usuários a partir desta busca :(',
            textAlign: TextAlign.center,
            style: ComooStyles.botaoRoxo,
          ),
        );
      },
    );
  }

  _openGroupInfo(GroupSearch groupSearch) async {
    print('open group info');
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) =>
            GroupDetailView(id: groupSearch.id)));
  }

  _requestGroupExit(GroupSearch groupSearch) async {
    // print('work in progress');

    await api
        .getHttpsCallable(functionName: 'leaveGroup')
        .call(<String, String>{
      'groupId': groupSearch.id,
    }).then((HttpsCallableResult httpResult) {
      if (httpResult?.data ?? false) {
        print('');
      } else {
        print('');
      }
    }).catchError((error) {
      print('$error');
    });
    setState(() {
      // _loading = false;
    });
  }

  _requestGroupEntry(GroupSearch groupSearch) async {
    String id = groupSearch.id;

    setState(() {
      currentUser.groups.add(id);
      groupSearch.inGroup = true;
    });
    try {
      await CloudFunctions.instance
          .getHttpsCallable(functionName: 'requestJoinGroup')
          .call(<String, String>{
        'groupId': id,
      }).then((HttpsCallableResult result) {
        print(result);
        return result.data;
      }).timeout(Duration(seconds: 60));
    } on TimeoutException catch (e) {
      print('>> requestJoinGroup TimeoutError!\n$e');
      setState(() {
        currentUser.groups.remove(id);
        groupSearch.inGroup = false;
      });
    } on CloudFunctionsException catch (e) {
      print('>> requestJoinGroup CloudFunctionsError!\n${e.message}');
      setState(() {
        currentUser.groups.remove(id);
        groupSearch.inGroup = false;
      });
    } catch (e) {
      print('>> requestJoinGroup UnkownError!\n$e');
      setState(() {
        currentUser.groups.remove(id);
        groupSearch.inGroup = false;
      });
    }
  }

  Widget _groupList(List<GroupSearch> groups) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return ListView.builder(
      addRepaintBoundaries: true,
      padding: EdgeInsets.only(top: mediaQuery * 10.0),
      itemCount: groups.length,
      itemBuilder: (context, index) {
        GroupSearch group = groups[index];
        return ListTile(
          contentPadding: EdgeInsets.symmetric(
            horizontal: mediaQuery * 10.0,
          ),
          leading: GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: ComooColors.cinzaClaro),
                  borderRadius: BorderRadius.circular(mediaQuery * 30.0),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AdvancedNetworkImage(group.photoURL),
                  )),
              width: mediaQuery * 55.0,
              height: mediaQuery * 55.0,
            ),
            onTap: () {
              _openGroupInfo(group);
            },
          ),
          title: GestureDetector(
            child: Text(
              group.name,
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.chumbo,
                fontSize: mediaQuery * 12.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            onTap: () {
              _openGroupInfo(group);
            },
          ),
          subtitle: GestureDetector(
            child: Text(
              '${group.membersQty} membros',
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.cinzaMedio,
                fontSize: mediaQuery * 12.0,
                fontWeight: FontWeight.w500,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            onTap: () {
              _openGroupInfo(group);
            },
          ),
          trailing: Container(
            // padding: EdgeInsets.only(right: mediaQuery * 25.0),
            child: IconButton(
              alignment: Alignment.center,
              iconSize: mediaQuery * 50.0,
              // icon: Icon(
              //   group.inGroup ? Icons.check_circle : Icons.add_circle_outline,
              //   color: ComooColors.turqueza,
              // ),
              icon: Image.asset(
                group.inGroup
                    ? 'images/checked_turquoise.png'
                    : 'images/add_white.png',
                width: mediaQuery * 30.0,
                height: mediaQuery * 30.0,
              ),
              onPressed: () {
                group.inGroup
                    ? _requestGroupExit(group)
                    : _requestGroupEntry(group);
                setState(() {});
              },
            ),
          ),
          // onTap: () {
          // Navigator.of(context).push(new MaterialPageRoute(
          //     builder: (context) => GroupChatPage(_groups[index])));
          // },
        );
      },
    );
  }

  Widget _groupSearchTab() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return StreamBuilder<List<GroupSearch>>(
      stream: groupSearchStream,
      builder:
          (BuildContext context, AsyncSnapshot<List<GroupSearch>> snapshot) {
        if (_isGroupSearchActive) {
          return Container(
            alignment: FractionalOffset.center,
            child: CircularProgressIndicator(),
          );
        }
        if (_searchText == '') {
          return Container(
            alignment: FractionalOffset.center,
            padding: EdgeInsets.all(mediaQuery * 48.0),
            child: Text(
              'Pode explorar! Faça uma busca por pessoas ou comoonidades.',
              textAlign: TextAlign.center,
              style: ComooStyles.botaoTurqueza,
            ),
          );
        }
        if (snapshot.hasData) {
          if (snapshot.data.isEmpty) {
            return Container(
              alignment: FractionalOffset.center,
              padding: EdgeInsets.all(mediaQuery * 48.0),
              child: Text(
                'Não existem COMOOs a partir desta busca :(',
                textAlign: TextAlign.center,
                style: ComooStyles.botaoRoxo,
              ),
            );
          }
          return _groupList(snapshot.data);
        }
        if (_groups.isNotEmpty) {
          return _groupList(_groups);
        }
        return Container(
          alignment: FractionalOffset.center,
          padding: EdgeInsets.all(mediaQuery * 48.0),
          child: Text(
            'Não existem COMOOs a partir desta busca :(',
            textAlign: TextAlign.center,
            style: ComooStyles.botaoRoxo,
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();

    userSearchStream = userSearchStreamCtrl.stream.asBroadcastStream();
    groupSearchStream = groupSearchStreamCtrl.stream.asBroadcastStream();
  }

  @override
  void dispose() {
    super.dispose();
    userSearchStreamCtrl.close();
    groupSearchStreamCtrl.close();
  }
}

class TabModel {
  TabModel({@required this.tab, @required this.view});
  Widget tab;
  Widget view;
}
