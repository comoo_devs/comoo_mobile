import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/pages/common/transaction/negotiation_page.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
// import 'dart:async';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
// import 'package:comoo/src/comoo/invoice.dart';
// import 'package:comoo/src/comoo/financials.dart';
import 'package:cloud_functions/cloud_functions.dart';
// import 'dart:convert';
// import 'package:comoo/src/widgets/request_withdraw.dart';

class Balance extends StatefulWidget {
  Balance({Key key}) : super(key: key);

  @override
  State createState() {
    return new BalanceState();
  }
}

class BalanceState extends State<Balance> with Reusables {
  List<Withdraw> withdrawList = new List();
  Withdraw selectedWithdraw = Withdraw();
  double inputWithdraw = 0.0;
  double _balance = 0.0;
  var withdrawController = new MoneyMaskedTextController(leftSymbol: 'R\$ ');
  final NumberFormat formater =
      NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');
  final NumberFormat formatter =
      new NumberFormat.currency(locale: 'pt-br', symbol: 'R\$');

  final api = CloudFunctions.instance;

  Widget _buildFinancial() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    TextStyle balanceStyle = TextStyle(fontSize: mediaQuery * 24.0);
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: mediaQuery * 10.0,
        horizontal: mediaQuery * 24.0,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          new FutureBuilder(
            future: _fetchBalance(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                int current = snapshot.data['current']['amount'];
                // int future = snapshot.data['future']['amount'];
                _balance = current / 100;
                var balance = formater.format(_balance);
                return Text(
                  'Seu saldo: $balance',
                  style: balanceStyle,
                );
              } else {
                var balance = formater.format(_balance);
                return Text(
                  'Seu saldo: $balance',
                  style: balanceStyle,
                );
              }
              // if (snapshot.connectionState == ConnectionState.waiting) {
              //   var balance = formater.format(_balance);
              //   return Text(
              //     'Seu saldo: $balance',
              //     style: balanceStyle,
              //   );
              // }
              // if (!snapshot.hasData && _balance <= 0.0) {
              //   var balance = formater.format(_balance);
              //   return Text(
              //     'Seu saldo: $balance',
              //     style: balanceStyle,
              //   );
              // } else {
              //   print(snapshot.data);
              //   if (snapshot.data.isEmpty && _balance <= 0.0) {
              //     var balance = formater.format(_balance);
              //     return Text(
              //       'Seu saldo: $balance',
              //       style: balanceStyle,
              //     );
              //   }
              // }
            },
          ),
          Container(
            alignment: Alignment.centerLeft,
            // padding: EdgeInsets.symmetric(horizontal: mediaQuery * 30.0),
            child: TextField(
              controller: withdrawController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: '',
                labelText: 'Quanto você deseja resgatar?',
              ),
            ),
          ),
          // Container(
          //   alignment: Alignment.centerLeft,
          //   margin: EdgeInsets.symmetric(
          //     vertical: mediaQuery * 20.0,
          //   ),
          //   child: Text(
          //     'Em quantos dias você deseja restagar?',
          //     textAlign: TextAlign.start,
          //   ),
          // ),
          // Expanded(
          //   child: ListView.builder(
          //     itemCount: withdrawList.length,
          //     itemBuilder: (context, index) {
          //       Withdraw item = withdrawList[index];
          //       return ListTile(
          //         dense: true,
          //         // contentPadding: EdgeInsets.only(
          //         //   left: mediaQuery * 44.4,
          //         //   top: mediaQuery * 0.0,
          //         //   bottom: mediaQuery * 0.0,
          //         // ),
          //         leading: selectedWithdraw.id == item.id
          //             ? new Icon(
          //                 Icons.check_circle,
          //                 color: ComooColors.turqueza,
          //                 size: mediaQuery * 23.2,
          //               )
          //             : new Icon(
          //                 Icons.check_circle_outline,
          //                 color: ComooColors.cinzaClaro,
          //                 size: mediaQuery * 23.2,
          //               ),
          //         onTap: () {
          //           setState(() {
          //             selectedWithdraw = item;
          //           });
          //         },
          //         title: new Text(
          //           '${item.days} dias',
          //           style: ComooStyles.payment_type_subtitle,
          //         ),
          //         subtitle: new Text(
          //           '${(inputWithdraw * item.factor).toStringAsFixed(2)}\%',
          //           style: ComooStyles.payment_type_subtitle,
          //         ),
          //         trailing: new Text(
          //           '${inputWithdraw - (inputWithdraw * item.factor)}',
          //           style: ComooStyles.payment_type_subtitle,
          //         ),
          //       );
          //     },
          //   ),
          // ),
          Container(
            child: OutlineButton(
              onPressed: _handleSubmit,
              child: Text(
                'TRANSFERIR',
                style: ComooStyles.botaoTurquezaChat,
              ),
              borderSide: BorderSide(color: ComooColors.turqueza),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _showWaitingDialog() {
    showCustomDialog(
      context: context,
      barrierDismissible: false,
      alert: customRoundAlert(
          context: context,
          title: 'Enviando solicitação...\nAguarde',
          actions: []),
    );
  }

  _handleSubmit() {
    _showWaitingDialog();
    _transferValues();
  }

  _transferValues() async {
    print('${withdrawController.numberValue}');
    // final mediaQuery = MediaQuery.of(context).size.width / 400;
    await api
        .getHttpsCallable(functionName: 'createTransfer')
        .call(<String, dynamic>{
      'amount': withdrawController.numberValue,
    }).then((HttpsCallableResult httpResult) {
      Navigator.of(context).pop();
      showCustomDialog(
        context: context,
        alert: customRoundAlert(
            context: context,
            title:
                '${withdrawController.numberValue} Transferido para a sua conta bancária com sucesso!'),
      );
    }).catchError((error) {
      Navigator.of(context).pop();
      print('createTransfer $error');
      _showErrorDialog();
    }).timeout(Duration(seconds: 40));
  }

  _showErrorDialog() {
    showCustomDialog(
      context: context,
      alert: customRoundAlert(
          context: context,
          title: 'Falha ao enviar requisição.\nTente novamente mais tarde.'),
    );
  }

  // String negotiationStatus(String status) {
  //   switch (status) {
  //     case 'cancelled':
  //       return 'Cancelado';
  //     case 'under negotiation':
  //       return 'Aguardando Pagamento';
  //     case 'pending':
  //       return 'Processando Pagamento';
  //     case 'sent':
  //       return 'Enviado';
  //     case 'paid':
  //       return 'Pago';
  //     case 'finished':
  //       return 'Concluído';
  //     default:
  //       return status;
  //   }
  // }

  Widget _buildInvoices() {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance
          .collection('negotiations')
          .where(
            'seller',
            isEqualTo: Firestore.instance
                .collection('users')
                .document(currentUser.uid),
          )
          .orderBy('lastUpdate', descending: true)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        // print('${snapshot.connectionState} - has data? ${snapshot.hasData}');
        switch (snapshot.connectionState) {
          case ConnectionState.active:
            if (snapshot.hasData) return _buildSnapshotData(snapshot);
            return _buildEmptyList();
            break;
          case ConnectionState.done:
            if (snapshot.hasData) return _buildSnapshotData(snapshot);
            return _buildEmptyList();
            break;
          case ConnectionState.none:
            return _buildEmptyList();
            break;
          case ConnectionState.waiting:
            if (snapshot.hasData) return _buildSnapshotData(snapshot);
            return LoadingContainer();
            break;
          default:
            return Container();
            break;
        }
        // if (!snapshot.hasData) return LoadingContainer();
      },
    );
  }

  _buildSnapshotData(AsyncSnapshot<QuerySnapshot> snapshot) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    if (snapshot.data.documents.isEmpty) return _buildEmptyList();
    List documents = List();
    snapshot.data.documents.forEach((DocumentSnapshot doc) {
      if (doc['status'] == 'finished') {
        documents.add(doc);
      }
    });
    print(documents.length);
    if (documents.isEmpty) return _buildEmptyList();
    return ListView.separated(
      padding: EdgeInsets.only(top: mediaQuery * 10.0),
      separatorBuilder: (context, index) {
        return new Divider(
          color: ComooColors.cinzaMedio,
        );
      },
      itemCount: documents.length,
      itemBuilder: (context, index) {
        var document = documents[index];
        var negotiation = new Negotiation.fromSnapshot(document);
        return new ListTile(
          contentPadding: EdgeInsets.symmetric(
              vertical: 0.0, horizontal: mediaQuery * 10.0),
          dense: true,
          leading: new Container(
            decoration: BoxDecoration(
                border: Border.all(color: ComooColors.cinzaClaro),
                borderRadius: BorderRadius.circular(mediaQuery * 10.0),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: new AdvancedNetworkImage(negotiation.thumb),
                )),
            width: mediaQuery * 55.0,
            height: mediaQuery * 55.0,
          ),
          title: new Text(
            negotiation.title,
            style: ComooStyles.productTitle,
          ),
          isThreeLine: true,
          trailing: new Container(
            width: mediaQuery * 150.0,
            child: Container(
              padding: new EdgeInsets.only(
                  bottom: mediaQuery * 50.0, left: mediaQuery * 70.0),
              child: new Text(
                DateFormat('dd/MM/yyyy').format(negotiation.date),
                textAlign: TextAlign.right,
                style: ComooStyles.productTitlePrice,
              ),
            ),
          ),
          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(formatter.format(negotiation.price),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: ComooStyles.productTitlePrice),
              // new Text(
              //   negotiationStatus(negotiation.status),
              //   style: ComooStyles.productTitlePrice,
              // )
            ],
          ),
          onTap: () {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (context) => new NegotiationPage(negotiation)));
          },
        );
      },
    );
  }

  _buildEmptyList() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return new Padding(
      padding: EdgeInsets.all(mediaQuery * 16.0),
      child: new Center(
          child: new RichText(
              text: new TextSpan(
        text: 'Você ainda não efetuou nenhuma transação.\n',
        style: ComooStyles.welcomeMessageBold,
      ))),
    );
  }

  @override
  void initState() {
    super.initState();

    withdrawList.add(new Withdraw(id: '30', days: 30, factor: 0.0));
    withdrawList.add(new Withdraw(id: '14', days: 14, factor: 0.0143));
    withdrawList.add(new Withdraw(id: '2', days: 2, factor: 0.0251));
  }

  _fetchBalance() async {
    return await api
        .getHttpsCallable(functionName: 'getBalance')
        .call()
        .timeout(Duration(seconds: 60))
        .then((HttpsCallableResult result) {
      // print(result);
      return result.data;
    }).catchError((error) {
      // print(error);
      var errorMessage = {
        'current': {'amount': 0, 'currency': 'BRL'},
        'future': {'amount': 0, 'currency': 'BRL'},
      };
      return errorMessage;
    });
  }

  @override
  Widget build(BuildContext context) {
    // return new Container(child: _buildEntryList());
    return DefaultTabController(
      length: 2,
      child: Column(children: <Widget>[
        TabBar(
          labelColor: ComooColors.roxo,
          indicatorColor: ComooColors.roxo,
          tabs: [
            Tab(
              child: Text('Vendas', style: ComooStyles.notificationTitle),
            ),
            Tab(
              child: Text('Saldo Disponível',
                  style: ComooStyles.notificationTitle),
            ),
          ],
        ),
        Expanded(
            child: TabBarView(
          children: [
            _buildInvoices(),
            _buildFinancial(),
          ],
        )),
      ]),
    );
  }
}

class Withdraw {
  String id;
  double factor;
  int days;
  bool isSelected = false;

  Withdraw({
    this.id,
    this.factor,
    this.days,
  });
}
