import 'dart:ui';

import 'package:comoo/src/comoo/categories_model.dart';
import 'product_creation_categories.dart';
import 'product_creation_configs.dart';
import 'package:comoo/src/utility/payment_type.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:uuid/uuid.dart';
import 'dart:async';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
// import 'package:multi_image_picker/multi_image_picker.dart';
// import 'package:flutter_luban/flutter_luban.dart';

class ProductCreateView extends StatefulWidget {
  ProductCreateView({this.product, this.openCamera});

  final Product product;
  final bool openCamera;

  @override
  State createState() => _ProductCreateState();
}

class _ProductCreateState extends State<ProductCreateView>
    with PaymentTypeEnum, Reusables {
  final Uuid uuid = Uuid();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final FirebaseStorage _storage = FirebaseStorage.instance;
  double _minimumProductValue = 15.0;

  final _maxImages = 6;
  final _imageHeight = 1080;
  final _imageWidth = 1080;
  List<File> files;
  List _groups;
  List<Widget> photos;
  String _title;
  String _description;
  String _id;
  String _status;

//  String _price;
  String _brand;
  String _size;
  String _location;
  String _condition;
  // String _paymentMethod;
  List<PaymentType> paymentMethods = PaymentType.values;

//  String _delivery;
  bool _loading = false;
  String _loadingMessage = '';
  // DateTime _date;
  var _priceController = MoneyMaskedTextController(leftSymbol: 'R\$ ');
  var _finalPriceController = TextEditingController();

  Future<bool> _onWillPop() async {
//    if (_status == 'public') {
//      return true;
//    }
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final double buttonWidth = (MediaQuery.of(context).size.width * 0.90 / 3);

    bool save = await showCustomDialog(
      context: context,
      barrierDismissible: true,
      alert: customRoundAlert(
        context: context,
        color: ComooColors.pink,
        title:
            'Você tem certeza que deseja descartar as alterações feitas no an\úncio?',
        actions: [
          Container(
            height: mediaQuery * 41.0,
            width: buttonWidth,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              // color: Colors.white,
              child: FittedBox(
                child: Text(
                  'DESCARTAR',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              onPressed: () {
                Navigator.of(context)
                    .pop(false); // Returning true to _onWillPop will pop again.
              },
            ),
          ),
          Container(
            height: mediaQuery * 41.0,
            width: buttonWidth,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: FittedBox(
                child: Text(
                  'SALVAR',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              onPressed: () async {
                Navigator.of(context).pop(true);
              },
            ),
          ),
        ],
      ),
    );
    // bool save = await showDialog<bool>(
    //       context: context,
    //       barrierDismissible: false,
    //       builder: (BuildContext context) {
    //         return AlertDialog(
    //           contentPadding: EdgeInsets.all(mediaQuery * 0.0),
    //           content: Column(
    //             mainAxisSize: MainAxisSize.min,
    //             children: <Widget>[
    //               Container(
    //                 height: mediaQuery * 186.0,
    //                 width: mediaQuery * 329.0,
    //                 decoration: BoxDecoration(
    //                   color: ComooColors.pink,
    //                   borderRadius: BorderRadius.all(
    //                     Radius.circular(mediaQuery * 20.0),
    //                   ),
    //                 ),
    //                 child: Center(
    //                   child: Column(
    //                     mainAxisSize: MainAxisSize.min,
    //                     mainAxisAlignment: MainAxisAlignment.center,
    //                     children: <Widget>[
    //                       Text(
    //                         'Você tem certeza que deseja descartar as alterações feitas no an\úncio?',
    //                         textAlign: TextAlign.center,
    //                         style: TextStyle(
    //                             color: Colors.white,
    //                             fontSize: mediaQuery * 18.0),
    //                       ),
    //                       SizedBox(height: mediaQuery * 30.0),
    //                       ButtonBar(
    //                         mainAxisSize: MainAxisSize.min,
    //                         alignment: MainAxisAlignment.center,
    //                         children: <Widget>[
    //                           Container(
    //                             height: mediaQuery * 25.0,
    //                             decoration: BoxDecoration(
    //                               border: Border.all(
    //                                 width: 1.0,
    //                                 color: Colors.white,
    //                               ),
    //                               borderRadius: BorderRadius.circular(20.0),
    //                             ),
    //                             child: FlatButton(
    //                               shape: RoundedRectangleBorder(
    //                                 borderRadius:
    //                                     BorderRadius.circular(20.0),
    //                               ),
    //                               // color: Colors.white,
    //                               child: Text(
    //                                 'DESCARTAR',
    //                                 style: TextStyle(
    //                                   color: Colors.white,
    //                                 ),
    //                               ),
    //                               onPressed: () {
    //                                 Navigator.of(context).pop(
    //                                     false); // Returning true to _onWillPop will pop again.
    //                               },
    //                             ),
    //                           ),
    //                           Container(
    //                             height: mediaQuery * 25.0,
    //                             decoration: BoxDecoration(
    //                               border: Border.all(
    //                                 width: 1.0,
    //                                 color: Colors.white,
    //                               ),
    //                               borderRadius: BorderRadius.circular(20.0),
    //                             ),
    //                             child: FlatButton(
    //                               shape: RoundedRectangleBorder(
    //                                   borderRadius:
    //                                       BorderRadius.circular(20.0)),
    //                               child: _status == 'draft'
    //                                   ? Text(
    //                                       'SALVAR',
    //                                       style: TextStyle(
    //                                         color: Colors.white,
    //                                       ),
    //                                     )
    //                                   : Text(
    //                                       'SALVAR',
    //                                       style: TextStyle(
    //                                         color: Colors.white,
    //                                       ),
    //                                     ),
    //                               onPressed: () async {
    //                                 Navigator.of(context).pop(true);
    //                               },
    //                             ),
    //                           )
    //                         ],
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //               ),
    //             ],
    //           ),
    //         );
    //       },
    //     ) ??
    //     false;
    if (save == null) {
      return false;
    }

    if (save) {
      if (_status == 'draft') {
        bool success = await _saveDraft();
        if (!success) return false;
        return true;
      } else {
        bool success = await _handleSubmit();
        if (!success) return false;
//        await _saveProduct(_groups, false);
        return true;
      }
    } else
      return true;
  }

  Widget _buildPhotosList() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    var _photos = photos.toList();
    for (int i = photos.length; i < _maxImages; i++) {
      _photos.add(AddPhotoButton(onFileSelect));
    }

    var w = GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 3,
        childAspectRatio: mediaQuery * 1.2,
        mainAxisSpacing: mediaQuery * 10.0,
        crossAxisSpacing: mediaQuery * 10.0,
        children: _photos);

    return w;
  }

  Widget _buildForm() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    final InputDecoration decoration = InputDecoration(
      // isDense: true,
      helperStyle: ComooStyles.texto,
      focusedBorder: ComooStyles.underLineBorder,
      labelStyle: ComooStyles.inputLabel,
      border: ComooStyles.underLineBorder,
    );
    return Form(
      key: _formKey,
      onWillPop: _onWillPop,
      child: ListView(
        padding: EdgeInsets.symmetric(
          horizontal: responsive(context, 10.0),
        ),
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          TextFormField(
            decoration: decoration.copyWith(
              labelText: 'nome do produto*'.toUpperCase(),
              hintText: 'insira o nome do seu produto',
              labelStyle: ComooStyles.productInputLabel,
            ),
            style: ComooStyles.inputText,
            textCapitalization: TextCapitalization.sentences,
            initialValue: _title,
            onSaved: (String title) {
              _title = title;
            },
            validator: (value) {
              if (value.isEmpty) {
                return 'Preenchimento obrigatório';
              }
              return null;
            },
          ),
          TextFormField(
            style: ComooStyles.inputText,
            decoration: decoration.copyWith(
              labelText: 'Marca',
              icon: Icon(
                ComooIcons.marca,
                color: ComooColors.roxo,
              ),
              enabledBorder: InputBorder.none,
              counterText: '',
              counterStyle: TextStyle(),
            ),
            textCapitalization: TextCapitalization.words,
            initialValue: _brand,
            maxLength: 15,
            onSaved: (String value) {
              _brand = value;
            },
            validator: (value) {
              if (value.isEmpty) {
                return 'Preenchimento obrigatório';
              }
              return null;
            },
          ),
          TextFormField(
            style: ComooStyles.inputText,
            decoration: decoration.copyWith(
              labelText: 'Tamanho ou dimensāo',
              icon: Icon(
                ComooIcons.tamanho,
                color: ComooColors.roxo,
              ),
              enabledBorder: InputBorder.none,
              counterText: '',
              counterStyle: TextStyle(),
            ),
            textCapitalization: TextCapitalization.none,
            initialValue: _size,
            maxLength: 15,
            onSaved: (String value) {
              _size = value;
            },
          ),
          SizedBox.fromSize(
            size: Size(
                MediaQuery.of(context).size.width, responsive(context, 40)),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  ComooIcons.estado_produto,
                  color: ComooColors.roxo,
                ),
                SizedBox(width: responsive(context, 20)),
                DropdownButton(
                  icon: Container(),
                  hint: Row(
                    children: <Widget>[
                      Text(
                        'Estado do produto',
                        style: ComooStyles.inputLabel,
                      ),
                      Icon(Icons.arrow_drop_down),
                    ],
                  ),
                  underline: Container(),
                  style: ComooStyles.dropdownText,
                  items: <DropdownMenuItem>[
                    DropdownMenuItem(
                      child: Text('Novo (nunca usado)'),
                      value: 'novo',
                    ),
                    DropdownMenuItem(
                      child: Text('Semi-novo (pouco uso,\n perfeito estado)'),
                      value: 'semi-novo',
                    ),
                    DropdownMenuItem(
                      child: Text('Usado'),
                      value: 'usado',
                    )
                  ],
                  value: _condition,
                  onChanged: (value) {
                    setState(() {
                      _condition = value;
                    });
                  },
                ),
              ],
            ),
          ),
          TextFormField(
            style: ComooStyles.inputText,
            decoration: decoration.copyWith(
              labelText: 'Localizaçāo',
              hintText: 'Ex: bairros ou zonas onde o produto está localizado',
              icon: Icon(
                ComooIcons.localizacao,
                color: ComooColors.roxo,
              ),
              enabledBorder: InputBorder.none,
            ),
            textCapitalization: TextCapitalization.words,
            initialValue: _location,
            onSaved: (String value) {
              _location = value;
            },
          ),
          Container(
            margin: EdgeInsets.symmetric(
              vertical: responsive(context, 20),
            ),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: mediaQuery * 144.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: mediaQuery * 10.0),
                            child: Text(
                              'PREÇO*',
                              style: ComooStyles.productInputLabel,
                              textAlign: TextAlign.start,
                            ),
                          ),
                          TextFormField(
                            style: ComooStyles.inputText,
                            decoration: InputDecoration(
                              isDense: true,
                              labelText: '',
                              hintStyle: ComooStyles.inputLabelCinzaClaro,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 15.0),
                              labelStyle: ComooStyles.inputLabelRoxo,
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: ComooColors.chumbo),
                                  gapPadding: 0.0,
                                  borderRadius: BorderRadius.circular(25.0)),
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: ComooColors.chumbo),
                                  gapPadding: 0.0,
                                  borderRadius: BorderRadius.circular(25.0)),
                            ),
                            controller: _priceController,
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Preenchimento obrigatório';
                              }
                              return null;
                            },
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Text(
                        '* Valor recebido pela venda = 90% do\n'
                        'preço anunciado menos taxas de\n'
                        'gateway de pagamento (de acordo\n'
                        'com a escolha do comprador):\n'
                        'Cartāo de crédito = 2,99% + R\$ 0,69\n'
                        'Boleto bancário = R\$ 3,49',
                        style: TextStyle(
                          fontFamily: COMOO.fonts.montSerrat,
                          color: COMOO.colors.gray,
                          fontSize: responsive(context, 10.0),
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: mediaQuery * 12.0),
            child: Text(
              'INFORMAÇÕES ADICIONAIS (opcional)',
              style: ComooStyles.productInputLabel,
            ),
          ),
          TextFormField(
            decoration: ComooStyles.inputDecorationRoundedRoxo('',
                hint: 'Se quiser, acrescente mais características e \n'
                    'informações que ajudem a vender o seu \n'
                    'produto. Boa sorte!'),
            style: ComooStyles.inputText,
            initialValue: _description,
            textCapitalization: TextCapitalization.sentences,
            keyboardType: TextInputType.multiline,
            maxLines: 4,
            onSaved: (String description) {
              _description = description;
            },
            // validator: (value) {
            //   if (value.isEmpty) return 'Preenchimento obrigatório';
            // },
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: mediaQuery * 30.0),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
              onPressed: () {
                this._handleSubmit();
              },
              child: Text(
                _status == 'public' ? 'SALVAR ALTERAÇÕES' : 'PUBLICAR',
                style: ComooStyles.botao_branco,
              ),
              color: ComooColors.turqueza,
            ),
          ),
          _status != 'public'
              ? Padding(
                  padding: EdgeInsets.symmetric(horizontal: mediaQuery * 30.0),
                  child: FlatButton(
                    onPressed: () {
                      _saveDraft().then((_) {
                        Navigator.of(context).pop();
                      });
                    },
                    child: FittedBox(
                      child: Center(
                        child: Text(
                          'SALVAR COMO RASCUNHO',
                          style: ComooStyles.botaoTurqueza,
                        ),
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(mediaQuery * 20.0),
                        side: BorderSide(
                            color: ComooColors.turqueza,
                            width: mediaQuery * 1.0)),
                  ),
                )
              : Container(),
        ],
      ),
    );
    // List<Widget> fields = <Widget>[];
    // fields.add(
    //   TextFormField(
    //     decoration: ComooStyles.inputDecorationRoxo(
    //       'nome do produto*',
    //       hint: 'insira o nome do seu produto',
    //     ).copyWith(
    //       labelStyle: ComooStyles.productInputLabel,
    //     ),
    //     style: ComooStyles.inputText,
    //     textCapitalization: TextCapitalization.sentences,
    //     initialValue: _title,
    //     onSaved: (String title) {
    //       _title = title;
    //     },
    //     validator: (value) {
    //       if (value.isEmpty) return 'Preenchimento obrigatório';
    //     },
    //   ),
    // );
    // fields.add(
    //   Center(
    //     child: TextFormField(
    //       style: ComooStyles.inputText,
    //       decoration: ComooStyles.inputDecoration(
    //         'marca',
    //         icon: Icon(
    //           ComooIcons.marca,
    //           color: ComooColors.roxo,
    //         ),
    //       ).copyWith(enabledBorder: InputBorder.none),
    //       textCapitalization: TextCapitalization.words,
    //       initialValue: _brand,
    //       maxLength: 15,
    //       onSaved: (String value) {
    //         _brand = value;
    //       },
    //       validator: (value) {
    //         if (value.isEmpty) return 'Preenchimento obrigatório';
    //       },
    //     ),
    //   ),
    // );
    // fields.add(
    //   TextFormField(
    //     style: ComooStyles.inputText,
    //     decoration: ComooStyles.inputDecoration(
    //       'tamanho',
    //       icon: Icon(
    //         ComooIcons.tamanho,
    //         color: ComooColors.roxo,
    //       ),
    //     ).copyWith(enabledBorder: InputBorder.none),
    //     textCapitalization: TextCapitalization.none,
    //     initialValue: _size,
    //     maxLength: 15,
    //     onSaved: (String value) {
    //       _size = value;
    //     },
    //   ),
    // );

    // fields.add(Row(
    //     mainAxisAlignment: MainAxisAlignment.spaceAround,
    //     children: <Widget>[
    //       Icon(
    //         ComooIcons.estado_produto,
    //         color: ComooColors.roxo,
    //       ),
    //       DropdownButton(
    //           hint: Text(
    //             'Estado do produto',
    //             style: ComooStyles.inputLabel,
    //           ),
    //           style: ComooStyles.dropdownText,
    //           items: <DropdownMenuItem>[
    //             DropdownMenuItem(
    //               child: Text('Novo (nunca usado)'),
    //               value: 'novo',
    //             ),
    //             DropdownMenuItem(
    //               child: Text('Semi-novo (pouco uso, perfeito estado)'),
    //               value: 'semi-novo',
    //             ),
    //             DropdownMenuItem(
    //               child: Text('Usado'),
    //               value: 'usado',
    //             )
    //           ],
    //           value: _condition,
    //           onChanged: (value) {
    //             setState(() {
    //               _condition = value;
    //             });
    //           }),
    //     ]));

    // fields.add(
    //   Container(
    //     padding: EdgeInsets.symmetric(horizontal: mediaQuery * 10.0),
    //     child: TextFormField(
    //       style: ComooStyles.inputText,
    //       decoration: ComooStyles.inputDecoration(
    //         'Região',
    //         hint: 'Ex: bairros ou zonas onde o produto está localizado',
    //         icon: Icon(
    //           ComooIcons.localizacao,
    //           color: ComooColors.roxo,
    //         ),
    //       ).copyWith(
    //         labelText: 'Localizaçāo',
    //         enabledBorder: InputBorder.none,
    //       ),
    //       textCapitalization: TextCapitalization.words,
    //       initialValue: _location,
    //       onSaved: (String value) {
    //         _location = value;
    //       },
    //     ),
    //   ),
    // );

    // fields.add(Container(
    //   margin: EdgeInsets.symmetric(
    //     vertical: responsive(context, 20),
    //   ),
    //   child: Column(children: <Widget>[
    //     Row(
    //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //       crossAxisAlignment: CrossAxisAlignment.start,
    //       children: <Widget>[
    //         Container(
    //           width: mediaQuery * 144.0,
    //           child: Column(
    //             mainAxisAlignment: MainAxisAlignment.start,
    //             crossAxisAlignment: CrossAxisAlignment.start,
    //             children: <Widget>[
    //               Padding(
    //                 padding: EdgeInsets.only(bottom: mediaQuery * 10.0),
    //                 child: Text(
    //                   'PREÇO*',
    //                   style: ComooStyles.productInputLabel,
    //                   textAlign: TextAlign.start,
    //                 ),
    //               ),
    //               TextFormField(
    //                 style: ComooStyles.inputText,
    //                 decoration: InputDecoration(
    //                   isDense: true,
    //                   labelText: '',
    //                   hintStyle: ComooStyles.inputLabelCinzaClaro,
    //                   contentPadding: EdgeInsets.symmetric(
    //                       vertical: 10.0, horizontal: 15.0),
    //                   labelStyle: ComooStyles.inputLabelRoxo,
    //                   focusedBorder: OutlineInputBorder(
    //                       borderSide: BorderSide(color: ComooColors.chumbo),
    //                       gapPadding: 0.0,
    //                       borderRadius: BorderRadius.circular(25.0)),
    //                   border: OutlineInputBorder(
    //                       borderSide: BorderSide(color: ComooColors.chumbo),
    //                       gapPadding: 0.0,
    //                       borderRadius: BorderRadius.circular(25.0)),
    //                 ),
    //                 controller: _priceController,
    //                 keyboardType: TextInputType.number,
    //                 validator: (value) {
    //                   if (value.isEmpty) return 'Preenchimento obrigatório';
    //                 },
    //               ),
    //             ],
    //           ),
    //         ),
    //         Container(
    //           child: Text(
    //             '* Valor recebido pela venda = 90% do\n'
    //             'preço anunciado menos taxas de\n'
    //             'gateway de pagamento (de acordo\n'
    //             'com a escolha do comprador):\n'
    //             'Cartāo de crédito = 2,99% + R\$ 0,69\n'
    //             'Boleto bancário = R\$ 3,49',
    //             style: TextStyle(
    //               fontFamily: COMOO.fonts.montSerrat,
    //               color: COMOO.colors.gray,
    //               fontSize: responsive(context, 10.0),
    //               fontWeight: FontWeight.w600,
    //             ),
    //           ),
    //         ),
    //       ],
    //     ),
    //   ]),
    // ));
    // fields.add(Padding(
    //   padding: EdgeInsets.only(bottom: mediaQuery * 12.0),
    //   child: Text(
    //     'INFORMAÇÕES ADICIONAIS (opcional)',
    //     style: ComooStyles.productInputLabel,
    //   ),
    // ));
    // fields.add(
    //   TextFormField(
    //     decoration: ComooStyles.inputDecorationRoundedRoxo('',
    //         hint: 'Se quiser, acrescente mais características e \n'
    //             'informações que ajudem a vender o seu \n'
    //             'produto. Boa sorte!'),
    //     style: ComooStyles.inputText,
    //     initialValue: _description,
    //     textCapitalization: TextCapitalization.sentences,
    //     keyboardType: TextInputType.multiline,
    //     maxLines: 4,
    //     onSaved: (String description) {
    //       _description = description;
    //     },
    //     validator: (value) {
    //       if (value.isEmpty) return 'Preenchimento obrigatório';
    //     },
    //   ),
    // );
    // fields.add(Padding(
    //   padding: EdgeInsets.symmetric(horizontal: mediaQuery * 30.0),
    //   child: FlatButton(
    //     shape: RoundedRectangleBorder(
    //         borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
    //     onPressed: () {
    //       this._handleSubmit();
    //     },
    //     child: Text(
    //       _status == 'public' ? 'SALVAR ALTERAÇÕES' : 'PUBLICAR',
    //       style: ComooStyles.botao_branco,
    //     ),
    //     color: ComooColors.turqueza,
    //   ),
    // ));
    // if (_status != 'public')
    //   fields.add(Padding(
    //     padding: EdgeInsets.symmetric(horizontal: mediaQuery * 30.0),
    //     child: FlatButton(
    //       onPressed: () {
    //         _saveDraft().then((_) {
    //           Navigator.of(context).pop();
    //         });
    //       },
    //       child: Text(
    //         'SALVAR COMO RASCUNHO',
    //         style: ComooStyles.botaoTurqueza,
    //       ),
    //       shape: RoundedRectangleBorder(
    //           borderRadius: BorderRadius.circular(mediaQuery * 20.0),
    //           side: BorderSide(
    //               color: ComooColors.turqueza, width: mediaQuery * 1.0)),
    //     ),
    //   ));

    // var w = Form(
    //   key: _formKey,
    //   onWillPop: _onWillPop,
    //   child: Column(
    //     mainAxisAlignment: MainAxisAlignment.start,
    //     crossAxisAlignment: CrossAxisAlignment.stretch,
    //     children: fields,
    //   ),
    // );

    // return w;
  }

  void _removeImage(key) {
    setState(() {
      this.photos.removeWhere((item) => item.key == key);
    });
  }

  void _editImage(key) async {
    int index = photos.indexWhere((photo) => photo.key == key);
    if (index != -1) {
      ImageFile photo = photos[index];
      File file = photo.file;
      if (file == null) {
        print(photo.url);
        var response = await http.get(photo.url);
        print(response);
        print(response.bodyBytes);
        Directory tempDir = await getTemporaryDirectory();
        String path = '${tempDir.path}/${photo.name}';
        // print(response == null);
        file = await File(path).writeAsBytes(response.bodyBytes);
        print(file.path);
      }

      File croppedFile = await ImageCropper.cropImage(
          sourcePath: file.path,
          ratioX: 1.0,
          // ratioY: 1.0,
          maxWidth: _imageWidth,
          maxHeight: _imageHeight,
          toolbarColor: Colors.purple,
          toolbarTitle: 'COMOO');
      var name = 'image${uuid.v1()}';
      var key = Key(name);
      if (croppedFile != null) {
        setState(() {
          photos[index] = ImageFile(
            croppedFile,
            name,
            (key) => _removeImage(key),
            (key) => _editImage(key),
            key: key,
          );
        });
      }
    }
  }

  Future writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  Future<void> _multiImage() async {
    try {
      List<Asset> resultList = await MultiImagePicker.pickImages(
        maxImages: _maxImages - photos.length,
      );
      if (!mounted) {
        return;
      }
      final int index = photos.length;
      for (int i = 0; i < resultList.length; ++i) {
        final Asset asset = resultList[i];
        var name = 'image${uuid.v1()}';
        var key = Key(name);
        ByteData original = await asset.requestOriginal();
        if (original != null) {
          print(asset.name);
          print('($original) == ByteData? ${original is ByteData}');
          Directory tempDir = await getTemporaryDirectory();
          String path = '${tempDir.path}/${asset.name}';
          print(path);
          File file = await writeToFile(original, path);
          if (mounted) {
            setState(() {
              photos.insert(
                  index + i,
                  ImageFile(
                    file,
                    name,
                    (key) => _removeImage(key),
                    (key) => _editImage(key),
                    key: key,
                  ));
            });
          }
        }
        // asset.release();
      }
    } catch (e) {
      print('>> Product Create ImageMultiPicker Error!\n$e');
    }
  }

  Future<void> _getImage(ImageSource source) async {
    if (!(await checkPermissions(source))) {
      return null;
    }
    if (source == ImageSource.gallery) {
      return _multiImage();
    }
    var image = await ImagePicker.pickImage(source: source);
    if (image == null) return null;
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        ratioX: 1.0,
        // ratioY: 1.0,
        maxWidth: _imageWidth,
        maxHeight: _imageHeight,
        toolbarColor: ComooColors.roxo,
        toolbarTitle: 'COMOO');
    var name = 'image${uuid.v1()}';
    var key = Key(name);
    image = null;
    if (croppedFile != null) {
      setState(() {
        photos.add(new ImageFile(
          croppedFile,
          name,
          (key) => _removeImage(key),
          (key) => _editImage(key),
          key: key,
        ));
      });
    }
//    setState(() {
//      _loading = true;
//      _loadingMessage = 'Carregando...';
//    });
//    var compImage = await compressImage(image);
//    var name = 'image${uuid.v1()}';
//    var key = Key(name);
//    image = null;
//    setState(() {
//      _loading = false;
//      photos.add(new ImageFile(
//        compImage,
//        name,
//        (key) => _removeImage(key),
//        key: key,
//      ));
//    });
  }

  void onFileSelect() async {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    var result = await showDialog<ImageSource>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text(
              'ESCOLHA ORIGEM',
              style: ComooStyles.appBarTitle,
            ),
            children: <Widget>[
              SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, ImageSource.camera);
                  },
                  child: Row(
                    children: <Widget>[
                      const Icon(
                        ComooIcons.camera,
                        color: ComooColors.roxo,
                      ),
                      Padding(
                          padding: EdgeInsets.only(left: mediaQuery * 20.0),
                          child: Text(
                            'Camera',
                            style: ComooStyles.botoes,
                          )),
                    ],
                  )),
              SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, ImageSource.gallery);
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(
                        ComooIcons.galeria,
                        color: ComooColors.roxo,
                        size: mediaQuery * 34.0,
                      ),
                      Padding(
                          padding: EdgeInsets.only(left: mediaQuery * 10.0),
                          child: const Text(
                            'Galeria de Fotos',
                            style: ComooStyles.botoes,
                          )),
                    ],
                  ))
            ],
          );
        });
    if (result == null) {
      return;
    }
    _getImage(result);
  }

  Future _handleSubmit() async {
    FormState form = _formKey.currentState;
    form.save();
    bool isValid = true;
    if (photos.isEmpty) {
      showInSnackBar('Insira ao menos uma foto do produto');
      isValid = false;
    } else if (_priceController.numberValue < _minimumProductValue) {
      showInSnackBar(
          'O valor do produto deve ser maior que R\$$_minimumProductValue');
      isValid = false;
    } else if (_condition == null || _condition.isEmpty) {
      showInSnackBar('Selecione o estado do produto');
      isValid = false;
      // } else if (_paymentMethod == null) {
      //   showInSnackBar('Selecione a forma de pagamento');
      //   isValid = false;
    } else if (!form.validate()) {
      showInSnackBar('Por favor, preencha todos os campos obrigatórios');
      isValid = false;
    }
    if (!isValid) {
      return false;
    }

    if (_status == 'draft') {
      ProductCreationConfigs configs = await Navigator.of(context)
          .push<ProductCreationConfigs>(MaterialPageRoute(
        builder: (context) => ProductCreationCategories(),
      ));
      final List<CategoriesModel> categories =
          configs?.categories ?? <CategoriesModel>[];
      final List<dynamic> groups = configs?.groups ?? <dynamic>[];
      for (CategoriesModel cat in categories) {
        print(cat.title);
      }
      for (dynamic g in groups) {
        print(g);
      }

      if (configs?.groups == null || (configs?.groups?.isEmpty ?? true)) {
        return false;
      }

      //Update the status to public
      this._status = 'public';
      return _saveProduct(
              groups: configs.groups,
              categories: configs.categories,
              isPublishing: configs.isPublishing)
          .then((_) {
        // setState(() {
        //   _loading = false;
        // });
        Navigator.pop(context, true);
      });
    } else if (_status == 'public') {
      return _saveProduct(
              groups: _groups, categories: null, isPublishing: false)
          .then((_) {
        // setState(() {
        //   _loading = false;
        // });
        Navigator.pop(context, true);
      });
    }
  }

  Future _saveDraft() async {
    FormState form = _formKey.currentState;
    form.save();
    if (_title.isEmpty) {
      showInSnackBar('Informe o título do produto antes de salvar.');
      return false;
    }
    if (photos.isEmpty) {
      showInSnackBar('Insira ao menos uma foto do produto');
      return false;
    }
    setState(() {
      _loading = true;
      _loadingMessage = 'Efetuando upload das imagens...';
    });

    return _saveProduct(groups: null, categories: null, isPublishing: false)
        .then((_) {
      setState(() {
        _loading = false;
      });
      Navigator.pop(context);
    });
  }

//  Future compressImage(File imageFile) async {
//    ReceivePort receivePort = ReceivePort();
//    await Isolate.spawn(ImageUtil.compressImage,
//        ImageCompressionMessage(imageFile, receivePort.sendPort));
//    return await receivePort.first;
//  }

  // Future _compress(File file) async {
  //   CompressObject compressObject = CompressObject(
  //     imageFile: file, //image
  //     path: file.parent.path, //compress to path
  //     quality: 80, //first compress quality, default 80
  //     // step: 9, //compress quality step, The bigger the fast, Smaller is more accurate, default 6
  //     mode: CompressMode.LARGE2SMALL, //default AUTO
  //   );
  //   return Luban.compressImage(compressObject).catchError((onError) {
  //     print('>> Compressing Error $onError');
  //     return '';
  //   });
  // }

  Future<String> _upload(File photo, String name) async {
    String compression = '';
    // compression = await _compress(photo);
    if (compression.isNotEmpty) photo = File(compression);
    print('Upload');
    print(photo.path);
    print(compression);
    StorageReference ref = _storage.ref().child('products').child('$name.jpg');
    final StorageTaskSnapshot uploadTask = await ref.putFile(photo).onComplete;
    final result = await uploadTask.ref.getDownloadURL().then((onValue) {
      print(onValue);
      return onValue.toString();
    }).catchError((onError) {
      print(onError);
      return '';
    });
    return result;
  }

  Future<List<String>> _uploadPhotos() async {
    List<Future<String>> uploadFutures = List<Future<String>>();
    photos.forEach((w) {
      ImageFile imageFile = w as ImageFile;
      if (imageFile.file == null)
        uploadFutures.add(Future.value(imageFile.url));
      else {
        var image = imageFile.file;
        uploadFutures.add(_upload(image, imageFile.name));
      }
    });

    return Future.wait(uploadFutures);
  }

  Future _saveProduct(
      {@required List groups,
      @required List<CategoriesModel> categories,
      @required bool isPublishing}) async {
    setState(() {
      _loading = true;
      _loadingMessage = 'Efetuando upload das imagens...';
    });
    List<String> photosUrl = await _uploadPhotos();
    // photosUrl.forEach((p) => print(p));
    List<String> photosName = <String>[];
    for (int i = 0; i < photos.length; ++i) {
      ImageFile image = (photos[i] as ImageFile);
      photosName.add('${image.name}.jpg');
      // print('${image.name}.jpg');
    }

    setState(() {
      _loadingMessage = 'Salvando dados...';
    });

    try {
      final _categories = categories ?? <CategoriesModel>[];
      await CloudFunctions.instance
          .getHttpsCallable(functionName: 'setProduct')
          .call(<String, dynamic>{
        'title': _title,
        'id': _id,
        'description': _description,
        'price': _priceController.numberValue,
        'brand': _brand,
        'size': _size,
        'status': _status,
        'condition': _condition,
        'location': _location,
        'photos': photosUrl,
        'imgs': photosName,
        'groups': groups,
        'categories': List<String>.from(
          _categories?.map<String>((CategoriesModel cat) => cat.id) ??
              <CategoriesModel>[],
        ),
        // 'payMethod': _paymentMethod,
//        'date': _date.toString(),
        'isPublishing': isPublishing,
      });
      showInSnackBar('Produto salvo com sucesso');
      return true;
    } on CloudFunctionsException catch (e) {
      print('Ocorreu um erro, tente novamente. COD:${e.code}');
      return false;
    } catch (e) {
      print('Ocorreu um erro, tente novamente. COD:$e');
      return false;
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text(value, style: ComooStyles.textoSnackBar)));
  }

  final NumberFormat formater =
      NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');

  _updateFinalPrice() {
    String finalPrice = '';
    double value = 0;
    if (_priceController.numberValue != 0)
      value = _priceController.numberValue -
          (_priceController.numberValue * 0.1299) -
          0.69;
    finalPrice = formater.format(value);
    _finalPriceController.text = finalPrice;
  }

  Future<void> _loadProduct() async {
    _title = widget.product.title;
    _description = widget.product.description;
    _priceController.updateValue(widget.product.price);
    _brand = widget.product.brand;
    _size = widget.product.size;
    _condition = widget.product.condition ?? 'new';
    _location = widget.product.location ?? '';
    _id = widget.product.id;
    _status = widget.product.status;
    // _paymentMethod = widget.product.paymentType;
    // _date = widget.product.date;
    _groups = widget.product.groups;
    if (widget.product.photos.isNotEmpty) {
      // int index = 0;
      widget.product.photos.forEach((url) {
        var name = 'image${uuid.v1()}';
        var key = Key(name);
        photos.add(ImageFile(
          null,
          name,
          (key) => _removeImage(key),
          (key) => _editImage(key),
          key: key,
          url: url,
        ));
        // index++;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    this._priceController.removeListener(_updateFinalPrice);
  }

  @override
  void initState() {
    super.initState();
    photos = List<Widget>();
    _priceController.addListener(_updateFinalPrice);
    if (widget.product != null) {
      _loadProduct();
    } else {
      if (widget.openCamera) {
        _getImage(ImageSource.camera);
      }
      _status = 'draft';
      _finalPriceController.text = 'R\$ 0,00';
    }
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    Widget c = ListView(
      padding: const EdgeInsets.all(10.0),
      children: <Widget>[
        Container(
          height: mediaQuery * 260.0,
          child: _buildPhotosList(),
        ),
        _buildForm()
      ],
    );
    c = Stack(
      children: <Widget>[
        IgnorePointer(
          ignoring: _loading,
          child: c,
        ),
        _loading
            ? LoadingContainer(
                message: _loadingMessage,
              )
            : Container(),
      ],
    );
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar.round(
          title: Text(
            'ANÚNCIO',
            style: ComooStyles.appBarTitleWhite,
          ),
          color: ComooColors.turqueza,
          actions: <Widget>[
            FlatButton(
                onPressed: _loading
                    ? null
                    : () {
                        _handleSubmit();
                      },
                child: Text(
                  _status == 'draft' ? 'PUBLICAR' : 'SALVAR',
                  style: ComooStyles.appBar_botao_branco,
                ))
          ],
        ),
        body: c);
  }
}

class AddPhotoButton extends StatelessWidget {
  final VoidCallback onFileSelect;

  AddPhotoButton(this.onFileSelect);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    Widget w;
    w = InkWell(
        onTap: onFileSelect,
        child: Container(
          width: mediaQuery * 105.0,
          height: mediaQuery * 105.0,
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(width: 1.0, color: ComooColors.roxo),
          ),
          child: Icon(
            ComooIcons.camera,
            color: ComooColors.roxo,
            size: mediaQuery * 31.0,
          ),
        ));
    return w;
  }
}

typedef void DeletedCallback(Key key);
typedef void EditCallback(Key key);

class ImageFile extends StatelessWidget {
  final file;
  final url;
  final DeletedCallback onDelete;
  final EditCallback onEdit;
  final name;

  ImageFile(this.file, this.name, this.onDelete, this.onEdit,
      {Key key, this.url})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    double iconSize = 30.0;
    return GestureDetector(
      onTap: () {
        onEdit(key);
      },
      child: Container(
        key: key,
        width: mediaQuery * 105.0,
        height: mediaQuery * 105.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(mediaQuery * 10.0),
          border: Border.all(
            color: ComooColors.roxo,
          ),
          image: DecorationImage(
              image: this.file != null
                  ? FileImage(file)
                  : AdvancedNetworkImage(this.url),
              fit: BoxFit.cover),
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              right: mediaQuery * 5.0,
              top: mediaQuery * 5.0,
              child: Container(
                height: mediaQuery * iconSize,
                width: mediaQuery * iconSize,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(mediaQuery * 28.0)),
                child: IconButton(
                  padding: EdgeInsets.all(0.0),
                  icon: Icon(
                    ComooIcons.photoDelete,
                  ),
                  iconSize: mediaQuery * 28.0,
                  onPressed: () {
                    onDelete(key);
                  },
                ),
              ),
            ),
            // Positioned(
            //   right: mediaQuery * 5.0,
            //   bottom: mediaQuery * 5.0,
            //   child: Container(
            //     height: mediaQuery * iconSize,
            //     width: mediaQuery * iconSize,
            //     decoration: BoxDecoration(
            //         color: Colors.white,
            //         borderRadius: BorderRadius.circular(mediaQuery * 28.0)),
            //     child: IconButton(
            //       padding: EdgeInsets.all(0.0),
            //       icon: Icon(
            //         Icons.edit,
            //       ),
            //       iconSize: mediaQuery * 28.0,
            //       onPressed: () {
            //         print('edit');
            //         onEdit(key);
            //       },
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
