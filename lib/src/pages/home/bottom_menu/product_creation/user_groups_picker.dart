import 'dart:async';
// import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
// import 'package:comoo/comoo/product.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';

class UserGroupPickerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _UserGroupPickerPageState();
  }
}

class _UserGroupPickerPageState extends State<UserGroupPickerPage> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final Firestore db = Firestore.instance;
  final FocusNode searchFocusNode = FocusNode();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  List<GroupItem> _groups = List<GroupItem>();
  bool isLoading = false;

  Future _fetchCurrentUserGroups() async {
    db
        .collection('users')
        .document(currentUser.uid)
        .collection('groups')
        .getDocuments()
        .then((QuerySnapshot query) {
      if (query.documents.isEmpty) {
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
        return;
      }
      // var groups = query.documents.map((DocumentSnapshot snapshot) async {
      //   DocumentSnapshot group =
      //       await db.collection('groups').document(snapshot.documentID).get().;
      //   return group;
      // });
      int lenght = query.documents.length;
      query.documents.forEach((DocumentSnapshot snapshot) async {
        lenght--;
        // var groupInfo = snapshot.data;
        DocumentSnapshot groupSnap =
            await db.collection('groups').document(snapshot.documentID).get();
        var groupInfo = groupSnap.data;
        Group group = Group(
            id: snapshot.documentID,
            name: groupInfo['name'],
            description: groupInfo['description'] ?? "",
            avatar: groupInfo['avatar']);
        if (mounted) {
          setState(() {
            _groups.add(GroupItem(group, selected: false));
            if (lenght <= 0) isLoading = false;
          });
          _groups.sort((GroupItem g1, GroupItem g2) =>
              g1.group.name.compareTo(g2.group.name));
        }
      });
    });
  }

  String _search = "";

  Widget _buildGroupList() {
    List<GroupItem> groupList = List<GroupItem>();
    if (_search.isNotEmpty == true)
      groupList = _groups
          .where((g) => g.group.name
              .startsWith(new RegExp('$_search', caseSensitive: false)))
          .toList();
    else
      groupList = _groups;

    if (groupList.isEmpty)
      return Container(
        alignment: FractionalOffset.center,
        padding: const EdgeInsets.all(30.0),
        child: Text(
          "Nenhum grupo encontrado, entre ou crie uma nova comoonidade do seu interesse para publicar seu produto",
          style: ComooStyles.welcomeMessage,
        ),
      );

    return ListView.builder(
        itemCount: groupList.length,
        itemBuilder: (context, index) {
          GroupItem group = groupList[index];
          return CheckboxListTile(
            value: group.selected ?? false,
            controlAffinity: ListTileControlAffinity.trailing,
            secondary: ClipOval(
              child: Image.network(
                group.group.avatar,
                fit: BoxFit.cover,
                width: 40.0,
                height: 40.0,
              ),
            ),
            title: Text(group.group.name, style: ComooStyles.texto_bold),
            subtitle: Text(
              group.group.description,
              maxLines: 3,
              style: ComooStyles.texto,
            ),
            onChanged: (value) {
              setState(() {
                searchFocusNode.unfocus();
                group.selected = value;
              });
            },
          );
        });
  }

  _createProduct(List<GroupItem> selectedGroups) async {
    var groups = selectedGroups.map((g) {
      return <String, dynamic>{
        'name': g.group.name,
        'id': g.group.id,
        'thumb': g.group.avatar,
      };
    }).toList();
    Navigator.pop(context, groups);
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text(value, style: ComooStyles.textoSnackBar)));
  }

  @override
  void initState() {
    isLoading = true;
    super.initState();
    _fetchCurrentUserGroups();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.roxo,
        title: const Text(
          'GRUPOS',
          style: ComooStyles.appBarTitleWhite,
        ),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                var selectedGroups =
                    _groups.where((g) => g.selected == true).toList();
                if (selectedGroups.length == 0) {
                  showInSnackBar('Selecione ao menos um grupo.');
                } else {
                  _createProduct(selectedGroups);
                }
              },
              child: Text(
                'PUBLICAR',
                style: ComooStyles.appBar_botao_branco,
              ))
        ],
      ),
      body: Column(
        children: <Widget>[
          Form(
            child: Container(
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    _search = text;
                  });
                },
                autofocus: false,
                focusNode: searchFocusNode,
                decoration: InputDecoration(
                    icon: Icon(Icons.search),
                    hintText: 'Busque pelo nome da comoonidade'),
              ),
            ),
          ),
          isLoading
              ? Expanded(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              : Expanded(child: _buildGroupList())
        ],
      ),
    );
  }
}

class GroupItem {
  GroupItem(this.group, {this.selected});

  Group group;
  bool selected = false;
}
