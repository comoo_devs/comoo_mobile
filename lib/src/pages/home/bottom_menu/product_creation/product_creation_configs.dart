import 'package:comoo/src/comoo/categories_model.dart';

class ProductCreationConfigs {
  ProductCreationConfigs({this.groups, this.categories=const <CategoriesModel>[], this.isPublishing=true});
  List groups = [];
  List<CategoriesModel> categories;
  bool isPublishing;
}
