import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/comoo/categories_model.dart';
import 'package:comoo/src/comoo/group.dart';
import '../group_creation/add_group.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/dashed_circle.dart';
import 'package:comoo/src/widgets/painters/message_painter.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';

class ProductCreationGroupPicker extends StatefulWidget {
  ProductCreationGroupPicker(
      {@required this.groups, @required this.categories});
  final List<GroupUser> groups;
  final List<CategoriesModel> categories;
  @override
  State<StatefulWidget> createState() => _ProductCreationGroupPickerState();
}

class _ProductCreationGroupPickerState extends State<ProductCreationGroupPicker>
    with Reusables {
  final List<GroupSelection> groups = <GroupSelection>[];
  @override
  void initState() {
    super.initState();

    groups.addAll(widget.groups
        .map((GroupUser gu) => GroupSelection.fromGroupUser(gu))
        .toList());
    groups.sort((GroupSelection a, GroupSelection b) =>
        a.group.name.compareTo(b.group.name));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.round(
        title: Text('anúncio'),
        color: COMOO.colors.turquoise,
        actions: widget.groups.isEmpty
            ? null
            : <Widget>[
                FlatButton(
                  child: Text(
                    'PUBLICAR',
                    style: TextStyle(
                      fontFamily: COMOO.fonts.montSerrat,
                      fontSize: responsive(context, 13.0),
                      fontWeight: FontWeight.bold,
                      letterSpacing: responsive(context, -0.108),
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () => _handleSubmit(context),
                ),
              ],
      ),
      backgroundColor:
          widget.groups.isEmpty ? COMOO.colors.purple : Colors.white,
      body: widget.groups.isEmpty
          ? _buildEmptyGroups(context)
          : _buildGroups(context),
    );
  }

  Widget _buildEmptyGroups(BuildContext context) {
    return CustomPaint(
      painter: MessagePainter(),
      child: Center(
        child: ListView(
          children: <Widget>[
            SizedBox(height: responsive(context, 82)),
            SizedBox(height: responsive(context, 33)),
            Padding(
              padding: EdgeInsets.only(left: responsive(context, 42)),
              child: Text(
                'Ops, você ainda nāo \n'
                'faz parte de nenhuma \n'
                'comoonidade nessa categoria.',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Color(0xFFEC1260),
                  fontSize: responsive(context, 29),
                  fontWeight: FontWeight.bold,
                  letterSpacing: responsive(context, -0.15),
                ),
              ),
            ),
            SizedBox(height: responsive(context, 32)),
            Container(
              margin: EdgeInsets.symmetric(horizontal: responsive(context, 32)),
              child: RaisedButton(
                elevation: 0.0,
                color: ComooColors.turqueza,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(14.0)),
                onPressed: () async {
                  final dynamic result = await Navigator.of(context)
                      .push(MaterialPageRoute<dynamic>(
                    builder: (BuildContext context) => AddGroupPage(
                        openFeed: false, categories: widget.categories),
                  ));
                  if (result != null) {
                    Navigator.of(context).pop(<dynamic>[result]);
                  }
                },
                child: FittedBox(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        COMOO.icons.createGroup.iconData,
                        color: Colors.white,
                        size: responsive(context, 40.0),
                      ),
                      SizedBox(width: responsive(context, 20.0)),
                      Text(
                        'Criar nova comoonidade',
                        style: TextStyle(
                          fontFamily: COMOO.fonts.montSerrat,
                          color: Colors.white,
                          fontSize: responsive(context, 15.0),
                          letterSpacing: responsive(context, -0.125),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: responsive(context, 19.0)),
            Container(
              margin: EdgeInsets.symmetric(horizontal: responsive(context, 32)),
              child: RaisedButton(
                elevation: 0.0,
                color: ComooColors.turqueza,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(14.0)),
                onPressed: () async {
                  //
                },
                child: Text(
                  'Salvar anúncio como rascunho',
                  style: TextStyle(
                    fontFamily: COMOO.fonts.montSerrat,
                    color: Colors.white,
                    fontSize: responsive(context, 16.0),
                    letterSpacing: responsive(context, 0.6),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildGroups(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: (widget.groups.length + 1),
      separatorBuilder: (BuildContext context, int index) {
        // if (index == 0) {
        //   return Container();
        // }
        return Divider(
          color: COMOO.colors.lead,
        );
      },
      itemBuilder: (BuildContext context, int index) {
        // if (index != widget.groups.length) {
        //   final GroupSelection group = groups[index];
        //   print('>>> group $group');
        // } else {
        //   print('OAKSODA');
        // }
        // return Container();
        if (index == widget.groups.length) {
          return Container(
            padding: EdgeInsets.symmetric(
              horizontal: responsive(context, 62.0),
              vertical: responsive(context, 29.0),
            ),
            child: Container(
              width: responsive(context, 252.0),
              height: responsive(context, 35.0),
              margin: EdgeInsets.symmetric(vertical: responsive(context, 10.0)),
              child: FlatButton(
                onPressed: () => _handleSubmit(context),
                color: COMOO.colors.turquoise,
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.circular(responsive(context, 20.0))),
                child: Text(
                  'PUBLICAR',
                  style: ComooStyles.botao_branco,
                ),
              ),
            ),
          );
        }
        final GroupSelection group = groups[index];
        print('>> $index ${group?.group?.name}');
        final Widget thumb = Padding(
          padding: EdgeInsets.all(responsive(context, 4.0)),
          child: group.group.avatar.isEmpty
              ? Container(
                  width: 40,
                  height: 40,
                )
              : CircleAvatar(
                  backgroundImage: CachedNetworkImageProvider(
                    group.group.avatar,
                  ),
                  backgroundColor: Colors.white,
                ),
        );
        return ListTile(
          leading: group.group.admin
              ? DashedCircle(
                  color: COMOO.colors.purple,
                  dashes: 25,
                  child: thumb,
                )
              : thumb,
          title: Text(
            '${group.group.name}',
            style: TextStyle(
              fontFamily: COMOO.fonts.montSerrat,
              color: COMOO.colors.turquoise,
              fontWeight: FontWeight.w600,
              fontSize: responsive(context, 14.0),
              letterSpacing: responsive(context, 0.257),
            ),
          ),
          subtitle: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // Text(
              //   '${doc['location']} - ${doc['city']}',
              //   style: TextStyle(
              //     fontFamily: COMOO.fonts.montSerrat,
              //     color: COMOO.colors.medianGray,
              //     fontSize: responsive(context, 12.0),
              //   ),
              //   maxLines: 1,
              //   overflow: TextOverflow.ellipsis,
              // ),
              Text(
                '${group?.group?.description ?? ''}',
                style: TextStyle(
                  fontFamily: COMOO.fonts.montSerrat,
                  color: COMOO.colors.lead,
                  fontSize: responsive(context, 12.0),
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
          trailing: IconButton(
            iconSize: responsive(context, 28.0),
            icon: Container(
              // padding: EdgeInsets.only(right: responsive(context, 25.0)),
              child: Image.asset(
                group.isSelected
                    ? 'images/checked_turquoise.png'
                    : 'images/add_white.png',
                alignment: Alignment.centerLeft,
                height: responsive(context, 28.0),
                width: responsive(context, 28.0),
              ),
            ),
            onPressed: () {
              setState(() {
                group.isSelected = !group.isSelected;
              });
            },
          ),
        );
      },
    );
  }

  Future<void> _handleSubmit(BuildContext context) async {
    final List<GroupUser> selected = groups
        .where((GroupSelection gs) => gs.isSelected)
        .map<GroupUser>((GroupSelection gs) => gs.group)
        .toList();
    if (selected.isEmpty) {
      showMessageDialog(context,
          'Você precisa selecionar pelo menos um grupo para publicar.');
    } else {
      Navigator.of(context).pop(selected
          .map<Map<String, dynamic>>((GroupUser gu) => <String, dynamic>{
                'name': gu.name,
                'id': gu.id,
                'thumb': gu.avatar,
              })
          .toList());
    }
  }
}

class GroupSelection {
  GroupSelection.fromGroupUser(GroupUser gu) : group = gu;

  bool isSelected = false;
  GroupUser group;
}
