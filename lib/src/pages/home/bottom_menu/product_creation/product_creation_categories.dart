import 'package:comoo/src/comoo/categories_model.dart';
import 'package:comoo/src/comoo/group.dart';
import 'product_creation_configs.dart';
import 'product_creation_group_picker.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/lined_circle.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ProductCreationCategories extends StatefulWidget {
  ProductCreationCategories();

  @override
  _ProductCreationCategoriesState createState() =>
      _ProductCreationCategoriesState();
}

class _ProductCreationCategoriesState extends State<ProductCreationCategories>
    with Reusables {
  final List<CategoriesModel> categoryList = categoriesModel;
  final NetworkApi api = NetworkApi();

  final BehaviorSubject<List<GroupUser>> _userGroups =
      BehaviorSubject<List<GroupUser>>();
  Function(List<GroupUser>) get editUserGroups => _userGroups.sink.add;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.round(
        title: Text('anúncio'),
        color: COMOO.colors.turquoise,
        actions: <Widget>[
          FlatButton(
            onPressed: () => _advance(context),
            child: Text(
              'AVANÇAR',
              style: TextStyle(
                fontFamily: COMOO.fonts.montSerrat,
                fontSize: responsive(context, 13),
                color: Colors.white,
                fontWeight: FontWeight.bold,
                letterSpacing: responsive(context, -0.103),
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: CustomScrollView(
          shrinkWrap: true,
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                margin: EdgeInsets.only(
                  top: responsive(context, 25),
                  left: responsive(context, 15),
                ),
                child: Text(
                  'Selecione a(s) categoria(s) do seu produto:',
                  style: TextStyle(
                    fontFamily: COMOO.fonts.montSerrat,
                    fontSize: responsive(context, 15),
                    color: COMOO.colors.lead,
                    fontWeight: FontWeight.w600,
                    letterSpacing: responsive(context, -0.144),
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(height: responsive(context, 22)),
            ),
            SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                childAspectRatio: responsive(context, 83 / 97),
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int i) {
                  final CategoriesModel category = categoryList[i];
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        category.isSelected = !category.isSelected;
                      });
                    },
                    child: Column(
                      children: <Widget>[
                        _buildCategoryIcon(context, category),
                        SizedBox(height: responsive(context, 5)),
                        SizedBox(
                          width: responsive(context, 66),
                          child: Text(
                            category.title,
                            maxLines: 2,
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                            style: ComooStyles.categoriesBold,
                          ),
                        ),
                      ],
                    ),
                  );
                },
                childCount: categoryList.length,
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(height: responsive(context, 22)),
            ),
            SliverToBoxAdapter(
              child: Container(
                margin:
                    EdgeInsets.symmetric(horizontal: responsive(context, 20)),
                child: RaisedButton(
                  elevation: 0.0,
                  color: ComooColors.turqueza,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  onPressed: () => _advance(context),
                  child: Text(
                    'AVANÇAR',
                    style: ComooStyles.botao_branco,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _advance(BuildContext context) async {
    //
    print('AVANÇAR');
    final List<GroupUser> userGroups = _userGroups.value ?? <GroupUser>[];
    final List<CategoriesModel> categories =
        categoryList.where((CategoriesModel cat) => cat.isSelected).toList();
    if (categories.isEmpty) {
      showMessageDialog(context, 'Selecione uma categoria para continuar');
      return null;
    }
    final List<String> selectedCategories =
        categories.map((CategoriesModel cm) => cm.id).toList();
    final List<GroupUser> groups = userGroups.map((GroupUser gu) {
      if (gu?.categories?.isNotEmpty ?? false) {
        final List<String> groupCategories =
            gu.categories.map((dynamic c) => c.toString()).toList();
        bool contains = false;
        for (String groupCategory in groupCategories) {
          if (selectedCategories.contains(groupCategory)) {
            contains = true;
            break;
          }
        }
        // print('\n\n >> ${gu.name} Contains? $contains $groupCategories');
        if (contains) {
          return gu;
        } else {
          return null;
        }
      } else {
        return gu;
      }
    }).toList();
    groups.removeWhere((GroupUser gu) => gu == null);
    // print('\n\n>> Groups: ${groups.length}\n\n');
    final List selectedGroups =
        await Navigator.of(context).push<List>(MaterialPageRoute(
      builder: (BuildContext context) =>
          ProductCreationGroupPicker(groups: groups, categories: categories),
    ));
    // final List selectedGroups =
    //     await Navigator.of(context).push<List>(MaterialPageRoute(
    //   builder: (BuildContext context) => UserGroupPickerPage(),
    // ));
    if (selectedGroups != null) {
      final ProductCreationConfigs configs = ProductCreationConfigs(
        groups: selectedGroups,
        categories: categories,
        isPublishing: true,
      );
      Navigator.of(context).pop(configs);
    }
  }

  Widget _buildCategoryIcon(BuildContext context, CategoriesModel category) {
    const double iconSize = 60;
    const double strokeWidth = 2.0;
    const double padding = 3.0;

    final Widget icon = FittedBox(
      child: Padding(
        padding: EdgeInsets.all(responsive(context, padding)),
        child: Icon(
          category.iconData,
          size: responsive(context, iconSize),
          color: COMOO.colors.turquoise,
        ),
      ),
    );

    return LimitedBox(
        maxHeight: responsive(context, iconSize + (padding * 2)),
        maxWidth: responsive(context, iconSize + (padding * 2)),
        child: Center(
          child: category.isSelected
              ? LinedCircle(
                  color: COMOO.colors.pink,
                  strokeWidth: strokeWidth,
                  child: icon,
                )
              : icon,
        ));
  }

  @override
  void dispose() {
    super.dispose();

    categoriesModel.forEach((CategoriesModel cat) {
      cat.isSelected = false;
    });
    _userGroups?.close();
  }

  @override
  void initState() {
    super.initState();

    _fetchUserGroups();
  }

  Future<void> _fetchUserGroups() async {
    final DateTime before = DateTime.now();
    final List<GroupUser> list = await api.fetchCurrentUserGroups();
    final DateTime after = DateTime.now();
    print('Fetching user groups took: '
        '${after.difference(before).inMilliseconds} milliseconds');
    editUserGroups(list);
  }
}
