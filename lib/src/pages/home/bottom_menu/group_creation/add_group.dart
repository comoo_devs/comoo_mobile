import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/categories_model.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/pages/common/group/group_chat/group_chat.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:image_cropper/image_cropper.dart';
import 'dart:io';
import 'package:comoo/src/comoo/categoryItem.dart';
import 'package:comoo/src/utility/states_list.dart';
import 'package:rxdart/rxdart.dart';

class AddGroupPage extends StatefulWidget {
  AddGroupPage({
    this.id,
    this.openFeed = true,
    this.categories = const <CategoriesModel>[],
  });
  final String id;
  final bool openFeed;
  final List<CategoriesModel> categories;
  @override
  State<AddGroupPage> createState() => _AddGroupPageState();
}

class _AddGroupPageState extends State<AddGroupPage> with Reusables {
  Color baseColor = ComooColors.roxo;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _isLoadingCurrentUser = false;
  Firestore _db = Firestore.instance;
  List<FriendItem> friendsList = List<FriendItem>();
  List<CategoryItem> categoryList = List<CategoryItem>();
  Group group;

  String _name;
  final TextEditingController nameController = TextEditingController();
  File _avatarImage;
  String _avatar = '';

  List<StateItem> statesList = List<StateItem>();
  List<String> citiesList = List<String>();
  String _stateInitial = '';
  String _stateName = '';
  final TextEditingController stateNameController = TextEditingController();
  String _city = '';
  final TextEditingController cityController = TextEditingController();
  String _description;
  final TextEditingController descriptionController = TextEditingController();
  String _location;
  final TextEditingController locationController = TextEditingController();
  bool _autoValidate = false;
  bool _loading = true;
  String _loadingMessage = 'Carregando...';
  // final FocusNode _focusNodeName = FocusNode();
  // final FocusNode _focusNodeLocation = FocusNode();
  // final FocusNode _focusNodeDescription = FocusNode();

  final BehaviorSubject<bool> _isPrivate = BehaviorSubject<bool>.seeded(false);
  Function(bool) get editPrivate => _isPrivate.sink.add;
  Stream<bool> get isPrivate => _isPrivate.stream;

  Widget _buildImagePicker(File image) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    if (image == null) {
      if (_avatar.isEmpty) {
        return Container(
          width: mediaQuery * 203.0,
          height: mediaQuery * 203.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: ComooColors.chumbo)),
          padding: EdgeInsets.only(right: mediaQuery * 17.0),
          child: Icon(
            ComooIcons.camera,
            size: mediaQuery * 43.0,
          ),
        );
      } else {
        return Container(
          width: mediaQuery * 203.0,
          height: mediaQuery * 203.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(image: NetworkImage(_avatar)),
              border: Border.all(color: ComooColors.chumbo)),
          padding: EdgeInsets.only(right: mediaQuery * 17.0),
        );
      }
    } else {
      return Container(
          width: mediaQuery * 203.0,
          height: mediaQuery * 203.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: ComooColors.chumbo),
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: FileImage(image),
                  alignment: Alignment.center)));
    }
  }

  void getImage(ImageSource source) async {
    if (!(await checkPermissions(source))) {
      return null;
    }
    var image = await ImagePicker.pickImage(source: source);
    if (image == null) return null;

    // setState(() {
    // _loading = true;
    // _loadingMessage = 'Carregando...';
    // });

    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        maxWidth: 550,
        maxHeight: 550,
        toolbarColor: ComooColors.roxo,
        toolbarTitle: 'COMOO');

    setState(() {
      // _loading = false;
      _avatarImage = croppedFile;
    });
  }

  void selectOrigin() async {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    var result = await showDialog<ImageSource>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text(
              'ESCOLHA ORIGEM',
              style: ComooStyles.appBarTitle,
            ),
            children: <Widget>[
              SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, ImageSource.camera);
                  },
                  child: Row(
                    children: <Widget>[
                      const Icon(
                        ComooIcons.camera,
                        color: ComooColors.roxo,
                      ),
                      Padding(
                          padding: EdgeInsets.only(left: mediaQuery * 20.0),
                          child: Text(
                            'Camera',
                            style: ComooStyles.botoes,
                          )),
                    ],
                  )),
              SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, ImageSource.gallery);
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(
                        ComooIcons.galeria,
                        color: ComooColors.roxo,
                        size: mediaQuery * 34.0,
                      ),
                      Padding(
                          padding: EdgeInsets.only(left: mediaQuery * 10.0),
                          child: const Text(
                            'Galeria de Fotos',
                            style: ComooStyles.botoes,
                          )),
                    ],
                  ))
            ],
          );
        });
    if (result == null) {
      return;
    }
    getImage(result);
  }

  // dynamic internal(dynamic name) {
  //   print(name);
  //   return '';
  // }

  String handleNullPic(json) {
    try {
      return json.data['photoURL'];
    } catch (e) {
      return '';
    }
  }

  String handleNull(value) {
    try {
      return value.data['name'];
    } catch (e) {
      return '';
    }
  }

  String handleUUID(dynamic json) {
    try {
      return json.documentID;
    } catch (e) {
      return '';
    }
  }

  Future fetchCurrentUser() async {
    final FirebaseAuth auth = FirebaseAuth.instance;
    final FirebaseUser fireUser = await auth.currentUser();
    final DocumentReference userRef =
        _db.collection('users').document(fireUser.uid);
    final QuerySnapshot friendsSnapshot = await userRef
        .collection('friends')
        .getDocuments()
        .timeout(const Duration(seconds: 60));
    if (friendsSnapshot.documents.isNotEmpty) {
      final List<Future<FriendItem>> friendsFuture = <Future<FriendItem>>[];
      friendsSnapshot.documents.forEach((DocumentSnapshot ref) {
        final DocumentReference userRef = ref.data['id'];
        friendsFuture.add(userRef.get().then((DocumentSnapshot user) {
          // try {
          //   // print('name: ${user.metadata}');
          //   internal(user);
          // } catch (e) {
          //   print(e);
          // }
          return FriendItem(
              name: handleNull(user),
              photoURL: handleNullPic(user),
              uid: handleUUID(user));
        }));

        // DocumentSnapshot user = await userRef.get();
        // print(user.data['photoURL']);
        // setState(() {
        //   friendsList.add(

        // });
      });
      Future.wait(friendsFuture).then((List<FriendItem> items) {
        items
            .sort((FriendItem f1, FriendItem f2) => f1.name.compareTo(f2.name));
        if (mounted) {
          setState(() {
            friendsList = items;
            _isLoadingCurrentUser = false;
          });
        }
      });
    } else {
      if (mounted) {
        setState(() {
          _isLoadingCurrentUser = false;
        });
      }
    }
    return null;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
      value,
      style: ComooStyles.textoSnackBar,
    )));
  }

  Future<String> _uploadAvatar(String groupId) async {
    if (_avatarImage == null && _avatar.isNotEmpty) return _avatar;
    final StorageReference ref =
        FirebaseStorage.instance.ref().child('/groups/$groupId/avatar.jpg');
    final StorageTaskSnapshot uploadTask =
        await ref.putFile(_avatarImage).onComplete;
    final String result = await uploadTask.ref.getDownloadURL().then((onValue) {
      return onValue.toString();
    }).catchError((onError) {
      print(onError);
      return '';
    });
    return result;
  }

  Future<void> _createGroup(
      List<FriendItem> selected, List<CategoryItem> categorySelected) async {
    final DocumentReference groupRef = widget.id == null
        ? _db.collection('groups').document()
        : _db.collection('groups').document(widget.id);
    // Upload image
    String photoURL = await _uploadAvatar(groupRef.documentID);
    //batch
    WriteBatch batch = _db.batch();

    var address = {'city': _city, 'state': _stateName};
    // create group
    batch.setData(
      groupRef,
      <String, dynamic>{
        'name': _name ?? '',
        'date': DateTime.now(),
        'description': _description ?? '',
        'location': _location ?? '',
        'address': address,
        'categories': categorySelected.map((c) => c.id).toList(),
        'avatar': photoURL,
        'askToJoin': _isPrivate?.value ?? true,
        'admin': currentUser.uid
      },
      merge: true,
    );

    var groupUserRef = groupRef.collection('users').document(currentUser.uid);
    batch.setData(
      groupUserRef,
      <String, dynamic>{
        'id': currentUser.uid,
        'name': currentUser.name,
        'admin': true
      },
      merge: true,
    );

    inviteUsers(selected, groupRef);
    // selected.forEach((FriendItem friend) {
    //   var groupFriendRef = groupRef.collection('users').document(friend.uid);
    //   batch.setData(groupFriendRef,
    //       <String, dynamic>{'name': friend.name, 'id': friend.uid});
    //   var userGroupRef = _db
    //       .collection('users')
    //       .document(friend.uid)
    //       .collection('groups')
    //       .document(groupRef.documentID);
    //   batch.setData(
    //     userGroupRef,
    //     <String, dynamic>{
    //       'name': _name,
    //       'description': _description,
    //       'id': groupRef.documentID,
    //       'admin': false,
    //       'avatar': photoURL
    //     },
    //     merge: true,
    //   );
    // });

    var userGroupCollectionRef = _db
        .collection('users')
        .document(currentUser.uid)
        .collection('groups')
        .document(groupRef.documentID);
    batch.setData(
        userGroupCollectionRef,
        <String, dynamic>{
          'name': _name,
          'description': _description,
          'id': groupRef.documentID,
          'admin': true,
          'avatar': photoURL
        },
        merge: true);

    if (widget.id == null) {
      var groupMessageRef = groupRef.collection('messages').document();
      batch.setData(groupMessageRef, <String, dynamic>{
        'date': DateTime.now(),
        'from': _db.collection('users').document(currentUser.uid),
        'text': '${currentUser.name} criou esse grupo',
        'type': 'event'
      });
    }
    // print(groupRef.documentID);
    // DocumentSnapshot snap =
    //     await _db.collection('groups').document(groupRef.documentID).get();
    // print(snap.documentID);
    // return await _db.collection('groups').document(groupRef.documentID).get().then((snap) {
    //   print(snap.documentID);
    //   return batch.commit();
    // });

    return batch.commit().then((result) async {
      return await _db
          .collection('groups')
          .document(groupRef.documentID)
          .get()
          .then((snap) {
        group = Group.fromSnapshot(snap);
        print(snap.documentID);
      });
    }).catchError((error) {
      print(error);
      showInSnackBar('Algo deu errado, verifique a conexão e tente novamente');
    });
  }

  inviteUsers(List friends, DocumentReference groupRef) async {
    if (friends == null || friends.isEmpty) {
      return false;
    }
    _addUsersToGroup(
      currentUser.uid,
      currentUser.name,
      groupRef,
      friends,
    );
    // showCustomDialog(
    //   context: context,
    //   alert: customRoundAlert(
    //     context: context,
    //     title: 'Seu convite foi enviado com sucesso :)',
    //   ),
    // );
  }

  _addUsersToGroup(
    String fromID,
    String fromName,
    DocumentReference groupRef,
    List<FriendItem> users,
  ) async {
    // DocumentSnapshot groupSnap = await groupRef.get();
    // WriteBatch batch = _db.batch();
    users.forEach((FriendItem user) {
      CloudFunctions.instance
          .getHttpsCallable(functionName: 'generateUserNotifications')
          .call(<String, dynamic>{
        'type': 'group_invite',
        'ref': groupRef.documentID,
        'userId': user.uid,
        'senderId': currentUser.uid,
      }).catchError((error) {
        print('>> generateUserNotifications Error!\n$error');
        var result = {'result': 'error!'};
        return result;
      });
      // var userRequestNotificationRef = _db
      //     .collection('users')
      //     .document(user.uid)
      //     .collection('notifications')
      //     .document();
      // print('${user.uid} ${userRequestNotificationRef.documentID}');

      // batch.setData(
      //   userRequestNotificationRef,
      //   <String, dynamic>{
      //     'id': userRequestNotificationRef.documentID,
      //     'thumb': groupSnap['avatar'],
      //     'from': fromID,
      //     'type': 'group_invite',
      //     'date': DateTime.now(),
      //     'name': fromName,
      //     'group': groupRef.documentID,
      //     'text':
      //         'Convidou você para entrar na comoonidade ${groupSnap['name']}',
      //   },
      // );
    });
    // batch.commit();
  }

  Future _handleSubmitted(
      List<FriendItem> selected, List<CategoryItem> categoriesSelected) async {
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autoValidate = true;
      showInSnackBar('Verifique as informações e tente novamente');
    } else {
      if (_stateName == '') {
        showInSnackBar('Selecione o estado.');
      } else if (_city == '') {
        showInSnackBar('Selecione a cidade.');
      } else if (_avatarImage == null && _avatar.isEmpty) {
        showInSnackBar('Selecione a imagem do grupo');
      } else {
        setState(() {
          _loadingMessage = 'Salvando informações da comoonidade';
          _loading = true;
        });
        form.save();
        await _createGroup(selected, categoriesSelected);
        showInSnackBar('Grupo $_name criado com sucesso');
        Navigator.of(context).pop(<String, dynamic>{
          'name': group.name,
          'id': group.id,
          'thumb': group.avatar,
        });
        if (group != null && widget.openFeed) {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => GroupChatPage(group)),
          );
        }
      }
    }
  }

  void fetchGroup(String id) async {
    var groupRef = _db.collection('groups').document(id);
    var groupSnapshot = await groupRef.get();
    var usersSnapshot = await groupRef.collection('users').getDocuments();

    List listCategories = groupSnapshot.data['categories'];
    final Group group = Group.fromSnapshot(groupSnapshot);
    editPrivate(group.askToJoin);
    print(group.name);
    setState(() {
      _avatar = group.avatar; //groupSnapshot.data['avatar'];
      _description = group.description; //groupSnapshot.data['description'];
      descriptionController.text = group.description;
      _name = group.name; //groupSnapshot.data['name'];
      nameController.text = group.name;
      _location = group.location; //groupSnapshot.data['location'];
      locationController.text = group.location;

      if (listCategories != null) {
        if (listCategories.isNotEmpty) {
          listCategories.forEach((id) {
            CategoryItem category =
                categoryList.firstWhere((c) => c.id == id, orElse: null);
            if (category != null) {
              category.selected = true;
            }
          });
        }
      }

      if (usersSnapshot.documents.isNotEmpty) {
        usersSnapshot.documents.forEach((snapshot) {
          if (snapshot.documentID != currentUser.uid) {
            FriendItem friend = friendsList.firstWhere(
                (f) => f.uid == snapshot.documentID,
                orElse: () => null);
            if (friend != null) friend.selected = true;
          }
        });
      }

      if (group.address != null) {
        _city = group?.address?.city ?? '';
        _stateName = group?.address?.state ?? '';
        final StateItem sItem = statesList.firstWhere(
            (StateItem si) => si.name == _stateName,
            orElse: () => null);
        _stateInitial = sItem?.initials ?? '';
      }
      _loading = false;
    });
    print(_name);
  }

  fetchStatesList() async {
    var list = states;
    list.forEach((state) {
      statesList.add(StateItem.fromJSON(state));
    });
  }

  selectCities(String initial) {
    StateItem state = statesList.firstWhere((s) {
      return s.initials == initial;
    });
    _stateName = state.name;
    citiesList = state.cities;
    print(_stateName);
  }

  void onSave() {
    List selected = friendsList.where((f) => f.selected == true).toList();
    List categoriesSelected =
        categoryList.where((c) => c.selected == true).toList();

    // if (selected.length == 0) {
    //   showInSnackBar('Selecione ao menos um integrante');
    //   return;
    // }
    if (categoriesSelected.length == 0) {
      showInSnackBar('Selecione ao menos uma categoria');
      return;
    }
    _handleSubmitted(selected, categoriesSelected);
  }

  Widget _buildCityState() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'ESTADO',
              style: ComooStyles.inputLabelRoxo,
            ),
            Container(
              width: mediaQuery * 360.0,
              height: mediaQuery * 50.0,
              child: DropdownButton(
                isExpanded: true,
                hint: Text(
                  _stateInitial,
                  style: ComooStyles.inputLabelRoxo,
                ),
                // value: _state,
                onChanged: (initial) {
                  selectCities(initial);
                  setState(() {
                    _stateInitial = initial;
                    _city = '';
                  });
                },
                items: statesList.map(
                  (state) {
                    return DropdownMenuItem<String>(
                      child: Text(
                        state.name,
                        style: ComooStyles.inputText,
                      ),
                      value: state.initials,
                    );
                  },
                ).toList(),
              ),
            ),
          ],
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'CIDADE',
              style: ComooStyles.inputLabelRoxo,
            ),
            Container(
              width: mediaQuery * 360.0,
              height: mediaQuery * 50.0,
              child: DropdownButton(
                isExpanded: true,
                hint: Text(
                  _city,
                  style: ComooStyles.inputLabelRoxo,
                ),
                // value: _state,
                onChanged: (name) {
                  setState(() {
                    _city = name;
                  });
                },
                items: citiesList.map(
                  (city) {
                    return DropdownMenuItem<String>(
                      child: Text(
                        city,
                        style: ComooStyles.inputText,
                      ),
                      value: city,
                    );
                  },
                ).toList(),
              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  void dispose() {
    _isPrivate?.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _isLoadingCurrentUser = true;
    fetchStatesList();
    List<CategoriesModel> _categories;
    if (widget.categories.isNotEmpty) {
      _categories = widget.categories;
    } else {
      _categories = categoriesModel;
    }
    categoryList = _categories
        .map((CategoriesModel category) =>
            CategoryItem(id: category.id, name: category.title))
        .toList();
    print('fetching current user');
    fetchCurrentUser().then((_) {
      if (widget.id != null) {
        _loading = true;
        fetchGroup(widget.id);
      } else {
        if (mounted) {
          setState(() {
            _loading = false;
          });
        }
      }
    }).catchError((e) {
      if (mounted) {
        setState(() {
          _loading = false;
        });
      }
    }).whenComplete(() {
      if (mounted) {
        setState(() {
          _loading = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.turqueza,
        // iconTheme: IconThemeData(color: baseColor),
        title: FittedBox(
          child: Text(
            widget.id != null ? 'ALTERAR COMOONIDADE' : 'NOVA COMOONIDADE',
            style: ComooStyles.appBarTitleWhite,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(
              widget.id != null ? 'SALVAR' : 'CRIAR',
              style: ComooStyles.appBar_botao_branco,
            ),
            onPressed: _loading ? null : onSave,
          )
        ],
      ),
      body: _loading
          ? LoadingContainer(
              message: _loadingMessage,
            )
          : SingleChildScrollView(
              child: Column(
                // padding: const EdgeInsets.all(10.0),
                children: <Widget>[
                  Form(
                    key: _formKey,
                    autovalidate: _autoValidate,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: mediaQuery * 28.5),
                          child: GestureDetector(
                            child: _buildImagePicker(_avatarImage),
                            onTap: selectOrigin,
                          ),
                        ),
                        SizedBox(
                          height: 45.0,
                        ),
                        TextFormField(
                          controller: nameController,
                          decoration: ComooStyles.inputDecorationRoxoNoBorder(
                              'nome da sua comoo'),
                          style: ComooStyles.inputLabelRoxo,
                          textCapitalization: TextCapitalization.sentences,
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Campo Obrigatório';
                            }
                            return null;
                          },
                          // initialValue: _name,
                          onSaved: (String name) {
                            _name = name;
                          },
                        ),
                        TextFormField(
                          controller: locationController,
                          // initialValue: _location,
                          decoration: ComooStyles.inputDecorationRoxoNoBorder(
                            'região',
                            // hint: 'ex: bairros, zonas, ...'
                          ),
                          style: ComooStyles.inputLabelRoxo,
                          textCapitalization: TextCapitalization.words,
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Campo Obrigatório';
                            }
                            return null;
                          },
                          onSaved: (String value) {
                            _location = value;
                          },
                        ),
                        _buildCityState(),
                        Container(
                          padding: EdgeInsets.only(
                              top: mediaQuery * 10.0, bottom: mediaQuery * 5.0),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'DESCRIÇÂO',
                            style: ComooStyles.inputLabelRoxo,
                          ),
                        ),
                        TextFormField(
                          controller: descriptionController,
                          // initialValue: _description,
                          decoration: ComooStyles.inputDecorationRoundedRoxo('',
                              hint: 'Descreva o que deseja comprar e vender na\nsua COMOO e as regras de participação.\n' +
                                  'Capriche no texto e atraia usuários que têm\ntudo a ver com sua comoonidade.'),
                          style: ComooStyles.inputText,
                          textCapitalization: TextCapitalization.sentences,
                          maxLines: 3,
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Campo Obrigatório';
                            }
                            return null;
                          },
                          onSaved: (String description) {
                            _description = description;
                          },
                        ),
                        StreamBuilder<bool>(
                          stream: isPrivate,
                          builder: (BuildContext context,
                              AsyncSnapshot<bool> snapshot) {
                            final bool isClosed = snapshot.data ?? false;
                            return Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              textDirection: TextDirection.ltr,
                              children: <Widget>[
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 2 / 3,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    textDirection: TextDirection.ltr,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        isClosed
                                            ? 'COMOONIDADE FECHADA'
                                            : 'COMOONIDADE ABERTA',
                                        style: TextStyle(
                                          fontFamily: COMOO.fonts.montSerrat,
                                          color: COMOO.colors.purple,
                                          fontSize: responsive(context, 14),
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                      Text(
                                        isClosed
                                            ? 'O administrador precisa aprovar a entrada de novos membros'
                                            : 'Novos membros entram sem a aprovaçāo do administrador',
                                        style: TextStyle(
                                          fontFamily: COMOO.fonts.montSerrat,
                                          color: COMOO.colors.lead,
                                          fontSize: responsive(context, 13),
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                CupertinoSwitch(
                                  onChanged: editPrivate,
                                  activeColor: COMOO.colors.turquoise,
                                  value: isClosed,
                                ),
                              ],
                            );
                          },
                        ),
                      ].map((Widget child) {
                        return Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: mediaQuery * 25.0,
                          ),
                          child: child,
                        );
                      }).toList(),
                    ),
                  ),
                  Divider(
                    color: ComooColors.chumbo,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: mediaQuery * 18.0,
                      vertical: mediaQuery * 8.0,
                    ),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'CATEGORIA(S)',
                      textAlign: TextAlign.start,
                      style: ComooStyles.inputLabelRoxo,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: mediaQuery * 18.0,
                      vertical: mediaQuery * 8.0,
                    ),
                    child: Text(
                      'Selecione a(s) categoria(s) de produtos que podem ser anunciados nessa comoonidade',
                      style: TextStyle(
                        fontFamily: ComooStyles.fontFamily,
                        color: ComooColors.cinzaMedio,
                        fontSize: mediaQuery * 12.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Column(
                    children: categoryList.map((category) {
                      return ListTile(
                        dense: true,
                        title: Text(category.name, style: ComooStyles.texto),
                        leading: category.selected
                            ? Image.asset(
                                'images/checked_turquoise.png',
                                height: mediaQuery * 21.0,
                                width: mediaQuery * 21.0,
                              )
                            : Image.asset(
                                'images/unchecked.png',
                                height: mediaQuery * 21.0,
                                width: mediaQuery * 21.0,
                                // scale: mediaQuery * 21.2,
                              ),
                        onTap: () {
                          if (mounted)
                            setState(() {
                              category.selected = !category.selected;
                            });
                        },
                      );
                      // return CheckboxListTile(
                      //   title: Text(category.name, style: ComooStyles.texto),
                      //   dense: true,
                      //   controlAffinity: ListTileControlAffinity.leading,
                      //   activeColor: ComooColors.cinzaMedio,
                      //   onChanged: (value) {
                      //     if (mounted)
                      //       setState(() {
                      //         category.selected = value;
                      //       });
                      //   },
                      //   value: category.selected,
                      // );
                    }).toList(),
                  ),
                  // Divider(
                  //   color: ComooColors.chumbo,
                  // ),
                  // Container(
                  //   child: Text(
                  //     'Enviar convite',
                  //     style: ComooStyles.listTitle,
                  //   ),
                  // ),
                  Divider(
                    color: ComooColors.chumbo,
                  ),
                  Padding(
                    padding: EdgeInsets.all(mediaQuery * 18.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'CONVIDAR PESSOAS',
                          style: TextStyle(
                            fontFamily: COMOO.fonts.montSerrat,
                            fontWeight: FontWeight.w600,
                            fontSize: responsive(context, 14),
                            color: COMOO.colors.turquoise,
                          ),
                        ),
                        SizedBox(
                          height: responsive(context, 27),
                          width: responsive(context, 116),
                          child: OutlineButton(
                            onPressed: () {
                              if (friendsList.isNotEmpty) {
                                friendsList.forEach(
                                  (FriendItem fi) => fi.selected = true,
                                );
                                if (mounted) {
                                  setState(() {});
                                }
                              }
                            },
                            padding: EdgeInsets.all(5),
                            highlightedBorderColor: COMOO.colors.turquoise,
                            child: FittedBox(
                              child: Center(
                                child: Text(
                                  'convidar todos',
                                  style: TextStyle(
                                    fontFamily: COMOO.fonts.montSerrat,
                                    fontWeight: FontWeight.w500,
                                    fontSize: responsive(context, 12),
                                    color: COMOO.colors.turquoise,
                                  ),
                                ),
                              ),
                            ),
                            borderSide: BorderSide(
                              color: COMOO.colors.turquoise,
                              style: BorderStyle.solid,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  responsive(context, 30.0)),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  _isLoadingCurrentUser
                      ? Center(child: CircularProgressIndicator())
                      : friendsList.isEmpty
                          ? Container(
                              margin: EdgeInsets.fromLTRB(
                                mediaQuery * 16.0,
                                0.0,
                                mediaQuery * 16.0,
                                mediaQuery * 24.0,
                              ),
                              child: Center(
                                child: RichText(
                                  text: TextSpan(
                                    text: 'Você ainda não adicionou amigos.\n',
                                    children: <TextSpan>[
                                      TextSpan(
                                          style: TextStyle(
                                            fontFamily: ComooStyles.fontFamily,
                                            color: ComooColors.roxo,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14.0,
                                          ),
                                          text:
                                              'Adicione pessoas à sua lista de amigos e convide-os para a sua comoonidade!')
                                    ],
                                    style: TextStyle(
                                      fontFamily: ComooStyles.fontFamily,
                                      color: ComooColors.roxo,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14.0,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Container(
                              child: ListView(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                children: friendsList.map((FriendItem friend) {
                                  return ListTile(
                                    dense: true,
                                    leading: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        backgroundImage: AdvancedNetworkImage(
                                            friend.photoURL)),
                                    title: Text(
                                      friend.name ?? '',
                                      style: TextStyle(
                                        fontFamily: COMOO.fonts.montSerrat,
                                        fontWeight: FontWeight.w600,
                                        fontSize: responsive(context, 14),
                                        letterSpacing:
                                            responsive(context, 0.257),
                                      ),
                                    ),
                                    // subtitle: Text(friend.nickname??''),
                                    trailing: IconButton(
                                      onPressed: () {
                                        if (mounted) {
                                          setState(() {
                                            friend.selected = !friend.selected;
                                          });
                                        }
                                      },
                                      icon: Image.asset(
                                        friend.selected
                                            ? 'images/checked_turquoise.png'
                                            : 'images/add_white.png',
                                        width: mediaQuery * 30.0,
                                        height: mediaQuery * 30.0,
                                      ),
                                      iconSize: responsive(context, 38),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                  Container(
                    margin: EdgeInsets.symmetric(
                      vertical: mediaQuery * 20.0,
                    ),
                    child: FlatButton(
                      padding: EdgeInsets.symmetric(
                        horizontal: mediaQuery * 110.0,
                      ),
                      onPressed: _loading ? null : onSave,
                      color: ComooColors.turqueza,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(mediaQuery * 20.0),
                      ),
                      child: Text(
                        'CRIAR',
                        style: ComooStyles.botao_branco,
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}

class FriendItem {
  FriendItem({this.name, this.photoURL, this.uid, this.selected = false});

  String name;
  String photoURL;
  String uid;
  bool selected;
}
