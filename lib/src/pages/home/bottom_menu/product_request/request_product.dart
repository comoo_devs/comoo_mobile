import 'dart:async';

import 'package:comoo/src/widgets/dashed_circle.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/validations.dart';

class RequestProductPage extends StatefulWidget {
  RequestProductPage();
  @override
  State createState() {
    return RequestProductPageState();
  }
}

class RequestProductPageState extends State<RequestProductPage> with Reusables {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final StreamController<List<Group>> _streamController = StreamController();
  Stream<List<Group>> _stream;
  final Firestore _db = Firestore.instance;
  List<Group> selectedGroups = List<Group>();
  Validations _validations = Validations();

  // bool _isLoading = false;
  bool _hasGroups = false;
  String _request = '';
  String _location = '';

  Future<void> _handleSubmit() async {
    setState(() {
      // _isLoading = true;
    });
    FormState form = _formKey.currentState;
    if (form == null) {
      _formKey = GlobalKey<FormState>();
      form = _formKey.currentState;
    }
    if (!form.validate()) {
      // _autoValidate = true;
      showInSnackBar('Verifique os erros antes de continuar.');
      setState(() {
        // _isLoading = false;
      });
    } else if (selectedGroups.isEmpty) {
      showInSnackBar('Selecione uma comoonidade antes de continuar.');
      setState(() {
        // _isLoading = false;
      });
    } else if (_request?.isEmpty ?? true) {
      showInSnackBar('O pedido nāo pode estar vazio!');
      setState(() {
        // _isLoading = false;
      });
    } else {
      final WriteBatch batch = _db.batch();

      final List<Map<String, dynamic>> groups =
          selectedGroups.map((Group group) => group.toJson()).toList();
      final DocumentReference postRef = _db.collection('posts').document();
      Map<String, dynamic> post = <String, dynamic>{
        'id': postRef.documentID,
        'date': DateTime.now(),
        'message': _request,
        'type': 'post',
        'groups': groups,
        'location': _location,
        'user': {
          'uid': currentUser.uid,
          'name': currentUser.name,
          'picture': currentUser.photoURL
        }
      };
      batch.setData(postRef, post);

      selectedGroups.forEach((Group group) {
        var groupId = group.id;
        var feed = _db
            .collection('groups')
            .document(groupId)
            .collection('feed')
            .document(postRef.documentID);
        batch.setData(feed, {
          'type': 'post',
          'location': _location,
          'post': postRef,
          'from': currentUser.uid,
          'date': DateTime.now(),
        });
      });

      await batch.commit().then((_) {
        _displaySuccessAlert();
      }).catchError((e) => print('>> Error $e '));
    }
    setState(() {
      // _isLoading = false;
    });
  }

  _displaySuccessAlert() async {
    await showCustomDialog(
      context: context,
      alert: customRoundAlertTitle(
        context: context,
        title: 'PEDIDO ENVIADO',
        subtitle: 'Seu pedido foi enviado para os grupos selecionados',
        // content: Text(
        //   'Seu pedido foi enviado para os grupos selecionados',
        //   style: ComooStyles.texto,
        // ),
      ),
    );
    setState(() {
      // _isLoading = false;
    });
    Navigator.pop(context);
  }

  Widget _buildUserRow() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Padding(
      padding: EdgeInsets.only(
        top: mediaQuery * 16.0,
        bottom: mediaQuery * 12.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              left: mediaQuery * 15.0,
              right: mediaQuery * 10.0,
            ),
            width: mediaQuery * 60.0,
            height: mediaQuery * 60.0,
            child: ClipOval(
              child: Image(
                image: AdvancedNetworkImage(currentUser.photoURL,
                    useDiskCache: true),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Column(
            children: <Widget>[
              Container(
                child: RichText(
                  overflow: TextOverflow.ellipsis,
                  text: TextSpan(
                    text: currentUser.name + ' ',
                    style: ComooStyles.nomeFeed,
                  ),
                ),
              ),
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
          ),
        ],
      ),
    );
  }

  Widget _buildRequestContent() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: mediaQuery * 46.0),
            height: 240.0,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  ComooColors.roxo,
                  ComooColors.pink,
                ],
              ),
            ),
            alignment: Alignment.center,
            child: TextField(
              decoration: InputDecoration(
                // labelText: '',
                hintText: 'Precisando de alguma coisa?\n'
                    'Faça aqui seu pedido e\n'
                    'compartilhe nas comoonidades\n'
                    'que você participa',
                hintStyle: ComooStyles.hintIndicacao,
                labelStyle: ComooStyles.inputLabel,
                border: InputBorder.none,
              ),
              textCapitalization: TextCapitalization.sentences,
              style: TextStyle(
                color: Colors.white,
                fontFamily: ComooStyles.fontFamily,
                fontWeight: FontWeight.w500,
                fontSize: 20.0,
              ),
              maxLines: 5,
              keyboardType: TextInputType.multiline,
              onChanged: (String value) {
                _request = value.trim();
              },
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: mediaQuery * 28.0),
            child: TextFormField(
              decoration: ComooStyles.inputDecoration(
                'Localização',
                hint: 'digite sua localização',
              ),
              style: ComooStyles.inputText,
              textCapitalization: TextCapitalization.words,
              validator: _validations.validateEmpty,
              initialValue: _location,
              keyboardType: TextInputType.text,
              onSaved: (String location) {
                _location = location.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildEmptyList() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return <Widget>[
      Container(
        margin: EdgeInsets.fromLTRB(
          mediaQuery * 16.0,
          0.0,
          mediaQuery * 16.0,
          mediaQuery * 24.0,
        ),
        child: Center(
          child: RichText(
            text: TextSpan(
              text: 'Você não possui comoonidades.\n',
              children: <TextSpan>[
                TextSpan(
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: ComooColors.roxo,
                    fontWeight: FontWeight.w500,
                    fontSize: 14.0,
                  ),
                  text: 'Entre para uma comoonidade para fazer um pedido!',
                )
              ],
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.roxo,
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
              ),
            ),
          ),
        ),
      )
    ];
  }

  Widget _buildGroupTile(Group group) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final Widget thumb = Padding(
      padding: EdgeInsets.all(mediaQuery * 4.0),
      child: CircleAvatar(
        backgroundImage: AdvancedNetworkImage(group.avatar),
        radius: mediaQuery * 27.5,
      ),
    );
    return Column(
      children: <Widget>[
        ListTile(
          contentPadding: EdgeInsets.symmetric(
            horizontal: mediaQuery * 10.0,
          ),
          // leading: Container(
          //   decoration: BoxDecoration(
          //       border: Border.all(color: ComooColors.cinzaClaro),
          //       borderRadius: BorderRadius.circular(mediaQuery * 30.0),
          //       image: DecorationImage(
          //         fit: BoxFit.cover,
          //         image: AdvancedNetworkImage(group.avatar),
          //       )),
          //   width: mediaQuery * 55.0,
          //   height: mediaQuery * 55.0,
          // ),

          leading: group.userAdmin == currentUser.uid
              ? DashedCircle(
                  color: COMOO.colors.turquoise,
                  dashes: 25,
                  child: thumb,
                )
              : thumb,
          title: Text(
            group.name,
            style: ComooStyles.productTitle,
          ),
          isThreeLine: true,
          subtitle: RichText(
            maxLines: 4,
            overflow: TextOverflow.ellipsis,
            text: TextSpan(
              text:
                  '${group.location} - ${group.address.city} - ${group.address.state}',
              style: ComooStyles.productTitlePrice.copyWith(
                  color: COMOO.colors.gray, fontSize: mediaQuery * 12.0),
              children: <TextSpan>[
                TextSpan(text: '\n'),
                TextSpan(
                  text: '${group.users.length} membros',
                  style: ComooStyles.productTitlePrice,
                ),
                TextSpan(text: '\n'),
                TextSpan(
                  text: '${group.description}',
                  style: ComooStyles.productTitlePrice,
                ),
              ],
            ),
          ),
          // subtitle: Text(
          //   '${group.users.length} membros\n'
          //       '${group.description}',
          //   style: ComooStyles.productTitlePrice,
          //   overflow: TextOverflow.ellipsis,
          //   maxLines: 2,
          // ),
          trailing: Container(
            padding: EdgeInsets.only(right: mediaQuery * 25.0),
            child: Image.asset(
              selectedGroups.contains(group)
                  ? 'images/checked_turquoise.png'
                  : 'images/add_white.png',
              height: mediaQuery * 30.0,
              width: mediaQuery * 30.0,
              alignment: Alignment.centerLeft,
            ),
          ),
          onTap: () {
            setState(() {
              selectedGroups.contains(group)
                  ? selectedGroups.remove(group)
                  : selectedGroups.add(group);
            });
            // Navigator.of(context).push(MaterialPageRoute(
            //     builder: (context) => GroupChatPage(_groups[index])));
          },
        ),
        Divider(
          height: mediaQuery * 2.0,
          color: COMOO.colors.gray,
        ),
      ],
    );
  }

  List<Widget> _buildSnapshotData(AsyncSnapshot<List<Group>> snapshot) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    List<Widget> list = <Widget>[];
    list.add(Container(
      padding: EdgeInsets.only(
        left: mediaQuery * 18.0,
        top: mediaQuery * 8.0,
      ),
      child: Text(
        'Selecione as comoonidades:',
        style: ComooStyles.texto_bold,
      ),
    ));
    list.addAll(snapshot.data.map((Group g) => _buildGroupTile(g)).toList());
    return list;
  }

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.turqueza,
        title: Text('fazer um pedido'),
        actions: <Widget>[
          _hasGroups
              ? FlatButton(
                  child: Text(
                    'PUBLICAR',
                    style: ComooStyles.botao_branco,
                  ),
                  onPressed: _handleSubmit,
                )
              : Container(),
        ],
      ),
      body: StreamBuilder<List<Group>>(
        stream: _stream,
        builder: (BuildContext context, AsyncSnapshot<List<Group>> snapshot) {
          List<Widget> list = <Widget>[];
          list.add(_buildUserRow());
          list.add(_buildRequestContent());
          List<Widget> snaps = <Widget>[];
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              snaps = snapshot.hasData
                  ? _buildSnapshotData(snapshot)
                  : _buildEmptyList();
              break;
            case ConnectionState.done:
              snaps = snapshot.hasData
                  ? _buildSnapshotData(snapshot)
                  : _buildEmptyList();
              break;
            case ConnectionState.none:
              snaps = _buildEmptyList();
              break;
            case ConnectionState.waiting:
              snaps = snapshot.hasData
                  ? _buildSnapshotData(snapshot)
                  : _buildLoadingSnapshot();
              break;
          }
          list.addAll(snaps);
          list.add(
            Container(
              padding: EdgeInsets.only(bottom: mediaQuery * 28.0),
              margin: EdgeInsets.symmetric(horizontal: mediaQuery * 50.0),
              child: RaisedButton(
                elevation: 0.0,
                color: ComooColors.turqueza,
                onPressed: snapshot.hasData ? _handleSubmit : null,
                child: Text(
                  'PUBLICAR',
                  style: TextStyle(color: Colors.white),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(mediaQuery * 50.0),
                ),
              ),
            ),
          );
          return ListView(
            children: list,
          );
        },
      ),
    );
  }

  List<Widget> _buildLoadingSnapshot() {
    return [const Center(child: CircularProgressIndicator())];
  }

  @override
  void initState() {
    _fetchCurrentUserGroups();
    _stream = _streamController.stream.asBroadcastStream();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_formKey.currentState == null) {
      _formKey = GlobalKey<FormState>();
    }
  }

  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  Future<void> _fetchCurrentUserGroups() async {
    List<Group> groups = List();
    var _user = currentUser;
    await _db
        .collection('users')
        .document(_user.uid)
        .collection('groups')
        .getDocuments()
        .then((QuerySnapshot query) {
      int length = query.documents.length;
      // int count = 0;
      if (length == 0) {
        _streamController.close();
        return;
      }
      query.documents.forEach((DocumentSnapshot snapshot) async {
        await _db
            .collection('groups')
            .document(snapshot.documentID)
            .get()
            .then((DocumentSnapshot groupSnap) async {
          // count++;
          Group group = Group.fromSnapshot(groupSnap);
          await _db
              .collection('groups')
              .document(snapshot.documentID)
              .collection('users')
              .getDocuments()
              .then((QuerySnapshot usersQuery) {
            usersQuery.documents.forEach((DocumentSnapshot userSnaps) {
              group.addUserFromSnapshot(userSnaps);
            });
          });
          setState(() {
            _hasGroups = true;
          });
          groups.add(group);
          groups.sort((Group g1, Group g2) => g1.name.compareTo(g2.name));
          _streamController.sink.add(groups);
        });
      });
    });
  }

  void showInSnackBar(String value) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Padding(
        padding: EdgeInsets.all(mediaQuery * 8.0),
        child: Text(value, style: ComooStyles.textoSnackBar),
      ),
      duration: Duration(seconds: 10),
    ));
  }
}
