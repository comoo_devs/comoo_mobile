import 'dart:async';

import 'package:comoo/src/blocs/buyings_bloc.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/group/group_chat/group_chat.dart';
import 'package:comoo/src/pages/common/group/group_detail.dart';
import 'package:comoo/src/pages/common/transaction/transaction_view.dart';
import 'package:comoo/src/pages/common/withdraw_page.dart';
import 'package:comoo/src/utility/responsiveness.dart';
import 'profile/purchases/buy_history.dart';
import 'search/search_hero.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/widgets/comments/product_comments.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/comoo.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';
import 'package:share/share.dart';
import 'feed/feed.dart';
import 'profile/profile.dart';
import 'package:flutter/cupertino.dart';
import 'bottom_menu/product_request/request_product.dart';
import 'notification/notification_messages.dart';
import 'bottom_menu/product_creation/product_create.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/pages/common/chat/direct_chat.dart';
import 'package:flushbar/flushbar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainPage extends StatefulWidget {
  final Comoo comoo;

  MainPage(this.comoo);

  @override
  State<StatefulWidget> createState() {
    return _MainPageState();
  }
}

class _MainPageState extends State<MainPage>
    with WidgetsBindingObserver, Reusables {
  final Connectivity connectivity = Connectivity();
  final NetworkApi api = NetworkApi();
  StreamSubscription<ConnectivityResult> subscription;
  final Flushbar connectionError = Flushbar(
    flushbarPosition: FlushbarPosition.TOP,
    title: 'Erro de conexão',
    message: 'Sem conexão com a internet!',
    backgroundColor: Colors.redAccent,
    boxShadows: <BoxShadow>[
      BoxShadow(color: Colors.red[800]),
    ],
    icon: Icon(
      Icons.error_outline,
      color: Colors.white,
    ),
    mainButton: FlatButton(
      child: Icon(
        Icons.close,
        color: COMOO.colors.lead,
      ),
      onPressed: () {
        // if (!connectionError.isDismissed()) {
        //   connectionError.dismiss();
        // }
      },
    ),
  );

  int counter = 0;

  final BehaviorSubject<bool> _refreshFeed = BehaviorSubject<bool>();
  Function(bool) get editRefreshFeed => _refreshFeed.sink.add;
  Stream<bool> get refreshFeed => _refreshFeed.stream;

  BehaviorSubject<bool> _wallet = BehaviorSubject<bool>();
  Function(bool) get changeWallet => _wallet.sink.add;
  Stream<bool> get wallet => _wallet.stream;

  // String _searchText = '';
  // bool _isSearchActive = false;
  final FocusNode searchFocusNode = FocusNode();
  ScrollController feedScrollController; // = ScrollController();
  StreamController cameraButtonController;
  bool _isCameraOpen = false;

  PageController pageController;
  int _currentIndex = 0;
  bool _hasUnreadNotification = false;
  static final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  // FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  //      FlutterLocalNotificationsPlugin();
  final Firestore db = Firestore.instance;
  Future<void> onSelectNotification(String id) async {
    if (currentUser == null) {
      currentUser = await comoo.currentUser();
    }
    if (currentUser == null) {
      _scaffoldKey.currentState.hideCurrentSnackBar(
        reason: SnackBarClosedReason.dismiss,
      );
      Navigator.of(context).pushReplacementNamed('/login');
    }

    final DocumentSnapshot document =
        await db.collection('negotiations').document(id).get();
    final Negotiation negotiation = Negotiation.fromSnapshot(document);
    _scaffoldKey.currentState.hideCurrentSnackBar(
      reason: SnackBarClosedReason.dismiss,
    );
    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => DirectChatView(negotiation)));
  }

  void _displayLocalNotification(Map<String, dynamic> message) async {
    double mediaQuery = MediaQuery.of(context).size.width / 400;
    String body = '';
    String title = '';
    String negotiationId = '';
    String type = '';
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    final String chat = _prefs.getString('chatOpened') ?? '';

    print('Message:          $message');
    if (message['data'] == null) {
      body = message['body'];
      title = message['title'];
      negotiationId = message['id'] ?? '';
    } else {
      final dynamic data = message['data'];
      if (data != null) {
        body = data['body'];
        title = data['title'];
        type = data['type'] ?? '';
        negotiationId = data['id'] ?? '';
      }
    }
    print('IS CHAT OPENED? $chat != $negotiationId? ${chat != negotiationId}');
    print(type);
    if (chat != negotiationId) {
      switch (type) {
        case 'snackbar':
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: mediaQuery * 210.0,
                    constraints: BoxConstraints(
                      maxWidth: mediaQuery * 210.0,
                    ),
                    child: Text(
                      body,
                      overflow: TextOverflow.clip,
                      maxLines: 2,
                      style: TextStyle(
                        fontFamily: ComooStyles.fontFamily,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: mediaQuery * 13.0,
                      ),
                    ),
                  ),
                  GestureDetector(
                    child: Text(
                      'VER MAIS',
                      style: TextStyle(
                        fontFamily: ComooStyles.fontFamily,
                        color: COMOO.colors.turquoise,
                        fontWeight: FontWeight.normal,
                        fontSize: mediaQuery * 13.0,
                      ),
                    ),
                    onTap: () {
                      final dynamic data =
                          message['data'] == null ? message : message['data'];
                      _navigateFromMessage(data);
                      // pageController.jumpToPage(1);
                      _scaffoldKey.currentState.hideCurrentSnackBar(
                        reason: SnackBarClosedReason.dismiss,
                      );
                    },
                  ),
                  IconButton(
                    alignment: Alignment.center,
                    icon: Icon(Icons.highlight_off),
                    onPressed: () =>
                        _scaffoldKey.currentState.hideCurrentSnackBar(
                      reason: SnackBarClosedReason.dismiss,
                    ),
                  ),
                ],
              ),
            ),
            duration: Duration(seconds: 10),
            backgroundColor: ComooColors.roxo,
          ));
          break;
        default:
          final Flushbar notificationBar = Flushbar(
            flushbarPosition: FlushbarPosition.TOP,
            duration: const Duration(seconds: 3),
            messageText: Text(
              body,
              style: TextStyle(
                fontFamily: ComooStyles.fontFamily,
                color: ComooColors.roxo,
                fontWeight: FontWeight.normal,
                fontSize: mediaQuery * 16.0,
              ),
            ),
            titleText: Text(
              title,
              style: ComooStyles.titulos,
            ),
            // duration : Duration(seconds: 3),
            backgroundColor: Colors.white,
            boxShadows: <BoxShadow>[
              BoxShadow(color: ComooColors.cinzaClaro),
            ],

            mainButton: FlatButton(
              onPressed: () {
                final dynamic data =
                    message['data'] == null ? message : message['data'];
                _navigateFromMessage(data);
                // if (type == 'negotiation_chat') {
                //   final Map<dynamic, dynamic> data = message['data'];
                //   if (data != null) {
                //     String id = data['id'];
                //     if (id != null) {
                //       _navigateToChat(id);
                //     }
                //   }
                // } else {
                //   _jumpToNotifications();
                // }
                // if (negotiationId.isEmpty)
                //   _jumpToNotifications();
                // else
                //   onSelectNotification(negotiationId);
              },
              child: Text(
                'VER',
                style: TextStyle(
                  fontFamily: ComooStyles.fontFamily,
                  fontWeight: FontWeight.w700,
                  fontSize: mediaQuery * 16.0,
                ),
              ),
            ),
          );
          // final Flushbar notification = Flushbar(
          //   messageText: Text(
          //     body,
          //     style: TextStyle(
          //       fontFamily: ComooStyles.fontFamily,
          //       color: ComooColors.roxo,
          //       fontWeight: FontWeight.normal,
          //       fontSize: mediaQuery * 16.0,
          //     ),
          //   ),
          //   titleText: Text(
          //     title,
          //     style: ComooStyles.titulos,
          //   ),
          //   duration: const Duration(seconds: 3),
          //   backgroundColor: Colors.white,
          //   boxShadow: BoxShadow(color: ComooColors.cinzaClaro),
          //   mainButton: FlatButton(
          //     onPressed: () {
          //       if (type == 'negotiation_chat') {
          //         final Map<dynamic, dynamic> data = message['data'];
          //         if (data != null) {
          //           String id = data['id'];
          //           if (id != null) {
          //             _navigateToChat(id);
          //           }
          //         }
          //       } else {
          //         _jumpToNotifications();
          //       }
          //       // if (negotiationId.isEmpty)
          //       //   _jumpToNotifications();
          //       // else
          //       //   onSelectNotification(negotiationId);
          //     },
          //     child: Text(
          //       'VER',
          //       style: TextStyle(
          //         fontFamily: ComooStyles.fontFamily,
          //         fontWeight: FontWeight.w700,
          //         fontSize: mediaQuery * 16.0,
          //       ),
          //     ),
          //   ),
          // );
          notificationBar.show(context);
      }
    }
    // var directory = await getApplicationDocumentsDirectory();
    // var bigPictureResponse = await http.get(
    //     'https://firebasestorage.googleapis.com/v0/b/comoo-app.appspot.com/o/defaultUser.png?alt=media&token=15452a28-dadb-4fc5-a20b-36812e908064');
    // var bigPicturePath = '${directory.path}/comoo-logo';
    // var file =  File(bigPicturePath);
    // await file.writeAsBytes(bigPictureResponse.bodyBytes);

    // var androidPlatformChannelSpecifics =  AndroidNotificationDetails(
    //     'comoo.io.NOTIFICATIONS',
    //     'COMOO',
    //     'Notificações relacionadas as transações com compradores e vendedores',
    //     importance: Importance.Max,
    //     largeIcon: bigPicturePath,
    //     largeIconBitmapSource: BitmapSource.FilePath,
    //     priority: Priority.High);
    // var iOSPlatformChannelSpecifics =  IOSNotificationDetails();
    // var platformChannelSpecifics =  NotificationDetails(
    //     androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    // await flutterLocalNotificationsPlugin.show(0, message['data']['title'] ?? '',
    //     message['data']['body'] ?? '', platformChannelSpecifics,
    //     payload: message['data']['negotiation'] ?? '');
  }

  Future<void> _navigateToChat(String id) async {
    if (id?.isEmpty ?? true) {
      return _jumpToNotifications();
    }
    final DocumentSnapshot snap =
        await db.collection('negotiations').document(id).get();
    final Negotiation negotiation = Negotiation.fromSnapshot(snap);
    // notificationBar.dismiss();
    _scaffoldKey.currentState.hideCurrentSnackBar(
      reason: SnackBarClosedReason.dismiss,
    );
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) => DirectChatView(negotiation),
    ));
  }

  Future<void> _navigateToGroupDetail(String id) async {
    if (id?.isEmpty ?? true) {
      return _jumpToNotifications();
    }
    final DocumentSnapshot snap =
        await db.collection('groups').document(id).get();
    final Group group = Group.fromSnapshot(snap);
    // notificationBar.dismiss();
    _scaffoldKey.currentState.hideCurrentSnackBar(
      reason: SnackBarClosedReason.dismiss,
    );
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) => GroupDetailView(group: group),
    ));
  }

  Future<void> _navigateToGroupFeed(String id) async {
    if (id?.isEmpty ?? true) {
      return _jumpToNotifications();
    }
    final DocumentSnapshot snap =
        await db.collection('groups').document(id).get();
    final Group group = Group.fromSnapshot(snap);
    // notificationBar.dismiss();
    _scaffoldKey.currentState.hideCurrentSnackBar(
      reason: SnackBarClosedReason.dismiss,
    );
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) => GroupChatPage(group),
    ));
  }

  Future<void> _navigateToProductComments(String id) async {
    if (id?.isEmpty ?? true) {
      return _jumpToNotifications();
    }
    final DocumentSnapshot snap =
        await db.collection('products').document(id).get();
    final Product product = Product.fromSnapshot(snap);
    // notificationBar.dismiss();
    _scaffoldKey.currentState.hideCurrentSnackBar(
      reason: SnackBarClosedReason.dismiss,
    );
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) => ProductCommentsView(product),
    ));
  }

  // Future<void> _navigateToNegotiation(String id) async {
  //   if (id?.isEmpty ?? true) {
  //     return _jumpToNotifications();
  //   }
  //   final DocumentSnapshot snap =
  //       await db.collection('negotiations').document(id).get();
  //   final Negotiation negotiation = Negotiation.fromSnapshot(snap);
  //   // notificationBar.dismiss();
  //   _scaffoldKey.currentState.hideCurrentSnackBar(
  //     reason: SnackBarClosedReason.dismiss,
  //   );
  //   Navigator.of(context).push(MaterialPageRoute<void>(
  //     builder: (BuildContext context) => NegotiationPage(negotiation),
  //   ));
  // }

  Future<void> _navigateToNegotiationPayment(String id) async {
    if (id?.isEmpty ?? true) {
      return _jumpToNotifications();
    }
    final DocumentSnapshot snap =
        await db.collection('negotiations').document(id).get();
    final Negotiation negotiation = Negotiation.fromSnapshot(snap);
    // notificationBar.dismiss();
    _scaffoldKey.currentState.hideCurrentSnackBar(
      reason: SnackBarClosedReason.dismiss,
    );
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) =>
          TransactionView(negotiation: negotiation),
    ));
  }

  Future<void> _navigateToWithdraw() async {
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) => WithdrawPage(),
    ));
  }

  Future<void> _navigateToSellings() async {
    Navigator.of(context).pushNamed('/sell_history');
  }

  Future<void> _navigateToBuyings() async {
    Navigator.of(context).pushNamed('/buy_history');
  }

  void _navigateFromMessage(Map<dynamic, dynamic> message) {
    print(message);
    // showMessageDialog(context, '${message['body']} ${message['type']}');
    final String type = message['type'];
    if ((type?.isEmpty) ?? true) {
      _jumpToNotifications();
    } else {
      final String id = message['id'];
      switch (type) {
        case 'negotiation_chat':
        case 'negotiation_start_seller':
        case 'negotiation_start_client':
          _navigateToChat(id);
          break;
        case 'negotiation_pending':
        case 'negotiation_waiting_send':
        case 'negotiation_finished':
          _navigateToSellings();
          break;
        case 'negotiation_cancelled':
          _navigateToBuyings();
          break;
        case 'negotiation_sent':
        case 'group_request':
        case 'group_invite':
          _jumpToNotifications();
          break;
        case 'group_suggestion':
          _navigateToGroupDetail(id);
          break;
        case 'received_money':
          _navigateToWithdraw();
          break;
        case 'approved_solicitation':
          _navigateToGroupFeed(id);
          break;
        case 'product_comment':
          _navigateToProductComments(id);
          break;
        case 'negotiation_twelvehours':
          _navigateToNegotiationPayment(id);
          break;
        default:
          _jumpToNotifications();
          break;
          // case 'product_comment':
          //   // _openProductCommentPage(notification);
          //   break;
          // case 'received_money':
          //   break;
          // case 'negotiation_start':
          // case 'negotiation_start_seller':
          // case 'negotiation_start_client':
          // case 'negotiation_twelvehours':
          // case 'negotiation':
          //   // _openNegotiationChat(notification);
          //   break;
          // case 'group_invite':
          // case 'group_request':
          // case 'approved_solicitation':
          // case 'suggestion':
          //   print('invite');
          //   // _openGroupPage(notification);
          //   break;
          // case 'comment_notification':
          //   // return _openProductPage(notification);
          //   break;
          // final bool isRequestApproval = notification.text
          //     .toLowerCase()
          //     .contains(
          //         'solicitação para entrar na comoonidade');
          // if (isRequestApproval) {
          //   _openGroupPage(notification);
          // } else {
          //   final String type = notification.type.contains('_')
          //       ? notification.type.split('_').first
          //       : notification.type;
          //   switch (type) {
          //     case 'product':
          //       _openProductPage(notification);
          //       break;
          //     case 'group':
          //       _openGroupPage(notification);
          //       break;
          //     case 'user':
          //       _navigateToUserPage(notification.from);
          //       break;
          //     case 'negotiation':
          //       _openNegotiation(notification);
          //       break;
          //     case 'post':
          //       // TODO(luan): Post Page needs to be done.
          //       //  _openPostPage
          //       break;
          //   }
          // }
          break;
      }
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      if (mounted) {
        _updateNotification();
      }
      print('$state');
      checkDynamicLinks(context, this);
    }
  }

  @override
  void initState() {
    super.initState();
    cameraButtonController = StreamController();
    WidgetsBinding.instance.addObserver(this);
    _updateNotification();
    feedScrollController = ScrollController();

    // connectionError.mainButton = FlatButton(
    //   child: Icon(
    //     Icons.close,
    //     color: COMOO.colors.lead,
    //   ),
    //   onPressed: () {
    //     if (!connectionError.isDismissed()) {
    //       connectionError.dismiss();
    //     }
    //   },
    // );
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      print(result);
      switch (result) {
        case ConnectivityResult.mobile:
        case ConnectivityResult.wifi:
          if (!connectionError.isDismissed()) {
            connectionError.dismiss();
          }
          break;
        case ConnectivityResult.none:
        default:
          connectionError.show(context);
          break;
      }
    });

    // var initializationSettingsAndroid =
    //      AndroidInitializationSettings('@mipmap/ic_launcher');
    // var initializationSettingsIOS =  IOSInitializationSettings();
    // var initializationSettings =  InitializationSettings(
    //     initializationSettingsAndroid, initializationSettingsIOS);
    // flutterLocalNotificationsPlugin =  FlutterLocalNotificationsPlugin();
    // flutterLocalNotificationsPlugin.initialize(initializationSettings,
    //     selectNotification: onSelectNotification);

    pageController = PageController();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        // print(message);
        _displayLocalNotification(message);
        if (pageController.page.toInt() != 3) {
          _hasUnreadNotification = true;
          if (mounted) {
            setState(() {
              _hasUnreadNotification = true;
            });
          }
        }
        return null;
      },
      onResume: (Map<String, dynamic> message) {
        final dynamic data =
            message['data'] == null ? message : message['data'];
        _navigateFromMessage(data);
        // print(message);
        // String negotiation = '';
        // if (message['data'] != null) {
        //   negotiation = message['data']['id'];
        // } else if (message['negotiation'] != null) {
        //   negotiation = message['negotiation'];
        // }
        // if (negotiation.isEmpty)
        //   _jumpToNotifications();
        // else {
        //   onSelectNotification(negotiation);
        // }
        return null;
      },
      onLaunch: (Map<String, dynamic> message) {
        final dynamic data =
            message['data'] == null ? message : message['data'];
        _navigateFromMessage(data);
        // print(message);
        // String negotiation = '';
        // if (message['data'] != null) {
        //   negotiation = message['data']['negotiation'];
        // } else if (message['negotiation'] != null) {
        //   negotiation = message['negotiation'];
        // }
        // if (negotiation.isEmpty)
        //   _jumpToNotifications();
        // else {
        //   onSelectNotification(negotiation);
        // }
        return null;
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.getToken().then((token) {
      print(token);
      _updateFCMToken(token);
    });
  }

  Future<void> _updateNotification() async {
    final User user = await comoo.currentUser();
    if (user == null) {
      return;
    }
    if (user.hasNotification) {
      user.hasNotification = false;
      if (mounted) {
        setState(() {
          _hasUnreadNotification = true;
        });
      }
      if (await isConnected()) {
        final WriteBatch batch = db.batch();
        final DocumentReference ref = db.collection('users').document(user.uid);
        batch.updateData(ref, <String, dynamic>{'littleBall': false});
        batch.commit();
        // .updateData(<String, dynamic>{'notification': false});
      }
    }
  }

  Future<void> _jumpToNotifications() async {
    if (currentUser == null) {
      currentUser = await comoo.currentUser();
    }
    if (currentUser == null) {
      _scaffoldKey.currentState.hideCurrentSnackBar(
        reason: SnackBarClosedReason.dismiss,
      );
      Navigator.of(context).pushReplacementNamed('/login');
    }
    setState(() {
      _hasUnreadNotification = false;
    });
    pageController.jumpToPage(3);
  }

  void _updateFCMToken(String token) {
    api.updateFCMToken(token);
    // if (currentUser == null) {
    //   currentUser = await comoo.currentUser();
    // }
    // await currentUser.updateFCMToken(token);
  }

  showNewNotification(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        message,
        style: ComooStyles.texto,
      ),
      backgroundColor: ComooColors.turqueza,
      action: SnackBarAction(
          label: 'Ver Mais',
          onPressed: () {
            _currentIndex = 3;
          }),
    ));
  }

  @override
  void dispose() {
    super.dispose();
    pageController?.dispose();
    cameraButtonController?.close();

    _wallet?.close();
    subscription?.cancel();
    _refreshFeed?.close();
  }

  Future<void> _handleProfileBalanceTap() async {
    print(_wallet.value);
    if (!(_wallet.value ?? false)) {
      final DocumentSnapshot snap =
          await db.collection('users').document(currentUser.uid).get();
      final double mediaQuery = MediaQuery.of(context).size.width / 400;
      final NumberFormat formater =
          NumberFormat.currency(locale: "pt_BR", symbol: "");
      changeWallet(true);

      ScaffoldFeatureController<SnackBar, SnackBarClosedReason> cont =
          _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: ComooColors.roxo,
        content: Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: mediaQuery * 210.0,
                constraints: BoxConstraints(
                  maxWidth: mediaQuery * 210.0,
                ),
                child: Text(
                  'Saldo atual: R\$ ${formater.format(snap['balance'] ?? 0.0)}',
                  overflow: TextOverflow.clip,
                  maxLines: 2,
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: mediaQuery * 13.0,
                  ),
                ),
              ),
              GestureDetector(
                child: Text(
                  'VER MAIS',
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: Color(0xFF00A1AD),
                    fontWeight: FontWeight.w500,
                    fontSize: mediaQuery * 13.0,
                  ),
                ),
                onTap: () {
                  _scaffoldKey.currentState.hideCurrentSnackBar(
                    reason: SnackBarClosedReason.dismiss,
                  );
                  Navigator.of(context).pushNamed('/my_extract');
                },
              ),
              GestureDetector(
                child: Icon(Icons.highlight_off),
                onTap: () => _scaffoldKey.currentState.hideCurrentSnackBar(
                  reason: SnackBarClosedReason.dismiss,
                ),
              ),
            ],
          ),
        ),
      ));

      cont.closed.whenComplete(() => changeWallet(false));
    }
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    final iPhone8Responsive responsive = iPhone8Responsive(context);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Theme.of(context).brightness));
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.5,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        centerTitle: true,
        titleSpacing: responsive.width(7),
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Image.asset(
                  'images/logo_fb.png',
                  width: responsive.width(147.5),
                  height: responsive.height(28.75),
                ),
              ),
              Spacer(),
              Container(
                width: responsive.width(44),
                height: responsive.width(44),
                decoration: BoxDecoration(
                  color: COMOO.colors.lightGray.withOpacity(0.25),
                  borderRadius: BorderRadius.circular(responsive.width(45)),
                ),
                padding: EdgeInsets.all(responsive.width(10)),
                child: SearchHero(
                  width: responsive.width(15),
                ),
              ),
              SizedBox(width: responsive.width(9)),
              StreamBuilder(
                initialData: false,
                stream: cameraButtonController.stream,
                builder: (context, btnSnapshot) {
                  return Container(
                    width: responsive.width(44),
                    height: responsive.width(44),
                    decoration: BoxDecoration(
                      color: COMOO.colors.lightGray.withOpacity(0.25),
                      borderRadius: BorderRadius.circular(responsive.width(45)),
                    ),
                    child: IconButton(
                      icon: Icon(
                        ComooIcons.camera,
                        size: responsive.width(25),
                      ),
                      iconSize: mediaQuery * 34.0,
                      padding: EdgeInsets.only(
                        right: responsive.width(10.0),
                      ),
                      color: COMOO.colors.lead,
                      onPressed: btnSnapshot.data == true
                          ? null
                          : () {
                              cameraButtonController.sink
                                  .add(_isCameraOpen = !_isCameraOpen);
                              onAddProductClick();
                            },
                    ),
                  );
                },
              ),
            ],
          ),
        ),
        // leading: Container(
        //   padding: EdgeInsets.only(left: mediaQuery * 5.0),
        //   child:
        //        Image.asset('images/logo_fb.png', width: mediaQuery * 88.0),
        // ),
        // title: Container(
        //   child:  IconButton(
        //     onPressed: () {
        //       // print('Search');
        //       // Navigator.of(context).push( MaterialPageRoute(
        //       //     builder: (context) =>  SearchNavigationPage()));
        //     },
        //     color: ComooColors.chumbo,
        //     icon:  Icon(Icons.search),
        //   ),
        // ),
        // actions: <Widget>[
        //    IconButton(
        //     icon:  Icon(ComooIcons.camera),
        //     iconSize: 34.0,
        //     padding: const EdgeInsets.only(right: 20.0),
        //     color: ComooColors.roxo,
        //     onPressed: onAddProductClick,
        //   ),
        // ],
      ),
      body: PageView(
        children: <Widget>[
          FeedPage(
            key: Key('Feed'),
            bottomModalCallback: onModalNavigationBarTap,
            scrollController: feedScrollController,
            refreshStream: _refreshFeed,
          ),
          Container(),
          //  Balance(key: Key('Balance')),
          Container(),
          NotificationMessages(key: Key('Notifications')),
          ProfilePage(
              key: Key('Profile'), onBalanceTap: _handleProfileBalanceTap),
        ],
        controller: pageController,
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: onPageChanged,
      ),
      bottomNavigationBar: StreamBuilder<bool>(
        initialData: false,
        stream: wallet,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          final double iconSize = mediaQuery * 43.0;
          return Stack(
            children: <Widget>[
              CupertinoTabBar(
                backgroundColor: Colors.white,
                inactiveColor: ComooColors.chumbo,
                border: snapshot.data
                    ? null
                    : Border(
                        top: BorderSide(
                          color: Color(0x4C000000),
                          width: 0.0, // One physical pixel.
                          style: BorderStyle.solid,
                        ),
                      ),
                iconSize: iconSize,
                items: [
                  BottomNavigationBarItem(
                      icon: Icon(
                        ComooIcons.home,
                        color: snapshot.data ? ComooColors.chumbo : null,
                      ),
                      title: Container()),
                  BottomNavigationBarItem(
                    icon: CustomPaint(
                      painter: snapshot.data ? SnackbarPainter() : null,
                      child: Icon(
                        ComooIcons.saldo,
                        color: snapshot.data
                            ? ComooColors.turqueza
                            : ComooColors.chumbo,
                      ),
                    ),
                    // icon:  Icon(ComooIcons.saldo),
                    title: Container(),
                  ),
                  BottomNavigationBarItem(
                      icon: Icon(
                        COMOO.icons.maisfilled.iconData,
                        color: COMOO.colors.turquoise,
                      ),
                      title: Container()),
                  BottomNavigationBarItem(
                      icon: Icon(
                        COMOO.icons.chat.iconData,
                        size: mediaQuery * 28.0,
                        // textDirection: TextDirection.rtl,
                        color: snapshot.data ? ComooColors.chumbo : null,
                      ),
                      title: Container()),
                  BottomNavigationBarItem(
                      icon: Icon(
                        ComooIcons.perfil,
                        color: snapshot.data ? ComooColors.chumbo : null,
                      ),
                      title: Container()),
                ],
                activeColor: ComooColors.turqueza,
                onTap: onNavigationBarTap,
                currentIndex: _currentIndex,
              ),
              Positioned(
                right: 2.2 * iconSize,
                top: mediaQuery * 10.0,
                child: _hasUnreadNotification
                    ? Container(
                        width: mediaQuery * 15.0,
                        height: mediaQuery * 15.0,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: ComooColors.pink),
                      )
                    : Container(),
              )
            ],
          );
        },
      ),
      floatingActionButton: _currentIndex == 0
          ? StreamBuilder<bool>(
              stream: api.inNegotiationAsClient().asStream(),
              builder: (BuildContext newContext, AsyncSnapshot<bool> snap) {
                // print('${snap.hasData} ${snap.data}');
                return snap.hasData && snap.data
                    ? FloatingActionButton(
                        backgroundColor: Color(0xFF00A1AD),
                        child: Image.asset(
                          'images/shopping_icon.png',
                          fit: BoxFit.cover,
                        ),
                        onPressed: () async {
                          final int page = pageController.page.toInt();
                          double offset = 0.0;
                          if (page == 0) {
                            offset = feedScrollController?.offset;
                          }
                          await Navigator.of(context)
                              .push(MaterialPageRoute<void>(
                            builder: (BuildContext context) =>
                                BlocProvider<BuyingsBloc>(
                              bloc: BuyingsBloc(),
                              child: BuyHistory(
                                sort: 'under negotiation',
                              ),
                            ),
                          ));
                          if (page == 0) {
                            if (feedScrollController.hasClients) {
                              feedScrollController.jumpTo(offset);
                            }
                          }
                        },
                      )
                    : Container();
              },
            )
          : null,
    );
  }

  Future<bool> showIncompleteStatusDialog() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return showCustomDialog<bool>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title:
            'Para começar a vender na COMOO você precisa acrescentar mais dados ao seu cadastro',
        actions: <Widget>[
          Container(
            height: mediaQuery * 41.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              child: Text(
                'DEPOIS',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Container(
            height: mediaQuery * 41.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: Text(
                'AGORA',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> showWaitingStatusDialog() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.all(0.0),
            content: Container(
              child: Container(
                padding: EdgeInsets.all(mediaQuery * 21.0),
                child: Text(
                  'Cadastro pendente. A gente te avisa assim que seu cadastro for aprovado.',
                  style: ComooStyles.welcomeMessage,
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  Future<void> generateInvite() async {
    currentUser = await comoo.currentUser();
    if (currentUser.sellerStatus == 'incomplete') {
      showIncompleteStatusDialog().then((result) {
        if (result != null) if (result) {
          _scaffoldKey.currentState.hideCurrentSnackBar(
            reason: SnackBarClosedReason.dismiss,
          );
          // Navigator.of(context).pushNamed('/user_profile');
          Navigator.of(context).pushNamed('/sign_up_personal');
        }
      });
    } else if (currentUser.sellerStatus == 'waiting') {
      showWaitingStatusDialog();
    } else if (currentUser.sellerStatus == 'accepted') {
      Navigator.of(context).pop();
      showCustomDialog<void>(
        context: context,
        barrierDismissible: false,
        alert: customRoundAlert(
          context: context,
          title: 'Gerando código de convite',
          textAlign: TextAlign.center,
          actions: <Widget>[
            Center(
              child: CircularProgressIndicator(),
            )
          ],
        ),
      );
      // final User user = await comoo.currentUser();
      // final String code = await comoo.api.fetchUserInviteCode(user.uid);

      // final Uri uri = await createInviteDynamicLink(code, user);
      // print(uri);
      // final String message =
      //     'Oi! Estou te indicando pra a COMOO, a mais nova rede social de compra '
      //     'e venda com lucro compartilhado. Crie ou participe de comoonidades, '
      //     'convide amigos e ganhe dinheiro todas as vezes que eles comprarem ou '
      //     'venderem. Use esse código: $code para o seu cadastro ser aprovado! '
      //     'Baixe agora: $uri';

      final String message = await api.fetchInviteMessage();
      Navigator.of(context).pop();
      if (message != null) {
        await Share.share(message);
      }
    }
  }

  void createProduct(bool openCamera) async {
    currentUser = await comoo.currentUser();
    if (currentUser.sellerStatus == 'incomplete') {
      showIncompleteStatusDialog().then((result) {
        if (result != null) if (result) {
          _scaffoldKey.currentState.hideCurrentSnackBar(
            reason: SnackBarClosedReason.dismiss,
          );
          // Navigator.of(context).pushNamed('/user_profile');
          Navigator.of(context).pushNamed('/sign_up_personal');
        }
      });
    } else if (currentUser.sellerStatus == 'waiting') {
      showWaitingStatusDialog();
    } else if (currentUser.sellerStatus == 'accepted') {
      Navigator.of(context)
          .push(MaterialPageRoute<bool>(
              builder: (BuildContext context) => ProductCreateView(
                    openCamera: openCamera,
                  )))
          .then((result) {
        if (result != null) {
          pageController.jumpToPage(0);
        }
        cameraButtonController.sink.add(false);
      });
    }
  }

  void onAddProductClick() {
    this.createProduct(true);
  }

  void onModalNavigationBarTap() {
    onNavigationBarTap(2);
  }

  Future<void> onNavigationBarTap(int index) async {
    if (index != 1) {
      changeWallet(false);
    }
    if (index == 0) {
      if (pageController.page == 0.0) {
        // feedScrollController.jumpTo(pageController.page);
        if (feedScrollController.offset != 0.0) {
          feedScrollController.animateTo(
            pageController.page,
            curve: Curves.easeOut,
            duration: Duration(seconds: 1),
          );
        }
      }
    }
    if (index == 3) {
      setState(() {
        _hasUnreadNotification = false;
      });
    }
    // if (index == 1) {
    //   var balance = await comoo.api.fetchUserBalance(currentUser.uid);
    //   _scaffoldKey.currentState.showSnackBar( SnackBar(
    //     content:  Text(
    //       'Saldo Atual: R\$ $balance',
    //       style: ComooStyles.botao_branco,
    //     ),
    //     backgroundColor: ComooColors.roxo,
    //     action:  SnackBarAction(
    //         label: 'Ver Mais',
    //         onPressed: () {
    //           pageController.jumpToPage(index);
    //         }),
    //   ));
    //   return;
    // }
    if (index == 2) {
      showModalBottomSheet<Null>(
        context: context,
        builder: (BuildContext context) => _MenuDrawer(
          onActionFinished: () {
            editRefreshFeed(true);
          },
          onCreateProduct: () {
            createProduct(false);
          },
          onGenerateInvite: () {
            generateInvite();
          },
        ),
      );
      return;
    }
    if (index == 1) {
      _handleProfileBalanceTap();
    } else {
      pageController.jumpToPage(index);
    }
  }

  void onPageChanged(int view) {
    setState(() {
      this._currentIndex = view;
    });
  }
}

class _MenuDrawer extends StatefulWidget {
  _MenuDrawer({
    this.onCreateProduct,
    this.onGenerateInvite,
    this.onActionFinished,
  });

  final VoidCallback onCreateProduct;
  final VoidCallback onGenerateInvite;
  final VoidCallback onActionFinished;

  @override
  State createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<_MenuDrawer> {
  // _MenuDrawerState({this.onCreateProduct});

  // final VoidCallback onCreateProduct;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Drawer(
      child: Container(
        height: mediaQuery * 240.0,
        color: ComooColors.roxo,
        child: _isLoading
            ? LoadingContainer()
            : Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: Icon(ComooIcons.anuncio,
                        size: mediaQuery * 48.0, color: ComooColors.turqueza),
                    title: const Text(
                      'Criar um anúncio',
                      style: ComooStyles.botao_branco,
                    ),
                    onTap: () {
                      widget.onCreateProduct();
                      widget.onActionFinished();
                    },
                  ),
                  ListTile(
                    leading: Padding(
                      padding: EdgeInsets.only(left: mediaQuery * 10.0),
                      child: Icon(
                        COMOO.icons.userWaves.iconData,
                        size: mediaQuery * 30.0,
                        color: ComooColors.turqueza,
                      ),
                    ),
                    title: const Text(
                      'Fazer um pedido nas suas comoonidades',
                      style: ComooStyles.botao_branco,
                    ),
                    onTap: () async {
                      // showDialog(
                      //     context: context,
                      //     barrierDismissible: true,
                      //     builder: (BuildContext context) {
                      //       return  Dialog(child:  UserPost());
                      //     });
                      await Navigator.of(context).push<void>(
                        MaterialPageRoute(
                          builder: (context) => RequestProductPage(),
                        ),
                      );
                      widget.onActionFinished();
                    },
                  ),
                  ListTile(
                    leading: Icon(COMOO.icons.createGroup.iconData,
                        size: mediaQuery * 44.0, color: ComooColors.turqueza),
                    title: const Text(
                      'Criar uma nova comoonidade',
                      style: ComooStyles.botao_branco,
                    ),
                    onTap: () async {
                      await Navigator.of(context).pushNamed('/add_group');
                      widget.onActionFinished();
                    },
                  ),
                  ListTile(
                    leading: Icon(
                      ComooIcons.convidar,
                      size: mediaQuery * 48.0,
                      color: ComooColors.turqueza,
                    ),
                    title: const Text(
                      'Convidar pessoas',
                      style: ComooStyles.botao_branco,
                    ),
                    onTap: () {
                      widget.onGenerateInvite();
                    },
                  ),
                ],
              ),
      ),
    );
  }
}

class SnackbarPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final double height = size.height;
    final double width = size.width;
    final Paint paint = Paint();

    final Path triangle = Path()
      ..addPolygon(<Offset>[
        Offset(width * 0, -height * 0.55 / 3),
        Offset(width / 2, height * 0.55 / 3),
        Offset(width * 1, -height * 0.55 / 3),
      ], true);
    paint.color = COMOO.colors.purple;
    canvas.drawPath(triangle, paint);
  }

  @override
  bool shouldRepaint(SnackbarPainter oldDelegate) {
    return oldDelegate != this;
  }
}
