// import 'dart:async';

// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:comoo/src/utility/network.dart';
// import 'package:comoo/src/utility/style.dart';
// import 'package:comoo/src/widgets/reusables.dart';
// import 'package:flutter/material.dart';
// import 'package:rxdart/rxdart.dart';
// import 'package:carousel_slider/carousel_slider.dart';
// import 'package:url_launcher/url_launcher.dart';

// class GeneralPostView extends StatefulWidget {
//   GeneralPostView(this.controller, {Key key}) : super(key: key);

//   final StreamController<bool> controller;

//   _GeneralPostViewState createState() => _GeneralPostViewState();
// }

// class _GeneralPostViewState extends State<GeneralPostView> with Reusables {
//   StreamSubscription<bool> _subscription;
//   final BehaviorSubject<bool> _refreshController = BehaviorSubject<bool>();
//   Stream<bool> get refresh => _refreshController.stream;
//   final BehaviorSubject<List<GeneralPost>> _posts =
//       BehaviorSubject<List<GeneralPost>>();
//   Stream<List<GeneralPost>> get posts => _posts.stream;
//   Function(List<GeneralPost>) get addPost => _posts.sink.add;
//   final NetworkApi api = NetworkApi();

//   @override
//   void initState() {
//     super.initState();
//     _fetchPosts();
//     // _subscription = widget.controller.stream
//     //     .asBroadcastStream()
//     //     .listen(_refreshController.sink.add);
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     _posts?.close();
//     _refreshController?.close();
//     _subscription?.cancel();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: EdgeInsets.only(bottom: responsive(context, 10.0)),
//       child: StreamBuilder<bool>(
//         stream: refresh,
//         builder: (BuildContext context, AsyncSnapshot<bool> reload) {
//           if (reload.hasData) {
//             if (reload.data ?? false) {
//               _fetchPosts();
//             }
//           }
//           return StreamBuilder<List<GeneralPost>>(
//             stream: posts,
//             builder: (BuildContext postContext,
//                 AsyncSnapshot<List<GeneralPost>> snapshot) {
//               // print('${snapshot.hasData} ${snapshot.data}');
//               return AnimatedContainer(
//                 curve: Curves.easeIn,
//                 duration: const Duration(milliseconds: 1000),
//                 child: snapshot.hasData
//                     ? buildSnapshotData(context, snapshot.data)
//                     : Container(),
//               );
//             },
//           );
//         },
//       ),
//     );
//   }

//   Widget buildSnapshotData(BuildContext context, List<GeneralPost> data) {
//     if (data.isEmpty) {
//       return Container();
//     }
//     final Size deviceSize = MediaQuery.of(context).size;
//     final Size postSize = Size(375, 200);
//     final double ratio = deviceSize.width / postSize.width;
//     return Container(
//       height: responsive(context, ratio * postSize.height),
//       child: Center(
//         child: data.length == 1
//             ? GeneralPostContainer(
//                 post: data[0],
//                 onClose: () {
//                   _closePost(data, data[0].id);
//                 },
//               )
//             : CarouselSlider(
//                 autoPlay: true,
//                 autoPlayAnimationDuration: const Duration(seconds: 2),
//                 autoPlayInterval: const Duration(seconds: 10),
//                 viewportFraction: ratio,
//                 pauseAutoPlayOnTouch: const Duration(seconds: 30),
//                 height: responsive(context, ratio * postSize.height),
//                 items: data.map<GeneralPostContainer>((GeneralPost post) {
//                   return GeneralPostContainer(
//                       post: post,
//                       onClose: () {
//                         _closePost(data, post.id);
//                       });
//                 }).toList(),
//               ),
//       ),
//     );
//   }

//   Future<void> _fetchPosts() async {
//     final dynamic result = await api.getPostGeneral();
//     if (result != null) {
//       final List<dynamic> list = (result as List);
//       if (list.isEmpty) {
//         if (mounted) {
//           addPost(<GeneralPost>[]);
//         }
//         return;
//       }
//       if (mounted) {
//         addPost(list
//             .map<GeneralPost>((dynamic json) => GeneralPost.fromCloud(json))
//             .toList());
//       }
//     } else {
//       if (mounted) {
//         addPost(<GeneralPost>[]);
//       }
//     }
//   }

//   Future<void> _closePost(List<GeneralPost> list, String uid) async {
//     print('fechar');
//     list.removeWhere((GeneralPost post) => post.id == uid);
//     addPost(list);
//     // api.closePost(uid);
//     // _fetchPosts();
//   }
// }

// class GeneralPost {
//   GeneralPost.fromCloud(dynamic json)
//       : id = json['id'],
//         imageURL = json['imageURL'] ?? '',
//         text = json['text'] ?? '',
//         link = json['link'];

//   String id;
//   String imageURL;
//   String text;
//   String link;
// }

// class GeneralPostContainer extends StatelessWidget with Reusables {
//   const GeneralPostContainer(
//       {Key key, @required this.post, @required this.onClose})
//       : super(key: key);
//   final GeneralPost post;
//   final Function onClose;

//   @override
//   Widget build(BuildContext context) {
//     return Builder(
//       builder: (BuildContext context) {
//         return Stack(
//           children: <Widget>[
//             GestureDetector(
//               onTap: post.link == null
//                   ? null
//                   : post.link.isEmpty
//                       ? null
//                       : () {
//                           launch(post.link);
//                         },
//               child: Container(
//                 width: MediaQuery.of(context).size.width,
//                 margin: const EdgeInsets.symmetric(horizontal: 5.0),
//                 decoration: BoxDecoration(
//                   color: post.imageURL.isEmpty ? null : Colors.white,
//                   gradient: post.imageURL.isEmpty
//                       ? LinearGradient(
//                           colors: [COMOO.colors.purple, COMOO.colors.pink])
//                       : null,
//                   image: post.imageURL.isEmpty
//                       ? null
//                       : DecorationImage(
//                           image: CachedNetworkImageProvider(post.imageURL),
//                         ),
//                 ),
//                 child: Container(
//                   padding: EdgeInsets.symmetric(
//                       horizontal: responsive(context, 15.0)),
//                   child: Center(
//                     child: Text(
//                       '${post.text}',
//                       overflow: TextOverflow.ellipsis,
//                       maxLines: 5,
//                       style: TextStyle(
//                         fontSize: responsive(context, 16.0),
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold,
//                         fontFamily: COMOO.fonts.montSerrat,
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: 0.0,
//               right: 0.0,
//               child: IconButton(
//                 icon: Icon(Icons.close),
//                 onPressed: onClose,
//               ),
//             ),
//           ],
//         );
//       },
//     );
//   }
// }
