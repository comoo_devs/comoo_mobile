import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/feed_bloc.dart';
import 'package:comoo/src/comoo/feed.dart';

import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/group/group_chat/group_chat.dart';
import 'request_permission.dart';
import '../search/search_navigation.dart';
import 'package:comoo/src/widgets/feed/product_feed.dart';
import 'package:comoo/src/widgets/network_image.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/widgets/recomendation/group_recommendation.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/recomendation/friends_recommendation.dart';

import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/post.dart';
import 'package:comoo/src/widgets/feed/post_feed.dart';
import 'package:flutter/widgets.dart';
import 'package:share/share.dart';

class FeedPage extends StatefulWidget {
  FeedPage(
      {Key key,
      this.bottomModalCallback,
      this.scrollController,
      this.refreshStream})
      : super(key: key);

  final Function bottomModalCallback;
  final ScrollController scrollController;
  final Stream<bool> refreshStream;

  @override
  State createState() {
    return _FeedPageState();
  }
}

class _FeedPageState extends State<FeedPage> with TickerProviderStateMixin {
  FeedBloc bloc;
  // TabController _tabController;
  @override
  void initState() {
    super.initState();

    // widget.scrollController.addListener(() {
    //   final ScrollController con = widget.scrollController;
    //   print('Offset: ${con.offset}');
    //   print('MaxScrollExtend: ${con.position.maxScrollExtent}');
    // });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // _tabController = TabController(vsync: this, length: 2, initialIndex: 0);
    bloc = FeedBloc(
      bottomModalCallback: widget.bottomModalCallback,
      scrollController: widget.scrollController,
      refreshStream: widget.refreshStream,
      // tabController: _tabController,
    );
  }

  @override
  void dispose() {
    // _tabController?.dispose();
    bloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _FeedPageStateless(bloc);
  }
}

class _FeedPageStateless extends StatelessWidget with Reusables {
  _FeedPageStateless(this.bloc);

  final FeedBloc bloc;

  // final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  //     GlobalKey<RefreshIndicatorState>();
  final Key _feedListViewKey = PageStorageKey('feedListViewKey');
  final Key _tabBarKey = PageStorageKey('feedTabBarKey');
  // final Key _tabStreamKey = PageStorageKey('feedTabStreamKey');
  final Key _feedPage1StreamKey = PageStorageKey('feedPage1StreamKey');

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      stream: bloc.isLoading,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        final bool isLoading = snapshot.data ?? false;
        // print('isLoading? $isLoading $bloc');
        return Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: isLoading
              ? Container(
                  alignment: FractionalOffset.center,
                  child: CircularProgressIndicator())
              : _newBuilder(context),
        );
      },
    );
    // print('Builded feed ${++counter} times');
    // if (!mounted) {
    //   return Container();
    // }
    // return StreamBuilder<bool>(
    //   stream: bloc.isLoading,
    //   builder: (BuildContext context, AsyncSnapshot<bool> isLoading) {
    //     return Container(
    //       height: MediaQuery.of(context).size.height,
    //       width: MediaQuery.of(context).size.width,
    //       child: (isLoading.data ?? false)
    //           ? Container(
    //               alignment: FractionalOffset.center,
    //               child: CircularProgressIndicator())
    //           : RefreshIndicator(
    //               key: _refreshIndicatorKey,
    //               onRefresh: bloc.handleRefresh,
    //               child: _buildFeedPage(context),
    //             ),
    //     );
    //   },
    // );
  }

  Widget _newBuilder(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: NestedScrollView(
        scrollDirection: Axis.vertical,
        controller: bloc.scrollController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              child: SliverList(
                delegate: SliverChildListDelegate(<Widget>[
                  StreamBuilder<List<Group>>(
                    stream: bloc.groupRecommendation,
                    builder: (BuildContext context,
                        AsyncSnapshot<List<Group>> snap) {
                      final bool hasGroups = snap.data?.isNotEmpty ?? false;
                      return hasGroups
                          ? GroupRecommendation(
                              'Comoonidades para você', snap.data)
                          : Container();
                    },
                  ),
                  StreamBuilder<List<User>>(
                    stream: bloc.userRecommendation,
                    builder:
                        (BuildContext context, AsyncSnapshot<List<User>> snap) {
                      final bool hasUsers = snap.data?.isNotEmpty ?? false;
                      return hasUsers
                          ? FriendsRecommendation(users: snap.data)
                          : Container();
                    },
                  ),
                  RequestPermissionCard(),
                  TabBar(
                    controller: bloc.tabController,
                    isScrollable: false,
                    indicator: UnderlineTabIndicator(
                      borderSide: BorderSide.none,
                    ),
                    tabs: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.view_day),
                          Text('visualizar feed'),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.list),
                          Text('visualizar lista'),
                        ],
                      ),
                    ],
                  ),
                  Divider(color: ComooColors.cinzaMedio),
                ]),
              ),
            ),
          ];
        },
        body: TabBarView(
          key: _tabBarKey,
          controller: bloc.tabController,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            _buildPageOne(context),
            _buildPageTwo(context),
          ],
        ),
      ),
    );
  }

  Widget _buildPageOne(BuildContext context) {
    return StreamBuilder<List<DocumentSnapshot>>(
      key: _feedPage1StreamKey,
      stream: bloc.feedSnapshots,
      builder: (BuildContext context,
          AsyncSnapshot<List<DocumentSnapshot>> snapshot) {
        final List<DocumentSnapshot> items =
            snapshot.data ?? <DocumentSnapshot>[];
        if (snapshot.hasData) {
          if (items.isEmpty) {
            return _buildEmptyFeed(context);
          } else {
            final int length = items.length;
            return ListView.builder(
              key: _feedListViewKey,
              // addAutomaticKeepAlives: true,
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: length,
              scrollDirection: Axis.vertical,
              // cacheExtent: 500,
              // controller: bloc.scrollController,
              itemBuilder: (BuildContext context, int index) {
                // print('building $index out of $length');
                final DocumentSnapshot snap = items[index];
                final String type = snap.data['type'];
                if (snap.data['product'] != null || snap.data['post'] != null) {
                  // final FeedItem item = FeedItem.fromSnapshot(snap);
                  // final FeedItemCard card = FeedItemCard(
                  //   item: item,
                  //   key: Key('${item.id}#feed'),
                  // );
                  // return card;
                  return Container();
                }
                // return StreamBuilder<DocumentSnapshot>(
                //   stream: (snap.data['ref'] as DocumentReference).snapshots(),
                //   builder: (BuildContext context,
                //       AsyncSnapshot<DocumentSnapshot> snapshot) {
                //     if (snapshot.hasData) {
                //       final DocumentSnapshot doc = snapshot.data;
                //       final FeedItem item = FeedItem(
                //         id: doc.documentID,
                //         type: FeedItem.typeFromString(snap.data['type']),
                //         ref: snap.data['ref'],
                //         snapshot: doc,
                //       );
                //       return FeedItemCard(
                //         item: item,
                //         key: Key('${item.id}#feed'),
                //       );
                //     }
                //     return Container();
                //   },
                // );
                switch (type) {
                  case 'product':
                    if (snap['status'] != 'deleted' ||
                        !(snap['sold'] ?? true)) {
                      final Product product = Product.fromSnapshot(snap);
                      return ProductFeed(
                        product,
                        key: Key(product.id),
                      );
                      // return ProductFeedCard(
                      //   product: product,
                      //   key: Key(product.id),
                      // );
                      // return ViniProductFeed(
                      //   product,
                      //   key: Key(product.id),
                      // );
                    }
                    break;
                  case 'post':
                    final Post post = Post.fromSnapshot(snap);
                    return PostFeed(post: post, key: Key(post.id));
                    break;
                }
                // final FeedItem item = items[index];
                // print(item);
                // final FeedItemCard card = FeedItemCard(
                //   item: item,
                //   key: Key('${item.id}#feed'),
                // );
                return Card(
                  child: SizedBox.shrink(
                      child: Center(
                    child: CircularProgressIndicator(),
                  )),
                );
                // return StreamBuilder<DocumentSnapshot>(
                //   stream: item.ref.snapshots(),
                //   builder: (BuildContext context,
                //       AsyncSnapshot<DocumentSnapshot> snapshot) {
                //     final Size size = MediaQuery.of(context).size;
                //     return Card(
                //       elevation: 5.0,
                //       child: Container(
                //         // width: size.width,
                //         // height: size.height,
                //         constraints: BoxConstraints.loose(
                //           Size(size.width, size.height),
                //         ),
                //         child: Center(
                //           child: snapshot.hasData
                //               ? Text(snapshot.data.documentID)
                //               : CircularProgressIndicator(),
                //         ),
                //       ),
                //     );
                //   },
                // );
                // return items[index];
                // return Container(
                //   child: Center(
                //     child: Text('$index'),
                //   ),
                // );
              },
            );
          }
        }
        return Container();
      },
    );
  }

  Widget _buildPageTwo(BuildContext context) {
    return StreamBuilder<bool>(
      stream: bloc.isLoadingFeedGroups,
      builder: (BuildContext context, AsyncSnapshot<bool> loading) {
        return StreamBuilder<List<FeedGroup>>(
          stream: bloc.feedGroups,
          builder:
              (BuildContext context, AsyncSnapshot<List<FeedGroup>> snapshot) {
            if (loading.data ?? false) {
              return Center(child: CircularProgressIndicator());
            }
            if (snapshot.hasData) {
              final List<FeedGroup> groups = snapshot.data ?? <FeedGroup>[];
              if (groups.isNotEmpty) {
                return ListView.separated(
                  shrinkWrap: true,
                  addAutomaticKeepAlives: true,
                  physics: ClampingScrollPhysics(),
                  // controller: bloc.scrollController,
                  itemCount: groups.length,
                  // semanticChildCount: 10,
                  // addAutomaticKeepAlives: true,
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider(color: COMOO.colors.lightGray);
                  },
                  itemBuilder: (BuildContext context, int index) {
                    // print('building $index out of ${groups.length} groups');
                    return _buildFeedGroup(context, groups[index]);
                  },
                );
              } else {
                return _buildEmptyFeedGroup(context);
              }
            }
            return Container();
          },
        );
      },
    );
  }

  // Widget _buildOne(BuildContext context) {
  //   return StreamBuilder<List<Widget>>(
  //     stream: bloc.feedWidgets,
  //     builder: (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
  //       final List<Widget> items = snapshot.data ?? <Widget>[];
  //       if (snapshot.hasData) {
  //         if (items.isEmpty) {
  //           return _buildEmptyFeed(context);
  //         } else {
  //           final int length = items.length;
  //           return Builder(
  //             builder: (BuildContext context) {
  //               return CustomScrollView(
  //                 // shrinkWrap: true,
  //                 // controller: bloc.scrollController,
  //                 cacheExtent: 1000,
  //                 // physics: AlwaysScrollableScrollPhysics(),
  //                 slivers: <Widget>[
  //                   SliverList(
  //                     delegate: SliverChildBuilderDelegate(
  //                       (BuildContext context, int index) {
  //                         return items[index];
  //                       },
  //                       childCount: length,
  //                     ),
  //                   )
  //                 ],
  //               );
  //             },
  //           );
  //           // return ListView.builder(
  //           //   addAutomaticKeepAlives: false,
  //           //   shrinkWrap: true,
  //           //   // physics: ClampingScrollPhysics(),
  //           //   itemCount: length,
  //           //   itemBuilder: (BuildContext context, int index) {
  //           //     return items[index];
  //           //     // return Container(
  //           //     //   child: Center(
  //           //     //     child: Text('$index'),
  //           //     //   ),
  //           //     // );
  //           //   },
  //           // );
  //         }
  //       }
  //       return Container();
  //     },
  //   );
  // }

  Widget _buildEmptyFeed(BuildContext context) {
    return Container(
      // height: responsive(context, 360.0),
      constraints: BoxConstraints.tightFor(
        width: MediaQuery.of(context).size.width,
        height: responsive(context, 360),
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/noFeedBackground.png'),
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.only(
        left: responsive(context, 37.0),
        right: responsive(context, 64.0),
        top: responsive(context, 74.0),
      ),
      alignment: FractionalOffset.center,
      child: LimitedBox(
        maxHeight: responsive(context, 360),
        child: ListView(
          shrinkWrap: true,
          controller: ScrollController(),
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            RichText(
                text: TextSpan(
                    style: ComooStyles.welcomeMessageTitle,
                    text: 'Bem-vindo!\r\n',
                    children: <TextSpan>[
                  TextSpan(
                      style: ComooStyles.welcomeMessage,
                      text: 'Entre para as '),
                  TextSpan(
                      style: ComooStyles.welcomeMessageBold,
                      text: 'comoonidades'),
                  TextSpan(
                      style: ComooStyles.welcomeMessage,
                      text:
                          ' que você mais curte e conecte-se com quem tem tudo a ver com você!')
                ])),
            SizedBox(height: responsive(context, 20.0)),
            Center(
              child: beginButton(
                context,
                width: responsive(context, 220.0),
                height: responsive(context, 35.0),
                radius: responsive(context, 50.0),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget beginButton(BuildContext context,
      {double width, double height, double radius}) {
    // final FeedBloc bloc = BlocProvider.of<FeedBloc>(context);
    return Container(
      width: width,
      height: height,
      child: RaisedButton(
        color: ComooColors.roxo,
        onPressed: bloc.bottomModalCallback,
        child: Text(
          'COMEÇAR',
          style: TextStyle(
            color: Colors.white,
            fontFamily: ComooStyles.fontFamily,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
        ),
      ),
    );
  }

  Widget _buildEmptyFeedGroup(BuildContext context) {
    // final FeedBloc bloc = BlocProvider.of<FeedBloc>(context);
    return Container(
      color: COMOO.colors.purple,
      alignment: Alignment.center,
      child: Center(
        child: ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            SizedBox(height: responsive(context, 5)),
            Icon(
              COMOO.icons.createGroup.iconData,
              color: Colors.white,
              size: responsive(context, 80),
            ),
            SizedBox(height: responsive(context, 5)),
            Text(
              'Você ainda nāo participa \n' 'de nenhuma comoonidade',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: COMOO.colors.pink,
                fontFamily: COMOO.fonts.montSerrat,
                fontSize: responsive(context, 18),
                fontWeight: FontWeight.bold,
                letterSpacing: responsive(context, -0.15),
              ),
            ),
            SizedBox(height: responsive(context, 20)),
            Text(
              'Econtre uma comoonidade e comece \n'
              'a comprar e vender entre amigos e \n'
              'pessoas com interesse em comum',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontFamily: COMOO.fonts.montSerrat,
                fontSize: responsive(context, 15),
                fontWeight: FontWeight.normal,
                letterSpacing: responsive(context, -0.125),
              ),
            ),
            SizedBox(height: responsive(context, 30)),
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: responsive(context, 65),
              ),
              width: responsive(context, 252),
              height: responsive(context, 35),
              child: RaisedButton(
                elevation: 0,
                color: COMOO.colors.turquoise,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute<void>(
                    builder: (BuildContext context) => SearchNavigationPage(),
                  ));
                },
                child: FittedBox(
                  child: Text(
                    'BUSCAR COMOONIDADES',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: ComooStyles.fontFamily,
                    ),
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(responsive(context, 50)),
                ),
              ),
            ),
            SizedBox(height: responsive(context, 17)),
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: responsive(context, 65),
              ),
              width: responsive(context, 252),
              height: responsive(context, 35),
              child: RaisedButton(
                elevation: 0,
                color: COMOO.colors.turquoise,
                onPressed: () async {
                  showCustomDialog<void>(
                    context: context,
                    barrierDismissible: false,
                    alert: customRoundAlert(
                      context: context,
                      title: 'Gerando código de convite',
                      textAlign: TextAlign.center,
                      actions: <Widget>[
                        Center(
                          child: CircularProgressIndicator(),
                        )
                      ],
                    ),
                  );

                  final String message = await bloc.fetchUserInviteMessage();
                  Navigator.of(context).pop();
                  if (message != null) {
                    await Share.share(message);
                  }
                },
                child: FittedBox(
                  child: Text(
                    'CONVIDAR PESSOAS',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: ComooStyles.fontFamily,
                    ),
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(responsive(context, 50)),
                ),
              ),
            ),
            SizedBox(height: responsive(context, 34)),
          ],
        ),
      ),
    );
  }

  Widget _buildFeedGroup(BuildContext context, FeedGroup item) {
    const Size padlockSize = Size(14, 20);
    final TextStyle titleStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: responsive(context, 14.0),
      color: COMOO.colors.turquoise,
      fontWeight: FontWeight.w600,
      letterSpacing: responsive(context, 0.257),
    );
    final TextStyle subtitleStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: responsive(context, 12.0),
      color: COMOO.colors.gray,
      fontWeight: FontWeight.w500,
    );
    final TextStyle descriptionStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: responsive(context, 14.0),
      color: COMOO.colors.lead,
      fontWeight: FontWeight.w500,
    );
    final double iconSize = 55.0;
    print('${item?.name} ${item?.avatar}');
    return ListTile(
      onTap: () {
        _navigateToGroup(context, item.id);
      },
      // leading: SizedBox(
      //   height: responsive(context, iconSize),
      //   width: responsive(context, iconSize),
      //   child: FittedBox(
      //     child: (item.avatar?.isEmpty ?? true)
      //         ? Container()
      //         : CachedNetworkImage(
      //             imageUrl: item.avatar,
      //             fit: BoxFit.cover,
      //             alignment: Alignment.center,
      //             // height: mediaQuery * size,
      //             // width: mediaQuery * size,
      //             imageBuilder: (BuildContext context,
      //                     ImageProvider<dynamic> imageProvider) =>
      //                 Center(
      //                   child: CircleAvatar(
      //                     backgroundImage: imageProvider,
      //                     radius: iconSize / 2,
      //                     backgroundColor: Colors.white,
      //                   ),
      //                 ),
      //             placeholder: (BuildContext context, String text) => Center(
      //                   child: Container(), //CircularProgressIndicator(),
      //                 ),
      //           ),
      //   ),
      // ),
      leading: (item.avatar?.isEmpty ?? true)
          ? null
          : CachedCircleAvatar(
              url: item.avatar,
              size: iconSize,
            ),
      title: Stack(
        children: <Widget>[
          Image.asset(
            item.askToJoin
                ? 'images/padlock_closed.png'
                : 'images/padlock_open.png',
            width: responsive(context, padlockSize.width),
            height: responsive(context, padlockSize.height),
          ),
          // SizedBox(width: responsive(context, 10.0)),
          Container(
            margin: EdgeInsets.only(
              left: responsive(context, padlockSize.width + 4),
            ),
            child: Text(
              '${item.name}',
              style: titleStyle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              // left: responsive(context, padlockSize.width + 4),
              top: responsive(context, padlockSize.height + 4),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              textDirection: TextDirection.ltr,
              children: <Widget>[
                Text(
                  item.subtitle,
                  style: subtitleStyle,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  '${item.description ?? ''}',
                  style: descriptionStyle,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                )
              ],
            ),
          ),
          Positioned(
            top: 0.0,
            right: 0.0,
            child: (item.quantity ?? 0) > 0
                ? Container(
                    margin: EdgeInsets.only(left: responsive(context, 5.0)),
                    height: responsive(context, 22.0),
                    constraints: BoxConstraints(
                      minWidth: 22.0,
                    ),
                    padding: EdgeInsets.symmetric(
                        horizontal: responsive(context, 5.0)),
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(responsive(context, 20)),
                      color: COMOO.colors.pink,
                    ),
                    child: Center(
                      child: FittedBox(
                          child: Text(
                        '${item.quantity}',
                        style: TextStyle(
                          fontFamily: COMOO.fonts.montSerrat,
                          color: Colors.white,
                          fontSize: responsive(context, 12.0),
                          fontWeight: FontWeight.w500,
                          letterSpacing: responsive(context, 0.52),
                        ),
                      )),
                    ),
                  )
                : Container(),
          ),
        ],
      ),
    );
  }

  Future<void> _navigateToGroup(BuildContext context, String id) async {
    // final FeedBloc bloc = BlocProvider.of<FeedBloc>(context);
    final Group group = await bloc.fetchGroup(id);
    if (group != null) {
      await Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => GroupChatPage(group),
      ));
      bloc.refreshGroups();
    }
  }
}
