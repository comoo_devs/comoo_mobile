import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/feed_item_bloc.dart';
import 'package:comoo/src/blocs/product_bloc.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/comments.dart';
import 'package:comoo/src/comoo/feed.dart';
import 'package:comoo/src/comoo/post.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/custom_cache_manager.dart';
import 'package:comoo/src/widgets/feed/post_feed.dart';
import 'package:comoo/src/widgets/post_user_info_row.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

class FeedItemCard extends StatefulWidget {
  FeedItemCard({this.item, Key key}) : super(key: key);
  final FeedItem item;
  @override
  State<StatefulWidget> createState() => _FeedItemState();
}

class _FeedItemState extends State<FeedItemCard> {
  FeedItemBloc bloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    bloc = FeedItemBloc(item: widget.item);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FeedItemBloc>(
      bloc: bloc,
      child: _FeedItemPage(),
    );
  }
}

class _FeedItemPage extends StatelessWidget with Reusables {
  _FeedItemPage();

  @override
  Widget build(BuildContext context) {
    final FeedItemBloc bloc = BlocProvider.of<FeedItemBloc>(context);
    // final Size size = MediaQuery.of(context).size;
    return StreamBuilder<DocumentSnapshot>(
      stream: bloc.item.ref.snapshots(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasData) {
          final DocumentSnapshot snap = snapshot.data;
          switch (bloc.item.type) {
            case FeedItemType.product:
              if (snap['status'] != 'deleted' || !(snap['sold'] ?? true)) {
                final Product product = Product.fromSnapshot(snap);
                // return ProductFeed(
                //   product,
                //   key: Key(product.id),
                // );
                return ProductFeedCard(
                  product: product,
                  key: Key(product.id),
                );
                // return ViniProductFeed(
                //   product,
                //   key: Key(product.id),
                // );
              }
              break;
            case FeedItemType.post:
              final Post post = Post.fromSnapshot(snap);
              return PostFeed(post: post, key: Key(post.id));
              break;
          }
        }
        return Container();
      },
    );
    // return PreferredSize(
    //   preferredSize: Size(size.width, size.width * 2.5),
    //   child: StreamBuilder<DocumentSnapshot>(
    //     key: Key('feed_document#${bloc.item.id}'),
    //     stream: bloc.document,
    //     builder:
    //         (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
    //       if (snapshot.hasData) {
    //         final DocumentSnapshot snap = snapshot.data;
    //         switch (bloc.item.type) {
    //           case FeedItemType.product:
    //             if (snap['status'] != 'deleted' || !(snap['sold'] ?? true)) {
    //               final Product product = Product.fromSnapshot(snap);
    //               // return ProductFeed(
    //               //   product,
    //               //   key: Key(product.id),
    //               // );
    //               return ProductFeedCard(
    //                 product: product,
    //                 key: Key(product.id),
    //               );
    //               // return ViniProductFeed(
    //               //   product,
    //               //   key: Key(product.id),
    //               // );
    //             }
    //             break;
    //           case FeedItemType.post:
    //             final Post post = Post.fromSnapshot(snap);
    //             return PostFeed(post: post, key: Key(post.id));
    //             break;
    //         }
    //         // print('${bloc.item.id} drawing container');
    //         // return Container();
    //       }
    //       return Card(
    //         child: Container(
    //           width: MediaQuery.of(context).size.width,
    //           height: MediaQuery.of(context).size.width,
    //           child: Center(child: CircularProgressIndicator()),
    //         ),
    //       );

    //       // return Container();
    //     },
    //   ),
    // );
  }
}

enum ProductMenu { update, delete, republish }

class ProductFeedCard extends StatefulWidget {
  ProductFeedCard({this.product, Key key}) : super(key: key);
  final Product product;
  @override
  State<StatefulWidget> createState() => __ProductFeedCardState();
}

class __ProductFeedCardState extends State<ProductFeedCard> {
  ProductBloc bloc;
  final double rand = Random().nextDouble();
  @override
  void initState() {
    super.initState();
    bloc = ProductBloc.seeded(widget.product);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // return _ProductCard(
    //   bloc,
    //   key: Key('feed#${widget.product.id}#productCard'),
    // );
    return BlocProvider<ProductBloc>(
      bloc: bloc,
      child: _ProductCard(key: Key('feed#${widget.product.id}#productCard')),
    );
  }
}

class _ProductCard extends StatelessWidget with Reusables {
  _ProductCard({Key key}) : super(key: key);

  // final ProductBloc bloc;

  final NumberFormat formatter =
      NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');

  @override
  Widget build(BuildContext context) {
    final ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
    final Size size = MediaQuery.of(context).size;
    final Product product = bloc.initialProduct;
    // print('building ${product.title} ${++count} times');
    final double productHeight = product?.height ?? size.width;
    final double productWidth = bloc?.initialProduct?.height ?? size.width;
    final double ratio = size.width / productWidth;
    final double height = 70 + 100.0 + (ratio * productHeight);
    print(
        'preferred: ${size.width}x${size.width * 2.5}  -  actual: ${size.width}x$height');
    return Card(
      margin: EdgeInsets.only(bottom: 10.0),
      elevation: 5.0,
      // tag: product.id,
      child: AnimatedContainer(
        curve: Curves.easeIn,
        duration: const Duration(milliseconds: 200),
        child: _buildProductInfo(context, product, ratio),
        // child: Center(
        //   child: _buildPhoto(context, product),
        // ),
      ),
    );
  }

  Widget _buildProductInfo(
      BuildContext context, Product product, double ratio) {
    final ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
    print('${DateTime.now()} rebuilded ${product.title}');
    return ListView(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      // mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: responsive(context, 70),
          ),
          child: SizedBox.expand(
            child: _sellerInfo(context, product),
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            left: responsive(context, 15.0),
            top: responsive(context, 10.0),
            bottom: responsive(context, 12.0),
            right: responsive(context, 5.0),
          ),
          child: RichText(
            overflow: TextOverflow.ellipsis,
            text: TextSpan(
                style: ComooStyles.produtoNome,
                text: '${product.title} ',
                children: [
                  TextSpan(
                      style: ComooStyles.produtoDesc, text: product.description)
                ]),
            maxLines: 3,
          ),
        ),
        Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: product.height * ratio,
          ),
          // color: Colors.redAccent,
          child: _buildPhoto(context, product),
        ),
        // _buildPhoto(context, product),

        Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: responsive(context, 35),
          ),
          child: SizedBox.expand(
            child: _productInfo(context, product),
          ),
        ),
        _productActions(context, bloc, product),
        // AnimatedContainer(
        //   key: Key('${product.id}_last_comment_animated_container'),
        //   curve: Curves.easeIn,
        //   duration: const Duration(milliseconds: 200),
        //   child:
        StreamBuilder<Comment>(
          key: Key('${product.id}_last_comment'),
          stream: bloc.lastComment,
          builder: (BuildContext context, AsyncSnapshot<Comment> snapshot) {
            if (snapshot.hasData) {
              final Comment comment = snapshot.data;
              return ListTile(
                // key: GlobalObjectKey(index),
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(comment.user.photoURL),
                ),
                title: RichText(
                    text: TextSpan(
                        text: '${comment.user.name} ',
                        style: ComooStyles.texto_bold,
                        children: [
                      TextSpan(
                        style: ComooStyles.texto,
                        text: timeago.format(comment.date, locale: 'pt_BR'),
                      )
                    ])),
                subtitle: Text(comment.text),
              );
            }
            return Container();
          },
        ),
        // ),
      ],
    );
  }

  Widget _sellerInfo(BuildContext context, Product _product) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final TextStyle menuItemStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: mediaQuery * 15.0,
      letterSpacing: mediaQuery * 0.21,
    );
    final bool showMenu =
        ((_product?.user['uid'] == currentUser?.uid) ?? false) &&
            !(_product?.underNegotiation ?? false);
    return PostUserInfoRow(
      userPhotoURL: _product?.user['picture'] ?? '',
      userName: _product?.user['name'] ?? '',
      location: _product?.location ?? '',
      dateString: timeago.format(_product.date, locale: 'pt_BR'),
      groups: _product.groups,
      onGroupTap: (String id) {
        // if (callback == null) {
        //   _navigateToGroupDetail(context, id);
        // } else {
        //   callback();
        // }
      },
      // onImageTap: callback ??
      //     () {
      //       _navigateToSellerDetail(context, _product);
      //     },
      // onNameTap: callback ??
      //     () {
      //       _navigateToSellerDetail(context, _product);
      //     },
      menu: showMenu
          ? PopupMenuButton<ProductMenu>(
              onSelected: (ProductMenu result) {
                switch (result) {
                  case ProductMenu.update:
                    // _updateProduct(context, _product);
                    break;
                  case ProductMenu.republish:
                    // _republishProduct(context, _product);
                    break;
                  case ProductMenu.delete:
                  // _deleteProduct(context, _product);
                }
              },
              itemBuilder: (BuildContext context) =>
                  <PopupMenuEntry<ProductMenu>>[
                PopupMenuItem<ProductMenu>(
                  value: ProductMenu.update,
                  child: Text(
                    'EDITAR ANÚNCIO',
                    style: menuItemStyle,
                  ),
                ),
                PopupMenuItem<ProductMenu>(
                  value: ProductMenu.republish,
                  child: Text(
                    'REPOSTAR ANÚNCIO',
                    style: menuItemStyle,
                  ),
                ),
                PopupMenuItem<ProductMenu>(
                  value: ProductMenu.delete,
                  child: Text(
                    'EXCLUIR ANÚNCIO',
                    style: menuItemStyle,
                  ),
                ),
              ],
            )
          : Container(),
    );
  }

  Widget _buildPhoto(context, product) {
    final ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
    return Stack(
      children: <Widget>[
        PageView.builder(
          controller: bloc.photoController,
          itemCount: product.photos.length,
          scrollDirection: Axis.horizontal,
          // physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            final String url = product?.photos[index] ?? '';
            if (url.isEmpty) {
              return Container();
            }
            return Container(
              child: CachedNetworkImage(
                cacheManager: CustomCacheManager(),
                imageUrl: product.photos[index],
                fit: BoxFit.cover,
                placeholder: (BuildContext context, String url) => Center(
                  child: CircularProgressIndicator(),
                ),
                errorWidget: (BuildContext context, String url, Object error) =>
                    Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            );
          },
        ),
        Positioned(
          right: responsive(context, 20.0),
          top: responsive(context, 20.0),
          child: Container(
            width: responsive(context, 120.0),
            height: responsive(context, 30.0),
            alignment: FractionalOffset.center,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(responsive(context, 14.0))),
            child: FittedBox(
              child: Text(
                formatter.format(product.price),
                style: ComooStyles.productPrice,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _productInfo(BuildContext context, Product _product) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      height: mediaQuery * 28.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                ComooIcons.marca,
                size: mediaQuery * 28.0,
              ),
              SizedBox(
                width: mediaQuery * 10.0,
              ),
              Text(
                _product.brand,
                overflow: TextOverflow.ellipsis,
                style: ComooStyles.product_tags,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                ComooIcons.tamanho,
                size: mediaQuery * 28.0,
              ),
              SizedBox(
                width: mediaQuery * 10.0,
              ),
              Text(
                _product.size,
                overflow: TextOverflow.ellipsis,
                style: ComooStyles.product_tags,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                ComooIcons.estado_produto,
                size: mediaQuery * 28.0,
              ),
              SizedBox(
                width: mediaQuery * 10.0,
              ),
              Text(_product.condition, style: ComooStyles.product_tags),
            ],
          ),
        ],
      ),
    );
  }

  Widget _productActions(
      BuildContext context, ProductBloc bloc, Product product) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      // height: mediaQuery * 58.0,
      child: IconTheme(
        data: ComooIcons.roxo,
        child: StreamBuilder<bool>(
          stream: bloc.liked,
          builder: (BuildContext likeContext, AsyncSnapshot<bool> likeSnap) {
            // print('likes: ${product.likes}');
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                StreamBuilder<bool>(
                  stream: bloc.isLoadingLike,
                  initialData: false,
                  builder:
                      (BuildContext context, AsyncSnapshot<bool> loadingLike) {
                    return IconButton(
                      icon: Icon(
                        likeSnap.data ?? false
                            ? Icons.favorite
                            : ComooIcons.curtir,
                        size: 28.0,
                      ),
                      alignment: Alignment.centerRight,
                      onPressed: () {},
                      // onPressed: loadingLike.data
                      //     ? null
                      //     : callback ??
                      //         () {
                      //           bloc.onLike(product);
                      //         },
                    );
                  },
                ),
                Text(
                  '${product.likes}',
                  style: ComooStyles.texto_roxo,
                ),
                IconButton(
                  icon: Icon(
                    ComooIcons.comentar,
                    size: mediaQuery * 28.0,
                  ),
                  alignment: Alignment.centerRight,
                  onPressed: () {},
                  // onPressed: callback ??
                  //     () {
                  //       _navigateToComments(context, product);
                  //     },
                ),
                Text(product.commentsQty.toString(),
                    style: ComooStyles.produto_social),
                Expanded(
                  child: Container(),
                ),
                Container(
                  height: mediaQuery * 48.0,
                  //  width: mediaQuery * 76.0,
                  padding: EdgeInsets.only(
                    left: mediaQuery * 10.0,
                    right: mediaQuery * 10.0,
                    bottom: mediaQuery * 10.0,
                    top: mediaQuery * 8.0,
                  ),
                  child: StreamBuilder<bool>(
                    initialData: false,
                    stream: bloc.loadingRequest,
                    builder: (BuildContext context,
                        AsyncSnapshot<bool> loadingSnap) {
                      return StreamBuilder<bool>(
                        initialData: true,
                        stream: bloc.userHasRequested,
                        builder: (BuildContext context,
                            AsyncSnapshot<bool> requestedSnap) {
                          return RaisedButton(
                            elevation: 0.0,
                            color: ComooColors.pink,
                            child: Text('COMPRAR', style: ComooStyles.quero),
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(mediaQuery * 20.0),
                            ),
                            onPressed: () {},
                            // onPressed: loadingSnap.data
                            //     ? null
                            //     : requestedSnap.data
                            //         ? null
                            //         : callback ??
                            //             () async {
                            //               final Map<String, dynamic> result =
                            //                   await bloc
                            //                       .requestProduct(product);
                            //               print(result);
                            //               if (result == null) {
                            //                 showCustomDialog<void>(
                            //                   context: context,
                            //                   alert: customRoundAlert(
                            //                     context: context,
                            //                     title:
                            //                         'Um erro inesperado aconteceu, tente novamente mais tarde.',
                            //                   ),
                            //                 );
                            //               } else {
                            //                 final bool isFirst =
                            //                     result['isFirst'] ?? false;
                            //                 if (isFirst) {
                            //                   //
                            //                   final Negotiation negotiation =
                            //                       result['negotiation']
                            //                           as Negotiation;

                            //                   await Navigator.of(context).push(
                            //                     MaterialPageRoute(
                            //                         builder: (context) =>
                            //                             DirectChatView(
                            //                                 negotiation)
                            //                         // NegotiationPage(negotiation, goToChat: true),
                            //                         ),
                            //                   );
                            //                   bloc.reloadProduct(product);
                            //                 } else {
                            //                   final int count = result['count'];
                            //                   _showNotFirstDialog(
                            //                       context, count);
                            //                 }
                            //               }
                            //             },
                          );
                        },
                      );
                    },
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
