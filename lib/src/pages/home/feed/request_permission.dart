import 'package:comoo/src/blocs/request_perimission_bloc.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:notification_permissions/notification_permissions.dart';

class RequestPermissionCard extends StatefulWidget {
  RequestPermissionCard();
  @override
  State<StatefulWidget> createState() => _RequestPermissionCardState();
}

class _RequestPermissionCardState extends State<RequestPermissionCard> {
  RequestPermissionBloc bloc;
  @override
  void initState() {
    super.initState();
    bloc = RequestPermissionBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RequestPermissionBloc>(
        bloc: bloc, child: RequestPermissionCardStateless());
  }
}

class RequestPermissionCardStateless extends StatelessWidget with Reusables {
  @override
  Widget build(BuildContext context) {
    final RequestPermissionBloc bloc =
        BlocProvider.of<RequestPermissionBloc>(context);
    return FutureBuilder<PermissionStatus>(
      future: NotificationPermissions.getNotificationPermissionStatus(),
      builder: (BuildContext context, AsyncSnapshot<PermissionStatus> notPerm) {
        bloc.init();
        return StreamBuilder<bool>(
            stream: bloc.hasNotificationPermission,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              return snapshot.hasData
                  ? snapshot.data
                      ? Container()
                      : Card(
                          color: COMOO.colors.purple,
                          child: Stack(
                            children: <Widget>[
                              Container(
                                // color: COMOO.colors.purple,
                                padding: EdgeInsets.only(
                                  left: responsive(context, 18.0),
                                  top: responsive(context, 18.0),
                                ),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Stack(
                                          children: <Widget>[
                                            Transform.rotate(
                                              angle: -math.pi / 6,
                                              child: Icon(
                                                COMOO.icons.notificacoes
                                                    .iconData,
                                                color: COMOO.colors.turquoise,
                                                size: responsive(context, 65.0),
                                              ),
                                            ),
                                            Positioned(
                                              right: responsive(context, 15.0),
                                              top: responsive(context, 10.0),
                                              child: Container(
                                                constraints:
                                                    BoxConstraints.tight(Size(
                                                  responsive(context, 15.0),
                                                  responsive(context, 15.0),
                                                )),
                                                decoration: BoxDecoration(
                                                  color: COMOO.colors.pink,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          50.0),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            width: responsive(context, 15.0)),
                                        Text(
                                          'Ative suas\n'
                                          'notificaçoões',
                                          style: TextStyle(
                                            color: COMOO.colors.pink,
                                            fontFamily: COMOO.fonts.montSerrat,
                                            fontSize: responsive(context, 25.0),
                                            fontWeight: FontWeight.bold,
                                            letterSpacing:
                                                responsive(context, -0.05),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: responsive(context, 15)),
                                    RichText(
                                      text: TextSpan(
                                          text:
                                              'A gente promete nāo te chatear!',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: COMOO.fonts.montSerrat,
                                            fontWeight: FontWeight.bold,
                                            fontSize: responsive(context, 15.0),
                                            letterSpacing:
                                                responsive(context, -0.125),
                                          ),
                                          children: <TextSpan>[
                                            TextSpan(text: '\n\n'),
                                            TextSpan(
                                              text:
                                                  'As notificações sāo fundamentais para que\n'
                                                  'sua experiência na COMOO seja incrível: você\n'
                                                  'será avisado sobre suas compras, vendas ou\n'
                                                  'quando alguém quiser falar com você.',
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontFamily:
                                                    COMOO.fonts.montSerrat,
                                                fontWeight: FontWeight.normal,
                                                fontSize:
                                                    responsive(context, 15.0),
                                                letterSpacing:
                                                    responsive(context, -0.125),
                                              ),
                                            ),
                                          ]),
                                    ),
                                    SizedBox(height: responsive(context, 20)),
                                    Center(
                                      child: RaisedButton(
                                        elevation: 0.0,
                                        onPressed: bloc.requestPermission,
                                        color: COMOO.colors.turquoise,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0)),
                                        padding: EdgeInsets.symmetric(
                                            horizontal:
                                                responsive(context, 32.0)),
                                        child: Text(
                                          'ATIVAR NOTIFICAÇÕES',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: COMOO.fonts.montSerrat,
                                            fontWeight: FontWeight.w500,
                                            fontSize: responsive(context, 15.0),
                                            letterSpacing:
                                                responsive(context, 0.608),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: responsive(context, 20)),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 0,
                                right: 0,
                                child: IconButton(
                                  color: Colors.white,
                                  icon: Icon(Icons.close),
                                  onPressed: () {
                                    bloc.dismiss();
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                  : Container();
            });
      },
    );
  }
}
