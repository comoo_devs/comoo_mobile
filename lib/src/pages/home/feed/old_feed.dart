// import 'dart:async';

// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:comoo/src/comoo/authentication.dart';

// import 'package:comoo/src/comoo/product.dart';
// import 'package:comoo/src/comoo/user.dart';
// import 'general_post.dart';
// import 'request_permission.dart';
// import 'package:comoo/src/widgets/feed/product_feed.dart';
// import 'package:flutter/material.dart';
// import 'package:comoo/src/widgets/recomendation/group_recommendation.dart';
// import 'package:comoo/src/utility/style.dart';
// import 'package:comoo/src/widgets/recomendation/friends_recommendation.dart';

// //import 'package:comoo/pages/user_detail.dart';
// //import 'package:shared_preferences/shared_preferences.dart';
// //import 'dart:convert';
// //enum IndicatorType { overscroll, refresh }
// import 'package:cloud_functions/cloud_functions.dart';
// import 'package:comoo/src/comoo/group.dart';
// import 'package:comoo/src/comoo/post.dart';
// import 'package:comoo/src/widgets/feed/post_feed.dart';

// class OldFeedPage extends StatefulWidget {
//   OldFeedPage(
//       {Key key,
//       this.bottomModalCallback,
//       this.scrollController,
//       this.refreshStream})
//       : super(key: key);

//   final Function bottomModalCallback;
//   final ScrollController scrollController;
//   final Stream<bool> refreshStream;

//   @override
//   State createState() {
//     return _FeedPageState();
//   }
// }

// class _FeedPageState extends State<OldFeedPage> {
//   StreamSubscription refreshSubscriptions;
//   final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
//       GlobalKey<RefreshIndicatorState>();

//   final Firestore _db = Firestore.instance;
//   StreamController<bool> feedController = StreamController<bool>();

//   List _feedList = [];
//   bool _isLoading = false;
//   List<Group> _groups = <Group>[];
//   List<User> _users = <User>[];

//   Future<void> _handleRefresh() async {
//     feedController.sink.add(true);
//     await _fetchFeed();
//   }

//   Future<ProductFeed> _fetchProductFeedItem(String id) async {
//     var productReference = _db.collection('products').document(id);
//     var productDoc = await productReference.get();
//     if (!productDoc.exists ||
//             productDoc['status'] == 'deleted' ||
//             productDoc['sold'] ??
//         false) {
//       return null;
//     }

//     final Product product = new Product.fromSnapshot(productDoc);
//     // if (productDoc['sold'] != null) {
//     //   // print('${product.id} ${product.title} ${productDoc['sold']}');
//     //   if (productDoc['sold']) {
//     //     return null;
//     //   }
//     // }
//     return ProductFeed(
//       product,
//       key: Key(product.id),
//     );
//   }

//   Future _buildFeedItem(id) async {
//     final DocumentReference postReference =
//         _db.collection('posts').document(id);
//     final DocumentSnapshot postDoc = await postReference.get();
//     if (!postDoc.exists) {
//       return null;
//     }
//     final Post post = Post.fromSnapshot(postDoc);
//     return PostFeed(post: post, key: Key(post.id));
//   }

//   buildFeed() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     List children = List();
//     if (_groups.isNotEmpty)
//       children.add(GroupRecommendation('Comoonidades para você', _groups));
//     // if (_users.isNotEmpty)
//     children.add(FriendsRecommendation(users: _users));
//     children.add(RequestPermissionCard());
//     // children.add(GeneralPostView(feedController));
//     if (_feedList != null && _feedList.isNotEmpty) {
//       children.addAll(_feedList);
//     } else {
//       children.add(Container(
//           height: mediaQuery * 360.0,
//           decoration: BoxDecoration(
//             image: DecorationImage(
//               image: AssetImage('images/noFeedBackground.png'),
//               fit: BoxFit.cover,
//             ),
//           ),
//           padding: EdgeInsets.only(
//               left: mediaQuery * 37.0,
//               right: mediaQuery * 64.0,
//               top: mediaQuery * 74.0),
//           alignment: FractionalOffset.center,
//           child: Column(children: <Widget>[
//             RichText(
//                 text: TextSpan(
//                     style: ComooStyles.welcomeMessageTitle,
//                     text: 'Bem-vindo!\r\n',
//                     children: <TextSpan>[
//                   TextSpan(
//                       style: ComooStyles.welcomeMessage,
//                       text: 'Entre para as '),
//                   TextSpan(
//                       style: ComooStyles.welcomeMessageBold,
//                       text: 'comoonidades'),
//                   TextSpan(
//                       style: ComooStyles.welcomeMessage,
//                       text:
//                           ' que você mais curte e conecte-se com quem tem tudo a ver com você!')
//                 ])),
//             SizedBox(height: mediaQuery * 20.0),
//             Center(
//               child: beginButton(
//                 width: mediaQuery * 220.0,
//                 height: mediaQuery * 35.0,
//                 radius: mediaQuery * 50.0,
//               ),
//             ),
//           ])));
//     }
//     children.removeWhere((item) => item == null);
//     return Container(
//       height: MediaQuery.of(context).size.height,
//       child: ListView.builder(
//         addAutomaticKeepAlives: true,
//         itemCount: children.length,
//         physics: ClampingScrollPhysics(),
//         controller: widget.scrollController,
//         itemBuilder: (_, int index) {
//           print('builded $index out of ${children.length} items');
//           return children[index];
//         },
//       ),
//     );
//     // return Column(
//     //   mainAxisAlignment: MainAxisAlignment.start,
//     //   crossAxisAlignment: CrossAxisAlignment.start,
//     //   children: children.cast<Widget>(),
//     // );
//   }

//   Future<void> _fetchRecommendations() async {
//     _groups = await currentUser.fetchGroupRecommendation();
//     _users = await currentUser.fetchFriendRecommendation();
//     if (this.mounted) {
//       setState(() {});
//     }
//   }

//   _fetchFeed() async {
//     if (currentUser == null) {
//       await UserAuth().fetchCurrentUser();
//     }
//     _fetchRecommendations();

//     final dynamic result = await CloudFunctions.instance
//         .call(functionName: 'fetchFeed')
//         .timeout(Duration(seconds: 30))
//         .catchError((error) {
//       print(error);
//       _isLoading = false;
//     });
//     if (result == null) {
//       return;
//     }
//     List feeds = result;
//     print('>>> -> ${feeds.length}');
//     List<Future> feedList = List();
//     feeds.forEach((feed) {
//       if (feed['type'] == 'product') {
//         var product = _fetchProductFeedItem(feed['id']);
//         // print(product);
//         if (product != null) feedList.add(product);
//       } else {
//         // print(feed);
//         feedList.add(_buildFeedItem(feed['id']));
//       }
//     });
//     Future.wait(feedList).then((pList) {
//       if (this.mounted) {
//         setState(() {
//           _isLoading = false;
//           _feedList = pList;
//         });
//       }
//     });
//   }

//   @override
//   void initState() {
//     refreshSubscriptions =
//         widget.refreshStream.listen((bool) => _handleRefresh());
//     super.initState();
//     _isLoading = true;

//     _fetchFeed();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     feedController?.close();
//     refreshSubscriptions?.cancel();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return _isLoading
//         ? Container(
//             alignment: FractionalOffset.center,
//             child: CircularProgressIndicator())
//         : RefreshIndicator(
//             key: _refreshIndicatorKey,
//             onRefresh: _handleRefresh,
//             child: buildFeed(),
//           );
//   }

//   Widget beginButton({double width, double height, double radius}) {
//     return Container(
//       width: width,
//       height: height,
//       child: RaisedButton(
//           color: ComooColors.roxo,
//           onPressed: widget.bottomModalCallback,
//           child: Text(
//             'COMEÇAR',
//             style: TextStyle(
//                 color: Colors.white, fontFamily: ComooStyles.fontFamily),
//           ),
//           shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(radius))),
//     );
//   }
// }
