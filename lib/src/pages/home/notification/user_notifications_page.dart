import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/comoo/post.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/group/group_chat/group_chat.dart';
import 'package:comoo/src/pages/common/group/group_detail.dart';
import 'package:comoo/src/pages/common/chat/direct_chat.dart';
import 'package:comoo/src/pages/common/product/product_page.dart';
import 'package:comoo/src/pages/common/transaction/negotiation_page.dart';
import 'package:comoo/src/pages/common/transaction/transaction_view.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:comoo/src/pages/common/withdraw_page.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/widgets/comments/post_comments.dart';
import 'package:comoo/src/widgets/comments/product_comments.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/network_image.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'package:comoo/src/comoo/user_notification.dart';

class UserNotificationPage extends StatefulWidget {
  UserNotificationPage({@required this.updateCounters});

  final Function() updateCounters;

  @override
  State createState() {
    return new UserNotificationPageState();
  }
}

class UserNotificationPageState extends State<UserNotificationPage>
    with Reusables {
  final Firestore _db = Firestore.instance;
  final NetworkApi api = NetworkApi();

  bool _isLoading = false;

  Future<void> _acceptInvite(UserNotification notification) async {
    notification.approved = true;
    final DocumentSnapshot group = await _db
        .collection('groups')
        .document(notification.ref ?? notification.group)
        .get();

    final WriteBatch batch = _db.batch();

    final DocumentReference notificationRef = _db
        .collection('users')
        .document(currentUser.uid)
        .collection('notifications')
        .document(notification.id);

    batch.updateData(notificationRef, <String, bool>{'approved': true});

    var groupUsersRef =
        group.reference.collection('users').document(currentUser.uid);

    batch.setData(groupUsersRef, <String, dynamic>{
      'id': currentUser.uid,
      'name': currentUser.name,
      'admin': false
    });

    final DocumentReference userRef = _db
        .collection('users')
        .document(currentUser.uid)
        .collection('groups')
        .document(notification.ref ?? notification.group);

    batch.setData(userRef, <String, dynamic>{
      'admin': false,
      'avatar': group['avatar'],
      'id': group.documentID,
      'name': group['name']
    });

    batch.commit().then((_) {
      setState(() {});
      // Scaffold.of(context).showSnackBar(new SnackBar(
      //   content: new Text(
      //       '${notification.name} foi adicionado ao grupo ${group['name']}',
      //       style: ComooStyles.textoSnackBar),
      //   duration: Duration(seconds: 2),
      // ));
    });
  }

  Future<void> _approveRequest(UserNotification notification) async {
    notification.approved = true;
    final DocumentSnapshot group = await _db
        .collection('groups')
        .document(notification.ref ?? notification.group)
        .get();

    final WriteBatch batch = _db.batch();

    final DocumentReference notificationRef = _db
        .collection('users')
        .document(currentUser.uid)
        .collection('notifications')
        .document(notification.id);

    batch.updateData(notificationRef, <String, bool>{'approved': true});

    final DocumentReference groupUsersRef =
        group.reference.collection('users').document(notification.from);

    batch.setData(groupUsersRef, <String, dynamic>{
      'id': notification.from,
      'name': notification.title ?? notification.name,
      'admin': false
    });

    final DocumentReference userRef = _db
        .collection('users')
        .document(notification.from)
        .collection('groups')
        .document(notification.ref ?? notification.group);

    batch.setData(userRef, <String, dynamic>{
      'admin': false,
      'avatar': group['avatar'],
      'id': group.documentID,
      'name': group['name']
    });

    // final userRequestNotificationRef = _db
    //     .collection('users')
    //     .document(notification.from)
    //     .collection('notifications')
    //     .document();

    final DocumentReference userReference =
        _db.collection('users').document(notification.from);
    final DocumentSnapshot userSnapshot = await userReference.get();
    List<String> groups = List();
    final list = userSnapshot['group_requests'];
    (list as List).forEach((g) {
      if (g != group.documentID) groups.add(g);
    });
    print(group.documentID);
    userReference.updateData(<String, dynamic>{
      'group_requests': groups,
    });

    print(<String, String>{
      'ref': group.documentID,
      'type': 'approved_solicitation',
      'userId': notification.from,
    });

    CloudFunctions.instance
        .getHttpsCallable(functionName: 'generateUserNotifications')
        .call(<String, String>{
      'ref': group.documentID,
      'type': 'approved_solicitation',
      'userId': notification.from,
    }).then((HttpsCallableResult result) {
      print('foi generateUserNotifications!');
      print(result.data);
    }).catchError((dynamic onError) {
      print('>> generateUserNotifications Error\n$onError');
    });
    // batch.setData(userRequestNotificationRef, {
    //   'id': userRequestNotificationRef.documentID,
    //   'thumb': group['avatar'],
    //   'from': currentUser.uid,
    //   'type': 'notification',
    //   'date': DateTime.now(),
    //   'name': currentUser.name,
    //   'group': group.documentID,
    //   'text':
    //       'Sua solicitação para entrar na comoonidade ${group['name']} foi aprovada'
    // });

    batch.commit().then((_) {
      setState(() {});
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(
            '${notification.title ?? notification.name} foi adicionado à comoonidade ${group['name']}',
            style: ComooStyles.textoSnackBar),
        duration: const Duration(seconds: 2),
      ));
    });
  }

  @override
  void initState() {
    super.initState();
    timeago.setLocaleMessages('pt_BR', timeago.PtBrMessages());
    api.clearNotificationsCounter();
  }

  StreamBuilder _buildNegotiationList() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return StreamBuilder<QuerySnapshot>(
        stream: _db
            .collection('users')
            .document(currentUser.uid)
            .collection('notifications')
            .orderBy('date', descending: true)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData ||
              snapshot.connectionState == ConnectionState.waiting)
            return Center(child: CircularProgressIndicator());
          if (snapshot.data.documents.isEmpty)
            return Padding(
              padding: EdgeInsets.all(mediaQuery * 16.0),
              child: Center(
                  child: Text(
                'Nenhuma notificação ;-)',
                style: ComooStyles.titulos,
              )),
            );
          return ListView(
              key: PageStorageKey('value'),
              padding: EdgeInsets.only(top: mediaQuery * 10.0),
              children:
                  snapshot.data.documents.map((DocumentSnapshot document) {
                final UserNotification notification =
                    UserNotification.fromSnapshot(document);
                if (notification.type == 'negotiation_chat') {
                  return Container();
                }
                return Dismissible(
                  key: Key(notification.id),
                  background: Container(
                    color: COMOO.colors.pink,
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(right: mediaQuery * 13.0),
                    child: Image.asset(
                      'images/trash.png',
                      width: mediaQuery * 15.0,
                      height: mediaQuery * 17.0,
                    ),
                    // child: Icon(
                    //   Icons.delete,
                    //   color: Colors.white,
                    // ),
                  ),
                  onDismissed: (DismissDirection dir) {
                    print('');
                    api.deleteNotification(context, notification.id);
                  },
                  child: ListTile(
                    // leading: ClipOval(
                    //   child: Image(
                    //     image: AdvancedNetworkImage(notification.thumb),
                    //     fit: BoxFit.cover,
                    //     width: mediaQuery * 60.0,
                    //     height: mediaQuery * 60.0,
                    //   ),
                    // ),
                    leading: Container(
                      width: mediaQuery * 60.0,
                      height: mediaQuery * 60.0,
                      child: CustomNetworkImage(url: notification.thumb),
                    ),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text:
                                '${(notification.title) ?? (notification.name)} ',
                            style: TextStyle(
                              fontFamily: COMOO.fonts.montSerrat,
                              color: Color(0xFF461D58),
                              fontSize: mediaQuery * 14.0,
                              fontWeight: FontWeight.bold,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                if (notification.from != null) {
                                  switch (notification.type) {
                                    case 'group_invite':
                                    case 'approved_solicitation':
                                    case 'negotiation_start_client':
                                    case 'negotiation_twelvehours':
                                      break;
                                    default:
                                      if (!notification.type
                                          .startsWith('negotiation')) {
                                        _navigateToUserPage(notification.from);
                                      }
                                      break;
                                  }
                                }
                              },
                          ),
                        ),
                        Text(
                          notification.text + ' ',
                          style: TextStyle(
                            fontFamily: COMOO.fonts.montSerrat,
                            color: Color(0xFF461D58),
                            fontSize: mediaQuery * 14.0,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        SizedBox(height: mediaQuery * 5),
                        Text(timeago.format(notification.date, locale: 'pt_BR'),
                            style: ComooStyles.subTitle),
                      ],
                    ),
                    trailing: _buildActionButton(context, notification),
                    onTap: () {
                      // print(notification.type);
                      switch (notification.type) {
                        case 'product_comment':
                          _openProductCommentPage(notification);
                          break;
                        case 'post_comment':
                          _openPostCommentPage(notification);
                          break;
                        case 'received_money':
                          _openWithdrawPage();
                          break;
                        case 'negotiation_twelvehours':
                          _openNegotiationPayment(notification);
                          break;
                        case 'approved_solicitation':
                          _openGroupFeed(notification);
                          break;
                        case 'negotiation_chat':
                        case 'negotiation_start':
                        case 'negotiation_start_seller':
                        case 'negotiation_start_client':
                        case 'negotiation':
                          _openNegotiationChat(notification);
                          break;
                        case 'negotiation_pending':
                        case 'negotiation_waiting_send':
                        case 'negotiation_finished':
                          _openMySellings();
                          break;
                        case 'negotiation_cancelled':
                          _openMyBuyings();
                          break;
                        case 'group_invite':
                        case 'group_request':
                        case 'approved_solicitation':
                        case 'suggestion':
                          print('invite');
                          _openGroupPage(notification);
                          break;
                        case 'comment_notification':
                          return _openProductPage(notification);
                          break;
                        default:
                          final bool isRequestApproval = notification.text
                              .toLowerCase()
                              .contains(
                                  'solicitação para entrar na comoonidade');
                          if (isRequestApproval) {
                            _openGroupPage(notification);
                          } else {
                            final String type = notification.type.contains('_')
                                ? notification.type.split('_').first
                                : notification.type;
                            switch (type) {
                              case 'product':
                                _openProductPage(notification);
                                break;
                              case 'group':
                                _openGroupPage(notification);
                                break;
                              case 'user':
                                _navigateToUserPage(notification.from);
                                break;
                              case 'negotiation':
                                _openNegotiation(notification);
                                break;
                              case 'post':
                                // TODO(luan): Post Page needs to be done.
                                //  _openPostPage
                                break;
                            }
                          }
                          break;
                      }
                      _updateCounters();
                      return null;
                    },
                  ),
                );
              }).toList());
        });
  }

  Future<void> _updateCounters() async {
    widget.updateCounters();
    api.clearNotificationsCounter();
  }

  Widget _buildActionButton(
      BuildContext context, UserNotification notification) {
    Widget button;
    switch (notification.type) {
      case 'group_request':
        button = NotificationButton(
          text: notification.approved ? 'APROVADO' : 'APROVAR',
          onPressed: notification.approved
              ? null
              : () {
                  _approveRequest(notification);
                  _updateCounters();
                },
        );
        break;
      case 'group_invite':
        button = NotificationButton(
          text: 'ACEITAR',
          onPressed: notification.approved
              ? null
              : () {
                  // _acceptInvite(notification);
                  showCustomDialog(
                    context: context,
                    alert: customRoundAlert(
                      context: context,
                      title:
                          'Tem certeza que você deseja aceitar o convite para a comoonidade?',
                      actions: [
                        Container(
                          margin: EdgeInsets.symmetric(
                            vertical: responsive(context, 10.0),
                          ),
                          child: OutlineButton(
                            child: Text(
                              'CANCELAR',
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            borderSide: BorderSide(color: ComooColors.white500),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  responsive(context, 20.0)),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(
                            vertical: responsive(context, 10.0),
                          ),
                          child: OutlineButton(
                            child: Text(
                              'ACEITAR',
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                              _acceptInvite(notification);
                              _updateCounters();
                            },
                            borderSide: BorderSide(color: ComooColors.white500),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  responsive(context, 20.0)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
        );
        break;
      case 'negotiation_sent':
        button = NotificationButton(
          text: 'RECEBI',
          onPressed: notification.approved
              ? null
              : () {
                  _receivedConfirmation(context, notification);
                  _updateCounters();
                },
        );
        break;
      default:
        button = null;
        break;
    }
    return button;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: _isLoading ? LoadingContainer() : _buildNegotiationList());
  }

  Future<void> _openNegotiationChat(UserNotification notification) async {
    final String ref = notification.ref ?? notification.group;
    setState(() {
      _isLoading = true;
    });
    final DocumentSnapshot negotiationSnapshot =
        await _db.collection('negotiations').document(ref).get();
    final Negotiation negotiation =
        Negotiation.fromSnapshot(negotiationSnapshot);

    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => DirectChatView(negotiation)));
    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _openNegotiationPayment(UserNotification notification) async {
    final String ref = notification.ref ?? notification.group;
    setState(() {
      _isLoading = true;
    });
    final DocumentSnapshot negotiationSnapshot =
        await _db.collection('negotiations').document(ref).get();
    final Negotiation negotiation =
        Negotiation.fromSnapshot(negotiationSnapshot);

    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) =>
          TransactionView(negotiation: negotiation),
    ));
    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _openWithdrawPage() async {
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) => WithdrawPage(),
    ));
  }

  Future<void> _openMySellings() async {
    Navigator.of(context).pushNamed('/sell_history');
  }

  Future<void> _openMyBuyings() async {
    Navigator.of(context).pushNamed('/buy_history');
  }

  Future<void> _openNegotiation(UserNotification notification) async {
    final String ref = notification.ref ?? notification.group;
    setState(() {
      _isLoading = true;
    });
    final DocumentSnapshot negotiationSnapshot =
        await _db.collection('negotiations').document(ref).get();
    final Negotiation negotiation =
        Negotiation.fromSnapshot(negotiationSnapshot);

    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => NegotiationPage(negotiation)));
    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _openProductCommentPage(UserNotification notification) async {
    print('vaca1');
    final String ref = notification.ref ?? notification.group;
    if (ref != null) {
      setState(() {
        _isLoading = true;
      });
      final DocumentSnapshot documentSnapshot =
          await _db.collection('products').document(ref).get();
      final Product product = Product.fromSnapshot(documentSnapshot);

      Navigator.push(context,
          MaterialPageRoute<void>(builder: (BuildContext context) {
        print('vaca');
        return ProductCommentsView(product);
      }));
      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<void> _openPostCommentPage(UserNotification notification) async {
    final String ref = notification.ref ?? notification.group;
    if (ref != null) {
      setState(() {
        _isLoading = true;
      });
      final DocumentSnapshot documentSnapshot =
          await _db.collection('posts').document(ref).get();
      final Post post = Post.fromSnapshot(documentSnapshot);

      Navigator.push(context,
          MaterialPageRoute<void>(builder: (BuildContext context) {
        print('vaca');
        return PostCommentsView(post);
      }));
      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<void> _openProductPage(UserNotification notification) async {
    print('vaca1');
    final String ref = notification.ref ?? notification.group;
    if (ref != null) {
      setState(() {
        _isLoading = true;
      });
      final DocumentSnapshot documentSnapshot =
          await _db.collection('products').document(ref).get();
      final Product product = Product.fromSnapshot(documentSnapshot);

      Navigator.push(context,
          MaterialPageRoute<void>(builder: (BuildContext context) {
        print('vaca');
        return ProductPage(product);
      }));

      // Navigator.of(context).push(MaterialPageRoute<void>(
      //     builder: (BuildContext context) {
      //       print('vaca');
      //       return ProductPage(product);
      //     }));

      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<void> _openGroupPage(UserNotification notification) async {
    final String ref = notification.ref ?? notification.group;
    if (ref != null) {
      setState(() {
        _isLoading = true;
      });
      final DocumentSnapshot documentSnapshot =
          await _db.collection('groups').document(ref).get();
      final Group group = Group.fromSnapshot(documentSnapshot);

      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => GroupDetailView(group: group)));

      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<void> _openGroupFeed(UserNotification notification) async {
    final String ref = notification.ref ?? notification.group;
    if (ref != null) {
      setState(() {
        _isLoading = true;
      });
      final DocumentSnapshot documentSnapshot =
          await _db.collection('groups').document(ref).get();
      final Group group = Group.fromSnapshot(documentSnapshot);

      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => GroupChatPage(group)));

      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<void> _navigateToUserPage(String id) async {
    final DocumentSnapshot snap =
        await _db.collection('users').document(id).get();
    if (snap.exists) {
      final User user = User.fromSnapshot(snap);
      Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => UserDetailView(user),
      ));
    }
  }

  Future<void> _receivedConfirmation(
      BuildContext context, UserNotification notification) async {
    final String ref = notification.ref ?? notification.group;
    setState(() {
      _isLoading = true;
    });
    final DocumentSnapshot negotiationSnapshot =
        await _db.collection('negotiations').document(ref).get();
    final Negotiation negotiation =
        Negotiation.fromSnapshot(negotiationSnapshot);
    await api.receivedConfirmation(negotiation);
    setState(() {
      _isLoading = false;
    });
  }
}

class NotificationButton extends StatelessWidget {
  NotificationButton({Key key, this.onPressed, this.text}) : super(key: key);
  final Function onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return RaisedButton(
      elevation: 0.0,
      color: COMOO.colors.turquoise,
      padding: EdgeInsets.all(0),
      onPressed: onPressed,
      child: FittedBox(
        child: Text(
          text,
          style: TextStyle(
            fontFamily: COMOO.fonts.montSerrat,
            letterSpacing: mediaQuery * 0.3,
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: mediaQuery * 14.0,
          ),
        ),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(mediaQuery * 50.0),
      ),
    );
  }
}
