//import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/pages/common/chat/direct_chat.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

class UserNegotiationsPage extends StatefulWidget {
  UserNegotiationsPage({Key key}) : super(key: key);

  @override
  State createState() {
    return new _UserNegotiationsPageState();
  }
}

class _UserNegotiationsPageState extends State<UserNegotiationsPage> {
  final Firestore _db = Firestore.instance;

  StreamBuilder _buildNegotiationList() {
    DocumentReference userRef =
        _db.collection("users").document(currentUser.uid);

    return new StreamBuilder<QuerySnapshot>(
        stream: _db
            .collection("negotiations")
            .where("client", isEqualTo: userRef)
            .orderBy("lastUpdate", descending: true)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData ||
              snapshot.connectionState == ConnectionState.waiting)
            return new Center(child: new CircularProgressIndicator());
          print(snapshot.data.documentChanges);
          if (snapshot.data.documents.isEmpty)
            return new Padding(
              padding: const EdgeInsets.all(16.0),
              child: new Center(
                  child: new RichText(
                      text: new TextSpan(
                text: "Você ainda não tem negociações.\n",
                children: <TextSpan>[
                  new TextSpan(
                      style: ComooStyles.welcomeMessage,
                      text:
                          "Entre para as comoonidades do seu interesse e conecte-se à pessoas que tenham a ver com você!")
                ],
                style: ComooStyles.welcomeMessageBold,
              ))),
            );
          return new ListView(
              children:
                  snapshot.data.documents.map((DocumentSnapshot document) {
            Negotiation negotiation = new Negotiation.fromSnapshot(document);
            return new ListTile(
              leading: new Image(
                image: new AdvancedNetworkImage(negotiation.thumb),
                fit: BoxFit.cover,
                width: 40.0,
                height: 40.0,
              ),
              title: new RichText(
                text: new TextSpan(
                    text: negotiation.title + '\r\n',
                    style: ComooStyles.texto,
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              timeago.format(negotiation.date, locale: 'pt_BR'),
                          style: ComooStyles.subTitle),
                    ]),
              ),
              subtitle: new Text(negotiation.lastMessage ?? ""),
              trailing: Text(
                negotiation.statusName.toUpperCase(),
                style: ComooStyles.statusNegociacaoNotificacoes,
              ),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (context) => new DirectChatView(negotiation)));
              },
            );
          }).toList());
        });
  }

  // _status(Negotiation negotiation) {
  //   switch (negotiation.status) {
  //     case "under negotiation":
  //       return 'AGUARDANDO PAGAMENTO';
  //     case 'pending':
  //       return 'PROCESSANDO PAGAMENTO';
  //     case "cancelled":
  //       return "CANCELADA";
  //     case "paid":
  //       return "PAGO";
  //     case "sent":
  //       return "ENVIADO";
  //     case "finished":
  //       return "FINALIZADO";
  //     case "expired":
  //       return "VENCIDO";
  //     default:
  //       return negotiation.status;
  //   }
  // }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(child: _buildNegotiationList());
  }

  @override
  void dispose() {
    super.dispose(); //new
  }
}
