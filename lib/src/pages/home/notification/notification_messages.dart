import 'package:comoo/src/blocs/notification_bloc.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'user_notifications_page.dart';
// import 'user_negotiations.dart';
import 'user_chats.dart';
import 'package:comoo/src/utility/style.dart';
// import 'user_sellings_negotiations.dart';

class NotificationMessages extends StatefulWidget {
  NotificationMessages({Key key}) : super(key: key);

  @override
  State createState() {
    return NotificationMessagesState();
  }
}

class NotificationMessagesState extends State<NotificationMessages>
    with Reusables {
  final NetworkApi api = NetworkApi();
  final BehaviorSubject<int> _notificationsCounter = BehaviorSubject<int>();
  final BehaviorSubject<int> _chatCounter = BehaviorSubject<int>();
  NotificationBloc bloc;
  // Function(int) get addNotificationsCounter => _notificationsCounter.sink.add;
  // Function(int) get addChatCounter => _chatCounter.sink.add;
  // Stream<int> get notificationsCounter => _notificationsCounter.stream;
  // Stream<int> get chatsCounter => _chatCounter.stream;

  Future<void> init() async {
    final Map<String, int> counters = await api.fetchNotificationsCounters();
    bloc.editNotificationsCounter(counters['notifications'] ?? 0);
    bloc.editChatsCounter(counters['chats'] ?? 0);
  }

  @override
  void initState() {
    bloc = NotificationBloc();
    init();
    super.initState();
  }

  @override
  void dispose() {
    _notificationsCounter?.close();
    _chatCounter?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle labelStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      fontSize: responsive(context, 16.0),
      fontWeight: FontWeight.bold,
      letterSpacing: responsive(context, 0.52),
      color: COMOO.colors.lead,
    );
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Column(
        children: <Widget>[
          TabBar(
            labelColor: ComooColors.roxo,
            indicatorColor: ComooColors.roxo,
            labelStyle: labelStyle,
            onTap: (int index) {
              init();
            },
            indicatorWeight: responsive(context, 5.0),
            unselectedLabelStyle:
                labelStyle.copyWith(fontWeight: FontWeight.normal),
            tabs: <Widget>[
              Tab(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Notificações',
                      style: ComooStyles.notificationTitle,
                    ),
                    StreamBuilder<int>(
                        stream: bloc.notificationsCounter,
                        builder:
                            (BuildContext context, AsyncSnapshot<int> counter) {
                          return counter.hasData
                              ? counter.data > 0
                                  ? Container(
                                      margin: EdgeInsets.only(
                                          left: responsive(context, 5.0)),
                                      height: responsive(context, 22.0),
                                      constraints: BoxConstraints(
                                        minWidth: 17.0,
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: responsive(context, 5.0)),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            responsive(context, 20)),
                                        color: COMOO.colors.pink,
                                      ),
                                      child: Center(
                                        child: FittedBox(
                                            child: Text(
                                          _convertIntToString(counter.data),
                                          style: TextStyle(
                                            fontFamily: COMOO.fonts.montSerrat,
                                            color: Colors.white,
                                            fontSize: responsive(context, 12.0),
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                                responsive(context, 0.52),
                                          ),
                                        )),
                                      ),
                                    )
                                  : Container()
                              : Container();
                        }),
                  ],
                ),
              ),
              // Tab(child: Text('Compras', style: ComooStyles.notificationTitle,) ,),
              // Tab(child: Text('Vendas', style: ComooStyles.notificationTitle,) ,),
              Tab(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Conversas',
                      style: ComooStyles.notificationTitle,
                    ),
                    StreamBuilder<int>(
                        stream: bloc.chatsCounter,
                        builder:
                            (BuildContext context, AsyncSnapshot<int> counter) {
                          return counter.hasData
                              ? counter.data > 0
                                  ? Container(
                                      margin: EdgeInsets.only(
                                          left: responsive(context, 5.0)),
                                      height: responsive(context, 22.0),
                                      constraints: BoxConstraints(
                                        minWidth: 22.0,
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: responsive(context, 5.0)),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            responsive(context, 20)),
                                        color: COMOO.colors.pink,
                                      ),
                                      child: Center(
                                        child: FittedBox(
                                            child: Text(
                                          _convertIntToString(counter.data),
                                          style: TextStyle(
                                            fontFamily: COMOO.fonts.montSerrat,
                                            color: Colors.white,
                                            fontSize: responsive(context, 12.0),
                                            fontWeight: FontWeight.w500,
                                            letterSpacing:
                                                responsive(context, 0.52),
                                          ),
                                        )),
                                      ),
                                    )
                                  : Container()
                              : Container();
                        }),
                  ],
                ),
              ),
            ],
          ),
          Expanded(
            child: TabBarView(
              children: <Widget>[
                UserNotificationPage(updateCounters: init),
                // UserNegotiationsPage(),
                // SellerNegotiationsPage(),
                ChatsPage(updateCounters: init),
              ],
            ),
          )
        ],
      ),
    );
  }

  String _convertIntToString(int value) {
    if (value > 99) {
      return '99+';
    } else {
      return '$value';
    }
  }
}
