import 'dart:async';
import 'package:comoo/src/blocs/chats_bloc.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/pages/common/chat/direct_chat.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/negotiation.dart';

class ChatsPage extends StatefulWidget {
  ChatsPage({Key key, @required this.updateCounters}) : super(key: key);

  final Function() updateCounters;

  @override
  State createState() {
    return _ChatsPageState();
  }
}

class _ChatsPageState extends State<ChatsPage> {
  ChatsBloc bloc;
  @override
  void initState() {
    bloc = ChatsBloc(updateCounters: widget.updateCounters);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChatsBloc>(
      bloc: bloc,
      child: _ChatsStateless(),
    );
  }
}

class _ChatsStateless extends StatelessWidget with Reusables {
  _ChatsStateless();
  @override
  Widget build(BuildContext context) {
    final ChatsBloc bloc = BlocProvider.of<ChatsBloc>(context);
    return Container(
      child: StreamBuilder<bool>(
        stream: bloc.isLoading,
        builder: (BuildContext context, AsyncSnapshot<bool> loading) {
          return loading.data ?? false
              ? LoadingContainer()
              : _buildChatsStream(context, bloc);
        },
      ),
    );
  }

  Widget _buildChatsStream(BuildContext context, ChatsBloc bloc) {
    return StreamBuilder<List<Negotiation>>(
      stream: bloc.chats,
      builder:
          (BuildContext context, AsyncSnapshot<List<Negotiation>> snapshot) {
        print(
            '${snapshot.connectionState} - has data? ${snapshot.hasData} ${snapshot.data?.length}');
        switch (snapshot.connectionState) {
          case ConnectionState.active:
            if (snapshot.hasData)
              return _buildChatsList(context, bloc, snapshot);
            return _buildEmptyList(context);
            break;
          case ConnectionState.done:
            if (snapshot.hasData)
              return _buildChatsList(context, bloc, snapshot);
            return _buildEmptyList(context);
            break;
          case ConnectionState.none:
            return _buildEmptyList(context);
            break;
          case ConnectionState.waiting:
            if (snapshot.hasData)
              return _buildChatsList(context, bloc, snapshot);
            return LoadingContainer();
            break;
          default:
            return Container();
            break;
        }
      },
    );
  }

  Widget _buildEmptyList(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(responsive(context, 16.0)),
      child: Center(
        child: RichText(
          text: TextSpan(
            text: 'Você ainda não tem conversas.\n',
            children: <TextSpan>[
              TextSpan(
                  style: ComooStyles.welcomeMessage,
                  text:
                      'Entre para as comoonidades do seu interesse e conecte-se à pessoas que tenham a ver com você!')
            ],
            style: ComooStyles.welcomeMessageBold,
          ),
        ),
      ),
    );
  }

  Widget _buildChatsList(BuildContext context, ChatsBloc bloc,
      AsyncSnapshot<List<Negotiation>> snapshot) {
    if (snapshot.data.isEmpty) {
      return _buildEmptyList(context);
    }
    snapshot.data.sort(
        (Negotiation a, Negotiation b) => b.lastUpdate.compareTo(a.lastUpdate));
    return AnimatedList(
      initialItemCount: snapshot.data.length,
      itemBuilder:
          (BuildContext context, int index, Animation<double> animation) {
        if (index >= snapshot.data.length) {
          return Container();
        }
        return _buildNegotiationCard(context, bloc, snapshot.data[index]);
      },
    );
  }

  Widget _buildNegotiationCard(
      BuildContext context, ChatsBloc bloc, Negotiation negotiation) {
    final bool isCurrentUserBuyer =
        currentUser.uid == negotiation.buyer.documentID;
    final List<String> nameSplit = isCurrentUserBuyer
        ? negotiation?.sellerName?.split(' ') ?? ['']
        : negotiation?.buyerName?.split(' ') ?? [''];
    final String userName = nameSplit.first + ' ' + nameSplit.last;
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
              // style: BorderStyle.solid,
              color: Color.fromRGBO(206, 206, 206, 0.5),
              width: responsive(context, 1.0)),
        ),
      ),
      child: ListTile(
        contentPadding:
            EdgeInsets.symmetric(horizontal: responsive(context, 10.0)),
        leading: Container(
          width: responsive(context, 55.0),
          height: responsive(context, 59.0),
          decoration: ShapeDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AdvancedNetworkImage(
                negotiation.thumb,
                useDiskCache: true,
              ),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(responsive(context, 10.0)),
              ),
            ),
          ),
        ),
        title: RichText(
          text: TextSpan(
            text: negotiation.title + ' ',
            style: TextStyle(
              fontFamily: ComooStyles.fontFamily,
              color: ComooColors.roxo,
              fontSize: responsive(context, 14.0),
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        subtitle: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${userName.split(' ').first} ${userName.split(' ').last}',
                    style: TextStyle(
                      fontFamily: ComooStyles.fontFamily,
                      color: ComooColors.cinzaMedio,
                      fontSize: responsive(context, 13.0),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              Text(
                timeago.format(negotiation.lastUpdate, locale: 'pt_BR'),
                style: ComooStyles.subTitle,
                overflow: TextOverflow.fade,
              ),
              Text(
                '${negotiation.lastMessage}',
                style: ComooStyles.subTitle.apply(
                  color: ComooColors.cinzaMedio.withOpacity(0.65),
                ),
                maxLines: 3,
                textAlign: TextAlign.start,
              ),
            ],
          ),
        ),
        trailing: FittedBox(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: responsive(context, 10.0)),
                alignment: Alignment.topCenter,
                constraints: BoxConstraints(
                  // maxHeight: mediaQuery * 30.0,
                  maxWidth: responsive(context, 125.0),
                ),
                child: Text(
                  negotiation.statusName.toUpperCase(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: ComooStyles.fontFamily,
                    color: _statusColor(negotiation),
                    fontSize: responsive(context, 11.0),
                    fontWeight: FontWeight.w600,
                    letterSpacing: responsive(context, 0.45),
                  ),
                ),
              ),
              negotiation.unreadMessages > 0
                  ? Container(
                      margin: EdgeInsets.only(left: responsive(context, 5.0)),
                      height: responsive(context, 22.0),
                      constraints:
                          BoxConstraints(minWidth: 17.0, maxWidth: 25.0),
                      padding: EdgeInsets.symmetric(
                          horizontal: responsive(context, 5.0)),
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(responsive(context, 20)),
                        color: COMOO.colors.pink,
                      ),
                      child: Center(
                        child: FittedBox(
                            child: Text(
                          negotiation.displayUnreadMessages(),
                          style: TextStyle(
                            fontFamily: COMOO.fonts.montSerrat,
                            color: Colors.white,
                            fontSize: responsive(context, 12.0),
                            fontWeight: FontWeight.w500,
                            letterSpacing: responsive(context, 0.52),
                          ),
                        )),
                      ),
                    )
                  : Container(width: 0, height: 0),
            ],
          ),
        ),
        onTap: () {
          _openCard(context, bloc, negotiation);
        },
      ),
    );
  }

  Color _statusColor(Negotiation negotiation) {
    switch (negotiation.status) {
      case 'cancelled':
      case 'finished':
        return ComooColors.cinzaMedio;
        break;
      default:
        return ComooColors.turqueza;
    }
  }

  Future<void> _openCard(
      BuildContext context, ChatsBloc bloc, Negotiation negotiation) async {
    await Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => DirectChatView(negotiation)));
    bloc.updateCounters();
    // Navigator.of(context).push(new MaterialPageRoute(
    //     builder: (context) => NegotiationPage(negotiation)));

    bloc.fetchNegotiations();
  }
}
