import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/pages/common/transaction/negotiation_page.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:intl/intl.dart';
import 'package:comoo/src/widgets/loading_container.dart';

class SellHistory extends StatefulWidget {
  @override
  State createState() {
    return SellHistoryState();
  }
}

class SellHistoryState extends State<SellHistory> with Reusables {
  // String negotiationStatus(String status) {
  //   switch (status) {
  //     case 'cancelled':
  //       return 'Cancelado';
  //     case 'under negotiation':
  //       return 'Aguardando Pagamento';
  //     case 'pending':
  //       return 'Processando Pagamento';
  //     case 'sent':
  //       return 'Enviado';
  //     case 'paid':
  //       return 'Pago';
  //     case 'finished':
  //       return 'Concluído';
  //     default:
  //       return '';
  //   }
  // }

  final NetworkApi api = NetworkApi();
  bool _loading = false;
  // final Firestore _db = Firestore.instance;
  Future<void> _sendDeliveryNotification(Negotiation negotiation) async {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final bool result = await showCustomDialog<bool>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title:
            'Você está prestes a infomar ao cliente que já enviou o produto, tem certeza disso?',
        actions: [
          Container(
            margin: EdgeInsets.symmetric(
              // horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'CANCELAR',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(false),
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(
              // horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'CONFIRMAR',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(true),
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
        ],
      ),
    );

    if (result == true) {
      //SEND
      if (mounted) {
        setState(() {
          _loading = true;
        });
      }
      negotiation.status = 'sent';
      try {
        api.deliveryConfirmation(negotiation).then((dynamic result) {
          if (mounted) {
            setState(() {
              _loading = false;
            });
          }
        });
      } catch (e) {
        print(e);
        if (mounted) {
          setState(() {
            _loading = false;
          });
        }
      }
      // _db.collection('negotiations').document(negotiation.id).updateData(
      //     {'status': 'sent', 'deliveryDate': DateTime.now()}).then((_) {
      //   setState(() {
      //     _loading = false;
      //   });
      // });
    }
  }

  final NumberFormat formatter =
      NumberFormat.currency(locale: 'pt-br', symbol: 'R\$');

  @override
  Widget build(BuildContext context) {
    // final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
        appBar: CustomAppBar.round(
          color: ComooColors.chumbo,
          title: Text('minhas vendas'),
        ),
        body: Stack(
          children: <Widget>[
            StreamBuilder<QuerySnapshot>(
              stream: Firestore.instance
                  .collection('negotiations')
                  .where(
                    'seller',
                    isEqualTo: Firestore.instance
                        .collection('users')
                        .document(currentUser.uid),
                  )
                  .orderBy('lastUpdate', descending: true)
                  .snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                // print('${snapshot.connectionState} - has data? ${snapshot.hasData}');
                switch (snapshot.connectionState) {
                  case ConnectionState.active:
                    if (snapshot.hasData) return _buildSnapshotData(snapshot);
                    return _buildEmptyList();
                    break;
                  case ConnectionState.done:
                    if (snapshot.hasData) return _buildSnapshotData(snapshot);
                    return _buildEmptyList();
                    break;
                  case ConnectionState.none:
                    return _buildEmptyList();
                    break;
                  case ConnectionState.waiting:
                    if (snapshot.hasData) return _buildSnapshotData(snapshot);
                    return LoadingContainer();
                    break;
                  default:
                    return Container();
                    break;
                }
              },
            ),
            _loading ? LoadingContainer() : Container()
          ],
        ));
  }

  Widget _buildSnapshotData(AsyncSnapshot<QuerySnapshot> snapshot) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    if (snapshot.data.documents.isEmpty) {
      return _buildEmptyList();
    }
    return ListView.separated(
      padding: EdgeInsets.only(top: mediaQuery * 10.0),
      separatorBuilder: (BuildContext context, int index) {
        return Divider(
          color: ComooColors.cinzaMedio,
        );
      },
      itemBuilder: (BuildContext context, int index) {
        final DocumentSnapshot document = snapshot.data.documents[index];
        final Negotiation negotiation = Negotiation.fromSnapshot(document);
        return ListTile(
          contentPadding: EdgeInsets.symmetric(
              vertical: 0.0, horizontal: mediaQuery * 10.0),
          dense: true,
          leading: Container(
            decoration: BoxDecoration(
                border: Border.all(color: ComooColors.cinzaClaro),
                borderRadius: BorderRadius.circular(mediaQuery * 10.0),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(negotiation.thumb),
                )),
            width: mediaQuery * 55.0,
            height: mediaQuery * 55.0,
          ),
          title: Text(
            negotiation.title,
            style: ComooStyles.productTitle,
          ),
          isThreeLine: true,
          trailing: Container(
            width: mediaQuery * 120.0,
            child: FittedBox(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: _buildActionButton(context, negotiation),
                    height: responsive(context, 30.0),
                  ),
                  SizedBox(height: responsive(context, 5.0)),
                  Text(
                    DateFormat('dd/MM/yyyy').format(negotiation.date),
                    style: ComooStyles.productTitlePrice,
                  ),
                ],
              ),
            ),
          ),
          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(formatter.format(negotiation.price),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: ComooStyles.productTitlePrice),
              Text(
                negotiation.statusName,
                style: ComooStyles.productTitlePrice,
              )
            ],
          ),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute<void>(
                builder: (context) => NegotiationPage(negotiation)));
          },
        );
      },
      itemCount: snapshot.data.documents.length,
    );
  }

  Widget _buildActionButton(BuildContext context, Negotiation negotiation) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    switch (negotiation.status) {
      case 'waiting_send':
        return Container(
          margin: EdgeInsets.only(
            top: mediaQuery * 1.0,
          ),
          height: mediaQuery * 30.0,
          child: RaisedButton(
            elevation: 0.0,
            color: ComooColors.turqueza,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
            onPressed: () {
              _sendDeliveryNotification(negotiation);
            },
            child: FittedBox(
              child: Text(
                'ENTREGOU?',
                style: TextStyle(
                  fontFamily: ComooStyles.fontFamily,
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        );
        break;
      default:
        return Container();
        break;
    }
  }

  Widget _buildEmptyList() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Padding(
      padding: EdgeInsets.all(mediaQuery * 16.0),
      child: Center(
          child: RichText(
              text: TextSpan(
        text: 'Você ainda não realizou nenhuma venda.\n',
        children: <TextSpan>[
          TextSpan(
              style: ComooStyles.welcomeMessage,
              text:
                  'Faça um anúncio nas suas comoonidades para que as pessoas encontrem o que você está vendendo!')
        ],
        style: ComooStyles.welcomeMessageBold,
      ))),
    );
  }
}
