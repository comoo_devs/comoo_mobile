import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FAQContent {
  final TextStyle text = TextStyle(
    fontFamily: COMOO.fonts.montSerrat,
    color: ComooColors.chumbo,
    letterSpacing: 0.6,
  );
  final TextStyle icon = TextStyle(
    color: ComooColors.turqueza,
  );
  final TextAlign textAlign = TextAlign.left;
  final Reusables reusables = Reusables();
  int count;
  BuildContext _context;
  // ScrollController _controller;

  getFAQContent(BuildContext context, {ScrollController controller}) {
    // _controller = controller;
    _context = context;
    count = 0;
    List<Tile> data = List();
    data.add(Tile(
      title: 'PARA TODOS',
      entries: _getAll(),
    ));
    data.add(Tile(
      title: 'PARA VENDEDORES',
      entries: _getSellers(),
    ));
    data.add(Tile(
      title: 'PARA COMPRADORES',
      entries: _getBuyers(),
    ));
    data.add(Tile(
      title: 'ENVIO e PÓS-VENDA',
      entries: _getShipping(),
    ));
    return data;
  }

  _onTap(Function f) {
    return TapGestureRecognizer()..onTap = () => f();
  }

  // TapGestureRecognizer _navigateToEntry(int index) {
  //   return TapGestureRecognizer()
  //     ..onTap = () {
  //       print('$index');
  // if (_controller != null) {
  // print(_controller.position.axis.index);
  //       }
  //     };
  // }

  _copySupportEmail() async {
    await Clipboard.setData(new ClipboardData(text: 'suporte@comoo.io'));
    reusables.showCustomDialog(
      context: _context,
      alert: reusables.customRoundAlert(
        context: _context,
        title: 'E-mail copiado para a área de transferência!',
      ),
    );
  }

  _navigateToMyProfile() {
    Navigator.of(_context).pushNamed('/user_profile');
  }

  _navigateToMyAccount() {
    Navigator.of(_context).pop();
  }

  _navigateToMyPreferences() {
    Navigator.of(_context).pushNamed('/user_categories');
  }

  _navigateToMyExtract() {
    Navigator.of(_context).pushNamed('/my_extract');
  }

  _navigateToBuyHistory() {
    Navigator.of(_context).pushNamed('/buy_history');
  }

  _navigateToSellHistory() {
    Navigator.of(_context).pushNamed('/sell_history');
  }

  _getAll() {
    List<Entry> all = List();
    all.add(Entry(
      index: ++count,
      title: 'O que é a COMOO?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          style: text,
          text: 'Que bom que você perguntou. A ',
          children: [
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' é uma rede social de compra e venda com lucro compartilhado, isso quer dizer que quando alguém compra ou quando alguém vende na ',
              style: text,
            ),
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ', todo mundo ganha. É um novo jeito de ser, um novo de ter, bom para todos.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'O que é lucro compartilhado e como funciona?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          style: text,
          text: 'O propósito da ',
          children: [
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' é ser lucrativa para todo mundo. E para viver isso, na prática, adotamos o lucro compartilhado como modelo de negócios. Funciona da seguinte forma: quando você convida alguém para a ',
              style: text,
            ),
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: ', tudo que seu convidado vender ou comprar rende ',
              style: text,
            ),
            TextSpan(
              text: '1,5%',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' de comissão pra você sobre valor anunciado. E não pára por aí, quando os seus convidados convidarem outros usuários, você continua ganhando! A cada transação realizada (compra ou venda) pelos convidados dos seus convidados, você ganha ',
              style: text,
            ),
            TextSpan(
              text: '1%',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: ' de comissão sobre o valor anunciado. Na ',
              style: text,
            ),
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' você sempre vai ganhar em cima de dois níveis de relacionamento: o primeiro, das pessoas que vieram diretamente convidadas por você, e o segundo, das pessoas que vieram convidadas pelos seus amigos. ',
              style: text,
            ),
          ],
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'Como convido amigos na COMOO?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Para convidar seus amigos para a COMOO, clique no botão ',
          style: text,
          children: [
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons_novo',
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: ' que fica na barra inferior da sua tela e, em seguida, ',
              style: text,
            ),
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons_novo',
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text:
                  '. Escolha como quer enviar seu convite (SMS, WhatsApp, Facebook, E-mail, Instagram e etc). Fique ligado, seu link de convite é único e é através dele que identificaremos de onde vem suas comissões. Esse processo é automatizado e não poderá ser alterado manualmente.',
              style: text,
            ),
            TextSpan(
              text:
                  '\n* Para convidar amigos você precisa preencher seu perfil completo em ',
              style: text,
            ),
            TextSpan(
              text: '\'Meu Perfil\' ',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToMyProfile),
            ),
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons_novo',
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
          ],
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title:
          'Quando convido alguém para a COMOO, a pessoa está automaticamente autorizada a participar das minhas comoonidades?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Não. Quando você convida alguém, o usuário entra na ',
          style: text,
          children: [
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  '. Para fazer parte das suas comoonidades, você precisa convidá-lo ou ele precisa solicitar sua permissão e aguardar sua aprovação.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'Quem me convidou pode ver o que estou comprando e vendendo?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Não. O usuário que enviou o convite só tem acesso ao valor de comissão gerado',
          style: text,
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'O que são comoonidades?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'As comoonidades são a alma da COMOO. São grupos de compra e venda criados pelos próprios usuários de acordo com seus interesses, estilo de vida, gostos e peculiaridades. Essa aproximação entre os participantes, torna a interação, parte fundamental em qualquer comunidade e grupo social, leve e divertida. Nas comoonidades, vendedores e compradores feitos um para o outro se encontram, dão “match”:). Vendedores encontram os compradores ideais para seus produtos e onde os compradores encontram exatamente o que estavam precisando, até mesmo antes de buscar ;).',
          style: text,
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'Não estou vendo nenhuma comoonidade',
      content: RichText(
        text: TextSpan(
          text:
              'A COMOO possuiu um filtro de proximidade, ou seja, se você for o primeiro'
              ' usuário da sua região você não verá comoonidades ou sugestão de usuários.'
              ' É uma ótima oportunidade de sair na frente e criar suas comoonidades e '
              'convidar pessoas com seu código de convite.',
          style: text,
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'Como faço para participar de uma comoonidade? ',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Clique no ícone da comoonidade de que deseja participar. Você terá acesso à página de perfil com a descrição e os membros. Clique no botão \'SOLICITAR PARTICIPAÇÃO\' e aguarde até que o administrador aprove sua solicitação. Você será notificado quando isso acontecer.',
          style: text,
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'Como criar comoonidades.',
      content: RichText(
        text: TextSpan(
          text:
              'Crie uma comoonidade clicando no menu + ou no seu perfil. Preencha a '
              'descrição da sua comoonidade, escolha uma foto legal e convide amigos '
              'dentro e fora da COMOO',
          style: text,
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'Como encontro uma comoonidade do meu interesse? ',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Primeiro, certifique-se de ter marcado as categorias do seu interesse em ',
          style: text,
          children: [
            TextSpan(
              text: COMOO.icons.like.unicode,
              style: TextStyle(
                fontFamily: COMOO.icons.like.fontFamily,
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 15.0,
              ),
            ),
            TextSpan(
              text: '\'Minhas Preferências\'. ',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToMyPreferences),
            ),
            TextSpan(
              text: '\nAlgumas comoonidades serão sugeridas no seu feed ',
              style: text,
            ),
            TextSpan(
              text: COMOO.icons.home.unicode,
              style: TextStyle(
                fontFamily: COMOO.icons.home.fontFamily,
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 15.0,
              ),
            ),
            TextSpan(
              text: 'e você também pode (e deve) realizar uma ',
              style: text,
            ),
            TextSpan(
              text: COMOO.icons.search.unicode,
              style: TextStyle(
                fontFamily: COMOO.icons.search.fontFamily,
                color: ComooColors.roxo,
                fontWeight: FontWeight.bold,
                fontSize: 15.0,
              ),
            ),
            TextSpan(
              text: ' busca por nome (de comoonidades ou pessoas). ',
              style: text,
            ),
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Nos resultados da busca por pessoa, você poderá visualizar e solicitar a participação nas comoonidades em que o usuário participa. ',
              style: text,
            ),
          ],
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title:
          ' O vendedor/comprador pediu para se comunicar comigo pelo telefone ou email.',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'A ',
          style: text,
          children: [
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' é feita de interação, acreditamos que tudo pode ser resolvido com diálogo. Portanto, a comunicação entre compradores e vendedores é muito importante para a harmonia e sucesso das transações. Todas as mensagens devem ser feitas através do nosso chat, isso vai garantir que a ',
              style: text,
            ),
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' possa te ajudar no surgimento de algum eventual problema.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'Posso excluir alguém da minha comoonidade?',
      content: RichText(
        text: TextSpan(
          text:
              'Sim. Na página da sua comoonidade, clique no nome da comoonidade na aba superior '
              'do app e clique em “Perfil da Comoonidade”. Você verá uma lista de participantes, '
              'basta clicar no ícone verde ao lado de cada nome e o usuário será removido.',
          style: text,
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'Como edito minha comoonidade?',
      content: RichText(
        text: TextSpan(
          text:
              'Para editar sua comoonidade, alterando área, descrição, adicionar mais '
              'usuários e etc, clique no nome da comoonidade na aba superior do app e '
              'clique em “Perfil da Comoonidade”. Basta clicar no nome do usuário e irá '
              'abrir um pop-up com a opção de removê-lo.',
          style: text,
        ),
      ),
    ));
    all.add(Entry(
      index: ++count,
      title: 'Por que preciso cadastrar minha conta bancária?',
      content: RichText(
        text: TextSpan(
          text:
              'Enquanto sua conta não estiver cadastrada, você não receberá os '
              'ganhos gerados pelo lucro compartilhado. Atenção, o sistema não '
              'opera retroativamente, então tudo que seus convidados venderem '
              'ou comprarem na COMOO não irá para sua conta antes do cadastro '
              'completo estar preenchido.',
          style: text,
        ),
      ),
    ));
    return all;
  }

  _getSellers() {
    List<Entry> sellers = List();
    sellers.add(Entry(
      index: ++count,
      title: 'Quero vender',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Você veio ao lugar certo. Para começar a vender, você precisa preencher o cadastro completo clicando em ',
          style: text,
          children: [
            TextSpan(
              text: '\'Meu Perfil\' ',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToMyProfile),
            ),
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons_novo',
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text:
                  '.\nO segundo passo é escolher onde publicar seus anúncios. Você pode criar uma comoonidade nova e convidar os usuários que tem tudo a ver com ela para participar ou solicitar a permissão para entrar em uma comoonidade que combine com você e com os produtos que você tem.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    sellers.add(Entry(
      index: ++count,
      title: 'Criando um anúncio',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Use a câmera: clique no ícone da câmera ',
          style: text,
          children: [
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons_novo',
                color: ComooColors.roxo,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text:
                  ' tire uma foto do produto que deseja vender ou clique em voltar para escolher imagens da sua galeria.',
              style: text,
            ),
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text: 'Ou acesse o botão ',
              style: text,
            ),
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons_novo',
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: ' que fica na barra inferior da sua tela e selecione ',
              style: text,
            ),
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons_novo',
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: '\'criar um anúncio\'',
              style: text,
            ),
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Depois de selecionar as fotos que deseja publicar, preencha os campos obrigatórios, capriche na descrição, determine o preço de venda e publique em uma ou mais comoonidades. Se preferir publicar depois, salve como rascunho.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    sellers.add(Entry(
      index: ++count,
      title: 'Quanto custa anunciar na COMOO?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Na ',
          style: text,
          children: [
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ', você não paga nada para anunciar. A cada venda, 5% é o lucro compartilhado, distribuídos para os usuários da ',
              style: text,
            ),
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ', e 5% é a nossa comissão. O resto é do vendedor, descontadas as taxas do gateway de pagamento.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    sellers.add(Entry(
      index: ++count,
      title: 'Como calculo o valor que vou receber em cada venda?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Nossa filosofia é ter total transparência e, por isso, optamos por deixar claras as taxas referentes ao gateway de pagamento. Assim, não precisamos aumentar nossa comissão e você pode definir seu preço de venda.',
          style: text,
          children: [
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  'No ato da venda será debitada a taxa referente ao meio de pagamento escolhido pelo ',
              style: text,
            ),
            TextSpan(
              text: 'comprador.',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: ' Taxa para vendas com cartão de crédito = ',
              style: text,
            ),
            TextSpan(
              text: ' 2,99% + R\$0,69',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: ' e nas vendas por boleto bancário e transferência = ',
              style: text,
            ),
            TextSpan(
              text: 'R\$ 3,59.',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Para facilitar, na hora de preencher o anúncio, colocamos uma calculadora que simula quanto você irá receber em uma compra feita com cartão de crédito.',
              style: text,
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text: 'O saldo de uma venda é calculado da seguinte forma:',
              style: text,
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Saldo para o vendedor = Valor do Produto - Taxas Moip - 5% COMOO - 5% Lucro Compartilhado ',
              style: text.copyWith(color: ComooColors.cinzaMedio),
            ),
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Venda com cartão = Valor do Produto - {[(Valor do Produto x 2,99%) + 0,69] - [(10% x Valor do Produto)]} ',
              style: text.copyWith(color: ComooColors.cinzaMedio),
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text: 'Exemplo em uma compra de R\$ 100 com cartão de crédito\n',
              style: text.copyWith(color: ComooColors.cinzaMedio),
            ),
            TextSpan(
              text: '100 - {[(100 x 2,99%) + 0,69] + (- 10% x 100)} \n',
              style: text.copyWith(color: ComooColors.cinzaMedio),
            ),
            TextSpan(
              text: '100 - [(2,99 + 0,69) - 10]\n',
              style: text.copyWith(color: ComooColors.cinzaMedio),
            ),
            TextSpan(
              text: '100 - 13,68 \n',
              style: text.copyWith(color: ComooColors.cinzaMedio),
            ),
            TextSpan(
              text: 'Saldo para o vendedor = R\$ 86,32 \n',
              style: text.copyWith(color: ComooColors.cinzaMedio),
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Venda com boleto ou transferência = Valor do Produto - [3,49 + (-10% x Valor do Produto)]',
              style: text.copyWith(color: ComooColors.cinzaMedio),
            ),
          ],
        ),
      ),
    ));
    sellers.add(Entry(
      index: ++count,
      title:
          'Como verifico o valor que tenho a receber pelas minhas vendas e comissões geradas pelos meus convidados?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Acesse ',
          style: text,
          children: [
            TextSpan(
              text: COMOO.icons.user.unicode,
              style: TextStyle(
                fontFamily: COMOO.icons.user.fontFamily,
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: ' \'Minha Conta\'',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToMyAccount),
            ),
            TextSpan(
              text: '  na barra inferior do app. Selecione a opção ',
              style: text,
            ),
            TextSpan(
              text: COMOO.icons.wallet.unicode,
              style: TextStyle(
                fontFamily: COMOO.icons.wallet.fontFamily,
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: ' \'Meu extrato\'',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToMyExtract),
            ),
            TextSpan(
              text:
                  ' . É aqui que você tem acesso ao seu saldo, aos valores que já estão disponíveis para saque e aos lançamentos futuros. No extrato você também pode verificar os valores gerados pelas suas vendas e os valores gerados pela movimentação dos seus usuários convidados por você. ',
              style: text,
            ),
          ],
        ),
      ),
    ));
    sellers.add(Entry(
      index: ++count,
      title: 'Como resgato meu dinheiro?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Acesse o ícone ',
          style: text,
          children: [
            TextSpan(
              text: COMOO.icons.wallet.unicode,
              style: TextStyle(
                fontFamily: COMOO.icons.wallet.fontFamily,
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text:
                  ' na barra inferior do app e vá para a aba \'Saldo Disponível\'.',
              style: text,
            ),
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Preencha o valor que deseja resgatar e clique em transferir.',
              style: text,
            ),
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text:
                  '*O CPF informado deve coincidir com o CPF da conta bancária.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    sellers.add(Entry(
      index: ++count,
      title: 'Quando posso resgatar meu saldo disponível?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Se você realizou uma venda à vista, esse dinheiro estará disponível para resgate em 30 dias. Caso sua venda tenha sido a prazo, você receberá de acordo com as parcelas (30, 60, 90 dias)*.',
          style: text,
          children: [
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text:
                  '* A taxa do gateway de pagamento será descontada integralmente na 1a parcela.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    sellers.add(Entry(
      index: ++count,
      title: 'Posso cancelar uma venda?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Sim, após 24 horas se o comprador ainda não tiver efetuado o pagamento. Caso haja algum problema com a venda, recomendamos que você entre em contato com o comprador e solicite que ele libere a compra.',
          style: text,
          children: [
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Caso o comprador já tenha efetuado o pagamento, entre em contato conosco através do email ',
              style: text,
            ),
            TextSpan(
              text: 'suporte@comoo.io.',
              style: text.copyWith(
                color: COMOO.colors.medianGray,
                fontWeight: FontWeight.bold,
              ),
              recognizer: _onTap(_copySupportEmail),
            ),
          ],
        ),
      ),
    ));
    return sellers;
  }

  _getBuyers() {
    List<Entry> buyers = List();
    buyers.add(Entry(
      index: ++count,
      title: 'Quero comprar',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Para dar início às suas compras, você precisa fazer parte de uma comoonidade (ou muitas). Você pode fazer isso criando uma comoonidade ou solicitando a permissão para entrar em uma comoonidade que tenha a ver com você.',
          style: text,
          children: [
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text: 'Encontrou com um produto e ele é a sua cara?',
              style: text,
            ),
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text: 'Não perde tempo e clica no botão ',
              style: text,
            ),
            TextSpan(
              text: 'COMPRAR',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  '. Você vai acessar uma tela de chat com o vendedor onde você poderá tirar todas as suas dúvidas e combinar o envio. Tudo certo? Então pode clicar no botão ',
              style: text,
            ),
            TextSpan(
              text: 'COMPRAR',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' e escolher a forma de pagamento. Desistiu da compra? Não tem problema, explique o motivo para o vendedor e clique em ',
              style: text,
            ),
            TextSpan(
              text: 'LIBERAR',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  '*Para efetuar o pagamento você precisa preencher seu perfil completo em ',
              style: text,
            ),
            TextSpan(
              text: '\'Meu Perfil\' ',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToMyProfile),
            ),
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons_novo',
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
          ],
        ),
      ),
    ));
    buyers.add(Entry(
      index: ++count,
      title: 'Fila!',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Na ',
          style: text,
          children: [
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' tudo acontece muito rápido. Se alguém clicar em COMPRAR antes de você, você entrará, automaticamente, na fila. Caso o produto seja liberado pelo comprador anterior, você será notificado e poderá concluir a compra.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    buyers.add(Entry(
      index: ++count,
      title: 'Quais são as formas de pagamento?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Na ',
          style: text,
          children: [
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' você conta com um ambiente seguro de pagamento e pode escolher entre cartão de crédito, boleto bancário ou transferência (Itaú).',
              style: text,
            ),
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Para que o seu pagamento seja refletido o quanto antes e para a compra ser liberada com mais agilidade, prefira o pagamento com o cartão de crédito ou transferência bancária.',
              style: text,
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  'As compras com cartão de crédito podem ser parceladas em até 3x (com juros)*.',
              style: text,
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  'O boleto bancário deve ser pago em 1 (um) dia útil ou perde a validade. Não é possível re-emitir o boleto no caso do vencimento do mesmo. Neste caso, a compra será cancelada.',
              style: text,
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Pagamentos em boleto dependem do tempo de compensação bancária e pode levar até 4 dias úteis para que o pagamento seja liberado pela instituição financeira. Enquanto isso, sua compra ficará com o status pendente.',
              style: text,
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Para sua segurança, em hipótese alguma deve haver pagamento em mãos ou fora da plataforma.',
              style: text,
            ),
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text:
                  '* Os juros são automáticos, mês a mês, e são estipulados pela Moip que é nosso gateway de pagamento.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    buyers.add(Entry(
      index: ++count,
      title: 'Quanto custa comprar na COMOO?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'O comprador não paga nada além do produto, a não ser que opte pelo parcelamento no ',
          style: text,
          children: [
            TextSpan(
              text: 'cartão de crédito.',
              style: text.copyWith(
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
              ),
              // recognizer: _navigateToEntry(21),
            ),
            TextSpan(
              text: '(ver item anterior)',
              style: text,
            ),
          ],
        ),
      ),
    ));
    buyers.add(Entry(
      index: ++count,
      title: 'Recebi meu produto e agora?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Que ótimo. Acesse o menu ',
          style: text,
          children: [
            TextSpan(
              text: '${COMOO.icons.sellings.unicode} ',
              style: TextStyle(
                fontFamily: COMOO.icons.sellings.fontFamily,
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: '\'Minhas Compras\'',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToBuyHistory),
            ),
            TextSpan(
              text:
                  ' e clique no botão \'Recebeu\'. Essa ação é muito importante para liberarmos o pagamento para o vendedor.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    buyers.add(Entry(
      index: ++count,
      title:
          ' O vendedor informou que entregou o produto, mas eu ainda não recebi.',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'No seu perfil, acesse ',
          style: text,
          children: [
            TextSpan(
              text: COMOO.icons.buyings.unicode,
              style: TextStyle(
                fontFamily: COMOO.icons.buyings.fontFamily,
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: ' a opção ',
              style: text,
            ),
            TextSpan(
              text: '\'Minhas Compras\'',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToBuyHistory),
            ),
            TextSpan(
              text:
                  ', clique no produto em questão e entre em contato com o vendedor através do chat ',
              style: text,
            ),
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons',
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: '\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Caso não obtenha reposta, entre em contato conosco através do email ',
              style: text,
            ),
            TextSpan(
              text: 'suporte@comoo.io.',
              style: text.copyWith(
                color: COMOO.colors.medianGray,
                fontWeight: FontWeight.bold,
              ),
              recognizer: _onTap(_copySupportEmail),
            ),
          ],
        ),
      ),
    ));
    buyers.add(Entry(
      index: ++count,
      title: 'Recebi meu produto, mas não estou satisfeita. E agora?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Não se preocupe. Você tem 7 dias corridos a partir da data do recebimento para solicitar a devolução. Envie sua solicitação através do email ',
          style: text,
          children: [
            TextSpan(
              text: 'suporte@comoo.io.',
              style: text.copyWith(
                color: COMOO.colors.medianGray,
                fontWeight: FontWeight.bold,
              ),
              recognizer: _onTap(_copySupportEmail),
            ),
          ],
        ),
      ),
    ));
    buyers.add(Entry(
      index: ++count,
      title: 'Já efetuei o pagamento, mas ainda consta como pendente.',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Pagamentos efetuados com boleto ou cartão podem levar até 4 dias para serem confirmados. Essa análise é feita pelos bancos e pela Moip/Wirecard e, assim que houver atualizações, nós enviaremos uma notificação.',
          style: text,
        ),
      ),
    ));
    return buyers;
  }

  _getShipping() {
    List<Entry> shipping = List();
    shipping.add(Entry(
      index: ++count,
      title: 'Como devo enviar o produto?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'A ',
          style: text,
          children: [
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' não realiza envio de produtos e não se responsabiliza pelo mesmo. A forma de envio deve ser acordada entre compradores e vendedores. Por questões de segurança, recomendamos a utilização de ferramentas confiáveis como Correios ou empresas de logística. Caso a entrega seja em mãos, recomendamos que o encontro seja em locais públicos.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title: 'Em quanto tempo preciso enviar o produto?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Você terá até 3 (três) dias para realizar o envio do produto.',
          style: text,
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title: 'Enviei meu produto e agora?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'No seu perfil, acesse ',
          style: text,
          children: [
            TextSpan(
              text: '',
              style: TextStyle(
                fontFamily: 'comoo_icons_novo',
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: ' a opção ',
              style: text,
            ),
            TextSpan(
              text: '\'Minhas Vendas\'',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToSellHistory),
            ),
            TextSpan(
              text: ' e selecione a opção \'Entregou\'. A ',
              style: text,
            ),
            TextSpan(
              text: 'COMOO',
              style: text.copyWith(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
                  ' está baseada no conceito de comoonidade e acredita na relação de confiança e proximidade entre os usuários. Porém, há momentos em que problemas podem surgir. Então, recomendamos que você não entregue o produto antes do pagamento ser efetuado e estar refletido dentro do aplicativo. Mesmo que o comprador informe que já realizou o pagamento por cartão, boleto ou transferência, aguarde a confirmação do sistema.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title:
          'Entreguei meu produto, mas o comprador ainda não confirmou o recebimento',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'No seu perfil, acesse ',
          style: text,
          children: [
            TextSpan(
              text: COMOO.icons.sellings.unicode,
              style: TextStyle(
                fontFamily: COMOO.icons.sellings.fontFamily,
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: 'a opção ',
              style: text,
            ),
            TextSpan(
              text: '\'Minhas Vendas\'',
              style: text.copyWith(fontWeight: FontWeight.bold),
              recognizer: _onTap(_navigateToSellHistory),
            ),
            TextSpan(
              text:
                  ', clique no produto em questão e entre em contato com o comprador através do chat ',
              style: text,
            ),
            TextSpan(
              text: COMOO.icons.chat.unicode,
              style: TextStyle(
                fontFamily: COMOO.icons.chat.fontFamily,
                color: ComooColors.turqueza,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            TextSpan(
              text: '\n',
            ),
            TextSpan(
              text:
                  'Caso não obtenha reposta, entre em contato conosco através do email ',
              style: text,
            ),
            TextSpan(
              text: 'suporte@comoo.io.',
              style: text.copyWith(
                color: COMOO.colors.medianGray,
                fontWeight: FontWeight.bold,
              ),
              recognizer: _onTap(_copySupportEmail),
            ),
          ],
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title:
          'O comprador pediu para experimentar ou ver o produto antes do pagamento. O que devo fazer?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Para garantir a segurança de todos os usuários, nenhum produto deve ser entregue antes da conclusão do pagamento.',
          style: text,
          children: [
            TextSpan(
              text: '\n',
            ),
            TextSpan(
              text:
                  'Após a entrega, o usuário tem 7 dias corridos para solicitar a devolução do produto e o cancelamento da compra.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title:
          'Quem é o responsável pela embalagem para envio ou devolução de produtos?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'A responsabilidade sobre a embalagem é sempre do remetente, ou seja, de quem está enviando o produto.',
          style: text,
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title: 'Preciso de ajuda. Como posso entrar em contato?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Ficaremos felizes em ajudar. Entre em contato conosco através do email ',
          style: text,
          children: [
            TextSpan(
              text: 'suporte@comoo.io',
              style: text.copyWith(
                color: COMOO.colors.medianGray,
                fontWeight: FontWeight.bold,
              ),
              recognizer: _onTap(_copySupportEmail),
            ),
          ],
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title:
          'Desejo reportar problemas na aplicaç��o. Como posso entrar em contato?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Seu feedback é muito importante para nós. Entre em contato conosco através do email ',
          style: text,
          children: [
            TextSpan(
              text: 'suporte@comoo.io',
              style: text.copyWith(
                color: COMOO.colors.medianGray,
                fontWeight: FontWeight.bold,
              ),
              recognizer: _onTap(_copySupportEmail),
            ),
            TextSpan(
              text: ' que encaminharemos para nosso suporte técnico.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title: 'Como cancelo minha conta?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text: 'Entre em contato conosco através do email ',
          style: text,
          children: [
            TextSpan(
              text: 'suporte@comoo.io',
              style: text.copyWith(
                color: COMOO.colors.medianGray,
                fontWeight: FontWeight.bold,
              ),
              recognizer: _onTap(_copySupportEmail),
            ),
            TextSpan(
              text: ' que encaminharemos para nosso suporte técnico.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title: 'Quais itens não posso vender na COMOO?',
      content: RichText(
        textAlign: textAlign,
        text: TextSpan(
          text:
              'Na COMOO você pode comprar e vender (quase) tudo. O que tem que ficar de fora:',
          style: text,
          children: [
            TextSpan(
              text: '\n\n',
              style: text,
            ),
            TextSpan(
              text: 'Tabaco e produtos relacionados\n',
              style: text,
            ),
            TextSpan(
              text: 'Produtos falsificados, roubados ou danificados\n',
              style: text,
            ),
            TextSpan(
              text: 'Armas, munições e material explosivo\n',
              style: text,
            ),
            TextSpan(
              text: 'Máquinas sem equipamentos de segurança\n',
              style: text,
            ),
            TextSpan(
              text: 'Narcóticos e substâncias proibidas\n',
              style: text,
            ),
            TextSpan(
              text: 'Medicamentos\n',
              style: text,
            ),
            TextSpan(
              text: 'Ossos, órgãos e membros humanos\n',
              style: text,
            ),
            TextSpan(
              text: 'Fogos de artifício\n',
              style: text,
            ),
            TextSpan(
              text: 'Produtos e serviços financeiros\n',
              style: text,
            ),
            TextSpan(
              text: 'Listas de e-mail e base de dados pessoais\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Conteúdos relacionados à pornografia, eróticos ou inadequados para menores\n',
              style: text,
            ),
            TextSpan(
              text: 'Violência e discriminação\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Produtos que violem as leis sobre Propriedade Intelectual\n',
              style: text,
            ),
            TextSpan(
              text: 'Documentos legais e pessoais\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Patrimônio histórico, cultural, arqueológico e paleontológico\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Programas ou serviços para hackear dispositivos eletrônicos\n',
              style: text,
            ),
            TextSpan(
              text: 'Loterias e rifas\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Anúncios com finalidade diferente a venda de um produto ou serviço\n',
              style: text,
            ),
            TextSpan(
              text: 'Produtos beneficentes\n',
              style: text,
            ),
            TextSpan(
              text:
                  'Produtos que dependem de homologação/ aprovação de órgãos governamentais \n',
              style: text,
            ),
            TextSpan(
              text:
                  'Veículos sem documentação e produtos para veículos que infrinjam leis vigentes \n',
              style: text,
            ),
            TextSpan(
              text: 'Inseticidas\n',
              style: text,
            ),
            TextSpan(
              text: 'Cerol\n',
              style: text,
            ),
          ],
        ),
      ),
    ));
    shipping.add(Entry(
      index: ++count,
      title: 'Como posso reportar um comportamento criminoso e/ou inadequado?',
      content: RichText(
        text: TextSpan(
          text:
              'A COMOO confia em seus usuários e conta com a ajuda de todos para '
              'tornar nossa comunidade cada vez mais segura e saudável para todos. Envie '
              'um email para ',
          style: text,
          children: <TextSpan>[
            TextSpan(
              text: 'suporte@comoo.io',
              style: text.copyWith(
                color: COMOO.colors.medianGray,
                fontWeight: FontWeight.bold,
              ),
              recognizer: _onTap(_copySupportEmail),
            ),
            TextSpan(
              text:
                  ' para denunciar comportamentos inadequados ou ilícitos como '
                  'pornografia, incitação ao ódio, racismo, venda de produtos ilegais, '
                  'conteúdo explícito ou violento, assédio, bullying entre outros.',
              style: text,
            ),
          ],
        ),
      ),
    ));
    return shipping;
  }
}

class Entry {
  Entry({this.index, this.title, this.content});

  int index;
  String title;
  Widget content;
}

class TileItem extends StatelessWidget {
  const TileItem(this.tile, {this.controller});

  final Tile tile;
  final ScrollController controller;

  Widget _buildTile(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    List<Widget> children = List();
    TextStyle sectionStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      color: COMOO.colors.pink,
      fontWeight: FontWeight.bold,
    );
    TextStyle entryStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      color: COMOO.colors.lead,
      fontWeight: FontWeight.bold,
    );
    // children.add(Padding(
    //   padding: EdgeInsets.only(left: mediaQuery * 15.0),
    //   child: new Text(
    //     tile.title,
    //     style: TextStyle(
    //       fontFamily: ComooStyles.fontFamily,
    //       color: ComooColors.pink,
    //       fontWeight: FontWeight.bold,
    //     ),
    //   ),
    // ));

    tile.entries.forEach((entry) {
      children.add(Padding(
        padding: EdgeInsets.only(
          top: mediaQuery * 10.0,
          bottom: mediaQuery * 10.0,
        ),
        child: Theme(
          data: Theme.of(context),
          child: ExpansionTile(
            key: PageStorageKey<int>(entry.index),
            title: Text(
              '${entry.index}) ${entry.title}',
              style: entryStyle,
            ),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  bottom: mediaQuery * 20.0,
                  left: mediaQuery * 14.0,
                  right: mediaQuery * 10.0,
                  top: mediaQuery * 15.0,
                ),
                child: entry.content,
              )
            ],
          ),
        ),
      ));
    });
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.white),
          child: ExpansionTile(
            initiallyExpanded: true,
            key: PageStorageKey<String>(tile.title),
            title: Padding(
              padding: EdgeInsets.only(
                left: mediaQuery * 15.0,
                top: mediaQuery * 10.0,
                bottom: mediaQuery * 10.0,
              ),
              child: new Text(
                tile.title,
                style: sectionStyle,
              ),
            ),
            children: children,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: _buildTile(context),
    );
  }
}

class Tile {
  Tile({this.title, this.entries});
  String title;
  List<Entry> entries;
}
