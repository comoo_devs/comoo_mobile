import '../help/faq_content.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';

class FAQPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FAQPageState();
}

class _FAQPageState extends State<FAQPage> with FAQContent {
  ScrollController scrollController = ScrollController();

  _buildSnapshotData(AsyncSnapshot<List> snapshot) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    // print("${snapshot.hasData}");

    return Container(
      // padding: EdgeInsets.only(top: mediaQuery * 20.0),
      child: ListView.builder(
        padding: EdgeInsets.only(top: mediaQuery * 20.0),
        itemBuilder: (BuildContext context, int index) =>
            TileItem(snapshot.data[index], controller: scrollController),
        itemCount: snapshot.data.length,
        controller: scrollController,
      ),
    );
  }

  _buildEmptyList() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Padding(
      padding: EdgeInsets.all(mediaQuery * 16.0),
      child: Center(
          child: RichText(
              text: TextSpan(
        text: "Falha ao carregar informações, tente novamente mais tarde.\n",
        children: <TextSpan>[
          TextSpan(
              style: ComooStyles.welcomeMessage,
              text: "Se o problema persistir, contacte o suporte!")
        ],
        style: ComooStyles.welcomeMessageBold,
      ))),
    );
  }

  Stream<List> _fetchData() {
    return Stream.fromIterable(
        [getFAQContent(context, controller: scrollController)]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text("Ajuda (FAQ)"),
      ),
      body: StreamBuilder<List>(
        stream: _fetchData(),
        builder: (_, AsyncSnapshot<List> snapshot) {
          // print("${snapshot.connectionState} - has data? ${snapshot.hasData}");
          // return _buildEmptyList();
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              if (snapshot.hasData) return _buildSnapshotData(snapshot);
              return _buildEmptyList();
              break;
            case ConnectionState.done:
              if (snapshot.hasData) return _buildSnapshotData(snapshot);
              return _buildEmptyList();
              break;
            case ConnectionState.none:
              return _buildEmptyList();
              break;
            case ConnectionState.waiting:
              if (snapshot.hasData) return _buildSnapshotData(snapshot);
              return LoadingContainer();
              break;
            default:
              return Container();
              break;
          }
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }
}
