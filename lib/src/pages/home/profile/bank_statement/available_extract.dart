import 'package:comoo/src/pages/common/withdraw_page.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AvailableExtract extends StatefulWidget {
  @override
  _AvailableExtractState createState() => _AvailableExtractState();
}

class _AvailableExtractState extends State<AvailableExtract> {
  final NetworkApi api = NetworkApi();
  final NumberFormat formater =
      NumberFormat.currency(locale: 'pt_BR', symbol: '');
  EdgeInsetsGeometry marginTableContent = EdgeInsets.only(
    top: 20.0,
    bottom: 10.0,
  );

  Widget _buildSnapshotData(AsyncSnapshot snapshot) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    List<ExtractProduct> data = (snapshot.data as List)
        .map((item) => ExtractProduct.fromSnapshot(item))
        .toList();
    // data.add( ExtractProduct.fromSnapshot({
    //   'name': 'Product 1',
    //   'price': 15.0,
    //   'date': DateTime.now(),
    // }));
    // data.add( ExtractProduct.fromSnapshot({
    //   'name': 'Product 2',
    //   'price': 105.0,
    //   'date': DateTime.now(),
    // }));
    if (data.isEmpty) {
      return _buildEmptyData();
    }

    TextStyle tabRowStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 13.0,
      fontWeight: FontWeight.w500,
    );
    List<TableRow> tableChildren = List();
    tableChildren.add(TableRow(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: mediaQuery * 1.2,
          ),
        ),
      ),
      children: [
        Container(
          margin: marginTableContent,
          child: Text(
            'ORIGEM',
            style: ComooStyles.texto_bold.apply(
              fontSizeFactor: mediaQuery * 0.8,
            ),
          ),
        ),
        Container(
          margin: marginTableContent,
          child: FittedBox(
            child: Text(
              'TRANSAÇÃO',
              style: ComooStyles.texto_bold.apply(
                fontSizeFactor: mediaQuery * 0.8,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Container(
          margin: marginTableContent,
          child: Text(
            'VALOR',
            style: ComooStyles.texto_bold.apply(
              fontSizeFactor: mediaQuery * 0.8,
            ),
            textAlign: TextAlign.end,
          ),
        ),
      ],
    ));
    data.forEach((extract) {
      tableChildren.add(TableRow(
        children: [
          Container(
            margin: marginTableContent,
            child: Text(
              extract.name,
              style: tabRowStyle,
            ),
          ),
          Container(
            margin: marginTableContent,
            child: Text(
              '${extract.date}',
              style: tabRowStyle,
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: marginTableContent,
            child: Text(
              formater.format(extract.price),
              style: tabRowStyle,
              textAlign: TextAlign.end,
            ),
          ),
        ],
      ));
    });
    return ListView(
      children: <Widget>[
        Table(
          border: TableBorder(
              horizontalInside: BorderSide(width: mediaQuery * 0.4),
              bottom: BorderSide(width: mediaQuery * 0.4)),
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          columnWidths: {
            0: FractionColumnWidth(mediaQuery * 0.6),
            1: FractionColumnWidth(mediaQuery * 0.2),
            2: FractionColumnWidth(mediaQuery * 0.2),
          },
          children: tableChildren,
        ),
      ],
    );
  }

  Widget _buildEmptyData() {
    return Center(
      child: Container(
        child: Text('Você ainda não tem saldo disponível.'),
      ),
    );
  }

  Stream _fetchData() {
    return api.getCurrentBankStatement().asStream();
    // return (CloudFunctions.instance.call(
    //   functionName: 'getCurrentBankStatement',
    //   parameters: {},
    // ).catchError((onError) {
    //   print(onError);
    //   return List();
    // }))
    //     .asStream();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(
          top: mediaQuery * 10.0,
          left: mediaQuery * 20.0,
          right: mediaQuery * 20.0,
        ),
        child: Center(
          child: StreamBuilder(
            stream: _fetchData(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              // print('${snapshot.connectionState} ${snapshot.hasData}');
              switch (snapshot.connectionState) {
                case ConnectionState.active:
                  if (snapshot.hasData) return _buildSnapshotData(snapshot);
                  return _buildEmptyData();
                  break;
                case ConnectionState.done:
                  if (snapshot.hasData) return _buildSnapshotData(snapshot);
                  return _buildEmptyData();
                  break;
                case ConnectionState.none:
                  return _buildEmptyData();
                  break;
                case ConnectionState.waiting:
                  if (snapshot.hasData) return _buildSnapshotData(snapshot);
                  return LoadingContainer();
                  break;
                default:
                  return Container();
                  break;
              }
            },
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: mediaQuery * 50.0,
        color: Color(0xFFD8D8D8),
        child: Center(
          child: Container(
            width: mediaQuery * 252.0,
            height: mediaQuery * 35.0,
            child: RaisedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute<void>(
                    builder: (BuildContext context) => WithdrawPage()));
              },
              elevation: 0.0,
              color: Color(0xFF471D58),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20),
              ),
              child: Text(
                'RESGATAR',
                style: TextStyle(
                  fontFamily: COMOO.fonts.montSerrat,
                  color: Colors.white,
                  fontSize: mediaQuery * 15.0,
                  letterSpacing: mediaQuery * 0.6,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ExtractProduct {
  String name;
  double price;
  String date;

  ExtractProduct({this.name, this.price, this.date});

  ExtractProduct.fromSnapshot(snap)
      : name = snap['name'],
        price = snap['price'] + 0.0,
        date = snap['date'];
}
