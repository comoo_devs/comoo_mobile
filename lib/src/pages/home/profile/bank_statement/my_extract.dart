import 'package:comoo/api/network.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import 'available_extract.dart';
import 'receive_extract.dart';
import 'total_extract.dart';
import 'package:intl/intl.dart';

class MyExtract extends StatefulWidget {
  @override
  _MyExtractState createState() => _MyExtractState();
}

class _MyExtractState extends State<MyExtract>
    with SingleTickerProviderStateMixin {
  final NetworkApi api = NetworkApi();
  TabController _tabController;
  bool _isLoadingBalance = false;
  // String _balanceText = 'TOTAL DISPONÍVEL';
  double _current = 0.0;
  double _future = 0.0;
  final NumberFormat formater =
      NumberFormat.currency(locale: 'pt_BR', symbol: 'R\$');

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);

    _tabController.addListener(() {
      // print('Listener ${_tabController.indexIsChanging}');
      // print('${_tabController.index}');
      if (this.mounted) {
        setState(() {});
      }
    });
    _fetchBalance();
  }

  _fetchBalance() async {
    setState(() {
      _isLoadingBalance = true;
    });
    final dynamic result = await api.getAllBalance();
    if (result != null) {
      var curr = result['current'];
      var fut = result['future'];
      setState(() {
        _current = curr['amount'] * 1.0;
        _future = fut['amount'] * 1.0;
        _isLoadingBalance = false;
      });
    }
    // final result = await CloudFunctions.instance
    //     .call(functionName: 'getBalance')
    //     .timeout(Duration(seconds: 60))
    //     .then((result) {
    //   print(result);
    //   var curr = result['current'];
    //   var fut = result['future'];
    //   setState(() {
    //     _current = curr['amount'] * 1.0;
    //     _future = fut['amount'] * 1.0;
    //     _isLoadingBalance = false;
    //   });
    //   return result;
    // }).catchError((error) {
    //   // print(error);
    //   setState(() {
    //     _current = 0.0;
    //     _future = 0.0;
    //     _isLoadingBalance = false;
    //   });
    //   var errorMessage = {
    //     'current': {'amount': 0, 'currency': 'BRL'},
    //     'future': {'amount': 0, 'currency': 'BRL'},
    //   };
    //   return errorMessage;
    // });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    // TextStyle balanceStyle = TextStyle(fontSize: mediaQuery * 24.0);
    // _balance = (_current + _future) / 100;
    return Scaffold(
      appBar: CustomAppBar.round(
        color: COMOO.colors.lead,
        title: Text('Meu extrato', style: ComooStyles.appBarTitleSixteen),
      ),
      body: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: mediaQuery * 20.0),
                child: Material(
                  color: Colors.white,
                  child: TabBar(
                    indicatorWeight: mediaQuery * 0.01,
                    indicatorColor: Colors.transparent,
                    controller: _tabController,
                    unselectedLabelStyle: TextStyle(
                      fontFamily: ComooStyles.fontFamily,
                      color: ComooColors.chumbo,
                      fontSize: mediaQuery * 13.0,
                      fontWeight: FontWeight.w500,
                      letterSpacing: mediaQuery * 0.52,
                    ),
                    labelStyle: TextStyle(
                      fontFamily: ComooStyles.fontFamily,
                      color: ComooColors.chumbo,
                      fontSize: mediaQuery * 13.0,
                      fontWeight: FontWeight.w600,
                      letterSpacing: mediaQuery * 0.52,
                    ),
                    tabs: <Widget>[
                      CustomPaint(
                        painter:
                            _tabController.index == 0 ? TabPainter() : null,
                        child: Tab(
                          text: 'DISPONÍVEL',
                        ),
                      ),
                      CustomPaint(
                        painter:
                            _tabController.index == 1 ? TabPainter() : null,
                        child: Tab(
                          text: 'A RECEBER',
                        ),
                      ),
                      CustomPaint(
                        painter:
                            _tabController.index == 2 ? TabPainter() : null,
                        child: Tab(
                          text: 'TOTAL',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: mediaQuery * 20.0,
                ),
                color: ComooColors.turqueza,
                height: mediaQuery * 55.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      _getBalanceTitle(),
                      style: TextStyle(
                        fontFamily: ComooStyles.fontFamily,
                        color: Colors.white,
                        fontSize: mediaQuery * 13.0,
                        fontWeight: FontWeight.w500,
                        letterSpacing: mediaQuery * 0.52,
                      ),
                    ),
                    _isLoadingBalance
                        ? Container(
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  ComooColors.roxo),
                            ),
                          )
                        : Text(
                            _getBalance(),
                            style: ComooStyles.containerTurquezaBold,
                          )
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    AvailableExtract(),
                    ReceiveExtract(),
                    TotalExtract(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getBalanceTitle() {
    String text = '';
    switch (_tabController.index) {
      case 0:
        text = 'TOTAL DISPONÍVEL';
        break;
      case 1:
        text = 'TOTAL A RECEBER';
        break;
      case 2:
        text = 'SALDO TOTAL';
        break;
    }
    return text;
  }

  _getBalance() {
    double balance = 0.0;
    switch (_tabController.index) {
      case 0:
        balance = _current / 100;
        break;
      case 1:
        balance = _future / 100;
        break;
      case 2:
        balance = (_current + _future) / 100;
        break;
    }
    return formater.format(balance);
  }
}

class TabPainter extends CustomPainter {
  TabPainter({
    this.color,
  }) : super();
  final Color color;
  @override
  void paint(Canvas canvas, Size size) {
    final double height = size.height;
    final double width = size.width;
    final Paint paint = Paint();

    final Path triangle = Path()
      ..addPolygon(<Offset>[
        Offset((width / 2) - 11.0, height + height * 0.3 / 3),
        Offset(width / 2, height - 10.0),
        Offset((width / 2) + 11.0, height + height * 0.3 / 3),
      ], true);
    paint.color = color ?? COMOO.colors.turquoise;
    canvas.drawPath(triangle, paint);
  }

  @override
  bool shouldRepaint(TabPainter oldDelegate) => oldDelegate != this;
}
