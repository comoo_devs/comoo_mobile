// import 'dart:convert';

import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ReceiveExtract extends StatefulWidget {
  @override
  _ReceiveExtractState createState() => _ReceiveExtractState();
}

class _ReceiveExtractState extends State<ReceiveExtract> {
  final NetworkApi api = NetworkApi();
  final NumberFormat formater =
      NumberFormat.currency(locale: 'pt_BR', symbol: '');
  EdgeInsetsGeometry marginTableContent = EdgeInsets.only(
    top: 20.0,
    bottom: 10.0,
  );

  Widget _buildSnapshotData(AsyncSnapshot snapshot) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    // var obj = json.decode((snapshot.data as List).first);
    // print((snapshot.data as List).first);
    List<ExtractProduct> data = (snapshot.data as List)
        .map((item) => ExtractProduct.fromSnapshot(item))
        .toList();
    // data.add(ExtractProduct.fromSnapshot({
    //   'name': 'Product 1',
    //   'price': 15.0,
    //   'date': '-',
    // }));
    // data.add(new ExtractProduct.fromSnapshot({
    //   'name': 'Product 2',
    //   'price': 105.0,
    //   'date': DateTime.now(),
    // }));
    if (data.isEmpty) {
      return _buildEmptyData();
    }

    TextStyle tabRowStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 13.0,
      fontWeight: FontWeight.w500,
    );
    List<TableRow> tableChildren = List();
    List<TableRow> tableHeader = List();
    tableHeader.add(new TableRow(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: mediaQuery * 1.2,
          ),
        ),
      ),
      children: [
        Container(
          margin: marginTableContent,
          child: Text(
            'ORIGEM',
            style: ComooStyles.texto_bold.apply(
              fontSizeFactor: mediaQuery * 0.8,
            ),
          ),
        ),
        Container(
          margin: marginTableContent,
          child: Text(
            'LIBERAÇÃO',
            style: ComooStyles.texto_bold.apply(
              fontSizeFactor: mediaQuery * 0.8,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          margin: marginTableContent,
          child: Text(
            'VALOR',
            style: ComooStyles.texto_bold.apply(
              fontSizeFactor: mediaQuery * 0.8,
            ),
            textAlign: TextAlign.right,
          ),
        ),
      ],
    ));
    tableChildren.add(tableHeader[0]);
    data.forEach((extract) {
      tableChildren.add(TableRow(
        children: [
          Container(
            margin: marginTableContent,
            child: Text(
              extract.name,
              style: tabRowStyle,
            ),
          ),
          Container(
            margin: marginTableContent,
            child: Text(
              'até ${extract.date}',
              textAlign: TextAlign.center,
              style: tabRowStyle,
            ),
          ),
          Container(
            margin: marginTableContent,
            child: Text(
              formater.format(extract.price),
              style: tabRowStyle,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ));
    });
    return ListView(
      children: [
        Table(
          border: TableBorder(
            horizontalInside: BorderSide(width: mediaQuery * 0.4),
            bottom: BorderSide(width: mediaQuery * 0.4),
          ),
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          columnWidths: {
            0: FractionColumnWidth(mediaQuery * 0.6),
            1: FractionColumnWidth(mediaQuery * 0.2),
            2: FractionColumnWidth(mediaQuery * 0.2),
          },
          children: tableChildren,
        ),
      ],
    );
    // return ListView(
    //   children: <Widget>[
    //     Container(
    //       child: Table(
    //         border: TableBorder(
    //           horizontalInside: BorderSide(width: mediaQuery * 0.4),
    //           bottom: BorderSide(width: mediaQuery * 0.4),
    //         ),
    //         defaultVerticalAlignment: TableCellVerticalAlignment.middle,
    //         columnWidths: {
    //           0: FractionColumnWidth(mediaQuery * 0.6),
    //           1: FractionColumnWidth(mediaQuery * 0.2),
    //           2: FractionColumnWidth(mediaQuery * 0.2),
    //         },
    //         children: tableHeader,
    //       ),
    //     ),
    //     Container(
    //       constraints: BoxConstraints(
    //         // maxHeight: (MediaQuery.of(context).size.height / 2) - 10.0,
    //         maxHeight: MediaQuery.of(context).size.height,
    //       ),
    //       child: ListView(
    //         children: [
    //           Table(
    //             border: TableBorder(
    //               horizontalInside: BorderSide(width: mediaQuery * 0.4),
    //               bottom: BorderSide(width: mediaQuery * 0.4),
    //             ),
    //             defaultVerticalAlignment: TableCellVerticalAlignment.middle,
    //             columnWidths: {
    //               0: FractionColumnWidth(mediaQuery * 0.6),
    //               1: FractionColumnWidth(mediaQuery * 0.2),
    //               2: FractionColumnWidth(mediaQuery * 0.2),
    //             },
    //             children: tableChildren,
    //           ),
    //         ],
    //       ),
    //     ),
    // Container(
    //   margin: EdgeInsets.symmetric(
    //     vertical: mediaQuery * 10.0,
    //   ),
    //   padding: EdgeInsets.symmetric(
    //     horizontal: mediaQuery * 30.0,
    //   ),
    // child: OutlineButton(
    //   onPressed: () {
    //     Navigator.of(context).push(
    //       MaterialPageRoute(
    //         builder: (context) => AnticipatePaymentPage(),
    //       ),
    //     );
    //   },
    //   child: Text(
    //     'ANTECIPAR RECEBIMENTO',
    //     style: ComooStyles.botaoTurquezaChat,
    //   ),
    //   borderSide: BorderSide(color: ComooColors.turqueza),
    //   shape: RoundedRectangleBorder(
    //     borderRadius: BorderRadius.circular(mediaQuery * 20.0),
    //   ),
    // ),
    //     ),
    //   ],
    // );
  }

  Widget _buildEmptyData() {
    return Center(
      child: Container(
        child: Text('Você não tem saldo a receber.'),
      ),
    );
  }

  Stream _fetchData() {
    // final Future result = CloudFunctions.instance.call(
    //   functionName: 'getFutureBankStatement',
    //   parameters: {},
    // ).catchError((onError) {
    //   print('getFutureBankStatement $onError');
    //   return List();
    // });
    return api.getFutureBankStatement().asStream();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(
          top: mediaQuery * 10.0,
          left: mediaQuery * 20.0,
          right: mediaQuery * 20.0,
        ),
        child: Center(
          child: StreamBuilder(
            stream: _fetchData(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              // print('${snapshot.connectionState} ${snapshot.hasData}');
              switch (snapshot.connectionState) {
                case ConnectionState.active:
                  if (snapshot.hasData) return _buildSnapshotData(snapshot);
                  return Container();
                  break;
                case ConnectionState.done:
                  if (snapshot.hasData) return _buildSnapshotData(snapshot);
                  return Container();
                  break;
                case ConnectionState.none:
                  return Container();
                  break;
                case ConnectionState.waiting:
                  if (snapshot.hasData) return _buildSnapshotData(snapshot);
                  return LoadingContainer();
                  break;
                default:
                  return Container();
                  break;
              }
            },
          ),
        ),
      ),
    );
  }
}

class ExtractProduct {
  String name;
  double price;
  String date;

  ExtractProduct({this.name, this.price, this.date});

  ExtractProduct.fromSnapshot(snap)
      : name = snap['name'],
        price = snap['price'] * 1.0,
        date = snap['date'];
}
