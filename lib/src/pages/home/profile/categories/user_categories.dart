import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/categories_model.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';

class UserCategoriesPage extends StatefulWidget {
  @override
  _UserCategoriesPageState createState() => _UserCategoriesPageState();
}

class _UserCategoriesPageState extends State<UserCategoriesPage> {
  final GlobalKey _scaffoldKey = GlobalKey<ScaffoldState>();
  List<CategoriesModel> categoryList = List();
  bool _isLoading = false;
  final Firestore db = Firestore.instance;
  String loadingMessage = "Carregando suas preferências...";

  void showInSnackBar(String value) {
    // _scaffoldKey.currentState.showSnackBar(new SnackBar(
    //     content: Text(value, style: ComooStyles.textoSnackBar)));
  }

  Future<Null> _setUserPersonalInformation(
      String uid, List<CategoriesModel> categorySelected) async {
    WriteBatch batch = db.batch();

    final DocumentReference userDocRef = db.collection('users').document(uid);

    batch.updateData(userDocRef, <String, dynamic>{
      'categories': categorySelected.map((c) => c.id).toList(),
    });

    return batch.commit();
  }

  _handleSubmit() async {
    setState(() {
      loadingMessage = "Salvando suas preferências...";
      _isLoading = true;
    });

    List categoriesSelected =
        categoriesModel.where((c) => c.isSelected == true).toList();

    categoriesSelected.forEach((cat) => print(cat.title));

    try {
      String uid = currentUser.uid;
      if (uid != null || uid != "") {
        await _setUserPersonalInformation(uid, categoriesSelected);

        setState(() {
          _isLoading = false;
        });
      } else {
        showInSnackBar('Algo deu errado, tente novamente.');

        setState(() {
          _isLoading = false;
        });
      }

      setState(() {
        _isLoading = false;
      });

      Navigator.of(context).pop();
    } catch (error) {
      showInSnackBar('Algo deu errado, tente novamente.');
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text('CATEGORIAS', style: ComooStyles.appBarTitleSixteen),
        actions: <Widget>[
          FlatButton(
            onPressed: _handleSubmit,
            child: Text(
              "SALVAR",
              style: ComooStyles.appBar_botao_branco,
            ),
          ),
        ],
      ),
      body: _isLoading
          ? LoadingContainer(message: loadingMessage)
          : Container(
              margin: EdgeInsets.symmetric(horizontal: mediaQuery * 21.0),
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverToBoxAdapter(
                    child: Container(
                      margin: EdgeInsets.only(
                        top: mediaQuery * 25.0,
                        bottom: mediaQuery * 21,
                      ),
                      child: Text(
                        "Escolha as categorias do seu interesse:",
                        style: TextStyle(
                          fontFamily: ComooStyles.fontFamily,
                          color: ComooColors.turqueza,
                          fontSize: mediaQuery * 15.0,
                          fontWeight: FontWeight.w600,
                          letterSpacing: mediaQuery * -0.14,
                        ),
                      ),
                    ),
                  ),
                  SliverGrid(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: mediaQuery * 28.0,
                      crossAxisSpacing: mediaQuery * 2.0,
                      childAspectRatio: mediaQuery * 0.90,
                    ),
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int i) {
                        return Container(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                categoryList[i].isSelected =
                                    !categoryList[i].isSelected;
                                // print(newUser.uid);
                                // if (categoryList[i].isSelected == true) {
                                //   print('${categoryList[i].title} is selected');
                                // } else {
                                //   print('${categoryList[i].title} is unselected');
                                // }
                              });
                            },
                            child: Center(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    width: mediaQuery * 101.0,
                                    height: mediaQuery * 99.0,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Icon(
                                          categoryList[i].iconData,
                                          size: mediaQuery * 96.0,
                                          color: ComooColors.turqueza,
                                        ),
                                        Positioned(
                                          right: 0.0,
                                          top: 0.0,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.white,
                                            ),
                                            child: Image.asset(
                                              !categoryList[i].isSelected
                                                  ? "images/add_white.png"
                                                  : "images/checked_turquoise.png",
                                              width: mediaQuery * 26.0,
                                              height: mediaQuery * 26.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.only(top: mediaQuery * 5.0),
                                    child: Text(
                                      categoryList[i].title,
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                      style: ComooStyles.categoriesBold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      childCount: categoryList.length,
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: mediaQuery * 32.0),
                      alignment: Alignment.center,
                      child: OutlineButton(
                        padding: EdgeInsets.symmetric(
                          horizontal: mediaQuery * 43.0,
                          vertical: mediaQuery * 8.0,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(mediaQuery * 20.0),
                        ),
                        borderSide: BorderSide(
                          color: ComooColors.roxo,
                        ),
                        highlightElevation: mediaQuery * 10.0,
                        child: Text(
                          "SALVAR",
                          style: TextStyle(
                            color: ComooColors.roxo,
                            fontFamily: ComooStyles.fontFamily,
                            fontWeight: FontWeight.normal,
                            fontSize: mediaQuery * 15.0,
                            letterSpacing: mediaQuery * 0.6,
                          ),
                        ),
                        onPressed: _handleSubmit,
                      ),
                    ),
                  )
                ],
              ),
            ),
    );
  }

  _fetchCurrentUserCategories() async {
    String uid = currentUser.uid;
    print(uid);
    if (uid != null && uid != "") {
      final result = await db.collection("users").document(uid).get();
      if (result['categories'] != null) {
        List<String> categories = List<String>.from(result['categories']);
        categoryList.forEach((categoryModel) {
          categoryModel.isSelected = categories.contains(categoryModel.id);
        });
      }
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    _fetchCurrentUserCategories();
    categoryList = categoriesModel;
  }
}
