import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/pages/common/user/user_detail.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter_advanced_networkimage/provider.dart';

class SharedProfitPage extends StatefulWidget {
  @override
  State createState() {
    return _SharedProfitPageState();
  }
}

class _SharedProfitPageState extends State<SharedProfitPage> {
  final GlobalKey<ScaffoldState> _scaffolKey = GlobalKey<ScaffoldState>();
  final Firestore _db = Firestore.instance;
  StreamController<List<UserSharedProfit>> streamController =
      StreamController();

  _fetchSharedProfitUsers() async {
    List<UserSharedProfit> sharedProfit = List();
    await _db
        .collection("users")
        .where("originator", isEqualTo: currentUser.uid)
        .getDocuments()
        .then((sons) {
      int length = sons.documents.length;
      print(length);
      int counter = 0;
      if (length == 0) {
        streamController.close();
        return;
      }
      sons.documents.forEach((son) async {
        counter++;
        sharedProfit.add(UserSharedProfit.fromSnapshot(son, 0));
        await _db
            .collection("users")
            .where("originator", isEqualTo: son.documentID)
            .getDocuments()
            .then((grandSons) {
          grandSons.documents.forEach((grandSon) {
            sharedProfit.add(UserSharedProfit.fromSnapshot(grandSon, 1));
          });
        });

        streamController.sink.add(sharedProfit);
        if (counter == length) {
          streamController.close();
        }
      });
    });
  }

  _openUserInfo(userId) async {
    DocumentSnapshot snap =
        await _db.collection('users').document(userId).get();
    User user = User.fromSnapshot(snap);

    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => UserDetailView(user)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffolKey,
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text('Lucro compartilhado'),
      ),
      body: _buildSharedProfitStream(context),
    );
  }

  _buildEmptyList() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Padding(
      padding: EdgeInsets.all(mediaQuery * 16.0),
      child: Center(
          child: RichText(
              text: TextSpan(
        text: "Você ainda não tem amigos para compartilhar o lucro.\n",
        children: <TextSpan>[
          TextSpan(
              style: ComooStyles.welcomeMessage,
              text:
                  "Envie seu código de convite para os seus amigos se cadastrarem e ganhe dinheiro com as negociações deles!")
        ],
        style: ComooStyles.welcomeMessageBold,
      ))),
    );
  }

  _buildSharedProfitList(snapshot) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (context, index) {
        User user = snapshot.data[index].user;
        int generation = snapshot.data[index].index + 1;
        return ListTile(
          contentPadding: EdgeInsets.symmetric(
            horizontal: mediaQuery * 10.0,
          ),
          leading: GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: ComooColors.cinzaClaro),
                  borderRadius: BorderRadius.circular(mediaQuery * 30.0),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AdvancedNetworkImage(user.photoURL),
                  )),
              width: mediaQuery * 55.0,
              height: mediaQuery * 55.0,
            ),
            onTap: () {
              // _openUserInfo(user);
            },
          ),
          title: GestureDetector(
            child: Text(
              user.name,
              style: ComooStyles.productTitle,
            ),
            onTap: () {
              // _openUserInfo(user);
            },
          ),
          subtitle: GestureDetector(
            child: Text(
              user.nickname == null ? "" : "@" + user.nickname,
              style: ComooStyles.productTitlePrice,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            onTap: () {
              // _openUserInfo(user);
            },
          ),
          trailing: Container(
            padding: EdgeInsets.only(right: mediaQuery * 25.0),
            child: Text(
              generation.toString() + "°",
            ),
          ),
          onTap: () {
            _openUserInfo(user.uid);
          },
        );
      },
    );
  }

  Widget _buildSharedProfitStream(BuildContext context) {
    // return Center(child: CircularProgressIndicator());
    return StreamBuilder<List<UserSharedProfit>>(
      stream: streamController.stream,
      builder: (BuildContext context,
          AsyncSnapshot<List<UserSharedProfit>> snapshot) {
        // print("${snapshot.connectionState} - has data? ${snapshot.hasData}");
        switch (snapshot.connectionState) {
          case ConnectionState.active:
            if (snapshot.hasData) return _buildSharedProfitList(snapshot);
            return _buildEmptyList();
            break;
          case ConnectionState.done:
            if (snapshot.hasData) return _buildSharedProfitList(snapshot);
            return _buildEmptyList();
            break;
          case ConnectionState.none:
            return _buildEmptyList();
            break;
          case ConnectionState.waiting:
            if (snapshot.hasData) return _buildSharedProfitList(snapshot);
            return LoadingContainer();
            break;
          default:
            return Container();
            break;
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();

    _fetchSharedProfitUsers();
  }

  @override
  void dispose() {
    super.dispose();
    streamController.close();
  }
}

class UserSharedProfit {
  int index;
  User user;

  UserSharedProfit({
    this.index,
    this.user,
  });

  UserSharedProfit.fromSnapshot(snap, index)
      : index = index,
        user = User.fromSnapshot(snap);
}
