import 'dart:async';

//import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/widgets/dashed_circle.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/pages/common/group/group_chat/group_chat.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';

class UserGroupsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _UserGroupsPageState();
  }
}

class _UserGroupsPageState extends State<UserGroupsPage> {
  final Firestore db = Firestore.instance;
  final GroupBloc<List<Group>> bloc = GroupBloc<List<Group>>();
  int groupsQty = 0;

  Future<void> _fetchGroupStream() async {
    final User _user = currentUser;
    final List<Group> groups = <Group>[];
    await db
        .collection('users')
        .document(_user.uid)
        .collection('groups')
        .getDocuments()
        .then((QuerySnapshot query) async {
      // if (bloc.value != null) {
      //   print('>> ${groups.length}');
      //   bloc.add(groups);
      // }
      groupsQty = query.documents.length;
      for (DocumentSnapshot snapshot in query.documents) {
        await db
            .collection('groups')
            .document(snapshot.documentID)
            .get()
            .then((DocumentSnapshot groupSnap) {
          final Group group = Group.fromSnapshot(groupSnap);
          db
              .collection('groups')
              .document(snapshot.documentID)
              .collection('users')
              .getDocuments()
              .then((QuerySnapshot querySnap) {
            if (mounted) {
              setState(() {
                for (DocumentSnapshot u in querySnap.documents) {
                  group.addUserFromSnapshot(u);
                }
              });
            }
          });
          if (mounted) {
            setState(() {
              groups.add(group);
            });
          }
        });
      }
      bloc.add(groups);
    });
  }

  Widget _buildSnapshotData(AsyncSnapshot<List<Group>> snapshot) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    // print(snapshot.data.length);
    if (groupsQty == 0) {
      return _buildEmptyList();
    }
    if (snapshot.data.isEmpty) {
      return LoadingContainer();
    }
    snapshot.data.sort((Group a, Group b) => a.name.compareTo(b.name));
    return RefreshIndicator(
      onRefresh: () => _fetchGroupStream(),
      child: Scrollbar(
        child: ListView.separated(
          addAutomaticKeepAlives: true,
          separatorBuilder: (context, index) {
            return Divider(
              height: mediaQuery * 1.0,
              color: Color.fromRGBO(206, 206, 206, 0.5),
            );
          },
          padding: EdgeInsets.only(top: mediaQuery * 10.0),
          itemCount: snapshot.data.length,
          itemBuilder: (BuildContext context, int index) {
            Group group = snapshot.data[index];
            final Widget thumb = Padding(
              padding: EdgeInsets.all(mediaQuery * 4.0),
              child: CircleAvatar(
                backgroundImage: AdvancedNetworkImage(group.avatar),
                backgroundColor: Colors.white,
              ),
            );
            return Stack(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(
                    0.0,
                    mediaQuery * 5.0,
                    45.0,
                    mediaQuery * 5.0,
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Color.fromRGBO(206, 206, 206, 0.5),
                      ),
                    ),
                  ),
                  child: ListTile(
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 0.0, horizontal: mediaQuery * 10.0),
                    dense: true,
                    leading: group.userAdmin == currentUser.uid
                        ? DashedCircle(
                            color: COMOO.colors.turquoise,
                            dashes: 25,
                            child: thumb,
                          )
                        : thumb,
                    // leading: Container(
                    //   decoration: BoxDecoration(
                    //     border: group.userAdmin == currentUser.uid
                    //         ? Border.all(
                    //             color: ComooColors.turqueza,
                    //             width: mediaQuery * 3.0,
                    //           )
                    //         : null,
                    //     borderRadius: BorderRadius.circular(mediaQuery * 30.0),
                    //     image: DecorationImage(
                    //       fit: BoxFit.cover,
                    //       image: AdvancedNetworkImage(group.avatar),
                    //     ),
                    //   ),
                    //   width: mediaQuery * 55.0,
                    //   height: mediaQuery * 55.0,
                    // ),
                    title: Text(
                      group.name,
                      style: ComooStyles.productTitle,
                    ),
                    subtitle: Container(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          RichText(
                            text: TextSpan(
                              text: group.location,
                              style: TextStyle(
                                fontFamily: ComooStyles.fontFamily,
                                color: ComooColors.cinzaMedio,
                                fontSize: mediaQuery * 12.0,
                                fontWeight: FontWeight.w500,
                              ),
                              children: [
                                TextSpan(
                                    text: ' - ',
                                    style: TextStyle(
                                      fontFamily: ComooStyles.fontFamily,
                                      color: ComooColors.cinzaMedio,
                                      fontSize: mediaQuery * 12.0,
                                      fontWeight: FontWeight.w500,
                                    )),
                                TextSpan(
                                    text: group.address.city,
                                    style: TextStyle(
                                      fontFamily: ComooStyles.fontFamily,
                                      color: ComooColors.cinzaMedio,
                                      fontSize: mediaQuery * 12.0,
                                      fontWeight: FontWeight.w500,
                                    )),
                                TextSpan(
                                    text: ' - ',
                                    style: TextStyle(
                                      fontFamily: ComooStyles.fontFamily,
                                      color: ComooColors.cinzaMedio,
                                      fontSize: mediaQuery * 12.0,
                                      fontWeight: FontWeight.w500,
                                    )),
                                TextSpan(
                                    text: group.address.state,
                                    style: TextStyle(
                                      fontFamily: ComooStyles.fontFamily,
                                      color: ComooColors.cinzaMedio,
                                      fontSize: mediaQuery * 12.0,
                                      fontWeight: FontWeight.w500,
                                    )),
                              ],
                            ),
                          ),
                          Text(
                            group.description ?? '',
                            style: ComooStyles.productTitlePrice,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      _navigateToGroup(snapshot.data[index]);
                    },
                  ),
                ),
                Positioned(
                  top: 0.0,
                  right: 0.0,
                  child: (group.users == null || group.users.isEmpty)
                      ? Container()
                      : Container(
                          constraints: BoxConstraints(
                            maxWidth: mediaQuery * 58.0,
                          ),
                          child: Row(
                            children: <Widget>[
                              Text(
                                '${group.users.length}',
                                style: TextStyle(
                                  fontFamily: ComooStyles.fontFamily,
                                  color: ComooColors.cinzaMedio,
                                  fontSize: mediaQuery * 14.0,
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: mediaQuery * 0.25,
                                ),
                              ),
                              Icon(
                                ComooIcons.perfil,
                                textDirection: TextDirection.ltr,
                                color: ComooColors.chumbo,
                                size: mediaQuery * 24.0,
                              ),
                            ],
                          ),
                        ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Future<void> _navigateToGroup(Group group) async {
    await Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => GroupChatPage(group)));
    _fetchGroupStream();
  }

  Widget _buildEmptyList() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Padding(
      padding: EdgeInsets.all(mediaQuery * 16.0),
      child: Center(
          child: RichText(
              text: TextSpan(
        text: 'Você ainda não entrou nenhuma comoonidade.\n',
        children: <TextSpan>[
          TextSpan(
              style: ComooStyles.welcomeMessage,
              text: 'Entre em uma comoonidade para encontrar produtos!')
        ],
        style: ComooStyles.welcomeMessageBold,
      ))),
    );
  }

  @override
  void initState() {
    super.initState();
    _fetchGroupStream();
  }

  @override
  void dispose() {
    super.dispose();
    bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text('MINHAS COMOONIDADES'.toLowerCase()),
      ),
      body: StreamBuilder<List<Group>>(
        stream: bloc.value,
        builder: (BuildContext context, AsyncSnapshot<List<Group>> snapshot) {
          print(
              'UserGroups: ${snapshot.connectionState} - has data? ${snapshot.hasData}');
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              return snapshot.hasData
                  ? _buildSnapshotData(snapshot)
                  : _buildEmptyList();
              break;
            case ConnectionState.done:
              return snapshot.hasData
                  ? _buildSnapshotData(snapshot)
                  : _buildEmptyList();
              break;
            case ConnectionState.none:
              return _buildEmptyList();
              break;
            case ConnectionState.waiting:
              return snapshot.hasData
                  ? _buildSnapshotData(snapshot)
                  : LoadingContainer();
              break;
            default:
              return Container();
              break;
          }
        },
      ),
    );
  }
}

class GroupBloc<T> {
  StreamController<T> _groupController = StreamController<T>.broadcast();

  Function(T) get add =>
      _groupController.isClosed ? null : _groupController.sink.add;

  Stream<T> get value => _groupController.stream;

  void dispose() {
    _groupController?.close();
  }
}
