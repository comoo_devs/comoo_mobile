import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:comoo/src/widgets/tab_widget.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import './published_products.dart';
import './draft_products.dart';
// import './deleted_products.dart';

class UserProductsPage extends StatefulWidget {
  @override
  State createState() {
    return _UserProductsPageState();
  }
}

class _UserProductsPageState extends State<UserProductsPage> {
  @override
  Widget build(BuildContext context) {
    final List<TabWidget> tabs = <TabWidget>[
      TabWidget(
        tab: Tab(text: 'PUBLICADOS'),
        widget: PublishedProductsTab(),
      ),
      TabWidget(
        tab: Tab(text: 'RASCUNHOS'),
        widget: DraftProductsTab(),
      ),
      // TabWidget(
      //   tab: Tab(text: 'EXCLUÍDOS'),
      //   widget: DeletedProductsTab(),
      // ),
    ];
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
        appBar: CustomAppBar.round(
          color: ComooColors.chumbo,
          title: Text('Meus anúncios', style: ComooStyles.appBarTitleSixteen),
        ),
        body: DefaultTabController(
          length: tabs.length,
          initialIndex: 0,
          child: Column(
            children: <Widget>[
              TabBar(
                labelColor: ComooColors.turqueza,
                indicatorColor: ComooColors.turqueza,
                labelStyle: TextStyle(
                  fontFamily: ComooStyles.fontFamily,
                  color: ComooColors.turqueza,
                  fontSize: mediaQuery * 13.0,
                  fontWeight: FontWeight.w600,
                  letterSpacing: mediaQuery * 0.52,
                ),
                unselectedLabelColor: ComooColors.chumbo,
                tabs: tabs.map<Widget>((TabWidget tw) => tw.tab).toList(),
              ),
              Expanded(
                child: TabBarView(
                  children:
                      tabs.map<Widget>((TabWidget tw) => tw.widget).toList(),
                ),
              )
            ],
          ),
        ));
  }
}
