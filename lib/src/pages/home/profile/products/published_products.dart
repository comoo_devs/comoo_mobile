import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/pages/common/product/product_page.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:math';

class PublishedProductsTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PublishedProductsTabState();
  }
}

class _PublishedProductsTabState extends State<PublishedProductsTab>
    with Reusables {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final Firestore db = Firestore.instance;
  StreamSubscription<QuerySnapshot> streamSubscription;

  List<Product> products = List<Product>();
  bool isLoading = false;
  final NumberFormat formater =
      NumberFormat.currency(locale: 'pt-br', symbol: 'R\$');

  Future<String> fetchProductThumb(DocumentReference productRef) async {
    var documents =
        await productRef.collection('photos').limit(1).getDocuments();
    if (documents.documents.length > 0) {
      return documents.documents[0].data['downloadURL'];
    } else
      return '';
  }

  _onQuerySnapshot(QuerySnapshot snapshot) async {
    List<Product> _products = List<Product>();
    _products = snapshot.documentChanges.map((DocumentChange productSnap) {
      return Product.fromSnapshot(productSnap.document);
    }).toList();
    setState(() {
      products.addAll(_products);
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    isLoading = true;
    this.streamSubscription = db
        .collection('products')
        .where('user.uid', isEqualTo: currentUser.uid)
        .snapshots()
        .listen(_onQuerySnapshot);
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    super.dispose(); //new
  }

  @override
  Widget build(BuildContext context) {
    Widget w = StreamBuilder<QuerySnapshot>(
        stream: db
            .collection('products')
            .where('user.uid', isEqualTo: currentUser.uid)
            .where('status', isEqualTo: 'public')
            .orderBy('date', descending: true)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              if (snapshot.hasData) return _buildSnapshotData(snapshot);
              return _buildEmptyList();
              break;
            case ConnectionState.done:
              if (snapshot.hasData) return _buildSnapshotData(snapshot);
              return _buildEmptyList();
              break;
            case ConnectionState.none:
              return _buildEmptyList();
              break;
            case ConnectionState.waiting:
              if (snapshot.hasData) return _buildSnapshotData(snapshot);
              return Center(
                child: CircularProgressIndicator(),
              );
              break;
            default:
              return Container();
              break;
          }
        });
    return Container(
      child: w,
    );
  }

  Widget _buildSnapshotData(AsyncSnapshot<QuerySnapshot> snapshot) {
    if (snapshot.data.documents.isEmpty) {
      return _buildEmptyList();
    }
    const double imageSize = 70.0;
    const double verticalPadding = 10.0;
    const double horizontalPadding = 10.0;
    final double soldWidth =
        sqrt(2 * pow(imageSize - horizontalPadding / 1.25, 2));
    const double soldHeight = 30.0;
    const double imageBorder = 1.0;
    return ListView(
        children: ListTile.divideTiles(
      color: ComooColors.cinzaMedio,
      tiles: snapshot.data.documents.map(
        (DocumentSnapshot snap) {
          final Product _product = Product.fromSnapshot(snap);
          List<Widget> _groups;
          if (_product.groups == null)
            _groups = [Container()];
          else
            _groups = _product.groups
                .map(
                  (g) => GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Text(
                        g['name'],
                        style: ComooStyles.localizacao,
                      ),
                    ),
                    onTap: () {},
                  ),
                )
                .toList();
          return Stack(
            children: <Widget>[
              ListTile(
                contentPadding: EdgeInsets.symmetric(
                  vertical: responsive(context, verticalPadding),
                  horizontal: responsive(context, horizontalPadding),
                ),
                leading: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: ComooColors.cinzaClaro),
                    borderRadius: BorderRadius.circular(10.0),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: CachedNetworkImageProvider(
                          _product.thumbs[0] ?? _product.photos[0] ?? ''),
                    ),
                  ),
                  width: responsive(context, imageSize),
                  height: responsive(context, imageSize),
                ),
                title: Text(
                  '${_product?.title ?? ''}',
                  style: ComooStyles.productTitle,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                subtitle: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(formater.format(_product.price),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: ComooStyles.productTitlePrice),
                    Container(
                      height: 20.0,
                      child: ListView(
                          scrollDirection: Axis.horizontal, children: _groups),
                    ),
                  ],
                ),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute<void>(
                      builder: (context) => ProductPage(_product),
                    ),
                  );
                },
              ),
              _product.sold
                  ? Positioned(
                      top: responsive(context,
                          verticalPadding + imageSize / 4 + imageBorder),
                      left: responsive(context, 1),
                      child: Container(
                        width: responsive(context, soldWidth),
                        height: responsive(context, soldHeight),
                        child: Transform.rotate(
                          angle: -pi / 4,
                          child: Container(
                            child: FittedBox(
                              child: Container(
                                decoration: BoxDecoration(
                                    color: COMOO.colors.pink,
                                    borderRadius: BorderRadius.circular(20.0)),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: responsive(
                                        context, horizontalPadding / 2),
                                    vertical: responsive(
                                        context, verticalPadding / 2),
                                  ),
                                  child: FittedBox(
                                    child: Text(
                                      'VENDIDO',
                                      style: TextStyle(
                                          fontFamily: COMOO.fonts.montSerrat,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: responsive(context, 12.0)),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  : Positioned(
                      top: 0.0,
                      child: Container(),
                    ),
            ],
          );
        },
      ),
    ).toList());
  }

  Widget _buildEmptyList() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Padding(
      padding: EdgeInsets.all(mediaQuery * 16.0),
      child: Center(
          child: RichText(
              text: TextSpan(
        text: 'Você ainda não publicou anúncios.\n',
        children: <TextSpan>[
          TextSpan(
              style: ComooStyles.welcomeMessage,
              text: 'Entre para as comoonidades para começar a anunciar!')
        ],
        style: ComooStyles.welcomeMessageBold,
      ))),
    );
  }
}
