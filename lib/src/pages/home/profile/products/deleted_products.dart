import 'dart:async';

//import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/product.dart';
// import 'package:comoo/pages/user_product_detail.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../../bottom_menu/product_creation/product_create.dart';
import 'package:intl/intl.dart';

class DeletedProductsTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _DeletedProductsTabState();
  }
}

class _DeletedProductsTabState extends State<DeletedProductsTab> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final Firestore db = Firestore.instance;
  StreamSubscription<QuerySnapshot> streamSubscription;

  List<Product> products = new List<Product>();
  bool isLoading = false;
  final NumberFormat formater =
      new NumberFormat.currency(locale: "pt-br", symbol: "R\$");

  Future<String> fetchProductThumb(DocumentReference productRef) async {
    var documents =
        await productRef.collection('photos').limit(1).getDocuments();
    if (documents.documents.length > 0) {
      return documents.documents[0].data["downloadURL"];
    } else
      return "";
  }

  _onQuerySnapshot(QuerySnapshot snapshot) async {
    List<Product> _products = new List<Product>();
    _products = snapshot.documentChanges.map((DocumentChange productSnap) {
      return new Product.fromSnapshot(productSnap.document);
    }).toList();
    setState(() {
      products.addAll(_products);
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    isLoading = true;
    this.streamSubscription = db
        .collection('products')
        .where('user.uid', isEqualTo: currentUser.uid)
        .snapshots()
        .listen(_onQuerySnapshot);
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget w = new StreamBuilder<QuerySnapshot>(
        stream: db
            .collection('products')
            .where('user.uid', isEqualTo: currentUser.uid)
            .where('status', isEqualTo: 'deleted')
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              if (snapshot.hasData) return _buildSnapshotData(snapshot);
              return _buildEmptyList();
              break;
            case ConnectionState.done:
              if (snapshot.hasData) return _buildSnapshotData(snapshot);
              return _buildEmptyList();
              break;
            case ConnectionState.none:
              return _buildEmptyList();
              break;
            case ConnectionState.waiting:
              if (snapshot.hasData) return _buildSnapshotData(snapshot);
              return Center(
                child: CircularProgressIndicator(),
              );
              break;
            default:
              return Container();
              break;
          }
        });
    return new Container(
      child: w,
    );
  }

  Widget _buildSnapshotData(AsyncSnapshot<QuerySnapshot> snapshot) {
    if (snapshot.data.documents.isEmpty) {
      return _buildEmptyList();
    }
    return new ListView(
        children: ListTile.divideTiles(
            color: ComooColors.cinzaMedio,
            tiles: snapshot.data.documents.map((DocumentSnapshot snap) {
              var _product = Product.fromSnapshot(snap);
              List<Widget> _groups;
              if (_product.groups == null)
                _groups = [new Container()];
              else
                _groups = _product.groups
                    .map(
                      (g) => new GestureDetector(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: new Text(
                            g["name"],
                            style: ComooStyles.localizacao,
                          ),
                        ),
                        onTap: () {},
                      ),
                    )
                    .toList();
              return new ListTile(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                leading: new Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: ComooColors.cinzaClaro),
                      borderRadius: BorderRadius.circular(10.0),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image:
                            new AdvancedNetworkImage(_product.photos[0] ?? ""),
                      )),
                  width: 55.0,
                  height: 55.0,
                ),
                title: new Text(
                  _product.title ?? "",
                  style: ComooStyles.productTitle,
                ),
                subtitle: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(formater.format(_product.price),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: ComooStyles.productTitlePrice),
                    new Container(
                      height: 20.0,
                      child: new ListView(
                          scrollDirection: Axis.horizontal, children: _groups),
                    ),
                  ],
                ),
                onTap: () async {
                  Navigator.of(context).push(new MaterialPageRoute(
                      builder: (context) => new ProductCreateView(
                          product: _product, openCamera: false)));
                },
              );
            })).toList());
  }

  Widget _buildEmptyList() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return new Padding(
      padding: EdgeInsets.all(mediaQuery * 16.0),
      child: new Center(
          child: new RichText(
              text: new TextSpan(
        text: "Você ainda não possui anúncios excluídos.\n",
        // children: <TextSpan>[
        // new TextSpan(
        //     style: ComooStyles.welcomeMessage,
        //     text:
        //         "Faça um anúncio nas suas comoonidades para que as pessoas encontrem o que você está vendendo!")
        // ],
        style: ComooStyles.welcomeMessageBold,
      ))),
    );
  }
}
