import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/blocs/buyings_bloc.dart';
import 'package:comoo/src/pages/common/transaction/negotiation_page.dart';
import 'package:comoo/src/pages/common/transaction/transaction_view.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:intl/intl.dart';
import 'package:comoo/src/widgets/loading_container.dart';

class BuyHistory extends StatefulWidget {
  BuyHistory({this.sort});
  final String sort;
  @override
  State createState() {
    return BuyHistoryState();
  }
}

class BuyHistoryState extends State<BuyHistory> with Reusables {
  // String negotiationStatus(String status) {
  //   switch (status) {
  //     case 'cancelled':
  //       return 'Cancelado';
  //     case 'under negotiation':
  //       return 'Aguardando Pagamento';
  //     case 'pending':
  //       return 'Processando Pagamento';
  //     case 'sent':
  //       return 'Enviado';
  //     case 'paid':
  //       return 'Pago';
  //     case 'finished':
  //       return 'Concluído';
  //     default:
  //       return '';
  //   }
  // }

  final NetworkApi api = NetworkApi();
  bool _loading = false;
  final Firestore db = Firestore.instance;

  Future<void> _finishNegotiation(Negotiation negotiation) async {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final bool result = await showCustomDialog<bool>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title:
            'Você está prestes a informar ao vendedor que recebeu o produto, isso irá finalizar a transação.\nVocê tem certeza?',
        actions: [
          Container(
            margin: EdgeInsets.symmetric(
              // horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'CANCELAR',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(false),
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(
              // horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'CONFIRMAR',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                ),
              ),
              onPressed: () => Navigator.of(context).pop(true),
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          )
        ],
      ),
    );
    // builder: (BuildContext context) {
    //   return AlertDialog(
    //       title: Text(
    //         'CONFIRMAR RECEBIMENTO',
    //         style: ComooStyles.texto_bold,
    //       ),
    //       content: Text(
    //           'VOCÊ ESTÁ PRESTES A INFORMAR AO VENDEDOR QUE RECEBEU O PRODUTO, ISSO IRÁ FINALIZAR A TRANSAÇÃO.'),
    //       actions: <Widget>[
    //         FlatButton(
    //           child: Text('CANCELAR'),
    //           onPressed: () => Navigator.of(context).pop(false),
    //         ),
    //         FlatButton(
    //           child: Text('CONFIRMAR'),
    //           onPressed: () => Navigator.of(context).pop(true),
    //         )
    //       ]);
    // });

    if (result ?? false) {
      if (mounted) {
        setState(() {
          _loading = true;
        });
      }
      // negotiation.status = 'finished';
      try {
        api.receivedConfirmation(negotiation).then((dynamic result) {
          if (mounted) {
            setState(() {
              negotiation.status = 'finished';
              _loading = false;
            });
          }
        });
      } catch (e) {
        print(e);
        if (mounted) {
          setState(() {
            negotiation.status = 'finished';
            _loading = false;
          });
        }
      }
      // db.collection('negotiations').document(negotiation.id).updateData(
      //     {'status': 'finished', 'receivedDate': DateTime.now()}).then((_) {
      //   setState(() {
      //     negotiation.status = 'finished';
      //     _loading = false;
      //   });
      // });
    }
  }

  final NumberFormat formatter =
      NumberFormat.currency(locale: 'pt-br', symbol: 'R\$');

  @override
  Widget build(BuildContext context) {
    // final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final BuyingsBloc bloc = BlocProvider.of<BuyingsBloc>(context);
    return Scaffold(
      appBar: CustomAppBar.round(
        color: ComooColors.chumbo,
        title: Text('minhas compras'),
      ),
      body: Stack(
        children: <Widget>[
          StreamBuilder<QuerySnapshot>(
            // stream: Firestore.instance
            //     .collection('negotiations')
            //     .where('client',
            //         isEqualTo: Firestore.instance
            //             .collection('users')
            //             .document(currentUser.uid))
            //     .orderBy('lastUpdate', descending: true)
            //     .snapshots(),
            stream: bloc.buyings,
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              // print('${snapshot.connectionState} - has data? ${snapshot.hasData}');
              switch (snapshot.connectionState) {
                case ConnectionState.active:
                  if (snapshot.hasData) return _buildSnapshotData(snapshot);
                  return _buildEmptyList();
                  break;
                case ConnectionState.done:
                  if (snapshot.hasData) return _buildSnapshotData(snapshot);
                  return _buildEmptyList();
                  break;
                case ConnectionState.none:
                  return _buildEmptyList();
                  break;
                case ConnectionState.waiting:
                  if (snapshot.hasData) return _buildSnapshotData(snapshot);
                  return LoadingContainer();
                  break;
                default:
                  return Container();
                  break;
              }
            },
          ),
          _loading ? LoadingContainer() : Container()
        ],
      ),
    );
  }

  Widget _buildSnapshotData(AsyncSnapshot<QuerySnapshot> snapshot) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final List<DocumentSnapshot> documents = snapshot.data.documents
        .where((DocumentSnapshot doc) => doc.data['status'] != 'cancelled')
        .toList();
    if (documents.isEmpty) {
      return _buildEmptyList();
    }
    if (widget.sort != null) {
      documents.sort((DocumentSnapshot snap1, DocumentSnapshot snap2) {
        final Negotiation n1 = Negotiation.fromSnapshot(snap1);
        final Negotiation n2 = Negotiation.fromSnapshot(snap2);

        return n1.compareTo(n2);
      });
    }
    return ListView.separated(
      padding: EdgeInsets.only(top: 10.0),
      separatorBuilder: (context, index) {
        return Divider(
          color: ComooColors.cinzaMedio,
        );
      },
      itemBuilder: (context, index) {
        final DocumentSnapshot document = documents[index];
        final Negotiation negotiation = Negotiation.fromSnapshot(document);
        return ListTile(
          contentPadding: EdgeInsets.symmetric(
              vertical: 0.0, horizontal: mediaQuery * 10.0),
          dense: true,
          leading: Container(
            decoration: BoxDecoration(
                border: Border.all(color: ComooColors.cinzaClaro),
                borderRadius: BorderRadius.circular(mediaQuery * 10.0),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(negotiation.thumb),
                )),
            width: mediaQuery * 55.0,
            height: mediaQuery * 55.0,
          ),
          title: Text(
            negotiation.title,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: ComooStyles.productTitle,
          ),
          isThreeLine: true,
          trailing: Container(
            width: mediaQuery * 120.0,
            // constraints: BoxConstraints(maxHeight: responsive(context, 120.0)),
            child: FittedBox(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: _buildActionButton(context, negotiation),
                    height: responsive(context, 30.0),
                  ),
                  SizedBox(height: responsive(context, 5.0)),
                  Text(
                    DateFormat('dd/MM/yyyy').format(negotiation.date),
                    style: ComooStyles.productTitlePrice,
                  ),
                ],
              ),
            ),
          ),
          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(formatter.format(negotiation.price),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: ComooStyles.productTitlePrice),
              Text(
                negotiation.statusName,
                style: ComooStyles.productTitlePrice,
              )
            ],
          ),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute<void>(
                builder: (context) => NegotiationPage(negotiation)));
          },
        );
      },
      itemCount: documents.length,
    );
  }

  Widget _buildEmptyList() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Padding(
      padding: EdgeInsets.all(mediaQuery * 16.0),
      child: Center(
          child: RichText(
              text: TextSpan(
        text: 'Você não possui compras em andamento.\n',
        children: <TextSpan>[
          TextSpan(
              style: ComooStyles.welcomeMessage,
              text:
                  'Navegue pela página principal para encontrar o(s) produto(s) que você procura!')
        ],
        style: ComooStyles.welcomeMessageBold,
      ))),
    );
  }

  Widget _buildActionButton(BuildContext context, Negotiation negotiation) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    switch (negotiation.status) {
      case 'waiting_send':
      case 'sent':
        return Container(
          margin: EdgeInsets.only(
            top: mediaQuery * 1.0,
          ),
          height: mediaQuery * 30.0,
          child: RaisedButton(
            elevation: 0.0,
            color: ComooColors.turqueza,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
            onPressed: () {
              _finishNegotiation(negotiation);
            },
            child: FittedBox(
              child: Text(
                'RECEBEU?',
                style: TextStyle(
                  fontFamily: ComooStyles.fontFamily,
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        );
        break;
      case 'under negotiation':
        return Container(
          margin: EdgeInsets.only(
            top: mediaQuery * 1.0,
          ),
          height: mediaQuery * 30.0,
          child: RaisedButton(
            elevation: 0.0,
            color: ComooColors.turqueza,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
            onPressed: () {
              _navigateToPaymentScreen(context, negotiation);
            },
            child: FittedBox(
              child: Text(
                'COMPRAR',
                style: TextStyle(
                  fontFamily: ComooStyles.fontFamily,
                  color: Colors.white,
                  fontSize: mediaQuery * 14.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        );
        break;
      default:
        return Container();
        break;
    }
  }

  Future<void> _navigateToPaymentScreen(
      BuildContext context, Negotiation negotiation) async {
    if (currentUser.sellerStatus == 'accepted') {
      await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => TransactionView(negotiation: negotiation),
          ));
      // Navigator.pop(context, result);
      // await _fetchNegotiation();
      print(negotiation.paid);
    } else {
      showIncompleteSignupDialog(context).then((result) {
        if (result ?? false) {
          // Navigator.of(context).pushNamed('/user_profile');
          Navigator.of(context).pushNamed('/sign_up_personal');
        }
      });
    }
  }
}
