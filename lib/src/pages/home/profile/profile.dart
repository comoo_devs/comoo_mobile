import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

import 'package:comoo/src/comoo/authentication.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:image_cropper/image_cropper.dart';

class ProfilePage extends StatefulWidget {
  final VoidCallback onBalanceTap;
  ProfilePage({Key key, this.onBalanceTap}) : super(key: key);

  @override
  State createState() {
    return _ProfilePageState(this.onBalanceTap);
  }
}

class _ProfilePageState extends State<ProfilePage> with Reusables {
  final NetworkApi api = NetworkApi();
  final googleSignIn = GoogleSignIn();
  final facebookLogin = FacebookLogin();
  // final _db = Firestore.instance;
//  FirebaseUser user;
//  String photoURL = '';
//  final FirebaseAuth auth = FirebaseAuth.instance;
  _ProfilePageState(this.onBalanceTap);
  TextEditingController displayNameTextController = TextEditingController();
  final UserAuth auth = UserAuth();
  bool isUploading = true;
  File _newPhoto;
  final VoidCallback onBalanceTap;
  // bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _loadCurrentUser();
  }

  Future<Null> _loadCurrentUser() async {
    setState(() {
      displayNameTextController.text = currentUser?.name ?? '';
    });
  }

  Future<File> getImage(ImageSource source) async {
    if (!(await checkPermissions(source))) {
      return null;
    }
    var image = await ImagePicker.pickImage(source: source);

    if (image == null) {
      return null;
    }
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        ratioX: 1.0,
        ratioY: 1.0,
        maxWidth: 512,
        maxHeight: 512,
        toolbarColor: ComooColors.roxo,
        toolbarTitle: 'COMOO');

    setState(() {
      isUploading = true;
      _newPhoto = croppedFile;
    });

//    var newImg = await compressImage(image);

    String url = await upload(croppedFile);
    currentUser.photoURL = url;
    UserUpdateInfo userUpdateInfo = UserUpdateInfo();
    userUpdateInfo.photoUrl = url;
    FirebaseUser firebaseUser = await FirebaseAuth.instance.currentUser();
    await firebaseUser.updateProfile(userUpdateInfo);
    await Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData({'photoURL': url});

    setState(() {
      _newPhoto = croppedFile;
      isUploading = false;
    });

    return croppedFile;
  }

  // Future<File> compressImage(File file) async {
  //   final tempDir = await getTemporaryDirectory();
  //   final path = tempDir.path;
  //   int rand = new Math.Random().nextInt(10000);
  //   Im.Image image = Im.decodeImage(file.readAsBytesSync());
  //   Im.copyResize(image, 500);
  //   var newImage = new File('$path/img_$rand.jpg')
  //     ..writeAsBytesSync(Im.encodeJpg(image, quality: 50));
  //   return newImage;
  // }

  Future<String> upload(File file) async {
    StorageReference ref = FirebaseStorage.instance
        .ref()
        .child('/users/${currentUser.uid}/avatar.jpg');
    final StorageTaskSnapshot uploadTask = await ref.putFile(file).onComplete;
    final String result = await uploadTask.ref.getDownloadURL().then((onValue) {
      return onValue.toString();
    }).catchError((onError) {
      print(onError);
      return '';
    });
    return result;
  }

  Widget _buildAvatar(String _photoUrl) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return GestureDetector(
      onTap: () async {
        var result = await showDialog<ImageSource>(
            context: context,
            builder: (BuildContext context) {
              return SimpleDialog(
                title: const Text(
                  'ESCOLHA ORIGEM',
                  style: ComooStyles.appBarTitle,
                ),
                children: <Widget>[
                  SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, ImageSource.camera);
                      },
                      child: Row(
                        children: <Widget>[
                          const Icon(
                            ComooIcons.camera,
                            color: ComooColors.roxo,
                          ),
                          Padding(
                              padding: EdgeInsets.only(left: mediaQuery * 20.0),
                              child: Text(
                                'Camera',
                                style: ComooStyles.botoes,
                              )),
                        ],
                      )),
                  SimpleDialogOption(
                    onPressed: () {
                      Navigator.pop(context, ImageSource.gallery);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(
                          ComooIcons.galeria,
                          color: ComooColors.roxo,
                          size: mediaQuery * 34.0,
                        ),
                        Padding(
                            padding: EdgeInsets.only(left: mediaQuery * 10.0),
                            child: const Text(
                              'Galeria de Fotos',
                              style: ComooStyles.botoes,
                            )),
                      ],
                    ),
                  )
                ],
              );
            });
        if (result == null) {
          return;
        }

        getImage(result);
      },
      child: Container(
        height: mediaQuery * 150.0,
        width: mediaQuery * 150.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(mediaQuery * 100.0),
            border: Border.all(
              width: mediaQuery * 1.2,
              color: ComooColors.cinzaClaro,
            ),
            image: DecorationImage(
              image: _newPhoto == null
                  ? CachedNetworkImageProvider(_photoUrl)
                  : FileImage(_newPhoto),
              fit: BoxFit.cover,
            )),
      ),
      // child: Container(
      //   height: mediaQuery * 150.0,
      //   width: mediaQuery * 150.0,
      //   decoration: BoxDecoration(
      //       borderRadius: BorderRadius.circular(mediaQuery * 100.0),
      //       border: Border.all(
      //           width: mediaQuery * 1.2, color: ComooColors.cinzaClaro),
      //       image: DecorationImage(
      //           image: _newPhoto == null
      //               ? AdvancedNetworkImage(_photoUrl)
      //               : FileImage(_newPhoto),
      //           fit: BoxFit.cover)),
//          child: _newPhoto == null
//              ? new Image(
//                  image: new AdvancedNetworkImage(_photoUrl),
//                  fit: BoxFit.cover,
//                  width: 150.0,
//                  height: 150.0,
//                )
//              : new Container(
//                  width: 150.0,
//                  height: 150.0,
//                  child: new Stack(
//                    children: <Widget>[
//                      Image.file(
//                        _newPhoto,
//                        fit: BoxFit.cover,
//                        width: 150.0,
//                        height: 150.0,
//                      ),
//                      isUploading
//                          ? new Center(
//                              child: CircularProgressIndicator(),
//                            )
//                          : new Container()
//                    ],
//                  ),
//                )
      // ),
    );
  }

  Widget _buildProfileName() {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Container(
      padding:
          EdgeInsets.only(bottom: mediaQuery * 50.0, top: mediaQuery * 15.0),
      child: Text(
        currentUser.name,
        style: ComooStyles.profile_name,
        textAlign: TextAlign.center,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    if (currentUser == null) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        final User fetchedUser = await auth.fetchCurrentUser();
        setState(() {});
        if (fetchedUser == null) {
          api.signOut();
          Navigator.of(context).pushReplacementNamed('/login');
        }
      });

      return Container();
    }
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: mediaQuery * 20.0),
              _buildAvatar(currentUser.photoURL),
              _buildProfileName(),
              _MenuCategory(
                title: 'MINHA CONTA',
                children: <Widget>[
                  _MenuItem(
                    icon: Icon(
                      ComooIcons.perfil_perfil,
                      color: ComooColors.turqueza,
                    ),
                    onPressed: () async {
                      // Navigator.of(context).pushNamed('/user_information');
                      await Navigator.of(context).pushNamed('/user_profile');
                      await auth.fetchCurrentUser();
                      if (mounted) {
                        setState(() {});
                      }
                    },
                    title: 'Perfil',
                  ),
                  Divider(
                    color: ComooColors.cinzaMedio,
                  ),
                  _MenuItem(
                    icon: Icon(
                      ComooIcons.meus_anuncios,
                      color: ComooColors.turqueza,
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed('/user_products');
                    },
                    title: 'Meus anúncios',
                  ),
                  Divider(
                    color: ComooColors.cinzaMedio,
                  ),
                  _MenuItem(
                    icon: Icon(
                      ComooIcons.minhas_comoos,
                      color: ComooColors.turqueza,
                    ),
                    title: 'Minhas comoonidades',
                    onPressed: () {
                      Navigator.of(context).pushNamed('/user_groups');
                    },
                  ),
                  Divider(
                    color: ComooColors.cinzaMedio,
                  ),
                  _MenuItem(
                    icon: Icon(
                      ComooIcons.curtir,
                      color: ComooColors.turqueza,
                    ),
                    title: 'Minhas preferências',
                    onPressed: () {
                      Navigator.of(context).pushNamed('/user_categories');
                    },
                  ),
                  Divider(
                    color: ComooColors.cinzaMedio,
                  ),
                  _MenuItem(
                    icon: Image.asset(
                      'images/faq.png',
                      colorBlendMode: BlendMode.multiply,
                    ),
                    title: 'Ajuda (FAQ)',
                    onPressed: () {
                      Navigator.of(context).pushNamed('/faq');
                    },
                  ),
                  Divider(
                    color: ComooColors.cinzaMedio,
                  ),
                  SizedBox(
                    height: mediaQuery * 45.0,
                  ),
                ],
              ),
              _MenuCategory(
                title: 'EXTRATO',
                children: <Widget>[
                  _MenuItem(
                    icon: Icon(
                      ComooIcons.saldo_perfil,
                      color: ComooColors.turqueza,
                    ),
                    title: 'Meu extrato',
                    onPressed: () {
                      Navigator.of(context).pushNamed('/my_extract');
                      // this.onBalanceTap();
                      // Navigator.of(context).pushNamed('/user_interests');
                    },
                  ),
                  Divider(
                    color: ComooColors.cinzaMedio,
                  ),
                  _MenuItem(
                    icon: Icon(
                      ComooIcons.vendas,
                      color: ComooColors.turqueza,
                    ),
                    title: 'Minhas Vendas',
                    onPressed: () {
                      Navigator.of(context).pushNamed('/sell_history');
                    },
                  ),
                  Divider(
                    color: ComooColors.cinzaMedio,
                  ),
                  _MenuItem(
                    icon: Icon(
                      ComooIcons.compras,
                      color: ComooColors.turqueza,
                    ),
                    title: 'Minhas Compras',
                    onPressed: () {
                      Navigator.of(context).pushNamed('/buy_history');
                    },
                  ),
                  Divider(
                    color: ComooColors.cinzaMedio,
                  ),
                  _MenuItem(
                    icon: Image.asset(
                      'images/shared_profit.png',
                      colorBlendMode: BlendMode.multiply,
                    ),
                    // icon: Icon(
                    //   COMOO.icons.sharedProfit.iconData,
                    //   color: ComooColors.turqueza,
                    // ),
                    title: 'Lucro compartilhado',
                    onPressed: () {
                      Navigator.of(context).pushNamed('/shared_profit');
                    },
                  ),
                  Divider(
                    color: ComooColors.cinzaMedio,
                  ),
                  Container(
                    alignment: FractionalOffset.center,
                    child: FlatButton(
                        onPressed: () {
                          _logOut();
                        },
                        child: Text(
                          'Sair da conta',
                          style: ComooStyles.profile_logout,
                        )),
                  ),
                  SizedBox(
                    height: mediaQuery * 10.0,
                  ),
                  SizedBox(
                    height: mediaQuery * 45.0,
                  ),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }

  _logOut() async {
    await api.signOut();
    Navigator.of(context).pushReplacementNamed('/login');
  }

  void showInSnackBar(String value) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(value, style: ComooStyles.textoSnackBar),
      // duration:  Duration(seconds: 10),
    ));
  }
}

class _MenuCategory extends StatelessWidget {
  const _MenuCategory({Key key, this.title, this.children}) : super(key: key);

  final String title;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    children.insert(
        0,
        Padding(
            padding: EdgeInsets.only(left: mediaQuery * 25.0),
            child: Text(title, style: ComooStyles.profileTitle)));
    children.insert(
      1,
      Divider(
        color: ComooColors.cinzaMedio,
      ),
    );
    return Container(
//      padding: const EdgeInsets.only(left: 16.0),
      child: SafeArea(
          top: false,
          bottom: false,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: children)),
    );
  }
}

class _MenuItem extends StatelessWidget {
  _MenuItem({Key key, this.icon, this.title, this.subTitle, this.onPressed})
      : super(key: key);

  final Widget icon;
  final String title;
  final String subTitle;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    final List<Widget> columnChildren = List<Widget>();

    columnChildren.add(Text(
      title,
      style: ComooStyles.profileMenu,
    ));
    if (subTitle != null) columnChildren.add(Text(subTitle));
    final List<Widget> rowChildren = <Widget>[
      SizedBox(
        width: mediaQuery * 72.0,
        child: Container(
          child: icon,
          height: mediaQuery * 28.0,
        ),
      ),
      Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: columnChildren)),
    ];

    return InkWell(
        onTap: onPressed,
        child: Container(
            height: mediaQuery * 46.0,
//            padding: const EdgeInsets.symmetric(vertical: 6.0),
            child: Row(children: rowChildren)));
  }
}
