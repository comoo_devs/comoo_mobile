import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/profile_personal_bloc.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/bank.dart';
import 'package:comoo/src/comoo/user.dart';
import './access_form.dart';
import './bank_form.dart';
import './personal_form.dart';
import './profile_picture.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/states_list.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class UserProfilePage extends StatefulWidget {
  @override
  State<UserProfilePage> createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final Firestore _db = Firestore.instance;
  final UserAuth auth = UserAuth();
  ProfilePersonalBloc _personalBLoC;
  StreamSubscription<dynamic> _zipSubscription;

  User _currentUser;

  bool _loading = false;

  Widget _buildForm(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: mediaQuery * 20.0),
          ),
          ProfilePicture(user: _currentUser),
          AccessForm(
            scaffoldKey: _scaffoldKey,
            user: _currentUser,
          ),
          BlocProvider<ProfilePersonalBloc>(
            bloc: _personalBLoC,
            child: PersonalForm(
                states: states
                    .map((dynamic state) => StateItem.fromJSON(state))
                    .toList()
                      ..sort((StateItem a, StateItem b) =>
                          a.name.compareTo(b.name))),
          ),
          StreamBuilder<List<Bank>>(
            stream: fetchBanks(),
            // initialData: initialData ,
            builder:
                (BuildContext context, AsyncSnapshot<List<Bank>> snapshot) {
              return snapshot.hasData
                  ? BankForm(user: _currentUser, banks: snapshot.data)
                  : Container();
            },
          ),
        ],
      ),
    );
  }

  Stream<List<Bank>> fetchBanks() async* {
    // List<Bank> _banks = <Bank>[];
    yield await _db
        .collection('banks')
        .orderBy('name')
        .getDocuments()
        .then((snapshot) {
      final List<Bank> _banks = snapshot.documents.map((doc) {
        final Bank b = Bank.fromSnapshot(doc);
        return b;
      }).toList();
      _banks.add(Bank(number: '0', name: 'selecione'));
      _banks.sort((Bank a, Bank b) => a.number.compareTo(b.number));

      return _banks;
    }).catchError((dynamic onError) {
      print(onError);
      return <Bank>[];
    });
  }

  @override
  void initState() {
    _loading = true;
    _fetchCurrentUser();
    super.initState();
  }

  Future<void> _fetchCurrentUser() async {
    _currentUser = await auth.fetchCurrentUser();
    _personalBLoC = ProfilePersonalBloc.seeded(
      user: _currentUser,
      states: states.map((dynamic state) => StateItem.fromJSON(state)).toList()
        ..sort((StateItem a, StateItem b) => a.name.compareTo(b.name)),
    );
    _zipSubscription = _personalBLoC.zip.listen((String zip) {
      _zipListener(zip, _personalBLoC);
    });
    if (mounted) {
      setState(() {
        _loading = false;
      });
    }
  }

  @override
  void dispose() {
    _zipSubscription?.cancel();
    super.dispose();
    _personalBLoC?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
        SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
      },
      child: Scaffold(
          key: _scaffoldKey,
          appBar: CustomAppBar.round(
            color: ComooColors.chumbo,
            title: Text('perfil', style: ComooStyles.appBarTitleSixteen),
          ),
          body: _loading
              ? LoadingContainer()
              : SingleChildScrollView(child: _buildForm(context))),
    );
  }

  Future<void> _zipListener(String data, ProfilePersonalBloc bloc) async {
    if (data.length == 9) {
      final String zip = bloc.zipController.text.replaceAll(RegExp(r'-'), '');
      final http.Response resp = await http
          .get('http://apps.widenet.com.br/busca-cep/api/cep.json?code=$zip');
      if (resp.statusCode == 200) {
        final dynamic jObj = json.decode(resp.body);
        print(jObj);
        final int status = jObj['status'] ?? 0;
        if (status != 0) {
          if (mounted) {
            bloc.changeAddressName(jObj['address']);
            bloc.addressNameController.text = jObj['address'];
            // bloc.addStateString(jObj['city']);
            // bloc.addUF(jObj['state']);
            //bloc.changeAddressComplement(jObj['complemento']);
            //bloc.addressComplementController.text = jObj['complemento'];
            bloc.changeAddressDistrict(jObj['district']);
            bloc.addressDistrictController.text = jObj['district'];
          }
        }
      }
    }
  }
}
