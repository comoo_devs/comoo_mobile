import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ProfilePicture extends StatefulWidget {
  ProfilePicture({Key key, @required this.user}) : super(key: key);
  final User user;
  @override
  _ProfilePictureState createState() => _ProfilePictureState();
}

class _ProfilePictureState extends State<ProfilePicture> with Reusables {
  File _newPhoto;
  bool isUploading = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _buildAvatar(currentUser.photoURL),
        _buildProfileName(),
      ],
    );
  }

  Widget _buildProfileName() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Text(
        currentUser.name,
        style: ComooStyles.profile_name,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildAvatar(String _photoUrl) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return GestureDetector(
      onTap: () async {
        var result = await showDialog<ImageSource>(
            context: context,
            builder: (BuildContext context) {
              return SimpleDialog(
                title: const Text(
                  'ESCOLHA ORIGEM',
                  style: ComooStyles.appBarTitle,
                ),
                children: <Widget>[
                  SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, ImageSource.camera);
                      },
                      child: Row(
                        children: <Widget>[
                          const Icon(
                            ComooIcons.camera,
                            color: ComooColors.roxo,
                          ),
                          Padding(
                              padding: EdgeInsets.only(left: mediaQuery * 20.0),
                              child: Text(
                                'Camera',
                                style: ComooStyles.botoes,
                              )),
                        ],
                      )),
                  SimpleDialogOption(
                    onPressed: () {
                      Navigator.pop(context, ImageSource.gallery);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(
                          ComooIcons.galeria,
                          color: ComooColors.roxo,
                          size: mediaQuery * 34.0,
                        ),
                        Padding(
                            padding: EdgeInsets.only(left: mediaQuery * 10.0),
                            child: const Text(
                              'Galeria de Fotos',
                              style: ComooStyles.botoes,
                            )),
                      ],
                    ),
                  )
                ],
              );
            });
        if (result == null) {
          return;
        }

        getImage(result);
      },
      child: Container(
        height: mediaQuery * 150.0,
        width: mediaQuery * 150.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(mediaQuery * 100.0),
            border: Border.all(
                width: mediaQuery * 1.2, color: ComooColors.cinzaClaro),
            image: DecorationImage(
                image: _newPhoto == null
                    ? CachedNetworkImageProvider(_photoUrl)
                    : FileImage(_newPhoto),
                fit: BoxFit.cover)),
      ),
    );
  }

  Future<File> getImage(ImageSource source) async {
    if (!(await checkPermissions(source))) {
      return null;
    }
    final File image = await ImagePicker.pickImage(source: source);

    if (image == null) {
      return null;
    }
    final File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        ratioX: 1.0,
        ratioY: 1.0,
        maxWidth: 512,
        maxHeight: 512,
        toolbarColor: ComooColors.roxo,
        toolbarTitle: 'COMOO');

    setState(() {
      isUploading = true;
      _newPhoto = croppedFile;
    });

//    var newImg = await compressImage(image);

    final String url = await upload(croppedFile);
    currentUser.photoURL = url;
    final UserUpdateInfo userUpdateInfo = UserUpdateInfo();
    userUpdateInfo.photoUrl = url;
    final FirebaseUser firebaseUser = await FirebaseAuth.instance.currentUser();
    await firebaseUser.updateProfile(userUpdateInfo);
    await Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData({'photoURL': url});
    if (mounted) {
      setState(() {
        _newPhoto = croppedFile;
        isUploading = false;
      });
    }
    return croppedFile;
  }

  Future<String> upload(File file) async {
    final StorageReference ref = FirebaseStorage.instance
        .ref()
        .child('/users/${currentUser.uid}/avatar.jpg');
    final StorageTaskSnapshot uploadTask = await ref.putFile(file).onComplete;
    final String result = await uploadTask.ref.getDownloadURL().then((onValue) {
      return onValue.toString();
    }).catchError((onError) {
      print(onError);
      return '';
    });
    return result;
  }
}
