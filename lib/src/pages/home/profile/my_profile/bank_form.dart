import 'package:comoo/src/blocs/profile_bank_bloc.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/bank.dart';
import 'package:comoo/src/comoo/user.dart';
import './menu_category.dart';
// import 'package:comoo/src/provider/provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

class BankForm extends StatefulWidget with Reusables, MapAccountType {
  BankForm({
    Key key,
    // @required this.bloc,
    @required this.user,
    @required this.banks,
  })  : assert(user != null),
        assert(banks != null),
        accountTypes = AccountType.values,
        super(key: key);

  final User user;
  final List<Bank> banks;
  final List<AccountType> accountTypes;
  final NetworkApi api = NetworkApi();
  final UserAuth auth = UserAuth();

  @override
  State<StatefulWidget> createState() => _BankFormState();
}

class _BankFormState extends State<BankForm> {
  _BankFormState() : bloc = ProfileBankBloc();
  final ProfileBankBloc bloc;
  User user;

  @override
  void initState() {
    super.initState();
    user = widget.user;
    initialValues();
  }

  @override
  void dispose() {
    super.dispose();
    bloc.dispose();
  }

  Future<void> _fetchCurrentUser() async {
    user = await widget.auth.fetchCurrentUser();
  }

  void initialValues() {
    if (user.bank != null) {
      if (mounted) {
        final String agency = user.bank.agency;
        final String account = user.bank.account;
        if (mounted) {
          setState(() {
            final String _agencyNumber = agency.split('-').first;
            final String _agencyDigit =
                agency.split('-').length > 1 ? agency.split('-').last : '';
            final String _accountNumber = account.split('-').first;
            final String _accountDigit =
                account.split('-').length > 1 ? account.split('-').last : '';
            // final AccountType acctype = user.bank.type == 'SAVING'
            //     ? AccountType.savings
            //     : AccountType.checking;
            // final Bank _bank = widget.banks.firstWhere(
            //     (Bank b) => b.number == user.bank.number,
            //     orElse: () => Bank(number: user.bank.number, name: ''));

            // bloc.accountController.text = acc?.name ?? '';
            // bankController.text = '${_bank?.number} - ${_bank?.name}';
            final Bank _bank = _fetchBank();
            if (_bank.number != '0') {
              bloc.bankController.text = _bank.name;
              bloc.changeBank(_bank);
            }
            if (_agencyNumber != null && (_agencyNumber?.isNotEmpty ?? false)) {
              bloc.agencyNumberController.text = _agencyNumber;
              bloc.changeAgencyNumber(_agencyNumber);
            }
            if (_agencyDigit != null && (_agencyDigit?.isNotEmpty ?? false)) {
              bloc.agencyDigitController.text = _agencyDigit;
              bloc.changeAgencyDigit(_agencyDigit);
            }
            if (_accountNumber != null &&
                (_accountNumber?.isNotEmpty ?? false)) {
              bloc.accountNumberController.text = _accountNumber;
              bloc.changeAccountNumber(_accountNumber);
            }
            if (_accountDigit != null && (_accountDigit?.isNotEmpty ?? false)) {
              bloc.accountDigitController.text = _accountDigit;
              bloc.changeAccountDigit(_accountDigit);
            }
            final AccountType acc = _fetchAccountType();
            bloc.accountController.text = widget.accountName(acc);
            bloc.changeAccountType(acc);
          });
        }
      }
    }
  }

  Bank _fetchBank() {
    return widget.banks.firstWhere(
      (Bank b) => b.number == user.bank.number,
      orElse: () => Bank(name: 'selecione', number: '0'),
    );
  }

  AccountType _fetchAccountType() {
    switch (user.bank.type) {
      case 'SAVING':
        return AccountType.savings;
        break;
      case 'CHECKING':
        return AccountType.checking;
        break;
      default:
        return AccountType.blank;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final TextStyle textFormStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w500,
    );
    // bloc.bankController.text = _fetchBank().name;
    // bloc.accountController.text = _fetchAccountType().name;
    return StreamBuilder<bool>(
      initialData: true,
      stream: bloc.editBankData,
      builder: (BuildContext context, AsyncSnapshot<bool> editSnap) {
        return StreamBuilder<bool>(
            initialData: false,
            stream: bloc.isLoading,
            builder:
                (BuildContext loadingContext, AsyncSnapshot<bool> loadingSnap) {
              return Column(
                children: <Widget>[
                  MenuCategory(
                    title: 'DADOS BANCÁRIOS',
                    padding:
                        EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
                    children: <Widget>[
                      Text(
                        'Insira seus dados bancários com atenção. Vamos\nutilizar essas informações para depositar seus\nganhos com vendas e com o lucro compartilhado.',
                        style: ComooStyles.listDescription,
                      ),
                      CupertinoDropdownInputStream<Bank>(
                        // initialData: _fetchBank(),
                        stream: bloc.bank,
                        controller: bloc.bankController,
                        keyboardType: TextInputType.number,
                        enabled: editSnap.data,
                        style: textFormStyle,
                        decoration: InputDecoration(
                          labelText: 'BANCO',
                          isDense: true,
                          helperStyle: ComooStyles.texto,
                          focusedBorder: ComooStyles.underLineBorder,
                          labelStyle: ComooStyles.inputLabel
                              .copyWith(color: COMOO.colors.purple),
                        ),
                        onChange: bloc.changeBank,
                        items: widget.banks,
                        convertToString: (Bank b) => b.name,
                        children: List<Widget>.generate(
                            widget.banks.length ?? 0, (int index) {
                          return Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: mediaQuery * 30.0),
                            child: Text(
                              widget.banks[index].number == '0'
                                  ? '${widget.banks[index].name}'
                                  : '${widget.banks[index].number} - ${widget.banks[index].name}',
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                            ),
                          );
                        }),
                      ),
                      InputStream(
                        style: textFormStyle,
                        // initialData: user.bank.agency?.split('-')?.first ?? '',
                        stream: bloc.agencyNumber,
                        onChanged: bloc.changeAgencyNumber,
                        decoration: ComooStyles.inputCustomDecoration(
                            'agência (sem dígito)'),
                        keyboardType: TextInputType.number,
                        enabled: editSnap.data,
                        controller: bloc.agencyNumberController,
                      ),
                      InputStream(
                        style: textFormStyle,
                        // initialData: user.bank.agency?.split('-')?.last,
                        stream: bloc.agencyDigit,
                        onChanged: bloc.changeAgencyDigit,
                        decoration: ComooStyles.inputCustomDecoration('dígito')
                            .copyWith(
                                hintText:
                                    'Coloque apenas se houve dígito verificador'),
                        keyboardType: TextInputType.number,
                        enabled: editSnap.data,
                        controller: bloc.agencyDigitController,
                      ),
                      CupertinoDropdownInputStream<AccountType>(
                        // initialData: _fetchAccountType(),
                        stream: bloc.accountType,
                        controller: bloc.accountController,
                        keyboardType: TextInputType.number,
                        enabled: editSnap.data,
                        style: textFormStyle,
                        decoration:
                            ComooStyles.inputCustomDecoration('TIPO DE CONTA'),
                        onChange: bloc.changeAccountType,
                        convertToString: widget.accountName,
                        items: AccountType.values,
                        children: List<Widget>.generate(
                          widget.accountTypes.length ?? 0,
                          (int index) {
                            return Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: mediaQuery * 30.0),
                              child: Text(
                                '${widget.accountName(widget.accountTypes[index])}',
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            );
                          },
                        ),
                      ),
                      InputStream(
                        style: textFormStyle,
                        // initialData: user.bank.account?.split('-')?.first,
                        stream: bloc.accountNumber,
                        onChanged: bloc.changeAccountNumber,
                        decoration: ComooStyles.inputCustomDecoration(
                            'número da conta (sem dígito)'),
                        keyboardType: TextInputType.number,
                        enabled: editSnap.data,
                        controller: bloc.accountNumberController,
                      ),
                      InputStream(
                        style: textFormStyle,
                        // initialData: user.bank.account?.split('-')?.last,
                        stream: bloc.accountDigit,
                        onChanged: bloc.changeAccountDigit,
                        decoration: ComooStyles.inputCustomDecoration('dígito')
                            .copyWith(
                          hintText:
                              'Coloque apenas se houve dígito verificador',
                        ),
                        keyboardType: TextInputType.number,
                        enabled: editSnap.data,
                        controller: bloc.accountDigitController,
                      ),
                    ],
                  ),
                  StreamBuilder<bool>(
                    stream: bloc.submitValid,
                    builder: (BuildContext submitContext,
                        AsyncSnapshot<bool> snapshot) {
                      return Container(
                        alignment: FractionalOffset.center,
                        margin:
                            EdgeInsets.symmetric(vertical: mediaQuery * 20.0),
                        child: OutlineButton(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(mediaQuery * 30.0)),
                          borderSide: BorderSide(color: ComooColors.roxo),
                          highlightElevation: mediaQuery * 10.0,
                          padding: EdgeInsets.symmetric(
                              horizontal: mediaQuery * 56.0,
                              vertical: mediaQuery * 8.0),
                          child: loadingSnap.data
                              ? Container(
                                  height: mediaQuery * 20.0,
                                  child: FittedBox(
                                    child: Center(
                                        child:
                                            const CircularProgressIndicator()),
                                  ),
                                )
                              : const Text(
                                  'SALVAR',
                                  style: ComooStyles.botoes,
                                ),
                          onPressed: editSnap.data
                              ? () {
                                  print('handle');
                                  _save(context, bloc);
                                }
                              : null,
                        ),
                      );
                    },
                  ),
                ],
              );
            });
      },
    );
  }

  Future<void> _save(BuildContext context, ProfileBankBloc bloc) async {
    bloc.changeLoading(true);
    Map<String, String> param = <String, String>{
      'number': bloc.bankValue ?? '',
      'type': bloc.accountTypeValue ?? '',
      'agency': bloc.agencyValue ?? '',
      'account': bloc.accountValue ?? '',
    };
    print(param);
    final bool isSuccess = await widget.api.updateBankAccount(context, param);

    if (isSuccess) {
      widget.showMessageDialog(context, 'Dados bancários salvos com sucesso!');
      //bloc.changeEditBankData(false);
      _fetchCurrentUser();
    }
    bloc.changeLoading(false);
  }
}
/*
class BankCategory extends StatelessWidget with Reusables, MapAccountType {
  BankCategory({
    Key key,
    // @required this.bloc,
    @required this.user,
    @required this.banks,
    @required this.accountTypes,
  })  : assert(user != null),
        assert(banks != null),
        assert(accountTypes != null),
        super(key: key);

  final User user;
  final List<Bank> banks;
  final List<BankAccount> accountTypes;
  final NetworkApi api = NetworkApi();
  // final ProfileBankBloc bloc;

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    ProfileBankBloc bloc = Provider.of(context).profileBank;
    final TextStyle textFormStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w500,
    );
    // bloc.bankController.text = _fetchBank().name;
    // bloc.accountController.text = _fetchAccountType().name;
    return StreamBuilder<bool>(
      initialData: false,
      stream: bloc.editBankData,
      builder: (BuildContext context, AsyncSnapshot<bool> editSnap) {
        return Column(
          children: <Widget>[
            MenuCategory(
              title: 'DADOS BANCÁRIOS',
              icon: IconButton(
                icon: Icon(Icons.edit),
                color: editSnap.data
                    ? COMOO.colors.purple
                    : COMOO.colors.turquoise,
                onPressed: () {
                  bloc.changeEditBankData(!editSnap.data);
                },
              ),
              padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
              children: <Widget>[
                Text(
                  'Insira seus dados bancários com atenção. Vamos\nutilizar essas informações para depositar seus\nganhos com vendas e com o lucro compartilhado.',
                  style: ComooStyles.listDescription,
                ),
                CupertinoDropdownInputStream<Bank>(
                  // initialData: _fetchBank(),
                  stream: bloc.bank,
                  controller: bloc.bankController,
                  keyboardType: TextInputType.number,
                  enabled: editSnap.data,
                  style: textFormStyle,
                  decoration: InputDecoration(
                    labelText: 'BANCO',
                    isDense: true,
                    helperStyle: ComooStyles.texto,
                    focusedBorder: ComooStyles.underLineBorder,
                    labelStyle: ComooStyles.inputLabel
                        .copyWith(color: COMOO.colors.purple),
                  ),
                  onChange: bloc.changeBank,
                  items: banks,
                  convertToString: (Bank b) => b.name,
                  children:
                      List<Widget>.generate(banks.length ?? 0, (int index) {
                    return Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: mediaQuery * 30.0),
                      child: Text(
                        banks[index].number == '0'
                            ? '${banks[index].name}'
                            : '${banks[index].number} - ${banks[index].name}',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    );
                  }),
                ),
                InputStream(
                  style: textFormStyle,
                  initialData: user.bank.agency?.split('-')?.first,
                  stream: bloc.agencyNumber,
                  onChanged: bloc.changeAgencyNumber,
                  decoration:
                      ComooStyles.inputCustomDecoration('agência (sem dígito)'),
                  keyboardType: TextInputType.number,
                  enabled: editSnap.data,
                  controller: bloc.agencyNumberController,
                ),
                InputStream(
                  style: textFormStyle,
                  initialData: user.bank.agency?.split('-')?.last,
                  stream: bloc.agencyDigit,
                  onChanged: bloc.changeAgencyDigit,
                  decoration: ComooStyles.inputDecoration('dígito',
                      hint: 'Coloque apenas se houve dígito verificador'),
                  keyboardType: TextInputType.number,
                  enabled: editSnap.data,
                  controller: bloc.agencyDigitController,
                ),
                CupertinoDropdownInputStream<AccountType>(
                  // initialData: _fetchAccountType(),
                  stream: bloc.accountType,
                  controller: bloc.accountController,
                  keyboardType: TextInputType.number,
                  enabled: editSnap.data,
                  style: textFormStyle,
                  decoration: InputDecoration(
                    labelText: 'TIPO DE CONTA',
                    isDense: true,
                    helperStyle: ComooStyles.texto,
                    focusedBorder: ComooStyles.underLineBorder,
                    labelStyle: ComooStyles.inputLabel
                        .copyWith(color: COMOO.colors.purple),
                  ),
                  onChange: bloc.changeAccountType,
                  convertToString: accountName,
                  items: AccountType.values,
                  children: List<Widget>.generate(
                    accountTypes.length ?? 0,
                    (int index) {
                      return Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: mediaQuery * 30.0),
                        child: Text(
                          '${accountTypes[index].name}',
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      );
                    },
                  ),
                ),
                InputStream(
                  style: textFormStyle,
                  initialData: user.bank.account?.split('-')?.first,
                  stream: bloc.accountNumber,
                  onChanged: bloc.changeAccountNumber,
                  decoration: ComooStyles.inputCustomDecoration(
                      'número da conta (sem dígito)'),
                  keyboardType: TextInputType.number,
                  enabled: editSnap.data,
                  controller: bloc.accountNumberController,
                ),
                InputStream(
                  style: textFormStyle,
                  initialData: user.bank.account?.split('-')?.last,
                  stream: bloc.accountDigit,
                  onChanged: bloc.changeAccountDigit,
                  decoration: ComooStyles.inputDecoration('dígito',
                      hint: 'Coloque apenas se houve dígito verificador'),
                  keyboardType: TextInputType.number,
                  enabled: editSnap.data,
                  controller: bloc.accountDigitController,
                ),
              ],
            ),
            StreamBuilder<bool>(
                stream: bloc.submitValid,
                builder: (BuildContext cont, AsyncSnapshot<bool> snapshot) {
                  return Container(
                    alignment: FractionalOffset.center,
                    margin: EdgeInsets.symmetric(vertical: mediaQuery * 20.0),
                    child: OutlineButton(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(mediaQuery * 30.0)),
                      borderSide: BorderSide(color: ComooColors.roxo),
                      highlightElevation: mediaQuery * 10.0,
                      padding: EdgeInsets.symmetric(
                          horizontal: mediaQuery * 56.0,
                          vertical: mediaQuery * 8.0),
                      child: const Text(
                        'SALVAR',
                        style: ComooStyles.botoes,
                      ),
                      onPressed: editSnap.data
                          ? snapshot.hasData
                              ? () {
                                  print('handle');
                                  _save(context, bloc);
                                }
                              : null
                          : null,
                    ),
                  );
                }),
          ],
        );
      },
    );
  }

  Future<void> _save(BuildContext context, ProfileBankBloc bloc) async {
    Map<String, String> param = <String, String>{
      'number': bloc.bankValue,
      'type': bloc.accountTypeValue,
      'agency': bloc.agencyValue,
      'account': bloc.accountValue,
    };
    final bool isSuccess = await api.updateBankAccount(context, param);

    if (isSuccess) {
      // await _showSuccessDialog();
      bloc.changeEditBankData(false);
    }
    bloc.changeLoading(false);
  }

  // Bank _fetchBank() {
  //   return banks.firstWhere(
  //     (Bank b) => b.number == user.bank.number,
  //     orElse: () => banks[0],
  //   );
  // }

  // BankAccount _fetchAccountType() {
  //   final AccountType type =
  //       user.bank.type == 'SAVING' ? AccountType.savings : AccountType.checking;
  //   return accountTypes.firstWhere(
  //     (BankAccount ba) => ba.type == type,
  //     orElse: () => accountTypes[0],
  //   );
  // }
}
*/
