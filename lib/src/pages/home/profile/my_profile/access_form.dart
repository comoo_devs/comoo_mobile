import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/profile_access_bloc.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/comoo/validations.dart';
import './menu_category.dart';
import 'package:comoo/src/provider/provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AccessForm extends StatefulWidget {
  AccessForm({Key key, @required this.scaffoldKey, @required this.user})
      : super(key: key);
  final GlobalKey<ScaffoldState> scaffoldKey;

  final User user;

  _AccessFormState createState() => _AccessFormState();
}

class _AccessFormState extends State<AccessForm> with Validations, Reusables {
  final GlobalKey<FormState> _formKeyAccess = GlobalKey<FormState>();

  final Firestore _db = Firestore.instance;
  final UserAuth auth = UserAuth();

  // User _currentUser;
  String _displayName = '';
  String _nickname = '';
  bool _validateNickname = false;
  String _validateNicknameMessage = '';
  String _email = '';
  String _password = '';

  bool _hasChangesAccess = false;
  bool _autoValidateAccess = false;

  // bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    ProfileAccessBloc bloc = Provider.of(context).profileAccess;
    // TextStyle prefixStyle = TextStyle(
    //   fontFamily: ComooStyles.fontFamily,
    //   color: ComooColors.cinzaClaro,
    //   fontSize: mediaQuery * 15.0,
    //   fontWeight: FontWeight.w400,
    // );
    final TextStyle textFormStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w500,
    );
    // TextStyle hintStyle = TextStyle(
    //   fontFamily: ComooStyles.fontFamily,
    //   color: ComooColors.cinzaClaro,
    //   fontSize: mediaQuery * 15.0,
    //   fontWeight: FontWeight.w400,
    // );
    final TextStyle labelFormStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.roxo,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w500,
    );
    // InputBorder inputBorder = ComooStyles.underLineBorder;
    return StreamBuilder<bool>(
      initialData: true,
      stream: bloc.editAccessData,
      builder: (BuildContext editContext, AsyncSnapshot<bool> snapshot) {
        return Form(
          onChanged: () {
            if (mounted) {
              print(_hasChangesAccess);
              setState(() {
                _hasChangesAccess = true;
              });
            }
          },
          key: _formKeyAccess,
          autovalidate: _autoValidateAccess,
          child: MenuCategory(
            title: 'DADOS DE ACESSO',
            padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
            children: <Widget>[
              TextFormField(
                style: textFormStyle,
                decoration: ComooStyles.inputCustomDecoration('NOME COMPLETO'),
                initialValue: _displayName,
                textCapitalization: TextCapitalization.words,
                enabled: snapshot.data,
                onSaved: (String fullName) {
                  _displayName = fullName.trim();
                },
                validator: validateEmpty,
              ),
              TextFormField(
                // decoration: ComooStyles.inputDecoration('NOME DE USUÁRIO'),
                decoration: InputDecoration(
                  labelText: 'NOME DE USUÁRIO',
                  helperStyle: ComooStyles.texto,
                  focusedBorder: ComooStyles.underLineBorder,
                  labelStyle: labelFormStyle,
                  border: ComooStyles.underLineBorder,
                  errorText:
                      _validateNickname ? _validateNicknameMessage : null,
                ),
                enabled: snapshot.data,
                style: textFormStyle,
                validator: validateNickname,
                initialValue: _nickname,
                keyboardType: TextInputType.text,
                onSaved: (String nickname) {
                  _nickname = nickname.trim();
                },
              ),
              TextFormField(
                initialValue: _email,
                keyboardType: TextInputType.emailAddress,
                validator: validateEmail,
                enabled: snapshot.data,
                onSaved: (String email) {
                  _email = email;
                },
                style: textFormStyle,
                decoration: ComooStyles.inputCustomDecoration('E-MAIL'),
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                style: ComooStyles.inputText,
                initialValue: _password,
                decoration: ComooStyles.inputCustomDecoration('SENHA'),
                obscureText: true,
                // validator: validatePassword,
                enabled: snapshot.data,
                onSaved: (String password) {
                  _password = password;
                },
              ),
              SizedBox(height: responsive(context, 25.0)),
              Container(
                alignment: FractionalOffset.center,
                margin: EdgeInsets.only(bottom: mediaQuery * 30.0),
                child: OutlineButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(mediaQuery * 30.0)),
                  disabledBorderColor: COMOO.colors.purple,
                  borderSide: BorderSide(color: ComooColors.roxo),
                  highlightElevation: mediaQuery * 10.0,
                  padding: EdgeInsets.symmetric(
                      horizontal: mediaQuery * 58.0,
                      vertical: mediaQuery * 8.0),
                  child: const Text(
                    'SALVAR',
                    style: ComooStyles.botoes,
                  ),
                  onPressed: snapshot.data
                      ? _hasChangesAccess
                          ? () {
                              _handleSaveAccess(context);
                            }
                          : null
                      : null,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void showInSnackBar(String value) {
    showCustomSnackbar(
      scaffoldKey: widget.scaffoldKey,
      content: Text(value, style: ComooStyles.textoSnackBar),
      color: COMOO.colors.lead,
    );
    // widget.scaffoldKey.currentState.showSnackBar(SnackBar(
    //   content: Text(value, style: ComooStyles.textoSnackBar),
    //   duration: const Duration(seconds: 10),
    // ));
  }

  Future<void> _handleSaveAccess(BuildContext context) async {
    setState(() {
      // _isLoading = true;
      _validateNickname = false;
    });
    final FormState form = _formKeyAccess.currentState;
    if (!form.validate()) {
      _autoValidateAccess = true;
      showInSnackBar('Verifique os erros antes de continuar.');
      setState(() {
        // _isLoading = false;
      });
    } else {
      form.save();

      final String nicknameValidation =
          await _nicknameDuplicateValidation(_nickname);
      if (nicknameValidation != null && _nickname != currentUser.nickname) {
        showInSnackBar('Verifique os erros antes de continuar.');
        setState(() {
          _validateNicknameMessage = nicknameValidation.toString();
          _validateNickname = true;
          // _isLoading = false;
        });
      } else {
        _saveAccess(context);
      }
    }
  }

  Future<String> _nicknameDuplicateValidation(String nickname) async {
    return await _db
        .collection('users')
        .where('nickname', isEqualTo: nickname)
        .getDocuments()
        .then((docs) {
      // print('IsEmpty? ${docs.documents.isEmpty}');
      return docs.documents.isEmpty ? null : 'Este nome de usuário já existe';
    }).catchError((onError) {
      return null;
    });
  }

  Future<void> _saveAccess(BuildContext context) async {
    print('nova senha: $_password');
    final String result = await _showPasswordDialog(context);
    if (result != '' && result != null) {
      try {
        final UserData newUser = UserData(
          email: currentUser.email,
          password: result,
        );
        await auth.verifyUser(newUser);
        if (_email != currentUser.email) {
          await auth.updateEmail(_email);
          _setUserAccessInformation(
            displayName: _displayName,
            nickname: _nickname,
            email: _email,
          );
        } else {
          _setUserAccessInformation(
            displayName: _displayName,
            nickname: _nickname,
          );
        }
        if (_password.isNotEmpty &&
            _password != result &&
            _password != '******') {
          auth.updatePassword(_password);
        }

        setState(() {
          currentUser.name = _displayName;
          // _isLoading = false;
        });
        _updatedData();
      } on PlatformException catch (e) {
        print('>> PlatformException $e');
        switch (e.code) {
          case 'ERROR_WRONG_PASSWORD':
            showInSnackBar('Senha errada!');
            break;
          default:
            showInSnackBar('Algo deu errado, tente novamente!');
            break;
        }
      } catch (error) {
        print(error);
        if (error.message ==
            'The email address is already in use by another account.') {
          showInSnackBar('E-mail já está em uso.');
        } else {
          showInSnackBar('Algo deu errado, tente novamente.');
        }
      }

      if (mounted) {
        setState(() {
          // _isLoading = false;
        });
      }
    } else {
      // print('password null or empty!');
      // showInSnackBar('Digite uma senha para continuar');
      setState(() {
        // _isLoading = false;
      });
    }
    //bloc.changeEditAccessData(false);
  }

  Future<void> _setUserAccessInformation(
      {String displayName, String nickname, String email}) {
    final String uid = currentUser.uid;
    final WriteBatch batch = _db.batch();
    final DocumentReference userDocRef = _db.collection('users').document(uid);
    if (email != null) {
      batch.updateData(userDocRef, <String, dynamic>{
        'name': displayName,
        'nickname': nickname,
        'email': email,
      });
    } else {
      batch.updateData(userDocRef, <String, dynamic>{
        'name': displayName,
        'nickname': nickname,
      });
    }

    return batch.commit();
  }

  Future<String> _showPasswordDialog(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    String _password = '';
    return showCustomDialog<String>(
      context: context,
      barrierDismissible: true,
      alert: customRoundAlert(
        context: context,
        title: 'Insira sua senha para validar os dados de acesso',
        subtitle: Container(
          padding: EdgeInsets.symmetric(
            horizontal: mediaQuery * 31.0,
          ),
          child: TextField(
            style: TextStyle(
              color: Colors.white,
              fontSize: mediaQuery * 18.0,
            ),
            decoration: InputDecoration(
              hintText: 'mínimo 6 dígitos',
              hintStyle: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 0.65),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: ComooColors.white500,
                  style: BorderStyle.solid,
                ),
              ),
            ),
            keyboardType: TextInputType.emailAddress,
            obscureText: true,
            onChanged: (String pass) {
              _password = pass;
            },
            onSubmitted: (value) {
              // _resetPassword(value);
              Navigator.of(context).pop(value);
            },
          ),
        ),
        actions: [
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: mediaQuery * 40.0,
              vertical: mediaQuery * 10.0,
            ),
            child: OutlineButton(
              child: Text(
                'ENVIAR',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop(_password);
                // _resetPassword(_password);
              },
              borderSide: BorderSide(color: ComooColors.white500),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(mediaQuery * 20.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _updatedData() {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return showCustomDialog<void>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Seus dados foram enviados com sucesso :)',
        actions: [
          Container(
            height: mediaQuery * 25.0,
            width: mediaQuery * 110.0,
            decoration: BoxDecoration(
              border: Border.all(width: 1.0, color: Colors.white),
              borderRadius: BorderRadius.circular(mediaQuery * 20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
              onPressed: () {
                Navigator.pop(context);
              },
              child: FittedBox(
                child: Text(
                  'FECHAR',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    _fetchCurrentUser();
    super.initState();
  }

  Future<void> _fetchCurrentUser() async {
    // _currentUser = await auth.fetchCurrentUser();

    if (mounted) {
      setState(() {
        _displayName = widget.user.name;
        _nickname = widget.user.nickname;
        _email = widget.user.email;
      });
    }

    print(_displayName);
  }
}
