// import 'dart:async';
// import 'dart:convert';
// import 'dart:io';

// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:cloud_functions/cloud_functions.dart';
// import 'package:comoo/src/blocs/profile_personal_bloc.dart';
// import 'package:comoo/src/comoo/authentication.dart';
// import 'package:comoo/src/comoo/bank.dart';
// import 'package:comoo/src/comoo/user.dart';
// import 'package:comoo/src/comoo/validations.dart';
// import './access_form.dart';
// import './bank_form.dart';
// import './personal_form.dart';
// import 'package:comoo/src/provider/bloc_provider.dart';
// import 'package:comoo/src/utility/controller.dart';
// import 'package:comoo/src/utility/states_list.dart';
// import 'package:comoo/src/utility/style.dart';
// import 'package:comoo/src/widgets/loading_container.dart';
// import 'package:comoo/src/widgets/reusables.dart';
// import 'package:comoo/src/widgets/round_appbar.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_storage/firebase_storage.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_masked_text/flutter_masked_text.dart';
// import 'package:http/http.dart';
// import 'package:http/http.dart' as http;
// import 'package:image_cropper/image_cropper.dart';
// import 'package:image_picker/image_picker.dart';
// // import 'package:masked_text/masked_text.dart';

// class UserProfilePage extends StatefulWidget {
//   //Temporary

//   @override
//   State<UserProfilePage> createState() => _UserProfilePageState();
// }

// class _UserProfilePageState extends State<UserProfilePage> {
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

//   final Firestore _db = Firestore.instance;
//   final UserAuth auth = UserAuth();

//   User _currentUser;

//   bool _loading = false;

//   Widget _buildForm(BuildContext context) {
//     final double mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Center(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: <Widget>[
//           Padding(
//             padding: EdgeInsets.only(top: mediaQuery * 20.0),
//           ),
//           AccessForm(
//             scaffoldKey: _scaffoldKey,
//             user: _currentUser,
//           ),
//           BlocProvider<ProfilePersonalBloc>(
//             bloc: ProfilePersonalBloc.seeded(
//               user: _currentUser,
//               states: states
//                   .map((dynamic state) => StateItem.fromJSON(state))
//                   .toList()
//                     ..sort(
//                         (StateItem a, StateItem b) => a.name.compareTo(b.name)),
//             ),
//             child: PersonalForm(
//                 states: states
//                     .map((dynamic state) => StateItem.fromJSON(state))
//                     .toList()
//                       ..sort((StateItem a, StateItem b) =>
//                           a.name.compareTo(b.name))),
//           ),
//           StreamBuilder<List<Bank>>(
//             stream: fetchBanks(),
//             // initialData: initialData ,
//             builder:
//                 (BuildContext context, AsyncSnapshot<List<Bank>> snapshot) {
//               return snapshot.hasData
//                   ? BankForm(user: _currentUser, banks: snapshot.data)
//                   : Container();
//             },
//           ),
//         ],
//       ),
//     );
//   }

//   Stream<List<Bank>> fetchBanks() async* {
//     // List<Bank> _banks = <Bank>[];
//     yield await _db
//         .collection('banks')
//         .orderBy('name')
//         .getDocuments()
//         .then((snapshot) {
//       final List<Bank> _banks = snapshot.documents.map((doc) {
//         final Bank b = Bank.fromSnapshot(doc);
//         return b;
//       }).toList();
//       _banks.add(Bank(number: '0', name: 'selecione'));
//       _banks.sort((Bank a, Bank b) => a.number.compareTo(b.number));

//       return _banks;
//     }).catchError((dynamic onError) {
//       print(onError);
//       return <Bank>[];
//     });
//   }

//   @override
//   void initState() {
//     if (mounted) {
//       setState(() {
//         _loading = true;
//       });
//     }
//     _fetchCurrentUser();
//     super.initState();
//   }

//   Future<void> _fetchCurrentUser() async {
//     _currentUser = await auth.fetchCurrentUser();
//     if (mounted) {
//       setState(() {
//         _loading = false;
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         // FocusScope.of(context).requestFocus(FocusNode());
//         // SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
//       },
//       child: Scaffold(
//           key: _scaffoldKey,
//           appBar: CustomAppBar.round(
//             color: ComooColors.chumbo,
//             title: Text('perfil', style: ComooStyles.appBarTitleSixteen),
//           ),
//           body: _loading
//               ? LoadingContainer()
//               : SingleChildScrollView(child: _buildForm(context))),
//     );
//   }
// }

// class OldUserProfilePage extends StatefulWidget {
//   OldUserProfilePage({Key key}) : super(key: key);

//   @override
//   State createState() {
//     return UserProfilePageState();
//   }
// }

// class UserProfilePageState extends State<OldUserProfilePage>
//     with Validations, Reusables {
//   // BehaviorSubject<bool> _editAccessData = BehaviorSubject<bool>();
//   // Function(bool) get changeEditAccessData => _editAccessData.sink.add;
//   // Stream<bool> get editAccessData => _editAccessData.stream;
//   // BehaviorSubject<bool> _editPersonalData = BehaviorSubject<bool>();
//   // Function(bool) get changeEditPersonalData => _editPersonalData.sink.add;
//   // Stream<bool> get editPersonalData => _editPersonalData.stream;
//   // BehaviorSubject<bool> _editBankData = BehaviorSubject<bool>();
//   // Function(bool) get changeEditBankData => _editBankData.sink.add;
//   // Stream<bool> get editBankData => _editAccessData.stream;

//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   final GlobalKey<FormState> _formKeyAccess = GlobalKey<FormState>();
//   final GlobalKey<FormState> _formKeyPayment = GlobalKey<FormState>();
//   final FocusNode stateFocus = FocusNode();
//   final Firestore _db = Firestore.instance;
//   final UserAuth auth = UserAuth();

//   List<StateItem> statesList = <StateItem>[];
//   List<String> citiesList = <String>[];
//   Validations _validations = Validations();
//   List<Bank> banks = <Bank>[];
//   List<BankAccount> bankAccounts = <BankAccount>[
//     BankAccount(name: 'Conta Corrente', type: AccountType.checking),
//     BankAccount(name: 'Conta Poupança', type: AccountType.savings),
//   ];
//   BankAccount _account;
//   Bank _bank;
//   User _currentUser;

//   String _displayName = '';
//   String _nickname = '';
//   bool _validateNickname = false;
//   String _validateNicknameMessage = '';
//   String _email = '';
//   String _password = '******';
//   String _phoneCountryCode = '55';
//   String _phoneNumber;
//   // String _phone;
//   MaskedTextController _phoneController =
//       ComooController.areaCodeAndPhoneNumberController;
//   bool _validatePhone = false;
//   bool _isBanksEnabled = true;
//   String _validatePhoneMessage = '';
//   String _phoneAreaCode;
//   // String _birthDate;
//   MaskedTextController _birthDateController = ComooController.dateController;
//   bool _validateBirthDate = false;
//   String _validateBirthDateMessage = '';
//   // String _cpf;
//   MaskedTextController _cpfController = ComooController.cpfController;
//   bool _validateCpf = false;
//   String _validateCpfMessage = '';
//   // String _zip;
//   final TextEditingController stateController = TextEditingController();
//   final TextEditingController cityController = TextEditingController();
//   TextEditingController _addressController = TextEditingController();
//   TextEditingController _complementController = TextEditingController();
//   TextEditingController _districtController = TextEditingController();
//   MaskedTextController _zipController = ComooController.zipController;
//   bool _validateZip = false;
//   String _validateZipMessage = '';
//   // String _address;
//   // String _district;
//   String _addressNumber;
//   // String _complement;
//   String _city;
//   StateItem _state;
//   String _stateInitial;

//   final TextEditingController bankController = TextEditingController();
//   final TextEditingController accountTypeController = TextEditingController();
//   // String _bankAccountType;
//   String _agencyNumber = '';
//   String _agencyDigit = '';
//   // MaskedTextController _agencyController;
//   // bool _validateAgency = false;
//   // String _bankNumber;
//   String _accountNumber = '';
//   String _accountDigit = '';
//   // MaskedTextController _accountController;
//   // bool _validateAccount = false;

//   File _newPhoto;
//   bool isUploading = true;
//   bool _isLoading = false;
//   bool _hasChangesAccess = false;
//   bool _hasChangesPayment = false;
//   bool _isSaving = false;
//   bool _autoValidateAccess = false;
//   bool _autoValidatePersonal = false;
//   bool _autoValidatePayment = false;

//   void showInSnackBar(String value) {
//     _scaffoldKey.currentState.showSnackBar(SnackBar(
//       content: Text(value, style: ComooStyles.textoSnackBar),
//       duration: const Duration(seconds: 10),
//     ));
//   }

//   Widget _buildAvatar(String _photoUrl) {
//     final double mediaQuery = MediaQuery.of(context).size.width / 400;
//     return GestureDetector(
//       onTap: () async {
//         var result = await showDialog<ImageSource>(
//             context: context,
//             builder: (BuildContext context) {
//               return SimpleDialog(
//                 title: const Text(
//                   'ESCOLHA ORIGEM',
//                   style: ComooStyles.appBarTitle,
//                 ),
//                 children: <Widget>[
//                   SimpleDialogOption(
//                       onPressed: () {
//                         Navigator.pop(context, ImageSource.camera);
//                       },
//                       child: Row(
//                         children: <Widget>[
//                           const Icon(
//                             ComooIcons.camera,
//                             color: ComooColors.roxo,
//                           ),
//                           Padding(
//                               padding: EdgeInsets.only(left: mediaQuery * 20.0),
//                               child: Text(
//                                 'Camera',
//                                 style: ComooStyles.botoes,
//                               )),
//                         ],
//                       )),
//                   SimpleDialogOption(
//                     onPressed: () {
//                       Navigator.pop(context, ImageSource.gallery);
//                     },
//                     child: Row(
//                       children: <Widget>[
//                         Icon(
//                           ComooIcons.galeria,
//                           color: ComooColors.roxo,
//                           size: mediaQuery * 34.0,
//                         ),
//                         Padding(
//                             padding: EdgeInsets.only(left: mediaQuery * 10.0),
//                             child: const Text(
//                               'Galeria de Fotos',
//                               style: ComooStyles.botoes,
//                             )),
//                       ],
//                     ),
//                   )
//                 ],
//               );
//             });
//         if (result == null) {
//           return;
//         }

//         getImage(result);
//       },
//       child: Container(
//         height: mediaQuery * 150.0,
//         width: mediaQuery * 150.0,
//         decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(mediaQuery * 100.0),
//             border: Border.all(
//                 width: mediaQuery * 1.2, color: ComooColors.cinzaClaro),
//             image: DecorationImage(
//                 image: _newPhoto == null
//                     ? CachedNetworkImageProvider(_photoUrl)
//                     : FileImage(_newPhoto),
//                 fit: BoxFit.cover)),
//       ),
//     );
//   }

//   Future<String> upload(File file) async {
//     final StorageReference ref = FirebaseStorage.instance
//         .ref()
//         .child('/users/${currentUser.uid}/avatar.jpg');
//     final StorageTaskSnapshot uploadTask = await ref.putFile(file).onComplete;
//     final String result = await uploadTask.ref.getDownloadURL().then((onValue) {
//       return onValue.toString();
//     }).catchError((onError) {
//       print(onError);
//       return '';
//     });
//     return result;
//   }

//   Future<File> getImage(ImageSource source) async {
//     if (!(await checkPermissions(source))) {
//       return null;
//     }
//     final File image = await ImagePicker.pickImage(source: source);

//     if (image == null) {
//       return null;
//     }
//     final File croppedFile = await ImageCropper.cropImage(
//         sourcePath: image.path,
//         ratioX: 1.0,
//         ratioY: 1.0,
//         maxWidth: 512,
//         maxHeight: 512,
//         toolbarColor: ComooColors.roxo,
//         toolbarTitle: 'COMOO');

//     setState(() {
//       isUploading = true;
//       _newPhoto = croppedFile;
//     });

// //    var newImg = await compressImage(image);

//     final String url = await upload(croppedFile);
//     currentUser.photoURL = url;
//     final UserUpdateInfo userUpdateInfo = UserUpdateInfo();
//     userUpdateInfo.photoUrl = url;
//     final FirebaseUser firebaseUser = await FirebaseAuth.instance.currentUser();
//     await firebaseUser.updateProfile(userUpdateInfo);
//     await Firestore.instance
//         .collection('users')
//         .document(currentUser.uid)
//         .updateData({'photoURL': url});

//     setState(() {
//       _newPhoto = croppedFile;
//       isUploading = false;
//     });

//     return croppedFile;
//   }

//   // Future<void> _setUserAccessInformation(
//   //     {String displayName, String nickname, String email}) {
//   //   final String uid = currentUser.uid;
//   //   final WriteBatch batch = _db.batch();
//   //   final DocumentReference userDocRef = _db.collection('users').document(uid);
//   //   if (email != null) {
//   //     batch.updateData(userDocRef, <String, dynamic>{
//   //       'name': displayName,
//   //       'nickname': nickname,
//   //       'email': email,
//   //     });
//   //   } else {
//   //     batch.updateData(userDocRef, <String, dynamic>{
//   //       'name': displayName,
//   //       'nickname': nickname,
//   //     });
//   //   }

//   //   return batch.commit();
//   // }

//   // Future<void> _handleSaveAccess(BuildContext context, ProfileBloc bloc) async {
//   //   setState(() {
//   //     _isLoading = true;
//   //     _validateNickname = false;
//   //   });
//   //   final FormState form = _formKeyAccess.currentState;
//   //   if (!form.validate()) {
//   //     _autoValidateAccess = true;
//   //     showInSnackBar('Verifique os erros antes de continuar.');
//   //     setState(() {
//   //       _isLoading = false;
//   //     });
//   //   } else {
//   //     form.save();

//   //     final String nicknameValidation =
//   //         await _nicknameDuplicateValidation(_nickname);
//   //     if (nicknameValidation != null && _nickname != currentUser.nickname) {
//   //       showInSnackBar('Verifique os erros antes de continuar.');
//   //       setState(() {
//   //         _validateNicknameMessage = nicknameValidation.toString();
//   //         _validateNickname = true;
//   //         _isLoading = false;
//   //       });
//   //     } else {
//   //       _saveAccess(context, bloc);
//   //     }
//   //   }
//   // }

//   // Future<void> _saveAccess(BuildContext context, ProfileBloc bloc) async {
//   //   print('nova senha: $_password');
//   //   final String result = await _showPasswordDialog(context);
//   //   if (result != '' && result != null) {
//   //     try {
//   //       final UserData newUser = UserData(
//   //         email: currentUser.email,
//   //         password: result,
//   //       );
//   //       await auth.verifyUser(newUser);
//   //       if (_email != currentUser.email) {
//   //         await auth.updateEmail(_email);
//   //         _setUserAccessInformation(
//   //           displayName: _displayName,
//   //           nickname: _nickname,
//   //           email: _email,
//   //         );
//   //       } else {
//   //         _setUserAccessInformation(
//   //           displayName: _displayName,
//   //           nickname: _nickname,
//   //         );
//   //       }
//   //       if (_password.isNotEmpty &&
//   //           _password != result &&
//   //           _password != '******') {
//   //         auth.updatePassword(_password);
//   //       }

//   //       setState(() {
//   //         currentUser.name = _displayName;
//   //         _isLoading = false;
//   //       });
//   //       _updatedData();
//   //     } on PlatformException catch (e) {
//   //       print('>> PlatformException $e');
//   //       switch (e.code) {
//   //         case 'ERROR_WRONG_PASSWORD':
//   //           showInSnackBar('Senha errada!');
//   //           break;
//   //         default:
//   //           showInSnackBar('Algo deu errado, tente novamente!');
//   //           break;
//   //       }
//   //     } catch (error) {
//   //       print(error);
//   //       if (error.message ==
//   //           'The email address is already in use by another account.') {
//   //         showInSnackBar('E-mail já está em uso.');
//   //       } else {
//   //         showInSnackBar('Algo deu errado, tente novamente.');
//   //       }
//   //     }

//   //     if (mounted) {
//   //       setState(() {
//   //         _isLoading = false;
//   //       });
//   //     }
//   //   } else {
//   //     // print('password null or empty!');
//   //     // showInSnackBar('Digite uma senha para continuar');
//   //     setState(() {
//   //       _isLoading = false;
//   //     });
//   //   }
//   //   //bloc.changeEditAccessData(false);
//   // }

//   // Future<String> _showPasswordDialog(BuildContext context) {
//   //   final double mediaQuery = MediaQuery.of(context).size.width / 400;
//   //   String _password = '';
//   //   return showCustomDialog<String>(
//   //     context: context,
//   //     barrierDismissible: true,
//   //     alert: customRoundAlert(
//   //       context: context,
//   //       title: 'Insira sua senha para validar os dados de acesso',
//   //       subtitle: Container(
//   //         padding: EdgeInsets.symmetric(
//   //           horizontal: mediaQuery * 31.0,
//   //         ),
//   //         child: TextField(
//   //           style: TextStyle(
//   //             color: Colors.white,
//   //             fontSize: mediaQuery * 18.0,
//   //           ),
//   //           decoration: InputDecoration(
//   //             hintText: 'mínimo 6 dígitos',
//   //             hintStyle: TextStyle(
//   //               color: Color.fromRGBO(255, 255, 255, 0.65),
//   //             ),
//   //             enabledBorder: UnderlineInputBorder(
//   //               borderSide: BorderSide(
//   //                 color: ComooColors.white500,
//   //                 style: BorderStyle.solid,
//   //               ),
//   //             ),
//   //           ),
//   //           keyboardType: TextInputType.emailAddress,
//   //           obscureText: true,
//   //           onChanged: (String pass) {
//   //             _password = pass;
//   //           },
//   //           onSubmitted: (value) {
//   //             // _resetPassword(value);
//   //             Navigator.of(context).pop(value);
//   //           },
//   //         ),
//   //       ),
//   //       actions: [
//   //         Container(
//   //           margin: EdgeInsets.symmetric(
//   //             horizontal: mediaQuery * 40.0,
//   //             vertical: mediaQuery * 10.0,
//   //           ),
//   //           child: OutlineButton(
//   //             child: Text(
//   //               'ENVIAR',
//   //               style: TextStyle(color: Colors.white),
//   //             ),
//   //             onPressed: () {
//   //               Navigator.of(context).pop(_password);
//   //               // _resetPassword(_password);
//   //             },
//   //             borderSide: BorderSide(color: ComooColors.white500),
//   //             shape: RoundedRectangleBorder(
//   //               borderRadius: BorderRadius.circular(mediaQuery * 20.0),
//   //             ),
//   //           ),
//   //         ),
//   //       ],
//   //     ),
//   //   );
//   // }

//   // Future<String> _nicknameDuplicateValidation(String nickname) async {
//   //   return await _db
//   //       .collection('users')
//   //       .where('nickname', isEqualTo: nickname)
//   //       .getDocuments()
//   //       .then((docs) {
//   //     // print('IsEmpty? ${docs.documents.isEmpty}');
//   //     return docs.documents.isEmpty ? null : 'Este nome de usuário já existe';
//   //   }).catchError((onError) {
//   //     return null;
//   //   });
//   // }

//   // Widget _buildAccessCategory(BuildContext context) {
//   //   final double mediaQuery = MediaQuery.of(context).size.width / 400;
//   //   ProfileBloc bloc = Provider.of(context).profile;
//   //   // TextStyle prefixStyle = TextStyle(
//   //   //   fontFamily: ComooStyles.fontFamily,
//   //   //   color: ComooColors.cinzaClaro,
//   //   //   fontSize: mediaQuery * 15.0,
//   //   //   fontWeight: FontWeight.w400,
//   //   // );
//   //   final TextStyle textFormStyle = TextStyle(
//   //     fontFamily: ComooStyles.fontFamily,
//   //     color: ComooColors.chumbo,
//   //     fontSize: mediaQuery * 15.0,
//   //     fontWeight: FontWeight.w500,
//   //   );
//   //   // TextStyle hintStyle = TextStyle(
//   //   //   fontFamily: ComooStyles.fontFamily,
//   //   //   color: ComooColors.cinzaClaro,
//   //   //   fontSize: mediaQuery * 15.0,
//   //   //   fontWeight: FontWeight.w400,
//   //   // );
//   //   final TextStyle labelFormStyle = TextStyle(
//   //     fontFamily: ComooStyles.fontFamily,
//   //     color: ComooColors.roxo,
//   //     fontSize: mediaQuery * 15.0,
//   //     fontWeight: FontWeight.w500,
//   //   );
//   //   // InputBorder inputBorder = ComooStyles.underLineBorder;
//   //   return StreamBuilder<bool>(
//   //     initialData: true,
//   //     stream: bloc.editAccessData,
//   //     builder: (BuildContext editContext, AsyncSnapshot<bool> snapshot) {
//   //       return Form(
//   //         onChanged: () {
//   //           if (mounted) {
//   //             print(_hasChangesAccess);
//   //             setState(() {
//   //               _hasChangesAccess = true;
//   //             });
//   //           }
//   //         },
//   //         key: _formKeyAccess,
//   //         autovalidate: _autoValidateAccess,
//   //         child: MenuCategory(
//   //           title: 'DADOS DE ACESSO',
//   //           padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
//   //           children: <Widget>[
//   //             TextFormField(
//   //               style: textFormStyle,
//   //               decoration: ComooStyles.inputCustomDecoration('NOME COMPLETO'),
//   //               initialValue: _displayName,
//   //               textCapitalization: TextCapitalization.words,
//   //               enabled: snapshot.data,
//   //               onSaved: (String fullName) {
//   //                 _displayName = fullName.trim();
//   //               },
//   //               validator: validateEmpty,
//   //             ),
//   //             TextFormField(
//   //               // decoration: ComooStyles.inputDecoration('NOME DE USUÁRIO'),
//   //               decoration: InputDecoration(
//   //                 labelText: 'NOME DE USUÁRIO',
//   //                 helperStyle: ComooStyles.texto,
//   //                 focusedBorder: ComooStyles.underLineBorder,
//   //                 labelStyle: labelFormStyle,
//   //                 border: ComooStyles.underLineBorder,
//   //                 errorText:
//   //                     _validateNickname ? _validateNicknameMessage : null,
//   //               ),
//   //               enabled: snapshot.data,
//   //               style: textFormStyle,
//   //               validator: validateNickname,
//   //               initialValue: _nickname,
//   //               keyboardType: TextInputType.text,
//   //               onSaved: (String nickname) {
//   //                 _nickname = nickname.trim();
//   //               },
//   //             ),
//   //             TextFormField(
//   //               initialValue: _email,
//   //               keyboardType: TextInputType.emailAddress,
//   //               validator: validateEmail,
//   //               enabled: snapshot.data,
//   //               onSaved: (String email) {
//   //                 _email = email;
//   //               },
//   //               style: textFormStyle,
//   //               decoration: ComooStyles.inputCustomDecoration('E-MAIL'),
//   //             ),
//   //             TextFormField(
//   //               keyboardType: TextInputType.text,
//   //               style: ComooStyles.inputText,
//   //               initialValue: _password,
//   //               decoration: ComooStyles.inputCustomDecoration('SENHA'),
//   //               obscureText: true,
//   //               // validator: validatePassword,
//   //               enabled: snapshot.data,
//   //               onSaved: (String password) {
//   //                 _password = password;
//   //               },
//   //             ),
//   //             Container(
//   //               alignment: FractionalOffset.center,
//   //               margin: EdgeInsets.only(bottom: mediaQuery * 30.0),
//   //               child: OutlineButton(
//   //                 shape: RoundedRectangleBorder(
//   //                     borderRadius: BorderRadius.circular(mediaQuery * 30.0)),
//   //                 borderSide: BorderSide(color: ComooColors.roxo),
//   //                 highlightElevation: mediaQuery * 10.0,
//   //                 padding: EdgeInsets.symmetric(
//   //                     horizontal: mediaQuery * 58.0,
//   //                     vertical: mediaQuery * 8.0),
//   //                 child: const Text(
//   //                   'SALVAR',
//   //                   style: ComooStyles.botoes,
//   //                 ),
//   //                 onPressed: snapshot.data
//   //                     ? _hasChangesAccess
//   //                         ? () {
//   //                             _handleSaveAccess(context, bloc);
//   //                           }
//   //                         : null
//   //                     : null,
//   //               ),
//   //             ),
//   //           ],
//   //         ),
//   //       );
//   //     },
//   //   );
//   // }

//   // void selectCities(String initial) {
//   //   StateItem state = statesList.firstWhere((s) {
//   //     return s.initials == initial;
//   //   });
//   //   _state = state.name;
//   //   citiesList = state.cities;
//   // }

//   Future<bool> sendPaymentInformation() async {
//     final String phone = _phoneController.text;
//     _phoneAreaCode = phone.substring(1, 3);
//     _phoneNumber = phone.substring(5, phone.length).replaceAll(r'-', '');
//     final String agency = _agencyNumber +
//         (_agencyDigit.isEmpty ? _agencyDigit : '-' + _agencyDigit);
//     final String account = _accountNumber +
//         (_accountDigit.isEmpty ? _accountDigit : '-' + _accountDigit);
//     Map<String, dynamic> param = <String, dynamic>{
//       'birthDate': _birthDateController.text,
//       'cpf': _cpfController.text,
//       'phone': <String, String>{
//         'countryCode': _phoneCountryCode,
//         'number': _phoneNumber,
//         'areaCode': _phoneAreaCode,
//       },
//       'address': <String, String>{
//         'zipCode': _zipController.text,
//         'street': _addressController.text,
//         'district': _districtController.text,
//         'complement': _complementController.text,
//         'number': _addressNumber == null
//             ? '0'
//             : _addressNumber.isEmpty ? '0' : _addressNumber,
//         'city': cityController.text,
//         'uf': _state.initials,
//         'state': _state.name,
//         'country': 'BRA',
//       },
//       'bank': <String, String>{
//         'number': _bank.number,
//         'type': _account.type == AccountType.savings ? 'SAVING' : 'CHECKING',
//         'agency': agency,
//         'account': account,
//       },
//     };

//     print(param);

//     final bool result = await CloudFunctions.instance
//         .call(
//           functionName: 'createAccount',
//           parameters: param,
//         )
//         .timeout(const Duration(seconds: 60))
//         .then((result) {
//       if (mounted) {
//         setState(() {
//           _isBanksEnabled = false;
//         });
//       }
//       return true;
//     }).catchError((error) {
//       print(error);
//       return false;
//     });
//     return result;
//   }

//   // Future<void> _handleSavePaymentInformation() async {
//   //   setState(() {
//   //     _isLoading = true;
//   //     _validateCpf = false;
//   //     _validatePhone = false;
//   //     _validateZip = false;
//   //     _validateBirthDate = false;
//   //   });
//   //   final FormState form = _formKeyPayment.currentState;
//   //   final String cpfValidation = _validations.validateCPF(_cpfController.text);
//   //   final String phoneValidation =
//   //       _validations.validatePhone(_phoneController.text);
//   //   final String birthDateValidation =
//   //       _validations.validateBirthDate(_birthDateController.text);
//   //   final String zipValidation =
//   //       _validations.validateEmpty(_zipController.text);

//   //   if (_bank == null) {
//   //     showInSnackBar('Selecione o tipo de conta antes de continuar.');
//   //     setState(() {
//   //       _isLoading = false;
//   //     });
//   //   } else if (_account == null) {
//   //     showInSnackBar('Selecione o banco antes de continuar.');
//   //     setState(() {
//   //       _isLoading = false;
//   //     });
//   //   } else if (cpfValidation != null) {
//   //     showInSnackBar('Verifique os erros antes de continuar.');
//   //     setState(() {
//   //       _validateCpfMessage = cpfValidation;
//   //       _validateCpf = true;
//   //       _isLoading = false;
//   //     });
//   //   } else if (phoneValidation != null) {
//   //     showInSnackBar('Verifique os erros antes de continuar.');
//   //     setState(() {
//   //       _validatePhoneMessage = phoneValidation;
//   //       _validatePhone = true;
//   //       _isLoading = false;
//   //     });
//   //   } else if (birthDateValidation != null) {
//   //     showInSnackBar('Verifique os erros antes de continuar.');
//   //     setState(() {
//   //       _validateBirthDateMessage = birthDateValidation;
//   //       _validateBirthDate = true;
//   //       _isLoading = false;
//   //     });
//   //   } else if (zipValidation != null) {
//   //     showInSnackBar('Verifique os erros antes de continuar.');
//   //     setState(() {
//   //       _validateZipMessage = zipValidation;
//   //       _validateZip = true;
//   //       _isLoading = false;
//   //     });
//   //   } else if (!form.validate()) {
//   //     _autoValidateAccess = true;
//   //     showInSnackBar('Verifique os erros antes de continuar.');
//   //     setState(() {
//   //       _isLoading = false;
//   //     });
//   //     // } else if(_zipController.value) {
//   //   } else {
//   //     form.save();

//   //     if ((await sendPaymentInformation()) ?? false) {
//   //       // print('oi');
//   //       _updatedData();
//   //     } else {
//   //       showInSnackBar('Falha ao enviar os dados, tente novamente mais tarde.');
//   //     }

//   //     setState(() {
//   //       _isLoading = false;
//   //     });
//   //   }
//   // }

//   // Future<void> _updatedData() {
//   //   final double mediaQuery = MediaQuery.of(context).size.width / 400;
//   //   return showCustomDialog<void>(
//   //     context: context,
//   //     alert: customRoundAlert(
//   //       context: context,
//   //       title: 'Seus dados foram enviados com sucesso :)',
//   //       actions: [
//   //         Container(
//   //           height: mediaQuery * 25.0,
//   //           width: mediaQuery * 110.0,
//   //           decoration: BoxDecoration(
//   //             border: Border.all(width: 1.0, color: Colors.white),
//   //             borderRadius: BorderRadius.circular(mediaQuery * 20.0),
//   //           ),
//   //           child: FlatButton(
//   //             shape: RoundedRectangleBorder(
//   //                 borderRadius: BorderRadius.circular(mediaQuery * 20.0)),
//   //             onPressed: () {
//   //               Navigator.pop(context);
//   //             },
//   //             child: Text(
//   //               'FECHAR',
//   //               style: TextStyle(color: Colors.white),
//   //             ),
//   //           ),
//   //         ),
//   //       ],
//   //     ),
//   //   );
//   // }

//   // Widget _buildPersonalCategory(BuildContext context) {
//   //   return PersonalForm(user: currentUser, states: statesList);
//   // final double mediaQuery = MediaQuery.of(context).size.width / 400;
//   // ProfileBloc bloc = Provider.of(context).profile;
//   // final TextStyle prefixStyle = TextStyle(
//   //   fontFamily: ComooStyles.fontFamily,
//   //   color: ComooColors.cinzaClaro,
//   //   fontSize: mediaQuery * 15.0,
//   //   fontWeight: FontWeight.w400,
//   // );
//   // final TextStyle textFormStyle = TextStyle(
//   //   fontFamily: ComooStyles.fontFamily,
//   //   color: ComooColors.chumbo,
//   //   fontSize: mediaQuery * 15.0,
//   //   fontWeight: FontWeight.w500,
//   // );
//   // final TextStyle hintStyle = TextStyle(
//   //   fontFamily: ComooStyles.fontFamily,
//   //   color: ComooColors.cinzaClaro,
//   //   fontSize: mediaQuery * 15.0,
//   //   fontWeight: FontWeight.w400,
//   // );
//   // final TextStyle labelFormStyle = TextStyle(
//   //   fontFamily: ComooStyles.fontFamily,
//   //   color: ComooColors.roxo,
//   //   fontSize: mediaQuery * 15.0,
//   //   fontWeight: FontWeight.w500,
//   // );
//   // final InputBorder inputBorder = ComooStyles.underLineBorder;
//   // return StreamBuilder<bool>(
//   //     initialData: false,
//   //     stream: bloc.editPersonalData,
//   //     builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
//   //       return MenuCategory(
//   //           title: 'DADOS PESSOAIS',
//   //           icon: IconButton(
//   //             icon: Icon(Icons.edit),
//   //             color: snapshot.data
//   //                 ? COMOO.colors.purple
//   //                 : COMOO.colors.turquoise,
//   //             onPressed: () {
//   //               bloc.changeEditPersonalData(!snapshot.data);
//   //             },
//   //           ),
//   //           padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
//   //           children: <Widget>[
//   //             TextField(
//   //               style: textFormStyle,
//   //               decoration: InputDecoration(
//   //                 labelText: 'CELULAR',
//   //                 prefix: Container(
//   //                   padding: EdgeInsets.only(right: mediaQuery * 5.0),
//   //                   child: Text(
//   //                     '+55',
//   //                     style: prefixStyle,
//   //                   ),
//   //                 ),
//   //                 isDense: true,
//   //                 hintText: '(00) 99999-0000',
//   //                 hintStyle: hintStyle,
//   //                 focusedBorder: inputBorder,
//   //                 labelStyle: labelFormStyle,
//   //                 border: inputBorder,
//   //                 errorText: _validatePhone ? _validatePhoneMessage : null,
//   //               ),
//   //               enabled: snapshot.data,
//   //               controller: _phoneController,
//   //               keyboardType: TextInputType.number,
//   //               onChanged: (String phone) {
//   //                 // this._phone = phone.trim();
//   //                 setState(() {
//   //                   _phoneController.updateText(phone);
//   //                 });
//   //               },
//   //             ),
//   //             TextField(
//   //               style: textFormStyle,
//   //               decoration: InputDecoration(
//   //                 labelText: 'DATA DE NASCIMENTO',
//   //                 hintText: '00/00/0000',
//   //                 hintStyle: hintStyle,
//   //                 focusedBorder: inputBorder,
//   //                 labelStyle: labelFormStyle,
//   //                 border: inputBorder,
//   //                 errorText:
//   //                     _validateBirthDate ? _validateBirthDateMessage : null,
//   //               ),
//   //               enabled: snapshot.data,
//   //               keyboardType: TextInputType.number,
//   //               controller: _birthDateController,
//   //               onChanged: (String value) {
//   //                 // this._birthDate = value.trim();
//   //                 setState(() {
//   //                   _birthDateController.updateText(value);
//   //                 });
//   //               },
//   //             ),
//   //             TextField(
//   //               decoration: InputDecoration(
//   //                 labelText: 'CPF',
//   //                 isDense: true,
//   //                 helperStyle: ComooStyles.texto,
//   //                 focusedBorder: ComooStyles.underLineBorder,
//   //                 labelStyle: labelFormStyle,
//   //                 border: ComooStyles.underLineBorder,
//   //                 errorText: _validateCpf ? _validateCpfMessage : null,
//   //               ),
//   //               autocorrect: false,
//   //               controller: _cpfController,
//   //               enabled: snapshot.data,
//   //               keyboardType: TextInputType.number,
//   //               onChanged: (String value) {
//   //                 // _cpf = value;
//   //                 setState(() {
//   //                   _cpfController.updateText(value);
//   //                 });
//   //               },
//   //             ),
//   //             TextField(
//   //               style: textFormStyle,
//   //               decoration: InputDecoration(
//   //                 labelText: 'CEP',
//   //                 isDense: true,
//   //                 helperStyle: ComooStyles.texto,
//   //                 focusedBorder: ComooStyles.underLineBorder,
//   //                 labelStyle: labelFormStyle,
//   //                 border: ComooStyles.underLineBorder,
//   //                 errorText: _validateZip ? _validateZipMessage : null,
//   //               ),
//   //               controller: _zipController,
//   //               enabled: snapshot.data,
//   //               keyboardType: TextInputType.number,
//   //               onChanged: (String zip) {
//   //                 // _zipController.text = zip;
//   //                 setState(() {
//   //                   _zipController.updateText(zip);
//   //                 });
//   //               },
//   //             ),
//   //             TextFormField(
//   //               style: textFormStyle,
//   //               decoration: ComooStyles.inputCustomDecoration('endereço'),
//   //               // initialValue: this._address,
//   //               controller: _addressController,
//   //               enabled: snapshot.data,
//   //               onSaved: (String address) {
//   //                 // _addressController.text = address.trim();
//   //               },
//   //               textCapitalization: TextCapitalization.words,
//   //               validator: _validations.validateEmpty,
//   //             ),
//   //             TextFormField(
//   //               style: textFormStyle,
//   //               decoration: ComooStyles.inputCustomDecoration('número'),
//   //               initialValue: _addressNumber,
//   //               enabled: snapshot.data,
//   //               keyboardType: TextInputType.number,
//   //               onSaved: (String value) {
//   //                 _addressNumber = value.trim();
//   //               },
//   //               // validator: _validations.validateEmpty,
//   //             ),
//   //             TextFormField(
//   //               style: textFormStyle,
//   //               decoration: ComooStyles.inputCustomDecoration('complemento'),
//   //               textCapitalization: TextCapitalization.sentences,
//   //               // initialValue: _complement,
//   //               controller: _complementController,
//   //               enabled: snapshot.data,
//   //               onSaved: (String complement) {
//   //                 // _complement = complement.trim();
//   //               },
//   //             ),
//   //             TextFormField(
//   //               style: textFormStyle,
//   //               decoration: ComooStyles.inputCustomDecoration('bairro'),
//   //               textCapitalization: TextCapitalization.words,
//   //               // initialValue: this._district,
//   //               controller: _districtController,
//   //               enabled: snapshot.data,
//   //               onSaved: (String address) {
//   //                 // this._district = address.trim();
//   //               },
//   //               validator: _validations.validateEmpty,
//   //             ),
//   //             SizedBox(height: mediaQuery * 5.0),
//   //             Container(
//   //               child: Stack(
//   //                 children: <Widget>[
//   //                   TextField(
//   //                     onTap: () async {
//   //                       int initialItem = statesList.indexOf(_state);
//   //                       initialItem = initialItem == -1 ? 0 : initialItem;
//   //                       stateFocus.unfocus();
//   //                       stateFocus.consumeKeyboardToken();
//   //                       await customCupertinoModal(
//   //                         context: context,
//   //                         initialItem: initialItem,
//   //                         onChange: (int index) {
//   //                           // changeState(statesList[index]);
//   //                           // changeCity('selecione');

//   //                           // _stateName = statesList[index].name;
//   //                           // selectCities(statesList[index].initials);
//   //                           setState(() {
//   //                             _state = statesList[index];
//   //                             stateController.text =
//   //                                 '${_state.initials} - ${_state.name}';
//   //                             _stateInitial = _state.initials;
//   //                             citiesList = _state.cities;
//   //                             // _city = 'selecione';
//   //                             cityController.text = 'selecione';
//   //                           });
//   //                         },
//   //                         children: List<Widget>.generate(statesList.length,
//   //                             (int index) {
//   //                           return Center(
//   //                             child: Text(statesList[index].name),
//   //                           );
//   //                         }),
//   //                       );
//   //                     },
//   //                     controller: stateController,
//   //                     focusNode: stateFocus,
//   //                     autofocus: false,
//   //                     enabled: snapshot.data,
//   //                     enableInteractiveSelection: false,
//   //                     textCapitalization: TextCapitalization.none,
//   //                     style: ComooStyles.inputText,
//   //                     decoration: InputDecoration(
//   //                       labelText: 'ESTADO',
//   //                       isDense: true,
//   //                       // disabledBorder: ComooStyles.underLineBorder,
//   //                       helperStyle: ComooStyles.texto,
//   //                       focusedBorder: ComooStyles.underLineBorder,
//   //                       labelStyle: ComooStyles.inputLabel
//   //                           .copyWith(color: COMOO.colors.purple),
//   //                       // border: ComooStyles.underLineBorder,
//   //                     ),
//   //                   ),
//   //                   Positioned(
//   //                     bottom: mediaQuery * 5.0,
//   //                     right: mediaQuery * 0.0,
//   //                     child: Icon(Icons.expand_more),
//   //                   ),
//   //                 ],
//   //               ),
//   //             ),
//   //             Container(
//   //               child: Stack(
//   //                 children: <Widget>[
//   //                   TextField(
//   //                     onTap: () async {
//   //                       int initialItem =
//   //                           citiesList.indexOf(cityController.text);
//   //                       initialItem = initialItem == -1 ? 0 : initialItem;
//   //                       stateFocus.unfocus();
//   //                       stateFocus.consumeKeyboardToken();
//   //                       await customCupertinoModal(
//   //                         context: context,
//   //                         initialItem: initialItem,
//   //                         onChange: (int index) {
//   //                           // _city = stateSnapshot.data?.cities[index];
//   //                           // changeCity(stateSnapshot.data?.cities[index]);
//   //                           setState(() {
//   //                             cityController.text = citiesList[index];
//   //                           });
//   //                         },
//   //                         children: List<Widget>.generate(
//   //                             citiesList.length ?? 0, (int index) {
//   //                           return Center(
//   //                             child: Text(citiesList[index]),
//   //                           );
//   //                         }),
//   //                       );
//   //                     },
//   //                     controller: cityController,
//   //                     focusNode: stateFocus,
//   //                     autofocus: false,
//   //                     enabled: snapshot.data,
//   //                     enableInteractiveSelection: false,
//   //                     textCapitalization: TextCapitalization.none,
//   //                     style: ComooStyles.inputText,
//   //                     decoration: InputDecoration(
//   //                       labelText: 'CIDADE',
//   //                       isDense: true,
//   //                       // disabledBorder: ComooStyles.underLineBorder,
//   //                       helperStyle: ComooStyles.texto,
//   //                       focusedBorder: ComooStyles.underLineBorder,
//   //                       labelStyle: ComooStyles.inputLabel
//   //                           .copyWith(color: COMOO.colors.purple),
//   //                       // border: ComooStyles.underLineBorder,
//   //                     ),
//   //                   ),
//   //                   Positioned(
//   //                     bottom: mediaQuery * 5.0,
//   //                     right: mediaQuery * 0.0,
//   //                     child: Icon(Icons.expand_more),
//   //                   ),
//   //                 ],
//   //               ),
//   //             ),
//   //             Container(
//   //               alignment: FractionalOffset.center,
//   //               margin: EdgeInsets.symmetric(vertical: mediaQuery * 20.0),
//   //               child: OutlineButton(
//   //                 shape: RoundedRectangleBorder(
//   //                     borderRadius: BorderRadius.circular(mediaQuery * 30.0)),
//   //                 borderSide: BorderSide(color: ComooColors.roxo),
//   //                 highlightElevation: mediaQuery * 10.0,
//   //                 padding: EdgeInsets.symmetric(
//   //                     horizontal: mediaQuery * 58.0,
//   //                     vertical: mediaQuery * 8.0),
//   //                 child: const Text(
//   //                   'SALVAR',
//   //                   style: ComooStyles.botoes,
//   //                 ),
//   //                 onPressed: snapshot.data
//   //                     ? () {
//   //                         _handleSavePersonal();
//   //                       }
//   //                     : null,
//   //               ),
//   //             ),
//   //             Container(
//   //               padding: EdgeInsets.all(10.0),
//   //             )
//   //           ]);
//   //     });
//   // }

//   Future<void> _handleSavePersonal() async {
//     print('save');
//   }

//   // Widget _buildBankCategory(BuildContext context) {
//   //   return BankForm(user: currentUser, banks: banks);
//   // }
//   // Widget _buildBankCategory() {
//   //   final double mediaQuery = MediaQuery.of(context).size.width / 400;
//   //   ProfileBloc bloc = Provider.of(context).profile;
//   //   // TextStyle prefixStyle = TextStyle(
//   //   //   fontFamily: ComooStyles.fontFamily,
//   //   //   color: ComooColors.cinzaClaro,
//   //   //   fontSize: mediaQuery * 15.0,
//   //   //   fontWeight: FontWeight.w400,
//   //   // );
//   //   final TextStyle textFormStyle = TextStyle(
//   //     fontFamily: ComooStyles.fontFamily,
//   //     color: ComooColors.chumbo,
//   //     fontSize: mediaQuery * 15.0,
//   //     fontWeight: FontWeight.w500,
//   //   );
//   //   // TextStyle hintStyle = TextStyle(
//   //   //   fontFamily: ComooStyles.fontFamily,
//   //   //   color: ComooColors.cinzaClaro,
//   //   //   fontSize: mediaQuery * 15.0,
//   //   //   fontWeight: FontWeight.w400,
//   //   // );
//   //   // TextStyle labelFormStyle = TextStyle(
//   //   //   fontFamily: ComooStyles.fontFamily,
//   //   //   color: ComooColors.turqueza,
//   //   //   fontSize: mediaQuery * 15.0,
//   //   //   fontWeight: FontWeight.w500,
//   //   // );
//   //   return StreamBuilder<bool>(
//   //     initialData: false,
//   //     stream: bloc.editBankData,
//   //     builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
//   //       return MenuCategory(
//   //         title: 'DADOS BANCÁRIOS',
//   //         padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
//   //         children: <Widget>[
//   //           Text(
//   //             'Insira seus dados bancários com atenção. Vamos\nutilizar essas informações para depositar seus\nganhos com vendas e com o lucro compartilhado.',
//   //             style: ComooStyles.listDescription,
//   //           ),
//   //           Container(
//   //             child: Stack(
//   //               children: <Widget>[
//   //                 TextField(
//   //                   onTap: () async {
//   //                     int initialItem = banks.indexOf(_bank);
//   //                     initialItem = initialItem == -1 ? 0 : initialItem;
//   //                     stateFocus.unfocus();
//   //                     stateFocus.consumeKeyboardToken();
//   //                     await customCupertinoModal(
//   //                       context: context,
//   //                       initialItem: initialItem,
//   //                       onChange: (int index) {
//   //                         setState(() {
//   //                           _bank = banks[index];
//   //                           bankController.text =
//   //                               '${_bank.number} - ${_bank.name}';
//   //                         });
//   //                       },
//   //                       children: List<Widget>.generate(banks.length ?? 0,
//   //                           (int index) {
//   //                         return Container(
//   //                           padding: EdgeInsets.symmetric(
//   //                               horizontal: mediaQuery * 30.0),
//   //                           child: Text(
//   //                             '${banks[index].number} - ${banks[index].name}',
//   //                             textAlign: TextAlign.left,
//   //                             overflow: TextOverflow.ellipsis,
//   //                             maxLines: 1,
//   //                           ),
//   //                         );
//   //                       }),
//   //                     );
//   //                   },
//   //                   controller: bankController,
//   //                   focusNode: stateFocus,
//   //                   autofocus: false,
//   //                   enabled: snapshot.data,
//   //                   enableInteractiveSelection: false,
//   //                   textCapitalization: TextCapitalization.none,
//   //                   style: ComooStyles.inputText,
//   //                   decoration: InputDecoration(
//   //                     labelText: 'BANCO',
//   //                     isDense: true,
//   //                     // disabledBorder: ComooStyles.underLineBorder,
//   //                     helperStyle: ComooStyles.texto,
//   //                     focusedBorder: ComooStyles.underLineBorder,
//   //                     labelStyle: ComooStyles.inputLabel
//   //                         .copyWith(color: COMOO.colors.purple),
//   //                     // border: ComooStyles.underLineBorder,
//   //                   ),
//   //                 ),
//   //                 Positioned(
//   //                   bottom: mediaQuery * 5.0,
//   //                   right: mediaQuery * 0.0,
//   //                   child: const Icon(Icons.expand_more),
//   //                 ),
//   //                 // Container(
//   //                 //   decoration: BoxDecoration(
//   //                 //       border: Border(
//   //                 //           bottom: BorderSide(color: Colors.black, width: 1.0))),
//   //                 //   child: DropdownButtonHideUnderline(
//   //                 //     child: Container(
//   //                 //       child: DropdownButton(
//   //                 //         value: this._bankNumber,
//   //                 //         hint: Text(_bankNumber ?? 'BANCO'),
//   //                 //         isExpanded: true,
//   //                 //         style: ComooStyles.inputLabel,
//   //                 //         items: banks
//   //                 //             .map((b) => DropdownMenuItem(
//   //                 //                   child: Text(
//   //                 //                     '${b.number} - ${b.name.toUpperCase()}',
//   //                 //                     style: textFormStyle,
//   //                 //                   ),
//   //                 //                   value: '${b.number}',
//   //                 //                 ))
//   //                 //             .toList(),
//   //                 //         onChanged: snapshot.data
//   //                 //             ? (value) {
//   //                 //                 setState(() {
//   //                 //                   this._bankNumber = value;
//   //                 //                 });
//   //                 //               }
//   //                 //             : null,
//   //                 //       ),
//   //                 //     ),
//   //                 //   ),
//   //                 // ),
//   //                 // Positioned(
//   //                 //   top: mediaQuery * 15.0,
//   //                 //   right: mediaQuery * 25.0,
//   //                 //   child: Text(
//   //                 //     'selecione',
//   //                 //     style: TextStyle(fontSize: mediaQuery * 12.0),
//   //                 //   ),
//   //                 // ),
//   //               ],
//   //             ),
//   //           ),
//   //           TextFormField(
//   //             style: textFormStyle,
//   //             decoration:
//   //                 ComooStyles.inputCustomDecoration('agência (sem dígito)'),
//   //             initialValue: _agencyNumber,
//   //             enabled: snapshot.data,
//   //             // controller: ComooController.agencyController(_bankNumber),
//   //             onSaved: (String agencyNumber) {
//   //               _agencyNumber = agencyNumber.trim();
//   //             },
//   //             keyboardType: TextInputType.number,
//   //             validator: _validations.validateEmpty,
//   //           ),
//   //           TextFormField(
//   //             style: textFormStyle,
//   //             decoration: ComooStyles.inputDecoration('dígito',
//   //                 hint: 'Coloque apenas se houve dígito verificador'),
//   //             initialValue: _agencyDigit,
//   //             enabled: snapshot.data,
//   //             onSaved: (String value) {
//   //               _agencyDigit = value.trim();
//   //             },
//   //             keyboardType: TextInputType.number,
//   //             // validator: _validations.validateEmpty,
//   //           ),
//   //           Container(
//   //             width: mediaQuery * 360.0,
//   //             height: mediaQuery * 50.0,
//   //             child: Stack(
//   //               children: <Widget>[
//   //                 TextField(
//   //                   onTap: () async {
//   //                     int initialItem = bankAccounts.indexOf(_account);
//   //                     initialItem = initialItem == -1 ? 0 : initialItem;
//   //                     stateFocus.unfocus();
//   //                     stateFocus.consumeKeyboardToken();
//   //                     await customCupertinoModal(
//   //                       context: context,
//   //                       initialItem: initialItem,
//   //                       onChange: (int index) {
//   //                         setState(() {
//   //                           _account = bankAccounts[index];
//   //                           accountTypeController.text = _account.name;
//   //                         });
//   //                       },
//   //                       children: List<Widget>.generate(
//   //                           bankAccounts.length ?? 0, (int index) {
//   //                         return Container(
//   //                           padding: EdgeInsets.symmetric(
//   //                               horizontal: mediaQuery * 30.0),
//   //                           child: Text(
//   //                             '${bankAccounts[index].name}',
//   //                             textAlign: TextAlign.left,
//   //                             overflow: TextOverflow.ellipsis,
//   //                             maxLines: 1,
//   //                           ),
//   //                         );
//   //                       }),
//   //                     );
//   //                   },
//   //                   controller: accountTypeController,
//   //                   focusNode: stateFocus,
//   //                   autofocus: false,
//   //                   enabled: snapshot.data,
//   //                   enableInteractiveSelection: false,
//   //                   textCapitalization: TextCapitalization.none,
//   //                   style: ComooStyles.inputText,
//   //                   decoration: InputDecoration(
//   //                     labelText: 'TIPO DE CONTA',
//   //                     isDense: true,
//   //                     // disabledBorder: ComooStyles.underLineBorder,
//   //                     helperStyle: ComooStyles.texto,
//   //                     focusedBorder: ComooStyles.underLineBorder,
//   //                     labelStyle: ComooStyles.inputLabel
//   //                         .copyWith(color: COMOO.colors.purple),
//   //                     // border: ComooStyles.underLineBorder,
//   //                   ),
//   //                 ),
//   //                 Positioned(
//   //                   bottom: mediaQuery * 5.0,
//   //                   right: mediaQuery * 0.0,
//   //                   child: const Icon(Icons.expand_more),
//   //                 ),
//   //                 // Container(
//   //                 //   decoration: BoxDecoration(
//   //                 //     border: Border(
//   //                 //       bottom: BorderSide(color: Colors.black, width: 1.0),
//   //                 //     ),
//   //                 //   ),
//   //                 //   child: DropdownButtonHideUnderline(
//   //                 //     child: Container(
//   //                 //       child: DropdownButton(
//   //                 //         isExpanded: true,
//   //                 //         hint: Text(
//   //                 //           'TIPO DE CONTA',
//   //                 //           style: ComooStyles.inputLabel,
//   //                 //         ),
//   //                 //         value: _bankAccountType,
//   //                 //         onChanged: snapshot.data
//   //                 //             ? (String value) {
//   //                 //                 setState(() {
//   //                 //                   this._bankAccountType = value;
//   //                 //                 });
//   //                 //               }
//   //                 //             : null,
//   //                 //         items: <DropdownMenuItem<String>>[
//   //                 //           DropdownMenuItem(
//   //                 //             child: Container(
//   //                 //               child: Text(
//   //                 //                 'Conta Corrente',
//   //                 //                 style: textFormStyle,
//   //                 //               ),
//   //                 //             ),
//   //                 //             value: 'Conta Corrente',
//   //                 //             // value: 'Checking',
//   //                 //           ),
//   //                 //           DropdownMenuItem(
//   //                 //             child: Text(
//   //                 //               'Conta Poupança',
//   //                 //               style: textFormStyle,
//   //                 //             ),
//   //                 //             value: 'Conta Poupança',
//   //                 //             // value: 'Saving',
//   //                 //           ),
//   //                 //         ],
//   //                 //       ),
//   //                 //     ),
//   //                 //   ),
//   //                 // ),
//   //                 // Positioned(
//   //                 //   top: mediaQuery * 15.0,
//   //                 //   right: mediaQuery * 25.0,
//   //                 //   child: Text(
//   //                 //     'selecione',
//   //                 //     style: TextStyle(fontSize: mediaQuery * 12.0),
//   //                 //   ),
//   //                 // ),
//   //               ],
//   //             ),
//   //           ),
//   //           TextFormField(
//   //             style: textFormStyle,
//   //             decoration: ComooStyles.inputCustomDecoration(
//   //                 'número da conta (sem dígito)'),
//   //             initialValue: _accountNumber,
//   //             enabled: snapshot.data,
//   //             onSaved: (String accountNumber) {
//   //               _accountNumber = accountNumber.trim();
//   //             },
//   //             // controller: ComooController.accountController(_bankNumber),
//   //             keyboardType: TextInputType.number,
//   //             validator: _validations.validateEmpty,
//   //           ),
//   //           TextFormField(
//   //             style: textFormStyle,
//   //             decoration: ComooStyles.inputDecoration('dígito',
//   //                 hint: 'Coloque apenas se houve dígito verificador'),
//   //             initialValue: _accountDigit,
//   //             enabled: snapshot.data,
//   //             onSaved: (String value) {
//   //               _accountDigit = value.trim();
//   //             },
//   //             keyboardType: TextInputType.number,
//   //             // validator: _validations.validateEmpty,
//   //           ),
//   //         ],
//   //       );
//   //     },
//   //   );
//   // }

//   Widget _buildProfileName() {
//     return Container(
//       padding: EdgeInsets.symmetric(vertical: 15.0),
//       child: Text(
//         currentUser.name,
//         style: ComooStyles.profile_name,
//         textAlign: TextAlign.center,
//       ),
//     );
//   }

//   Widget _buildForm() {
//     final double mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Center(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: <Widget>[
//           Padding(
//             padding: EdgeInsets.only(top: mediaQuery * 20.0),
//           ),
//           _buildAvatar(currentUser.photoURL),
//           _buildProfileName(),
//           // _buildAccessCategory(context),
//           AccessForm(
//             scaffoldKey: _scaffoldKey,
//           ),
//           BlocProvider<ProfilePersonalBloc>(
//             bloc: ProfilePersonalBloc.seeded(
//               user: currentUser,
//               states: statesList,
//             ),
//             child: PersonalForm(),
//           ),
//           // PersonalForm(user: currentUser, states: statesList),
//           BankForm(user: currentUser, banks: banks),
//           // Form(
//           //   onChanged: () {
//           //     setState(() {
//           //       _hasChangesPayment = true;
//           //     });
//           //   },
//           //   key: _formKeyPayment,
//           //   autovalidate: _hasChangesPayment,
//           //   child: Column(
//           //     children: <Widget>[
//           //       PersonalForm(user: currentUser, states: statesList),
//           //       BankForm(user: currentUser, banks: banks),
//           //     ],
//           //   ),
//           // ),
//         ],
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         // FocusScope.of(context).requestFocus(FocusNode());
//         // SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
//       },
//       child: Scaffold(
//         key: _scaffoldKey,
//         appBar: CustomAppBar.round(
//           color: ComooColors.chumbo,
//           title: Text('perfil', style: ComooStyles.appBarTitleSixteen),
//         ),
//         body: _isLoading
//             ? LoadingContainer()
//             : SingleChildScrollView(
//                 // padding: EdgeInsets.symmetric(horizontal: 20.0),
//                 child: _buildForm()),
//       ),
//     );
//   }

//   Future<void> fetchBanks() async {
//     List<Bank> _banks = <Bank>[];
//     await _db
//         .collection('banks')
//         .orderBy('name')
//         .getDocuments()
//         .then((snapshot) {
//       _banks = snapshot.documents.map((doc) {
//         Bank b = Bank.fromSnapshot(doc);
//         return b;
//       }).toList();
//       _banks.add(Bank(number: '0', name: 'selecione'));
//       _banks.sort((Bank a, Bank b) => a.number.compareTo(b.number));

//       setState(() {
//         banks = _banks;
//       });
//     });
//   }

//   Future<void> _fetchCurrentUser() async {
//     _currentUser = await auth.fetchCurrentUser();

//     _displayName = _currentUser.name;
//     _nickname = _currentUser.nickname;
//     _email = _currentUser.email;

//     if (mounted) {
//       setState(() {
//         _birthDateController.updateText(_currentUser.birthDate);
//         _cpfController.updateText(_currentUser.cpf);
//       });
//     }
//     if (_currentUser.phone != null) {
//       if (_currentUser.phone.number != null &&
//           _currentUser.phone.areaCode != null) {
//         if (mounted) {
//           setState(() {
//             _phoneController.updateText(
//                 _currentUser.phone.areaCode + _currentUser.phone.number);
//           });
//         }
//       }
//     }
//     if (_currentUser.address != null) {
//       Address address = _currentUser.address;
//       _addressController.text = address.street;
//       _districtController.text = address.district;
//       _addressNumber = address.number;
//       _complementController.text = address.complement;
//       // _city = address.city;
//       // _state = address.state;
//       StateItem state = statesList.firstWhere(
//           (StateItem s) => s.name == address.state,
//           orElse: () => null);
//       _state = state;
//       stateController.text =
//           state == null ? 'selecione' : '${state.initials} - ${state.name}';
//       citiesList = _state?.cities;
//       cityController.text = address.city;
//       if (mounted) {
//         setState(() {
//           _zipController.updateText(address.zipCode ?? '');
//         });
//       }
//     }
//     if (_currentUser.bank != null) {
//       // print(banks.length);
//       if (mounted) {
//         final String agency = _currentUser.bank.agency;
//         final String account = _currentUser.bank.account;
//         // Bank bank = banks
//         //     .where((bank) => bank.number == _currentUser.bank.number)
//         //     .first;
//         if (mounted) {
//           setState(() {
//             _agencyNumber = agency.split('-').first;
//             _agencyDigit =
//                 agency.split('-').length > 1 ? agency.split('-').last : '';
//             _accountNumber = account.split('-').first;
//             _accountDigit =
//                 account.split('-').length > 1 ? account.split('-').last : '';
//             // _bankNumber = bank.number + ' - ' + bank.name;
//             AccountType acctype = _currentUser.bank.type == 'SAVING'
//                 ? AccountType.savings
//                 : AccountType.checking;
//             BankAccount acc = bankAccounts.firstWhere(
//               (BankAccount ba) => ba.type == acctype,
//               orElse: () => null,
//             );
//             accountTypeController.text = acc?.name ?? '';
//             // print(banks.length);
//             _bank = banks.firstWhere(
//                 (Bank b) => b.number == currentUser.bank.number,
//                 orElse: () => Bank(number: currentUser.bank.number, name: ''));
//             // print('Bank: ${_bank.name} - ${_bank.number}');
//             bankController.text = '${_bank?.number} - ${_bank?.name}';

//             _isBanksEnabled = false;
//           });
//         }
//       }
//     }

//     // for (StateItem state in statesList) {
//     //   if (state.name == _state.name) {
//     //     _stateInitial = state.initials;
//     //     break;
//     //   }
//     // }
//     if (mounted) {
//       setState(() {
//         _isLoading = false;
//       });
//     }
//   }

//   Future<void> _fetchAddress() async {
//     // print('${_zipController.text} ${_zipController.text.length}');
//     if (_zipController.text.length == 9) {
//       if (mounted) {
//         setState(() {
//           _validateZip = false;
//         });
//       }
//       final String zip = _zipController.text.replaceAll(RegExp(r'-'), '');
//       final Response resp = await http
//           .get('http://apps.widenet.com.br/busca-cep/api/cep.json?code=$zip');
//       if (resp.statusCode == 200) {
//         final dynamic jObj = json.decode(resp.body);
//         // print(jObj);
//         if (jObj.containsKey('erro')) {
//           if (mounted) {
//             setState(() {
//               _validateZip = true;
//               _validateZipMessage = 'CEP inválido!';
//             });
//           }
//         } else {
//           if (mounted) {
//             setState(() {
//               _addressController.text = jObj['city'];
//               _districtController.text = jObj['district'];
//             });
//           }
//         }
//       }
//     }
//   }

//   @override
//   void initState() {
//     super.initState();
//     _isLoading = true;
//     fetchStatesList();
//     fetchBanks();
//     _fetchCurrentUser();
//     _zipController.addListener(_fetchAddress);
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     _cpfController.clear();
//     _phoneController.clear();
//     _birthDateController.clear();
//     _zipController.clear();
//     _zipController.removeListener(_fetchAddress);
//     stateController.clear();
//     cityController.clear();
//     _addressController.clear();
//     _complementController.clear();
//     _districtController.clear();
//     bankController.clear();
//     accountTypeController.clear();
//   }

//   Future<void> fetchStatesList() async {
//     final List<StateItem> _states =
//         states.map((dynamic state) => StateItem.fromJSON(state)).toList();
//     _states.sort((StateItem a, StateItem b) => a.name.compareTo(b.name));
//     statesList.addAll(_states);
//   }
// }
