import 'package:comoo/src/utility/style.dart';
import 'package:flutter/material.dart';

class MenuCategory extends StatelessWidget {
  const MenuCategory({
    Key key,
    @required this.title,
    this.children,
    this.padding,
    this.icon,
  }) : super(key: key);

  final String title;
  final List<Widget> children;
  final EdgeInsets padding;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    List<Widget> menu = List<Widget>();
    menu.insert(
      0,
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: padding ?? EdgeInsets.only(left: mediaQuery * 20.0),
            child: Text(
              title,
              style: ComooStyles.profileTitle,
            ),
          ),
          icon ?? Container(),
        ],
      ),
    );
    menu.insert(
      1,
      Divider(
        color: ComooColors.cinzaMedio,
      ),
    );
    menu.insert(
      2,
      Padding(
        padding: padding ?? EdgeInsets.all(0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children,
        ),
      ),
    );
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: menu,
      ),
    );
  }
}
