import 'package:comoo/src/blocs/profile_personal_bloc.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/bank.dart';
import './menu_category.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/states_list.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:comoo/src/provider/provider.dart';

class PersonalForm extends StatelessWidget with Reusables, MapAccountType {
  PersonalForm({
    Key key,
    // @required this.bloc,
    // @required this.user,
    @required this.states,
  })  : assert(states != null),
        super(key: key);

  // final User user;
  final List<StateItem> states;
  final NetworkApi api = NetworkApi();
  final UserAuth auth = UserAuth();

  @override
  Widget build(BuildContext context) {
    final ProfilePersonalBloc bloc =
        BlocProvider.of<ProfilePersonalBloc>(context);

    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    final TextStyle prefixStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.cinzaClaro,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w400,
    );
    final TextStyle textFormStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: mediaQuery * 15.0,
      fontWeight: FontWeight.w500,
    );
    // final TextStyle hintStyle = TextStyle(
    //   fontFamily: ComooStyles.fontFamily,
    //   color: ComooColors.cinzaClaro,
    //   fontSize: mediaQuery * 15.0,
    //   fontWeight: FontWeight.w400,
    // );
    // final TextStyle labelFormStyle = TextStyle(
    //   fontFamily: ComooStyles.fontFamily,
    //   color: ComooColors.roxo,
    //   fontSize: mediaQuery * 15.0,
    //   fontWeight: FontWeight.w500,
    // );
    return StreamBuilder<bool>(
      initialData: true,
      stream: bloc.edit,
      builder: (BuildContext context, AsyncSnapshot<bool> editSnap) {
        return StreamBuilder<bool>(
          initialData: false,
          stream: bloc.isLoading,
          builder:
              (BuildContext loadingContext, AsyncSnapshot<bool> loadingSnap) {
            return Column(
              children: <Widget>[
                MenuCategory(
                  title: 'DADOS PESSOAIS',
                  // icon: IconButton(
                  //   icon: Icon(Icons.edit),
                  //   color: editSnap.data
                  //       ? COMOO.colors.purple
                  //       : COMOO.colors.turquoise,
                  //   onPressed: () {
                  //     bloc.changeEdit(!editSnap.data);
                  //     if (editSnap.data) {
                  //       initialValues();
                  //     }
                  //   },
                  // ),
                  padding: EdgeInsets.symmetric(horizontal: mediaQuery * 20.0),
                  children: <Widget>[
                    InputStream(
                      style: textFormStyle,
                      stream: bloc.phone,
                      onChanged: bloc.changePhone,
                      decoration:
                          ComooStyles.inputCustomDecoration('CELULAR').copyWith(
                        hintText: '(00) 99999-0000',
                        prefix: Container(
                          padding: EdgeInsets.only(right: mediaQuery * 5.0),
                          child: Text(
                            '+55',
                            style: prefixStyle,
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.number,
                      enabled: editSnap.data,
                      controller: bloc.phoneController,
                    ),
                    InputStream(
                      style: textFormStyle,
                      stream: bloc.birthdate,
                      onChanged: bloc.changeBirthdate,
                      decoration: ComooStyles.inputCustomDecoration(
                              'DATA DE NASCIMENTO')
                          .copyWith(hintText: '00/00/0000'),
                      keyboardType: TextInputType.number,
                      enabled: editSnap.data,
                      controller: bloc.birthdateController,
                    ),
                    InputStream(
                      style: textFormStyle,
                      stream: bloc.cpf,
                      onChanged: bloc.changeCPF,
                      decoration: ComooStyles.inputCustomDecoration('CPF')
                          .copyWith(hintText: '000.0000.000-00'),
                      keyboardType: TextInputType.number,
                      enabled: editSnap.data,
                      controller: bloc.cpfController,
                    ),
                    InputStream(
                      style: textFormStyle,
                      stream: bloc.zip,
                      onChanged: bloc.changeZip,
                      decoration: ComooStyles.inputCustomDecoration('CEP')
                          .copyWith(hintText: '00000-00'),
                      keyboardType: TextInputType.number,
                      enabled: editSnap.data,
                      controller: bloc.zipController,
                    ),
                    InputStream(
                      style: textFormStyle,
                      stream: bloc.addressName,
                      onChanged: bloc.changeAddressName,
                      decoration: ComooStyles.inputCustomDecoration('ENDEREÇO'),
                      keyboardType: TextInputType.text,
                      enabled: editSnap.data,
                      controller: bloc.addressNameController,
                    ),
                    InputStream(
                      style: textFormStyle,
                      stream: bloc.addressNumber,
                      onChanged: bloc.changeAddressNumber,
                      decoration: ComooStyles.inputCustomDecoration('NÚMERO'),
                      keyboardType: TextInputType.number,
                      onSubmitted: null,
                      enabled: editSnap.data,
                      controller: bloc.addressNumberController,
                    ),
                    InputStream(
                        style: textFormStyle,
                        stream: bloc.addressComplement,
                        onChanged: bloc.changeAddressComplement,
                        decoration:
                            ComooStyles.inputCustomDecoration('COMPLEMENTO'),
                        keyboardType: TextInputType.text,
                        enabled: editSnap.data,
                        controller: bloc.addressComplementController),
                    InputStream(
                      style: textFormStyle,
                      stream: bloc.addressDistrict,
                      onChanged: bloc.changeAddressDistrict,
                      decoration: ComooStyles.inputCustomDecoration('BAIRRO'),
                      keyboardType: TextInputType.text,
                      enabled: editSnap.data,
                      controller: bloc.addressDistrictController,
                    ),
                    StreamBuilder<StateItem>(
                        stream: bloc.state,
                        builder: (BuildContext context,
                            AsyncSnapshot<StateItem> stateSnap) {
                          return Column(
                            children: <Widget>[
                              TextField(
                                onTap: () async {
                                  FocusScope.of(context)
                                      .requestFocus(FocusNode());
                                  SystemChannels.textInput
                                      .invokeMethod<void>('TextInput.hide');
                                  int initialItem =
                                      states.indexOf(stateSnap.data);
                                  initialItem =
                                      initialItem == -1 ? 0 : initialItem;
                                  // onTap();
                                  await customCupertinoModal(
                                    context: context,
                                    initialItem: initialItem,
                                    onChange: (int index) {
                                      final StateItem item = states[index];
                                      bloc.changeState(item);
                                      bloc.stateController.text =
                                          item.displayName;
                                      bloc.changeCity('');
                                      bloc.cityController.text = 'selecione';
                                    },
                                    children: List<Widget>.generate(
                                        states.length, (int index) {
                                      return Center(
                                        child: Text(states[index].name),
                                      );
                                    }),
                                  );
                                },
                                controller: bloc.stateController,
                                // focusNode: stateFocus,
                                autofocus: false,
                                enabled: editSnap.data,
                                enableInteractiveSelection: false,
                                textCapitalization: TextCapitalization.none,
                                style: textFormStyle,
                                decoration:
                                    ComooStyles.inputCustomDecoration('ESTADO')
                                        .copyWith(
                                  suffix: const Icon(Icons.expand_more),
                                  // isDense: true,
                                ),
                              ),
                              CupertinoDropdownInputStream<String>(
                                initialData: 'selecione',
                                stream: bloc.city,
                                controller: bloc.cityController,
                                keyboardType: TextInputType.number,
                                enabled: editSnap.data,
                                style: textFormStyle,
                                decoration:
                                    ComooStyles.inputCustomDecoration('CIDADE'),
                                onChange: bloc.changeCity,
                                convertToString: (String s) => s,
                                items: stateSnap.data?.cities ??
                                    <String>['selecione'],
                                children: List<Widget>.generate(
                                  (stateSnap.data?.cities ??
                                              <String>['selecione'])
                                          .length ??
                                      0,
                                  (int index) {
                                    return Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: mediaQuery * 30.0),
                                      child: Text(
                                        stateSnap.hasData
                                            ? '${stateSnap.data.cities[index]}'
                                            : 'selecione',
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ],
                          );
                        }),
                    StreamBuilder<bool>(
                      stream: bloc.submitValid,
                      builder: (BuildContext submitContext,
                          AsyncSnapshot<bool> submitSnap) {
                        return Container(
                          alignment: FractionalOffset.center,
                          margin:
                              EdgeInsets.symmetric(vertical: mediaQuery * 20.0),
                          child: OutlineButton(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(mediaQuery * 30.0)),
                            borderSide: BorderSide(color: ComooColors.roxo),
                            highlightElevation: mediaQuery * 10.0,
                            padding: EdgeInsets.symmetric(
                                horizontal: mediaQuery * 56.0,
                                vertical: mediaQuery * 8.0),
                            child: loadingSnap.data
                                ? Container(
                                    height: mediaQuery * 20.0,
                                    child: FittedBox(
                                      child: Center(
                                          child:
                                              const CircularProgressIndicator()),
                                    ),
                                  )
                                : const Text(
                                    'SALVAR',
                                    style: ComooStyles.botoes,
                                  ),
                            onPressed: editSnap.data
                                ? submitSnap.hasData
                                    ? () {
                                        // print('handle');
                                        _save(context, bloc);
                                      }
                                    : null
                                : null,
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ],
            );
          },
        );
      },
    );
  }

  Future<void> _save(BuildContext context, ProfilePersonalBloc bloc) async {
    bloc.changeLoading(true);
    print('complemento: ${bloc.addressComplementValue}');
    Map<String, Object> param = <String, Object>{
      'phone': bloc.phoneMapValue,
      'birthDate': bloc.birthdateValue,
      'cpf': bloc.cpfValue,
      'address': <String, String>{
        'zipCode': bloc.zipValue,
        'street': bloc.addressNameValue,
        'district': bloc.addressDistrictValue,
        'complement': bloc.addressComplementValue,
        'number': bloc.addressNumberValue ?? '',
        'city': bloc.cityValue,
        'uf': bloc.stateValue.initials,
        'state': bloc.stateValue.name,
        'country': 'BRA',
      },
    };
    print(param);
    final bool isSuccess = await api.updatePersonalData(context, param);

    if (isSuccess) {
      showMessageDialog(context, 'Dados pessoais salvos com sucesso!');
      //bloc.changeEdit(false);
      // _fetchCurrentUser();
    }
    bloc.changeLoading(false);
  }
}
