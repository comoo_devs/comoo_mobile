import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/painters/message_painter.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

import 'signup/tos.dart';

class UpdateToSPage extends StatelessWidget with Reusables {
  UpdateToSPage();

  final NetworkApi api = NetworkApi();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF471D58),
      body: CustomPaint(
        painter: MessagePainter(),
        child: Center(
          child: ListView(
            children: <Widget>[
              SizedBox(height: responsive(context, 82)),
              Image.asset(
                'images/tos.png',
                width: responsive(context, 99),
                height: responsive(context, 93),
              ),
              SizedBox(height: responsive(context, 33)),
              Text(
                'Olá',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFFEC1260),
                  fontSize: responsive(context, 29),
                  fontWeight: FontWeight.bold,
                  letterSpacing: responsive(context, -0.15),
                ),
              ),
              SizedBox(height: responsive(context, 16)),
              Text(
                'Atualizamos nossos Termos \n'
                'e Condiçoões e nossa \n'
                'Política de Privacidade',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFFFFFFFF),
                  fontSize: responsive(context, 20),
                  fontWeight: FontWeight.w500,
                  letterSpacing: responsive(context, -0.15),
                ),
              ),
              SizedBox(height: responsive(context, 32)),
              Container(
                margin:
                    EdgeInsets.symmetric(horizontal: responsive(context, 32)),
                child: RaisedButton(
                  elevation: 0.0,
                  color: ComooColors.turqueza,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14.0)),
                  onPressed: () async {
                    final bool result = await Navigator.of(context)
                        .push(MaterialPageRoute<bool>(
                      builder: (BuildContext context) => TermsOfServicePage(),
                    ));
                    if (result ?? false) {
                      api.agreedWithToS();
                      final User user = await api.auth.fetchCurrentUser();
                      welcomeNavigation(context, user);
                    }
                  },
                  child: Text(
                    'LER E ACEITAR',
                    style: TextStyle(
                      fontFamily: COMOO.fonts.montSerrat,
                      color: Colors.white,
                      fontSize: responsive(context, 16.0),
                      letterSpacing: responsive(context, 0.6),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
