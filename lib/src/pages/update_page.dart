import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/painters/message_painter.dart';
import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';

class UpdatePage extends StatelessWidget {
  const UpdatePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      backgroundColor: Color(0xFF471D58),
      body: CustomPaint(
        painter: MessagePainter(),
        child: Center(
          child: ListView(
            children: <Widget>[
              SizedBox(height: mediaQuery * 82),
              Image.asset(
                'images/update_icon.png',
                width: mediaQuery * 99,
                height: mediaQuery * 93,
              ),
              SizedBox(height: mediaQuery * 33),
              Text(
                'Tem versão nova\n'
                'da COMOO no ar',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFFEC1260),
                  fontSize: mediaQuery * 23,
                  fontWeight: FontWeight.bold,
                  letterSpacing: mediaQuery * -0.15,
                ),
              ),
              SizedBox(height: mediaQuery * 16),
              Text(
                'Queremos tornar sua experiência\n'
                'cada vez melhor. Atualize o app\n'
                'e tenha acesso a melhorias,\n'
                'novidades e correções.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFFFFFFFF),
                  fontSize: mediaQuery * 15,
                  fontWeight: FontWeight.w500,
                  letterSpacing: mediaQuery * -0.125,
                ),
              ),
              SizedBox(height: mediaQuery * 32),
              Container(
                margin: EdgeInsets.symmetric(horizontal: mediaQuery * 32),
                child: RaisedButton(
                  elevation: 0.0,
                  color: ComooColors.turqueza,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14.0)),
                  onPressed: () {
                    LaunchReview.launch(
                      androidAppId: 'io.comoo',
                      iOSAppId: '1397595044',
                      writeReview: false,
                    );
                  },
                  child: Text(
                    'ATUALIZAR AGORA',
                    style: TextStyle(
                      fontFamily: COMOO.fonts.montSerrat,
                      color: Colors.white,
                      fontSize: mediaQuery * 16.0,
                      letterSpacing: mediaQuery * 0.6,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
