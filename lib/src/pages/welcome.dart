import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/comoo.dart';

class WelcomePage extends StatefulWidget {
  final Comoo comoo;
  WelcomePage(this.comoo);

  @override
  State createState() {
    return new _WelcomePageState();
  }
}

class _WelcomePageState extends State<WelcomePage>
    with WidgetsBindingObserver, Reusables {
  final NetworkApi api = NetworkApi();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Image(image: AssetImage('images/logo_fb.png')),
        ),
      ),
    );
  }

  void _checkVersion() async {
    final bool forceUpdate = await api.validateVersion();
    if (forceUpdate ?? false) {
      Navigator.of(context).pushReplacementNamed('/update');
    } else {
      _checkToS();
    }
  }

  Future<void> _checkToS() async {
    final bool hasReadLatestToS = await api.validateToS();

    if (hasReadLatestToS ?? false) {
      _checkForToken();
    } else {
      Navigator.of(context).pushReplacementNamed('/agreeToS');
    }
  }

  void _checkForToken() async {
    final User user = await api.fetchCurrentUser();
    welcomeNavigation(context, user);
  }

  @override
  void initState() {
    // checkForToken();
    _checkVersion();
    super.initState();
  }
}
