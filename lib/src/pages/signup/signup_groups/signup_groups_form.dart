import 'package:comoo/src/blocs/signup_groups_bloc.dart';
import 'package:comoo/src/comoo/group.dart';
import '../../home/bottom_menu/group_creation/add_group.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/group_tile.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/painters/message_painter.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

import 'group_search.dart';

class SignupGroupsForm extends StatelessWidget with Reusables {
  SignupGroupsForm({Key key}) : super(key: key);
  final NetworkApi api = NetworkApi();

  void _navigateHome(BuildContext context) {
    Navigator.of(context).pushReplacementNamed('/home');
  }

  @override
  Widget build(BuildContext context) {
    final SignupGroupsBloc bloc = BlocProvider.of<SignupGroupsBloc>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      // key: _scaffoldKey,
      appBar: CustomAppBar.round(
        color: ComooColors.turqueza,
        title: Text(
          'COMOONIDADES',
          style: ComooStyles.appBarTitleWhite,
        ),
        actions: <Widget>[
          StreamBuilder<bool>(
              stream: bloc.canProgress,
              builder: (BuildContext context, AsyncSnapshot<bool> canProgress) {
                print(
                    'CanProgress: ${canProgress.connectionState} ${canProgress.data}');
                return FlatButton(
                  // onPressed: _handleSubmit,
                  onPressed: () async {
                    final bool res1 = canProgress.data ?? false;
                    final bool res2 = await bloc.canProgressPrefs();
                    if (res1 || res2) {
                      _navigateHome(context);
                    } else {
                      // _navigateHome(context);
                      showCantProgressDialog(context);
                    }
                  },
                  child: Text(
                    'AVANÇAR',
                    style: TextStyle(
                      fontFamily: COMOO.fonts.montSerrat,
                      fontWeight: FontWeight.bold,
                      fontSize: responsive(context, 13.0),
                      color: Colors.white,
                      letterSpacing: responsive(context, -0.108),
                    ),
                  ),
                );
              }),
        ],
      ),
      body: StreamBuilder<bool>(
        stream: bloc.isLoading,
        builder: (BuildContext context, AsyncSnapshot<bool> snap) {
          return (snap.data ?? false)
              ? LoadingContainer()
              : _buildPage(context, bloc);
        },
      ),
    );
  }

  Future<void> showCantProgressDialog(BuildContext context) {
    return showCustomDialog(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: 'Você precisa selecionar pelo menos uma comoonidade.\n'
            'Nāo se preocupe, você poderá alterar sua escolha a qualquer momento.',
        textAlign: TextAlign.center,
        actions: <Widget>[
          Container(
            height: responsive(context, 41.0),
            // width: mediaQuery * 163.0,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              color: Colors.white,
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'OK!',
                style: TextStyle(
                  color: COMOO.colors.pink,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPage(BuildContext context, SignupGroupsBloc bloc) {
    final TextStyle textStyle = TextStyle(
      fontFamily: COMOO.fonts.montSerrat,
      color: COMOO.colors.purple,
      fontSize: responsive(context, 18.0),
      fontWeight: FontWeight.w600,
      letterSpacing: responsive(context, -0.163),
    );
    return StreamBuilder<bool>(
        stream: bloc.hasRecommendation,
        builder: (BuildContext context, AsyncSnapshot<bool> snap) {
          print('${snap.hasData} ${snap.data}');
          return snap.hasData
              ? (snap.data ?? false)
                  ? ListView(
                      key: Key('signupGroupsFormListView'),
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      children: <Widget>[
                        SizedBox(height: responsive(context, 25.0)),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: responsive(context, 20.0)),
                          child: RichText(
                            text: TextSpan(
                                text: 'Bem-vindo à COMOO!',
                                style: textStyle.copyWith(
                                  fontSize: responsive(context, 21),
                                ),
                                children: <TextSpan>[
                                  TextSpan(text: '\n'),
                                  TextSpan(
                                    text:
                                        'Para começar a comprar e vender você precisa fazer parte de comoonidades',
                                    style: textStyle,
                                  ),
                                ]),
                          ),
                        ),
                        SizedBox(height: responsive(context, 17.0)),
                        // TextField(),
                        Padding(
                          padding:
                              EdgeInsets.only(left: responsive(context, 21.0)),
                          child: InkWell(
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute<void>(
                                    builder: (BuildContext context) =>
                                        GroupSearchView(),
                                  ),
                                );
                              },
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      // Icon(COMOO.icons.search.iconData),
                                      Image.asset(
                                        'images/search_black.png',
                                        width: responsive(context, 24.0),
                                      ),
                                      SizedBox(
                                          width: responsive(context, 14.0)),
                                      Text(
                                        'Busque uma comoonidade',
                                        style: TextStyle(
                                          fontFamily: COMOO.fonts.montSerrat,
                                          fontSize: responsive(context, 15.0),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Divider(),
                                ],
                              )),
                        ),
                        SizedBox(height: responsive(context, 10.0)),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: responsive(context, 45.0)),
                          child: RaisedButton(
                            elevation: 0.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  responsive(context, 30.0)),
                            ),
                            color: COMOO.colors.purple,
                            // padding: EdgeInsets.symmetric(horizontal: responsive(context, 35.0)),
                            // borderSide: BorderSide(color: Color.fromRGBO(70, 30, 90, 1.0)),
                            highlightElevation: 10.0,
                            child: FittedBox(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Icon(
                                    COMOO.icons.createGroup.iconData,
                                    color: Colors.white,
                                    size: responsive(context, 40.0),
                                  ),
                                  SizedBox(width: responsive(context, 20.0)),
                                  Text(
                                    'Criar nova comoonidade',
                                    style: TextStyle(
                                      fontFamily: COMOO.fonts.montSerrat,
                                      color: Colors.white,
                                      letterSpacing:
                                          responsive(context, -0.125),
                                      fontSize: responsive(context, 15.0),
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            onPressed: () async {
                              final dynamic result =
                                  await Navigator.of(context).push(
                                MaterialPageRoute<dynamic>(
                                  builder: (BuildContext context) =>
                                      AddGroupPage(openFeed: false),
                                ),
                              );
                              if (result != null) {
                                bloc.editCanProgress(result);
                              }
                            },
                          ),
                        ),
                        SizedBox(height: responsive(context, 35.0)),
                        StreamBuilder<List<Group>>(
                          stream: bloc.suggested,
                          builder: (BuildContext context,
                              AsyncSnapshot<List<Group>> snaps) {
                            // print('${snaps.hasData} ${snaps.connectionState} ${snaps.data}');
                            return snaps.hasData
                                ? _buildSnapshotData(context, bloc, snaps.data)
                                : Container();
                          },
                        ),
                      ],
                    )
                  : _buildEmptyRecommendation(context, bloc)
              : Container();
        });
  }

  Widget _buildEmptyRecommendation(
      BuildContext context, SignupGroupsBloc bloc) {
    return Container(
      color: COMOO.colors.purple,
      child: CustomPaint(
        painter: MessagePainter(),
        child: ListView(
          key: Key('emptySignupGroupsFormListView'),
          // shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.symmetric(horizontal: responsive(context, 36.0)),
          children: <Widget>[
            SizedBox(height: responsive(context, 33)),
            Text(
              'Bem-vindo à COMOO!',
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Color(0xFFEC1260),
                fontSize: responsive(context, 23.0),
                fontWeight: FontWeight.bold,
                letterSpacing: responsive(context, -0.163),
              ),
            ),
            SizedBox(height: responsive(context, 16)),
            Text(
              'Você é o 1º usuário de sua regiāo\n'
              'ou o primeiro com interesse \n'
              'nessas categorias\n\n'
              'Aproveite essa oportunidade e\n'
              'faça a COMOO do seu jeito. Crie\n'
              'comoonidades e compartilhe\n'
              'seu código de convite',
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.white,
                fontSize: responsive(context, 18.0),
                fontWeight: FontWeight.w500,
                letterSpacing: responsive(context, 0.125),
              ),
            ),
            SizedBox(height: responsive(context, 19.0)),
            Container(
              margin: EdgeInsets.symmetric(horizontal: responsive(context, 8)),
              child: RaisedButton(
                elevation: 0.0,
                color: ComooColors.turqueza,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
                onPressed: () async {
                  final dynamic result = await Navigator.of(context).push(
                    MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) =>
                          AddGroupPage(openFeed: false),
                    ),
                  );
                  if (result != null) {
                    _navigateHome(context);
                  }
                },
                child: FittedBox(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        COMOO.icons.createGroup.iconData,
                        color: Colors.white,
                        size: responsive(context, 50.0),
                      ),
                      SizedBox(width: responsive(context, 20.0)),
                      Text(
                        'Criar nova comoonidade',
                        style: TextStyle(
                          fontFamily: COMOO.fonts.montSerrat,
                          color: Colors.white,
                          fontSize: responsive(context, 15.0),
                          letterSpacing: responsive(context, -0.125),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: responsive(context, 19.0)),
            Container(
              margin: EdgeInsets.symmetric(horizontal: responsive(context, 8)),
              child: RaisedButton(
                elevation: 0.0,
                color: ComooColors.turqueza,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
                onPressed: () async {
                  showCustomDialog<void>(
                    context: context,
                    barrierDismissible: false,
                    alert: customRoundAlert(
                      context: context,
                      title: 'Gerando código de convite',
                      textAlign: TextAlign.center,
                      actions: <Widget>[
                        Center(
                          child: CircularProgressIndicator(),
                        )
                      ],
                    ),
                  );

                  final String message = await api.fetchUserInviteMessage();
                  Navigator.of(context).pop();
                  if (message != null) {
                    await Share.share(message);
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      COMOO.icons.convidar.iconData,
                      color: Colors.white,
                      size: responsive(context, 50.0),
                    ),
                    SizedBox(width: responsive(context, 20.0)),
                    Text(
                      'Convidar pessoas',
                      style: TextStyle(
                        fontFamily: COMOO.fonts.montSerrat,
                        color: Colors.white,
                        fontSize: responsive(context, 15.0),
                        letterSpacing: responsive(context, -0.125),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSnapshotData(
      BuildContext context, SignupGroupsBloc bloc, List<Group> groups) {
    // return Container();
    if (groups.isEmpty) {
      return Container();
    }
    return ListView.separated(
      key: Key('separatedSignupGroupsFormListView'),
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: groups.length + 1,
      separatorBuilder: (BuildContext context, index) {
        if (index != 0) {
          return Divider();
        } else {
          return Container();
        }
      },
      itemBuilder: (BuildContext context, int index) {
        if (index == 0) {
          return Padding(
            padding: EdgeInsets.only(left: responsive(context, 18.0)),
            child: Text(
              'Comoonidades sugeridas para você',
              style: TextStyle(
                fontFamily: COMOO.fonts.montSerrat,
                color: COMOO.colors.purple,
                fontWeight: FontWeight.w600,
                letterSpacing: responsive(context, -0.134),
                fontSize: responsive(context, 14.0),
              ),
            ),
          );
        }
        // print('builded $index out of ${groups.length}');
        // return Text('$index');
        final Group group = groups[--index];
        // print('address: ${group.address}');
        return GroupTile(group: group, fromSignup: true);
        // return Row(
        //   children: <Widget>[
        //     CachedCircleAvatar(
        //       url: group.avatar,
        //     ),
        //     Text('${group.name}'),
        //   ],
        // );
        // return ListTile(
        //   leading: CachedCircleAvatar(
        //     url: group.avatar,
        //   ),
        //   // title: Container(
        //   //   color: Colors.amber,
        //   //   child: Text(
        //   //     '${group.name}',
        //   //     style: TextStyle(color: Colors.black),
        //   //   ),
        //   // ),
        //   title: RichText(
        //     text: TextSpan(
        //         text: '${group.name}',
        //         style: TextStyle(
        //           fontFamily: COMOO.fonts.montSerrat,
        //           color: COMOO.colors.turquoise,
        //           fontSize: responsive(context, 14.0),
        //         )),
        //   ),
        // );
      },
    );
  }
}
