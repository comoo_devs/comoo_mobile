import 'package:comoo/src/blocs/group_detail_bloc.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/pages/common/group/group_page.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:flutter/material.dart';

class SignupGroupsView extends StatefulWidget {
  SignupGroupsView({this.group});
  final Group group;
  @override
  State<StatefulWidget> createState() => _SignupGroupsViewState();
}

class _SignupGroupsViewState extends State<SignupGroupsView> {
  SignupGroupDetailBloc bloc;

  Future<void> init() async {
    bloc = SignupGroupDetailBloc.seeded(group: widget.group);
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      initialData: true,
      stream: bloc?.isLoading,
      builder: (BuildContext context, AsyncSnapshot<bool> loading) {
        return (loading.data ?? true)
            ? Scaffold(body: LoadingContainer())
            : BlocProvider<SignupGroupDetailBloc>(
                bloc: bloc,
                child: SignupGroupPage(),
              );
      },
    );
  }
}
