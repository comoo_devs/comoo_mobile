import 'package:comoo/src/blocs/group_search_bloc.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/group_tile.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

class GroupSearchView extends StatefulWidget {
  GroupSearchView();
  @override
  State<StatefulWidget> createState() => _GroupSearchState();
}

class _GroupSearchState extends State<GroupSearchView> {
  GroupSearchBloc bloc;
  @override
  void initState() {
    bloc = GroupSearchBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GroupSearchBloc>(
      bloc: bloc,
      child: GroupSearchPage(),
    );
  }
}

class GroupSearchPage extends StatelessWidget with Reusables {
  GroupSearchPage();
  @override
  Widget build(BuildContext context) {
    final GroupSearchBloc bloc = BlocProvider.of<GroupSearchBloc>(context);
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.5,
        brightness: Theme.of(context).brightness,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        titleSpacing: mediaQuery * 40.0,
        title: TextField(
          autocorrect: true,
          onChanged: bloc.editSearch,
          autofocus: true,
          focusNode: bloc.searchFocusNode,
          decoration: InputDecoration(
            icon: Image.asset(
              'images/search_black.png',
              height: mediaQuery * 30.0,
              width: mediaQuery * 30.0,
            ),
            hintText: '',
            border: InputBorder.none,
          ),
        ),
        actions: <Widget>[
          Container(
            width: mediaQuery * 95.0,
            child: IconButton(
              iconSize: mediaQuery * 34.0,
              icon: Text(
                'CANCELAR',
                style: TextStyle(
                  fontFamily: ComooStyles.fontFamily,
                  color: ComooColors.chumbo,
                  fontSize: mediaQuery * 12.0,
                  fontWeight: FontWeight.bold,
                  letterSpacing: mediaQuery * 0.60,
                ),
              ),
              color: ComooColors.chumbo,
              onPressed: () {
                bloc.searchFocusNode.unfocus();
                Navigator.of(context).pop();
              },
            ),
          ),
        ],
      ),
      body: StreamBuilder<bool>(
        stream: bloc.isLoading,
        builder: (BuildContext loadingContext, AsyncSnapshot<bool> snap) {
          print('Loading: ${snap.connectionState} ${snap.data}');
          final bool isLoading = snap.data ?? false;
          // print('isLoading? $isLoading');
          return StreamBuilder<String>(
            stream: bloc.search,
            builder:
                (BuildContext textContext, AsyncSnapshot<String> searchedText) {
              print(
                  'Text: ${searchedText.connectionState} ${searchedText.data}');
              final String text = searchedText.data ?? '';
              print('SearchedText: $text');
              return StreamBuilder<List<GroupSearch>>(
                stream: bloc.searchedGroups,
                builder: (BuildContext groupsContext,
                    AsyncSnapshot<List<GroupSearch>> snapshot) {
                  print('List: ${snapshot.connectionState} ${snapshot.data}');
                  if (text.isEmpty) {
                    return Container(
                      alignment: FractionalOffset.center,
                      padding: EdgeInsets.all(mediaQuery * 48.0),
                      child: Text(
                        'Pode explorar! Faça uma busca por pessoas ou comoonidades.',
                        textAlign: TextAlign.center,
                        style: ComooStyles.botaoTurqueza,
                      ),
                    );
                  }
                  if (isLoading) {
                    return Container(
                      alignment: FractionalOffset.center,
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (snapshot.hasData) {
                    if (snapshot.data.isEmpty) {
                      return Container(
                        alignment: FractionalOffset.center,
                        padding: EdgeInsets.all(mediaQuery * 48.0),
                        child: Text(
                          'Não existem COMOOs a partir desta busca :(',
                          textAlign: TextAlign.center,
                          style: ComooStyles.botaoRoxo,
                        ),
                      );
                    }
                    return _groupList(context, snapshot.data);
                  }
                  // if (_groups.isNotEmpty) {
                  //   return _groupList(context, _groups);
                  // }
                  return Container(
                    alignment: FractionalOffset.center,
                    padding: EdgeInsets.all(mediaQuery * 48.0),
                    child: Text(
                      'Não existem COMOOs a partir desta busca :(',
                      textAlign: TextAlign.center,
                      style: ComooStyles.botaoRoxo,
                    ),
                  );
                },
              );
            },
          );
        },
      ),
    );
  }

  Widget _groupList(BuildContext context, List<GroupSearch> groups) {
    if (groups.isEmpty) {
      return Container();
    }
    return ListView.separated(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: groups.length + 1,
      separatorBuilder: (BuildContext context, index) {
        if (index != 0) {
          return Divider();
        } else {
          return Container();
        }
      },
      itemBuilder: (BuildContext context, int index) {
        if (index == 0) {
          return Padding(
            padding: EdgeInsets.only(left: responsive(context, 18.0)),
            child: Text(
              'Comoonidades sugeridas para você',
              style: TextStyle(
                fontFamily: COMOO.fonts.montSerrat,
                color: COMOO.colors.purple,
                fontWeight: FontWeight.w600,
                letterSpacing: responsive(context, -0.134),
                fontSize: responsive(context, 14.0),
              ),
            ),
          );
        }
        final Group group = Group.fromGroupSearch(groups[--index]);
        // print('address: ${group.address}');
        return GroupTile(group: group, fromSignup: true);
      },
    );
    // final mediaQuery = MediaQuery.of(context).size.width / 400;
    // final GroupSearchBloc bloc = BlocProvider.of<GroupSearchBloc>(context);
    // return ListView.builder(
    //   addRepaintBoundaries: true,
    //   padding: EdgeInsets.only(top: mediaQuery * 10.0),
    //   itemCount: groups.length,
    //   itemBuilder: (context, index) {
    //     GroupSearch group = groups[index];
    //     return ListTile(
    //       contentPadding: EdgeInsets.symmetric(
    //         horizontal: mediaQuery * 10.0,
    //       ),
    //       leading: GestureDetector(
    //         child: Container(
    //           decoration: BoxDecoration(
    //               border: Border.all(color: ComooColors.cinzaClaro),
    //               borderRadius: BorderRadius.circular(mediaQuery * 30.0),
    //               image: DecorationImage(
    //                 fit: BoxFit.cover,
    //                 image: AdvancedNetworkImage(group.photoURL),
    //               )),
    //           width: mediaQuery * 55.0,
    //           height: mediaQuery * 55.0,
    //         ),
    //         onTap: () {
    //           _openGroupInfo(context, group);
    //         },
    //       ),
    //       title: GestureDetector(
    //         child: Text(
    //           group.name,
    //           style: TextStyle(
    //             fontFamily: ComooStyles.fontFamily,
    //             color: ComooColors.chumbo,
    //             fontSize: mediaQuery * 12.0,
    //             fontWeight: FontWeight.w600,
    //           ),
    //         ),
    //         onTap: () {
    //           _openGroupInfo(context, group);
    //         },
    //       ),
    //       subtitle: GestureDetector(
    //         child: Text(
    //           '${group.membersQty} membros',
    //           style: TextStyle(
    //             fontFamily: ComooStyles.fontFamily,
    //             color: ComooColors.cinzaMedio,
    //             fontSize: mediaQuery * 12.0,
    //             fontWeight: FontWeight.w500,
    //           ),
    //           overflow: TextOverflow.ellipsis,
    //           maxLines: 2,
    //         ),
    //         onTap: () {
    //           _openGroupInfo(context, group);
    //         },
    //       ),
    //       trailing: Container(
    //         // padding: EdgeInsets.only(right: mediaQuery * 25.0),
    //         child: IconButton(
    //           alignment: Alignment.center,
    //           iconSize: mediaQuery * 50.0,
    //           // icon: Icon(
    //           //   group.inGroup ? Icons.check_circle : Icons.add_circle_outline,
    //           //   color: ComooColors.turqueza,
    //           // ),
    //           icon: Image.asset(
    //             group.inGroup
    //                 ? 'images/checked_turquoise.png'
    //                 : 'images/add_white.png',
    //             width: mediaQuery * 30.0,
    //             height: mediaQuery * 30.0,
    //           ),
    //           onPressed: () {
    //             group.inGroup
    //                 ? bloc.requestGroupExit(group)
    //                 : bloc.requestGroupEntry(group);
    //           },
    //         ),
    //       ),
    //       // onTap: () {
    //       // Navigator.of(context).push(new MaterialPageRoute(
    //       //     builder: (context) => GroupChatPage(_groups[index])));
    //       // },
    //     );
    //   },
    // );
  }

  // Future<void> _openGroupInfo(BuildContext context, GroupSearch groupSearch) async {
  //   print('open group info');
  //   Navigator.of(context).push(MaterialPageRoute(
  //       builder: (BuildContext context) =>
  //           GroupDetailView(id: groupSearch.id)));
  // }
}
