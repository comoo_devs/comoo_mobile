import 'package:comoo/src/blocs/signup_groups_bloc.dart';
import '../signup_groups/signup_groups_form.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:flutter/material.dart';

class SignUpGroups extends StatefulWidget {
  SignUpGroups({this.code});
  final String code;
  @override
  State<StatefulWidget> createState() => _SignUpGroupsState();
}

class _SignUpGroupsState extends State<SignUpGroups> {
  SignupGroupsBloc bloc;
  int counter = 0;

  @override
  void initState() {
    bloc = SignupGroupsBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // return SignupForm();
    return BlocProvider<SignupGroupsBloc>(
      bloc: bloc,
      child: SignupGroupsForm(),
    );
  }
}
