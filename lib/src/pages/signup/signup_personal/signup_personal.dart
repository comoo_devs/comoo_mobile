import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/blocs/signup_personal_bloc.dart';
import 'package:comoo/src/comoo/user.dart';
import '../signup_personal/signup_personal_form.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/states_list.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class SignUpPersonal extends StatefulWidget {
  SignUpPersonal({this.showBackButton = true});
  final bool showBackButton;
  @override
  State<StatefulWidget> createState() => _SignUpPersonalState();
}

class _SignUpPersonalState extends State<SignUpPersonal> {
  SignupPersonalBloc bloc;
  StreamSubscription<dynamic> _zipSubscription;
  User user;
  int counter = 0;

  final BehaviorSubject<bool> _loading = BehaviorSubject<bool>();
  Function(bool) get editLoading => _loading.sink.add;
  Stream<bool> get isLoading => _loading.stream;

  Future<void> init() async {
    editLoading(true);
    final Firestore db = Firestore.instance;
    final FirebaseUser fireUser = await FirebaseAuth.instance.currentUser();
    if (fireUser != null) {
      final DocumentSnapshot snap =
          await db.collection('users').document(fireUser.uid).get();
      if (!snap.exists) {
        return init();
      }
      user = User.fromSnapshot(snap);
      bloc = SignupPersonalBloc.seeded(
        user: user,
        states: states
            .map<StateItem>((dynamic state) => StateItem.fromJSON(state))
            .toList()
              ..sort((StateItem a, StateItem b) => a.name.compareTo(b.name)),
      );
      _zipSubscription = bloc.zip.listen((String zip) {
        _zipListener(zip, bloc);
      });
    }
    editLoading(false);
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  @override
  void dispose() {
    _zipSubscription?.cancel();
    _loading?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        initialData: false,
        stream: isLoading,
        builder: (BuildContext contextLoading, AsyncSnapshot snap) {
          return snap.data && bloc == null
              ? Scaffold(body: LoadingContainer())
              : BlocProvider<SignupPersonalBloc>(
                  bloc: bloc,
                  child: SignupPersonalForm(
                    user: user,
                    showBackButton: widget.showBackButton ?? true,
                  ),
                );
        });
  }

  Future<void> _zipListener(String data, SignupPersonalBloc bloc) async {
    if (data.length == 9) {
      final String zip = bloc.zipController.text.replaceAll(RegExp(r'-'), '');
      final http.Response resp = await http
          .get('http://apps.widenet.com.br/busca-cep/api/cep.json?code=$zip');
      if (resp.statusCode == 200) {
        final dynamic jObj = json.decode(resp.body);
        print(jObj);
        final int status = jObj['status'] ?? 0;
        if (status != 0) {
          final String state = jObj['state'];
          final String city = jObj['city'];
          StateItem stateItem;
          for (dynamic s in states) {
            if (s['sigla'] == state) {
              final String initial = state;
              final String name = s['nome'];
              final List<dynamic> cities = s['cidades'];
              stateItem =
                  StateItem(initials: initial, name: name, cities: cities);
              break;
            }
          }
          if (mounted) {
            bloc.changeAddressName(jObj['address']);
            bloc.addressNameController.text = jObj['address'];
            // bloc.addStateString(jObj['city']);
            // bloc.addUF(jObj['state']);
            //bloc.changeAddressComplement(jObj['complemento']);
            //bloc.addressComplementController.text = jObj['complemento'];
            bloc.changeAddressDistrict(jObj['district']);
            bloc.addressDistrictController.text = jObj['district'];
            if (stateItem != null) {
              bloc.changeState(stateItem);
              bloc.changeCity(city);
              bloc.stateController.text = stateItem.displayName;
              bloc.cityController.text = city;
            }
          }
        }
      }
    }
  }
}
