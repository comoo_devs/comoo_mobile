import 'package:comoo/src/blocs/signup_personal_bloc.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/widgets/painters/rect_painter.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/states_list.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SignupPersonalForm extends StatelessWidget with Reusables {
  SignupPersonalForm({Key key, @required this.user, this.showBackButton = true})
      : super(key: key);
  final NetworkApi api = NetworkApi();
  final User user;
  final bool showBackButton;

  @override
  Widget build(BuildContext context) {
    final SignupPersonalBloc bloc =
        BlocProvider.of<SignupPersonalBloc>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      // key: _scaffoldKey,
      // appBar: CustomAppBar.round(
      //   color: ComooColors.turqueza,
      //   title: Text(
      //     'CADASTRO',
      //     style: ComooStyles.appBarTitleWhite,
      //   ),
      // ),
      backgroundColor: COMOO.colors.purple,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
          SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
        },
        child: StreamBuilder(
          initialData: false,
          stream: bloc?.isLoading,
          builder: (BuildContext contextLoading, AsyncSnapshot snap) {
            return snap.data || bloc == null
                ? LoadingContainer(message: 'Carregando...')
                : SingleChildScrollView(
                    physics: ClampingScrollPhysics(),
                    child: _buildForm(context, bloc),
                  );
          },
        ),
      ),
    );
  }

  Widget _buildForm(BuildContext context, SignupPersonalBloc bloc) {
    // final TextStyle prefixStyle = TextStyle(
    //   fontFamily: ComooStyles.fontFamily,
    //   color: ComooColors.cinzaClaro,
    //   fontSize: responsive(context, 15.0),
    //   fontWeight: FontWeight.w400,
    // );
    final TextStyle textFormStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: responsive(context, 15.0),
      fontWeight: FontWeight.w500,
    );
    // final TextStyle hintStyle = TextStyle(
    //   fontFamily: ComooStyles.fontFamily,
    //   color: ComooColors.cinzaClaro,
    //   fontSize: mediaQuery * 15.0,
    //   fontWeight: FontWeight.w400,
    // );
    // final TextStyle labelFormStyle = TextStyle(
    //   fontFamily: ComooStyles.fontFamily,
    //   color: COMOO.colors.turquoise,
    //   fontSize: responsive(context, 14.0),
    //   fontWeight: FontWeight.w500,
    // );

    final InputDecoration inputDecoration = InputDecoration(
      contentPadding: EdgeInsets.symmetric(
        horizontal: responsive(context, 15),
        vertical: responsive(context, 8),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: COMOO.colors.turquoise,
        ),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: COMOO.colors.turquoise,
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: COMOO.colors.turquoise,
        ),
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: COMOO.colors.turquoise,
        ),
      ),
      filled: true,
      fillColor: Colors.white,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      // addAutomaticKeepAlives: true,
      // addRepaintBoundaries: true,
      // semanticChildCount: 15,
      // addSemanticIndexes: true,
      children: <Widget>[
        SizedBox(height: responsive(context, 20.0)),

        Align(
          alignment: Alignment.centerLeft,
          child: showBackButton
              ? BackButton(color: Colors.white)
              : Container(height: 48),
        ),
        // SizedBox(height: responsive(context, 24)),
        Padding(
          padding: EdgeInsets.only(
            left: responsive(context, 20),
          ),
          child: Text(
            'Compre, venda, \n' 'convide, ganhe.',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontFamily: COMOO.fonts.montSerrat,
              color: COMOO.colors.pink,
              fontSize: responsive(context, 24),
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        // Padding(
        //   padding: EdgeInsets.only(left: responsive(context, 27.0)),
        //   child: Text(
        //     'Para comprar e vender com segurança, precisamos somente de mais umas informações. Depois disso...',
        //     style: TextStyle(
        //       fontFamily: COMOO.fonts.montSerrat,
        //       fontWeight: FontWeight.w600,
        //       fontSize: responsive(context, 17.0),
        //       color: COMOO.colors.purple,
        //       letterSpacing: responsive(context, -0.163),
        //     ),
        //   ),
        // ),
        // SizedBox(height: responsive(context, 59.0)),
        _buildPainters(
          child: Padding(
            padding: EdgeInsets.fromLTRB(
              responsive(context, 25),
              responsive(context, 91),
              responsive(context, 25),
              0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'DATA DE NASCIMENTO',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: responsive(context, 5)),
                InputStream(
                  style: textFormStyle,
                  stream: bloc.birthdate,
                  onChanged: bloc.changeBirthdate,
                  decoration: inputDecoration.copyWith(
                    hintText: '00/00/0000',
                  ),
                  // decoration:
                  //     ComooStyles.inputCustomDecoration('DATA DE NASCIMENTO')
                  //         .copyWith(
                  //   hintText: '00/00/0000',
                  //   labelStyle: labelFormStyle,
                  // ),
                  keyboardType: TextInputType.number,
                  controller: bloc.birthdateController,
                ),
                SizedBox(height: responsive(context, 12)),
                Text(
                  'CPF',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: responsive(context, 5)),
                InputStream(
                  style: textFormStyle,
                  stream: bloc.cpf,
                  onChanged: bloc.changeCPF,
                  decoration: inputDecoration.copyWith(
                    hintText: '000.000.000-00',
                  ),
                  // decoration: ComooStyles.inputCustomDecoration('CPF').copyWith(
                  //   hintText: '000.0000.000-00',
                  //   labelStyle: labelFormStyle,
                  // ),
                  keyboardType: TextInputType.number,
                  controller: bloc.cpfController,
                ),
                SizedBox(height: responsive(context, 12)),
                Text(
                  'TELEFONE (COM DDD)',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: responsive(context, 5)),
                InputStream(
                  style: textFormStyle,
                  stream: bloc.phone,
                  onChanged: bloc.changePhone,
                  decoration: inputDecoration.copyWith(
                    hintText: '(00) 99999-0000',
                    // prefix: Container(
                    //   padding: EdgeInsets.only(right: responsive(context, 5.0)),
                    //   child: Text(
                    //     '+55',
                    //     style: prefixStyle,
                    //   ),
                    // ),
                  ),
                  // decoration:
                  //     ComooStyles.inputCustomDecoration('CELULAR').copyWith(
                  //   labelStyle: labelFormStyle,
                  //   hintText: '(00) 99999-0000',
                  //   prefix: Container(
                  //     padding: EdgeInsets.only(right: responsive(context, 5.0)),
                  //     child: Text(
                  //       '+55',
                  //       style: prefixStyle,
                  //     ),
                  //   ),
                  // ),
                  keyboardType: TextInputType.number,
                  controller: bloc.phoneController,
                ),
                SizedBox(height: responsive(context, 12)),
                Text(
                  'CEP',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: responsive(context, 5)),
                InputStream(
                  style: textFormStyle,
                  stream: bloc.zip,
                  onChanged: bloc.changeZip,
                  decoration: inputDecoration.copyWith(
                    hintText: '00000-00',
                  ),
                  // decoration: ComooStyles.inputCustomDecoration('CEP').copyWith(
                  //   hintText: '00000-00',
                  //   labelStyle: labelFormStyle,
                  // ),
                  keyboardType: TextInputType.number,
                  controller: bloc.zipController,
                ),
                SizedBox(height: responsive(context, 12)),
                Text(
                  'ENDEREÇO',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: responsive(context, 5)),
                InputStream(
                  style: textFormStyle,
                  stream: bloc.addressName,
                  onChanged: bloc.changeAddressName,
                  decoration: inputDecoration,
                  // decoration: ComooStyles.inputCustomDecoration('ENDEREÇO')
                  //     .copyWith(labelStyle: labelFormStyle),
                  keyboardType: TextInputType.text,
                  controller: bloc.addressNameController,
                ),
                SizedBox(height: responsive(context, 12)),
                Text(
                  'NÚMERO',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: responsive(context, 5)),
                InputStream(
                  style: textFormStyle,
                  stream: bloc.addressNumber,
                  onChanged: bloc.changeAddressNumber,
                  decoration: inputDecoration,
                  // decoration: ComooStyles.inputCustomDecoration('NÚMERO')
                  //     .copyWith(labelStyle: labelFormStyle),
                  keyboardType: TextInputType.number,
                  onSubmitted: null,
                  controller: bloc.addressNumberController,
                ),
                SizedBox(height: responsive(context, 12)),
                Text(
                  'COMPLEMENTO',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: responsive(context, 5)),
                InputStream(
                    style: textFormStyle,
                    stream: bloc.addressComplement,
                    onChanged: bloc.changeAddressComplement,
                    decoration: inputDecoration,
                    // decoration: ComooStyles.inputCustomDecoration('COMPLEMENTO')
                    //     .copyWith(labelStyle: labelFormStyle),
                    keyboardType: TextInputType.text,
                    controller: bloc.addressComplementController),
                SizedBox(height: responsive(context, 12)),
                Text(
                  'BAIRRO',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: responsive(context, 5)),
                InputStream(
                  style: textFormStyle,
                  stream: bloc.addressDistrict,
                  onChanged: bloc.changeAddressDistrict,
                  decoration: inputDecoration,
                  // decoration: ComooStyles.inputCustomDecoration('BAIRRO')
                  //     .copyWith(labelStyle: labelFormStyle),
                  keyboardType: TextInputType.text,
                  controller: bloc.addressDistrictController,
                ),
                SizedBox(height: responsive(context, 12)),
                Text(
                  'ESTADO',
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: responsive(context, 5)),
                StreamBuilder<StateItem>(
                  stream: bloc.state,
                  builder: (BuildContext contextState,
                      AsyncSnapshot<StateItem> stateSnap) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextField(
                          onTap: () async {
                            FocusScope.of(context).requestFocus(FocusNode());
                            SystemChannels.textInput
                                .invokeMethod<void>('TextInput.hide');
                            int initialItem =
                                bloc.getStates.indexOf(stateSnap.data);
                            initialItem = initialItem == -1 ? 0 : initialItem;
                            // onTap();
                            await customCupertinoModal(
                              context: context,
                              initialItem: initialItem,
                              onChange: (int index) {
                                final StateItem item = bloc.getStates[index];
                                bloc.changeState(item);
                                bloc.stateController.text = item.displayName;
                                bloc.changeCity('');
                                bloc.cityController.text = 'selecione';
                              },
                              children: List<Widget>.generate(
                                  bloc.getStates.length, (int index) {
                                return Center(
                                  child: Text(bloc.getStates[index].name),
                                );
                              }),
                            );
                          },
                          controller: bloc.stateController,
                          // focusNode: stateFocus,
                          autofocus: false,
                          enableInteractiveSelection: false,
                          textCapitalization: TextCapitalization.none,
                          style: textFormStyle,
                          decoration: inputDecoration.copyWith(
                            hintText: 'ESCOLHA O ESTADO',
                            hintStyle: TextStyle(
                              fontFamily: COMOO.fonts.montSerrat,
                              fontSize: responsive(context, 13),
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF4A294E),
                            ),
                          ),
                          // decoration:
                          //     ComooStyles.inputCustomDecoration('ESTADO')
                          //         .copyWith(
                          //   suffix: const Icon(Icons.expand_more),
                          //   labelStyle: labelFormStyle,
                          //   // isDense: true,
                          // ),
                        ),
                        SizedBox(height: responsive(context, 12)),
                        Text(
                          'CIDADE',
                          style: Theme.of(context).textTheme.caption.copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                              ),
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(height: responsive(context, 5)),
                        CupertinoDropdownInputStream<String>(
                          initialData: 'selecione',
                          stream: bloc.city,
                          controller: bloc.cityController,
                          keyboardType: TextInputType.number,
                          style: textFormStyle,
                          decoration: inputDecoration.copyWith(
                            hintText: 'ESCOLHA A CIDADE',
                            hintStyle: TextStyle(
                              fontFamily: COMOO.fonts.montSerrat,
                              fontSize: responsive(context, 13),
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF4A294E),
                            ),
                          ),
                          // decoration:
                          //     ComooStyles.inputCustomDecoration('CIDADE')
                          //         .copyWith(labelStyle: labelFormStyle),
                          onChange: bloc.changeCity,
                          convertToString: (String s) => s,
                          items:
                              stateSnap.data?.cities ?? <String>['selecione'],
                          children: List<Widget>.generate(
                            (stateSnap.data?.cities ?? <String>['selecione'])
                                    .length ??
                                0,
                            (int index) {
                              return Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: responsive(context, 30.0)),
                                child: Text(
                                  stateSnap.hasData
                                      ? '${stateSnap.data.cities[index]}'
                                      : 'selecione',
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    );
                  },
                ),
                SizedBox(height: responsive(context, 32.0)),
                StreamBuilder<bool>(
                  stream: bloc.submitValid,
                  builder: (BuildContext submitContext,
                      AsyncSnapshot<bool> submitSnap) {
                    return Container(
                      alignment: FractionalOffset.center,
                      child: RaisedButton(
                        elevation: 0.0,
                        shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(responsive(context, 30.0)),
                        ),
                        color: COMOO.colors.purple,
                        padding: EdgeInsets.symmetric(
                            horizontal: responsive(context, 35.0)),
                        // borderSide: BorderSide(color: Color.fromRGBO(70, 30, 90, 1.0)),
                        highlightElevation: 10.0,
                        child: FittedBox(
                          child: const Text(
                            'AVANÇAR',
                            style: ComooStyles.botao_branco,
                          ),
                        ),
                        onPressed: submitSnap.hasData || true
                            ? () {
                                // print('handle');
                                _save(context, bloc);
                              }
                            : null,
                      ),
                    );
                  },
                ),
                SizedBox(height: responsive(context, 40.0)),
                // MenuCategory(
                //   title: 'DADOS PESSOAIS',
                //   padding:
                //       EdgeInsets.symmetric(horizontal: responsive(context, 24.0)),
                //   children: <Widget>[
                //   ],
                // ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPainters({Widget child}) {
    return CustomPaint(
      painter: RectPainter(
        topLeft: 0.03,
        topRight: 0.09,
      ),
      child: CustomPaint(
        painter: RectPainter(
          color: COMOO.colors.turquoise,
          topLeft: 0.09,
        ),
        child: child,
      ),
    );
  }

  Future<void> _save(BuildContext context, SignupPersonalBloc bloc) async {
    bloc.changeLoading(true);
    print('complemento: ${bloc.addressComplementValue}');
    Map<String, Object> param = <String, Object>{
      'phone': bloc.phoneMapValue,
      'birthDate': bloc.birthdateValue,
      'cpf': bloc.cpfValue,
      'address': <String, String>{
        'zipCode': bloc.zipValue,
        'street': bloc.addressNameValue,
        'district': bloc.addressDistrictValue,
        'complement': bloc.addressComplementValue,
        'number': bloc.addressNumberValue ?? '',
        'city': bloc.cityValue,
        'uf': bloc.stateValue.initials,
        'state': bloc.stateValue.name,
        'country': 'BRA',
      },
    };
    print(param);
    final bool isSuccess = await api.updatePersonalData(context, param);

    if (isSuccess) {
      Navigator.of(context).pop();
      // final bool goCategories =
      //     user.interests == null || (user.interests ?? <dynamic>[]).isEmpty;
      // if (goCategories) {
      //   print('go fill categories');
      //   Navigator.of(context).pushReplacementNamed('/cat_sign_up');
      // } else {
      //   final bool goHome = await bloc.goHome(user);
      //   if (goHome) {
      //     Navigator.of(context).pushReplacementNamed('/home');
      //   } else {
      //     Navigator.of(context).pushReplacementNamed('/sign_up_groups');
      //   }
      // }
      // Navigator.of(context).pushReplacementNamed("/cat_sign_up");
      // welcomeNavigation(context, user);
      // showMessageDialog(context, 'Dados pessoais salvos com sucesso!');
      //bloc.changeEdit(false);
      // _fetchCurrentUser();
    }
    bloc.changeLoading(false);
  }
}
