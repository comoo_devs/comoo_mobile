import 'package:comoo/src/blocs/signup_invite_code_bloc.dart';
import 'package:comoo/src/pages/signup/signup_invite_code/signup_invite_code_view.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:flutter/material.dart';

class SignUpInviteCode extends StatefulWidget {
  SignUpInviteCode({this.code = ''});
  final String code;
  @override
  State<StatefulWidget> createState() => _SignUpInviteCodeState();
}

class _SignUpInviteCodeState extends State<SignUpInviteCode> {
  SignUpInviteCodeBloc bloc;

  @override
  void initState() {
    super.initState();

    bloc = widget.code.isEmpty
        ? SignUpInviteCodeBloc()
        : SignUpInviteCodeBloc.seeded(widget.code);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SignUpInviteCodeBloc>(
      bloc: bloc,
      child: SignUpInviteCodeView(),
    );
  }
}
