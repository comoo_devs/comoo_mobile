import 'package:comoo/src/blocs/signup_invite_code_bloc.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';

import '../tos.dart';

class SignUpInviteCodeView extends StatelessWidget with Reusables {
  final String code = 'VEMPRACOMOO';

  @override
  Widget build(BuildContext context) {
    final Size device = MediaQuery.of(context).size;
    final SignUpInviteCodeBloc bloc =
        BlocProvider.of<SignUpInviteCodeBloc>(context);
    final Color borderColor = COMOO.colors.lead;
    final InputDecoration inputDecoration = InputDecoration(
      hintStyle: TextStyle(
        fontFamily: COMOO.fonts.montSerrat,
        fontSize: responsiveWidth(context, 13),
        color: COMOO.colors.medianGray,
        fontWeight: FontWeight.w600,
      ),
      contentPadding: EdgeInsets.symmetric(
        horizontal: responsive(context, 15),
        vertical: responsive(context, 8),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: borderColor,
        ),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: borderColor,
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: borderColor,
        ),
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: borderColor,
        ),
      ),
      filled: true,
      fillColor: Colors.white,
    );
    return Scaffold(
      body: StreamBuilder<bool>(
          stream: bloc.isLoading,
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            final bool isLoading = snapshot.data ?? false;
            return Stack(
              children: <Widget>[
                ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    SizedBox(height: responsiveHeight(context, 31)),
                    Image.asset(
                      'images/logo_fb.png',
                      width: responsiveWidth(context, 302),
                      height: responsiveWidth(context, 62.0),
                    ),
                    SizedBox(height: responsiveHeight(context, 90)),
                    Text(
                      'Recebeu um \n' 'código de convite?\n' 'Digite aqui:',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title.copyWith(
                            fontWeight: FontWeight.w600,
                            fontSize: responsive(context, 24),
                            color: COMOO.colors.turquoise,
                          ),
                    ),
                    SizedBox(height: responsiveHeight(context, 25)),
                    Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: responsiveWidth(context, 97),
                      ),
                      child: InputStream(
                        stream: bloc.inviteCode,
                        onChanged: bloc.editInviteCode,
                        controller: bloc.codeController,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        style: ComooStyles.inputText.copyWith(
                          color: COMOO.colors.turquoise,
                        ),
                        textAlign: TextAlign.center,
                        decoration: inputDecoration.copyWith(
                          hintText: 'digite seu código',
                        ),
                      ),
                    ),
                    SizedBox(height: responsiveHeight(context, 24)),
                    Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: responsiveWidth(context, 97),
                      ),
                      constraints: BoxConstraints(
                        maxWidth: responsive(context, 180),
                        maxHeight: responsive(context, 36),
                      ),
                      alignment: FractionalOffset.center,
                      child: OutlineButton(
                        padding: EdgeInsets.symmetric(
                          horizontal: responsiveWidth(context, 10),
                        ),
                        borderSide: BorderSide(color: COMOO.colors.turquoise),
                        highlightedBorderColor: COMOO.colors.turquoise,
                        highlightElevation: 10.0,
                        child: FittedBox(
                          child: Text(
                            'NĀO RECEBI O CÓDIGO',
                            style: Theme.of(context).textTheme.button.copyWith(
                                  color: COMOO.colors.turquoise,
                                ),
                          ),
                        ),
                        onPressed: () {
                          bloc.codeController.text = code;
                          bloc.editInviteCode(code);
                        },
                      ),
                    ),
                    SizedBox(height: responsiveHeight(context, 105)),
                    Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: responsiveWidth(context, 30.5),
                      ),
                      child: StreamBuilder<bool>(
                        stream: bloc.tos,
                        builder: (BuildContext context,
                            AsyncSnapshot<bool> snapshot) {
                          final bool tos = snapshot.data ?? false;
                          final TextStyle tosStyle =
                              Theme.of(context).textTheme.body1.copyWith(
                                    color: COMOO.colors.purple,
                                    fontSize: responsive(context, 12),
                                  );
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: tos
                                    ? IconButton(
                                        icon: Image.asset(
                                          'images/checked_turquoise.png',
                                          scale: responsive(context, 27.0),
                                        ),
                                        onPressed: () => bloc.editToS(!tos),
                                      )
                                    : IconButton(
                                        icon: Image.asset(
                                          'images/unchecked.png',
                                          scale: responsive(context, 27.0),
                                        ),
                                        onPressed: () => bloc.editToS(!tos),
                                      ),
                              ),
                              Expanded(
                                child: GestureDetector(
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'Concordo com os ',
                                            style: tosStyle),
                                        TextSpan(
                                          text:
                                              'Termos e condições de uso e Política de Privacidade',
                                          style: tosStyle.copyWith(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: () async {
                                    final bool result =
                                        await Navigator.of(context)
                                            .push(MaterialPageRoute<bool>(
                                      builder: (BuildContext context) =>
                                          TermsOfServicePage(),
                                    ));

                                    bloc.editToS(result ?? false);
                                  },
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                    SizedBox(height: responsiveHeight(context, 38)),
                    StreamBuilder<bool>(
                        stream: bloc.isValid,
                        builder: (BuildContext contextValid,
                            AsyncSnapshot<bool> valid) {
                          return StreamBuilder<bool>(
                              stream: bloc.tos,
                              builder: (BuildContext contextToS,
                                  AsyncSnapshot<bool> snapshot) {
                                final bool tos = snapshot.data ?? false;
                                // print('valid: ${valid.hasData} | tos: ${tos.data}');
                                return Container(
                                  constraints: BoxConstraints(
                                    minWidth: device.width * 0.616,
                                    maxHeight: device.height * 0.045,
                                  ),
                                  margin: EdgeInsets.symmetric(
                                    horizontal: responsiveWidth(context, 97),
                                  ),
                                  alignment: Alignment.center,
                                  child: RaisedButton(
                                    elevation: 0.0,
                                    color: COMOO.colors.pink,
                                    padding: EdgeInsets.symmetric(
                                      horizontal: responsiveWidth(context, 53),
                                    ),
                                    child: FittedBox(
                                      child: Text(
                                        'AVANÇAR',
                                        style:
                                            Theme.of(context).textTheme.button,
                                      ),
                                    ),
                                    onPressed: (valid.hasData && tos) || true
                                        ? () => _handleSubmitted(context, bloc)
                                        : null,
                                  ),
                                );
                              });
                        }),
                    SizedBox(height: responsiveHeight(context, 45)),
                  ],
                ),
                Positioned(
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: isLoading
                      ? LoadingContainer(message: 'Carregando...')
                      : Container(),
                ),
              ],
            );
          }),
    );
  }

  Future<void> _handleSubmitted(
      BuildContext context, SignUpInviteCodeBloc bloc) async {
    final bool hasAgreedWithToS = bloc.getTos;
    if (!hasAgreedWithToS) {
      showMessageDialog(context,
          'Você precisa concordar os Termos e condiçoões de uso e Política de Privacidade antes de continuar.');
      return;
    }
    bloc.editLoading(true);
    if (await isConnected()) {
      final bool result = await bloc.handleSubmitted(context);
      if (result ?? false) {
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/cat_sign_up', (_) => false);
      }
    } else {
      showConnectionErrorMessage(context);
    }
    bloc.editLoading(false);
  }
}
