import 'dart:async';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/api/network.dart';
import 'package:comoo/src/utility/states_list.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/painters/rect_painter.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/comoo/categories_model.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rxdart/rxdart.dart';

class CategoriesSignUpPage extends StatefulWidget {
  @override
  _CategoriesSignUpPageState createState() => _CategoriesSignUpPageState();
}

class _CategoriesSignUpPageState extends State<CategoriesSignUpPage>
    with Reusables {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<CategoriesModel> categoryList = List();
  FirebaseAuth auth = FirebaseAuth.instance;
  UserAuth userAuth = UserAuth();
  UserData newUser = UserData();

  bool _isLoading = false;

  final Firestore db = Firestore.instance;
  List<StateItem> _stateList;
  final BehaviorSubject<StateItem> _state = BehaviorSubject<StateItem>();
  final BehaviorSubject<String> _city = BehaviorSubject<String>();

  List<StateItem> get getStates => _stateList;
  Function(StateItem) get changeState => _state.sink.add;
  Function(String) get changeCity => _city.sink.add;

  Stream<StateItem> get state => _state.stream;
  Stream<String> get city => _city.stream;

  final TextEditingController stateController = TextEditingController();
  final TextEditingController cityController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    fetchCurrentUser();
    categoryList = categoriesModel;

    _stateList = states
        .map<StateItem>((dynamic state) => StateItem.fromJSON(state))
        .toList()
          ..sort((StateItem a, StateItem b) => a.name.compareTo(b.name));

    _fetchLocation();
  }

  @override
  void dispose() {
    super.dispose();

    _state?.close();
    _city?.close();
  }

  Future<void> _fetchLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    print(position);
    if (position != null) {
      List<Placemark> placemarks = await Geolocator()
          .placemarkFromCoordinates(position.latitude, position.longitude);
      Placemark placemark = placemarks.first;
      final String city = placemark.locality;
      final String stateInitial = placemark.administrativeArea;
      final StateItem stateItem = _stateList.firstWhere(
          (StateItem si) => si.initials == stateInitial,
          orElse: () => null);
      if (stateItem != null) {
        if (mounted) {
          setState(() {
            changeState(stateItem);
            stateController.text = stateItem.displayName;
            changeCity(city);
            cityController.text = city;
          });
        }
      }
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(
        new SnackBar(content: Text(value, style: ComooStyles.textoSnackBar)));
  }

  void fetchCurrentUser() async {
    //Get current firebase user
    FirebaseUser user = await auth.currentUser();

    if (user == null) {
      // Navigator.of(context).pop();
    } else {
      setState(() {
        newUser.uid = user?.uid;
      });
    }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _setUserPersonalInformation(
      String uid, List<CategoriesModel> categorySelected) async {
    if (await isConnected()) {
      try {
        final Map<String, dynamic> parameters = <String, dynamic>{
          'uid': uid,
          'categories': categorySelected.map((c) => c.id).toList(),
          'state': _state?.value?.name,
          'city': _city?.value,
        };
        final NetworkApi api = NetworkApi();
        await api.saveCategories(context, parameters);
        // await CloudFunctions.instance
        //     .getHttpsCallable(functionName: 'saveCategories')
        //     .call(<String, dynamic>{
        //   'uid': uid,
        //   'categories': categorySelected.map((c) => c.id).toList(),
        // }).timeout(Duration(seconds: 60));
      } on TimeoutException catch (e) {
        print(e);
        _setUserPersonalInformation(uid, categorySelected);
      } catch (e) {
        print(e);
        rethrow;
      }
    } else {
      showConnectionErrorMessage(context);
    }
  }

  Future<void> _handleSubmit() async {
    if (mounted) {
      setState(() {
        _isLoading = true;
      });
    }

    if (_state.value == null) {
      showInSnackBar('Selecione seu estado para continuar');
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
      return;
    }
    if (_city.value == null) {
      showInSnackBar('Selecione sua cidade para continuar');
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
      return;
    }

    final List<CategoriesModel> categoriesSelected = categoriesModel
        .where((CategoriesModel c) => c.isSelected == true)
        .toList();

    categoriesSelected.forEach((CategoriesModel cat) => print(cat.title));
    if (categoriesSelected.length < 3) {
      showInSnackBar('Selecione ao menos 3 categorias para continuar.');
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
      return;
    }

    try {
      if (newUser != null) {
        await _setUserPersonalInformation(newUser.uid, categoriesSelected);

        if (mounted) {
          setState(() {
            _isLoading = false;
          });
        }
      } else {
        showInSnackBar('Algo deu errado, tente novamente.');

        if (mounted) {
          setState(() {
            _isLoading = false;
          });
        }
      }
      // //Updating profile on ios is broken
      //if (Platform.isAndroid) await _updateProfile();

      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
      categoriesModel.forEach((cat) {
        cat.isSelected = false;
      });
      // Navigator.of(context)
      //     .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);

      print('PRE');
      final bool goHome = (await fetchUserGroups(newUser.uid)).isNotEmpty;
      print('after');
      if (goHome) {
        Navigator.of(context).pushReplacementNamed('/home');
      } else {
        Navigator.of(context).pushReplacementNamed('/sign_up_groups');
      }
    } catch (error) {
      print(error);
      showInSnackBar('Algo deu errado, tente novamente.');
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  Future<List<Group>> fetchUserGroups(String id) async {
    final Firestore db = Firestore.instance;
    final QuerySnapshot query = await db
        .collection('users')
        .document(id)
        .collection('groups')
        .getDocuments();
    if (query?.documents?.isEmpty ?? true) {
      return <Group>[];
    } else {
      return query.documents
          .map<Group>((DocumentSnapshot snap) => Group.fromUserSnapshot(snap))
          .toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    final double mediaQuery = MediaQuery.of(context).size.width / 400;

    final TextStyle textFormStyle = TextStyle(
      fontFamily: ComooStyles.fontFamily,
      color: ComooColors.chumbo,
      fontSize: responsive(context, 15.0),
      fontWeight: FontWeight.w500,
    );
    final InputDecoration inputDecoration = InputDecoration(
      contentPadding: EdgeInsets.symmetric(
        horizontal: responsive(context, 15),
        vertical: responsive(context, 8),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide.none,
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide.none,
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: COMOO.colors.turquoise,
        ),
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide.none,
      ),
      filled: true,
      fillColor: Colors.white,
    );
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      // appBar: CustomAppBar.round(
      //   color: Colors.transparent,
      //   title: Container(),
      //   // title: Text(
      //   //   'CATEGORIAS',
      //   //   style: ComooStyles.appBarTitleSixteen,
      //   // ),
      // ),
      // body: _isLoading
      //     ? LoadingContainer()
      //     :
      body: Stack(
        children: <Widget>[
          Container(
            // margin: EdgeInsets.symmetric(
            //   horizontal: responsive(context, 21.0),
            // ),
            child: CustomScrollView(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              slivers: <Widget>[
                SliverToBoxAdapter(
                  child: _buildPainters(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: responsiveHeight(context, 269),
                      child: ListView(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        children: <Widget>[
                          // Align(
                          //   alignment: Alignment.centerLeft,
                          //   child: BackButton(color: Colors.white),
                          // ),
                          SizedBox(height: responsiveHeight(context, 40)),
                          Padding(
                            padding: EdgeInsets.only(
                              left: responsiveWidth(context, 37),
                            ),
                            child: Text(
                              'Escolha sua regiāo para \n'
                              'sugerirmos as melhores \n'
                              'comoonidades para você',
                              style:
                                  Theme.of(context).textTheme.headline.copyWith(
                                        color: COMOO.colors.purple,
                                        fontSize: responsive(context, 18),
                                        fontWeight: FontWeight.w600,
                                      ),
                            ),
                          ),
                          SizedBox(height: responsiveHeight(context, 14)),
                          StreamBuilder<StateItem>(
                            stream: state,
                            builder: (BuildContext contextState,
                                AsyncSnapshot<StateItem> stateSnap) {
                              return ListView(
                                shrinkWrap: true,
                                padding: EdgeInsets.symmetric(
                                  horizontal: responsiveWidth(context, 25),
                                ),
                                physics: const ClampingScrollPhysics(),
                                // crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  TextField(
                                    onTap: () async {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      SystemChannels.textInput
                                          .invokeMethod<void>('TextInput.hide');
                                      int initialItem =
                                          getStates.indexOf(stateSnap.data);
                                      initialItem =
                                          initialItem == -1 ? 0 : initialItem;
                                      // onTap();
                                      await customCupertinoModal(
                                        context: context,
                                        initialItem: initialItem,
                                        onChange: (int index) {
                                          final StateItem item =
                                              getStates[index];
                                          changeState(item);
                                          stateController.text =
                                              item.displayName;
                                          changeCity('');
                                          cityController.text = 'selecione';
                                        },
                                        children: List<Widget>.generate(
                                            getStates.length, (int index) {
                                          return Center(
                                            child: Text(getStates[index].name),
                                          );
                                        }),
                                      );
                                    },
                                    controller: stateController,
                                    // focusNode: stateFocus,
                                    autofocus: false,
                                    enableInteractiveSelection: false,
                                    textCapitalization: TextCapitalization.none,
                                    style: textFormStyle,
                                    decoration: inputDecoration.copyWith(
                                      hintText: 'Estado',
                                      hintStyle: TextStyle(
                                        fontFamily: COMOO.fonts.montSerrat,
                                        fontSize: responsive(context, 13),
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xFF4A294E),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                      height: responsiveHeight(context, 9)),
                                  CupertinoDropdownInputStream<String>(
                                    initialData: 'selecione',
                                    stream: city,
                                    controller: cityController,
                                    keyboardType: TextInputType.number,
                                    style: textFormStyle,
                                    decoration: inputDecoration.copyWith(
                                      hintText: 'Cidade',
                                      hintStyle: TextStyle(
                                        fontFamily: COMOO.fonts.montSerrat,
                                        fontSize: responsive(context, 13),
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xFF4A294E),
                                      ),
                                    ),
                                    onChange: changeCity,
                                    convertToString: (String s) => s,
                                    items: stateSnap.data?.cities ??
                                        <String>['selecione'],
                                    children: List<Widget>.generate(
                                      (stateSnap.data?.cities ??
                                                  <String>['selecione'])
                                              .length ??
                                          0,
                                      (int index) {
                                        return Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal:
                                                  responsive(context, 30.0)),
                                          child: Text(
                                            stateSnap.hasData
                                                ? '${stateSnap.data.cities[index]}'
                                                : 'selecione',
                                            textAlign: TextAlign.left,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: responsiveWidth(context, 19),
                      top: responsiveHeight(context, 21),
                      bottom: responsiveHeight(context, 33),
                    ),
                    child: Text(
                      'Marque seus interesses:',
                      style: TextStyle(
                        fontFamily: COMOO.fonts.montSerrat,
                        color: COMOO.colors.pink,
                        fontSize: responsive(context, 18),
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
                SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: mediaQuery * 28.0,
                    crossAxisSpacing: mediaQuery * 2.0,
                    childAspectRatio: mediaQuery * 0.90,
                  ),
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int i) {
                      return Container(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              categoryList[i].isSelected =
                                  !categoryList[i].isSelected;
                              // print(newUser.uid);
                              // if (categoryList[i].isSelected == true) {
                              //   print('${categoryList[i].title} is selected');
                              // } else {
                              //   print('${categoryList[i].title} is unselected');
                              // }
                            });
                          },
                          child: Center(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: mediaQuery * 101.0,
                                  height: mediaQuery * 99.0,
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Icon(
                                        categoryList[i].iconData,
                                        size: mediaQuery * 96.0,
                                        color: COMOO.colors.turquoise,
                                      ),
                                      Positioned(
                                        right: 0.0,
                                        top: 0.0,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                          ),
                                          child: Image.asset(
                                            !categoryList[i].isSelected
                                                ? 'images/add_white.png'
                                                : 'images/checked_turquoise.png',
                                            width: responsive(context, 28.0),
                                            height: responsive(context, 28.0),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(top: mediaQuery * 5.0),
                                  child: Text(
                                    categoryList[i].title,
                                    textAlign: TextAlign.center,
                                    style: ComooStyles.categoriesBold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    childCount: categoryList.length,
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    alignment: FractionalOffset.center,
                    child: RaisedButton(
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.circular(responsive(context, 30.0)),
                      ),
                      color: COMOO.colors.pink,
                      padding: EdgeInsets.symmetric(
                          horizontal: responsive(context, 35.0)),
                      // borderSide: BorderSide(color: Color.fromRGBO(70, 30, 90, 1.0)),
                      highlightElevation: 10.0,
                      child: FittedBox(
                        child: const Text(
                          'AVANÇAR',
                          style: ComooStyles.botao_branco,
                        ),
                      ),
                      onPressed: _handleSubmit,
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: SizedBox(height: responsiveHeight(context, 42)),
                ),
              ],
            ),
          ),
          Positioned(
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            child: _isLoading ? LoadingContainer() : Container(),
          ),
        ],
      ),
    );
  }

  Widget _buildPainters({Widget child}) {
    return CustomPaint(
      painter: RectPainter(color: Colors.white),
      child: CustomPaint(
        painter: RectPainter(
          // bottomRight: 0.76,
          bottomLeft: 269 / 313,
          color: COMOO.colors.white500,
        ),
        child: child,
      ),
    );
  }
}
