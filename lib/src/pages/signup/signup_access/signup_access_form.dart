import 'dart:io';

import 'package:comoo/src/blocs/signup_bloc.dart';
import 'package:comoo/src/pages/signup/signup_invite_code/signup_invite_code.dart';
import 'package:comoo/src/widgets/buttons.dart';
import 'package:comoo/src/widgets/painters/rect_painter.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:comoo/src/widgets/input_stream.dart';
import 'package:comoo/src/widgets/loading_container.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:comoo/src/widgets/round_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class SignupAccessForm extends StatelessWidget with Reusables {
  SignupAccessForm({Key key}) : super(key: key);
  final String code = 'VEMPRACOMOO';

  @override
  Widget build(BuildContext context) {
    final SignupBloc bloc = BlocProvider.of<SignupBloc>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      backgroundColor: COMOO.colors.white500,
      // key: _scaffoldKey,
      appBar: CustomAppBar.round(
        brightness: Brightness.light,
        color: Colors.white,
        iconTheme: Theme.of(context).iconTheme.copyWith(
              color: COMOO.colors.lead,
            ),
        title: Text(
          'Bem-vindx',
          style: Theme.of(context).textTheme.title.copyWith(
                color: COMOO.colors.purple,
                fontWeight: FontWeight.w600,
              ),
        ),
      ),
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
            SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
          },
          child: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: StreamBuilder(
              initialData: false,
              stream: bloc.isLoading,
              builder: (BuildContext contextLoading, AsyncSnapshot snap) {
                final bool isLoading = snap.data ?? false;
                return Stack(
                  children: <Widget>[
                    _buildForm(context, bloc),
                    Positioned(
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: isLoading
                          ? LoadingContainer(message: 'Carregando...')
                          : Container(),
                    ),
                  ],
                );
                // return snap.data
                //     ? LoadingContainer(message: 'Carregando...')
                //     : ;
              },
            ),
          )),
    );
  }

  Widget _buildForm(BuildContext context, SignupBloc bloc) {
    final Size device = MediaQuery.of(context).size;
    final Color borderColor = COMOO.colors.turquoise;
    final InputDecoration inputDecoration = InputDecoration(
      contentPadding: EdgeInsets.symmetric(
        horizontal: responsive(context, 15),
        vertical: responsive(context, 8),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide.none,
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide.none,
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: borderColor,
        ),
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide.none,
      ),
      filled: true,
      fillColor: Colors.white,
    );
    return Column(
      // shrinkWrap: true,
      // physics: ClampingScrollPhysics(),
      // padding: EdgeInsets.all(responsive(context, 20.0)),
      children: <Widget>[
        CustomPaint(
          painter: RectPainter(
            color: Colors.white,
          ),
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.192,
            ),
            physics: const ClampingScrollPhysics(),
            children: <Widget>[
              _buildFacebookLoginButton(context, bloc),
              _buildGoogleLoginButton(context, bloc),
              SizedBox(height: device.height * 0.014),
            ],
          ),
        ),
        CustomPaint(
          painter: RectPainter(
            color: Colors.white,
          ),
          child: CustomPaint(
            painter: RectPainter(
              color: COMOO.colors.white500,
              topRight: 0.12,
            ),
            child: ListView(
              padding: EdgeInsets.symmetric(
                horizontal: responsive(context, 25),
              ),
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                SizedBox(height: device.height * 0.095),
                Text(
                  'ou preencha seu cadastro',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: COMOO.colors.purple,
                        fontWeight: FontWeight.w600,
                        fontSize: responsive(context, 18),
                      ),
                ),
                SizedBox(height: responsive(context, 25)),
                Container(
                  child: GestureDetector(
                    child: _buildImagePicker(context, bloc),
                    onTap: () => selectOrigin(context, bloc),
                  ),
                  alignment: FractionalOffset.center,
                ),
                SizedBox(height: responsive(context, 10)),
                Container(
                  child: GestureDetector(
                    child: Text(
                      'FOTO PERFIL',
                      style: Theme.of(context).textTheme.caption.copyWith(
                            color: COMOO.colors.purple,
                            fontWeight: FontWeight.w600,
                            fontSize: responsive(context, 13),
                          ),
                      textAlign: TextAlign.center,
                    ),
                    onTap: () => selectOrigin(context, bloc),
                  ),
                  alignment: FractionalOffset.center,
                ),
                SizedBox(height: device.height * 0.038),
                InputStream(
                  stream: bloc.name,
                  onChanged: bloc.editName,
                  controller: bloc.nameController,
                  decoration: inputDecoration.copyWith(
                    hintText: 'nome',
                  ),
                  style: ComooStyles.inputText,
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.words,
                ),
                SizedBox(height: device.height * 0.011),
                InputStream(
                  stream: bloc.nickname,
                  onChanged: bloc.editNickname,
                  controller: bloc.nicknameController,
                  decoration: inputDecoration.copyWith(
                    hintText: 'usuario( somente letras, números, . e _)',
                  ),
                  textCapitalization: TextCapitalization.none,
                  keyboardType: TextInputType.text,
                  style: ComooStyles.inputText,
                ),
                SizedBox(height: device.height * 0.011),
                InputStream(
                  stream: bloc.email,
                  onChanged: bloc.editEmail,
                  controller: bloc.emailController,
                  keyboardType: TextInputType.emailAddress,
                  style: ComooStyles.inputText,
                  textCapitalization: TextCapitalization.none,
                  decoration: inputDecoration.copyWith(
                    hintText: 'email',
                  ),
                ),
                SizedBox(height: device.height * 0.011),
                StreamBuilder<bool>(
                  initialData: false,
                  stream: bloc.obscurePassword,
                  builder: (BuildContext context, AsyncSnapshot<bool> obscure) {
                    return InputStream(
                      stream: bloc.password,
                      onChanged: bloc.addPassword,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.none,
                      style: ComooStyles.inputText,
                      decoration: inputDecoration.copyWith(
                        hintText: 'senha',
                      ),
                      obscureText: obscure.data,
                      showObscureIcon: true,
                      onObscureClick: (bool obscure) =>
                          bloc.editObscurePassword(!obscure),
                    );
                  },
                ),
                SizedBox(height: device.height * 0.025),
                StreamBuilder<bool>(
                    stream: bloc.isValid,
                    builder:
                        (BuildContext contextValid, AsyncSnapshot<bool> valid) {
                      return Container(
                        constraints: BoxConstraints(
                          minWidth: device.width * 0.616,
                          maxHeight: device.height * 0.045,
                        ),
                        alignment: Alignment.center,
                        child: RaisedButton(
                          elevation: 0.0,
                          color: COMOO.colors.turquoise,
                          padding: EdgeInsets.symmetric(
                            horizontal: device.width * 0.208,
                          ),
                          child: FittedBox(
                            child: Text(
                              'AVANÇAR',
                              style: Theme.of(context).textTheme.button,
                            ),
                          ),
                          onPressed: () => _handleSubmitted(context, bloc),
                        ),
                      );
                    }),
                SizedBox(height: device.height * 0.041),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildFacebookLoginButton(BuildContext context, SignupBloc bloc) {
    return Container(
      padding: EdgeInsets.only(top: responsive(context, 5.0)),
      // width: MediaQuery.of(context).size.width * 0.65,
      // height: MediaQuery.of(context).size.height * 0.065,
      child: FacebookButton(
        onPressed: () => bloc.handleFacebookAuth(context,
            onSuccess: () => _navigateToNextPage(context)),
        text: 'Cadastrar com Facebook',
      ),
    );
  }

  Widget _buildGoogleLoginButton(BuildContext context, SignupBloc bloc) {
    return Container(
      padding: EdgeInsets.only(top: responsiveHeight(context, 11)),
      // width: MediaQuery.of(context).size.width * 0.65,
      // height: MediaQuery.of(context).size.height * 0.065,
      child: GoogleButton(
        onPressed: () => bloc.handleGoogleAuth(context,
            onSuccess: () => _navigateToNextPage(context)),
        text: 'Cadastrar com Google',
      ),
    );
  }

  Future<void> _handleSubmitted(BuildContext context, SignupBloc bloc) async {
    // final bool hasAgreedWithToS = bloc.getTos;
    // if (!hasAgreedWithToS) {
    //   showMessageDialog(context,
    //       'Você precisa concordar os Termos e condiçoões de uso e Política de Privacidade antes de continuar.');
    //   return;
    // }
    final bool hasImage = bloc.getImage != null;
    if (!hasImage) {
      showMessageDialog(context,
          'Você precisa colocar sua imagem de perfil antes de continuar.');
      return;
    }
    bloc.editLoading(true);

    if (await isConnected()) {
      final bool isUserCreated = await bloc.createUserViaEmail(context);
      if (isUserCreated ?? false) {
        _navigateToNextPage(context);
      }
    } else {
      showConnectionErrorMessage(context);
    }
    bloc.editLoading(false);
  }

  void _navigateToNextPage(BuildContext context) {
    // Navigator.of(context)
    //     .pushNamedAndRemoveUntil('/sign_up_personal', (_) => false);
    final SignupBloc bloc = BlocProvider.of<SignupBloc>(context);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (BuildContext context) =>
                SignUpInviteCode(code: bloc.getCode)),
        (_) => false);
  }

  Widget _buildImagePicker(BuildContext context, SignupBloc bloc) {
    Widget iconContainer = Container(
      width: responsiveWidth(context, 100.0),
      height: responsiveHeight(context, 88.0),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: COMOO.colors.lead),
        color: Colors.white,
      ),
      child: Icon(
        COMOO.icons.perfil.iconData,
        color: COMOO.colors.lead,
        size: responsive(context, 88),
      ),
    );

    Widget image = StreamBuilder<File>(
      stream: bloc.image,
      builder: (BuildContext context, AsyncSnapshot<File> snap) {
        return snap.hasData
            ? Container(
                width: responsive(context, 151.0),
                height: responsive(context, 151.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.cover, image: FileImage(snap.data)),
                    border: Border.all(color: COMOO.colors.lead)),
              )
            : iconContainer;
      },
    );
    return Stack(
      children: <Widget>[
        image,
        Positioned(
          top: 0,
          right: 0,
          child: Container(
            width: responsiveWidth(context, 28),
            height: responsiveWidth(context, 28),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: SvgPicture.asset(
              'svg/grande_cinza.svg',
              fit: BoxFit.fill,
              width: responsiveWidth(context, 28),
              height: responsiveWidth(context, 28),
            ),
          ),
        )
      ],
    );
    // if (userData != null && _avatarImage == null) {
    //   if (userData.photoURL.isNotEmpty) {
    //     return Container(
    //       width: mediaQuery * 151.0,
    //       height: mediaQuery * 151.0,
    //       alignment: Alignment.center,
    //       decoration: BoxDecoration(
    //           shape: BoxShape.circle,
    //           image: DecorationImage(
    //               fit: BoxFit.contain, image: NetworkImage(userData.photoURL)),
    //           border: Border.all(color: COMOO.colors.lead)),
    //       padding: EdgeInsets.only(right: mediaQuery * 17.0),
    //     );
    //   } else {
    //     return iconContainer;
    //   }
    // } else {
    //   if (_avatarImage == null) {
    //     return iconContainer;
    //   } else {
    //     return Container(
    //       width: mediaQuery * 151.0,
    //       height: mediaQuery * 151.0,
    //       decoration: BoxDecoration(
    //           shape: BoxShape.circle,
    //           image: DecorationImage(
    //               fit: BoxFit.cover, image: FileImage(_avatarImage)),
    //           border: Border.all(color: COMOO.colors.lead)),
    //     );
    //   }
    // }
  }

  void selectOrigin(BuildContext context, SignupBloc bloc) async {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    var result = await showDialog<ImageSource>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text(
              'ESCOLHA ORIGEM',
              style: ComooStyles.appBarTitle,
            ),
            children: <Widget>[
              SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, ImageSource.camera);
                  },
                  child: Row(
                    children: <Widget>[
                      const Icon(
                        ComooIcons.camera,
                        color: ComooColors.roxo,
                      ),
                      Padding(
                          padding: EdgeInsets.only(left: mediaQuery * 20.0),
                          child: Text(
                            'Camera',
                            style: ComooStyles.botoes,
                          )),
                    ],
                  )),
              SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, ImageSource.gallery);
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(
                        ComooIcons.galeria,
                        color: ComooColors.roxo,
                        size: mediaQuery * 34.0,
                      ),
                      Padding(
                          padding: EdgeInsets.only(left: mediaQuery * 10.0),
                          child: const Text(
                            'Galeria de Fotos',
                            style: ComooStyles.botoes,
                          )),
                    ],
                  ))
            ],
          );
        });
    if (result == null) {
      return;
    }
    final File image = await getImage(context, result);
    if (image != null) {
      print('new image! $image');
      bloc.addImage(image);
    }
  }

  Future<File> getImage(BuildContext context, ImageSource source) async {
    if (!(await checkPermissions(source))) {
      return null;
    }
    File image = await ImagePicker.pickImage(source: source);
    if (image == null) {
      return null;
    }

    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      ratioX: 1.0,
      ratioY: 1.0,
      maxWidth: 360,
      maxHeight: 360,
      toolbarColor: ComooColors.roxo,
      toolbarTitle: 'COMOO',
    );
    image = null;
    return croppedFile;
    // setState(() {
    //   _avatarImage = croppedFile;
    // });
  }
}
