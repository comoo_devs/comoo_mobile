import 'package:comoo/src/blocs/signup_bloc.dart';
import 'signup_access/signup_access_form.dart';
import 'package:comoo/src/provider/bloc_provider.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({this.code});
  final String code;
  @override
  State<StatefulWidget> createState() => _SignUpState();
}

class _SignUpState extends State<SignUpPage> {
  SignupBloc bloc;
  int counter = 0;

  @override
  void initState() {
    bloc = (widget.code ?? '').isEmpty
        ? SignupBloc()
        : SignupBloc.seeded(code: widget.code);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // print('buildei: ${++counter}x');
    return BlocProvider<SignupBloc>(
      bloc: bloc,
      child: SignupAccessForm(),
    );
  }
}

// class OldSignUpPage extends StatefulWidget {
//   OldSignUpPage({this.code});
//   final String code;
//   @override
//   State createState() {
//     return _SignUpPageState();
//   }
// }

// class _SignUpPageState extends State<OldSignUpPage> with Reusables {
//   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   List<StateItem> statesList = List<StateItem>();
//   List<String> citiesList = List<String>();
//   UserData _user = UserData();
//   UserAuth userAuth = UserAuth();
//   var _confirmPasswordController = TextEditingController();
//   var _passwordController = TextEditingController();
//   GoogleSignInAuthentication googleCredentials;
//   FacebookLoginResult facebookResult;
//   final FocusNode stateFocus = FocusNode();
//   final TextEditingController stateController = TextEditingController();
//   final TextEditingController cityController = TextEditingController();
//   final BehaviorSubject<StateItem> _stateController =
//       BehaviorSubject<StateItem>();
//   Function(StateItem) get changeState => _stateController.sink.add;
//   Stream<StateItem> get stateStream => _stateController.stream;
//   final BehaviorSubject<String> _cityController = BehaviorSubject<String>();
//   Function(String) get changeCity => _cityController.sink.add;
//   Stream<String> get cityStream => _cityController.stream;
//   final StateItem initialState =
//       StateItem(cities: <dynamic>[], name: ' ', initials: 'selecione');

//   final SignupBloc bloc = SignupBloc();

//   FirebaseAuth auth = FirebaseAuth.instance;
//   Validations _validations = Validations();
//   // bool _validateNickname = false;
//   // String _validateNicknameMessage = '';
//   // bool _validateEmail = false;
//   // String _validateEmailMessage = '';

//   TextEditingController _inviteCodeController = TextEditingController();
//   String _inviteCode = '';
//   bool _autoValidate = false;
//   bool _isLoading = false;
//   String _loadMessage = 'Carregando ...';
//   bool _userAgreement = false;
//   File _avatarImage;
//   String _stateInitial = '';
//   String _stateName = '';
//   String _city = '';
//   bool _isSocialLogin = false;
//   String _socialId = '';
//   GoogleSignIn googleSignIn;
//   FacebookLogin facebookLogin;
//   bool showPassword = true;
//   bool showPassword2 = true;

//   final Firestore db = Firestore.instance;

//   Future getImage(ImageSource source) async {
//     if (!(await checkPermissions(source))) {
//       return;
//     }
//     var image = await ImagePicker.pickImage(source: source);
//     if (image == null) return;

//     File croppedFile = await ImageCropper.cropImage(
//       sourcePath: image.path,
//       ratioX: 1.0,
//       ratioY: 1.0,
//       maxWidth: 512,
//       maxHeight: 512,
//       toolbarColor: ComooColors.roxo,
//       toolbarTitle: 'COMOO',
//     );
//     image = null;
//     setState(() {
//       _avatarImage = croppedFile;
//     });
//   }

//   void selectOrigin() async {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     var result = await showDialog<ImageSource>(
//         context: context,
//         builder: (BuildContext context) {
//           return SimpleDialog(
//             title: const Text(
//               'ESCOLHA ORIGEM',
//               style: ComooStyles.appBarTitle,
//             ),
//             children: <Widget>[
//               SimpleDialogOption(
//                   onPressed: () {
//                     Navigator.pop(context, ImageSource.camera);
//                   },
//                   child: Row(
//                     children: <Widget>[
//                       const Icon(
//                         ComooIcons.camera,
//                         color: ComooColors.roxo,
//                       ),
//                       Padding(
//                           padding: EdgeInsets.only(left: mediaQuery * 20.0),
//                           child: Text(
//                             'Camera',
//                             style: ComooStyles.botoes,
//                           )),
//                     ],
//                   )),
//               SimpleDialogOption(
//                   onPressed: () {
//                     Navigator.pop(context, ImageSource.gallery);
//                   },
//                   child: Row(
//                     children: <Widget>[
//                       Icon(
//                         ComooIcons.galeria,
//                         color: ComooColors.roxo,
//                         size: mediaQuery * 34.0,
//                       ),
//                       Padding(
//                           padding: EdgeInsets.only(left: mediaQuery * 10.0),
//                           child: const Text(
//                             'Galeria de Fotos',
//                             style: ComooStyles.botoes,
//                           )),
//                     ],
//                   ))
//             ],
//           );
//         });
//     if (result == null) {
//       return;
//     }
//     getImage(result);
//   }

//   void showInSnackBar(String value) {
//     _scaffoldKey.currentState.showSnackBar(SnackBar(
//         content: Text(
//       value,
//       style: ComooStyles.textoSnackBar,
//       maxLines: 2,
//     )));
//   }

//   void showErrorMessage(String message) async {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return AlertDialog(
//           contentPadding: EdgeInsets.all(mediaQuery * 0.0),
//           content: Column(
//             mainAxisSize: MainAxisSize.min,
//             children: <Widget>[
//               Container(
//                 height: mediaQuery * 180.0,
//                 decoration: BoxDecoration(
//                   color: ComooColors.pink,
//                   borderRadius: BorderRadius.all(
//                     Radius.circular(mediaQuery * 32.0),
//                   ),
//                 ),
//                 child: Center(
//                   child: Column(
//                     mainAxisSize: MainAxisSize.min,
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       Text(
//                         message,
//                         textAlign: TextAlign.center,
//                         style: TextStyle(
//                             color: Colors.white, fontSize: mediaQuery * 18.0),
//                       ),
//                       SizedBox(height: mediaQuery * 30.0),
//                       ButtonBar(
//                         alignment: MainAxisAlignment.center,
//                         children: <Widget>[
//                           Container(
//                             height: mediaQuery * 25.0,
//                             width: mediaQuery * 110.0,
//                             decoration: BoxDecoration(
//                               border:
//                                   Border.all(width: 1.0, color: Colors.white),
//                               borderRadius:
//                                   BorderRadius.circular(mediaQuery * 20.0),
//                             ),
//                             child: FlatButton(
//                               shape: RoundedRectangleBorder(
//                                   borderRadius:
//                                       BorderRadius.circular(mediaQuery * 20.0)),
//                               onPressed: () {
//                                 Navigator.pop(context);
//                               },
//                               child: Text(
//                                 'FECHAR',
//                                 style: TextStyle(color: Colors.white),
//                               ),
//                             ),
//                           )
//                         ],
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         );
//       },
//     );
//   }

//   // Future<String> _validateCode(String value) async {
//   //   Query query =
//   //       db.collection('invites').where('code', isEqualTo: value).limit(1);
//   //   return query.getDocuments().then((querySnapshots) {
//   //     if (querySnapshots.documents.isNotEmpty) {
//   //       return querySnapshots.documents[0].data['uid'];
//   //     } else {
//   //       return '';
//   //     }
//   //   });
//   // }

//   Future<String> _uploadAvatar(uid) async {
//     final StorageReference ref =
//         FirebaseStorage.instance.ref().child('/users/$uid/avatar.jpg');
//     final StorageTaskSnapshot uploadTask =
//         await ref.putFile(_avatarImage).onComplete;
//     final String result = await uploadTask.ref.getDownloadURL().then((onValue) {
//       return onValue.toString();
//     }).catchError((onError) {
//       return '';
//     });
//     return result;
//   }

//   // Future _updateProfile() async {
//   //   UserUpdateInfo updateInfo =  UserUpdateInfo();
//   //   updateInfo.displayName = _user.displayName;
//   //   updateInfo.photoUrl = _user.photoURL;

//   //   return FirebaseAuth.instance.updateProfile(updateInfo);
//   // }

//   // Future<Null> _setUserPersonalInformation(String uid, String originator,
//   //     bool update, String state, String city) async {
//   //   if (_avatarImage != null) {
//   //     _user.photoURL = await _uploadAvatar(uid);
//   //   }

//   //   var address = {'city': _city, 'state': state};
//   //   WriteBatch batch = db.batch();

//   //   final DocumentReference userDocRef = db.collection('users').document(uid);

//   //   // Create user personal information
//   //   bool active = true;
//   //   if (update) {
//   //     batch.updateData(userDocRef, <String, dynamic>{
//   //       'name': _user.displayName,
//   //       'nickname': _user.nickname,
//   //       'email': _user.email,
//   //       'originator': originator,
//   //       'status': 'completed',
//   //       // 'sellerStatus': 'waiting',
//   //       'active': active,
//   //       'photoURL': _user.photoURL,
//   //       'address': address,
//   //       'googeId': googleSignIn == null ? '' : _socialId,
//   //       'fbId': facebookLogin == null ? '' : _socialId,
//   //     });
//   //   } else {
//   //     batch.setData(userDocRef, <String, dynamic>{
//   //       'name': _user.displayName,
//   //       'nickname': _user.nickname,
//   //       'email': _user.email,
//   //       'originator': originator,
//   //       'status': 'completed',
//   //       // 'sellerStatus': 'waiting',
//   //       'active': active,
//   //       'photoURL': _user.photoURL,
//   //       'address': address,
//   //       'googeId': googleSignIn == null ? '' : _socialId,
//   //       'fbId': facebookLogin == null ? '' : _socialId,
//   //     });
//   //   }
//   //   // Set user friendship with originator
//   //   // batch.setData(friendReference, <String, dynamic>{
//   //   //   'origin': 'referred',
//   //   //   'id': db.collection('users').document(originator)
//   //   // });

//   //   // Set originator friendship with user
//   //   // batch.setData(originatorReference,
//   //   //     <String, dynamic>{'origin': 'indicated', 'id': userDocRef});

//   //   return batch.commit();
//   // }

//   // _codeValidation(String value) async {
//   //   Query query = db
//   //       .collection('invites')
//   //       .where('code', isEqualTo: value.toUpperCase())
//   //       .limit(1);
//   //   return query.getDocuments().then((querySnapshots) {
//   //     if (querySnapshots.documents.isNotEmpty) {
//   //       return querySnapshots.documents[0].data['uid'];
//   //     } else {
//   //       return null;
//   //     }
//   //   });
//   // }

//   // _emailValidation(String email) async {
//   //   final validate = _validations.validateEmail(email);
//   //   if (validate != null) return validate;
//   //   return await db
//   //       .collection('users')
//   //       .where('email', isEqualTo: email)
//   //       .getDocuments()
//   //       .then((docs) {
//   //     return docs.documents.isEmpty
//   //         ? null
//   //         : 'Já existe um cadastro com este e-mail';
//   //   }).catchError((onError) {
//   //     return null;
//   //   });
//   // }

//   // _nicknameValidation(String nickname) async {
//   //   final validate = _validations.validateNickname(nickname);
//   //   if (validate != null) return validate;
//   //   return await db
//   //       .collection('users')
//   //       .where('nickname', isEqualTo: nickname)
//   //       .getDocuments()
//   //       .then((docs) {
//   //     return docs.documents.isEmpty ? null : 'Este nome de usuário já existe';
//   //   }).catchError((onError) {
//   //     return null;
//   //   });
//   // }

//   Future<bool> _createUser() async {
//     try {
//       print('SocialID: $_socialId');
//       print('UID: ${_user.uid}');
//       final dynamic result = await CloudFunctions.instance.call(
//         functionName: 'createComooAccount',
//         parameters: <String, dynamic>{
//           'photoURL': _user.photoURL,
//           'name': _user.displayName.trim(),
//           'email': _user.email,
//           'nickname': _user.nickname,
//           'pass': _user.password,
//           'uid': _user.uid,
//           'googleId': googleSignIn == null ? '' : _socialId,
//           'fbId': facebookLogin == null ? '' : _socialId,
//           'code': _inviteCodeController.text,
//           'address': <String, String>{
//             'state': _stateName,
//             'city': _city,
//           },
//         },
//       ).timeout(Duration(seconds: 60));
//       print(result);

//       final FirebaseUser user = await userAuth
//           .verifyUser(UserData(email: _user.email, password: _user.password));
//       // if (facebookLogin != null) {
//       //   userAuth.linkWithFacebook(facebookResult.accessToken);
//       // } else if (googleSignIn != null) {
//       //   userAuth.linkWithGoogle(googleCredentials);
//       // }
//       if (_avatarImage != null) {
//         _user.photoURL = await _uploadAvatar(user.uid);
//         await CloudFunctions.instance.call(
//           functionName: 'attPhoto',
//           parameters: <String, dynamic>{
//             'photo': _user.photoURL,
//           },
//         ).timeout(Duration(seconds: 60));
//       }
//       return true;
//     } on CloudFunctionsException catch (e) {
//       print(e);
//       print(e.message);
//       print(e.details);
//       print(e.code);
//       final String message = e.message == 'INTERNAL'
//           ? 'Algo deu errado, contate o suporte!'
//           : e.message;
//       showSnackbarError(scaffoldKey: _scaffoldKey, text: message);
//     } catch (e) {
//       print(e);
//       showSnackbarError(
//           scaffoldKey: _scaffoldKey,
//           text: 'Algo deu errado, contate o suporte.');
//     }
//     return false;
//   }

//   _handleSubmitted() async {
//     setState(() {
//       _isLoading = true;
//       // _validateNickname = false;
//       // _validateEmail = false;
//     });
//     final FormState form = _formKey.currentState;
//     if (!form.validate()) {
//       showInSnackBar('Verifique os erros antes de continuar.');
//       _autoValidate = true;
//       setState(() {
//         _isLoading = false;
//       });
//       // if (_inviteCodeController.text.isEmpty) {
//       //   final mediaQuery = MediaQuery.of(context).size.width / 400;
//       //   final result = await showCustomDialog(
//       //     context: context,
//       //     alert: customRoundAlert(
//       //       context: context,
//       //       title: 'Você tem código de convite?',
//       //       actions: [
//       //          Container(
//       //           height: mediaQuery * 41.0,
//       //           decoration:  BoxDecoration(
//       //             border: Border.all(
//       //               width: 1.0,
//       //               color: Colors.white,
//       //             ),
//       //             borderRadius:  BorderRadius.circular(20.0),
//       //           ),
//       //           child:  FlatButton(
//       //             shape:  RoundedRectangleBorder(
//       //               borderRadius:  BorderRadius.circular(20.0),
//       //             ),
//       //             onPressed: () {
//       //               Navigator.of(context).pop(true);
//       //             },
//       //             child:  Text(
//       //               'SIM',
//       //               style:  TextStyle(
//       //                 color: Colors.white,
//       //               ),
//       //             ),
//       //           ),
//       //         ),
//       //          Container(
//       //           height: mediaQuery * 41.0,
//       //           decoration:  BoxDecoration(
//       //             border: Border.all(
//       //               width: 1.0,
//       //               color: Colors.white,
//       //             ),
//       //             borderRadius:  BorderRadius.circular(20.0),
//       //           ),
//       //           child:  FlatButton(
//       //             shape:  RoundedRectangleBorder(
//       //               borderRadius:  BorderRadius.circular(20.0),
//       //             ),
//       //             onPressed: () {
//       //               setState(() {
//       //                 _inviteCodeController.text = 'VEMPRACOMOO';
//       //               });
//       //               Navigator.of(context).pop(false);
//       //             },
//       //             child:  Text(
//       //               'NÃO',
//       //               style:  TextStyle(
//       //                 color: Colors.white,
//       //               ),
//       //             ),
//       //           ),
//       //         ),
//       //       ],
//       //     ),
//       //   );
//       //   if (result ?? true) {
//       //   }
//       // }
//     } else if (_avatarImage == null && _user.photoURL.isEmpty) {
//       showInSnackBar('Selecione uma foto para o seu perfil.');
//       setState(() {
//         _isLoading = false;
//       });
//     } else {
//       form.save();

//       if (await isConnected()) {
//         final bool isUserCreated = await _createUser();
//         if (isUserCreated ?? false) {
//           Navigator.of(context)
//               .pushNamedAndRemoveUntil('/cat_sign_up', (_) => false);
//         }
//       } else {
//         showConnectionErrorMessage(context);
//       }

//       setState(() {
//         _isLoading = false;
//       });
//       // var nicknameValidation = await _nicknameValidation(_user.nickname);
//       // var emailValidation = await _emailValidation(_user.email);
//       // var codeValidation = await _codeValidation(_inviteCode);
//       // if (codeValidation == null) {
//       //   showInSnackBar('Este código de convite é invalido.');
//       //   setState(() {
//       //     _isLoading = false;
//       //   });
//       // } else if (emailValidation != null) {
//       //   showInSnackBar('Verifique os erros antes de continuar.');
//       //   setState(() {
//       //     _validateEmailMessage = emailValidation.toString();
//       //     _validateEmail = true;
//       //     _isLoading = false;
//       //   });
//       // } else if (nicknameValidation != null) {
//       //   showInSnackBar('Verifique os erros antes de continuar.');
//       //   setState(() {
//       //     _validateNicknameMessage = nicknameValidation.toString();
//       //     _validateNickname = true;
//       //     _isLoading = false;
//       //   });
//       // } else {
//       //   String originator = codeValidation;
//       // if (_stateName == '') {
//       //   showInSnackBar('Selecione o seu estado.');
//       //   setState(() {
//       //     _isLoading = false;
//       //   });
//       // } else {
//       // try {
//       //   FirebaseUser user = await auth.currentUser();
//       //   if (user == null) {
//       //     user = await userAuth.createUser(User);
//       //     print(user.uid);
//       //     String uid = _isSocialLogin ? _user.uid : user.uid;
//       //     await _setUserPersonalInformation(
//       //         uid, originator, false, _stateName, _city);
//       //   } else {
//       //     print('update user');
//       //     if (_isSocialLogin) {
//       //       user = await userAuth.linkWithEmailAndPassword(User);
//       //     }
//       //     String uid = _isSocialLogin ? _user.uid : user.uid;
//       //     await _setUserPersonalInformation(
//       //         uid, originator, !_isSocialLogin, _stateName, _city);
//       //   }

//       //   setState(() {
//       //     _isLoading = false;
//       //   });

//       //   Navigator.of(context)
//       //       .pushNamedAndRemoveUntil('/cat_sign_up', (route) => false);
//       // } catch (error) {
//       //   String message = error.message;
//       //   print('>> SignUp Error!\n$error');
//       //   if (message.contains('email') && message.contains('already in use')) {
//       //     // showInSnackBar('Esse e-mail já está cadastrado.');
//       //     showErrorMessage('Esse e-mail já está cadastrado.');
//       //   }
//       //   if (message.contains('requires recent authentication')) {
//       //     _clearAllFields();
//       //     await showCustomDialog(
//       //       context: context,
//       //       alert: customRoundAlert(
//       //         context: context,
//       //         title: 'Sessão expirada, tente novamente!',
//       //       ),
//       //     );
//       //   } else {
//       //     showInSnackBar('Algo deu errado, contate o suporte.');
//       //   }
//       //   setState(() {
//       //     _isLoading = false;
//       //   });
//       // }
//       // }
//       // }
//     }
//   }

//   _clearAllFields() async {
//     _user.clear();
//     _inviteCodeController.clear();
//     _passwordController.clear();
//     _confirmPasswordController.clear();
//     setState(() {
//       _avatarImage = null;
//       this._stateInitial = '';
//       this._city = '';
//       _inviteCode = '';
//       _isSocialLogin = false;
//     });
//   }

//   Future<Null> promptCompleteSignup() async {
//     return showDialog<Null>(
//       context: context,
//       barrierDismissible: false, // user must tap button!
//       builder: (BuildContext context) {
//         return AlertDialog(
//           title: Text('Parabéns'),
//           content: SingleChildScrollView(
//             child: ListBody(
//               children: <Widget>[
//                 Text('Seu cadastro no COMOO foi confirmado com sucesso!'),
//                 Text(
//                     'Para começar a comprar ou vender, você precisa cadastrar informações adicionais, como endereço e forma de pagamento. Deseja fazer isso agora?'),
//               ],
//             ),
//           ),
//           actions: <Widget>[
//             FlatButton(
//               child: Text('Não'),
//               onPressed: () {
//                 Navigator.of(context).pushReplacementNamed('/home');
//               },
//             ),
//             FlatButton(
//               child: Text('Sim, completar cadastro'),
//               onPressed: () {
//                 // Navigator.of(context).pop();
//                 Navigator.of(context).pushReplacementNamed('/user_information');
//               },
//             ),
//           ],
//         );
//       },
//     );
//   }

//   void fetchCurrentUser() async {
//     //Get current firebase user
//     await auth.signOut();
//     FirebaseUser user = await auth.currentUser();
//     UserData userData = UserData();

//     if (user != null) {
//       var userSnap = await db.collection('users').document(userData.uid).get();

//       if (userSnap.exists) {
//         if (userSnap.data['status'] == 'completed') {
//           Navigator.of(context).pushReplacementNamed('/home');
//           return;
//         }
//       }

//       //check if user profile is complete
//       userData.displayName = user.displayName;
//       userData.email = user.email;
//       userData.uid = user.uid;
//       userData.photoURL = user.photoUrl;
//     }

//     setState(() {
//       // User = userData;
//       _isLoading = false;
//     });
//   }

//   @override
//   void initState() {
//     fetchStatesList();
//     super.initState();
//     _isLoading = true;
//     _inviteCodeController.text = widget.code ?? '';
//     fetchCurrentUser();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     _user = UserData();
//     _inviteCodeController?.dispose();
//     _stateController?.close();
//     _cityController?.close();
//     bloc?.dispose();
//   }

//   fetchStatesList() async {
//     statesList = <StateItem>[initialState];
//     final List<StateItem> _states =
//         states.map((dynamic state) => StateItem.fromJSON(state)).toList();
//     _states.sort((StateItem a, StateItem b) => a.name.compareTo(b.name));
//     statesList.addAll(_states);
//     print(statesList.length);
//     // list.forEach((state) {
//     //   statesList.add(StateItem.fromJSON(state));
//     // });
//     // statesList.add( StateItem(name: 'Teste', initials: 'TT', cities: ['Teste 01', 'Teste 02']));
//     // statesList = map;
//     // print(list.length);
//   }

//   Widget _buildImagePicker(UserData userData) {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     Widget iconContainer = Container(
//       width: mediaQuery * 151.0,
//       height: mediaQuery * 151.0,
//       alignment: Alignment.center,
//       decoration: BoxDecoration(
//           shape: BoxShape.circle,
//           border: Border.all(color: ComooColors.chumbo)),
//       padding: EdgeInsets.only(right: mediaQuery * 17.0),
//       child: Icon(
//         ComooIcons.camera,
//         color: ComooColors.chumbo,
//         size: mediaQuery * 41.0,
//       ),
//     );

//     if (userData != null && _avatarImage == null) {
//       if (userData.photoURL.isNotEmpty) {
//         return Container(
//           width: mediaQuery * 151.0,
//           height: mediaQuery * 151.0,
//           alignment: Alignment.center,
//           decoration: BoxDecoration(
//               shape: BoxShape.circle,
//               image: DecorationImage(
//                   fit: BoxFit.contain, image: NetworkImage(userData.photoURL)),
//               border: Border.all(color: ComooColors.chumbo)),
//           padding: EdgeInsets.only(right: mediaQuery * 17.0),
//         );
//       } else {
//         return iconContainer;
//       }
//     } else {
//       if (_avatarImage == null) {
//         return iconContainer;
//       } else {
//         return Container(
//           width: mediaQuery * 151.0,
//           height: mediaQuery * 151.0,
//           decoration: BoxDecoration(
//               shape: BoxShape.circle,
//               image: DecorationImage(
//                   fit: BoxFit.cover, image: FileImage(_avatarImage)),
//               border: Border.all(color: ComooColors.chumbo)),
//         );
//       }
//     }
//   }

//   List getCities() {
//     List cities = [];
//     if (_stateInitial == '') {
//       cities.add(DropdownMenuItem(
//           child: Text(
//             'Alagoas',
//             style: ComooStyles.inputText,
//           ),
//           value: 'Alagoas'));
//     } else {
//       citiesList.forEach((city) {
//         cities.add(DropdownMenuItem<String>(
//             child: Text(
//               city,
//               style: ComooStyles.inputText,
//             ),
//             value: city));
//       });
//     }
//     return cities;
//   }

//   // selectCities(String initial) {
//   //   StateItem state = statesList.firstWhere((s) {
//   //     return s.initials == initial;
//   //   });
//   //   _stateName = state.name;
//   //   citiesList = state.cities;
//   // }

//   Future<Null> loginWithFacebook() async {
//     if (mounted) {
//       setState(() {
//         _isLoading = true;
//       });
//     }
//     facebookLogin = FacebookLogin();
//     bool isLoggedIn = await facebookLogin.isLoggedIn;
//     if (isLoggedIn) await facebookLogin.logOut();
//     facebookResult = await facebookLogin.logInWithReadPermissions(
//         ['email', 'public_profile']).catchError((onError) {
//       print('Facebook Erro sinistro!');
//     });
//     // print(facebookResult?.status);
//     // print(facebookResult?.errorMessage);
//     // print(facebookResult?.accessToken?.userId);
//     // print(facebookResult?.accessToken?.token);
//     switch (facebookResult.status) {
//       case FacebookLoginStatus.loggedIn:
//         auth = FirebaseAuth.instance;
//         print('getting login information.');
//         print(facebookResult);
//         try {
//           final AuthCredential credential = FacebookAuthProvider.getCredential(
//               accessToken: facebookResult.accessToken.token);
//           final FirebaseUser signedInUser = await auth
//               .signInWithCredential(credential)
//               .timeout(Duration(seconds: 60));
//           UserInfo userInfo;
//           for (UserInfo info in signedInUser.providerData) {
//             if (info.providerId == 'facebook.com') {
//               userInfo = info;
//               break;
//             }
//           }
//           print(userInfo);
//           print(userInfo.displayName);
//           print(userInfo.email);
//           print(userInfo.providerId);
//           print(userInfo.uid);
//           print(userInfo.photoUrl);
//           print(userInfo.phoneNumber);
//           print('Signed in as $userInfo');
//           // String fbId = signedInUser.providerData.last.providerId == 'firebase'
//           //     ? signedInUser.providerData.first.uid
//           //     : signedInUser.providerData.last.uid;
//           print('ID ${userInfo.uid}');
//           setState(() {
//             // _avatarImage =  File(userInfo.photoUrl);
//             _user.displayName = userInfo.displayName;
//             _user.email = userInfo.email;
//             _user.uid = signedInUser.uid;
//             _user.photoURL = userInfo.photoUrl;
//             _avatarImage = null;
//             _isSocialLogin = true;
//             _socialId = userInfo.uid;
//           });
//         } catch (e) {
//           showInSnackBar(
//               'Ocorreu um erro ao tentar fazer o login via Facebook. Tente novamente mais tarde.');
//         }
//         break;
//       case FacebookLoginStatus.cancelledByUser:
//         print('Cancelled');
//         break;
//       case FacebookLoginStatus.error:
//         print('Error');
//         print(facebookResult.errorMessage);
//         showInSnackBar(
//             'Ocorreu um erro ao tentar fazer o login via Facebook. Tente novamente mais tarde.');
//         break;
//     }
//     setState(() {
//       _isLoading = false;
//     });
//   }

//   Future loginWithGoogle() async {
//     if (mounted) {
//       setState(() {
//         _isLoading = true;
//       });
//     }
//     googleSignIn = GoogleSignIn();
//     // GoogleSignInAccount gUser = await googleSignIn.signIn().catchError((error) {
//     //   print(error);
//     //   if (mounted) {
//     //     setState(() {
//     //       _isLoading = false;
//     //     });
//     //   }
//     // });
//     GoogleSignInAccount gUser; // = googleSignIn.currentUser;
//     bool isSignIn = await googleSignIn.isSignedIn();
//     if (isSignIn) await googleSignIn.signOut();
//     // if (gUser == null) gUser = await googleSignIn.signInSilently();
//     try {
//       if (gUser == null) gUser = await googleSignIn.signIn();
//     } catch (e) {
//       showInSnackBar(
//           'Ocorreu um erro ao tentar fazer o login via Google. Tente novamente mais tarde.');
//     }
//     if (gUser == null) {
//       showInSnackBar('Você cancelou o acesso via Google');
//       if (mounted) {
//         setState(() {
//           _isLoading = false;
//         });
//       }
//       return null;
//     }
//     googleCredentials = await gUser.authentication.catchError((error) {
//       if (mounted) {
//         setState(() {
//           _isLoading = false;
//         });
//       }
//     });
//     final AuthCredential credential = GoogleAuthProvider.getCredential(
//         idToken: googleCredentials.idToken,
//         accessToken: googleCredentials.accessToken);
//     final FirebaseUser user =
//         await auth.signInWithCredential(credential).catchError((error) {
//       if (mounted) {
//         setState(() {
//           _isLoading = false;
//         });
//       }
//     });

//     UserInfo userInfo;
//     for (UserInfo info in user.providerData) {
//       if (info.providerId == 'google.com') {
//         userInfo = info;
//         break;
//       }
//     }
//     // String goglId = user.providerData.last.providerId == 'firebase'
//     //     ? user.providerData.first.uid
//     //     : user.providerData.last.uid;
//     setState(() {
//       _isLoading = false;
//       // _avatarImage =  File(user.photoUrl);
//       _user.displayName = userInfo.displayName;
//       _user.email = userInfo.email;
//       _user.uid = user.uid;
//       _user.photoURL = userInfo.photoUrl;
//       _avatarImage = null;
//       _isSocialLogin = true;
//       _socialId = userInfo.uid;
//     });
//   }

//   Widget _buildGoogleLoginButton() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Container(
//       padding: EdgeInsets.only(top: mediaQuery * 20.0),
//       child: RawMaterialButton(
//         shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(mediaQuery * 20)),
//         onPressed: () {
//           loginWithGoogle();
//         },
//         fillColor: ComooColors.turqueza,
//         constraints: BoxConstraints.expand(
//             width: mediaQuery * 275.0, height: mediaQuery * 43.0),
//         child: Text(
//           'CADASTRAR COM GOOGLE',
//           style: ComooStyles.botao_branco.copyWith(fontSize: mediaQuery * 15.0),
//         ),
//       ),
//     );
//   }

//   Widget _buildFacebookLoginButton() {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Container(
//       padding: EdgeInsets.only(top: mediaQuery * 46.0),
//       child: RawMaterialButton(
//         shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(mediaQuery * 20)),
//         onPressed: () {
//           loginWithFacebook();
//         },
//         fillColor: ComooColors.turqueza,
//         constraints: BoxConstraints.expand(
//             width: mediaQuery * 275.0, height: mediaQuery * 43.0),
//         child: Text(
//           'CADASTRAR COM FACEBOOK',
//           style: ComooStyles.botao_branco.copyWith(fontSize: mediaQuery * 15.0),
//         ),
//       ),
//     );
//   }

//   Widget _buildForm(BuildContext context, {UserData userData}) {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Form(
//       key: _formKey,
//       autovalidate: _autoValidate,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: <Widget>[
//           Container(
//             child: GestureDetector(
//               child: _buildImagePicker(_user),
//               onTap: selectOrigin,
//             ),
//             alignment: FractionalOffset.center,
//           ),
//           Container(
//             child: GestureDetector(
//               child: Text(
//                 'EDITAR',
//                 style: ComooStyles.texto,
//                 textAlign: TextAlign.center,
//               ),
//               onTap: selectOrigin,
//             ),
//             alignment: FractionalOffset.center,
//           ),
//           _isSocialLogin
//               ? Container()
//               : Center(child: _buildFacebookLoginButton()),
//           _isSocialLogin
//               ? Container()
//               : Center(child: _buildGoogleLoginButton()),
//           Padding(
//             padding: EdgeInsets.only(top: mediaQuery * 46.0),
//             child: Text(
//               'DADOS DE ACESSO',
//               style: ComooStyles.appBarTitle,
//               textAlign: TextAlign.left,
//             ),
//           ),
//           Divider(),
//           Stack(
//             children: <Widget>[
//               Container(
//                 margin: EdgeInsets.only(bottom: mediaQuery * 20.0),
//                 padding: EdgeInsets.all(0.0),
//                 child: TextFormField(
//                   decoration: ComooStyles.inputCustomDecoration(
//                           'código de convite')
//                       .copyWith(
//                           // counterText: '',
//                           // counterText: 'Ainda não tem código de convite? Clique aqui',
//                           ),
//                   style: ComooStyles.inputText,
//                   validator: _validations.validateInviteCode,
//                   // initialValue: _inviteCode,
//                   controller: _inviteCodeController,
//                   keyboardType: TextInputType.text,
//                   textCapitalization: TextCapitalization.words,
//                   onSaved: (String name) {
//                     _inviteCode = name.trim();
//                     // _user.displayName = name;
//                   },
//                 ),
//               ),
//               Positioned(
//                 bottom: 0.0,
//                 left: 0.0,
//                 child: Container(
//                   child: GestureDetector(
//                     onTap: () {
//                       setState(() {
//                         _inviteCodeController.text = 'VEMPRACOMOO';
//                       });
//                     },
//                     child: RichText(
//                       text: TextSpan(
//                         text: 'Ainda não tem código de convite? ',
//                         style: TextStyle(
//                           fontFamily: ComooStyles.fontFamily,
//                           color: ComooColors.chumbo,
//                           fontSize: mediaQuery * 13.0,
//                           fontWeight: FontWeight.normal,
//                         ),
//                         children: [
//                           TextSpan(
//                             text: 'Clique aqui',
//                             style: TextStyle(
//                               fontFamily: ComooStyles.fontFamily,
//                               color: ComooColors.chumbo,
//                               fontSize: mediaQuery * 13.0,
//                               fontWeight: FontWeight.bold,
//                             ),
//                           )
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//           Container(
//             child: TextFormField(
//               decoration: ComooStyles.inputCustomDecoration('NOME COMPLETO'),
//               style: ComooStyles.inputText,
//               validator: _validations.validateEmpty,
//               initialValue: _user.displayName,
//               keyboardType: TextInputType.text,
//               textCapitalization: TextCapitalization.words,
//               onSaved: (String name) {
//                 _user.displayName = name.trim();
//               },
//             ),
//           ),
//           Container(
//             child: TextFormField(
//               // decoration: ComooStyles.inputDecoration('NOME DE USUÁRIO'),
//               decoration: InputDecoration(
//                 labelText: 'NOME DE USUÁRIO',
//                 isDense: true,
//                 helperStyle: ComooStyles.texto,
//                 focusedBorder: ComooStyles.underLineBorder,
//                 labelStyle: ComooStyles.inputLabel,
//                 border: ComooStyles.underLineBorder,
//                 // errorText: _validateNickname ? _validateNicknameMessage : null,
//               ),
//               style: ComooStyles.inputText,
//               validator: _validations.validateNickname,
//               initialValue: _user.nickname,
//               autocorrect: false,
//               textCapitalization: TextCapitalization.none,
//               keyboardType: TextInputType.text,
//               onSaved: (String nickname) {
//                 _user.nickname = nickname.trim().toLowerCase();
//               },
//             ),
//           ),
//           Container(
//             child: TextFormField(
//               initialValue: _user.email,
//               keyboardType: TextInputType.emailAddress,
//               validator: _validations.validateEmail,
//               onSaved: (String email) {
//                 _user.email = email.trim().toLowerCase();
//               },
//               style: ComooStyles.inputText,
//               autocorrect: false,
//               // decoration: ComooStyles.inputDecoration('E-MAIL'),
//               decoration: InputDecoration(
//                 labelText: 'E-MAIL',
//                 isDense: true,
//                 helperStyle: ComooStyles.texto,
//                 focusedBorder: ComooStyles.underLineBorder,
//                 labelStyle: ComooStyles.inputLabel,
//                 border: ComooStyles.underLineBorder,
//                 // errorText: _validateEmail ? _validateEmailMessage : null,
//                 // errorMaxLines: 3,
//               ),
//             ),
//           ),
//           Container(
//             child: TextFormField(
//               keyboardType: TextInputType.text,
//               style: ComooStyles.inputText,
//               decoration: ComooStyles.inputCustomDecoration(
//                 'SENHA',
//               ).copyWith(
//                 suffixIcon: IconButton(
//                   icon: Icon(
//                       showPassword ? Icons.visibility_off : Icons.visibility),
//                   onPressed: () {
//                     setState(() {
//                       showPassword = !showPassword;
//                     });
//                   },
//                 ),
//               ),
//               obscureText: showPassword,
//               controller: _passwordController,
//               validator: _validations.validatePassword,
//               onSaved: (String password) {
//                 _user.password = password;
//               },
//             ),
//           ),
//           Container(
//             child: TextFormField(
//               keyboardType: TextInputType.text,
//               style: ComooStyles.inputText,
//               decoration: ComooStyles.inputCustomDecoration(
//                 'CONFIRMAR SENHA',
//               ).copyWith(
//                 hintText: 'Confirmar senha',
//                 suffixIcon: IconButton(
//                     icon: Icon(showPassword2
//                         ? Icons.visibility_off
//                         : Icons.visibility),
//                     onPressed: () {
//                       setState(() {
//                         showPassword2 = !showPassword2;
//                       });
//                     }),
//               ),
//               obscureText: showPassword2,
//               controller: _confirmPasswordController,
//               validator: (String value) {
//                 if (_passwordController.text !=
//                     _confirmPasswordController.text) {
//                   return 'Essa senha não coincide com a digitada acima';
//                 }
//               },
//               onSaved: (String confirmPassword) {
//                 _user.confirmPassword = confirmPassword;
//               },
//             ),
//           ),

//           StreamBuilder<StateItem>(
//             initialData: initialState,
//             stream: stateStream,
//             builder:
//                 (BuildContext context, AsyncSnapshot<StateItem> stateSnapshot) {
//               if (stateSnapshot.hasData) {
//                 if (stateSnapshot.data.initials == 'selecione') {
//                   stateController.text = stateSnapshot.data.initials;
//                 } else {
//                   stateController.text =
//                       '${stateSnapshot.data.initials} - ${stateSnapshot.data.name}';
//                 }
//               }
//               return Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: <Widget>[
//                   Container(
//                     child: TextField(
//                       onTap: () async {
//                         stateFocus.unfocus();
//                         stateFocus.consumeKeyboardToken();
//                         await customCupertinoModal(
//                           context: context,
//                           onChange: (int index) {
//                             changeState(statesList[index]);
//                             changeCity('selecione');

//                             _stateName = statesList[index].name;
//                           },
//                           children: List<Widget>.generate(statesList.length,
//                               (int index) {
//                             return Center(
//                               child: Text(statesList[index].name),
//                             );
//                           }),
//                         );
//                       },
//                       controller: stateController,
//                       focusNode: stateFocus,
//                       autofocus: false,
//                       enableInteractiveSelection: false,
//                       textCapitalization: TextCapitalization.none,
//                       style: ComooStyles.inputText,
//                       decoration: InputDecoration(
//                         labelText: 'ESTADO',
//                         isDense: true,
//                         disabledBorder: ComooStyles.underLineBorder,
//                         helperStyle: ComooStyles.texto,
//                         focusedBorder: ComooStyles.underLineBorder,
//                         labelStyle: ComooStyles.inputLabel
//                             .copyWith(color: COMOO.colors.purple),
//                         border: ComooStyles.underLineBorder,
//                         errorText: stateSnapshot.error,
//                       ),
//                     ),
//                   ),
//                   Container(
//                     child: StreamBuilder<String>(
//                       stream: cityStream,
//                       initialData: 'selecione',
//                       builder: (BuildContext context,
//                           AsyncSnapshot<String> citySnapshot) {
//                         if (citySnapshot.hasData) {
//                           cityController.text = citySnapshot.data;
//                         }
//                         return Container(
//                           child: TextField(
//                             onTap: () async {
//                               stateFocus.unfocus();
//                               stateFocus.consumeKeyboardToken();
//                               await customCupertinoModal(
//                                 context: context,
//                                 onChange: (int index) {
//                                   _city = stateSnapshot.data?.cities[index];
//                                   changeCity(stateSnapshot.data?.cities[index]);
//                                 },
//                                 children: List<Widget>.generate(
//                                     stateSnapshot.data?.cities?.length ?? 0,
//                                     (int index) {
//                                   return Center(
//                                     child:
//                                         Text(stateSnapshot.data?.cities[index]),
//                                   );
//                                 }),
//                               );
//                             },
//                             controller: cityController,
//                             focusNode: stateFocus,
//                             autofocus: false,
//                             enabled: stateSnapshot.data.cities.isNotEmpty,
//                             enableInteractiveSelection: false,
//                             textCapitalization: TextCapitalization.none,
//                             style: ComooStyles.inputText,
//                             decoration: InputDecoration(
//                               labelText: 'CIDADE',
//                               isDense: true,
//                               disabledBorder: ComooStyles.underLineBorder,
//                               helperStyle: ComooStyles.texto,
//                               focusedBorder: ComooStyles.underLineBorder,
//                               labelStyle: ComooStyles.inputLabel
//                                   .copyWith(color: COMOO.colors.purple),
//                               border: ComooStyles.underLineBorder,
//                               errorText: citySnapshot.error,
//                             ),
//                           ),
//                         );
//                       },
//                     ),
//                   ),
//                 ],
//               );
//             },
//           ),
//           // Container(
//           //   child: TextField(
//           //     onTap: () async {
//           //       stateFocus.unfocus();
//           //       stateFocus.consumeKeyboardToken();
//           //       await customCupertinoModal(
//           //         context: context,
//           //         onChange: (int index) {
//           //           final StateItem state = statesList[index];
//           //           setState(() {
//           //             stateController.text = state.name;
//           //             _stateName = state.name;
//           //             citiesList = state.cities;
//           //             this._stateInitial = state.initials;
//           //             this._city = '';
//           //           });
//           //         },
//           //         children:
//           //             List<Widget>.generate(statesList.length, (int index) {
//           //           return Center(
//           //             child: Text(statesList[index].name),
//           //           );
//           //         }),
//           //       );
//           //     },
//           //     controller: stateController,
//           //     focusNode: stateFocus,
//           //     autofocus: false,
//           //     enableInteractiveSelection: false,
//           //     textCapitalization: TextCapitalization.none,
//           //     style: ComooStyles.inputText,
//           //     decoration: InputDecoration(
//           //       labelText: 'ESTADO',
//           //       isDense: true,
//           //       disabledBorder: ComooStyles.underLineBorder,
//           //       helperStyle: ComooStyles.texto,
//           //       focusedBorder: ComooStyles.underLineBorder,
//           //       labelStyle:
//           //           ComooStyles.inputLabel.copyWith(color: COMOO.colors.purple),
//           //       border: ComooStyles.underLineBorder,
//           //     ),
//           //   ),
//           // ),
//           // Container(
//           //   margin: EdgeInsets.only(top: mediaQuery * 10.0),
//           //   child: Column(
//           //     mainAxisSize: MainAxisSize.min,
//           //     crossAxisAlignment: CrossAxisAlignment.start,
//           //     children: <Widget>[
//           //       Text(
//           //         'ESTADO',
//           //         style: ComooStyles.inputLabel,
//           //       ),
//           //       Container(
//           //         width: mediaQuery * 360.0,
//           //         height: mediaQuery * 50.0,
//           //         decoration: BoxDecoration(
//           //             border: Border(
//           //                 bottom: BorderSide(
//           //                     color: Colors.black, width: mediaQuery * 1.0))),
//           //         child: DropdownButtonHideUnderline(
//           //           child: Stack(
//           //             children: <Widget>[
//           //               DropdownButton(
//           //                   isExpanded: true,
//           //                   hint: Container(
//           //                     margin: EdgeInsets.only(top: mediaQuery * 10.0),
//           //                     child: Text(
//           //                       _stateInitial,
//           //                       style: ComooStyles.dropdownText,
//           //                     ),
//           //                   ),
//           //                   // value: _state,
//           //                   onChanged: (initial) {
//           //                     selectCities(initial);
//           //                     setState(() {
//           //                       this._stateInitial = initial;
//           //                       this._city = '';
//           //                     });
//           //                   },
//           //                   items: statesList.map((state) {
//           //                     return DropdownMenuItem<String>(
//           //                         child: Text(
//           //                           state.name,
//           //                           style: ComooStyles.inputText,
//           //                         ),
//           //                         value: state.initials);
//           //                   }).toList()),
//           //               Positioned(
//           //                 top: mediaQuery * 15.0,
//           //                 right: mediaQuery * 30.0,
//           //                 child: Text('selecione'),
//           //               )
//           //             ],
//           //           ),
//           //         ),
//           //       ),
//           //     ],
//           //   ),
//           // ),
//           // SizedBox(height: mediaQuery * 10.0),
//           // Column(
//           //   mainAxisSize: MainAxisSize.min,
//           //   crossAxisAlignment: CrossAxisAlignment.start,
//           //   children: <Widget>[
//           //     Text(
//           //       'CIDADE',
//           //       style: ComooStyles.inputLabel,
//           //     ),
//           //     Container(
//           //       width: mediaQuery * 360.0,
//           //       height: mediaQuery * 50.0,
//           //       decoration: BoxDecoration(
//           //         border: Border(
//           //             bottom: BorderSide(color: Colors.black, width: 1.0)),
//           //       ),
//           //       child: DropdownButtonHideUnderline(
//           //         child: Stack(
//           //           children: <Widget>[
//           //             DropdownButton(
//           //                 isExpanded: true,
//           //                 hint: Container(
//           //                   margin: EdgeInsets.only(top: mediaQuery * 10.0),
//           //                   child: Text(
//           //                     _city,
//           //                     style: ComooStyles.dropdownText,
//           //                   ),
//           //                 ),
//           //                 // value: _state,
//           //                 onChanged: (name) {
//           //                   setState(() {
//           //                     this._city = name;
//           //                   });
//           //                 },
//           //                 items: citiesList.map((city) {
//           //                   return DropdownMenuItem<String>(
//           //                       child: Text(
//           //                         city,
//           //                         style: ComooStyles.inputText,
//           //                       ),
//           //                       value: city);
//           //                 }).toList()),
//           //             Positioned(
//           //               top: mediaQuery * 15.0,
//           //               right: mediaQuery * 30.0,
//           //               child: Text('selecione'),
//           //             )
//           //           ],
//           //         ),
//           //       ),
//           //     ),
//           //   ],
//           // ),
//           ListTile(
//             leading: _userAgreement
//                 ? IconButton(
//                     icon: Image.asset(
//                       'images/check_filled.png',
//                       scale: mediaQuery * 27.0,
//                     ),
//                     onPressed: () {
//                       setState(() {
//                         _userAgreement = false;
//                       });
//                     },
//                   )
//                 : IconButton(
//                     icon: Image.asset(
//                       'images/unchecked.png',
//                       scale: mediaQuery * 27.0,
//                     ),
//                     onPressed: () {
//                       setState(() {
//                         _userAgreement = true;
//                       });
//                     },
//                   ),
//             title: Column(
//               children: <Widget>[
//                 GestureDetector(
//                   child: RichText(
//                     text: TextSpan(
//                       children: <TextSpan>[
//                         TextSpan(
//                             text: 'Concordo com os ', style: ComooStyles.texto),
//                         TextSpan(
//                           text:
//                               'Termos e condições de uso e Política de Privacidade',
//                           style: ComooStyles.texto_bold,
//                         ),
//                       ],
//                     ),
//                   ),
//                   onTap: _handleTos,
//                 )
//               ],
//             ),
//           ),
//           //  Row(
//           //   mainAxisAlignment: MainAxisAlignment.start,
//           //   mainAxisSize: MainAxisSize.min,
//           //   children: <Widget>[
//           //     Checkbox(
//           //       value: _userAgreement,
//           //       onChanged: (bool value) {
//           //         setState(() {
//           //           _userAgreement = value;
//           //         });
//           //       },
//           //     ),
//           //     Text('Concordo com os'),
//           //     FlatButton(
//           //       padding: EdgeInsets.all(mediaQuery * 2.0),
//           //       child: Column(
//           //         children: <Widget>[
//           //           Text(
//           //             'Termos e condições de uso e Política de Privacidade',
//           //             style: ComooStyles.texto_bold,
//           //           ),
//           //         ],
//           //       ),
//           //       onPressed: _handleTos,
//           //     ),
//           //   ],
//           // ),

//           Container(
//             alignment: FractionalOffset.center,
//             child: OutlineButton(
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(30.0)),
//               borderSide: BorderSide(color: Color.fromRGBO(70, 30, 90, 1.0)),
//               highlightElevation: 10.0,
// //                  padding: EdgeInsets.symmetric(
// //                      horizontal: 50.0, vertical: 15.0),
//               child: const Text(
//                 'CONTINUAR',
//                 style: ComooStyles.botoes,
//               ),
//               onPressed: !_userAgreement
//                   ? null
//                   : () {
//                       if (!_userAgreement) {
//                         showInSnackBar(
//                             'Você deve aceitar os termos de usuário para continuar');
//                         return null;
//                       } else {
//                         _handleSubmitted();
//                       }
//                     },
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   _handleTos() async {
//     final bool result =
//         await Navigator.of(context).push(MaterialPageRoute<bool>(
//       builder: (BuildContext context) => TermsOfServicePage(),
//     ));

//     setState(() {
//       _userAgreement = result ?? false;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     final mediaQuery = MediaQuery.of(context).size.width / 400;
//     return Scaffold(
//       key: _scaffoldKey,
//       appBar: CustomAppBar.round(
//         color: ComooColors.turqueza,
//         title: Text(
//           'CADASTRO',
//           style: ComooStyles.appBarTitleWhite,
//         ),
//       ),
//       body: GestureDetector(
//         onTap: () {
//           FocusScope.of(context).requestFocus(FocusNode());
//           SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
//         },
//         child: _isLoading
//             ? LoadingContainer(
//                 message: _loadMessage,
//               )
//             : SingleChildScrollView(
//                 padding: EdgeInsets.all(mediaQuery * 20.0),
//                 child: _buildForm(context),
//               ),
//       ),
//     );
//   }
// }
