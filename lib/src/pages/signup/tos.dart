import 'package:comoo/src/widgets/loading_container.dart';
import 'package:flutter/material.dart';
import 'package:comoo/src/utility/style.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class TermsOfServicePage extends StatefulWidget {
  @override
  State createState() {
    return _TermsOfServicePageState();
  }
}

class _TermsOfServicePageState extends State<TermsOfServicePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String tosText = 'texts/tos.txt';

  _handleAccept() {
    Navigator.of(context).pop(true);
  }

  Future<String> _fetchText() {
    return rootBundle
        .loadString(tosText)
        .timeout(Duration(seconds: 30))
        .catchError((onError) {
      return rootBundle
          .loadString(tosText)
          .catchError((onError) => '')
          .timeout(Duration(seconds: 60));
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width / 400;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        iconTheme: IconThemeData(color: ComooColors.roxo),
        centerTitle: true,
        textTheme: TextTheme(
            title: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                color: ComooColors.roxo)),
        title: Text(
          'TERMOS E CONDIÇÕES DE USO\nE POLITICA DE PRIVACIDADE',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.bold,
            fontSize: mediaQuery * 14.0,
            color: ComooColors.cinzaMedio,
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        padding: EdgeInsets.all(mediaQuery * 18.0),
        children: <Widget>[
          Container(
            width: mediaQuery * 124.0,
            padding: EdgeInsets.only(bottom: mediaQuery * 28.0),
            child: Image(
              image: AssetImage('images/logo_fb.png'),
              width: mediaQuery * 224.0,
              height: mediaQuery * 44.0,
            ),
          ),
          Container(
            height:
                (MediaQuery.of(context).size.height / 2) + (mediaQuery * 95.0),
            child: FutureBuilder<String>(
              future: _fetchText(),
              builder: (_, snapshot) {
                print('${snapshot.connectionState} ${snapshot.hasData}');
                switch (snapshot.connectionState) {
                  case ConnectionState.active:
                  case ConnectionState.done:
                    String text = snapshot.data.trim();
                    return ListView(
                      itemExtent: (0.533 * text.length),
                      children: [
                        Text(
                          text,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w500,
                            fontSize: mediaQuery * 14.0,
                            letterSpacing: mediaQuery * 0.2,
                            color: ComooColors.chumbo,
                          ),
                        ),
                      ],
                    );
                    break;
                  case ConnectionState.none:
                    return Center(
                      heightFactor: 5.0,
                      child: Container(
                        child: GestureDetector(
                          onTap: () {
                            launch('https://comoo.io');
                          },
                          child: RichText(
                            text: TextSpan(
                              text:
                                  'Falha ao carregar os termos e condições, tente pelo ',
                              style: ComooStyles.titulos,
                              children: [
                                TextSpan(
                                    text: 'nosso site.',
                                    style: ComooStyles.titulos
                                        .copyWith(color: ComooColors.turqueza)),
                              ],
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    );
                    break;
                  case ConnectionState.waiting:
                    return LoadingContainer();
                    break;
                  default:
                    return Container();
                    break;
                }
              },
            ),
          ),
          RawMaterialButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(17.5)),
            onPressed: _handleAccept,
            fillColor: ComooColors.turqueza,
            constraints: BoxConstraints.expand(width: 252.0, height: 35.0),
            child: Text(
              'LI E CONCORDO',
              style: ComooStyles.botao_branco,
            ),
          ),
        ],
      ),
    );
  }
}
