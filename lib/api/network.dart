import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/authentication.dart';
import 'package:comoo/src/comoo/comments.dart';
import 'package:comoo/src/comoo/comoo.dart';
import 'package:comoo/src/comoo/feed.dart';
import 'package:comoo/src/comoo/group.dart';
import 'package:comoo/src/comoo/negotiation.dart';
import 'package:comoo/src/comoo/product.dart';
import 'package:comoo/src/comoo/user.dart';
import 'package:comoo/src/widgets/reusables.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkApi with Reusables {
  NetworkApi();

  final UserAuth auth = UserAuth();
  final Firestore db = Firestore.instance;
  final CloudFunctions _cloud = CloudFunctions.instance;
  final bool showDebug = false;

  void _exceptionDialog(BuildContext context, String message) {
    message =
        message == 'INTERNAL' ? 'Algo deu errado, contate o suporte!' : message;
    showCustomDialog<void>(
      context: context,
      alert: customRoundAlert(
        context: context,
        title: message,
      ),
    );
  }

  @protected
  Future call<T>(
      BuildContext context, String name, Map<String, dynamic> param) async {
    final DateTime before = DateTime.now();
    try {
      final HttpsCallableResult result = await _cloud
          .getHttpsCallable(functionName: name)
          .call(param)
          .timeout(const Duration(seconds: 60));
      final DateTime after = DateTime.now();
      // print('$name returned: ${result.data}');
      if (showDebug) {
        print('Function $name took: '
            '${after.difference(before).inMilliseconds} milliseconds');
      }
      return result.data;
    } on TimeoutException catch (_) {
      print('>> Function $name Timedout!');
    } on CloudFunctionsException catch (e) {
      print('>> $name error => $e');
      print(e.details);
      print(e.message);
      if (context != null) {
        _exceptionDialog(context, e.message);
      }
    } catch (e) {
      print('unexpedted error in $name function');
    }
  }

  Future<User> fetchCurrentUser() async {
    return comoo.currentUser();
  }

  Future<FirebaseUser> fetchFireUser() async {
    final FirebaseAuth fireAuth = FirebaseAuth.instance;
    FirebaseUser fireUser = await fireAuth.currentUser();
    fireUser = fireUser ??= await fireAuth.currentUser();
    return fireUser;
  }

  Future<bool> creditCardPayment<T>(
      BuildContext context, Map<String, T> param) async {
    print(param);
    final dynamic result = await call<T>(context, 'payWithCreditCard', param);
    if (result != null) {
      return true;
    }
    return false;
  }

  Future<dynamic> createCreditCard(
      BuildContext context, Map<String, String> param) async {
    final dynamic result =
        await call<String>(context, 'createCreditCard', param);
    if (result != null) {
      return result;
    }
    return null;
  }

  Future<void> deleteNotification(BuildContext context, String uid) async {
    await call<String>(
      context,
      'deleteNotification',
      <String, String>{'notificationId': uid},
    );
  }

  Future<bool> updateBankAccount(
      BuildContext context, Map<String, String> param) async {
    final dynamic result =
        await call<String>(context, 'updateBankAccount', param);
    if (result != null) {
      return true;
    }
    return false;
  }

  Future<bool> updatePersonalData(
      BuildContext context, Map<String, dynamic> param) async {
    final dynamic result = await call(context, 'updatePersonalData', param);
    if (result != null) {
      return true;
    }
    return false;
  }

  Future<Product> getProduct(String id) async {
    final DocumentSnapshot snap =
        await db.collection('products').document(id).get();
    if (snap.exists) {
      return Product.fromSnapshot(snap);
    }
    return null;
  }

  Future<List> getFutureBankStatement() async {
    final dynamic result = await call<dynamic>(
        null, 'getFutureBankStatement', <String, dynamic>{});
    if (result != null) {
      return result;
    }
    return [];
  }

  Future<List> getAllBankStatement() async {
    final dynamic result =
        await call<dynamic>(null, 'getAllBankStatement', <String, dynamic>{});
    if (result != null) {
      return result;
    }
    return [];
  }

  Future<List> getCurrentBankStatement() async {
    final dynamic result = await call<dynamic>(
        null, 'getCurrentBankStatement', <String, dynamic>{});
    if (result != null) {
      return result;
    }
    return [];
  }

  Future<dynamic> getAllBalance() async {
    final dynamic result =
        await call<dynamic>(null, 'getBalance', <String, dynamic>{});
    if (result != null) {
      return result;
    }
    return <String, dynamic>{
      'future': <String, dynamic>{'amount': 0.0, 'currency': 'BRL'},
      'current': <String, dynamic>{'amount': 0.0, 'currency': 'BRL'},
    };
  }

  Future<bool> createTransfer(
      BuildContext context, Map<String, int> param) async {
    final dynamic result = await call<int>(null, 'createTransfer', param);
    if (result != null) {
      return true;
    }
    return false;
  }

  Future<List<Map<String, dynamic>>> fetchUserRecommendation() async {
    return call<dynamic>(null, 'fetchUserRecommendation', <String, dynamic>{});
  }

  Future<bool> validateVersion() async {
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final dynamic result =
        await call<bool>(null, 'validateVersion', <String, String>{
      'version': '${packageInfo?.version ?? ''}',
    });
    if (result != null) {
      return result as bool;
    }
    return false;
  }

  Future<bool> validateToS() async {
    final User user = await comoo.currentUser();
    if (user == null) {
      return true;
    }
    final dynamic result =
        await call<String>(null, 'isAlreadyReadTerm', <String, String>{});
    print('Validate TOS: $result');
    if (result != null) {
      return result as bool;
    }
    return true;
  }

  Future<void> agreedWithToS() async {
    await call<String>(null, 'readTerm', <String, String>{});
  }

  Future<dynamic> getPostGeneral() async {
    // return call<dynamic>(null, 'getPostGeneral', <String, dynamic>{});
    List<Map<String, String>> list = <Map<String, String>>[
      <String, String>{
        'id': '01',
        'imageURL':
            'https://firebasestorage.googleapis.com/v0/b/app-comoo.appspot.com/o/banners%2Fcriar_comoonidade.png?alt=media&token=77ce146c-43ab-401b-9ef7-c3ad8a36c791',
      },
      <String, String>{
        'id': '02',
        'imageURL':
            'https://firebasestorage.googleapis.com/v0/b/app-comoo.appspot.com/o/banners%2Fentrar_comoonidades.png?alt=media&token=1a4166c8-f6ec-457f-946c-100f4c9e6468',
      },
      <String, String>{
        'id': '03',
        'imageURL':
            'https://firebasestorage.googleapis.com/v0/b/app-comoo.appspot.com/o/banners%2Flucro%20compartilhado.png?alt=media&token=71cac1c5-3839-45c6-b23b-dd81beeab415',
      },
      <String, String>{
        'id': '04',
        'imageURL':
            'https://firebasestorage.googleapis.com/v0/b/app-comoo.appspot.com/o/banners%2Fnotificacoes.png?alt=media&token=bcc960d9-1c37-401b-b522-f9b7decbfe73',
      },
      <String, String>{
        'id': '05',
        'imageURL':
            'https://firebasestorage.googleapis.com/v0/b/app-comoo.appspot.com/o/banners%2Fvenda.png?alt=media&token=dc58364a-747e-4994-9a95-507a7900b80e',
      },
      <String, String>{
        'id': '06',
        'text':
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an.",
      },
    ];
    return Future.value(list);
  }

  Future<dynamic> closePost(String id) async {
    print(id);
    return call<String>(null, 'userClosedPost', <String, String>{
      'id': id,
    });
  }

  Future<bool> inNegotiationAsClient({int attempts = 0}) async {
    final bool result =
        await call<dynamic>(null, 'inNegotiationAsClient', <String, String>{});
    // print('result of inNegotiationAsClient: $result');
    if (result == null) {
      if (attempts == 5) {
        return false;
      }
      return inNegotiationAsClient(attempts: attempts++);
    }
    return result;
  }

  Future<dynamic> deliveryConfirmation(Negotiation negotaiton) async {
    return await call<String>(null, 'deliveryConfirmation',
        <String, String>{'negotiationId': negotaiton.id});
  }

  Future<dynamic> receivedConfirmation(Negotiation negotaiton) async {
    return await call<String>(null, 'receivedConfirmation',
        <String, String>{'negotiationId': negotaiton.id});
  }

  Future<String> generateGroupInviteLink(Group group) async {
    String result =
        await call<String>(null, 'generateGroupInvite', <String, String>{
      'groupId': group.id,
    });
    if (result == null) {
      result = await call<String>(null, 'generateGroupInvite', <String, String>{
        'groupId': group.id,
      });
    }
    return result;
  }

  Future<bool> createProductComment(BuildContext context,
      {Product product, Comment comment}) async {
    final dynamic result =
        await call<dynamic>(null, 'createComment', <String, dynamic>{
      'to': 'Product',
      'type': 'text',
      'id': product.id,
      'text': comment.text,
      'mentions': comment.refs.isEmpty
          ? <String>[]
          : comment.refs.values
              .map<String>((DocumentReference ref) => ref.documentID)
              .toList(),
    });
    if (result != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> createGroupMessageComment(BuildContext context,
      {String id, Comment comment}) async {
    final dynamic result =
        await call<dynamic>(null, 'createComment', <String, dynamic>{
      'to': 'LooseComment',
      'type': 'text',
      'id': id,
      'text': comment.text,
      'mentions': comment.refs.isEmpty
          ? <String>[]
          : comment.refs.values
              .map<String>((DocumentReference ref) => ref.documentID)
              .toList(),
    });
    if (result != null) {
      return true;
    } else {
      return false;
    }
  }

  /// Acceptable parameters
  /// {
  ///  "uid" : string?,
  ///  "facebookID" : string?,
  ///  "googleID" : string?,
  ///  "photoURL" : string?,
  ///  "name" : string,
  ///  "email" : string,
  ///  "nickname" : string,
  ///  "password" : string,
  ///  "facebookAccessToken" : string?,
  ///  "googleIDToken" : string?,
  ///  "googleAccessToken" : string?,
  /// }
  ///
  /// Response
  /// {
  ///  "uid" : string,
  ///  "photoURL" : string?,
  ///  "name" : string,
  ///  "email" : string,
  ///  "nickname" : string,
  ///  "password" : string,
  /// }
  ///
  Future<bool> signUp(BuildContext context, Map<String, dynamic> param) async {
    final dynamic result = await call<dynamic>(context, 'firstRegister', param);
    if (result != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateOriginator(BuildContext context, String code) async {
    final dynamic result =
        await call<dynamic>(context, 'validateCode', <String, dynamic>{
      'code': code,
    });
    if (result != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> saveCategories(
      BuildContext context, Map<String, dynamic> param) async {
    final dynamic result =
        await call<dynamic>(context, 'saveCategories', param);
    if (result != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateImage(BuildContext context, String url) async {
    final dynamic result =
        await call<dynamic>(context, 'attPhoto', <String, dynamic>{
      'photo': url,
    });
    if (result != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<Group>> fetchGroupRecommendation() async {
    final dynamic result = await call<dynamic>(
        null, 'fetchGroupRecommendation', <String, dynamic>{});
    // print('RESULT: $result');
    if (result != null) {
      final List<Group> groups =
          (result as List<dynamic>).map<Group>((dynamic g) {
        // print(g);
        return Group.fromJson(g);
        // return Group(
        //     id: g['id'],
        //     name: g['name'],
        //     avatar: g['avatar'],
        //     userAdmin: g['admin'],
        //     location: g['location'],
        //     askToJoin: true,
        //     admin: false,
        //     description: g['description']);
      }).toList();
      return groups;
    } else {
      return <Group>[];
    }
  }

  Future<List<Group>> fetchGroupRecommendationWithFeed() async {
    final dynamic result = await call<dynamic>(
        null, 'fetchGroupRecommendationWithFeed', <String, dynamic>{});
    // print('RESULT: $result');
    if (result != null) {
      final List<Group> groups =
          (result as List<dynamic>).map<Group>((dynamic g) {
        // print(g);
        return Group.fromJson(g);
        // return Group(
        //     id: g['id'],
        //     name: g['name'],
        //     avatar: g['avatar'],
        //     userAdmin: g['admin'],
        //     location: g['location'],
        //     askToJoin: true,
        //     admin: false,
        //     description: g['description']);
      }).toList();
      return groups;
    } else {
      return <Group>[];
    }
  }

  Future<List<User>> fetchFriendRecommendation() async {
    final List<dynamic> result =
        await call(null, 'fetchUserRecommendation', <String, String>{});
    // print(result.first is Map<dynamic, dynamic>);
    if (result == null) {
      return <User>[];
    }
    List<Map<dynamic, dynamic>> users = result
        .map<Map<dynamic, dynamic>>((dynamic u) => u as Map<dynamic, dynamic>)
        .toList();
    return users
        .map<User>((Map<dynamic, dynamic> u) => User(
            u['uid'],
            u['name'],
            u['photoURL'] ??
                'https://firebasestorage.googleapis.com/v0/b/comoo-app.appspot.com/o/defaultUser.png?alt=media&token=15452a28-dadb-4fc5-a20b-36812e908064'))
        .toList();
  }

  Future<String> fetchUserInviteMessage() async {
    final dynamic result =
        await call<dynamic>(null, 'getUserInvite', <String, dynamic>{});
    if (result != null) {
      return result.toString();
    }
    return null;
  }
  // Future<bool> createProductComment(BuildContext context,
  //     {Product product, Comment comment}) async {
  //   final dynamic result =
  //       await call<dynamic>(null, 'createComment', <String, dynamic>{
  //     'to': 'Product',
  //     'type': 'text',
  //     'id': product.id,
  //     'text': comment.text,
  //     'mentions': comment.refs.isEmpty
  //         ? <String>[]
  //         : comment.refs.values
  //             .map<String>((DocumentReference ref) => ref.documentID)
  //             .toList(),
  //   });
  //   if (result != null) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  Future<void> updateFCMToken(String token) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('fcmToken', token);
    call<dynamic>(null, 'signInToken', <String, dynamic>{'token': token});
  }

  Future<void> signOut() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final String token = pref.getString('fcmToken');
    call<dynamic>(null, 'signOutToken', <String, String>{'token': token});
    // pref.setString('fcmToken', null);
    pref.clear();
    await auth.signOut();
  }

  Future<Map<String, int>> fetchNotificationsCounters() async {
    print('chamei');
    final DateTime b4 = DateTime.now();
    final result =
        await call(null, 'getNotificationsInfo', <String, dynamic>{});
    final DateTime after = DateTime.now();
    print(
        'getNotificationsInfo took ${after.difference(b4).inMilliseconds} ms');
    if (result == null) {
      return <String, int>{
        'notifications': 0,
        'chats': 0,
      };
    } else {
      print(result);
      return <String, int>{
        'notifications': result['notifications'],
        'chats': result['chats'],
      };
    }
  }

  Future<void> clearNotificationsCounter() async {
    await call(null, 'clearNotifications', <String, dynamic>{});
  }

  Future<void> clearChatCounter(String id) async {
    await call(null, 'clearChats', <String, String>{'negotiationId': id});
  }

  Future<List<dynamic>> getUserNegotiations() async {
    // print('getting user negotiations');
    final dynamic result =
        await call(null, 'getUserNegotiations', <String, String>{});
    // print('got user negotiations -> $result');
    if (result == null) {
      return <dynamic>[];
    } else {
      final List<dynamic> list = result;
      return list;
    }
  }

  Future<bool> requestGroupEntry(String id) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setBool('_canProgressFromGroups', true);
    print('setted canProgress');
    call(null, 'requestJoinGroup', <String, String>{'groupId': id});
    return true;
  }

  Future<bool> requestGroupExit(String id) async {
    call(null, 'leaveGroup', <String, String>{'groupId': id});
    return true;
  }

  Future<bool> addFriend(String uid) async {
    if (uid == null) {
      return false;
    }
    // call(null, 'addFriend', <String, String>{'userId': uid});
    final FirebaseUser fireUser = await FirebaseAuth.instance.currentUser();
    if (fireUser != null) {
      try {
        final DocumentReference friend = db
            .collection('users')
            .document(fireUser.uid)
            .collection('friends')
            .document(uid);

        await friend.setData({
          'id': db.collection('users').document(uid),
          'origin': 'added',
        }, merge: true);
        return true;
      } catch (e) {
        print(e);
      }
    }
    return false;
  }

  Future<bool> removeFriend(String uid) async {
    if (uid == null) {
      return false;
    }
    // call(null, 'removeFriend', <String, String>{'userId': uid});

    final FirebaseUser fireUser = await FirebaseAuth.instance.currentUser();
    if (fireUser != null) {
      try {
        await db
            .collection('users')
            .document(fireUser.uid)
            .collection('friends')
            .document(uid)
            .delete();
        return true;
      } catch (e) {
        print(e);
      }
    }
    return false;
  }

  Future<String> fetchInviteMessage({Uri uri}) async {
    try {
      final HttpsCallableResult result = await CloudFunctions.instance
          .getHttpsCallable(functionName: 'getUserInvite')
          .call(<String, dynamic>{
        'url': uri?.toString(),
      });
      return result.data;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<GroupSearch>> searchGroupByName(String name) async {
    List<GroupSearch> groups = List();
    final List<dynamic> result = await call(
      null,
      'searchComooByName',
      <String, String>{
        'name': name,
      },
    );
    if (result == null) {
      return <GroupSearch>[];
    }
    groups = result.map<GroupSearch>((dynamic json) {
      GroupSearch group = GroupSearch.fromSnapshot(json);
      group.membersQty = json['members'];
      return group;
    }).toList();
    return groups;
  }

  Future<List<FeedItem>> fetchFeedItens() async {
    final List<dynamic> result =
        await call(null, 'fetchFeed', <String, String>{});
    if (result == null) {
      return <FeedItem>[];
    }
    if (result.isEmpty) {
      return <FeedItem>[];
    }
    // print(result.first['date'] is DateTime);
    final List<FeedItem> feed = <FeedItem>[];
    // final List<Widget> feed = <Widget>[];
    for (Map<dynamic, dynamic> item in result) {
      final String id = item['id'];
      final String type = item['type'];
      if (id?.isNotEmpty ?? false) {
        switch (type) {
          case 'product':
            final DocumentReference ref =
                db.collection('products').document(id);
            final DocumentSnapshot snap = await ref.get();
            if (snap.exists) {
              final FeedItem item = FeedItem(
                id: id,
                type: FeedItemType.product,
                ref: ref,
                snapshot: snap,
              );
              feed.add(item);

              // if (snap['status'] != 'deleted' || !(snap['sold'] ?? true)) {
              //   final Product product = new Product.fromSnapshot(snap);
              //   feed.add(ProductFeed(
              //     product,
              //     key: Key(product.id),
              //   ));
              // }
            }
            break;
          case 'post':
            final DocumentReference ref = db.collection('posts').document(id);
            final DocumentSnapshot snap = await ref.get();
            if (snap.exists) {
              final FeedItem item = FeedItem(
                id: id,
                type: FeedItemType.post,
                ref: ref,
                snapshot: snap,
              );
              feed.add(item);

              // final Post post = Post.fromSnapshot(snap);
              // feed.add(PostFeed(post: post, key: Key(post.id)));
            }
            break;
          default:
            break;
        }
      }
    }
    return feed;
  }

  // Future<void> test() async {
  //   final List<dynamic> result =
  //       await call(null, 'fetchFeed', <String, String>{});
  //   print('Quantity: ${result.length}');
  // }

  Stream<List<DocumentSnapshot>> fetchFeedSnapshots(String uid,
      {int attempts = 0}) {
    if (uid.isNotEmpty) {
      return db
          .collection('users')
          .document(uid)
          .collection('generalFeed')
          .orderBy('date', descending: true)
          .snapshots()
          .map<List<DocumentSnapshot>>(
              (QuerySnapshot query) => query.documents);
      //     .asyncMap<List<FeedItem>>((QuerySnapshot query) async {
      //   List<DocumentSnapshot> docs = query.documents;
      //   print('returned ${docs.length} documents');
      //   if (docs.isEmpty) {
      //     return <FeedItem>[];
      //   } else {
      //     final List<FeedItem> result = <FeedItem>[];
      //     for (DocumentSnapshot doc in docs) {
      //       if (doc.exists) {
      //         final FeedItem item = FeedItem.fromSnapshot(doc);
      //         // final DocumentSnapshot itemSnap = await item.ref.get();
      //         // final QuerySnapshot commentQuery = await item.ref
      //         //     .collection('comments')
      //         //     .orderBy('date', descending: true)
      //         //     .limit(1)
      //         //     .getDocuments();
      //         // if (commentQuery.documents.isNotEmpty) {
      //         //   item.comment = commentQuery.documents.first;
      //         // }
      //         // item.snapshot = itemSnap;
      //         result.add(item);
      //       }
      //     }
      //     return result;
      //   }
      // });
    }
    return Stream.empty();
  }

  Future<List<FeedGroup>> fetchFeedGroups() async {
    final List<dynamic> result =
        await call(null, 'getAllGroups', <String, String>{});
    if (result == null) {
      return <FeedGroup>[];
    }
    if (result.isEmpty) {
      return <FeedGroup>[];
    }

    // print(result.first);
    final List<FeedGroup> groups = <FeedGroup>[];
    for (Map<dynamic, dynamic> item in result) {
      final String id = item['id'] ?? '';
      if (id.isNotEmpty) {
        final Map<dynamic, dynamic> address = item['address'];
        String city = '';
        String state = '';
        if (address != null) {
          city = address['city'] ?? '';
          state = address['state'] ?? '';
        }
        final FeedGroup group = FeedGroup(
          id: id,
          avatar: item['avatar'] ?? '',
          name: item['name'] ?? '',
          description: item['description'] ?? '',
          quantity: ((item['quantity'] ?? 0) as num).toInt(),
          askToJoin: item['askToJoin'] ?? true,
          location: item['location'] ?? '',
          city: city,
          state: state,
        );
        groups.add(group);
      }
    }
    return groups;
  }

  Future<Group> fetchGroupFromId(String id) async {
    if (id?.isEmpty ?? true) {
      return null;
    }
    final DocumentSnapshot snap =
        await db.collection('groups').document(id).get();
    if (!snap.exists) {
      return null;
    }
    return Group.fromSnapshot(snap);
  }

  Future<Product> fetchProductFromId(String id) async {
    if (id?.isEmpty ?? true) {
      return null;
    }
    final DocumentSnapshot snap =
        await db.collection('products').document(id).get();
    if (!snap.exists) {
      return null;
    }
    return Product.fromSnapshot(snap);
  }

  Future<void> openedGroupFeed(String id) async {
    call(null, 'lastSeenAGroup', <String, String>{'groupId': id});
  }

  Future<List<Comment>> fetchProductComments(String id, {int limit = 0}) async {
    Query _query = db
        .collection('products')
        .document(id)
        .collection('comments')
        .orderBy('date', descending: true);
    if (limit > 0) {
      _query = _query.limit(limit);
    }
    final QuerySnapshot query = await _query.getDocuments();

    final List<Comment> comments = <Comment>[];
    for (DocumentSnapshot doc in query.documents) {
      if (doc.exists) {
        DocumentReference userRef = doc.data['from'];
        final DocumentSnapshot userDoc = await userRef.get();
        final User user = User.fromSnapshot(userDoc);
        final Comment comment = Comment(
          doc.data['text'],
          user,
          (doc.data['date'] is Timestamp)
              ? DateTime.fromMillisecondsSinceEpoch(
                  (doc.data['date'] as Timestamp).millisecondsSinceEpoch)
              : (doc.data['date'] as DateTime),
        );
        comments.add(comment);
      }
    }
    return comments;
  }

  Future<Map<String, dynamic>> requestProduct(String id) async {
    final dynamic result =
        await call(null, 'requestProduct', <String, String>{'productId': id});
    print(result);
    if (result == null) {
      return null;
    }
    Map<String, dynamic> map = <String, dynamic>{};
    if (result['isFirst'] ?? false) {
      var _negotiationData = result['negotiation'];
      final DocumentSnapshot snap = await db
          .collection('negotiations')
          .document(_negotiationData['id'])
          .get();
      if (snap.exists) {
        final Negotiation negotiation = Negotiation.fromSnapshot(snap);
        map.putIfAbsent('isFirst', () => true);
        map.putIfAbsent('negotiation', () => negotiation);
      }
    } else {
      final int count = (result['count'] as num)?.toInt();
      map.putIfAbsent('isFirst', () => false);
      map.putIfAbsent('count', () => count);
    }
    return map;
  }

  Future<bool> fetchUserHasRequestedProduct(
      String uid, String productId) async {
    final DocumentReference userRef = db.collection('users').document(uid);
    final QuerySnapshot requestsSnapshot = await db
        .collection('products')
        .document(productId)
        .collection('requests')
        .where('user', isEqualTo: userRef)
        .getDocuments();
    List<String> requestsIds = <String>[];
    requestsSnapshot.documents.forEach((req) {
      if (req['status'] != 'cancelled') {
        requestsIds.add(req.documentID);
      }
    });
    return requestsIds.isNotEmpty;
  }

  Future<bool> deleteProduct(String id) async {
    final dynamic result =
        await call(null, 'deleteProduct', <String, String>{'productId': id});
    if (result == null) {
      return false;
    }
    return true;
  }

  Future<List<Group>> fetchUserGroups(String id) async {
    final QuerySnapshot query = await db
        .collection('users')
        .document(id)
        .collection('groups')
        .getDocuments();
    if (query?.documents?.isEmpty ?? true) {
      return <Group>[];
    } else {
      return query.documents
          .map<Group>((DocumentSnapshot snap) => Group.fromSnapshot(snap))
          .toList();
    }
  }

  Stream<List<Negotiation>> fetchUserSellingsStream() {
    final String uid = currentUser?.uid ?? '';
    final DocumentReference userRef = db.collection('users').document(uid);
    return db
        .collection('negotiations')
        .where('seller', isEqualTo: userRef)
        .orderBy('lastUpdate', descending: true)
        .snapshots()
        .asyncMap<List<Negotiation>>((QuerySnapshot query) async {
      final List<Negotiation> negotiations = <Negotiation>[];
      for (DocumentSnapshot doc in query.documents) {
        final Negotiation negotiation = await _fetchNegotiation(doc);
        negotiations.add(negotiation);
      }
      return negotiations;
    });
  }

  Stream<List<Negotiation>> fetchUserBuyingsStream() {
    final String uid = currentUser?.uid ?? '';
    final DocumentReference userRef = db.collection('users').document(uid);
    return db
        .collection('negotiations')
        .where('client', isEqualTo: userRef)
        .orderBy('lastUpdate', descending: true)
        .snapshots()
        .asyncMap<List<Negotiation>>((QuerySnapshot query) async {
      final List<Negotiation> negotiations = <Negotiation>[];
      for (DocumentSnapshot doc in query.documents) {
        final Negotiation negotiation = await _fetchNegotiation(doc);
        if (negotiation.status != 'cancelled') {
          negotiations.add(negotiation);
        }
      }
      return negotiations;
    });
  }

  Future<Negotiation> _fetchNegotiation(DocumentSnapshot s) async {
    final Negotiation negotiation = Negotiation.fromSnapshot(s);

    if (negotiation.lastMessage.isEmpty) {
      QuerySnapshot messagesQuery = await db
          .collection('negotiations')
          .document(negotiation.id)
          .collection('messages')
          .orderBy('date', descending: true)
          .getDocuments();
      DocumentSnapshot messageSnap = messagesQuery.documents.isEmpty
          ? null
          : messagesQuery.documents.first;
      negotiation.message = messageSnap == null
          ? null
          : NegotiationMessage.fromSnapshot(messageSnap);
      negotiation.lastMessage =
          negotiation.message == null ? '' : negotiation.message.text;
      print('${negotiation.title} - Last message: ${negotiation.lastMessage}');
    }
    await negotiation.buyer.get().then((buyer) {
      negotiation.buyerName = buyer.data['name'];
    });
    await negotiation.seller.get().then((seller) {
      negotiation.sellerName = seller.data['name'];
    });
    return negotiation;
  }

  Future<List<GroupUser>> fetchCurrentUserGroups() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    final String uid = _prefs.getString('uid');
    if (uid != null) {
      final QuerySnapshot query = await db
          .collection('users')
          .document(uid)
          .collection('groups')
          .getDocuments();
      if (query.documents.isEmpty) {
        return <GroupUser>[];
      } else {
        final List list = <GroupUser>[];
        for (DocumentSnapshot snapshot in query.documents) {
          final GroupUser group = GroupUser.fromSnapshot(snapshot);
          list.add(group);
        }
        return list;
      }
    } else {
      return <GroupUser>[];
    }
  }
}
