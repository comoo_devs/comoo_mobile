import 'package:flutter/widgets.dart';

import 'network.dart';

class AuthApi extends NetworkApi {
  /// Acceptable parameters
  /// {
  ///  "uid" : string?,
  ///  "facebookID" : string?,
  ///  "googleID" : string?,
  ///  "photoURL" : string?,
  ///  "name" : string,
  ///  "email" : string,
  ///  "nickname" : string,
  ///  "password" : string,
  ///  "facebookAccessToken" : string?,
  ///  "googleIDToken" : string?,
  ///  "googleAccessToken" : string?,
  /// }
  ///
  /// Response
  /// {
  ///  "uid" : string,
  ///  "photoURL" : string?,
  ///  "name" : string,
  ///  "email" : string,
  ///  "nickname" : string,
  ///  "password" : string,
  /// }
  ///
  Future<bool> signUp(BuildContext context, Map<String, dynamic> param) async {
    final dynamic result = await call<dynamic>(context, 'firstRegister', param);
    if (result != null) {
      return true;
    } else {
      return false;
    }
  }
}
