import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comoo/src/comoo/comments.dart';
import 'package:flutter/widgets.dart';

import 'network.dart';

class GroupMessageApi extends NetworkApi {
  final String collectionName = 'loose_comments';
  Future<void> createGroupMessage(BuildContext context,
      {String id, String message}) async {
    final dynamic result =
        await call(context, 'createLooseComments', <String, dynamic>{
      'message': message,
      'groups': <String>[id]
    });
    print(result);
  }

  Future<void> likeGroupMessage(BuildContext context, String id) async {
    return await call(context, 'likeALooseComment', <String, dynamic>{
      'looseCommentId': id,
    });
  }

  Future<bool> deleteGroupMessage(BuildContext context, String id) async {
    final dynamic result =
        call(context, 'deleteLooseComment', <String, dynamic>{
      'looseCommentId': id,
    });
    return result != null;
  }

  Future<List> fetchGroupComments(String id) async {
    final DocumentReference ref = db.collection(collectionName).document(id);
    final DocumentSnapshot snap = await ref.get();
    if (snap.exists) {
      final QuerySnapshot query =
          await ref.collection('comments').getDocuments();
      return query.documents;
    }
    return [];
  }
}
