# COMOO

Uma nova forma de vender e comprar em grupo.

## Getting Started

+ Clone repository
+ Open project folder with terminal
+ Run `flutter run`


## Folder Structure
```bash
/ (PROJECT_FOLDER)
│  README.md # This file
│  pubspec.yaml # Contains all packages imported
│
└──[certificates] # Contains all certificates and keys
│
└──[fonts] # Contains all fonts
│
└──[images] # Contains all images
│
└──[svg] #~DEPRECATED~ Contains all svg images
│
└──[texts] # Contains all texts
│
└──[android] # Contains all fonts needed for the project
│  │  Podfile # Contains iOS configuration
│  │
│  └──[app]
│     │  google-services.json # Contains Android Firebase cofigurations
│     │  key.jks # Contains android key authentications
│     └──...
│
└──[ios]
│  │  Podfile
│  │  GoogleService-Info.plist # Contains iOS Firebase cofigurations
│  └──[Runner]
│     │  Info.plist # Contains iOS cofigurations
│     └──...
│
└──[lib]
   │  main.dart
   └──[src]
      │  comoo_app.dart # MaterialApp and Routes to the App
      │
      └──[comoo] # Contains DAO file
      │
      └──[pages] # Contains pages displayed
      │  │  welcome.dart # First page that routes to landing page or home page
      │  │
      │  └──[common] # Contains common pages that may be called by any page
      │  │
      │  └──[landing] # Contains landing page structure
      │
      └──[utility] # Contains utility functions
      │
      └──[widget] # Contains custom widgets (utility widgets)
```

### Dependecies
  - [rxdart](https://pub.dartlang.org/packages/rxdart)
  - [image_picker](https://pub.dartlang.org/packages/image_picker)
  - [firebase_analytics](https://pub.dartlang.org/packages/firebase_analytics)
  - [firebase_auth](https://pub.dartlang.org/packages/firebase_auth)
  - [cloud_firestore](https://pub.dartlang.org/packages/cloud_firestore)
  - [firebase_storage](https://pub.dartlang.org/packages/firebase_storage)
  - [shared_preferences](https://pub.dartlang.org/packages/shared_preferences)
  - [google_sign_in](https://pub.dartlang.org/packages/google_sign_in)
  - timeago
  - async_loader
  - share
  - uuid
  - nanoid
  - [flutter_facebook_login](https://pub.dartlang.org/packages/flutter_facebook_login)
  - [firebase_messaging](https://pub.dartlang.org/packages/firebase_messaging)
  - camera
  - carousel_pro
  - intl
  - image_cropper
  - flutter_masked_text
  - cloud_functions
  - flutter_advanced_networkimage
  - url_launcher
  - money
  - http
  - flushbar
  - masked_text
  - font_awesome_flutter
  - [firebase_dynamic_links](https://pub.dartlang.org/packages/firebase_dynamic_links)
  - flutter_svg
  - cached_network_image
  - connectivity